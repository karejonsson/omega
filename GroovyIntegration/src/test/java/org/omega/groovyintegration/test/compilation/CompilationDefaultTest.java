package org.omega.groovyintegration.test.compilation;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.bsf.BSFManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.Cruddable;
import org.omega.groovyintegration.scope.GenerateJava;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

public class CompilationDefaultTest {
	
	public static final String defaultpackage = "org.omega.testdefault";
	
	@Before
	public void setup() throws Exception {
		String project = System.getProperty("user.dir");

		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		EmbeddedModelHandling details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/countrywithdefault.omega", 
				"UTF-8", "mymemdb", "SA", "", "frametitle");
		
		ModelConfiguration config = details.getModelConfiguration();
		
		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			//System.out.println(javacode);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}

		TableConfiguration country = config.getDBTableHandlingSpecificationOnTableName("country");
		env = new EmbeddedTableHandling(details, country);
		
		pool = new DBPool(details);
		
		Class<?>[] helloClasses = null;
		try {
			helloClasses = InMemoryJavaCompiler.compile(compile);
			assertTrue(true);
		}
		catch(Exception e) {
			assertTrue(false);
		}

		one = helloClasses[0];
		
	}
	
	@After
	public void teardown() throws Exception {
		OmegaEmbeddedHSQL.clearMemory(env.getDetails().getConnection());
		one = null;
		env = null;
		pool = null;
	}
	
	private Class<?> one = null;
	private EmbeddedTableHandling env = null;
	private DBPool pool = null;
	
	@Test
	public void invokeForDefault() throws Exception {
		Method setDefaults = one.getDeclaredMethod("setDefaults", EmbeddedTableHandling.class, DBPool.class);
	    setDefaults.invoke(null, env, pool);

		Method newCountry = one.getDeclaredMethod("newCountry");
		Cruddable cruddable = (Cruddable) newCountry.invoke(null);
		
		BSFManager manager = new BSFManager();
		manager.declareBean("node", cruddable, Object.class);
		Object out = manager.eval("groovy", "script.groovy", 0, 0, "return node.name;");

		assertTrue(out != null);
		assertTrue(out instanceof String);
		assertTrue(out.toString().contains("Sverige"));
	}

	@Test
	public void invokeForParticular() throws Exception {
		Constructor<?> cons = one.getConstructor(DBPool.class, DBPoolObject.class, Boolean.class);

		DBPoolObject po = new DBPoolObject(null, env);

		Cruddable cruddable = (Cruddable) cons.newInstance(pool, po, false);

		BSFManager manager = new BSFManager();
		manager.declareBean("node", cruddable, Object.class);
		Object out = manager.eval("groovy", "script.groovy", 0, 0, 
				"node.code = \"SE\";\n"+
				"return node.code+node.code;");

		assertTrue(out != null);
		assertTrue(out instanceof String);
		assertTrue(out.toString().contains("SESE"));
	}

}
