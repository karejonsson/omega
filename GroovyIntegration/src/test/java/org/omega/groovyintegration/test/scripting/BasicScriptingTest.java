package org.omega.groovyintegration.test.scripting;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.bsf.BSFManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.scope.Cruddable;
import org.omega.groovyintegration.scope.GenerateJava;
import org.omega.memorycompiler.DynamicClassLoader;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

import groovy.lang.GroovyClassLoader;
import groovy.util.GroovyScriptEngine;
import sun.reflect.Reflection;
import groovy.lang.GroovyClassLoader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class BasicScriptingTest {

	public static final String defaultpackage = "p";//"org.omega.basicscripting";

	@Before
	public void setup() throws Exception {
		String project = System.getProperty("user.dir");
		
		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/countrycity-eng.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");

		ModelConfiguration config = details.getModelConfiguration();

		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}

		pool = new DBPool(details);

		Class<?>[] classes = null;
		try {
			classes = InMemoryJavaCompiler.compile(compile);
			assertTrue(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}

		DynamicClassLoader dcl = new DynamicClassLoader(classes[0].getClassLoader());

		for(Class clazz : classes) {
			String fullyQualifyingClassname = clazz.getName();
			int idx = Math.max(0, fullyQualifyingClassname.lastIndexOf('.'))+1;
			String objectName = fullyQualifyingClassname.substring(idx);
			TableConfiguration ths = config.getDBTableHandlingSpecificationOnObjectName(objectName);

			Method setDefaults = clazz.getDeclaredMethod("setDefaults", EmbeddedTableHandling.class, DBPool.class);
			setDefaults.invoke(null, new EmbeddedTableHandling(details, ths), pool);
		}

		manager = new BSFManager();
		manager.setClassLoader(dcl);
	}

	private Class<?> one = null;
	EmbeddedModelHandling details = null;
	private DBPool pool = null;
	private BSFManager manager = null;

	@After
	public void teardown() throws Exception {
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
		//one = null;
		details = null;
		pool = null;
		manager = null;
	}

	private String getScript(String relpath) throws IOException {
		String project = System.getProperty("user.dir");
		byte[] encoded = Files.readAllBytes(Paths.get(project+"/src/test/resources/"+relpath));
		return new String(encoded, "UTF-8");
	}

	@Test
	public void t1_hello() throws Exception {
		String script = getScript("basicscripting/1.ogy");
		manager.eval("groovy", "script.groovy", 0, 0, script);
	}

	@Test
	public void t2_new() throws Exception {
		String script = getScript("basicscripting/2.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof String);
		assertTrue(o.toString().equals("Sverige"));
	}

	@Test
	public void t3_persist() throws Exception {
		String script = getScript("basicscripting/3.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
	}

	/*
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
		}
	 */
	@Test
	public void t4_persist_nordics() throws Exception {
		String script = getScript("basicscripting/4.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
	}

	@Test
	public void t5_set_downstream() throws Exception {
		String script = getScript("basicscripting/5.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
	}

	@Test
	public void t6_set_again() throws Exception {
		String script = getScript("basicscripting/6.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof String);
		assertTrue(o.toString().equals("Sweden"));
	}

	@Test
	public void t7_double_deref() throws Exception {
		String script = getScript("basicscripting/7.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof String);
		assertTrue(o.toString().equals("Sverige"));
	}

	@Test
	public void t8_find_by() throws Exception {
		String script = getScript("basicscripting/8.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof Integer);
		assertTrue(((Integer) o) == 102);
	}

	@Test 
	public void t9_upstream() throws Exception {
		String script = getScript("basicscripting/9.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof Integer);
		assertTrue(((Integer) o) == 3);
	}

	@Test 
	public void t10_all() throws Exception {
		String script = getScript("basicscripting/10.ogy");
		Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		assertTrue(o != null);
		assertTrue(o instanceof Integer);
		assertTrue(((Integer) o) == 4);
	}

	@Test 
	public void t11_null_value() throws Exception {
		String script = getScript("basicscripting/11.ogy");
		//Object o = manager.eval("groovy", "script.groovy", 0, 0, script);
		//assertTrue(o == null);
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
		}
	}

}
