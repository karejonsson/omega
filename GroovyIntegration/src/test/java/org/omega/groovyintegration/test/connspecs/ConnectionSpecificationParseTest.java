package org.omega.groovyintegration.test.connspecs;

import static org.junit.Assert.*;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.connectionspecification.parse.OmegaConnectionSpecificationParser;
import org.omega.connectionspecification.struct.DBConnectionSpecification;
import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

public class ConnectionSpecificationParseTest {

	private ClassLoader cl = null;
	
	@Before
	public void setup() {
		cl = this.getClass().getClassLoader();		
	}
	
	@After
	public void teardowm() {
		cl = null;
	}

	@Test
	public void mssqlserver() throws Exception {
		InputStream is = cl.getResourceAsStream("connspecs/mssqlserver.oapp");
		InputStreamReader isr = new InputStreamReader(is);
		DBConnectionSpecification dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		assertTrue(dbcs instanceof JDBCConnectionSpecification);
		JDBCConnectionSpecification jdbcs = (JDBCConnectionSpecification) dbcs;
		TunnelForwardLParameters tflp = jdbcs.getTunnelForwardLParameters();
		assertTrue(tflp != null);
	}

	@Test
	public void mssqlserver_nossh() throws Exception {
		InputStream is = cl.getResourceAsStream("connspecs/mssqlserver_nossh.oapp");
		InputStreamReader isr = new InputStreamReader(is);
		DBConnectionSpecification dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		assertTrue(dbcs instanceof JDBCConnectionSpecification);
		JDBCConnectionSpecification jdbcs = (JDBCConnectionSpecification) dbcs;
		TunnelForwardLParameters tflp = jdbcs.getTunnelForwardLParameters();
		assertTrue(tflp == null);
	}

	@Test
	public void postgres_ssh() throws Exception {
		InputStream is = cl.getResourceAsStream("connspecs/postgres_ssh.oapp");
		InputStreamReader isr = new InputStreamReader(is);
		DBConnectionSpecification dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		assertTrue(dbcs instanceof JDBCConnectionSpecification);
		JDBCConnectionSpecification jdbcs = (JDBCConnectionSpecification) dbcs;
		TunnelForwardLParameters tflp = jdbcs.getTunnelForwardLParameters();
		assertTrue(tflp != null);
	}

	@Test
	public void postgres() throws Exception {
		InputStream is = cl.getResourceAsStream("connspecs/postgres.oapp");
		InputStreamReader isr = new InputStreamReader(is);
		DBConnectionSpecification dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		assertTrue(dbcs instanceof JDBCConnectionSpecification);
		JDBCConnectionSpecification jdbcs = (JDBCConnectionSpecification) dbcs;
		TunnelForwardLParameters tflp = jdbcs.getTunnelForwardLParameters();
		assertTrue(tflp == null);
	}

	@Test
	public void memory() throws Exception {
		InputStream is = cl.getResourceAsStream("connspecs/memory.oapp");
		InputStreamReader isr = new InputStreamReader(is);
		DBConnectionSpecification dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		assertTrue(dbcs instanceof JDBCConnectionSpecification);
		JDBCConnectionSpecification jdbcs = (JDBCConnectionSpecification) dbcs;
		TunnelForwardLParameters tflp = jdbcs.getTunnelForwardLParameters();
		assertTrue(tflp == null);
	}

}
