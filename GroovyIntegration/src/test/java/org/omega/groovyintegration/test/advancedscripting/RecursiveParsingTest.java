package org.omega.groovyintegration.test.advancedscripting;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.executablescript.struct.CombinedFileStruct;
import org.omega.executablescript.struct.Flow;
import org.omega.executablescript.struct.FlowStruct;
import org.omega.executablescript.struct.ParseRecursively;
import org.omega.executablescript.struct.ScriptStruct;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.groovyscripting.GroovyShellScriptingScope;

public class RecursiveParsingTest {

	private File getScript(String relpath) throws IOException {
		String project = System.getProperty("user.dir");

		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		File out = new File(project+"/src/test/resources/"+relpath);
		if(!out.exists()) {
			return null;
		}
		return out;
	}

	private ClassLoader cl = null;
	
	@Before
	public void setup() {
		cl = this.getClass().getClassLoader();		
	}
	
	@After
	public void teardowm() {
		cl = null;
	}

	@Test
	public void test1() throws Exception {
		File file = getScript("executables/1.oexe");
		assertTrue(file.exists());
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);
		assertTrue(cfs != null);
		
		assertTrue(cfs.getConnectionKeys().size() == 1);
		assertTrue(cfs.getScriptKeys().size() == 2);
		assertTrue(cfs.getFlowKeys().size() == 1);
		
		System.out.println(cfs.toString());
	}

	@Test
	public void test2() throws Exception {
		File file = getScript("executables/2.oexe");
		assertTrue(file.exists());
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);
		assertTrue(cfs != null);
		
		assertTrue(cfs.getConnectionKeys().size() == 2);
		assertTrue(cfs.getScriptKeys().size() == 2);
		assertTrue(cfs.getFlowKeys().size() == 1);
		
		System.out.println(cfs.toString());
	}

	@Test
	public void test345() throws Exception {
		File file = getScript("executables/5.oexe");
		assertTrue(file.exists());
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);
		assertTrue(cfs != null);
		
		System.out.println(cfs.toString());
	}

	@Test
	public void test6() throws Exception {
		File file = getScript("executables/6.oexe");
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);
		assertTrue(cfs.getScriptKeys().size() == 1);
		assertTrue(cfs.getFlowKeys().size() == 0);
		Vector<ScriptStruct> sss = cfs.getScripts("bye");
		assertTrue(sss.size() == 2);
	}
	
	@Test
	public void test7() throws Exception {
		File file = getScript("executables/7.oexe");
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);

		assertTrue(cfs.getScriptKeys().size() == 2);
		assertTrue(cfs.getFlowKeys().size() == 1);
		Vector<FlowStruct> flows = cfs.getFlows("id");
		
		assertTrue(flows != null);
		assertTrue(flows.size() == 1);
		
		FlowStruct flow = flows.get(0);
		Flow f = flow.getFlow();
		
		GroovyShellScriptingScope mmss = new GroovyShellScriptingScope(null);
		Object out = f.evaluate(cfs, mmss);
		
		assertTrue(out != null);
		assertTrue(out instanceof Integer);
		assertTrue(((Integer) out) == 51);
	}
	
	@Test
	public void test8() throws Exception {
		File file = getScript("executables/8.oexe");
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);

		Vector<FlowStruct> flows = cfs.getFlows("id");
		
		assertTrue(flows != null);
		assertTrue(flows.size() == 1);
		
		FlowStruct flow = flows.get(0);
		Flow f = flow.getFlow();
		
		GroovyShellScriptingScope mmss = new GroovyShellScriptingScope(null);
		Object out = f.evaluate(cfs, mmss);
		
		assertTrue(out != null);
		assertTrue(out instanceof Integer);
		assertTrue(((Integer) out) == 2);
	}
	
	@Test
	public void test9() throws Exception {
		File file = getScript("executables/9.oexe");
		CombinedFileStruct cfs = ParseRecursively.parse(file, null);

		Vector<FlowStruct> flows = cfs.getFlows("lister");
		
		assertTrue(flows != null);
		assertTrue(flows.size() == 1);
		
		FlowStruct flow = flows.get(0);
		Flow f = flow.getFlow();
		
		GroovyShellScriptingScope mmss = new GroovyShellScriptingScope(null);
		Object out = f.evaluate(cfs, mmss);
		
		assertTrue(out != null);
		assertTrue(out instanceof List);
		List l = (List) out;
		assertTrue(l.size() == 4);
		assertTrue((Integer) l.get(0) == 2);
		assertTrue((Integer) l.get(1) == 9);
		assertTrue((Integer) l.get(2) == 5);
		assertTrue((Integer) l.get(3) == 7);

	}

}
