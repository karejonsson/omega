package org.omega.groovyintegration.test.compilation;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.groovyintegration.scope.GenerateJava;

public class CompilationDefaultvalsTest {
	
	private ModelConfiguration config = null;
	
	@Before
	public void setup() throws Exception {
		File fi = new File(System.getProperty("user.dir"));
		try {
			File conf = new File(fi, "../Configuration/src/test/resources/defaultvals.omegaconf");
			config = new ModelConfiguration();
			InputStreamReader isr = new InputStreamReader(new FileInputStream(conf), "UTF-8");
			OmegaConfigParser.parseOmegaConfig(config, isr, "File "+conf.getCanonicalPath());
			config.performTableOrderCalcule();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void teardowm() {
		config = null;
	}

	public static final String defaultpackage = "org.omega.test.generatedmodel";

	@Test
	public void sample1() throws Exception {
		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			//System.out.println(javacode);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}
		try {
			Class<?>[] helloClasses = InMemoryJavaCompiler.compile(compile);
			assertTrue(true);
		}
		catch(Exception e) {
			assertTrue(false);
		}
	}

}
