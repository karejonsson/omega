package org.omega.groovyintegration.test.scripting;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.bsf.BSFManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.scope.GenerateJava;
import org.omega.memorycompiler.DynamicClassLoader;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

public class VariousScriptingTest {

	public static final String defaultpackage = "ccpp";

	@Before
	public void setup() throws Exception {
		String project = System.getProperty("user.dir");

		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/countrycitypersonparty.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");

		ModelConfiguration config = details.getModelConfiguration();

		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}

		pool = new DBPool(details);

		Class<?>[] classes = null;
		try {
			classes = InMemoryJavaCompiler.compile(compile);
			assertTrue(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}

		for(Class clazz : classes) {
			String fullyQualifyingClassname = clazz.getName();
			int idx = Math.max(0, fullyQualifyingClassname.lastIndexOf('.'))+1;
			String objectName = fullyQualifyingClassname.substring(idx);
			TableConfiguration ths = config.getDBTableHandlingSpecificationOnObjectName(objectName);

			Method setDefaults = clazz.getDeclaredMethod("setDefaults", EmbeddedTableHandling.class, DBPool.class);
			setDefaults.invoke(null, new EmbeddedTableHandling(details, ths), pool);
		}

		manager = new BSFManager();
		DynamicClassLoader dcl = new DynamicClassLoader(classes[0].getClassLoader());
		manager.setClassLoader(dcl);
		
		String script = getScript("variousscripting/setup.ogy");
		manager.eval("groovy", "script.groovy", 0, 0, script);

	}

	private Class<?> one = null;
	EmbeddedModelHandling details = null;
	private DBPool pool = null;
	private BSFManager manager = null;

	@After
	public void teardown() throws Exception {
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
		details = null;
		pool = null;
		manager = null;
	}

	private String getScript(String relpath) throws IOException {
		String project = System.getProperty("user.dir");
		byte[] encoded = Files.readAllBytes(Paths.get(project+"/src/test/resources/"+relpath));
		return new String(encoded, "UTF-8");
	}

	@Test
	public void t1_hello() throws Exception {
		String script = getScript("variousscripting/1.ogy");
		manager.eval("groovy", "script.groovy", 0, 0, script);
	}

	@Test
	public void t2_country_up() throws Throwable {
		String script = getScript("variousscripting/2.ogy");
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw t;
		}
	}

	@Test
	public void t3_country_up() throws Throwable {
		String script = getScript("variousscripting/3.ogy");
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw t;
		}
	}

	@Test
	public void t4_country_up_null() throws Throwable {
		String script = getScript("variousscripting/4.ogy");
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw t;
		}
	}

	@Test
	public void t5_assign_null() throws Throwable {
		String script = getScript("variousscripting/5.ogy");
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw t;
		}
		assertTrue(o == null);
	}

	@Test
	public void t6_compare() throws Throwable {
		String script = getScript("variousscripting/6.ogy");
		Object o = null;
		try {
			o = manager.eval("groovy", "script.groovy", 0, 0, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw t;
		}
		assertTrue(o != null);
		assertTrue(o instanceof Integer);
		Integer i = (Integer) o;
		assertTrue(i == 1);
	}


}
