package org.omega.groovyintegration.test.compilation;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.groovyintegration.scope.GenerateJava;

public class JavaGenerateDevelopMany {

	public static final String defaultpackage = "gdbapp";

	public static void main(String args[]) throws Exception {
		File fi = null;
		if(args.length == 1) {
			fi = new File(args[0]);
		}
		else {
			fi = new File(System.getProperty("user.dir"));
		}
		//System.out.println("Katalog "+fi.getCanonicalPath());
		File conf = new File(fi, "src/test/resources/countrycity.omegaconf");
		//System.out.println("Konfig "+conf.getCanonicalPath()+", finns "+conf.exists());
		ModelConfiguration config = new ModelConfiguration();
		InputStreamReader isr = new InputStreamReader(new FileInputStream(conf), "UTF-8");
		OmegaConfigParser.parseOmegaConfig(config, isr, "File "+conf.getCanonicalPath());
		config.performTableOrderCalcule();
		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			System.out.println(javacode);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}
		
		Class<?>[] helloClasses = InMemoryJavaCompiler.compile(compile);

	}
	
}
 