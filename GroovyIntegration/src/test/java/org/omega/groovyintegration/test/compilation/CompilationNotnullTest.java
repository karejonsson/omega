package org.omega.groovyintegration.test.compilation;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.bsf.BSFManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.Cruddable;
import org.omega.groovyintegration.scope.GenerateJava;

public class CompilationNotnullTest {

	private ModelConfiguration config = null;
	
	@Before
	public void setup() throws Exception {
		File fi = new File(System.getProperty("user.dir"));
		try {
			File conf = new File(fi, "../Configuration/src/test/resources/notnull.omegaconf");
			config = new ModelConfiguration();
			InputStreamReader isr = new InputStreamReader(new FileInputStream(conf), "UTF-8");
			OmegaConfigParser.parseOmegaConfig(config, isr, "File "+conf.getCanonicalPath());
			config.performTableOrderCalcule();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void teardowm() {
		config = null;
	}

	public static final String defaultpackage = "org.omega.test.generatedmodel";

	@Test
	public void notnull() throws Exception {
		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(defaultpackage, ths);
			//System.out.println(javacode);
			compile[ctr++] = new SourceCode(defaultpackage+"."+ths.getObjectName(), javacode);
		}
		Class<?>[] helloClasses = null;
		try {
			helloClasses = InMemoryJavaCompiler.compile(compile);
			assertTrue(true);
		}
		catch(Exception e) {
			assertTrue(false);
		}
		
	}

}
