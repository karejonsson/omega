package org.omega.groovyintegration.test.advancedscripting;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.InputStreamReader; 

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.executablescript.parse.OmegaExecutableScriptParser;
import org.omega.executablescript.struct.OneFileStruct;

public class BasicParsingTest {

	private ClassLoader cl = null;
	
	@Before
	public void setup() {
		cl = this.getClass().getClassLoader();		
	}
	
	@After
	public void teardowm() {
		cl = null;
	}

	@Test
	public void test1() throws Exception {
		InputStream is = cl.getResourceAsStream("executables/1.oexe");
		InputStreamReader isr = new InputStreamReader(is);
		OneFileStruct ofs = OmegaExecutableScriptParser.parseExecutableScript(isr, null);
		assertTrue(true);
	}

}
