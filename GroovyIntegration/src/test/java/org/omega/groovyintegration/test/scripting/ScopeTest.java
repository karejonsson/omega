package org.omega.groovyintegration.test.scripting;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.bsf.BSFException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.renderers.SpecifiedTextRenderer;
import org.omega.configuration.struct.ProgramBlock;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;

import se.modlab.generics.exceptions.IntolerableException;

public class ScopeTest {
	
	private ScriptingScopeFactory factory = null;
	private EmbeddedModelHandling details = null;
	
	@Before
	public void setup() throws IntolerableException, Exception {
		String project = System.getProperty("user.dir");

		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/countrycitypersonparty.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");
		
		SystemProgrammingResourceAccess spra = new SystemProgrammingResourceAccess() {
			@Override
			public String textDialog_OK_Cancel(String frametitle, String question) {
				System.out.println("SystemProgrammingResourceAccess.textDialog_OK_Cancel(\""+frametitle+"\", \""+question+"\")");
				return null;
			}

			@Override
			public void infoMessageDialog_OK(String frametitle, String message) {
				System.out.println("SystemProgrammingResourceAccess.infoMessageDialog_OK(\""+frametitle+"\", \""+message+"\")");
				// TODO Auto-generated method stub
				
			}

			@Override
			public void errorMessageDialog_OK(String frametitle, String message) {
				System.out.println("SystemProgrammingResourceAccess.errorMessageDialog_OK(\""+frametitle+"\", \""+message+"\")");
				// TODO Auto-generated method stub
				
			}

			@Override
			public void warningMessageDialog_OK(String frametitle, String message) {
				System.out.println("SystemProgrammingResourceAccess.warningMessageDialog_OK(\""+frametitle+"\", \""+message+"\")");
				// TODO Auto-generated method stub
				
			}

			@Override
			public Boolean infoChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
				System.out.println("SystemProgrammingResourceAccess.infoChoiceDialog_YES_NO_Cancel(\""+frametitle+"\", \""+question+"\")");
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Boolean warningChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
				System.out.println("SystemProgrammingResourceAccess.warningChoiceDialog_YES_NO_Cancel(\""+frametitle+"\", \""+question+"\")");
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Boolean errorChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
				System.out.println("SystemProgrammingResourceAccess.errorChoiceDialog_YES_NO_Cancel(\""+frametitle+"\", \""+question+"\")");
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void announceRuntimeError(String message) {
				System.out.println("SystemProgrammingResourceAccess.announceRuntimeError(\""+message+"\")");
				// TODO Auto-generated method stub
			}
		};
		
		factory = new ScriptingScopeFactory(details, "abc.def", spra);
	}
	
	@After
	public void teardown() throws Exception {
		factory = null;
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
		details = null;
	}
	
	private String getScript(String relpath) throws IOException {
		String project = System.getProperty("user.dir");
		byte[] encoded = Files.readAllBytes(Paths.get(project+"/src/test/resources/"+relpath));
		return new String(encoded, "UTF-8");
	}

	@Test
	public void t1() throws BSFException, IOException {
		ScriptingScope scope = factory.getNodelessScope();
		String script = getScript("scopescripts/1.ogy");
		ProgramBlock pb = new ProgramBlock("Test resurce scopescripts/1.ogy", script, -1, -1, -1, -1);
		scope.exec(pb);
	}

	@Test
	public void t2() throws Exception {
		EmbeddedModelHandling details = factory.getDetails();
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnObjectName("Country");
		EmbeddedTableHandling env = new EmbeddedTableHandling(details, ths);
		DBPoolObject po = new DBPoolObject(null, env);
		po.set("name", "Knäckebrödhult");
		SpecifiedTextRenderer str = new SpecifiedTextRenderer("Inline for test", "return node.name;", -1, -1, -1, -1);
		ScriptingScope scope = factory.getNodeScope(po, false);

		Object out = scope.eval(str);
		assertTrue(out != null);
		assertTrue(out instanceof String);
		assertTrue(out.toString().equals("Knäckebrödhult"));
		System.out.println("2: "+out);
	}

}
