package org.omega.groovyintegration.test.compilation;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.groovyintegration.scope.GenerateJava;

public class JavaGenerateDevelop {

	public final static String defaultpackage = "omega";

	public static void main(String args[]) throws Exception {
		File fi = null;
		if(args.length == 1) {
			fi = new File(args[0]);
		}
		else {
			fi = new File(System.getProperty("user.dir"));
		}
		//System.out.println("Katalog "+fi.getCanonicalPath());
		File conf = new File(fi, "src/test/resources/lob.omega");
		//System.out.println("Konfig "+conf.getCanonicalPath()+", finns "+conf.exists());
		ModelConfiguration config = new ModelConfiguration();
		InputStreamReader isr = new InputStreamReader(new FileInputStream(conf), "UTF-8");
		OmegaConfigParser.parseOmegaConfig(config, isr, "File "+conf.getCanonicalPath());

		TableConfiguration ths = config.getDBTableHandlingSpecificationOnTableName("document");
		String javacode = GenerateJava.getCode(defaultpackage, ths);
		System.out.println("Kod:\n"+javacode);
		
		Class<?> helloClass = InMemoryJavaCompiler.compile(defaultpackage+"."+ths.getObjectName(), javacode);

	}
	
}
