package omega.blob;

import se.modlab.generics.util.FilesAndBytes;

public class ByteArrayManage {

	public static String getFilename(byte[] sum) {
		return new String(FilesAndBytes.getFilenameFromArray(sum));
	}
	
	public static byte[] getContents(byte[] sum) {
		return FilesAndBytes.getFilecontentsFromArray(sum);
	}
	
	public static byte[] getSum(String filename, byte[] contents) {
		return FilesAndBytes.getByteArrayFromStringAndArray(filename, contents);
	}
	
}
