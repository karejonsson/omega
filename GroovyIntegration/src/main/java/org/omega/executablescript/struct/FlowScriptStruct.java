package org.omega.executablescript.struct;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Vector;
import java.util.concurrent.Callable;

import org.omega.groovyscripting.GroovyShellScriptingScope;

public class FlowScriptStruct implements FlowInstructionStruct {
	
	private TokenStruct identifier = null;
	//private boolean add = false;
	private File origin = null;
	private File importingFile = null;

	public FlowScriptStruct(TokenStruct identifier/*, boolean add*/, File origin) {
		this.identifier = identifier;
		//this.add = add;
		this.origin = origin;
	}

	public String verifyReturnErrorDescription(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) {
		//System.out.println("FlowScriptStruct.verifyReturnErrorDescription - Origin "+origin+" for add "+add);
		Vector<ScriptStruct> scripts = cfs.getScripts(identifier.getText());
		if(scripts == null) {
			return "No script named "+identifier.getText();
		}
		if(scripts.size() == 0) {
			return "No script named "+identifier.getText();
		}
		if(scripts.size() > 1) {
			return "Multiple scripts named "+identifier.getText()+". "+scripts.size()+" found. First two are\n"+
					scripts.get(0)+"\n"+
					scripts.get(1);
		}
		return null;
		/*
		if(!add) {
			return null;
		}
		
		ScriptStruct ss = scripts.get(0);
		try {
			mmss.execute("groovy", ss.getOrigin().getCanonicalPath(), ss.getScript().getBeginLine(), ss.getScript().getBeginColumn(), ss.getScript().getText());
		}
		catch(Exception e) {
			String out = e.getMessage();
			if(out == null) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				out = sw.toString();
			}
			return "The script "+ss.getIdentifier()+" cannot be compiled.\n"+out;
		}
		return null;
		*/
	}

	public void execute(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception { 
		//System.out.println("FlowScriptStruct.execute - Origin "+origin+" for add "+add);
		Vector<ScriptStruct> scripts = cfs.getScripts(identifier.getText());
		if(scripts == null) {
			throw new Exception("No script named "+identifier.getText());
		}
		if(scripts.size() == 0) {
			throw new Exception("No script named "+identifier.getText());
		}
		if(scripts.size() > 1) {
			throw new Exception("Multiple scripts named "+identifier.getText()+". "+scripts.size()+" found. First two are\n"+
					scripts.get(0)+"\n"+
					scripts.get(1));
		}
		ScriptStruct ss = scripts.get(0);

		try {
			mmss.execute("groovy", ss.getIdentifier().getText()+".groovy", ss.getScript().getBeginLine(), ss.getScript().getBeginColumn(), ss.getScript().getText());
		}
		catch(Exception e) {
			String out = e.getMessage();
			if(out == null) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				out = sw.toString();
			}
			throw new Exception("The script "+ss.getIdentifier()+" cannot be compiled.\n"+out);
		}
	}
	
	public Object evaluate(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		//System.out.println("FlowScriptStruct.evaluate - Origin "+origin+" for add "+add);
		Vector<ScriptStruct> scripts = cfs.getScripts(identifier.getText());
		if(scripts == null) {
			throw new Exception("No script named "+identifier.getText());
		}
		if(scripts.size() == 0) {
			throw new Exception("No script named "+identifier.getText());
		}
		if(scripts.size() > 1) {
			throw new Exception("Multiple scripts named "+identifier.getText()+". "+scripts.size()+" found. First two are\n"+
					scripts.get(0)+"\n"+
					scripts.get(1));
		}
		ScriptStruct ss = scripts.get(0);

		try {
			//System.out.println("Evaluate "+ss.getScript());
			return mmss.evaluate("groovy", ss.getIdentifier().getText()+".groovy"/*ss.getOrigin().getCanonicalPath()*/, ss.getScript().getBeginLine(), ss.getScript().getBeginColumn(), ss.getScript().getText());
		}
		catch(Exception e) {
			String out = e.getMessage();
			if(out == null) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				out = sw.toString();
			}
			throw new Exception("The script "+ss.getIdentifier()+" cannot be compiled.\n"+out);
		}
	}

	public File getOrigin() {
		return origin;
	}

	public void setImportingFile(File importingFile) {
		this.importingFile = importingFile;
	}

	public Callable getForAfterExecution() {
		return null;
	}

}
