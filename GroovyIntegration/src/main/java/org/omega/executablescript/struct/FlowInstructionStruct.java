package org.omega.executablescript.struct;

import java.io.File;
import java.util.concurrent.Callable;

import org.omega.groovyscripting.GroovyShellScriptingScope;

public interface FlowInstructionStruct {

	public String verifyReturnErrorDescription(CombinedFileStruct cfs, GroovyShellScriptingScope mmss);
	public void execute(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception;
	public Object evaluate(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception;
	public Callable getForAfterExecution();

	public void setImportingFile(File importingFile);
	
}
