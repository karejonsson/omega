package org.omega.executablescript.struct;

import java.io.File;

public class ScriptStruct {
	
	private TokenStruct identifier = null;
	private TokenStruct script = null;
	private File origin = null;
	private ImportStruct importingFile = null;

	public String toString() {
		return "Script "+identifier+" with script at "+script.place()+". "+(origin != null ? " from file "+origin : "")+(importingFile != null ? " imported from "+importingFile : "");
	}

	public ScriptStruct(TokenStruct identifier, TokenStruct script, File origin) {
		this.identifier = identifier;
		this.script = script;
		this.origin = origin;
	}
	
	public TokenStruct getIdentifier() {
		return identifier;
	}
	
	public TokenStruct getScript() {
		return script;
	}
	
	public File getOrigin() {
		return origin;
	}

	public void setImportingStruct(ImportStruct importingFile) {
		this.importingFile = importingFile;
	}

}
