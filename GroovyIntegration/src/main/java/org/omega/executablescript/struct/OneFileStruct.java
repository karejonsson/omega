package org.omega.executablescript.struct;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class OneFileStruct {
	
	private Map<String, Vector<ConnectionStruct>> connections = new HashMap<String, Vector<ConnectionStruct>>();
	private Map<String, Vector<ScriptStruct>> scripts = new HashMap<String, Vector<ScriptStruct>>();
	private Vector<ImportStruct> imports = new Vector<ImportStruct>();
	private Map<String, Vector<FlowStruct>> flows = new HashMap<String, Vector<FlowStruct>>();
	private File origin = null;
	
	private FlowExploreStruct.ExplorerLauncher launcher = null; 
	
	public OneFileStruct(FlowExploreStruct.ExplorerLauncher launcher) {
		this.launcher = launcher;
	}
	
	public FlowExploreStruct.ExplorerLauncher getlauncher() {
		return launcher;
	}
	
	public File getOrigin() {
		return origin;
	}
	
	public void setOrigin(File origin) {
		this.origin = origin;
	}
	
	public String toString() {
		return "File "+origin+(importingStruct != null ? " imported from "+importingStruct : "");
	}
	
	private ImportStruct importingStruct = null;
	
	public void setImportingStruct(ImportStruct importingFile) {
		importingStruct = importingFile;
		for(String key : connections.keySet()) {
			Vector<ConnectionStruct> v = connections.get(key);
			for(ConnectionStruct cs : v) {
				cs.setImportingStruct(importingFile);
			}
		}
		
		for(String key : scripts.keySet()) {
			Vector<ScriptStruct> v = scripts.get(key);
			for(ScriptStruct ss : v) {
				ss.setImportingStruct(importingFile);
			}
		}

		for(ImportStruct is : imports) {
			is.setImportingStruct(importingFile);
		}

		for(String key : flows.keySet()) {
			Vector<FlowStruct> v = flows.get(key);
			for(FlowStruct fs : v) {
				fs.setImportingStruct(importingFile);
			}
		}
	}
	
	public void addConnection(
			String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn,
			String script_text, int script_beginLine, int script_beginColumn, int script_endLine, int script_endColumn, 
			File origin
			) 
	{
		Vector<ConnectionStruct> v = connections.get(identifier_text);
		if(v == null) {
			v = new Vector<ConnectionStruct>();
			connections.put(identifier_text, v);
		}
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		TokenStruct script = new TokenStruct(script_text, script_beginLine, script_beginColumn, script_endLine, script_endColumn);
		ConnectionStruct cs = new ConnectionStruct(identifier, script, origin);
		v.add(cs);
	}

	public Set<String> getConnectionKeys() {
		return connections.keySet();
	}

	public List<ConnectionStruct> getConnections(String key) {
		Vector<ConnectionStruct> out = connections.get(key);
		if(out == null) {
			return null;
		}
		return Collections.unmodifiableList(out);
	}

	public void addScript(
			String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn,
			String script_text, int script_beginLine, int script_beginColumn, int script_endLine, int script_endColumn,
			File origin
			) 
	{
		Vector<ScriptStruct> v = scripts.get(identifier_text);
		if(v == null) {
			v = new Vector<ScriptStruct>();
			scripts.put(identifier_text, v);
		}
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		TokenStruct script = new TokenStruct(script_text, script_beginLine, script_beginColumn, script_endLine, script_endColumn);
		ScriptStruct ss = new ScriptStruct(identifier, script, origin);
		v.add(ss);
	}
	
	public Set<String> getScriptKeys() {
		return scripts.keySet();
	}

	public List<ScriptStruct> getScripts(String key) {
		Vector<ScriptStruct> out = scripts.get(key);
		if(out == null) {
			return null;
		}
		return Collections.unmodifiableList(out);
	}

	public void addImport(
			String filename_text, int filename_beginLine, int filename_beginColumn, int filename_endLine, int filename_endColumn,
			File origin
			) 
	{
		TokenStruct filename = new TokenStruct(filename_text, filename_beginLine, filename_beginColumn, filename_endLine, filename_endColumn);
		imports.add(new ImportStruct(filename, origin));
	}
	
	public List<ImportStruct> getImports() {
		return Collections.unmodifiableList(imports);
	}
	
	public void addFlow(String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn, Flow flow, File origin) 
	{
		Vector<FlowStruct> v = flows.get(identifier_text);
		if(v == null) {
			v = new Vector<FlowStruct>();
			flows.put(identifier_text, v);
		}
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		FlowStruct flowstruct = new FlowStruct(identifier, flow, origin);
		v.add(flowstruct);
	}
	
	public Set<String> getFlowKeys() {
		return flows.keySet();
	}
	
	public List<FlowStruct> getFlows(String key) {
		Vector<FlowStruct> out = flows.get(key);
		if(out == null) {
			return null;
		}
		return Collections.unmodifiableList(out);
	}

}
