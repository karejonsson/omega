package org.omega.executablescript.struct;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class CombinedFileStruct {

	private Map<String, Vector<ConnectionStruct>> connections = new HashMap<String, Vector<ConnectionStruct>>();
	private Map<String, Vector<ScriptStruct>> scripts = new HashMap<String, Vector<ScriptStruct>>();
	private Map<String, Vector<FlowStruct>> flows = new HashMap<String, Vector<FlowStruct>>();
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		Set<String> keys = connections.keySet();
		sb.append("Number of connections "+keys.size()+"\n");
		for(String key : keys) {
			Vector<ConnectionStruct> v = connections.get(key);
			sb.append(key+" has "+v.size()+" instances: ");
			for(ConnectionStruct cs : v) {
				sb.append(cs.toString()+" ");
			}
			sb.append("\n");
		}
		
		keys = scripts.keySet();
		sb.append("Number of scripts "+keys.size()+"\n");
		for(String key : keys) {
			Vector<ScriptStruct> v = scripts.get(key);
			sb.append(key+" has "+v.size()+" instances: ");
			for(ScriptStruct ss : v) {
				sb.append(ss.toString()+" ");
			}
			sb.append("\n");
		}
		
		keys = flows.keySet();
		sb.append("Number of flows "+keys.size()+"\n");
		for(String key : keys) {
			Vector<FlowStruct> v = flows.get(key);
			sb.append(key+" has "+v.size()+" instances: ");
			for(FlowStruct fs : v) {
				sb.append(fs.toString()+" ");
			}
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	public Set<String> getFlowKeys() {
		return flows.keySet();
	}
	
	public Vector<FlowStruct> getFlows(String key) {
		return flows.get(key);
	}

	public Set<String> getScriptKeys() {
		return scripts.keySet();
	}

	public Vector<ScriptStruct> getScripts(String key) {
		return scripts.get(key);
	}

	public Set<String> getConnectionKeys() {
		return connections.keySet();
	}

	public Vector<ConnectionStruct> getConnections(String key) {
		return connections.get(key);
	}

	public void addByMerge(OneFileStruct ofs) throws Exception {
		File f = ofs.getOrigin();
		if(has(f)) {
			throw new Exception("File "+ofs+" merged second time.");
		}
		
		for(String key : ofs.getConnectionKeys()) {
			Vector<ConnectionStruct> v = connections.get(key);
			if(v == null) {
				v = new Vector<ConnectionStruct>();
				connections.put(key, v);
			}
			v.addAll(ofs.getConnections(key));
		}
		
		for(String key : ofs.getScriptKeys()) {
			Vector<ScriptStruct> v = scripts.get(key);
			if(v == null) {
				v = new Vector<ScriptStruct>();
				scripts.put(key, v);
			}
			v.addAll(ofs.getScripts(key));
		}
		
		for(String key : ofs.getFlowKeys()) {
			Vector<FlowStruct> v = flows.get(key);
			if(v == null) {
				v = new Vector<FlowStruct>();
				flows.put(key, v);
			}
			v.addAll(ofs.getFlows(key));
		}
		
		filesMerged.add(f);
	}
	
	private Vector<File> filesMerged = new Vector<File>();
	
	public boolean has(File f) {
		for(File merged : filesMerged) {
			if(same(merged, f)) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean same(File f1, File f2) {
		try {
			return f1.getCanonicalPath().equals(f2.getCanonicalPath());
		}
		catch(Exception e) {
			return null;
		}
	}
	
}
