package org.omega.executablescript.struct;

import java.io.File;

public class ImportStruct {
	
	private TokenStruct filename = null;
	private File origin = null;
	private ImportStruct importingFile = null;
	
	public String toString() {
		return "Import "+filename+" from "+origin;
	}

	public String toStringDeep() {
		return toString()+(importingFile != null ? " previously imported from "+importingFile : "");
	}

	public ImportStruct(TokenStruct filename, File origin) {
		this.filename = filename;
		this.origin = origin;
	}
	
	public TokenStruct getFilename() {
		return filename;
	}
	
	public File getOrigin() {
		return origin;
	}

	public void setImportingStruct(ImportStruct importingFile) {
		this.importingFile = importingFile;
	}

}
