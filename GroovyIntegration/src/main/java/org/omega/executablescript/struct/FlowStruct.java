package org.omega.executablescript.struct;

import java.io.File;

public class FlowStruct {
	
	private TokenStruct identifier = null;
	private Flow flow = null;
	private File origin = null;
	private ImportStruct importingFile = null;

	public String toString() {
		return "Flow "+identifier+(origin != null ? " "+origin : "")+(importingFile != null ? " imported from "+importingFile : "");
	}

	public FlowStruct(TokenStruct identifier, Flow flow, File origin) {
		this.identifier = identifier;
		this.flow = flow;
		this.origin = origin;
	}
	
	public TokenStruct getIdentifier() {
		return identifier;
	}
	
	public Flow getFlow() {
		return flow;
	}
	
	public File getOrigin() {
		return origin;
	}

	public void setImportingStruct(ImportStruct importingFile) {
		this.importingFile = importingFile;
	}

}
