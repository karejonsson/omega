package org.omega.executablescript.struct;

import java.io.File;

public class ConnectionStruct {

	private TokenStruct identifier = null;
	private TokenStruct specification = null;
	private File origin = null;
	private ImportStruct importingStruct = null;
	private File importingFile = null;
	
	public String toString() {
		return "Connection "+identifier+(origin != null ? " "+origin : "")+(importingFile != null ? " imported from "+importingFile : "");
	}

	public ConnectionStruct(TokenStruct identifier, TokenStruct specification, File origin) {
		this.identifier = identifier;
		this.specification = specification;
		this.origin = origin;
	}
	
	public TokenStruct getIdentifier() {
		return identifier;
	}
	
	public TokenStruct getSpecification() {
		return specification;
	}
	
	public File getOrigin() {
		return origin;
	}

	public void setImportingStruct(ImportStruct importingStruct) {
		this.importingStruct = importingStruct;
	}

	public void setImportingFile(File importingFile) {
		this.importingFile = importingFile;
	}

}
