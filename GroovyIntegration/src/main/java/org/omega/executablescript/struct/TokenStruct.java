package org.omega.executablescript.struct;

public class TokenStruct {
	
	private String text; 
	private int bl; 
	private int bc; 
	private int el; 
	private int ec;
	
	public TokenStruct(String text, int bl, int bc, int el, int ec) {
		this.text = text;
		this.bl = bl;
		this.bc = bc;
		this.el = el;
		this.ec = ec;
	}
	
	public String getText() {
		return text;
	}
	
	public String toString() {
		return "\""+text+"\", declared starting "+place();
	}
	
	public String place() {
		return "line "+bl+", column "+bc+", finishing line "+el+", column "+ec;
	}

	public int getBeginLine() {
		return bl;
	}

	public int getBeginColumn() {
		return bc;
	}

}
