package org.omega.executablescript.struct;

import java.io.File;
import java.util.concurrent.Callable;

import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.groovyscripting.GroovyShellScriptingScope;

public class FlowExploreStruct implements FlowInstructionStruct {
	
	private TokenStruct identifier = null;
	private File origin = null;
	private File importingFile = null;
	
	public interface ExplorerLauncher {
		public void launch(EmbeddedModelHandling emh, String packagename, boolean hold);
	}
	
	private ExplorerLauncher launcher = null;

	public FlowExploreStruct(TokenStruct identifier, File origin, ExplorerLauncher launcher) {
		this.identifier = identifier;
		this.origin = origin;
		this.launcher = launcher;
	}

	public String verifyReturnErrorDescription(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) {
		//System.out.println("FlowExploreStruct.verifyReturnErrorDescription "+identifier.getText());
		EmbeddedModelHandling connection = mmss.getConnection(identifier.getText());
		if(connection == null) {
			return 
					"The connection "+identifier.getText()+" was to be used to launch an explorer on "+
					identifier.place()+" but it is not defined.";
		}
		return null;
	}

	public void execute(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		//System.out.println("FlowExploreStruct.execute "+identifier.getText());
		EmbeddedModelHandling emh = mmss.getConnection(identifier.getText());
		if(emh == null) {
			throw new Exception( 
					"The connection "+identifier.getText()+" was to be used to launch an explorer on "+
					identifier.place()+" but it is not defined.");
		}
		launcher.launch(emh, identifier.getText(), true);
	}

	public Object evaluate(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		// Done
		return null;
	}

	public Callable getForAfterExecution() {
		// Done
		return null;
	}

	public void setImportingFile(File importingFile) {
		this.importingFile = importingFile;
	}

}
