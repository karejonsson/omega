package org.omega.executablescript.struct;

import java.io.File;
import java.util.Vector;
import java.util.concurrent.Callable;

import org.omega.executablescript.struct.FlowExploreStruct.ExplorerLauncher;
import org.omega.groovyscripting.GroovyShellScriptingScope;

public class Flow implements FlowInstructionStruct {
	
	private Vector<FlowInstructionStruct> lines = new Vector<FlowInstructionStruct>();
	private File origin = null;
	private File importingFile = null;
	private ExplorerLauncher launcher = null;
	
	public Flow(File origin, ExplorerLauncher launcher) {
		this.origin = origin;
		this.launcher = launcher;
	}

	public void addConnection(
			String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn,
			File origin
		) 
	{
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		FlowConnectionStruct fcs = new FlowConnectionStruct(identifier, origin);
		lines.add(fcs);
	}
	
	public void addScript(
			String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn,
			File origin
		) 
	{
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		FlowScriptStruct fss = new FlowScriptStruct(identifier/*, add*/, origin);
		lines.add(fss);
	}

	public void addExplorer(
			String identifier_text, int identifier_beginLine, int identifier_beginColumn, int identifier_endLine, int identifier_endColumn,
			File origin
		) 
	{
		TokenStruct identifier = new TokenStruct(identifier_text, identifier_beginLine, identifier_beginColumn, identifier_endLine, identifier_endColumn);
		FlowExploreStruct fes = new FlowExploreStruct(identifier, origin, launcher);
		lines.add(fes);
	}

	public String verifyReturnErrorDescription(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) {
		StringBuffer sb = new StringBuffer();
		int ctr = 0;
		for(FlowInstructionStruct line : lines) {
			String errorMessage = line.verifyReturnErrorDescription(cfs, mmss);
			if(errorMessage != null) {
				sb.append(errorMessage);
				ctr++;
			}
		}
		if(ctr == 0) {
			return null;
		}
		return sb.toString();
	}

	public void execute(final CombinedFileStruct cfs, final GroovyShellScriptingScope mmss) throws Exception {
		Vector<Callable> forafter = new Vector<Callable>();
		for(FlowInstructionStruct line : lines) {
			line.execute(cfs, mmss);
			Callable r = line.getForAfterExecution();
			if(r != null) {
				forafter.add(r);
			}
		}
		StringBuffer sb = new StringBuffer();
		boolean errorHappened = false;
		for(Callable r : forafter) {
			try {
				r.call();
			}
			catch(Exception e) {
				errorHappened = true;
				sb.append(e.getMessage()+"\n");
			}
		}
		if(errorHappened) {
			throw new Exception(sb.toString());
		}
	}

	public Object evaluate(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		Vector<FlowInstructionStruct> program = new Vector<FlowInstructionStruct>();
		for(FlowInstructionStruct line : lines) {
			program.add(line);
		}
		Vector<Callable> forafter = new Vector<Callable>();
		for(int i = 0 ; i < program.size()-1 ; i++) {
			//System.out.println("Flow.Execute "+i+" innan");
			FlowInstructionStruct line = program.get(i);
			line.execute(cfs, mmss);
			Callable r = line.getForAfterExecution();
			if(r != null) {
				forafter.add(r);
			}
			//System.out.println("Flow.Execute "+i+" efter");
		}
		//System.out.println("Flow.Evaluate innan");
		FlowInstructionStruct line = program.get(program.size()-1);
		Object out = line.evaluate(cfs, mmss);
		Callable r = line.getForAfterExecution();
		if(r != null) {
			forafter.add(r);
		}
		StringBuffer sb = new StringBuffer();
		boolean errorHappened = false;
		for(Callable c : forafter) {
			try {
				c.call();
			}
			catch(Exception e) {
				errorHappened = true;
				sb.append(e.getMessage()+"\n");
			}
		}
		if(errorHappened) {
			throw new Exception(sb.toString());
		}
		return out;
	}

	public void setImportingFile(File importingFile) {
		this.importingFile = importingFile;
	}

	public Callable getForAfterExecution() {
		return null;
	}

}
