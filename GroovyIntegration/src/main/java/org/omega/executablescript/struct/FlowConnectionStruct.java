package org.omega.executablescript.struct;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Vector;
import java.util.concurrent.Callable;

import org.omega.connectionspecification.parse.OmegaConnectionSpecificationParser;
import org.omega.connectionspecification.struct.DBConnectionSpecification;
import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.manager.GenesisVisitor;
import org.omega.groovyscripting.GroovyShellScriptingScope;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;

public class FlowConnectionStruct implements FlowInstructionStruct {
	
	private TokenStruct identifier = null;
	//private boolean create = false;
	private File origin = null;
	private File importingFile = null;

	public FlowConnectionStruct(TokenStruct identifier/*, boolean create*/, File origin) {
		this.identifier = identifier;
		//this.create = create;
		this.origin = origin;
	}

	public String verifyReturnErrorDescription(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) {
		Vector<ConnectionStruct> connections = cfs.getConnections(identifier.getText());
		if(connections == null) {
			return "No connection named "+identifier.getText();
		}
		if(connections.size() == 0) {
			return "No connection named "+identifier.getText();
		}
		if(connections.size() > 1) {
			return "Multiple connections named "+identifier.getText()+". "+connections.size()+" found. First two are\n"+
					connections.get(0)+"\n"+
					connections.get(1);
		}
		ConnectionStruct cs = connections.get(0);
		String specification = cs.getSpecification().getText();
		InputStream stream = new ByteArrayInputStream(specification.getBytes(StandardCharsets.UTF_8));
		InputStreamReader isr = new InputStreamReader(stream);
		JDBCConnectionSpecification dbcs = null;
		try {
			dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		}
		catch(Exception e) {
			return cs.getSpecification().toString()+" cannot be parsed.";
		}

		EmbeddedModelHandling connection = getDBConnection(dbcs);
		
		if(connection == null) {
			return "Unable to create the connection for "+identifier.getText();
		}
		
		mmss.addConnection(identifier.getText(), connection);
		
		try {
			connection.close();
		} catch (Exception e) {
			return "Unable to close connection "+identifier.getText();
		}
		return null;
	}
	
	private EmbeddedModelHandling getDBConnection(JDBCConnectionSpecification dbcs) {
		EmbeddedModelHandling emh = null;;
		try {
			emh = dbcs.getDetailsImplementation();
		} 
		catch (IntolerableException e) {
			e.printStackTrace();
		}
		return emh;
	}
	
	public void execute(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		//System.out.println("FlowConnectionStruct.execute: create "+create);
		Vector<ConnectionStruct> connections = cfs.getConnections(identifier.getText());
		if(connections == null) {
			throw new Exception("No connection named "+identifier.getText());
		}
		//System.out.println("FlowConnectionStruct.execute: 1");
		if(connections.size() == 0) {
			throw new Exception("No connection named "+identifier.getText());
		}
		//System.out.println("FlowConnectionStruct.execute: 2");
		if(connections.size() > 1) {
			throw new Exception("Multiple connections named "+identifier.getText()+". "+connections.size()+" found. First two are\n"+
					connections.get(0)+"\n"+
					connections.get(1));
		}
		//System.out.println("FlowConnectionStruct.execute: 3");
		ConnectionStruct cs = connections.get(0);
		String specification = cs.getSpecification().getText();
		InputStream stream = new ByteArrayInputStream(specification.getBytes(StandardCharsets.UTF_8));
		InputStreamReader isr = new InputStreamReader(stream);
		JDBCConnectionSpecification dbcs = null;
		//System.out.println("FlowConnectionStruct.execute: 4");
		try {
			dbcs = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
		}
		catch(Exception e) {
			throw new Exception(cs.getSpecification().toString()+" cannot be parsed.");
		}
		
		HierarchyObject.setReferenceFile(origin.getParentFile());
		//System.out.println("FlowConnectionStruct.execute: 5");

		final EmbeddedModelHandling connection = getDBConnection(dbcs);
		//System.out.println("FlowConnectionStruct.execute: 6");
		
		if(connection == null) {
			throw new Exception("Unable to create the connection for "+identifier.getText());
		}

		mmss.addConnection(identifier.getText(), connection);

		//System.out.println("FlowConnectionStruct.execute: 10");
		
		forafter = new Callable() {
			public Object call() throws Exception {
				try {
					//System.out.println("FlowConnectionStruct.execute connection closes. Name "+identifier.getText());
					connection.close();
				} catch (Exception e) {
					throw new Exception("Unable to close connection "+identifier.getText());
				}
				return null;
			}
		};
		//System.out.println("FlowConnectionStruct.execute: 11");
		
		mmss.addModel(connection, identifier.getText());
	}
	
	public Object evaluate(CombinedFileStruct cfs, GroovyShellScriptingScope mmss) throws Exception {
		execute(cfs, mmss);
		//ConnectionStruct flow = cfs.getConnections(identifier.getText()).get(0);
		return null;
	}
	// GroovyShellScriptingScope
	
	public File getOrigin() {
		return origin;
	}

	public void setImportingFile(File importingFile) {
		this.importingFile = importingFile;
	}

	private Callable forafter = null;
	
	public Callable getForAfterExecution() {
		return forafter;
	}

}
