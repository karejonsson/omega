package org.omega.executablescript.struct;

import java.io.File;
import java.util.List;
import java.util.Stack;

import org.omega.executablescript.parse.OmegaExecutableScriptParser;

public class ParseRecursively {
	
	public static CombinedFileStruct parse(File file, FlowExploreStruct.ExplorerLauncher launcher) throws Exception {
		CombinedFileStruct out = new CombinedFileStruct();
		OneFileStruct ofs = null;
		//System.out.println("File "+file);
		try {
			ofs = OmegaExecutableScriptParser.parseExecutableScript(file, launcher);
		}
		catch(Exception e) {
			System.out.println("File "+file);
			throw new Exception("Unable to parse initial file", e);
		}
		Stack<OneFileStruct> stack = new Stack<OneFileStruct>();
		stack.add(ofs);
		
		while(!stack.isEmpty()) {
			ofs = stack.pop();
			File origin = ofs.getOrigin();
			if(out.has(origin)) {
				continue;
			}
			//System.out.println("File "+origin+", stack currently "+stack.size());
			out.addByMerge(ofs);
			List<ImportStruct> imports = ofs.getImports();
			for(ImportStruct is : imports) {
				File current = new File(origin.getParentFile(), is.getFilename().getText());
				if(!current.exists()) {
					throw new Exception("File "+is+" does not exist");
				}
				if(out.has(current)) {
					//System.out.println("File "+current+" already imported");
					continue;
				}
				OneFileStruct subjacent = null;
				try {
					subjacent = OmegaExecutableScriptParser.parseExecutableScript(current, launcher);
				}
				catch(Exception e) {
					throw new Exception("File "+is+" has parse error", e);
				}
				subjacent.setImportingStruct(is);
				stack.push(subjacent);
			}
		}
		return out;
	}

}
