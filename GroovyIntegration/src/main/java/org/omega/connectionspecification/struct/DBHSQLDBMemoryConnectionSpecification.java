package org.omega.connectionspecification.struct;

import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.recreatesql.HSQLDBRecreator;

import se.modlab.generics.exceptions.IntolerableException;

public class DBHSQLDBMemoryConnectionSpecification extends JDBCConnectionSpecification {

	private EmbeddedModelHandling details = null;

	public DBHSQLDBMemoryConnectionSpecification() {
	}
	
	public EmbeddedModelHandling getDetailsImplementation() throws IntolerableException {
		if(details != null) {
			return details;
		}
		DBConnectionSpecificationNamecastVisitor visitor = getVisitor("HSQLDB Memory");
		accept(visitor);
		details = (EmbeddedModelHandling) visitor.getConnection();
		return details;
	}
	
	public void accept(DBConnectionSpecificationVisitorTypecast visitor) {
		visitor.visit(this);
	} 
	
	public void accept(DBConnectionSpecificationNamecastVisitor visitor) throws IntolerableException {
		visitor.visitDBHSQLDBMemory();
	} 
	
	public String getSymbol() {
		return HSQLDBRecreator.symbol;
	}


}
