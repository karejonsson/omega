package org.omega.connectionspecification.struct;

import java.sql.SQLException;

import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.manager.GenesisVisitor;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;

public abstract class JDBCConnectionSpecification extends DBConnectionSpecification  {
	
	private String dbname = null;
	private String password = null;
	private String username = null;
                                                           // se/modlab/gdbappinternalconnectivity/manager/GenesisVisitor
	//public final static String genesisVisitorClassnameHSQL = "se.modlab.gdbapphsqlconnectivity.manager.GenesisVisitor";
	//public final static GenesisVisitor genesisVisitorClassnameInternal = "se.modlab.gdbappinternalconnectivity.manager.GenesisVisitor";

	//private String classToLoad = null;
	
	public JDBCConnectionSpecification(/*String classToLoad*/) {
		//this.classToLoad = classToLoad;
	}
	
	public String toString() {
		return "Config "+getConfigfile()+", dbname "+dbname+".\nDetails "+details;
	}
	
	public void close() throws IntolerableException, SQLException {
		if(details != null) {
			details.close();
		}
	}
	
	private TunnelForwardLParameters tflp = null;
	public void setTunnelForwardLParameters(TunnelForwardLParameters tflp) {
		this.tflp = tflp;
	}
	
	public TunnelForwardLParameters getTunnelForwardLParameters() {
		return tflp;
	}
	
	protected DBConnectionSpecificationNamecastVisitor getVisitor(String purpose) throws IntolerableException {
		DBConnectionSpecificationNamecastVisitor visitor = new GenesisVisitor();
		/*
		try {
			visitor = getVisitor(classToLoad, purpose);
		}
		catch(Exception e) {
			e.printStackTrace();
			String message = "Unable to create connection for "+purpose+" because of error.";
			UserRuntimeError out = new UserRuntimeError(message, e);
			throw out;
		}
		if(visitor == null) {
			String message = "Unable to create connection for "+purpose+". Attempt gave no connection.";
			UserRuntimeError out = new UserRuntimeError(message);
			throw out;
		}
		*/
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.DBNAME, getDbname());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.USERNAME, getUsername());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.PASSWORD, getPassword());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.ENCODING, getEncoding());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FRAMETITLE, getFrametitle());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.CONFIGFILE, getConfigfile());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS, getTunnelForwardLParameters());
		return visitor;
	}
	
	//public abstract GDBAPPEmbeddedDetails getDetailsImplementation() throws IntolerableException ;
	
	//public abstract ExplorerWindow getExplorer() throws intolerableException ;
	
	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		//System.out.println("JDBCCnnectionSpecification.setDBName ("+dbname+")");
		this.dbname = dbname;
	}

	public String getPassword() {
		return password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setPassword(String _password) {
		password = _password;;
	}
	
	public void setUsername(String _username) {
		username = _username;
	}
	
}
