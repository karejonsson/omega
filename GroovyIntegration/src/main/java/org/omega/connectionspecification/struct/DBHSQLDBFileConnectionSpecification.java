package org.omega.connectionspecification.struct;

import java.io.File;

import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.recreatesql.HSQLDBRecreator;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserCompiletimeError;

public class DBHSQLDBFileConnectionSpecification  extends JDBCConnectionSpecification {
	
	private String file = null;
	private EmbeddedModelHandling details = null;
	private boolean create = false;
	
	public DBHSQLDBFileConnectionSpecification() {
	}
	
	public EmbeddedModelHandling getDetailsImplementation() throws IntolerableException {
		if(details != null) {
			return details;
		}
		DBConnectionSpecificationNamecastVisitor visitor = getVisitor("HSQLDB File");
		if(create) { 
			visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FILE, getFileCorrectedWouldBe());
		}
		else {
			visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FILE, getFileCorrected());			
		}
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.CREATE, create);
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.DBNAME, "hsqldbname");
		accept(visitor);
		details = (EmbeddedModelHandling) visitor.getConnection();
		return details;
	}
	
	public String getFile() {
		return file;
	}

	private String getFileCorrected() throws IntolerableException {
		File f = new File(file);
		if(f.exists()) {
			return file;
		}
		String folderPath = HierarchyObject.getReferenceFilePath();
		File folder = new File(folderPath);
		if(!folder.exists()) {
			return null;
		}
		f = new File(folder, file+".script");
		if(f.exists()) {
			f = new File(folder, file);
			return f.getAbsolutePath();
		}
		throw new UserCompiletimeError("Cannot construct full filename from filename "+file+" and mother catalog "+folderPath);
	}

	private String getFileCorrectedWouldBe() throws IntolerableException {
		try {
			String out = getFileCorrected();
			if(out != null) {
				return out;
			}
		}
		catch(Exception e) {
		}
		File f = new File(file);
		if(f.exists()) {
			return f.getAbsolutePath();
		}
		String folderPath = HierarchyObject.getReferenceFilePath();
		File folder = new File(folderPath);

		f = new File(folder, file);
		return f.getAbsolutePath();
	}

	public void setFile(String _file) {
		file = _file;
	}

	/*
	public ExplorerWindow getExplorer() throws intolerableException {
		GDBAPPEmbeddedDetails details = getDetails();
		ExplorerWindow ew = new ExplorerWindow(getDetailsImplementation(), details.getHSQLDBExtraMenu());
		return ew;
	}
	*/
	
	public void accept(DBConnectionSpecificationVisitorTypecast visitor) {
		visitor.visit(this);
	} 
	
	public void accept(DBConnectionSpecificationNamecastVisitor visitor) throws IntolerableException {
		visitor.visitDBHSQLDBFile();
	} 
	
	public void setCreate(boolean _create) {
		create = _create;
	}
	
	public String getFrametitle() {
		String _frametitle = super.getFrametitle();
		if(_frametitle == null || _frametitle.trim().length() == 0) {
			return "GDBAPP Explorer HSQLDB File";
		}
		return _frametitle;
	}
	
	public String getSymbol() {
		return HSQLDBRecreator.symbol;
	}

}
