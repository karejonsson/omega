package org.omega.connectionspecification.struct;

import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.recreatesql.MYSQLRecreator;

import se.modlab.generics.exceptions.IntolerableException;

public final class DBMySQLConnectionSpecification extends JDBCConnectionSpecification {
	
	private String ipresolvable = null;
	private String port = null;
	private EmbeddedModelHandling details = null;
	private String frametitle = null;
	
	public DBMySQLConnectionSpecification() {
	}
		

	public EmbeddedModelHandling getDetailsImplementation() throws IntolerableException {
		if(details != null) {
			return details;
		}
		DBConnectionSpecificationNamecastVisitor visitor = getVisitor("MySQL");
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE, ipresolvable);
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.PORT, port);
		//visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FRAMETITLE, getFrametitle());
		accept(visitor);
		details = (EmbeddedModelHandling) visitor.getConnection();
		return details;
	}
	
	public String getPort() {
		return port;
	}

	public String getIpresolvable() {
		return ipresolvable;
	}
	
	public void setPort(String _port) {
		port = _port;
	}

	public void setIpresolvable(String _ipresolvable) {
		ipresolvable = _ipresolvable;
	}

	public void accept(DBConnectionSpecificationVisitorTypecast visitor) {
		visitor.visit(this);
	} 
	
	public void accept(DBConnectionSpecificationNamecastVisitor visitor) throws IntolerableException {
		visitor.visitDBMySQL();
	} 

	public void setFrametitle(String _frametitle) {
		frametitle = _frametitle;
	}
	
	public String getFrametitle() {
		if(frametitle == null || frametitle.trim().length() == 0) {
			return "GDBAPP Explorer MySQL";
		}
		return frametitle;
	}
	
	public String getSymbol() {
		return MYSQLRecreator.symbol;
	}
	
}
