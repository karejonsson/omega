package org.omega.connectionspecification.struct;

public interface DBConnectionSpecificationVisitorTypecast {

	public void visit(DBHSQLDBFileConnectionSpecification hsqldbf);
	public void visit(DBHSQLDBMemoryConnectionSpecification hsqldbm);
	public void visit(DBHSQLDBRemoteConnectionSpecification hsqldbr);
	public void visit(DBMSSQLSERVERConnectionSpecification mssqlserver);
	public void visit(DBMySQLConnectionSpecification mysql);
	public void visit(DBOracleConnectionSpecification mysql);
	public void visit(DBIBMDB2ConnectionSpecification mysql);
	public void visit(DBPOSTGRESConnectionSpecification postgres);
	//public void visit(RestfulConnectionSpecification restful);
	
}
