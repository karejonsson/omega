package org.omega.connectionspecification.struct;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Vector;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitorAcceptor;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.scope.ScriptingScope;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;

public abstract class DBConnectionSpecification implements DBConnectionSpecificationNamecastVisitorAcceptor {

	private String configfile = null;
	private String encoding = null;
	protected EmbeddedModelHandling details = null;
	private DBPool pool = null;
	protected ScriptingScope scope = null;
	private String frametitle;
	private String iconfile;
	private String name = null;
	
	public DBConnectionSpecification() {
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	/*
	protected DBConnectionSpecificationNamecastVisitor getVisitor(String fullyQualifyingClassname, String purpuseMessage) throws IntolerableException {
		Class clazz = null;
		try {
			clazz = Class.forName(fullyQualifyingClassname);
		}
		catch(ClassNotFoundException cnfe) {
			String message = "Unable to create connection for JDBC. The interface is not supplied.";
			UserRuntimeError out = new UserRuntimeError(message, cnfe);
			throw out;
		}
		
		Object obj = null;
		try {
			obj = clazz.newInstance();
		}
		catch(IllegalAccessException iae) {
			String message = "Unable to create connection for JDBC. The interface cannot be accessed.";
			UserRuntimeError out = new UserRuntimeError(message, iae);
			throw out;
		}
		catch(InstantiationException ie) {
			String message = "Unable to create connection for JDBC. The interface cannot be instanciated.";
			UserRuntimeError out = new UserRuntimeError(message, ie);
			throw out;
		}
		if(!(obj instanceof DBConnectionSpecificationNamecastVisitor)) {
			String message = "Unable to create connection for JDBC. The interface has implementation issues.";
			UserRuntimeError out = new UserRuntimeError(message);
			throw out;
		}
		DBConnectionSpecificationNamecastVisitor visitor =  (DBConnectionSpecificationNamecastVisitor) obj;

		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.ENCODING, getEncoding());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FRAMETITLE, getFrametitle());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.CONFIGFILE, getConfigfileCorrected());
		return visitor;
	}
	*/
	
	public DBPool getPool() {
		return pool;
	}
	
	public void setFrametitle(String frametitle) {
		this.frametitle = frametitle;
	}
	
	public String getFrametitle() {
		String usedEncoding = encoding;
		if(usedEncoding == null || usedEncoding.trim().length() < 3) {
			return frametitle;
		}
		try {
			StringBufferInputStream sbis = new StringBufferInputStream(frametitle);
			InputStreamReader isr = new InputStreamReader(sbis, usedEncoding);
			char buf[] = new char[100];
			int len = isr.read(buf);
			return new String(buf, 0, len);
		}
		catch(UnsupportedEncodingException use) {
		}
		catch(IOException ioe) {
		}
		return frametitle;
	}

	public void setIconfile(String _iconfile) {
		//System.out.println("DBConnectionSpecification.setIconfile("+_iconfile+")");
		if(details == null) {
			iconfile = _iconfile;
			//System.out.println("DBConnectionSpecification.setIconfile("+_iconfile+") - 1");
			return;
		}
		ModelConfiguration hs = details.getModelConfiguration();
		if(hs == null) {
			iconfile = _iconfile;
			//System.out.println("DBConnectionSpecification.setIconfile("+_iconfile+") - 2");
			return;
		}
		//System.out.println("DBConnectionSpecification.setIconfile("+_iconfile+") - 3");
		iconfile = _iconfile;
		hs.setIconfile(_iconfile);
		//details.getDBHandlingSpecification().setIconfile(iconfile);
	}
	
	public String getIconfile() {
		//System.out.println("DBConnectionSpecification.getIconfile()");
		if(details == null) {
			//System.out.println("DBConnectionSpecification.getIconfile() - 1");
			return iconfile;
		}
		ModelConfiguration hs = details.getModelConfiguration();
		if(hs == null) {
			//System.out.println("DBConnectionSpecification.getIconfile() - 2");
			return iconfile;
		}
		//System.out.println("DBConnectionSpecification.getIconfile() - 3");
		return hs.getIconfile();
	}
		
	/*
	public final GDBAPPEmbeddedDetails initializeForGDBAPP() throws IntolerableException {
		if(details == null) {
			details = getDetailsImplementation();
			DBHandlingSpecification hs = details.getDBHandlingSpecification();
			hs.setIconfile(iconfile);
		}
		if(pool == null) {
			pool = new DBPool(details);
		}
		scope = ScopeBuilder.getDBScopeNoOptimization("", pool);
		return details;
	}
	
	public final void initializeForProgramming() throws IntolerableException {
		if(details == null) {
			details = getDetailsImplementation();
		}
		if(pool == null) {
			pool = new DBPool(details);
		}
		scope = ScopeBuilder.getDBScopeForConnection("", pool);
		// Rätt? Faktoriserar när jag ser att detta kanske saknas 20140114
		// initializeCrossDatabaseIntegrations();
	}
	
	private GDBAPPScopeFactory scopeFactory = new GDBAPPScopeFactory();
	public GDBAPPScopeFactory getScopeFactory() {
		return scopeFactory;
	}
	*/
	
	public EmbeddedModelHandling getDetails() throws UserRuntimeError {
		if(details == null) {
			throw new UserRuntimeError("Connection not initialized. ("+toString()+")");
		}
		return details;
	}
	
	public String toString() {
		return "Config "+configfile+", restful.\nDetails "+details;
	}
	
	public void close() throws IntolerableException, SQLException {
		if(details != null) {
			details.close();
		}
	}
	
	public abstract EmbeddedModelHandling getDetailsImplementation() throws IntolerableException ;
	
	public String getConfigfile() {
		return configfile;
	}

	public String getConfigfileCorrected() throws IntolerableException {
		File f = new File(configfile);
		if(f.exists()) {
			return configfile;
		}
		String folderPath = HierarchyObject.getReferenceFilePath();
		File folder = new File(folderPath);
		if(!folder.exists()) {
			return null;
		}
		f = new File(folder, configfile);
		if(f.exists()) {
			return f.getAbsolutePath();
		}
		throw new UserRuntimeError("Cannot construct full config filename from filename "+configfile+" and mother catalog "+folderPath);
	}

	public void setConfigfile(String configfile) {
		this.configfile = configfile;
	}
	
	public String getEncoding() {
		if(encoding != null) {
			return encoding;
		}
		return System.getProperty("file.encoding");
	}
	
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
	public abstract String getSymbol();
	
}
