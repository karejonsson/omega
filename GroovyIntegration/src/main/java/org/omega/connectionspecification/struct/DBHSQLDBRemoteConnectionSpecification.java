package org.omega.connectionspecification.struct;

import org.omega.connectionspecification.struct.JDBCConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.recreatesql.HSQLDBRecreator;
import org.omega.connectivity.recreatesql.MYSQLRecreator;

import se.modlab.generics.exceptions.IntolerableException;

public class DBHSQLDBRemoteConnectionSpecification extends JDBCConnectionSpecification {

	private String ipresolvable = null;
	private String port = null;
	private String protocol = null;
	private EmbeddedModelHandling details = null;
	private String frametitle = null;

	public DBHSQLDBRemoteConnectionSpecification() {
	}
		
	public EmbeddedModelHandling getDetailsImplementation() throws IntolerableException {
		if(details != null) {
			return details;
		}
		DBConnectionSpecificationNamecastVisitor visitor = getVisitor("HSQLDB Remote");
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE, ipresolvable);
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.PORT, port);
		//visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.FRAMETITLE, getFrametitle());
		visitor.setAttribute(DBConnectionSpecificationNamecastVisitor.PROTOCOL, protocol);
		accept(visitor);
		details = (EmbeddedModelHandling) visitor.getConnection();
		return details;
	}
	
	public String getPort() {
		return port;
	}

	public String getIpresolvable() {
		return ipresolvable;
	}

	public String getProtocol() {
		return protocol;
	}
	
	public void setPort(String _port) {
		port = _port;
	}

	public void setIpresolvable(String _ipresolvable) {
		ipresolvable = _ipresolvable;
	}

	public void setProtocol(String _protocol) {
		protocol = _protocol;
	}
	
	public void accept(DBConnectionSpecificationVisitorTypecast visitor) {
		visitor.visit(this);
	} 
	
	public void accept(DBConnectionSpecificationNamecastVisitor visitor) throws IntolerableException {
		visitor.visitDBHSQLDBRemote();
	} 

	public void setFrametitle(String _frametitle) {
		frametitle = _frametitle;
	}
	
	public String getFrametitle() {
		if(frametitle == null || frametitle.trim().length() == 0) {
			return "GDBAPP Explorer HSQLDB Remote";
		}
		return frametitle;
	}
	
	public String getSymbol() {
		return HSQLDBRecreator.symbol;
	}

}
