package org.omega.groovyscripting;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.apache.bsf.util.CodeBuffer;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.memorycompiler.DynamicClassLoader;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

public class GroovyShellScriptingScope {

	private DynamicClassLoader reusedClassLoader = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
	private Vector<OneModel> models = new Vector<OneModel>(); 
	
	private GroovyShell shell = null;
	private Binding bind = null;

	private SystemProgrammingResourceAccess sysobject = null;

	public GroovyShellScriptingScope(SystemProgrammingResourceAccess sysobject) throws BSFException {
		this.sysobject = sysobject;
		reusedClassLoader = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
		bind = new Binding();
		bind.setProperty("sys", sysobject);
		shell = new GroovyShell(reusedClassLoader, bind);
	}
	
	public void addModel(EmbeddedModelHandling details, String packagename) throws Exception {
		models.add(new OneModel(details, packagename, reusedClassLoader));
	}
	
	public void execute(String lang, String source, int lineNo, int columnNo, String script) throws BSFException {
		try {
			shell.run(script, source, new String[0]);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw e;
		}
	}
	
	public Object evaluate(String lang, String source, int lineNo, int columnNo, String script) {
		//System.out.println("GroovyShellScriptingScope.evaluate("+lang+", "+source+", "+script+")");
		try {
			return shell.evaluate(script, source, "");
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw e;
		}
	}
	
	private Map<String, EmbeddedModelHandling> connections = new HashMap<String, EmbeddedModelHandling>();
	
	public boolean addConnection(String symbol, EmbeddedModelHandling connection) {
		if(connections.get(symbol) != null) {
			return false;
		}
		connections.put(symbol, connection);
		return true;
	}

	public EmbeddedModelHandling getConnection(String symbol) {
		return connections.get(symbol);
	}
	
}
