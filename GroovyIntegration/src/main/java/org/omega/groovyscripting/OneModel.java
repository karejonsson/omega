package org.omega.groovyscripting;

import java.lang.reflect.Method;
import java.util.Hashtable;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.scope.GenerateJava;
import org.omega.memorycompiler.DynamicClassLoader;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

public class OneModel {

	private DynamicClassLoader cl = null;

	private Hashtable<String, Class> lookup = new Hashtable<String, Class>();
	private DBPool pool = null;
	private Class<?>[] classes = null;
	private EmbeddedModelHandling details = null;

	public OneModel(EmbeddedModelHandling details, String packagename) throws Exception {
		this(details, packagename, new DynamicClassLoader(ClassLoader.getSystemClassLoader()));
	}
	
	public OneModel(EmbeddedModelHandling details, String packagename, DynamicClassLoader cl) throws Exception {
		this.cl = cl;
		this.details = details;
		ModelConfiguration config = details.getModelConfiguration();

		int noTables = config.getNoTables();
		SourceCode compile[] = new SourceCode[noTables];
		int ctr = 0;
		for(TableConfiguration ths : config.getTablehandlersLoopable()) {
			String javacode = GenerateJava.getCode(packagename, ths);
			compile[ctr++] = new SourceCode(packagename+"."+ths.getObjectName(), javacode);
		}

		pool = new DBPool(details);
		classes = InMemoryJavaCompiler.compile(cl, compile);
		
		for(Class clazz : classes) {
			String fullyQualifyingClassname = clazz.getName();
			int idx = Math.max(0, fullyQualifyingClassname.lastIndexOf('.'))+1;
			String objectName = fullyQualifyingClassname.substring(idx);
			lookup.put(objectName, clazz);
			TableConfiguration ths = config.getDBTableHandlingSpecificationOnObjectName(objectName);

			Method setDefaults = clazz.getDeclaredMethod("setDefaults", EmbeddedTableHandling.class, DBPool.class);
			setDefaults.invoke(null, new EmbeddedTableHandling(details, ths), pool);
		}
		
	}

	public DBPool getPool() {
		return pool;
	}

	public Class<?>[] getClasses() {
		return classes;
	}

	public Class lookupClass(String objectName) {
		return lookup.get(objectName);
	}
	
}
