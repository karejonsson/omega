package org.omega.groovyintegration.scope;

import java.io.InputStream;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.VarcharType;

public class GenerateJava {
	
	public static String beansCamelName(String name) {
		return name.substring(0,1).toUpperCase()+name.substring(1).toLowerCase();
	}
	
	public static String getType(Column col) {
		Type type = col.getType();
		if(type instanceof DatetimeType) {
			return "Date";
		}
		if(type instanceof DecimalType) {
			return "Decimal";
		}
		if(type instanceof DoubleType) {
			return "Double";
		}
		if(type instanceof IntType) {
			return "Long";
		}
		if(type instanceof TextualType) {
			return "String";
		}
		if(type instanceof LobType) {
			return "byte[]";
		}
		return "Object";
	}
	
	public static Integer getStringsLength(Column col) {
		Type type = col.getType();
		if(type instanceof VarcharType) {
			return ((VarcharType) type).getLength();
		}
		return null;
	}
	
	/*
		Hashtable<String, Object> ht = new Hashtable<String, Object>();
		ht.put(name.toUpperCase(), obj);
		List<DBPoolObject> found = pool.searchPoolObjects(defaultEnv.ths, ht);
		List<Country> out = new ArrayList<Country>();
		for(int i = 0 ; i < found.size() ; i++) {
		   out.add(new Country(pool, found.get(i)));
	    }
        return out;	 */
	public static void addSearchOnTextalField(StringBuffer sb, Column col, String objectname) {
		String name = beansCamelName(col.getName());
		sb.append("  \n");
		sb.append("  public static List<"+objectname+"> findBy"+name+"(String search) throws Exception {\n");
		sb.append("    Hashtable<String, Object> ht = new Hashtable<String, Object>();\n");
		sb.append("    ht.put(\""+name.toUpperCase()+"\", search);\n");
		sb.append("    List<DBPoolObject> found = defaultPool.searchPoolObjects(defaultEnv.ths, ht);\n");
		sb.append("    List<"+objectname+"> out = new ArrayList<"+objectname+">();\n");
		sb.append("    for(int i = 0 ; i < found.size() ; i++) {\n");
		sb.append("      out.add(new "+objectname+"(defaultPool, found.get(i), true));\n");
		sb.append("    }\n");
		sb.append("    return out;\n");
		sb.append("  }\n");
		sb.append("\n");
	}

	public static void addBlobGetter(StringBuffer sb, Column col, String objectname) {
		String name = col.getName();
		sb.append("  public byte[] get"+beansCamelName(name)+"() throws Exception {\n");
		sb.append("    return po.downloadByteArrayFromBlob_with_executeQuery(\""+name+"\");\n");
		sb.append("  }\n");
	}

	public static void addGetter(StringBuffer sb, Column col, String objectname) {
		Type type = col.getType();
		if(type instanceof LobType) {
			addBlobGetter(sb, col, objectname);
			return;
		}
		
		String typing = getType(col);
		//System.out.println("GenerateJava.addGetter "+objectname+", typing = "+typing);
		String name = col.getName();
		sb.append("  public "+typing+" get"+beansCamelName(name)+"() {\n");
		sb.append("    Object out = po.get(\""+name+"\");\n");
		if(type instanceof IntType) {
			sb.append("    if(out instanceof Integer) {;\n");
			sb.append("      out = ((Integer) out).longValue();\n");
			sb.append("    };\n");
		}
		sb.append("    return ("+typing+") out;\n");
		sb.append("  }\n");
		if(col.getType() instanceof TextualType) {
			addSearchOnTextalField(sb, col, objectname);
		}
	}
	
	public static void addBlobSetter(StringBuffer sb, Column col, String objectname) {
		String name = col.getName();
		String nameCamelized = beansCamelName(name);
		sb.append("  public void set"+nameCamelized+"(byte[] value) throws Exception {\n");
		sb.append("    po.uploadToBlob(\""+name+"\", value);\n");
		sb.append("  }\n");
		sb.append("  public void set"+nameCamelized+"(String filename, byte[] contents) throws Exception {\n");
		sb.append("    set"+nameCamelized+"(omega.blob.ByteArrayManage.getSum(filename, contents));\n");
		sb.append("  }\n");
	}

	public static void addSetter(StringBuffer sb, Column col, String objectname) {
		Type type = col.getType();
		if(type instanceof LobType) {
			addBlobSetter(sb, col, objectname);
			return;
		}
		String typing = getType(col);
		Integer len = getStringsLength(col);
		String name = col.getName();
		String lengthmaxname = null;
		if(len != null) {
			lengthmaxname = name.toLowerCase()+"_maxlen";
			sb.append("  public static final int "+lengthmaxname+" = "+len+";\n");
		}
		boolean notNull = col.getType().getNotNull();
		sb.append("  public void set"+beansCamelName(name)+"("+typing+" value) throws Exception {\n");
		sb.append("    if(deleted) {\n");
		sb.append("      throw new Exception(\"Setting field \\\""+name+"\\\" on deleted "+objectname+" object\");\n");
		sb.append("    }\n");
		if(notNull) {
			sb.append("    if(value == null) {\n");
			sb.append("      throw new Exception(\"Field \\\""+name+"\\\" on "+objectname+" object does not allow null\");\n");
			sb.append("    }\n");
		}
		if(len != null) {
			sb.append("    if(value.length() > "+len+") {\n");
			sb.append("      throw new Exception(\"Field \\\""+name+"\\\" on "+objectname+" object allows "+len+" characters. This string has length \"+value.length());\n");
			sb.append("    }\n");
		}
		sb.append("    po.set(\""+name+"\", value);\n");
		sb.append("  }\n");
	}

	public static void addGetterAndSetter(StringBuffer sb, Column col, String objectname) {
		addGetter(sb, col, objectname);
		addSetter(sb, col, objectname);
		sb.append("\n");
	}
	
	public static void addGetterDownstream(StringBuffer sb, String contraintname, String classname) {
		sb.append("  public "+classname+" get"+beansCamelName(contraintname)+"() throws Exception {\n");
		sb.append("    PrimaryKey pk = po.getPrimaryKeyFromDownstreamsForeignKeyName(\""+contraintname+"\");\n");
		sb.append("    if(!pk.hasCompleteSetOfValues()) {\n");
		sb.append("      return null;\n");
		sb.append("    }\n");
		sb.append("    DBPoolObject out = pool.getPoolObject(pk);\n");
		sb.append("    return new "+classname+"(pool, out, true);\n");
		sb.append("  }\n");
	}
	
	public static void addSetterDownstream(StringBuffer sb, String constraintname, String objectName) {
		sb.append("  public void set"+beansCamelName(constraintname)+"("+objectName+" value) throws Exception {\n");
		sb.append("    ForeignKeyDeclaration fkd = po.getForeignKeyDeclarationFromName(\""+constraintname+"\");\n");
		sb.append("    PrimaryKey pk = (value == null ? new PrimaryKey(fkd.getPrimaryKeyDeclaration()) : value.getPrimaryKey());\n");
		sb.append("    ForeignKey fk = ForeignKey.assembleForeignKey(pk, fkd);\n");
		sb.append("    po.setForeignKey(fk);\n");
		sb.append("  }\n");
	}
	
	public static void addGetterAndSetterDownstream(StringBuffer sb, String contraintname, String objectName) {
		addGetterDownstream(sb, contraintname, objectName);
		addSetterDownstream(sb, contraintname, objectName);
	}
	
	public static void addGetPrimaryKey(StringBuffer sb) {
		sb.append("  public PrimaryKey getPrimaryKey() {\n");
		sb.append("    return po.getPrimaryKey();\n");
		sb.append("  }\n");
	}
	
	public static void addReferrerGetter(StringBuffer sb, String referrerObjectName, String referrerTableName, String constraintname) {
		sb.append("  public List<"+referrerObjectName+"> get"+beansCamelName(constraintname)+"Referrers() throws Exception {\n");
		sb.append("    List<DBPoolObject> referrers = pool.getReferred(po, \""+referrerTableName+"\", \""+constraintname+"\");\n");
		sb.append("    List<"+referrerObjectName+"> out = new ArrayList<"+referrerObjectName+">();\n");
		sb.append("    for(DBPoolObject onePo : referrers) {\n");
		sb.append("      out.add(new "+referrerObjectName+"(pool, onePo, true));\n");
		sb.append("    }\n");
		sb.append("    return out;\n");
		sb.append("  }\n");		
	}
	
	public static void addDefaultSetting(StringBuffer sb, Column col) {
		Type type = col.getType();
		if(!type.hasDefault()) {
			return;
		}
		if(type instanceof DatetimeType) {
			if(type.hasNullDefault()) {
				return;
			}
			sb.append("    po.set(\""+col.getName()+"\", "+type.getDefaultAsString()+");\n");
			return;
		}
		if(type instanceof DecimalType) {
			if(type.hasNullDefault()) {
				return;
			}
			sb.append("    po.set(\""+col.getName()+"\", \""+type.getDefaultAsString()+"\");\n");
			return;
		}
		if(type instanceof DoubleType) {
			if(type.hasNullDefault()) {
				return;
			}
			sb.append("    po.set(\""+col.getName()+"\", "+type.getDefaultAsString()+");\n");
			return;
		}
		if(type instanceof IntType) {
			if(type.hasNullDefault()) {
				return;
			}
			sb.append("    po.set(\""+col.getName()+"\", "+type.getDefaultAsString()+");\n");
			return;
		}
		if(type instanceof TextualType) {
			if(type.hasNullDefault()) {
				return;
			}
			sb.append("    po.set(\""+col.getName()+"\", \""+type.getDefaultAsString()+"\");\n");
			return;
		}
		System.out.println("Type "+type+", class "+type.getTypename());
	}
	
	public static String getCode(String packagequalification, TableConfiguration ths) {
		StringBuffer out = new StringBuffer();
		String objectname = ths.getObjectName();
		String tablename = ths.getTableName();
		
		if(packagequalification != null && packagequalification.trim().length() > 0) {
			out.append("package "+packagequalification+";\n\n");
		}
		out.append("import org.omega.groovyintegration.pool.DBPoolObject;\n");
		out.append("import org.omega.groovyintegration.pool.DBPool;\n");
		out.append("import org.omega.groovyintegration.scope.Cruddable;\n");
		out.append("import org.omega.configuration.dbreferences.PrimaryKey;\n");
		out.append("import org.omega.configuration.dbreferences.ForeignKey;\n");
		out.append("import org.omega.configuration.struct.ForeignKeyDeclaration;\n");
		out.append("import org.omega.connectivity.abstraction.EmbeddedTableHandling;\n");
		//out.append("import java.io.InputStream;\n");
		//out.append("import java.io.ByteArrayOutputStream;\n");
		out.append("import java.util.Date;\n");
		out.append("import java.util.List;\n");
		out.append("import java.util.ArrayList;\n");
		out.append("import java.util.Hashtable;\n");
		out.append("\n");
		
		out.append("public class "+objectname+" implements Cruddable {\n\n");
		
		out.append("  private DBPoolObject po = null;\n");
		out.append("  private DBPool pool = null;\n");
		out.append("  public "+objectname+"(DBPool pool, DBPoolObject po, Boolean persisted) {\n");
		out.append("    this.pool = pool;\n");
		out.append("    this.po = po;\n");
		out.append("    this.persisted = persisted;\n");
		out.append("  }\n\n");
		
		out.append("  private static EmbeddedTableHandling defaultEnv = null;\n");
		out.append("  private static DBPool defaultPool = null;\n");
		out.append("  public static void setDefaults(EmbeddedTableHandling typesEnv, DBPool sessionsPool) {\n");
		out.append("    defaultEnv = typesEnv;\n");
		out.append("    defaultPool = sessionsPool;\n");
		out.append("  }\n");
		out.append("  public static "+objectname+" new"+objectname+"() throws Exception {\n");
		out.append("    DBPoolObject po = new DBPoolObject(null, defaultEnv);\n");

		CreateStatement cs = ths.getCreateStatement();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			if(col.isAutoIncrement()) {
				continue;
			}
			addDefaultSetting(out, col);
		}
		
		out.append("    "+objectname+" out = new "+objectname+"(defaultPool, po, false);\n");
		out.append("    return out;\n");
		out.append("  }\n\n");
		
		/*
		Vector<DBPoolObject> getPoolObjects(String table) throws Exception
		 */
		out.append("  public static List<"+objectname+"> all() throws Exception {\n");
		out.append("    List<DBPoolObject> found = defaultPool.getPoolObjects(\""+tablename+"\");\n");
		out.append("    List<"+objectname+"> out = new ArrayList<"+objectname+">();\n");
		out.append("    for(int i = 0 ; i < found.size() ; i++) {\n");
		out.append("      out.add(new "+objectname+"(defaultPool, found.get(i), true));\n");
		out.append("    }\n");
		out.append("    return out;\n");
		out.append("  }\n");
		out.append("\n");
		
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			if(col.isAutoIncrement()) {
				addGetter(out, col, objectname);
				out.append("\n");
				continue;
			}
			final String colname = col.getName();
			int noForeignKeyMemberships = cs.getNumberOfForeignKeyDeclarationMemberships(colname);
			if(noForeignKeyMemberships > 0) {
				addGetter(out, col, objectname);
				out.append("\n");
				continue;
			}
			addGetterAndSetter(out, col, objectname);
		}
		
		out.append("  private boolean persisted = true;\n");
		out.append("  public void persist() throws Exception {\n");
		out.append("    if(persisted) {;\n");
		out.append("      po.write();\n");
		out.append("    }\n");
		out.append("    else {\n");
		out.append("      po = pool.insertPoolObject(defaultEnv.ths, po.getHashtable());\n");
		out.append("    }\n");
		out.append("    persisted = true;\n");		
		out.append("  }\n\n");
		out.append("  public boolean isPersisted() throws Exception {\n");
		out.append("    return persisted && po.isWritten();\n");
		out.append("  }\n\n");
		out.append("  private boolean deleted = false;\n");
		out.append("  public void delete() throws Exception {\n");
		out.append("    deleted = true;\n");
		out.append("    pool.removePoolObject(po);\n");
		out.append("  }\n\n");
		out.append("\n");
		out.append("  public boolean equals(Object obj) {\n");
		out.append("    if(obj == null) {\n");
		out.append("      return false;\n");
		out.append("    }\n");
		out.append("    if(this == obj) {\n");
		out.append("      return true;\n");
		out.append("    }\n");
		out.append("    if(!(obj instanceof "+objectname+")) {\n");
		out.append("      return false;\n");
		out.append("    }\n");
		out.append("    PrimaryKey thisPk = po.getPrimaryKey();\n");
		out.append("    if(!thisPk.hasCompleteSetOfValues()) {\n");
		out.append("      return false;\n");
		out.append("    }\n");
		out.append("    PrimaryKey otherPk = (("+objectname+") obj).getPrimaryKey();\n");
		out.append("    return thisPk.equals(otherPk);\n");
		out.append("  }\n\n");
		
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
			ModelConfiguration hs = ths.getDBHandlingSpecification();
			TableConfiguration referenced = hs.getDBTableHandlingSpecificationOnTableName(fkd.getReferencedTablename());
			String name = referenced.getObjectName();
			addGetterAndSetterDownstream(out, fkd.getConstraintname(), name);
			out.append("\n");
		}
		
		
		addGetPrimaryKey(out);
		out.append("\n");
		
		int noReferringConstrtaints = ths.getNoReferringConstraints();
		ModelConfiguration hs = ths.getDBHandlingSpecification();
		
		for(int i = 0 ; i < noReferringConstrtaints ; i++) {
			ForeignKeyDeclaration fkd = ths.getReferringConstraint(i);
			String referringTablename = fkd.getReferringTablename();
			TableConfiguration referrer = hs.getDBTableHandlingSpecificationOnTableName(referringTablename);
			addReferrerGetter(out, referrer.getObjectName(), referrer.getTableName(), fkd.getConstraintname());
		}
		
		out.append("}");
		return out.toString();
	}

}
