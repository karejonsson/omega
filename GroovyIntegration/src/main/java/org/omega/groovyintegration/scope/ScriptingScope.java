package org.omega.groovyintegration.scope;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.apache.bsf.util.CodeBuffer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.ProgramBlock;

public class ScriptingScope {
	
	private BSFManager manager = null;
	
	public ScriptingScope(BSFManager manager) {
		this.manager = manager;
	}
	
	public Object eval(TextRenderer tr) throws BSFException {
		return eval("groovy", "("+tr.getScriptFromDescription()+").groovy", tr.getBeginLine(), tr.getBeginColumn(), tr.getScript());
	}

	private Object _eval(String script) throws BSFException {
		return eval("groovy", "script.groovy", 0, 0, script);
	}

	private Object eval(String language, String scriptname, int first, int second, String script) throws BSFException {
		//System.out.println("ScriptingScope.eval "+script);
		try {
			return manager.eval(language, scriptname, first, second, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw e;
		}
	}

	public void exec(ProgramBlock pb) throws BSFException {
		exec("groovy", "("+pb.getScriptFromDescription()+").groovy", pb.getBeginLine(), pb.getBeginColumn(), pb.getScript());
	}

	private void exec(String script) throws BSFException {
		exec("groovy", "script.groovy", 0, 0, script);
	}

	private void exec(String language, String scriptname, int first, int second, String script) throws BSFException {
		//System.out.println("ScriptingScope.exec "+script);
		try {
			manager.exec(language, scriptname, first, second, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw e;
		}
	}

	public void add(String lang, String source, int lineNo, int columnNo, String script) throws BSFException {
		//System.out.println("ScriptingScope.exec "+script);
		try {
			//manager.compileScript(lang, source, lineNo, columnNo, script, cb);
			manager.compileScript(lang, source, lineNo, columnNo, script, new CodeBuffer());
			//.exec(language, scriptname, first, second, script);
		}
		catch(Exception e) {
			Throwable t = e;
			while(t.getCause() != null) {
				t = t.getCause();
			}
			t.printStackTrace();
			throw e;
		}
	}

}
