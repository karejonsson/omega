package org.omega.groovyintegration.scope;

public interface SystemProgrammingResourceAccess {

	public void infoMessageDialog_OK(String frametitle, String message);
	public void errorMessageDialog_OK(String frametitle, String message);
	public void warningMessageDialog_OK(String frametitle, String message);
	
	public Boolean infoChoiceDialog_YES_NO_Cancel(String frametitle, String question);
	public Boolean warningChoiceDialog_YES_NO_Cancel(String frametitle, String question);
	public Boolean errorChoiceDialog_YES_NO_Cancel(String frametitle, String question);

	public String textDialog_OK_Cancel(String frametitle, String question);

	public void announceRuntimeError(String message);

}
