package org.omega.groovyintegration.scope;

import org.omega.configuration.dbreferences.PrimaryKey;

public interface Cruddable {

	public void persist() throws Exception;
	public boolean isPersisted() throws Exception;
	public void delete() throws Exception;
	public PrimaryKey getPrimaryKey();

}
