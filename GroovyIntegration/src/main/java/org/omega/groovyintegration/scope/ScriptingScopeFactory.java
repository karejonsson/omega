package org.omega.groovyintegration.scope;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyscripting.OneModel;
import org.omega.memorycompiler.DynamicClassLoader;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

public class ScriptingScopeFactory {
	
	private EmbeddedModelHandling details = null;
	private DBPool pool = null;
	private String packagename = null;
	private Class<?>[] classes = null;
	private OneModel om = null;
	private SystemProgrammingResourceAccess sysobject = null;
	
	public ScriptingScopeFactory(EmbeddedModelHandling details, String packagename, SystemProgrammingResourceAccess sysobject) throws Exception {
		this.details = details;
		this.packagename = packagename;
		this.sysobject = sysobject;
		
		om = new OneModel(details, packagename);
		pool = om.getPool();
		classes = om.getClasses();
	}
	
	public SystemProgrammingResourceAccess getSysObject() {
		return sysobject;
	}
	
	public String getAPI(String tablename) {
		return getAPI(details.getModelConfiguration().getDBTableHandlingSpecificationOnTableName(tablename));
	}
	
	public String getAPI(TableConfiguration ths) {
		return GenerateJavaAPI.getAPI(packagename, ths);
	}
	
	public String getPackagename() {
		return packagename;
	}
	
	public void clearPool() {
		pool.clearPool();
	}
	
	public DBPool getDBPool() {
		return pool;
	}
	
	public EmbeddedModelHandling getDetails() {
		return details;
	}
	
	public ScriptingScope getNodeScope(DBPoolObject po, boolean persisted) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		BSFManager manager = new BSFManager();
		DynamicClassLoader dcl = new DynamicClassLoader(classes[0].getClassLoader());
		manager.setClassLoader(dcl);
		
		TableConfiguration ths = po.getEnvironment().ths;
		Class objectClass = om.lookupClass(ths.getObjectName());
		
		Constructor<?> cons = objectClass.getConstructor(DBPool.class, DBPoolObject.class, Boolean.class);
		Cruddable cruddable = (Cruddable) cons.newInstance(pool, po, persisted);
		manager.declareBean("node", cruddable, Object.class);

		manager.declareBean("sys", sysobject, Object.class);

		return new ScriptingScope(manager);
	}

	public ScriptingScope getNodelessScope() {
		BSFManager manager = new BSFManager();
		DynamicClassLoader dcl = new DynamicClassLoader(classes[0].getClassLoader());
		manager.setClassLoader(dcl);
		return new ScriptingScope(manager);
	}

}
