package org.omega.groovyintegration.scope;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.VarcharType;

public class GenerateJavaAPI {

	public static String getType(Column col) {
		Type type = col.getType();
		if(type instanceof DatetimeType) {
			return "Date";
		}
		if(type instanceof DecimalType) {
			return "Decimal";
		}
		if(type instanceof DoubleType) {
			return "Double";
		}
		if(type instanceof IntType) {
			return "Long";
		}
		if(type instanceof TextualType) {
			return "String";
		}
		return "Object";
	}

	public static Integer getStringsLength(Column col) {
		Type type = col.getType();
		if(type instanceof VarcharType) {
			return ((VarcharType) type).getLength();
		}
		return null;
	}

	public static void addSearchOnTextalField(StringBuffer sb, Column col, String objectname) {
		String name = GenerateJava.beansCamelName(col.getName());
		sb.append("static List<"+objectname+"> findBy"+name+"(String search) throws Exception;\n");
	}

	public static void addBlobGetter(StringBuffer sb, Column col, String objectname) {
		sb.append("byte[] get"+GenerateJava.beansCamelName(col.getName())+"() throws Exception;\n");
	}

	public static void addGetter(StringBuffer sb, Column col, String objectname) {
		Type type = col.getType();
		if(type instanceof LobType) {
			addBlobGetter(sb, col, objectname);
			return;
		}
		String typing = getType(col);
		String name = col.getName();
		sb.append(typing+" get"+GenerateJava.beansCamelName(name)+"();\n");
		if(col.getType() instanceof TextualType) {
			addSearchOnTextalField(sb, col, objectname);
		}
	}
	
	public static void addBlobSetter(StringBuffer sb, Column col, String objectname) {
		sb.append("public void set"+GenerateJava.beansCamelName(col.getName())+"(byte[] value) throws Exception;\n");
		sb.append("public void set"+GenerateJava.beansCamelName(col.getName())+"(String filename, byte[] contents) throws Exception;\n");
	}

	public static void addSetter(StringBuffer sb, Column col, String objectname) {
		Type type = col.getType();
		if(type instanceof LobType) {
			addBlobSetter(sb, col, objectname);
			return;
		}
		String typing = getType(col);
		Integer len = getStringsLength(col);
		String name = col.getName();
		String lengthmaxname = null;
		if(len != null) {
			lengthmaxname = name.toLowerCase()+"_maxlen";
			sb.append("static final int "+lengthmaxname+" = "+len+";\n");
		}
		sb.append("void set"+GenerateJava.beansCamelName(name)+"("+typing+" value) throws Exception;\n");
	}

	public static void addGetterAndSetter(StringBuffer sb, Column col, String objectname) {
		addGetter(sb, col, objectname);
		addSetter(sb, col, objectname);
	}

	public static void addGetterDownstream(StringBuffer sb, String contraintname, String classname) {
		sb.append(classname+" get"+GenerateJava.beansCamelName(contraintname)+"() throws Exception;\n");
	}

	public static void addSetterDownstream(StringBuffer sb, String constraintname, String objectName) {
		sb.append("void set"+GenerateJava.beansCamelName(constraintname)+"("+objectName+" value) throws Exception;\n");
	}

	public static void addGetterAndSetterDownstream(StringBuffer sb, String contraintname, String objectName) {
		addGetterDownstream(sb, contraintname, objectName);
		addSetterDownstream(sb, contraintname, objectName);
	}

	public static void addGetPrimaryKey(StringBuffer sb) {
		sb.append("PrimaryKey getPrimaryKey();\n");
	}

	public static void addReferrerGetter(StringBuffer sb, String referrerObjectName, String referrerTableName, String constraintname) {
		sb.append("List<"+referrerObjectName+"> get"+GenerateJava.beansCamelName(constraintname)+"Referrers() throws Exception;\n");
	}

	public static String getAPI(String packagequalification, TableConfiguration ths) {
		StringBuffer out = new StringBuffer();
		String objectname = ths.getObjectName();

		if(packagequalification != null && packagequalification.trim().length() > 0) {
			out.append("package "+packagequalification+";\n\n");
		}

		out.append("static "+objectname+" new"+objectname+"() throws Exception;\n");

		CreateStatement cs = ths.getCreateStatement();

		out.append("static List<"+objectname+"> all() throws Exception;\n");

		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			if(col.isAutoIncrement()) {
				addGetter(out, col, objectname);
				continue;
			}
			final String colname = col.getName();
			int noForeignKeyMemberships = cs.getNumberOfForeignKeyDeclarationMemberships(colname);
			if(noForeignKeyMemberships > 0) {
				addGetter(out, col, objectname);
				continue;
			}
			addGetterAndSetter(out, col, objectname);
		}

		out.append("void persist() throws Exception;\n");
		out.append("boolean isPersisted() throws Exception;\n");
		out.append("void delete() throws Exception;\n");

		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
			ModelConfiguration hs = ths.getDBHandlingSpecification();
			TableConfiguration referenced = hs.getDBTableHandlingSpecificationOnTableName(fkd.getReferencedTablename());
			String name = referenced.getObjectName();
			addGetterAndSetterDownstream(out, fkd.getConstraintname(), name);
		}

		addGetPrimaryKey(out);

		int noReferringConstrtaints = ths.getNoReferringConstraints();
		ModelConfiguration hs = ths.getDBHandlingSpecification();

		for(int i = 0 ; i < noReferringConstrtaints ; i++) {
			ForeignKeyDeclaration fkd = ths.getReferringConstraint(i);
			String referringTablename = fkd.getReferringTablename();
			TableConfiguration referrer = hs.getDBTableHandlingSpecificationOnTableName(referringTablename);
			addReferrerGetter(out, referrer.getObjectName(), referrer.getTableName(), fkd.getConstraintname());
		}

		return out.toString();
	}

}
