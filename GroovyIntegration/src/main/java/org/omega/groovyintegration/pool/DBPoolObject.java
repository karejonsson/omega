package org.omega.groovyintegration.pool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.values.LobValue;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.helpers.DBTypeManagement;
import org.omega.connectivity.resultset.SQLResult;
import org.omega.connectivity.resultset.SQLResultMetaData;

import se.modlab.generics.util.FilesAndBytes;

public class DBPoolObject {
	
	private PrimaryKey pk;
	private EmbeddedTableHandling env;
	private Hashtable<String, Object> ht = new Hashtable<String, Object>();
	private boolean isWritten = true;
	//private String idName = null;
	private long time;
	
	public DBPoolObject(PrimaryKey pk, EmbeddedTableHandling env) throws Exception {
		this.pk = pk;
		this.env = env;
		isWritten = pk != null;
		if(pk != null) {
			for(String colname : pk.getNames()) {
				Object obj = pk.get(colname);
				set(colname, obj);
			}
		}
	}
	
	public Hashtable<String, Object> getHashtable() {
		return ht;
	}
	
	public String toString() {
		StringBuffer out = new StringBuffer();
		try {
			out.append("DBPoolObject: primary key "+pk+" {\n");
			out.append("  Type "+env.ths.getObjectName()+", table "+env.ths.getTableName()+"\n");
			out.append("  Is written "+isWritten+"\n");
			out.append("  Number of listeners "+listeners.size()+"\n");
			out.append("  Values {\n");
			for(String key : ht.keySet()) {
				out.append("    "+key+" : "+ht.get(key.toUpperCase())+"\n");
			}
			out.append("  }\n");
			out.append("}\n");
			return out.toString();
		}
		catch(Exception e) {
			String message = "DBPoolObject.toString() exception after "+out.toString();
			env.log(message, e);
			return message;
		}
	}
	
	public boolean hasColumn(String key) {
		CreateStatement cs = env.ths.getCreateStatement();
		Column col = cs.getColumn(key);
		return col != null;
	}
	
	public void set(String propertyName, Object obj) throws Exception {
		//System.out.println("DBPoolObject.set("+propertyName+", "+obj+") =====================================");
		CreateStatement cs = env.ths.getCreateStatement();
		Column col = cs.getColumn(propertyName);
		if(col == null) {
			String message = "There is no column "+propertyName+" in type "+env.getObjectName();
			env.logText(message+", Object "+toString());
			throw new Exception(message);
		}
		if(obj != null) {
			ht.put(propertyName.toUpperCase(), obj);
			if(pk != null && pk.getPrimaryKeyDeclaration().hasMember(propertyName)) {
				pk.set(propertyName, obj);
			}
		}
		else {
			ht.remove(propertyName.toUpperCase());
		}
		isWritten = false;
		time = System.currentTimeMillis();
	}
	
	public Object get(String propertyName) {
		return ht.get(propertyName.toUpperCase());
	}
	
	public void imperativeRefresh() throws Exception {
		SQLResult rs = rs = env.getReferred(env.ths, getPrimaryKey());
		try {
			SQLResultMetaData rsmd = rs.getMetaData();

			int colcount = rsmd.getColumnCount();
			if(colcount != env.ths.getCreateStatement().getNoColumns()) {
				String message = "Internal error. Columns count of DB table is "+env.ths.getCreateStatement().getNoColumns()+" but of SQL request "+colcount;
				env.logText(message);
				throw new Exception(message);
			}

			//Column col = env.ths.getCreateStatement().getFirstAutoIncrementColumn();
			//if(col == null) {
			//	throw new Exception("Internal error. There is no AUTO INCREMENT column in the table "+env.ths.getName());
			//}
			/*
			int idfieldnumber = -1;
			String idfieldname = col.getName();
			for(int i = 0 ; i < colcount ; i++) {
				if(idfieldname.toUpperCase().compareTo(idfieldname.toUpperCase()) == 0) {
					idfieldnumber = i;
					break;
				}
			}
			*/
			
			int ctr = 0;

			while (rs.next()) {
				ctr++;
				if(ctr == 2) {
					String message = "Selected for one row but got two or more rows. SQL="+env.getLastSQL();
					env.logText(message);
					throw new Exception(message);
				}
				//int idvalue = rs.getInt(idfieldnumber+1);
				for (int j = 0; j < colcount ; j++) {
					//if(j == idfieldnumber) {
						//continue;
					//}
					String colname = rsmd.getColumnName(j+1);
					if(!env.ths.hasColumn(colname)) {
						String message = "Column "+colname+" of the SQL result set is not conform to table "+env.getObjectName();
						env.logText(message);
						throw new Exception(message);
					}
					
					Object obj = DBTypeManagement.getDBValue(rs, j, env, colname);
					set(colname, obj);
				}
			}
			if(ctr == 0) {
				String message = "Select for one row made no rows. SQL="+env.getLastSQL();
				env.logText(message);
				throw new Exception(message);
			}
		}
		catch(SQLException sqle) {
			throw sqle;
		}
		catch(Exception e) {
			throw e;
		}
	}
	
	public void refresh() throws Exception {
		if(System.currentTimeMillis() - time < DBPool.maxTimeValid) {
			return;
		}
		imperativeRefresh();
	}
	
	public TableConfiguration getDBTableHandlingSpecification() {
		return env.ths;
	}
	
	public void write() throws Exception {
		env.update(ht, pk);
		isWritten = true;
	}
	
	public void setWritten(boolean written) {
		isWritten = written;
	}
	
	public boolean isWritten() {
		return isWritten;
	}

	public PrimaryKey get_Id() throws Exception {
		return getPrimaryKey();
	}
	
	private Vector<DBPoolObjectListener> listeners = new Vector<DBPoolObjectListener>();

	public void addListener(DBPoolObjectListener rl) {
		if(listeners.contains(rl)) {
			return;
		}
		listeners.add(rl);
	}

	public void removeAllListeners() {
		listeners.removeAllElements();
	}

	public void alertListeners() {
		for(DBPoolObjectListener listener : listeners) {
			listener.rowWasManipulated(this);
		}
	}

	public String _toString() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("Type "+env.getObjectName()+" id "+getPrimaryKey()+" ");
			for(String key : ht.keySet()) {
				sb.append(key+" = "+ht.get(key)+" ");
			}
			return sb.toString();
		}
		catch(Exception e) {
			String message = "DBPoolObject.toString() exception after "+sb.toString();
			env.log(message, e);
			return message;
		}
	}
	
	public long getTimeOfLastSetProperty() {
		return time;
	}
	
	public PrimaryKey getPrimaryKey() {
		//System.out.println("DBPoolObject.getPrimaryKey 1");
		if(pk != null) {
			//System.out.println("DBPoolObject.getPrimaryKey 2");
			return pk;
		}
		//System.out.println("DBPoolObject.getPrimaryKey 3");
		CreateStatement cs = env.ths.getCreateStatement();
		//System.out.println("DBPoolObject.getPrimaryKey 4");
		PrimaryKeyDeclaration pkd = cs.getPrimaryKeyDeclaration();
		//System.out.println("DBPoolObject.getPrimaryKey 5");
		PrimaryKey out = new PrimaryKey(pkd);
		//System.out.println("DBPoolObject.getPrimaryKey 6");
		for(String name : pkd.getNames()) {
			//System.out.println("DBPoolObject.getPrimaryKey 7");
			out.set(name, ht.get(name.toUpperCase()));
		}
		//System.out.println("DBPoolObject.getPrimaryKey 8");
		pk = out;
		return out;
	}
	
	public int getNoForeignKeys() {
		return env.ths.getCreateStatement().getNoForeignKeyDeclarations();
	}

	public int getNoKeys() {
		return env.ths.getCreateStatement().getNoKeyDeclarations();
	}
	
	public Key getKey(int idx) {
		CreateStatement cs = env.ths.getCreateStatement();
		KeyDeclaration kd = cs.getKeyDeclaration(idx);
		Key out = new Key(kd);
		for(String name : kd.getNames()) {
			//System.out.println("DBPoolObject.getPrimaryKey 7");
			out.set(name, ht.get(name.toUpperCase()));
		}
		return out;
	}

	public ForeignKeyDeclaration getForeignKeyDeclaration(int i) throws Exception {
		CreateStatement cs = env.ths.getCreateStatement();
		ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
		return fkd;
	}
	
	public ForeignKey getForeignKey(int i) throws Exception {
		//System.out.println("DBPoolObject getForeignKey("+i+")");
		CreateStatement cs = env.ths.getCreateStatement();
		//System.out.println("DBPoolObject getForeignKey 1 cs="+cs+" och i="+i);
		ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
		//System.out.println("DBPoolObject getForeignKey 2 fkd="+fkd);
		ForeignKey out = new ForeignKey(fkd);
		//System.out.println("DBPoolObject getForeignKey 3 fk="+out);
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String membername = fkd.getReferringMember(ii);
			//System.out.println("DBPoolObject getForeignKey 4");
			//String tablemembername = fkd.getTableMember(ii);
			//System.out.println("DBPoolObject getForeignKey member = "+membername+", tablemembername = "+tablemembername);
			out.set(membername, ht.get(membername.toUpperCase()));
			//System.out.println("DBPoolObject getForeignKey 5");
		}
		return out;
	}

/*
	public ForeignKey getForeignKey(String name) throws Exception {
		//System.out.println("DBPoolObject getForeignKey("+name+"), table="+env.ths.getName());
		CreateStatement cs = env.ths.getCreateStatement();
		//System.out.println("DBPoolObject getForeignKey 1 cs="+cs);
		ForeignKeyDeclaration fkd = cs.getFirstForeignKeyDeclarationFromName(name);
		//System.out.println("DBPoolObject getForeignKey 2 fkd="+fkd);
		ForeignKey out = new ForeignKey(fkd);
		//System.out.println("DBPoolObject getForeignKey 3 fk="+out);
		for(int ii = 0 ; ii < fkd.getNoMembers() ; ii++) {
			String membername = fkd.getMember(ii);
			//System.out.println("DBPoolObject getForeignKey 4");
			String tablemembername = fkd.getTableMember(ii);
			//System.out.println("DBPoolObject getForeignKey member = "+membername+", tablemembername = "+tablemembername);
			out.set(membername, ht.get(membername.toUpperCase()));
			//System.out.println("DBPoolObject getForeignKey 5");
		}
		return out;
	}
*/

	public void setForeignKey(ForeignKey fk) throws Exception {
		ForeignKeyDeclaration fkd = fk.getForeignKeyDeclaration();
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String membername = fkd.getReferringMember(ii);
			Object value = fk.get(membername);
			if(value != null) {
				ht.put(membername.toUpperCase(), fk.get(membername));				
			}
			else {
				ht.remove(membername.toUpperCase());
			}
		}
	}
	
	public int getIndexOfForeignKeyDeclaration(ForeignKeyDeclaration fkd) {
		CreateStatement cs = env.ths.getCreateStatement();
		return cs.getIndexOfForeignKeyDeclaration(fkd);
	}
	
	public EmbeddedTableHandling getEnvironment() {
		return env;
	}
	
	public PrimaryKey getPrimaryKeyFromDownstreamsForeignKeyName(String foreignKeyName) throws Exception {
		//System.out.println("DBPoolObject.getPrimaryKeyFromForeignKeyName("+foreignKeyName+") \n"+toString());
		return getForeignKeyDownstreams(foreignKeyName).assemblePrimaryKey();
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclarationFromName(String foreignKeyName) {
		CreateStatement cs = env.ths.getCreateStatement();
		return cs.getForeignKeyDeclaration(foreignKeyName);
	}
	
	public ForeignKey getForeignKeyDownstreams(String foreignKeyName) throws Exception {
		ForeignKeyDeclaration fkd = getForeignKeyDeclarationFromName(foreignKeyName);
		if(fkd == null) {
			return null;
		}
		ForeignKey fk = new ForeignKey(fkd);
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String membername = fkd.getReferringMember(ii);
			Object obj = ht.get(membername.toUpperCase());
			fk.set(membername, obj);
		}
		return fk;
	}
	
	public ForeignKey getForeignKeyUpstreams(ForeignKeyDeclaration fkd) throws Exception {
		EmbeddedModelHandling emh = env.getDetails();
		return env.getUpstreamForeignKey(fkd, ht);
	}
	

	
	/*
	public void uploadToBlob(String columnName, File f) throws Exception {
		if(f != null) {
			byte[] uploadable = FilesAndBytes.byteArrayFromFile(f);
			String inFilename = FilesAndBytes.getLastSectionOfFilename(f.getAbsolutePath());
	        InputStream inputStream_in = new BufferedInputStream(
	                      new ProgressMonitorInputStream(null,"Uploading "+inFilename, new ByteArrayInputStream(uploadable)));
	        LobValue val = new LobValue();
	        val.setFilename(FilesAndBytes.getFilenameFromArray(uploadable));
	        val.setLength(FilesAndBytes.getFilesizeFromArray(uploadable));
	        ht.put(columnName.toUpperCase(), val);
			env.uploadToBlob(inputStream_in, columnName, getPrimaryKey());
		}
		else {
	        LobValue val = new LobValue();
	        ht.put(columnName.toUpperCase(), val);
			env.uploadToBlob(null, columnName, getPrimaryKey());
		}
	}
	*/

	public void uploadToBlob(String columnName, byte[] uploadable) throws Exception {
		ByteArrayInputStream inputStream_in = new ByteArrayInputStream(uploadable);
        LobValue val = new LobValue();
        val.setFilename(FilesAndBytes.getFilenameFromArray(uploadable));
        val.setLength(FilesAndBytes.getFilesizeFromArray(uploadable));
        ht.put(columnName.toUpperCase(), val);
		env.uploadToBlob(inputStream_in, columnName, getPrimaryKey());
	}

	public void uploadToBlob(String columnName, InputStream inputStream_in, byte[] uploadable, String inFilename) throws Exception {
		if(inputStream_in != null) {
	        LobValue val = new LobValue();
	        val.setFilename(FilesAndBytes.getFilenameFromArray(uploadable));
	        val.setLength(FilesAndBytes.getFilesizeFromArray(uploadable));
	        ht.put(columnName.toUpperCase(), val);
			env.uploadToBlob(inputStream_in, columnName, getPrimaryKey());
		}
		else {
	        LobValue val = new LobValue();
	        ht.put(columnName.toUpperCase(), val);
			env.uploadToBlob(null, columnName, getPrimaryKey());
		}
	}

	public InputStream downloadFromBlob_with_executeQuery(String columnName) throws Exception {
		return env.downloadFromBlob_with_executeQuery(columnName, getPrimaryKey());
	}
	
	public byte[] downloadByteArrayFromBlob_with_executeQuery(String columnName) throws Exception {
		InputStream is = env.downloadFromBlob_with_executeQuery(columnName, getPrimaryKey());
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int nRead;
		byte[] data = new byte[16384];
		while ((nRead = is.read(data, 0, data.length)) != -1) {
		  buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
	
	

}
