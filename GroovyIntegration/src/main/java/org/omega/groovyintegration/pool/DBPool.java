package org.omega.groovyintegration.pool;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.helpers.DBTypeManagement;
import org.omega.connectivity.resultset.SQLResult;
import org.omega.connectivity.resultset.SQLResultMetaData;

import se.modlab.generics.util.Sort;

public class DBPool {
	
	private EmbeddedModelHandling details;
	private Hashtable<String, Hashtable<PrimaryKey, DBPoolObject>> pool = new Hashtable<String, Hashtable<PrimaryKey, DBPoolObject>>();
	private Hashtable<String, Boolean> hasAllMarker = new Hashtable<String, Boolean>();
	private Hashtable<ForeignKey, Boolean> hasAllPointing = new Hashtable<ForeignKey, Boolean>();
	private Hashtable<Key, Boolean> hasAllMemberHaving = new Hashtable<Key, Boolean>();
	static final long maxTimeValid = 1000*60*10; // 10 minutes in millis
	
	private static class TimeLength {
		private long time = 0;
		private int length = 0;
		public TimeLength(int length) {
			time = System.currentTimeMillis();
			this.length = length;
		}
		public long getTime() {
			return time;
		}
		public int getLength() {
			return length;
		}
	}
	
	public DBPool(EmbeddedModelHandling details) {
		this.details = details;
	}
	
	public void clearPool() {
		pool = new Hashtable<String, Hashtable<PrimaryKey, DBPoolObject>>();
		hasAllMarker = new Hashtable<String, Boolean>();
		hasAllPointing = new Hashtable<ForeignKey, Boolean>();
		hasAllMemberHaving = new Hashtable<Key, Boolean>();
		tl = new Hashtable<String, TimeLength>();
	}
	
	private Hashtable<String, TimeLength> tl = new Hashtable<String, TimeLength>();
	
	private void setLength(String tablename, int length) {
		tl.put(tablename, new TimeLength(length));
	}
	
	private int getLength(String tablename) {
		TimeLength tlo = tl.get(tablename);
		if(tlo == null) {
			return -1;
		}
		if(tlo.getTime() + maxTimeValid < System.currentTimeMillis()) {
			return -1;
		}
		return tlo.getLength();
	}
	
	public EmbeddedModelHandling getDetails() {
		return details;
	}
	
	public boolean hasAll(String tablename) {
		Boolean b = hasAllMarker.get(tablename);
		if(b == null) return false;
		return b;
	}
	
	public boolean hasAllPointing(ForeignKey fk) {
		//System.out.println("DBPool.hasAllPointing("+fk+")");
		String table = fk.getTablenamePointingFrom();
		//System.out.println("DBPool.hasAllPointing("+fk+") table = "+table);
		if(hasAll(table)) {
			//System.out.println("DBPool.hasAllPointing("+fk+") Has all");
			setHasAllPointing(fk);
			return true;
		}
		Boolean b = hasAllPointing.get(fk);
		//System.out.println("DBPool.hasAllPointing("+fk+") b = "+b);
		if(b == null) return false;
		return b;
	}
	
	public boolean hasAllMemberHaving(Key k) {
		String table = k.getTablename();
		if(hasAll(table)) {
			setHasAllMemberHaving(k);
			return true;
		}
		Boolean b = hasAllMemberHaving.get(k);
		if(b == null) return false;
		return b;
	}
	
	public void setHasAll(String tablename) {
		hasAllMarker.put(tablename, new Boolean(true));
	}
	
	public void setHasAllPointing(ForeignKey fk) {
		hasAllPointing.put(fk, new Boolean(true));
	}
	
	public void setHasAllMemberHaving(Key k) {
		hasAllMemberHaving.put(k, new Boolean(true));
	}
	
	private DBPoolObject getPooledObjectNoDB(PrimaryKey pk) throws Exception {
		if(!pk.hasCompleteSetOfValues()) {
			//System.out.println("DBPool.getPooledObjectNoDB ej komplett v???rden "+pk+" state = "+toString());
			return null;
		}
		//System.out.println("State "+toString()+", pk="+pk);
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(pk.getTablename());
		Hashtable<PrimaryKey, DBPoolObject> ht = pool.get(ths.getObjectName());
		if(ht == null) {
			details.logText("Get pooled object no db id = "+pk+", table = "+ths.getObjectName()+" returns null cause nothing seen from the whole table so far.");
			return null;
		}
		DBPoolObject obj = ht.get(pk);
		if(obj == null) {
			details.logText("Get pooled object no db id = "+pk+", table = "+ths.getObjectName()+" returns null");
		}
		else {
			details.logText("Get pooled object no db id = "+pk+", table = "+ths.getObjectName()+" returns pooled object");
		}
		return obj;
	}
	
	private void poolObject(DBPoolObject ro) throws Exception {
		//System.out.println("DBPool.poolObject 1");
		TableConfiguration ths = ro.getDBTableHandlingSpecification();
		//System.out.println("DBPool.poolObject 2");
		Hashtable<PrimaryKey, DBPoolObject> ht = pool.get(ths.getObjectName());
		//System.out.println("DBPool.poolObject 3");
		if(ht == null) {
			//System.out.println("DBPool.poolObject 4");
			ht = new Hashtable<PrimaryKey, DBPoolObject>();
			//System.out.println("DBPool.poolObject 5");
			details.logText("Pooling object "+ro.getPrimaryKey()+" of table "+ths.getObjectName()+" as first object in table");
			//System.out.println("DBPool.poolObject 6");
			pool.put(ths.getObjectName(), ht);
			//System.out.println("DBPool.poolObject 7");
		}
		//System.out.println("DBPool.poolObject 8");
		details.logText("Pooling object "+ro.getPrimaryKey()+" of table "+ths.getObjectName()+" as not first object in table");
		//System.out.println("DBPool.poolObject 9");
		ht.put(ro.getPrimaryKey(), ro);
		//System.out.println("DBPool.poolObject 10");
	}
	
	private DBPoolObject getPoolObjectCreateIfNecessaryDoPooling(PrimaryKey pk) throws Exception {
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(pk.getTablename());
		if(!pk.hasCompleteSetOfValues()) {
			return null;
		}
		DBPoolObject ro = getPooledObjectNoDB(pk);
		if(ro != null) {
			if(System.currentTimeMillis() - ro.getTimeOfLastSetProperty() > maxTimeValid) {
				ro.refresh();
				details.logText("Get object "+pk+" of type "+ths.getObjectName()+" by returning it from the pool with refresh first. "+ro);
				return ro;
			}
			details.logText("Get object "+pk+" of type "+ths.getObjectName()+" by returning it from the pool, no refresh. "+ro);
			return ro;
		}
		ro = getPoolObjectFromDBNoPooling(ths, pk);
		poolObject(ro);
		details.logText("Get object "+pk+" of type "+ths.getObjectName()+" by getting it from the DB "+ro);
		return ro;
	}
	
	private DBPoolObject getPoolObjectFromDBNoPooling(TableConfiguration ths, PrimaryKey pk) throws Exception {
		if(!pk.hasCompleteSetOfValues()) {
			return null;
		}
		if(ths == null) {
			//System.out.println("DBRowPool.getRowObjectFromDBNoPooling(null, "+pk+")");
		}
		SQLResult rs = null; 
		try {
			rs = details.getReferred(ths, pk);
		}
		catch(SQLException sqle) {
			details.log(sqle.getMessage(), sqle);
			throw sqle;
		}
		catch(Exception e) {
			details.log(e.getMessage(), e);
			throw e;
		}
		try {
			SQLResultMetaData rsmd = rs.getMetaData();

			if(rsmd == null) {
				details.logText("DBRowPool.getRowObjectFromDBNoPooling : ResultSetMetaData rsmd = rs.getMetaData() == null");
			}

			int resultSetsColCount = rsmd.getColumnCount();
			String[] nonBlobColumnNames = details.allNonBlobMembers(ths.getCreateStatement());

			if(resultSetsColCount != nonBlobColumnNames.length) {
				String message = "Internal error. Non blob columns count of DB table is "+nonBlobColumnNames.length+" but of SQL request "+resultSetsColCount;
				details.logText(message);
				throw new Exception(message);
			}
			int ctr = 0;
			DBPoolObject ro = null;
			EmbeddedTableHandling otherEnv = details.getEnvironment(ths);

			while (rs.next()) {
				ctr++;
				if(ctr == 2) {
					String message = "Selected for one row but got two or more rows. SQL="+details.getLastSQL();
					details.logText(message);
					throw new Exception(message);
				}
				//int idvalue = rs.getInt(idfieldnumber+1);
				ro = getPooledObjectNoDB(pk);
				if(ro == null) {
					ro = new DBPoolObject(pk, details.getEnvironment(ths)); 
					//poolRowObject(ro);
				}
				for (int j = 0; j < resultSetsColCount ; j++) {
					//if(j == idfieldnumber) {
					//	continue;
					//}
					String colname = rsmd.getColumnName(j+1);
					if(pk.hasName(colname)) {
						continue;
					}
					if(!ths.hasColumn(colname)) {
						String message = "Column "+colname+" of the SQL result set is not conform to table "+ths.getObjectName();
						details.logText(message);
						throw new Exception(message);
					}

					Object obj = null;
					try {
						obj = DBTypeManagement.getDBValue(rs, j, otherEnv, colname);
					}
					catch(Exception e) {
						details.logText("Unable to handle value in row "+ctr+", column "+j);
					}
					if(colname == null || obj == null) {
						//System.out.println("colname = "+colname+", obj = "+obj);
					}
					ro.set(colname, obj);
				}
			}
			if(ctr == 0) {
				String message = "Select for one row made no rows. SQL="+details.getLastSQL();
				details.logText(message);
				throw new Exception(message);
			}
			return ro;
		}
		catch(SQLException sqle) {
			details.log(sqle.getMessage(), sqle);
			throw sqle;
		}
		catch(Exception e) {
			details.log(e.getMessage(), e);
			throw e;
		}
	}

	public DBPoolObject getPoolObject(PrimaryKey pk) throws Exception {
		//System.out.println("DBPool.getPoolObject pk = "+pk);
		if(!pk.hasCompleteSetOfValues()) {
			return null;
		}
		/*DBTableHandlingSpecification ths = env.hs.getDBTableHandlingSpecification(pk.getTablename());
		if(ths == null) {
			String message = "Table "+pk.getTablename()+" is not in this information model. Error when getting object "+pk;
			env.slw.logText(message);
			throw new Exception(message);
		}*/
		return getPoolObjectCreateIfNecessaryDoPooling(pk);
	}
	
	public Vector<DBPoolObject> searchPoolObjects(TableConfiguration ths, Hashtable<String, Object> ht) throws Exception {
		SQLResult rs = details.getReferrers(ths, ht);
		SQLResultMetaData rsmd = rs.getMetaData();

		int resultSetsColCount = rsmd.getColumnCount();
		String[] nonBlobColumnNames = details.allNonBlobMembers(ths.getCreateStatement());
		
		if(resultSetsColCount != nonBlobColumnNames.length) {
			String message = "Non blob columns count of DB table is "+nonBlobColumnNames.length+" but of SQL request "+resultSetsColCount;
			details.logText(message);
			throw new Exception(message);
		}
		Vector<String> colnames = new Vector<String>();
		for(int i = 1 ; i <= resultSetsColCount ; i++) {
			colnames.add(rsmd.getColumnName(i));
		}

		PrimaryKeyDeclaration pkd = ths.getCreateStatement().getPrimaryKeyDeclaration();
		if(pkd == null) {
			String message = "(Pool.getPoolObjects) Table "+ths.getObjectName()+" has no primary key";
			details.logText(message);
			throw new Exception(message);
		}

		Hashtable<String, Integer> placelookup = new Hashtable<String, Integer>();
		for(int i = 0 ; i < resultSetsColCount ; i++) {
			String rstitle = rsmd.getColumnName(i+1).toUpperCase();
			if(pkd.hasMember(rstitle)) {
				placelookup.put(rstitle, i);
			}
		}
		if(placelookup.size() != pkd.getNoMembers()) {
			StringBuffer sb = new StringBuffer();
			for(String name : placelookup.keySet()) {
				sb.append(name+" ");
			}
			String message = "(Pool.getPoolObjects) The primary key declaration is "+pkd+" but only "+placelookup.size()+" of the members where found.\n"+
			"("+sb.toString()+")";
			details.logText(message);
			throw new Exception(message);
		}

		int ctr  = 0;
		Vector<DBPoolObject> out = new Vector<DBPoolObject>();
		EmbeddedTableHandling otherEnv = details.getEnvironment(ths);
		
		while (rs.next()) {
			//int id = rs.getInt(idfieldnumber+1);
			PrimaryKey pk = new PrimaryKey(pkd);
			for(String name : placelookup.keySet()) {
				pk.set(name, DBTypeManagement.getDBValue(rs, placelookup.get(name), otherEnv, name));
			}
			DBPoolObject ro = getPooledObjectNoDB(pk);
			if(ro == null) {
				ro = new DBPoolObject(pk, otherEnv);//getPoolObjectFromDBNoPooling(ths, pk);
			}
			for (int j = 0; j < resultSetsColCount ; j++) {
				//if(j == idfieldnumber) {
					//continue;
				//}
				String colname = rsmd.getColumnName(j+1);
				if(!ths.hasColumn(colname)) {
					String message = "Column "+colname+" of the SQL result set is not conform to table "+ths.getObjectName();
					details.logText(message);
					throw new Exception(message);
				}
				Object obj = DBTypeManagement.getDBValue(rs, j, otherEnv, colname);
				if(obj != null) {
					ro.set(colname, obj);
				}
			}
			poolObject(ro);
			out.add(ro);
			ctr++;
		}
		//setHasAllMemberHaving(k);
		return out;
	}
	
	public Vector<DBPoolObject> getPoolObjectsHavingMember(Key k) throws Exception {
		if(!k.hasCompleteSetOfValues()) {
			return new Vector<DBPoolObject>();
		}
		String table = k.getTablename();
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(table);
		if(ths == null) {
			String message = "Table "+table+" is not in this information model.";
			details.logText(message);
			throw new Exception(message);
		}
		
		if(hasAll(table)) {
			return getPoolObjectsHavingKeyNoDB(k);
		}
		if(!k.hasCompleteSetOfValues()) { 
			return new Vector<DBPoolObject>();
		}
		if(hasAllMemberHaving(k)) {
			return getPoolObjectsHavingKeyNoDB(k);
		}
		SQLResult rs = details.getReferrers(ths, k);
		SQLResultMetaData rsmd = rs.getMetaData();

		int resultSetsColCount = rsmd.getColumnCount();
		String[] nonBlobColumnNames = details.allNonBlobMembers(ths.getCreateStatement());
		
		if(resultSetsColCount != nonBlobColumnNames.length) {
			String message = "Non blob columns count of DB table is "+nonBlobColumnNames.length+" but of SQL request "+resultSetsColCount;
			details.logText(message);
			throw new Exception(message);
		}
		Vector<String> colnames = new Vector<String>();
		for(int i = 1 ; i <= resultSetsColCount ; i++) {
			colnames.add(rsmd.getColumnName(i));
		}

		PrimaryKeyDeclaration pkd = ths.getCreateStatement().getPrimaryKeyDeclaration();
		if(pkd == null) {
			String message = "(Pool.getPoolObjects) Table "+ths.getObjectName()+" has no primary key";
			details.logText(message);
			throw new Exception(message);
		}
		Hashtable<String, Integer> placelookup = new Hashtable<String, Integer>();
		KeyDeclaration kd = k.getKeyDeclaration();
		for(int i = 0 ; i < resultSetsColCount ; i++) {
			String rstitle = rsmd.getColumnName(i+1).toUpperCase();
			if(kd.hasMember(rstitle)) {
				placelookup.put(rstitle, i);
			}
		}
		if(placelookup.size() != kd.getNoMembers()) {
			StringBuffer sb = new StringBuffer();
			for(String name : placelookup.keySet()) {
				sb.append(name+" ");
			}
			String message = "(Pool.getPoolObjects) The key declaration is "+kd+" but only "+placelookup.size()+" of the members where found.\n"+
			"("+sb.toString()+")";
			details.logText(message);
			throw new Exception(message);
		}

		int ctr  = 0;
		Vector<DBPoolObject> out = new Vector<DBPoolObject>();
		EmbeddedTableHandling otherEnv = details.getEnvironment(ths);
		
		while (rs.next()) {
			//int id = rs.getInt(idfieldnumber+1);
			PrimaryKey pk = new PrimaryKey(pkd);
			for(String name : placelookup.keySet()) {
				pk.set(name, DBTypeManagement.getDBValue(rs, placelookup.get(name), otherEnv, name));
			}
			DBPoolObject ro = getPooledObjectNoDB(pk);
			if(ro == null) {
				ro = new DBPoolObject(pk, otherEnv);//getPoolObjectFromDBNoPooling(ths, pk);
			}
			for (int j = 0; j < resultSetsColCount ; j++) {
				//if(j == idfieldnumber) {
					//continue;
				//}
				String colname = rsmd.getColumnName(j+1);
				if(!ths.hasColumn(colname)) {
					String message = "Column "+colname+" of the SQL result set is not conform to table "+ths.getObjectName();
					details.logText(message);
					throw new Exception(message);
				}
				Object obj = DBTypeManagement.getDBValue(rs, j, otherEnv, colname);
				if(obj != null) {
					ro.set(colname, obj);
				}
			}
			poolObject(ro);
			out.add(ro);
			ctr++;
		}
		setHasAllMemberHaving(k);
		return out;
	}
	
	public DBPoolObject insertPoolObject(String table, Hashtable<String, Object> ht) throws Exception {
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(table);
		if(ths == null) {
			String message = "Table "+table+" is not in this information model.";
			details.logText(message);
			throw new Exception(message);
		}
		return insertPoolObject(ths, ht);
	}

	public DBPoolObject insertPoolObject(TableConfiguration ths, Hashtable<String, Object> ht) throws Exception {
		String sql = null;
		try {
			if(ht.size() == 0) {
				String message = "Insert was requested but there are no values for the DB row.";
				details.logText(message);
				throw new Exception(message);
			}
			//System.out.println("DBPool.insertPoolObject - 1");
			Column col = ths.getCreateStatement().getAutoIncrementColumn();
			if(col != null) {
				//System.out.println("DBPool.insertPoolObject - 2");
				String idName = col.getName();
				//System.out.println("DBPool.insertPoolObject - 3");
				Object obj = ht.get(idName.toUpperCase());
				if(obj != null) {
					String message = "The value "+obj+" was provided with label "+idName+" which is an auto increment field.";
					details.logText(message);
					throw new Exception(message);
				}
			}
			//System.out.println("DBPool.insertPoolObject - 4");

			details.insert(ths, ht);

			//System.out.println("DBPool.insertPoolObject - 6");
			PrimaryKey pk = PrimaryKey.assemblePrimaryKey(ht, ths.getCreateStatement().getPrimaryKeyDeclaration());
			//System.out.println("DBPool.insertPoolObject - 7 "+pk);
			DBPoolObject ro = getPooledObjectNoDB(pk);
			//System.out.println("DBPool.insertPoolObject - 8");
			if(ro == null) {
				//System.out.println("DBPool.insertPoolObject - 9");
				ro = new DBPoolObject(pk, details.getEnvironment(ths));
			}
			//System.out.println("DBPool.insertPoolObject - 11");
			for(String key : ht.keySet()) {
				//System.out.println("DBPool.insertPoolObject - 12");
				ro.set(key, ht.get(key));
			} 
			//System.out.println("DBPool.insertPoolObject - 13");
			poolObject(ro);
			//System.out.println("DBPool.insertPoolObject - 14");
			return ro;
		}
		catch(Exception e) {
			String sqlmessage = "Failed on insert. Sql was "+sql+"\n\n"+
					"Exceptions message was "+e.getMessage();
			details.logText(sqlmessage);
			e.printStackTrace();
			throw new Exception(sqlmessage, e);
		}
	}

	public Vector<DBPoolObject> getPoolObjects(String table) throws Exception {
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(table);
		if(ths == null) {
			String message = "Table "+table+" is not in this information model.";
			details.logText(message);
			throw new Exception();
		}
		return getPoolObjects(ths);
	}

	private Comparator<PrimaryKey> pkc = new Comparator<PrimaryKey>() {

		public int compare(PrimaryKey pk1, PrimaryKey pk2) {
			PrimaryKeyDeclaration pkd = pk1.getPrimaryKeyDeclaration();
			for(int i = 0 ; i < pkd.getNoMembers() ; i++) {
				Object obj1 = null;
				Object obj2 = null;
				try {
					obj1 = pk1.get(pkd.getMember(i));
					obj2 = pk2.get(pkd.getMember(i));
					if(obj1.getClass().getName().compareTo(obj2.getClass().getName()) != 0 ) {
						// Can hope to get something comparable next loop
						continue;
					}
					if(obj1 instanceof String) {
						int out = obj1.toString().compareTo(obj2.toString());
						if(out != 0) {
							return out;
						}
					}
					if(obj1 instanceof Integer) {
						int out = ((Integer) obj1) - ((Integer) obj2);
						if(out != 0) {
							return out;
						}
					}
					if(obj1 instanceof Double) {
						double out = ((Double) obj1) - ((Double) obj2);
						if(out != 0) {
							return out > 0 ? 1 : -1;
						}
					}
					if(obj1 instanceof Long) {
						Long out = ((Long) obj1) - ((Long) obj2);
						if(out != 0) {
							return out > 0 ? 1 : -1;
						}
					}
				}
				catch(Exception e) {
					// Can hope to get something comparable next loop
				}
			}
			return 0;
		}

	};

	public Vector<DBPoolObject> getPoolObjects(final TableConfiguration ths) throws Exception {
		//System.out.println("DBPool.getPoolObjects beg - 1");
		int ctr  = 0;
		String tablename = ths.getObjectName();
		//System.out.println("DBPool.getPoolObjects beg - 2");
		if(hasAll(tablename)) {
			//System.out.println("DBPool.getPoolObjects beg - 3");
			Vector<DBPoolObject> out = new Vector<DBPoolObject>();
			//System.out.println("DBPool.getPoolObjects beg - 4");
			Hashtable<PrimaryKey, DBPoolObject> table = pool.get(tablename);
			if(table == null) {
				return out;
			}
			//System.out.println("DBPool.getPoolObjects beg - 5");
			Vector<PrimaryKey> sorted = new Vector<PrimaryKey>();
			for(PrimaryKey pk : table.keySet()) {
				//System.out.println("DBPool.getPoolObjects beg - 6");
				Sort.add(sorted, pk, pkc);
			}

			for(int i = 0 ; i < sorted.size() ; i++) {
				//System.out.println("DBPool.getPoolObjects beg - 6");
				out.addElement(table.get(sorted.elementAt(i)));
			}
			setLength(tablename, out.size());
			return out;
		}
		try {
			//System.out.println("DBPool.getPoolObjects - 1");
			SQLResult rs = details.selectAll(ths);
			//System.out.println("DBPool.getPoolObjects - 2");
			SQLResultMetaData rsmd = rs.getMetaData();
			int resultSetsColCount = rsmd.getColumnCount();
			String[] nonBlobColumnNames = details.allNonBlobMembers(ths.getCreateStatement());
			//System.out.println("DBPool.getPoolObjects - 3");
			if(resultSetsColCount != nonBlobColumnNames.length) {
				String message = "Blob free column count of DB table is "+nonBlobColumnNames.length+" but of SQL request "+resultSetsColCount;
				//System.out.println("DBPool.getPoolObjects - 4");
				details.logText(message);
				//System.out.println("DBPool.getPoolObjects - 5");
				throw new Exception(message);
			}
			
			Hashtable<String, Integer> placelookup = new Hashtable<String, Integer>();
			PrimaryKeyDeclaration pkd = ths.getCreateStatement().getPrimaryKeyDeclaration();
			//System.out.println("DBPool.getPoolObjects - 6");
			if(pkd == null) {
				throw new Exception("(Pool.getPoolObjects) Table "+ths.getObjectName()+" has no primary key");
			}
			//System.out.println("DBPool.getPoolObjects colcount = "+colcount);
			for(int i = 0 ; i < resultSetsColCount ; i++) {
				String rstitle = rsmd.getColumnName(i+1).toUpperCase();
				//System.out.println("DBPool.getPoolObjects - 7");
				if(pkd.hasMember(rstitle)) {
					//System.out.println("DBPool.getPoolObjects - 8 colname "+rstitle+" on place "+i);
					placelookup.put(rstitle, i);
					
				}
			}
			if(placelookup.size() != pkd.getNoMembers()) {
				//System.out.println("DBPool.getPoolObjects - 9");
				StringBuffer sb = new StringBuffer();
				for(String name : placelookup.keySet()) {
					//System.out.println("DBPool.getPoolObjects - 10");
					sb.append(name+" ");
				}
				throw new Exception("(Pool.getPoolObjects) The primary key declaration is "+pkd+" but only "+placelookup.size()+" of the members where found.\n"+
						"("+sb.toString()+")");
			}

			Vector<DBPoolObject> out = new Vector<DBPoolObject>();
			EmbeddedTableHandling otherEnv = details.getEnvironment(ths);
			
			//long before = System.currentTimeMillis();
			//long before_keep = before;
			//long after = 0;
			while (rs.next()) {
				//after = System.currentTimeMillis();
				//System.out.println("DBPool.getPoolObjects: "+ctr+" row after "+(after - before)+" millis. Total "+(after - before_keep));
				//before = after;
				PrimaryKey pk = new PrimaryKey(pkd);
				//System.out.println("DBPool.getPoolObjects - 11 pk="+pk);
				for(String name : placelookup.keySet()) {
					Object obj = DBTypeManagement.getDBValue(rs, placelookup.get(name), otherEnv, name);
					//System.out.println("DBPool.getPoolObjects - 12 obj="+obj);
					pk.set(name, obj);
				}
				//System.out.println("DBPool.getPoolObjects - 12.5 pk="+pk);
				//if(!pk.hasCompleteSetOfValues()) {
					
					//ctr++;
					//continue;
				//}
				DBPoolObject ro = getPooledObjectNoDB(pk);
				//System.out.println("DBPool.getPoolObjects - ");
				if(ro == null) {
					//System.out.println("DBPool.getPoolObjects - 13");
					ro = new DBPoolObject(pk, otherEnv);
				}
				for (int j = 0; j < resultSetsColCount ; j++) {
					//System.out.println("DBPool.getPoolObjects - 14");
					String colname = rsmd.getColumnName(j+1);
					//System.out.println("DBPool.getPoolObjects - 15 colname="+colname);
					if(!ths.hasColumn(colname)) {
						String message = "Column "+colname+" of the SQL result set is not conform to table "+ths.getObjectName();
						details.logText(message+", object "+ro);
						throw new Exception(message);
					}
					//System.out.println("DBPool.getPoolObjects - 16");
					Object obj = DBTypeManagement.getDBValue(rs, j, otherEnv, colname);
					//System.out.println("DBPool.getPoolObjects - obj="+obj);
					ro.set(colname, obj);
				}
				//System.out.println("DBPool.getPoolObjects - 18");
				poolObject(ro);
				//System.out.println("DBPool.getPoolObjects - 19");
				out.add(ro);
				ctr++;
			}
			details.logText("All objects of table "+ths.getObjectName()+" pooled. Count is "+out.size());
			setHasAll(tablename);
			//System.out.println("Has all objects of table "+tablename);
			setLength(tablename, out.size());
			return out;
		}
		catch(SQLException sqle) {
			String message = "SQL exception: "+sqle.getMessage();
			details.logText(message+", object "+ctr);
			details.logTrace(sqle);
			//System.out.println("DBPool.getPoolObjects SQLE "+message);
			throw new Exception(message);
		}
		catch(Exception e) {
			String message = "Exception: "+e.getMessage();
			details.logText(message+", object "+ctr);
			e.printStackTrace();
			//System.out.println("DBPool-getPoolObjects E "+message);
			throw new Exception(message);
		}
	}
	
	private boolean hasForeignKey(DBPoolObject po, ForeignKey fk) throws Exception {
		EmbeddedTableHandling env = po.getEnvironment();
		Hashtable<String, Object> ht = po.getHashtable();
		for(int i = 0 ; i < po.getNoForeignKeys() ; i++) {
			if(fk.equals(env.getForeignKey(i, ht))) {
				return true;
			}
		}
		return false;
	}

	private boolean hasKey(DBPoolObject po, Key k) throws Exception {
		EmbeddedTableHandling env = po.getEnvironment();
		Hashtable<String, Object> ht = po.getHashtable();
		for(int i = 0 ; i < po.getNoKeys() ; i++) {
			if(k.equals(env.getKey(i, ht))) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasKeyImplicitly(DBPoolObject po, Key k) throws Exception {
		EmbeddedTableHandling env = po.getEnvironment();
		KeyDeclaration kd = k.getKeyDeclaration();
		Key implicitKey = new Key(kd);
		for(int i = 0 ; i < kd.getNoMembers() ; i++) {
			String name = kd.getMember(i);
			implicitKey.set(name, po.get(name));
		}
		return implicitKey.equals(k);
	}

	private Vector<DBPoolObject> getPoolObjectsHavingPointerNoDB(ForeignKey fk) throws Exception {
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB fk="+fk);
		if(!fk.hasCompleteSetOfValues()) {
			//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB incomplete fk");
			return new Vector<DBPoolObject>();
		}
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(fk.getTablenamePointingFrom());
		CreateStatement cs = ths.getCreateStatement();
		PrimaryKeyDeclaration pkd = cs.getPrimaryKeyDeclaration();
		if(pkd == null) {
			String message = "No primary key declared in table "+ths.getObjectName();
			details.logText(message);
			throw new Exception(message);
		}
		Hashtable<PrimaryKey, DBPoolObject> tablepool = pool.get(ths.getObjectName());
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB tables name "+ths.getName());
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB currently pooled "+((tablepool != null) ? tablepool.size() : 0)+" objects.");
		Vector<DBPoolObject> out = new Vector<DBPoolObject>();
		if(tablepool == null) {
			return out;
		}
		for(PrimaryKey apk : tablepool.keySet()) {
			DBPoolObject po = tablepool.get(apk);
			if(hasForeignKey(po, fk)) {
				out.add(po);
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB OK "+fk+" in "+apk);
			}
			else {
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB NOK "+fk+" not in "+apk);
			}
		}
		return out;
	}
	
	private Vector<DBPoolObject> getPoolObjectsHavingKeyNoDB(Key k) throws Exception {
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB fk="+fk);
		if(!k.hasCompleteSetOfValues()) {
			//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB incomplete fk");
			return new Vector<DBPoolObject>();
		}
		TableConfiguration ths = details.getDBTableHandlingSpecificationOnTableName(k.getTablename());
		CreateStatement cs = ths.getCreateStatement();
		PrimaryKeyDeclaration pkd = cs.getPrimaryKeyDeclaration();
		if(pkd == null) {
			String message = "No primary key declared in table "+ths.getObjectName();
			details.logText(message);
			throw new Exception(message);
		}
		/*
		if(!k.getForeignKeyDeclaration().compliant(pkd)) {
			String message = "Foreign key "+fk+" is not compliant with primary key declaration "+pkd;
			details.logText(message);
			throw new Exception(message);
		} */
		//PrimaryKey pk = fk.assemblePrimaryKey(pkd);
		Hashtable<PrimaryKey, DBPoolObject> tablepool = pool.get(ths.getObjectName());
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB tables name "+ths.getName());
		//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB currently pooled "+((tablepool != null) ? tablepool.size() : 0)+" objects.");
		Vector<DBPoolObject> out = new Vector<DBPoolObject>();
		if(tablepool == null) {
			return out;
		}
		for(PrimaryKey apk : tablepool.keySet()) {
			DBPoolObject po = tablepool.get(apk);
			//System.out.println("DBPool.getPoolObjectsHavingKeyNoDB("+k+"), po = "+po+", out stlk = "+out.size()+", innan");
			if(hasKey(po, k)) {
				out.add(po);
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB OK "+fk+" in "+apk);
			}
			else {
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB NOK "+fk+" not in "+apk);
			}
			if(hasKeyImplicitly(po, k)) {
				out.add(po);
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB OK "+fk+" in "+apk);
			}
			else {
				//System.out.println("DBPool.getPoolObjectsHavingPointerNoDB NOK "+fk+" not in "+apk);
			}
			//System.out.println("DBPool.getPoolObjectsHavingKeyNoDB("+k+"), po = "+po+", out stlk = "+out.size()+", efter");
		}
		return out;
	}
	
	public Vector<DBPoolObject> getPoolObjectsHavingPointer(ForeignKey fk) throws Exception {
		//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+")");
		if(!fk.hasCompleteSetOfValues()) {
			return new Vector<DBPoolObject>();
		}
		String tableFrom = fk.getTablenamePointingFrom();
		TableConfiguration thsFrom = details.getDBTableHandlingSpecificationOnTableName(tableFrom);
		if(thsFrom == null) {
			String message = "Table "+tableFrom+" is not in this information model.";
			details.logText(message);
			throw new Exception(message);
		}
		
		String tableTo = fk.getTablenamePointingTo();//fk.getTablenamePointingFrom();
		TableConfiguration thsTo = details.getDBTableHandlingSpecificationOnTableName(tableTo);
		if(thsTo == null) {
			String message = "Table "+tableTo+" is not in this information model.";
			details.logText(message);
			throw new Exception(message);
		}
		
		if(hasAll(tableFrom)) {
			//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+") has all");
			return getPoolObjectsHavingPointerNoDB(fk);
		}
		if(!fk.hasCompleteSetOfValues()) { 
			return new Vector<DBPoolObject>();
		}
		if(hasAllPointing(fk)) {
			//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+") has all pointing");
			return getPoolObjectsHavingPointerNoDB(fk);
		}
		SQLResult rs = details.getReferrers(thsFrom, fk);
		SQLResultMetaData rsmd = rs.getMetaData();

		int resultSetsColCount = rsmd.getColumnCount();
		String[] nonBlobColumnNames = details.allNonBlobMembers(thsFrom.getCreateStatement());

		//int colcount = rsmd.getColumnCount();
		if(resultSetsColCount != nonBlobColumnNames.length) {
			String message = "Non blob columns count of DB table is "+nonBlobColumnNames.length+" but of SQL request "+resultSetsColCount;
			details.logText(message);
			throw new Exception(message);
		}
		Vector<String> colnames = new Vector<String>();
		for(int i = 1 ; i <= resultSetsColCount ; i++) {
			colnames.add(rsmd.getColumnName(i));
		}

		PrimaryKeyDeclaration pkd = thsFrom.getCreateStatement().getPrimaryKeyDeclaration();
		//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+") pkd="+pkd);

		if(pkd == null) {
			String message = "(Pool.getPoolObjects) Table "+thsFrom.getObjectName()+" has no primary key";
			details.logText(message);
			throw new Exception(message);
		}
		Hashtable<String, Integer> placelookup = new Hashtable<String, Integer>();
		for(int i = 0 ; i < resultSetsColCount ; i++) {
			String rstitle = rsmd.getColumnName(i+1).toUpperCase();
			if(pkd.hasMember(rstitle)) {
				placelookup.put(rstitle, i);
			}
		}
		if(placelookup.size() != pkd.getNoMembers()) {
			StringBuffer sb = new StringBuffer();
			for(String name : placelookup.keySet()) {
				sb.append(name+" ");
			}
			String message = "(Pool.getPoolObjects) The primary key declaration is "+pkd+" but only "+placelookup.size()+" of the members where found.\n"+
			"("+sb.toString()+")";
			details.logText(message);
			throw new Exception(message);
		}

		int ctr  = 0;
		Vector<DBPoolObject> out = new Vector<DBPoolObject>();
		EmbeddedTableHandling otherEnv = details.getEnvironment(thsFrom);
		
		while (rs.next()) {
			//int id = rs.getInt(idfieldnumber+1);
			PrimaryKey pk = new PrimaryKey(pkd);
			for(String name : placelookup.keySet()) {
				pk.set(name, DBTypeManagement.getDBValue(rs, placelookup.get(name), otherEnv, name));
			}
			DBPoolObject ro = getPooledObjectNoDB(pk);
			if(ro == null) {
				ro = new DBPoolObject(pk, otherEnv);//getPoolObjectFromDBNoPooling(ths, pk);
			}
			for (int j = 0; j < resultSetsColCount ; j++) {
				//if(j == idfieldnumber) {
					//continue;
				//}
				String colname = rsmd.getColumnName(j+1);
				if(!thsFrom.hasColumn(colname)) {
					String message = "Column "+colname+" of the SQL result set is not conform to table "+thsFrom.getObjectName();
					details.logText(message);
					throw new Exception(message);
				}
				Object obj = DBTypeManagement.getDBValue(rs, j, otherEnv, colname);
				if(obj != null) {
					ro.set(colname, obj);
					//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+"): colname "+colname+" -> "+obj);
				}
				else {
					//System.out.println("DBPool.getPoolObjectsHavingPointer("+fk+"): colname "+colname+" -> null, thus not inserted");
				}
			}
			poolObject(ro);
			out.add(ro);
			ctr++;
		}
		setHasAllPointing(fk);
		return out;
	}
	
	private void removePoolObjectNoDBUpdate(DBPoolObject ro) throws Exception {
		PrimaryKey pk = ro.getPrimaryKey();
		TableConfiguration ths = ro.getDBTableHandlingSpecification();
		String tablename = ths.getObjectName();
		Hashtable<PrimaryKey, DBPoolObject> tablespool = pool.get(tablename);
		if(tablespool == null) {
			return;
		}
		tablespool.remove(pk);
	}
	
	public void removePoolObject(DBPoolObject ro) throws Exception {
		PrimaryKey pk = ro.getPrimaryKey();
		details.delete(pk);
		removePoolObjectNoDBUpdate(ro);
	}
		
    public String toString() {
    	// Hashtable<String, Hashtable<Integer, DBRowObject>>
    	StringBuffer sb = new StringBuffer();
    	int ctr = 0;
    	sb.append("Number of tables represented "+pool.size()+"\n");
    	for(String tablename : pool.keySet()) {
    		Hashtable<PrimaryKey, DBPoolObject> tablespool = pool.get(tablename);
    		sb.append("  "+details.translate(tablename)+"   "+tablespool.size()+" objects. ");
    		Boolean hasAll = hasAllMarker.get(tablename);
    		if(hasAll != null && hasAll == true) {
    			sb.append("(all)");
    		}
    		sb.append("\n");	
    	    ctr += tablespool.size();
    	}
    	sb.append("Total number of pooled rowobjects: "+ctr+"\n");
    	return sb.toString();
    }
    
    public int getTableLength(TableConfiguration ths) throws Exception {
		details.logText("DBPool.getTableLength table "+ths.getObjectName());
    	String name = ths.getObjectName();
    	if(hasAll(name)) {
    		details.logText("DBPool.getTableLength has all");
    		Hashtable<PrimaryKey, DBPoolObject> tpool = pool.get(name);
    		if(tpool == null) {
    			details.logText("DBPool.getTableLength has all which is zero");
    			return 0;
    		}
    		details.logText("DBPool.getTableLength has all which is "+tpool.size());
    		return tpool.size();
    	}
    	else {
    		details.logText("DBPool.getTableLength has not all");
    	}
    	int length = getLength(name);
    	if(length != -1) {
    		Hashtable<PrimaryKey, DBPoolObject> tpool = pool.get(name);
    		if(tpool == null) {
    			details.logText("DBPool.getTableLength table "+ths.getObjectName()+", length was cached. Table pool was null");
        		return length;
    		} /*
    		else {
    			int actualSize = tpool.size();
    			details.logText("DBPool.getTableLength table "+ths.getName()+", length was cached. Table pool size was "+actualSize+" ################");
    			if(actualSize != length) {
    				setLength(name, actualSize);
    				return actualSize;
    			}
    			return length;
    		} */
    	}
		int out = details.getTableLength(ths.getObjectName(), ths.getCreateStatement());
		details.logText("DBPool.getTableLength table "+ths.getObjectName()+", out="+out+". Asked by SQL.");
		setLength(name, out);
    	return out;
    }

    public int _getTableLength(TableConfiguration ths) throws Exception {
    	try {
    		int out = _getTableLength(ths);
        	String name = ths.getObjectName();
    		if(hasAll(name)) {
        		Hashtable<PrimaryKey, DBPoolObject> tpool = pool.get(name);
        		if(tpool == null) {
            		System.out.println("DBPool.getTableLength has all which is zero");
        			return 0;
        		}
        		System.out.println("DBPool.getTableLength has all which is "+tpool.size());
        		return tpool.size();
    		}
    		return out;
    	}
    	catch(Exception e) {
    		//System.out.println("DBPool.getTableLength exception "+e.getMessage());
    		//e.printStackTrace();
    		throw e;
    	}
    }
    
	public boolean equals(Object o) {
		if(o == null) {
			//System.out.println("DBPool.equals null");
			return false;
		}
		if(o == this) {
			//System.out.println("DBPool.equals same");
			return true;
		}
		if(!(o instanceof DBPool)) {
			//System.out.println("DBPool.equals wrong class "+o.getClass().getName());
			return false;
		}
		DBPool other = (DBPool) o;
		return details.equals(other.details);
	}
	
	public List<DBPoolObject> getReferred(DBPoolObject referred, String tablename, String constraintname) throws Exception {
		  TableConfiguration ths = details.getModelConfiguration().getDBTableHandlingSpecificationOnTableName(tablename);
		  ForeignKeyDeclaration fkd = ths.getCreateStatement().getForeignKeyDeclaration(constraintname);
		  PrimaryKey pk = referred.getPrimaryKey();
		  ForeignKey fk = ForeignKey.assembleForeignKey(pk, fkd);
		  return getPoolObjectsHavingPointer(fk);
	}

}
