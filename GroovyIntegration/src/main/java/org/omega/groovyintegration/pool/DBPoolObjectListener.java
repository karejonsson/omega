package org.omega.groovyintegration.pool;

public interface DBPoolObjectListener {
	
	public void rowWasManipulated(DBPoolObject ro);
	
}
