package org.omega.nativeclient.typegraphics;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.ReferrerEditorCommons;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
/*
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.renderers.TableRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.NumericType;
import org.omega.connectivity.helpers.GeneralComparator;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.nativeclient.helpers.NativeClientTableHandling;
*/
import org.omega.nativeclient.treeview.ExplorerWindow;
import org.omega.nativeclient.treeview.NewRowDialog;
import org.omega.nativeclient.treeview.ReferrerNode;
import org.omega.nativeclient.treeview.RowNode;

import org.omega.nativeclient.treeview.MainWindow;

import se.modlab.generics.util.Sort;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.gui.plot.TimePlotComponent;
import se.modlab.generics.gui.plot.TimePlotDataContainer;
import se.modlab.generics.gui.util.Clipboard;

public class ReferrerGraphicalEditor extends GraphicalEditor {

	private final JScrollPane view = new JScrollPane();
	private JTable table = null;//new JTable();
	private JButton state = new JButton("Open");
	private JLabel message = new JLabel("Click on button at right to view");
	private StateDot dot = null;
	private int height = -1;
	private boolean expanded = false;

	//private NativeClientTableHandling referredTable = null;
	//private ForeignKey fk;
	//private DBPool pool;
	//private Vector<DBPoolObject> tableRowsOrder = null;//new Vector<RowNode>();
	
	private ClientTableLogic referred = null;
	private ClientModelLogic cml = null;
	private ClientTableLogic referring = null;
	
	private Window currentWindow = null;
	
	private ReferrerEditorCommons rec = null;
	//private final boolean persisted = true;

	private RowNode referredInstance = null;
	private ReferrerNode refn = null;

	public ReferrerGraphicalEditor(ReferrerEditorCommons rec, Window currentWindow) {
		super(rec.getReferenceName());
		this.rec = rec;
		referred = rec.getReferredClientTableLogic();
		cml = referred.getClientModelLogic();
		referring = rec.getReferringClientTableLogic();
		//this.fk = fk;
		//this.referredTable = referredTable;
		this.currentWindow = currentWindow;
		//referredTable.logText("Pointer editor set to "+fk+" <ctor>");
		//this.pool = pool;
		height = (int) (new JTextField().getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		setEditable(false);
		setLayout(new StringGridBagLayout());

		//JPanel jp = new JPanel();
	    add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,"+GdbappInsets.fieldInsets, view);
		state.setEnabled(true);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		view.setViewportView(message);
	}
	
	public void setReferredInstance(RowNode referredInstance) {
		this.referredInstance = referredInstance;
	}
	
	private void activateTableView() {
	}
	
	private void closeTableView() {
	}
	
	private void initializeTable() {
		if(table != null) {
			return;
		}
		table = new JTable();
		final ResultSetTableModel model = new ResultSetTableModel();
		model.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent arg0) {
			}
		});
		table.setModel(model);
		table.addMouseListener(new PopupListener()); 

		TableColumnModelListener tableColumnModelListener = new TableColumnModelListener() {
			public void columnAdded(TableColumnModelEvent e) {
			}

			public void columnMarginChanged(ChangeEvent e) {
			}

			public void columnMoved(TableColumnModelEvent e) {
				int from = e.getFromIndex();
				int to = e.getToIndex();
				rec.columnMoved(to);
				model.fireTableStructureChanged();
			}

			public void columnRemoved(TableColumnModelEvent e) {
			}

			public void columnSelectionChanged(ListSelectionEvent e) {
			}
		};

		TableColumnModel columnModel = table.getColumnModel();
		columnModel.addColumnModelListener(tableColumnModelListener);
		String rows = "rows";
		if(rec.getRowCountForTable() == 1) {
			rows = "row";
		}
		message.setText("Table contains "+rec.getRowCountForTable()+" "+rows);
	}
	
	private void stateButtonPressed() {
		expanded = !expanded;
		if(expanded) {
			if(table == null) {
				invalidate();
				initializeTable();
				validate();
			}
			state.setText("Close");
			view.setViewportView(table);
		}
		else {
			state.setText("Open");
			String rows = "rows";
			if(rec.getRowCountForTable() == 1) {
				rows = "row";
			}
			message.setText("Table contains "+rec.getRowCountForTable()+" "+rows);
			view.setViewportView(message);
		}
		repaint();
	}
	
	private void handleState() {
	}

	public int rowsUsed() {
		return 5;
	}

	public void setEditable(Boolean editable) {
	}

	public boolean isEditable() {
		return false;
	}

	public void setInitialValue(Object obj) throws Exception {
	}

	public void setValue(Object obj) throws Exception {
	}

	public Object getValue() throws Exception {
		return null;
	}

	public void setErrorMessage(String message) {
	}
	
	public boolean isCorrect() {
		return true;
	}

	private class ResultSetTableModel extends AbstractTableModel
	{  
		//Hashtable<Integer, scope> sht = new Hashtable<Integer, scope>();
		
		public ResultSetTableModel() {
		}
		
		public void clear() {
			//sht.clear(); 
			rec.rearrangeTableOrder();
			this.fireTableDataChanged();
		}
		
		public String getColumnName(int c) {
			String name = referred.getColumnName(c);//colnames.elementAt(c);
			return referred.translate(name);
		}

		public int getColumnCount() { 
			//int count = env.ths.getCreateStatement().getNoColumns();
			//System.out.println("ResultSetTableModel count = "+count);
			return referred.getColumnCount();
		}

		public Object getValueAt(int r, int c) {
			if(r == 0 && rec.getRowCountForTable() == 0) {
				return "";
			}
			return rec.getTableStringToPresent(r, c);
		}

		public int getRowCount() {
			int r = rec.getRowCountForTable();
			//System.out.println("ResultSetTableModel getRowCount() = "+r);
			return r == 0 ? 1 : r;
		}
	} 
	
	public Dimension getPreferredSize() {
		Dimension out = super.getPreferredSize();
		//System.out.println("ReferrerGraphicsEditor. Preferred size innan "+out);
		out.height = (expanded ? 14 : 4) * height;
		//System.out.println("ReferrerGraphicsEditor. Preferred size efter "+out);
		return out;
	}

	public Dimension getMaximumSize() {
		Dimension out = getPreferredSize();
		//System.out.println("ReferrerGraphicsEditor. Maximum size "+out);
		return out;
	}
	
	private ReferrerNode getReferrernodeOfThisReference() {
		for(int i = 0 ; i < referredInstance.getChildCount() ; i++) {
			TreeNode tn = referredInstance.getChildAt(i);
			if(!(tn instanceof ReferrerNode)) {
				System.out.println("Varv 1, fel typ. Var "+tn.getClass().getName());
				continue;
			}
			ReferrerNode refn = (ReferrerNode) tn;
			if(refn.getReferenceNode().referencesSame(rec)) {
				return refn;
			}
		}
		return null;
	}
	
	private RowNode getTablesRowNodeAndExpand(boolean expand) {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return null;
		}
		ClientDBObject referredDBObject = rec.getClientDBObjectAtPlaceForTable(rowno);
		if(referredDBObject == null) {
			return null;
		}
		/*
		DBPoolObject po = rec.getRowCountForTable().getNodeAtPlaceForTable(rowno);
		if(po == null) {
			return null;
		}
		*/
		
		if(expand) {
    		JTree tree = ((MainWindow) cml.getMainViewObject()).getTree();
    		TreePath tp = new TreePath(refn.getPath());
    		tree.setSelectionPath(tp);
    		tree.expandPath(tp);
    		tree.makeVisible(tp);
		}
		
		refn = getReferrernodeOfThisReference();

		for(int j = 0 ; j < refn.getChildCount() ; j++) {
			TreeNode tn = refn.getChildAt(j);
			//if(!(tn instanceof RowNode)) {
			//	System.out.println("ReferrerGraphicalEditor.getTablesRowNodeAndExpand: Verklig klass "+tn.getClass().getName());
			//}
			RowNode out = (RowNode) tn;
			if(!out.sameObject(referredDBObject)) { // ClientDBObject.sameObject
				System.out.println("Varv "+j+" inre, fel objekt. Var "+out+", egna är "+referredDBObject);
				continue;
			}
			
			if(expand) {
				TreePath tp = new TreePath(out.getPath());
	    		JTree tree = ((MainWindow) cml.getMainViewObject()).getTree();
	    		tree.setSelectionPath(tp);
	    		tree.expandPath(tp);
	    		tree.makeVisible(tp);
			}
    		return out;
		}

		/*
		//--- ReferrerNode refn = getReferrernodeOfThisReference();
		
		for(int i = 0 ; i < referredInstance.getChildCount() ; i++) {
			TreeNode tn = referredInstance.getChildAt(i);
			if(!(tn instanceof ReferrerNode)) {
				System.out.println("Varv 1, fel typ. Var "+tn.getClass().getName());
				continue;
			}
			ReferrerNode refn = (ReferrerNode) tn;
			
			if(!referredDBObject.sameObject(refn.getReferenceNode().getReferredobject())) {
				System.out.println("Varv "+i+" yttre, fel objekt. Var "+refn);
				continue;
			}
			
			if(expand) {
	    		JTree tree = ((MainWindow) cml.getMainViewObject()).getTree();
	    		TreePath tp = new TreePath(refn.getPath());
	    		tree.setSelectionPath(tp);
	    		tree.expandPath(tp);
	    		tree.makeVisible(tp);
			}
    		//PrimaryKey pk = po.getPrimaryKey();
			for(int j = 0 ; j < refn.getChildCount() ; j++) {
				tn = refn.getChildAt(j);
				//if(!(tn instanceof RowNode)) {
				//	System.out.println("ReferrerGraphicalEditor.getTablesRowNodeAndExpand: Verklig klass "+tn.getClass().getName());
				//}
				RowNode out = (RowNode) tn;
				if(!out.sameObject(referredDBObject)) { // ClientDBObject.sameObject
					System.out.println("Varv "+j+" inre, fel objekt. Var "+out+", egna är "+referredDBObject);
					continue;
				}
				
				if(expand) {
					TreePath tp = new TreePath(out.getPath());
		    		JTree tree = ((MainWindow) cml.getMainViewObject()).getTree();
		    		tree.setSelectionPath(tp);
		    		tree.expandPath(tp);
		    		tree.makeVisible(tp);
				}
	    		return out;
			}
		}
		*/
		return null;
	}

	public class PopupListener extends MouseAdapter 
	{

		public PopupListener()
		{
		}

		public void mouseClicked(MouseEvent e) {
	        if (e.getComponent().isEnabled() && e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
	        	//System.out.println("TableNode.PopupListener.mouse listener dubbelklick ###");
	        	RowNode selectedRowNode = getTablesRowNodeAndExpand(true);
	        }
	        
		}

		public void mousePressed(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) 
		{
			//System.out.println("TableNode.PopupListener.maybeShowPopup "+e.getComponent().getClass().getName());
			if(e.isPopupTrigger()) 
			{
				//System.out.println("TableNode.PopupListener.maybeShowPopup trigger -> "+e.getComponent().getClass().getName());
				if(e == null) return;
				JTable table= (JTable) e.getComponent();
				if(table == null) return;
				JPopupMenu popup = getTablePopup();
				if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}

	}
	
	private ReferrerNode getReferrerNode() {
		ClientDBObject cdbo = rec.getClientDBObject();
		for(int i = 0 ; i < referredInstance.getChildCount() ; i++) {
			TreeNode tn = referredInstance.getChildAt(i);
			if(tn instanceof ReferrerNode) {
				ReferrerNode refn = (ReferrerNode) tn;
				if(refn.getReferenceNode().getReferredobject().sameObject(cdbo)) {
					return refn;
				}
			}
		}
		return null;
	}

	public void onNewInTable() { 
		NewRowDialog nrd = null;;
		try { // ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, Window previousWindow
			nrd = new NewRowDialog(referred, null, rec.getReferenceName(), rec.getClientDBObject(), currentWindow);
		} catch (Exception e1) {
			UniversalTellUser.error(null, "Unable to create instance", "Internal error");
			referred.logTrace(e1);
			return;
		}
		nrd.setVisible(true);
		referredInstance.redefineChildNodes(); 
		DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) cml.getMainViewObject()).getTree().getModel();
		model.nodeStructureChanged(referredInstance);

		if(table != null) {
			((ResultSetTableModel) table.getModel()).clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}

		((MainWindow) cml.getMainViewObject()).invalidate();
		((MainWindow) cml.getMainViewObject()).validate();
		((MainWindow) cml.getMainViewObject()).repaint();
		if(nrd.getEditAfterNew()) {
			try {
				ClientDBObject other = nrd.getCreatedClientDBObject();
				ReferrerNode refn = getReferrerNode();
				if(refn != null) {
					//PrimaryKey pk = fk.assemblePrimaryKey();
					for(int i = 0 ; i < refn.getChildCount() ; i++) {
						RowNode child = (RowNode) refn.getChildAt(i);
						if(child.sameObject(other)) {
							child.editPressed(false);
	
							if(table != null) {
								((ResultSetTableModel) table.getModel()).clear();
								table.invalidate();
								table.validate();
								table.repaint();
							}
	
							((MainWindow) cml.getMainViewObject()).invalidate();
							((MainWindow) cml.getMainViewObject()).validate();
							((MainWindow) cml.getMainViewObject()).repaint();
						}
					}
				}
			} 
			catch (Exception e) {
				referred.log(e.getMessage(), e);
				//e.printStackTrace();
			}
		}
	}
	
	public void onEditInTable() {
    	RowNode selectedRowNode = getTablesRowNodeAndExpand(false);
    	//if(selectedRowNode == null) {
    	//	System.out.println("ReferrerGrahicalEditor.onEdit - Hittade ingen nod");
    	//}

    	selectedRowNode.editPressed(false);

		if(table != null) {
			((ResultSetTableModel) table.getModel()).clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}

		((MainWindow) cml.getMainViewObject()).invalidate();
		((MainWindow) cml.getMainViewObject()).validate();
		((MainWindow) cml.getMainViewObject()).repaint();
	}
	
	private void onDeleteInTable() {
		//System.out.println("ReferrerGrpahicalEditor.onDelete - 1");
    	RowNode selectedRowNode = getTablesRowNodeAndExpand(false);
    	if(selectedRowNode == null) {
    		System.out.println("ReferrerGrpahicalEditor.onDelete - Hittade ingen nod");
    	}
		//System.out.println("ReferrerGrpahicalEditor.onDelete - 2");
    	try {
    		selectedRowNode.delete();
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		//System.out.println("ReferrerGrpahicalEditor.onDelete - 3");
		referredInstance.redefineChildNodes(); 
		//System.out.println("ReferrerGrpahicalEditor.onDelete - 4");
		DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) cml.getMainViewObject()).getTree().getModel();
		model.nodeStructureChanged(referredInstance);
	}
	
	private void exploreFromTablesSelected() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = getTablesRowNodeAndExpand(false);
		try {
			ExplorerWindow ew = new ExplorerWindow(cml, null, rn);
			ew.setVisible(true);
		}
		catch(Exception e) {
			cml.log("Unable to start explorer", e);
			e.printStackTrace();
			UniversalTellUser.error(null, "Unable to start explorer", "Internal error");
		}
	}
 
	/*
	private int getFirstDatetimeColumnNumber() {
		TableRenderer tr = referredTable.getEmbeddedEnvironment().ths.getTableRenderer();
		for(int i = 0 ; i < tr.getNoColumns() ; i++) {
			String colname = tr.getColumnName(i);
			Column col = referredTable.getEmbeddedEnvironment().ths.getColumn(colname);
			if(col == null) {
				continue;
			}
			if(col.getType() instanceof DatetimeType) {
				return i;
			}
		}
		return -1;
	}
	*/
	/*
	private int[] getNumericColumnsNumbers() {
		Vector<Integer> outs = new Vector<Integer>();
		TableRenderer tr = referredTable.getEmbeddedEnvironment().ths.getTableRenderer();
		for(int i = 0 ; i < tr.getNoColumns() ; i++) {
			String colname = tr.getColumnName(i);
			Column col = referredTable.getEmbeddedEnvironment().ths.getColumn(colname);
			if(col == null) {
				continue;
			}
			if(col.getType() instanceof NumericType) {
				outs.add(i);
			}
		}
		int out[] = new int[outs.size()];
		for(int i = 0 ; i < outs.size(); i++) {
			out[i] = outs.elementAt(i);
		}
		return out;
	}
	*/

	public JPopupMenu getTablePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("New "+referred.translate(referred.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onNewInTable();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Edit this "+referred.translate(referred.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onEditInTable();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromTablesSelected();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onDeleteInTable();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Export to clipboard");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exportToClipboard();
			}
		}); 
		pm.add(menuItem);
		final int datetimeColumnNumber = referred.getFirstDatetimeColumnNumer();
		if(datetimeColumnNumber == -1) {
			return pm;
		}
		final int idxs[] = referred.getNumericColumnsNumbers();
		if((idxs == null) || (idxs.length == 0)) {
			return pm;
		}
		// We can auto generate some plots
		for(int i = 0 ; i < idxs.length ; i++) {
			//final TableRenderer tr = referredTable.getEmbeddedEnvironment().ths.getTableRenderer();
			menuItem = new JMenuItem("Plot "+referred.translate(referred.getColumnName(idxs[i])));
			final int x = i;
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					plot(datetimeColumnNumber, idxs[x], referred.translate(referred.getColumnName(idxs[x])));
				}
			});
			pm.add(menuItem);
		}
		return pm;
	} // getPossibleValueColumnIndexes

	private void plot(int datetimeColumnNumber, int idx, String name) {
		TimePlotDataContainer tp = new TimePlotDataContainer(name);
		//...
		ResultSetTableModel model = (ResultSetTableModel) table.getModel();
		int rowcount = model.getRowCount();
		for(int r = 0 ; r < rowcount ; r++) {
			String datetext = model.getValueAt(r, datetimeColumnNumber).toString();
			String valtext = model.getValueAt(r, idx).toString();
			long time = CommonSQLAssembler.getDateFromString(datetext).getTime();
			double value = Double.parseDouble(valtext);
			tp.add(time, value);
		}

		//...
		final JFrame frame = new JFrame(tp.getName());
		frame.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          frame.setVisible(false);
		          frame.dispose();
		        }
		      });
		frame.getContentPane().add(new TimePlotComponent(tp), BorderLayout.CENTER);
		try {
			frame.pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(((MainWindow) cml.getMainViewObject()), ie.getMessage());
			return;
		}
		frame.setSize(500,350);
		frame.setVisible(true);
	}
	

	private void exportToClipboard() {
		Clipboard.export((AbstractTableModel) table.getModel());
	}
	
	public String toString() {
		return "Referrer editor "+rec;
	}

	public boolean isTextual() {
		return rec.isTextual();
	}

}
