package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.UnmanageableEditorCommons;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.values.UnmanagedValue;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class UnmanageableTypeEditor extends GraphicalEditor {

	private JTextField tf = new JTextField("Not manageable");
	private JButton state = new JButton("?");
	private StateDot dot = null;
	private int height = -1;
	private UnmanageableEditorCommons uec = null;

	public UnmanageableTypeEditor(UnmanageableEditorCommons uec) {
		super(uec.getName());
		height = (int) (tf.getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		dot.setState(true);
		setEditable(false);
		tf.setEditable(false);
		setLayout(new StringGridBagLayout());
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,insets=[12,12,0,0]", tf);
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
	}
	
	private void stateButtonPressed() {
		//System.out.println("IntGraphiclaEditor stateButtonPresed");
		UniversalTellUser.info(this, "Value "+uec.getName()+" is of unmanageable type "+uec.getTypename()+". No handling available.");
	}

	public Object getValue() throws Exception {
		return new UnmanagedValue(uec.getName());
	}

	public boolean isCorrect() {
		return true;
	}

	public int rowsUsed() {
		return 0;
	}

	public void setEditable(Boolean editable) {
	}
	
	public boolean isEditable() {
		return false;
	}

	public boolean getEditable() {
		return false;
	}

	public void setErrorMessage(String message) {
	}

	public void setInitialValue(Object obj) throws Exception {
	}

	public void setValue(Object obj) throws Exception {
	}

	public String toString() {
		return "Unmanageable type editor ";
	}
	
	public boolean isTextual() {
		return uec.isTextual();
	}


}
