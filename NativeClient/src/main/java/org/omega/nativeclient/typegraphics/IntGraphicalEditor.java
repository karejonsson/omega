package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.Double;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.IntegerEditorCommons;
import org.omega.clientcommon.editors.DoubleEditorCommons;
import org.omega.clientcommon.editors.EditorCommons;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class IntGraphicalEditor extends GraphicalEditor {
	
	private JTextField tf = new JTextField();
	private JButton state = new JButton("?");
	private StateDot dot = null;
	private int height = -1;
	private IntegerEditorCommons iec = null;
	
	public IntGraphicalEditor(IntegerEditorCommons iec) throws Exception {
		super(iec.getName());
		this.iec = iec;
		height = (int) (tf.getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		tf.setEditable(iec.isEditable());
		setLayout(new StringGridBagLayout());
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,insets=[12,12,0,0]", tf);
		tf.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				try {
					if(iec != null) {
						iec.setValue(iec.getIntegerFromString(tf.getText()));
					}
				} catch (Exception e1) {
					iec.logTrace(e1);
				}
				iec.edited();
				edited();
				handleState();
			}

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				iec.edited();
				edited();
			}
		});
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		iec.setUpdaterOfVisibleField(new EditorCommons.UpdateVisibleField() {
			public boolean isEditable() {
				return iec.isEditable();
			}
			public void setText(String text) {
				tf.setText(text);
			}
		});
	}
	
	private void stateButtonPressed() {
		UniversalTellUser.error(this, "Integer "+iec.errorStateDescription());
	}
	
	private void handleState() {
		//System.out.println("IntGraphiclaEditor stateButtonPresed");
		String text = tf.getText();
		if((text == null) || (text.trim().length() == 0)) {
			dot.setState(true);
			state.setEnabled(false);
			iec.setErrorMessage(null);
			iec.setNulled(true);
			tf.setText("");
			return;
		}
		try {
			Integer i = iec.getIntegerFromString(text.trim());
			dot.setState(i != null);
			state.setEnabled(i == null);
			iec.setErrorMessage(null);
		}
		catch(Exception e) {
			dot.setState(false);
			state.setEnabled(true);
			iec.setErrorMessage(e.getMessage());
		}
	}
	
	public int rowsUsed() {
		return 1;
	}

	public void setEditable(Boolean editable) {
		tf.setEditable(editable);
	}

	public boolean isEditable() {
		return tf.isEditable();
	}

	public void setInitialValue(Object obj) throws Exception {
		iec.setNulled(obj == null);
		if(obj == null) {
			iec.setValue(0);
			tf.setText("");
			return;
		}
		if(!(obj instanceof Integer) && !(obj instanceof Long)) {
			throw new Exception("Integer field set to type "+obj.getClass().getName());
		}
		iec.setValue(obj);
		tf.setText(iec.getStringFromObject(iec.getValue()));
	}

	public void setValue(Object obj) throws Exception {
		iec.edited();
		super.setValue(obj);
		handleState();
	}
	
	public Object getValue() throws Exception {
		if(!iec.isEdited()) {
			return iec.getValue();
		}
		String text = tf.getText();
		try {
			return iec.getIntegerFromString(text);
		}
		catch(Exception e) {
			if(text == null) {
				return null;
			}
			if(text.trim().length() == 0) {
				return null;
			}
		}
		throw new Exception("The value "+text+" cannot be interpreted as an integer. Happened to field "+iec.getName());
	}

	public void setErrorMessage(String message) {
		iec.setErrorMessage(message);
		state.setEnabled(true);
		tf.setText("");
		dot.setState(false);
	}
	
	public boolean isCorrect() {
		return state.isEnabled() || iec.isNulled();
	}
	
	public String toString() {
		return "Integer editor "+tf.getText();
	}

	public boolean isTextual() {
		return iec.isTextual();
	}

}
