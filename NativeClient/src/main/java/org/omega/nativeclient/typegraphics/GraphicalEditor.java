package org.omega.nativeclient.typegraphics;

import javax.swing.JPanel;

import org.omega.nativeclient.treeview.RowNode;

public abstract class GraphicalEditor extends JPanel {
	
	protected String name;
	private Runnable listener = null;
	
	public GraphicalEditor(String name) { 
		this.name = name;
	}

	protected void edited() {
		if(listener != null) {
			listener.run();
		}
	}
	
	public String getName() {
		return name;
	}
	
	abstract public int rowsUsed();
	abstract public boolean isEditable();
	abstract public boolean isTextual();
	abstract public void setInitialValue(Object obj) throws Exception;

	public void setValue(Object obj) throws Exception {
		setInitialValue(obj);
		edited();
	}
	
	abstract public Object getValue() throws Exception;
	abstract public void setErrorMessage(String message);
	abstract public boolean isCorrect();
	
	public void setNotifier(Runnable r) {
		listener = r;
	}
	 
	/*
	public String getName() {
		return name;
	}
	
	public boolean isPointer() {
		return false;
	}
	
	public void setPointersReferredInstance(RowNode rn) {
		this.rn = rn;
	}
	
	public String toString() {
		return "Is pointer "+isPointer();
	}
	*/
}
