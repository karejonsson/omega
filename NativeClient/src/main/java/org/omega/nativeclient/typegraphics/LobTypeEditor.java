package org.omega.nativeclient.typegraphics;

import javax.swing.JTextField;

import org.omega.clientcommon.editors.LobEditorCommons;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.values.LobValue;

import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.util.FilesAndBytes;

public class LobTypeEditor extends GraphicalEditor {

	private JTextField tf = new JTextField("Lob field. Not checked if entry is set.");
	private LobEditorCommons lec = null;
	
	public LobTypeEditor(LobEditorCommons lec) throws Exception {
		super(lec.getName());
		this.lec = lec;
		setEditable(false);
		tf.setEditable(false);
		setLayout(new StringGridBagLayout());
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,insets=[12,12,0,0]", tf);
		clear();
		Object cached = lec.getCachedValue();
		if(cached != null) {
			//System.out.println("LobTypeEditor.<ctor> fann chachat värde "+cached);
			lec.setValueUntyped(cached);
			setInitialValue(cached);
		}
	}
	
	public LobEditorCommons getLobEditorCommons() {
		return lec;
	}
	
	public boolean isUnchanged() {
		return lec.isUnchanged();
	}

	public boolean isNulled() {
		return lec.isNulled();
	}

	public boolean isCheckedWithDatabase() {
		return lec.isCheckedWithDatabase();
	}
	
	public void setCheckedWithDatabase(boolean state) {
		lec.setCheckedWithDatabase(state);
	}
	
	public void clear() {
		/*
		System.out.println("LobTypeEditor.clear() --------------------------------------------");
		
		(new Throwable()).printStackTrace();
		System.out.println("LobTypeEditor.clear() 2 -------------------------------------------- 2");
		*/
		lec.setValue(new LobValue());
		lec.setEdited(false);
		lec.setCheckedWithDatabase(false);
		setText();
	}
	
	public String state() {
		return "checkedWithDatabase "+lec.isCheckedWithDatabase()+", isNulled "+lec.isNulled()+", isEdited "+lec.isEdited()+", value "+(lec.getValue() == null ? "null" : lec.getValue().state());
	}
	
	public LobValue getValue() {
		return lec.getValue();
	}

	public boolean isCorrect() {
		return true;
	}

	public int rowsUsed() {
		return 1;
	}

	public void setEditable(Boolean editable) {
	}
	
	public boolean isEditable() {
		return false;
	}

	public boolean getEditable() {
		return false;
	}

	public void setErrorMessage(String message) {
	}
	
	public void setText() {
		//System.out.println("LobTypeEditor.setText() - 1"); 
		if(lec.getValue() == null) {
			tf.setText("?");
		}
		else {
			byte[] filename = lec.getValue().getFilename();
			String text = "Meta data not read";
			if(lec.isNulled()) {
				//System.out.println("LobTypeEditor.setText() - is nulled"); 
				text = "null";
			}
			if(filename != null) {
				//System.out.println("LobTypeEditor.setText() - filename ej null"); 
				text = new String(filename)+" size "+lec.getValue().getLength();
			}
			if(lec.getValue().isDatabaseBased()) {
				//System.out.println("LobTypeEditor.setText() - databasbaserad"); 
				if(lec.isCheckedWithDatabase()) {
					//System.out.println("LobTypeEditor.setText() - kontrollerad med DB"); 
					tf.setText(text+", updated");
				}
				else {
					//System.out.println("LobTypeEditor.setText() - INTE kontrollerad med DB"); 
					tf.setText("Meta data not verified");
				}
			}
			else {
				//System.out.println("LobTypeEditor.setText() - ej uppdaterad"); 
				tf.setText(text+", not updated");
			}
		}
	}
	
	public void setInitialValue(byte[] meta, boolean databaseBased) throws Exception {
		String filename = LobEditorCommons.getFilenameFromMeta(meta);
		//System.out.println("LobTypeEditor.setInitialValue() filename = "+filename+", meta length "+(meta != null ? meta.length : ">NULL>"));
		int length = FilesAndBytes.getFilesizeFromMeta(meta);
		LobValue lv = new LobValue();
		lv.setLength(length);
		lv.setFilename(filename.getBytes());
		lv.setDatabaseBased(databaseBased);
		setInitialValue(lv);
		lec.setLobValue(getName(), lv); 
	}
	
	public void setInitialValue(Object obj) throws Exception {
		if(obj == null) {
			lec.setValue(null);
			lec.setNulled(true);
			setText();
			return;
		}
		if(!(obj instanceof LobValue)) {
			throw new Exception("Lob field is set to non Omega-Lob value. Type was "+((LobType) obj).getTypename()+", class "+obj.getClass().getName());
		}
		lec.setValue((LobValue) obj);
		lec.setNulled(lec.getValue().getFilename() == null);
		setText();
	}

	public void setValue(Object obj) throws Exception {
		//System.out.println("LobTypeEditor.setValue() obj = "+obj);
		lec.setEdited(true);
		super.setValue(obj);
	}

	public String toString() {
		return "Lob type editor ";
	}
	
	public boolean isTextual() {
		return lec.isTextual();
	}

}
