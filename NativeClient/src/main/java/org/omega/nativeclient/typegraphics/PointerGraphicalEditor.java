package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.EditorCommons;
import org.omega.clientcommon.editors.PointerEditorCommons;
import org.omega.clientcommon.hierarchy.DataNode;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class PointerGraphicalEditor extends GraphicalEditor {

	private static final long serialVersionUID = 1L;
	private JLabel lab = new JLabel();
	//private ForeignKey fk;
	private JButton state = new JButton("?");
	private StateDot dot = null; 
	private int height = -1;
	private PointerEditorCommons pec = null;

	public PointerGraphicalEditor(PointerEditorCommons pec) throws Exception {
		super(pec.getName()); 
		this.pec = pec;
		pec.logText("Pointer editor set to "+pec+" <ctor>");
		
		/*
		// Likley to move the following into PointerEditorCommons
		ImageIconByteArrayHolder rawNullIconByteArray = pec.getNullIconByteArrayHolder();//env.hs.getDBTableHandlingSpecification(fk.getTablenamePointingTo()).getImageIcon(DBTableHandlingSpecification.nullPointerIcon_I);
		System.out.println("PointerGraphicalEditor.<ctor> "+rawNullIconByteArray);
		IconHelper.updateCacheWithNullPointerImage(pec.getClientTableLogic());
		ImageIconByteArrayHolder fixedNullIconByteArray = IconHelper.createAndInsertImageIcon(pec.getClientTableLogic(), pec.getNameOfTablePointedAt(), size, rawNullIconByteArray != null ? rawNullIconByteArray.getBytes() : null);
		pec.setNullIcon(fixedNullIconByteArray);
		*/
		//setEditable(false);
		lab.setIcon(pec.getIcon(size));

		setLayout(new StringGridBagLayout());
		height = (int) (new JTextField().getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,"+GdbappInsets.fieldInsets, lab);
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		pec.setUpdaterOfVisibleField(new EditorCommons.UpdateVisibleField() {
			public boolean isEditable() {
				return false;
			}
			public void setText(String text) {
				lab.setText(text);
			}
		});
		try {
			setInitialValue(pec.getValue());
			pec.init();
		}
		catch(Exception e) {
			pec.logText("Unable to set pointers value initial to edit.");
		}
	}
	
	public void setInitialValue(Object obj) throws Exception {
		//System.out.println("PointerGraphicalEditor.setInitialValue("+obj+") - name "+getName());
		//(new Throwable()).printStackTrace();
		try {
			//System.out.println("PointerGraphicalEditor.setInitialValue("+obj+") innan, pekar på : po="+po);
			//new Throwable().printStackTrace();
			//if(obj == null) {
				//pec.setValue((ClientDBObject) null);
				//fk.clear();
			//}
			if(obj != null && !(obj instanceof ClientDBObject)) {
				throw new Exception("Pointer field set to type "+obj.getClass());
			}
			/*
			ClientDBObject cdbo = (ClientDBObject) obj;
			ForeignKey fkobj = ForeignKey.assembleForeignKey(cdbo.getPrimaryKey(), fkd);
			pec.setNulled(obj == null);
			if(obj != null && !(obj instanceof ForeignKey)) {
				String message = "Pointer field set to type "+obj.getClass().getName();
				pec.logText(message);
				throw new Exception(message);
			}
			*/
			//ForeignKey fkobj = (ForeignKey) obj;
			lab.setIcon(pec.getIcon(size));
			/*
			if(pec.hasCompleteSetOfValues()) { 
				lab.setIcon(new ImageIcon(pec.getTableIcon().getBytes()));
			}
			else {
				lab.setIcon(new ImageIcon(pec.getNullIcon().getBytes()));
			}
			*/
			//lab.setIcon(icon);
			pec.setValue((ClientDBObject) obj);
			//if(obj == null) {
				//setText();
				//return;
			//}
			//fk = (ForeignKey) obj;
			//po = pool.getPoolObject(fk.assemblePrimaryKey());
			//po.setForeignKey(fk);
			//System.out.println("PointerGraphicalEditor.setInitialValue("+obj+") efter, pekar nu på : po="+po);
			setText();
		}
		catch(Exception e) {
			String message = "Pointer editor set text failed :"+e.getMessage();
			pec.log(message, e);
			throw e;
		}
	}
	
	public String getTableNamePointingFrom() {
		return pec.getClientTableLogic().getTableName();
	}

	private void stateButtonPressed() {
		UniversalTellUser.error(this, "Pointer not correct.\n"+pec.errorStateDescription());
	}

	public int rowsUsed() {
		return 1;
	}

	public boolean isEditable() {
		return pec.isEditable();
	}

	private static final int size = 26;

	protected void edited() {
		super.edited();
	}

	public void setValue(Object value) throws Exception {
		if(value instanceof DataNode) {
			value = ((DataNode) value).getClientDBObject();
		}
		if(value != null && !(value instanceof ClientDBObject)) {
			throw new Exception("Pointer field set to type "+value.getClass());
		}
		//System.out.println("PointerGraphicalEditor.setValue("+value+")");
		pec.logText("Pointer editor set to "+value+" <setValue>");
		pec.setValue((ClientDBObject) value); 
		super.setValue(value);
		setText();
	}

	public Object getValue() throws Exception {
		//System.out.println("PointerGraphicalEditor.getValue() -> "+fk);
		return pec.getValue();
	}
	
	public void setErrorMessage(String message) {
		pec.setErrorMessage(message);
		state.setEnabled(true);
		lab.setText("");
		dot.setState(false);
	}

	public boolean isCorrect() {
		return state.isEnabled() || pec.isNulled();
	}

	public boolean isPointer() {
		return true;
	}


	private void setText() {
		pec.setText();
	}
	
	/*
	public ForeignKey getForeignKey() {
		return fk;
	}
	*/
	
	public String toString() {
		return pec.toString();
	}
	
	public ClientDBObject getPoolObject() {
		return pec.getClientDBObject();
	}

	public boolean isTextual() {
		return pec.isTextual();
	}

}
