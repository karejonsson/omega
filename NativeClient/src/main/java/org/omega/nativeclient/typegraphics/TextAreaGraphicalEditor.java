package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.String;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.TextareaEditorCommons;
import org.omega.clientcommon.editors.EditorCommons;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class TextAreaGraphicalEditor extends GraphicalEditor {

	private JTextArea ta = new JTextArea();
	private JButton state = new JButton("?");
	private StateDot dot = null;
	private int height = -1;
	private TextareaEditorCommons taec = null;
	
	public TextAreaGraphicalEditor(TextareaEditorCommons taec) throws Exception {
		super(taec.getName());
		this.taec = taec;
		height = (int) (new JTextField().getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		setLayout(new StringGridBagLayout());
		final JScrollPane view = new JScrollPane();
		JPanel jp = new JPanel();
		view.getViewport().add(jp);
	    add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,"+GdbappInsets.fieldInsets, view);
		ta.setEditable(taec.isEditable());
		jp.setLayout(new StringGridBagLayout());
		jp.add("gridx=0,gridy=0,fill=BOTH,insets=[0,0,0,0]", ta);
		ta.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				try {
					if(taec != null) {
						taec.setValue(ta.getText());
					}
				} catch (Exception e1) {
					taec.logTrace(e1);
				}
				taec.edited();
				edited();
				handleState();
			}

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				taec.edited();
				edited();
			}
		});
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		taec.setUpdaterOfVisibleField(new EditorCommons.UpdateVisibleField() {
			public boolean isEditable() {
				return taec.isEditable();
			}
			public void setText(String text) {
				ta.setText(text);
			}
		});
	}
	
	private void stateButtonPressed() {
		UniversalTellUser.error(this, "Textarea "+taec.errorStateDescription());
	}
	
	private void handleState() {
		String text = ta.getText();
		if((text == null) || (text.trim().length() == 0)) {
			dot.setState(true);
			state.setEnabled(false);
			taec.setErrorMessage(null);
			taec.setNulled(true);
			ta.setText("");
			return;
		}
		if(text.length() > taec.getMaxlen()) {
			taec.setErrorMessage("Text may not contain more than "+taec.getMaxlen()+" characters in this field.");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("\"")) {
			taec.setErrorMessage("Text may not contain the character \"");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("'")) {
			taec.setErrorMessage("Text may not contain the character '");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("`")) {
			taec.setErrorMessage("Text may not contain the character `");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		dot.setState(true);
		state.setEnabled(false);
		taec.setErrorMessage(null);
	}

	public int rowsUsed() {
		return 4;
	}
	
	public void setEditable(Boolean editable) {
		ta.setEditable(editable);
	}

	public boolean isEditable() {
		return ta.isEditable();
	}

	public void setInitialValue(Object obj) throws Exception {
		//System.out.println("TextAreaGraphicalEditor.setInitialValue("+obj+")");
		taec.setNulled(obj == null);
		if(obj == null) {
			taec.setValue(null);
			ta.setText("");
			return;
		}
		if(!(obj instanceof String)) {
			throw new Exception("TextField field set to type "+obj.getClass().getName());
		}
		taec.setValue((String) obj);
		ta.setText(obj.toString());
	}

	public void setValue(Object obj) throws Exception {
		//System.out.println("TextAreaGraphicalEditor.setValue("+obj+")");
		taec.edited();
		super.setValue(obj);
		handleState();
	}

	public Object getValue() throws Exception {
		if(!taec.isEdited()) {
			return ta.getText();
		}
		return taec.getValue();
	}

	public void setErrorMessage(String message) {
		taec.setErrorMessage(message);
		state.setEnabled(true);
		ta.setText("");
		dot.setState(false);
	}

	public Dimension getMinimumSize() {
		return new Dimension(100,100);
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(100,100);
	}

	public boolean isCorrect() {
		return state.isEnabled() || taec.isNulled();
	}

	public String toString() {
		return "Textarea editor "+ta.getText();
	}

	public boolean isTextual() {
		return taec.isTextual();
	}

}
