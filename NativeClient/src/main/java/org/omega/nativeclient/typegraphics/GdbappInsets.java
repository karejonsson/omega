package org.omega.nativeclient.typegraphics;

public class GdbappInsets {
	
	public static final String insets = "insets=[0,12,0,0]";
	public static final String noinsets = "insets=[0,0,0,0]";
	public static final String fieldInsets = "insets=[12,12,0,0]";
	public static final String labelInsets = "insets=[12,12,0,0]";
	
}
