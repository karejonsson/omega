package org.omega.nativeclient.typegraphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class StateDot extends JPanel {

	private boolean state = true;
	private Dimension dim = null;

	public StateDot(Dimension dim) {
		this.dim = dim;
        setOpaque(false); // we don't paint all our bits
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createLineBorder(Color.black));
	}

	public void setState(boolean state) {
		this.state = state;
		repaint();
	}

	public boolean getState() {
		return state;
	}
	
    protected void paintComponent(Graphics g) {
		Dimension d = getSize();
		if(state) {
            g.setColor(Color.green);
            g.fillOval(0, 0, d.width-1, d.height-1);
		}
		else {
            g.setColor(Color.red);
            g.fillOval(0, 0, d.width-1, d.height-1);
		}
    }

	public Dimension getMinimumSize() {
		return dim;
	}
	
	public Dimension getPreferredSize() {
		return dim;
	}

}
