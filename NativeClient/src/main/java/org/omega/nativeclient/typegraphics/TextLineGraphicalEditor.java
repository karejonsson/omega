package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.String;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.TextlineEditorCommons;
import org.omega.clientcommon.editors.EditorCommons;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class TextLineGraphicalEditor extends GraphicalEditor {

	private JTextField tf = new JTextField();
	private JButton state = new JButton("?");
	private StateDot dot = null;
	private int height = -1;
	private TextlineEditorCommons tlec = null;
	
	public TextLineGraphicalEditor(TextlineEditorCommons tlec) throws Exception {
		super(tlec.getName());
		this.tlec = tlec;
		height = (int) (tf.getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		tf.setEditable(tlec.isEditable());
		setLayout(new StringGridBagLayout());
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,"+GdbappInsets.fieldInsets, tf);
		tf.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				try {
					if(tlec != null) {
						tlec.setValue(tf.getText());
					}
				} catch (Exception e1) {
					tlec.logTrace(e1);
				}
				tlec.edited();
				edited();
				handleState();
			}

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				tlec.edited();
				edited();
			}
		});
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		tlec.setUpdaterOfVisibleField(new EditorCommons.UpdateVisibleField() {
			public boolean isEditable() {
				return tlec.isEditable();
			}
			public void setText(String text) {
				tf.setText(text);
			}
		});
	}
	
	private void stateButtonPressed() {
		UniversalTellUser.error(this, "Textline "+tlec.errorStateDescription());
	}
	
	private void handleState() {
		String text = tf.getText();
		if((text == null) || (text.trim().length() == 0)) {
			dot.setState(true);
			state.setEnabled(false);
			tlec.setErrorMessage(null);
			tlec.setNulled(true);
			tf.setText("");
			return;
		}
		if(text.length() > tlec.getMaxlen()) {
			tlec.setErrorMessage("Text may not contain more than "+tlec.getMaxlen()+" characters in this field.");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("\"")) {
			tlec.setErrorMessage("Text may not contain the character \"");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("'")) {
			tlec.setErrorMessage("Text may not contain the character '");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		if(text.contains("`")) {
			tlec.setErrorMessage("Text may not contain the character `");
			dot.setState(false);
			state.setEnabled(true);
			return;
		}
		dot.setState(true);
		state.setEnabled(false);
		tlec.setErrorMessage(null);
	}
	
	public int rowsUsed() {
		return 1;
	}

	public void setEditable(Boolean editable) {
		tf.setEditable(editable);
	}

	public boolean isEditable() {
		return tf.isEditable();
	}

	public void setInitialValue(Object obj) throws Exception{
		tlec.setNulled(obj == null);
		if(obj == null) {
			tlec.setValue(null);
			tf.setText("");
			return;
		}
		if(!(obj instanceof String)) {
			throw new Exception("TextField field set to type "+obj.getClass().getName());
		}
		tlec.setValue((String) obj);
		tf.setText(obj.toString());
	}

	public void setValue(Object obj) throws Exception {
		tlec.edited();
		super.setValue(obj);
		handleState();
	}

	public Object getValue() throws Exception {
		if(!tlec.isEdited()) {
			return tf.getText();
		}
		return tlec.getValue();
	}

	public void setErrorMessage(String message) {
		tlec.setErrorMessage(message);
		state.setEnabled(true);
		tf.setText("");
		dot.setState(false);
	}

	public boolean isCorrect() {
		return state.isEnabled() || tlec.isNulled();
	}

	public String toString() {
		return "Textline editor "+tf.getText();
	}

	public boolean isTextual() {
		return tlec.isTextual();
	}

}
