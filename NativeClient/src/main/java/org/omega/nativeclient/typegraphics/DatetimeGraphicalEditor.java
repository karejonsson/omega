package org.omega.nativeclient.typegraphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.omega.clientcommon.editors.DatetimeEditorCommons;
import org.omega.clientcommon.editors.EditorCommons;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class DatetimeGraphicalEditor extends GraphicalEditor {

	private JTextField tf = new JTextField();
	private JButton state = new JButton("?");
	private StateDot dot = null;
	private int height = -1;
	private DatetimeEditorCommons dec = null;

	public DatetimeGraphicalEditor(DatetimeEditorCommons dec) throws Exception {
		super(dec.getName());
		this.dec = dec;
		height = (int) (tf.getMinimumSize().height / 2);
		dot = new StateDot(new Dimension(height, height));
		tf.setEditable(dec.isEditable());
		setLayout(new StringGridBagLayout());
		add("gridx=0,gridy=0,fill=BOTH,weightx=1.0,insets=[12,12,0,0]", tf);
		tf.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				try {
					if(dec != null) {
						dec.setValue(dec.getDateFromString(tf.getText()));
					}
				} catch (Exception e1) {
					dec.logTrace(e1);
				}
				dec.edited();
				edited();
				handleState();
			}

			public void keyTyped(KeyEvent e) {
			}

			public void keyPressed(KeyEvent e) {
				dec.edited();
				edited();
			}
		});
		state.setEnabled(false);
		add("gridx=1,gridy=0,anchor=EAST,insets=[12,12,0,0]", state);
		add("gridx=2,gridy=0,anchor=EAST,insets=["+height+","+height+",0,0]", dot);
		state.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stateButtonPressed();
			}
		});
		dec.setUpdaterOfVisibleField(new EditorCommons.UpdateVisibleField() {
			public boolean isEditable() {
				return dec.isEditable();
			}
			public void setText(String text) {
				tf.setText(text);
			}
		});
	}
	
	private void stateButtonPressed() {
		UniversalTellUser.error(this, "Datetime "+dec.errorStateDescription());
	}
	
	private void handleState() {
		//System.out.println("IntGraphiclaEditor stateButtonPresed");
		String text = tf.getText();
		if((text == null) || (text.trim().length() == 0)) {
			dot.setState(true);
			state.setEnabled(false);
			dec.setErrorMessage(null);
			dec.setNulled(true);
			tf.setText("");
			return;
		}
		try {
			Date d = dec.getDateFromString(text.trim());
			dot.setState(d != null);
			state.setEnabled(d == null);
			dec.setErrorMessage(null);
		}
		catch(Exception e) {
			dot.setState(false);
			state.setEnabled(true);
			dec.setErrorMessage(e.getMessage());
		}
	}
	
	public int rowsUsed() {
		return 1;
	}

	public boolean isEditable() {
		return tf.isEditable();
	}

	public void setInitialValue(Object obj) throws Exception {
		dec.setNulled(obj == null);
		if(obj == null) {
			dec.setValue(null);
			tf.setText("");
			return;
		}
		if(!(obj instanceof Date)) {
			throw new Exception("Datetime field set to type "+obj.getClass().getName());
		}
		dec.setValue((Date) obj);
		if(obj == null) {
			return;
		}
		tf.setText(dec.getStringFromDate(dec.getValue()));
	}

	public void setValue(Object obj) throws Exception {
		dec.edited();
		super.setValue(obj);
		handleState();
	}

	public Object getValue() throws Exception {
		if(!dec.isEdited()) {
			return dec.getValue();
		}
		String text = tf.getText();
		try {
			return dec.getDateFromString(tf.getText());
		}
		catch(Exception e) {
			if(text == null) {
				return null;
			}
			if(text.trim().length() == 0) {
				return null;
			}
		}
		throw new Exception("The value "+text+" cannot be interpreted as a double. Happened to field "+dec.getName());
	}
	
	public void setErrorMessage(String message) {
		dec.setErrorMessage(message);
		state.setEnabled(true);
		tf.setText("");
		dot.setState(false);
	}

	public boolean isCorrect() {
		return state.isEnabled() || dec.isNulled();
	}
	
	public String toString() {
		return "Datetime editor "+tf.getText();
	}

	public boolean isTextual() {
		return dec.isTextual();
	}

}