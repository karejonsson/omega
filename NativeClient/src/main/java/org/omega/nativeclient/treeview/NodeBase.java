package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Window;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;

import org.omega.clientcommon.editors.ClientModelLogic;

public abstract class NodeBase extends DefaultMutableTreeNode {

	protected boolean areChildrenDefined = false;

	public NodeBase() {
	}

	abstract public Component getRightSide();

	abstract public void gotFocus();

	public JPopupMenu getTreePopup() { return null; };
	
	public void refresh() {};
	
	public void about() {};
	
	abstract public NodeBase cloneForOtherWindow(Window window);
	
	abstract public Icon getImageIcon(int size, int usedImageBufferType);

	abstract public ClientModelLogic getClientModelLogic();
}
