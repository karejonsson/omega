package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.clientcommon.hierarchy.MetaNode;
import org.omega.configuration.model.TableConfiguration;

public class MetaTableNode extends NodeBase {

	private MetaNode mn = null;

	public MetaTableNode(MetaNode mn) {
		this.mn = mn;
	}

	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}
	
	public ClientTableLogic getClientTableLogic() {
		return mn.getClientTableLogic();
	}

	/*
	public TableConfiguration getTableHandlingSpecification() {
		return ths;
	}
	*/
	
	public Component getRightSide() {
		return new JTable(new ResultSetTableModel());
		//return new JLabel(ths.getName());
	}

	public void gotFocus() {
	}

	/*
	public DBPool getPool() {
		return null;
	}
	*/
	/*
	public NativeClientModelHandling getDetails() {
		return null;
	}
	*/
	
	public String getName() {
		return mn.getTableName();
	}

	public String toString() {
		return mn.toString();
	}

	public int getChildCount() {
		if(!areChildrenDefined) {
			defineChildNodes();
		}
		return(super.getChildCount());
	}

	private void defineChildNodes() {
		areChildrenDefined = true;
		List<MetaNode> children = mn.getChildren();
		for(MetaNode child : children) {
			add(new MetaTableNode(child));			
		}
	}

	private class ResultSetTableModel extends AbstractTableModel
	{  
		private static final long serialVersionUID = 1L;

		public ResultSetTableModel() {
		}

		public void clear() {
		}

		public String getColumnName(int c) { 
			if(c == 0) {
				return "Name";
			}
			if(c == 1) {
				return "Type";
			}
			return "Default";
		}

		public int getColumnCount() { 
			return 3;
		}

		public Object getValueAt(int r, int c) {
			return mn.getValueAt(r, c);
		}

		public int getRowCount() {
			return mn.getRowCount();
		}
	}

	public Icon getImageIcon(final int size, final int usedImageBufferType) {
		return mn.getClientTableLogic().getMetaTableNodeImageIcon(size, usedImageBufferType);
	}

	public ClientModelLogic getClientModelLogic() {
		return mn.getClientTableLogic().getClientModelLogic();
	}

}
