package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;

import se.modlab.generics.bshro.ifc.HierarchyLeaf;

public class NullPointerNode extends NodeBase {
	
	private ClientTableLogic ctl;
	private String fieldnamePresentable = null;
	
	public NullPointerNode(ClientTableLogic ctl, String fieldnamePresentable) {
		this.ctl = ctl;
		this.fieldnamePresentable = fieldnamePresentable;
	}
	
	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}
	
	public ClientTableLogic getClientTableLogic() {
		return ctl;
	}

	public ClientModelLogic getClientModelLogic() {
		return ctl.getClientModelLogic();
	}
	
	public Component getRightSide() {
		return null;
	}

	public void gotFocus() {
	}

	public String toString() {
		return fieldnamePresentable+" (Not set)";
	}
	
	public String getName() {
		return ctl.getTableName();
	}
	
	private String getMetaDescription() {
		return ctl.getMetaDescription();
	}

	private void showMeta() {
		Object[] options = { "OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) ctl.getClientModelLogic().getMainViewObject()), 
				getMetaDescription(), 
				"SQL Create table",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
	}

	public JPopupMenu getTreePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showMeta();
			}
		});
		pm.add(menuItem);
		return pm;
	}

	public Icon getImageIcon(int size, int usedImageBufferType) {
		return ctl.getNullPointerNodeImageIcon(size, usedImageBufferType);
	}

}
