package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreePath;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.connectivity.helpers.GeneralComparator;

import se.modlab.generics.util.Sort;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
 
public class ChoseFromFullTableDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private ClientTableLogic env = null; 
	//private DBPool pool = null;
	private String sqlmessage = null;
	private Vector<RowNode> rows = new Vector<RowNode>();
	private JTable table = null;
	private JScrollPane sp = new JScrollPane();
	private RowNode selectedRow = null;
	private boolean nullPressed = false;

	private int sortcolumn = -1;
	private boolean sortInverted = false;
	private Vector<RowNode> tableRowsOrder = null;//new Vector<RowNode>();

	public ChoseFromFullTableDialog(JDialog parent, ClientTableLogic env, String title) {
		super(parent, title, true);
		this.env = env;
		//this.pool = pool;
	    addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent windowevent) {
	          setVisible(false);
	          dispose();
	        }
	      });
		getContentPane().add(sp);
		JPanel jp = new JPanel();
		sp.getViewport().add(jp);
		defineChildNodes();
		if(table != null) {
		    table.addMouseListener(new PopupListener());   
			jp.setLayout(new BorderLayout()); // unless already there
			JScrollPane sp = new JScrollPane();
			sp.setViewportView(table);
			jp.add(sp, BorderLayout.CENTER);
			JPanel buttons = new JPanel();
			JButton nullbutton = new JButton("Set to Null");
			nullbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					nullPressed();
				}
			});
			buttons.add(nullbutton);
			JButton cancelbutton = new JButton("Cancel");
			cancelbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelPressed();
				}
			});
			buttons.add(cancelbutton);
			JButton newbutton = new JButton("New "+env.translate(env.getTableName()));
			newbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					newPressed();
				}
			});
			buttons.add(newbutton);
			JButton searchbutton = new JButton("Search");
			searchbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					searchPressed();
				}
			});
			buttons.add(searchbutton);
			JButton okbutton = new JButton("OK");
			okbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					okPressed();
				}
			});
			buttons.add(okbutton);
			jp.add(buttons, BorderLayout.SOUTH);
		}
		else {
			JTextArea ta = new JTextArea();
			ta.setText(
					"Right side "+getClass().getName()+"\n\nInstance "+this.toString()+
					"\n\nSomething went wrong with getting table."+"\n\n"+
					((sqlmessage != null) ? sqlmessage : ""));
			jp.setLayout(new StringGridBagLayout());
			jp.add(ta);
		}
		pack();
		setVisible(true);
	}
	
	public RowNode getSelectedRow() {
		RowNode rn = selectedRow;
		selectedRow = null;
		return rn;
	}
	
	private void cancelPressed() {
		selectedRow = null;
		setVisible(false);
	}
	
	private void searchPressed() {
		SearchRowDialog srd = new SearchRowDialog(env, ChoseFromFullTableDialog.this, true);
		ClientDBObject po = srd.getChosenObject();
		if(po == null) {
			//System.out.println("ChoseFromFullTableDialog.searchPressed. po == null");
			setVisible(false);
			return;
		}
		selectedRow = null;
		for(int i = 0 ; i < tableRowsOrder.size() ; i++) {
			RowNode rn = tableRowsOrder.elementAt(i);
			if(rn.getRowObject().sameObject(po)) {
				selectedRow = rn;
				//System.out.println("ChoseFromFullTableDialog.searchPressed. po != null ; i = "+i);
				setVisible(false);
				return;
			}
		}
		setVisible(false);
	}

	private void nullPressed() {
		nullPressed = true;
		selectedRow = null;
		setVisible(false);
	}
	
	public boolean getNullPressed() {
		return nullPressed;
	}

	private void okPressed() {
		selectedRow = null;
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			UniversalTellUser.info(((MainWindow) env.getClientModelLogic().getMainViewObject()), "Choice -1 = No choice");
			setVisible(false);
			return;
		}
		if(rows.size() == 0) {
			UniversalTellUser.info(((MainWindow) env.getClientModelLogic().getMainViewObject()), "No rows to select = no choice");
			selectedRow = null;
			setVisible(false);
			return;
		}
		selectedRow = tableRowsOrder.elementAt(rowno);
		setVisible(false);
	}
	
	private void newPressed() {
		
		Runnable r = new Runnable() {
			public void run() {
				defineChildNodes();
				if(table != null) {
					ResultSetTableModel model = (ResultSetTableModel) table.getModel();
					model.clear();
					table.invalidate();
					table.validate();
					table.repaint();
				}
				invalidate();
				validate();
				repaint();
			}
		};
		NewRowDialog nrd = null;
		try {
			nrd = new NewRowDialog(env, null, null, null, ChoseFromFullTableDialog.this);
		} catch (Exception e) {
			UniversalTellUser.error(null, "Unable to create new instance", "Internal error");
			env.logTrace(e);
			return;
		}
		nrd.setVisible(true);
		defineChildNodes();
		if(table != null) {
			ResultSetTableModel model = (ResultSetTableModel) table.getModel();
			model.clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}
		invalidate();
		validate();
		repaint();
		if(nrd.getEditAfterNew()) {
			UniversalTellUser.info(this, "No edit after insert in chose dialog");
		}
	}
	
	private void defineChildNodes() {
		try {
			//System.out.println("ChoseFromTableDialog.defineChildNodes of "+env.ths.getName());
			// Dont do it. Only on focus. Else whole db is loaded on init.
			List<DataNode> children = env.getClientModelLogic().getAll(env.getTableName());

			rows.removeAllElements();
			for(int i = 0 ; i < children.size() ; i++) {
				DataNode ro = children.get(i);
				RowNode rn = new RowNode(ro, null, ChoseFromFullTableDialog.this);
				rows.add(rn);
			}

			final ResultSetTableModel model = new ResultSetTableModel();
			model.addTableModelListener(new TableModelListener() {
				public void tableChanged(TableModelEvent arg0) {
					//System.out.println("mod - tableChanged "+arg0);
				}
			});
			//System.out.println("ChoseFromTableDialog.defineChildNodes SQL:"+sql+" successfully created "+ctr+" rows.");
			if(table == null) {
				table = new JTable(model);
			}
		    TableColumnModelListener tableColumnModelListener = new TableColumnModelListener() {
		        public void columnAdded(TableColumnModelEvent e) {
		          //System.out.println("TableNode.column Added");
		        }

		        public void columnMarginChanged(ChangeEvent e) {
		          //System.out.println("TableNode.column Margin");
		        }

		        public void columnMoved(TableColumnModelEvent e) {
		          int from = e.getFromIndex();
		          int to = e.getToIndex();
		          //System.out.println("TableNode.column Moved "+from+" -> "+to);
		          sortInverted = to == sortcolumn ? !sortInverted : false;
		          sortcolumn = to;
		          rearrangeTableOrder();
		          model.fireTableStructureChanged();
		        }

		        public void columnRemoved(TableColumnModelEvent e) {
		          //System.out.println("TableNode.column Removed");
		        }

		        public void columnSelectionChanged(ListSelectionEvent e) {
		          //System.out.println("TableNode.column Selection Changed");
		        }
		      };

		      TableColumnModel columnModel = table.getColumnModel();
		      columnModel.addColumnModelListener(tableColumnModelListener);
		}
		catch(SQLException sqle) {
			sqlmessage = sqle.getMessage();
			env.log("SQL exception: "+sqlmessage, sqle);
		}
		catch(Exception e) {
			sqlmessage = e.getMessage();
			env.log("SQL exception: "+sqlmessage, e);
		}
	}

	private class ResultSetTableModel extends AbstractTableModel {  
		
		public ResultSetTableModel() {
		}
		
		public void clear() {
			//sht.clear();
			rearrangeTableOrder();
			this.fireTableDataChanged();
		}
		
		public String getColumnName(int c) {
			String name = env.getColumnNameTranslated(c);//colnames.elementAt(c);
			return env.translate(name);
		}

		public int getColumnCount() { 
			return env.getColumnCount();
		}

		public Object getValueAt(int r, int c) {
			return getTableStringToPresent(r, c);
		}

		public int getRowCount() {
			int r = getRowCountForTable();
			return r;
		}
	} 

	private int getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
			return tableRowsOrder.size();
		}
		int len = -1;
		try {
			len = env.getClientModelLogic().getTableLength(env.getTableName());
		}
		catch(Exception e) {
		}
		if(tableRowsOrder.size() != len) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.size();
		//return getChildCount();
	}

	private String getTableStringToPresent(int r, int c) {
		if(r >= getRowCountForTable()) {
			//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> <Out of bounds>");
			return "Out of bounds ("+r+" >= "+getRowCountForTable()+")";
		}
		return getTableStringToPresent(getNodeAtPlaceForTable(r), c);
	}
	
	private RowNode getNodeAtPlaceForTable(int row) {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		//return (RowNode) getChildAt(row);
		return tableRowsOrder.elementAt(row);
	}
	
	private String getTableStringToPresent(RowNode rn, int c) {
		String obj = null;
		try {
			obj = rn.getRowObject().renderForTableAtPosition(c);
		}
		catch(Exception ie) {
			env.logTrace(ie);
			//System.out.println("2: Scope dump "+s+" when evaluating "+vl);
			obj = "<no information>";
		}
		if(obj == null) {
			obj = "<not set>";
		}
		return obj;
	}

	
	private void rearrangeTableOrder() {
		//System.out.println("TableNode.rearrangeTableOrder col="+sortcolumn+", inv="+sortInverted);
		if(tableRowsOrder == null) {
			tableRowsOrder = new Vector<RowNode>();
		}
		if(tableRowsOrder.size() != rows.size()) {
			//System.out.println("TableNode.rearrangeTableOrder initierar Vectorn");
			tableRowsOrder.removeAllElements();
			for(int i = 0 ; i < rows.size() ; i++) {
				tableRowsOrder.addElement(rows.elementAt(i));
			}
		}
		if(sortcolumn == -1) {
			return;
		}
		Comparator<Object> comp = new Comparator<Object>() {
			private GeneralComparator gp = new GeneralComparator();
			public int compare(Object arg0, Object arg1) {
				String s1 = getTableStringToPresent((RowNode) arg0, sortcolumn);
				String s2 = getTableStringToPresent((RowNode) arg1, sortcolumn);
				int out = gp.compare(s1, s2);
				if(sortInverted) {
					out = -out;
				}
				return out;
			}
		};
		Vector<RowNode> tmp = new Vector<RowNode>();
		for(int i = 0 ; i < tableRowsOrder.size(); i++) {
			//System.out.println("TableNode.rearrangeTableOrder sorterar Vectorn "+tmp.size());
			Sort.add(tmp, getNodeAtPlaceForTable(i), comp);
		}
		tableRowsOrder = tmp;
	}
	
	  public class PopupListener extends MouseAdapter 
	  {
	    
	    public PopupListener()
	    {
	    }

		public void mouseClicked(MouseEvent e) {
			//System.out.println("ChoseFromFullTableDialog.PopupListener.mouse listener clicked "+e);
	        if (e.getComponent().isEnabled() && e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
	        	//System.out.println("TableNode.PopupListener.mouse listener dubbelklick ###");
	    		int rowno = table.getSelectedRow();
	    		if(rowno == -1) {
	    			return;
	    		}
	    		RowNode rn = getNodeAtPlaceForTable(rowno);
	    		//int row = rn.
	    		JTree tree = ((MainWindow) env.getClientModelLogic().getMainViewObject()).getTree();
	    		TreePath tp = new TreePath(rn.getPath());
	    		tree.setSelectionPath(tp);
	    		tree.expandPath(tp);
	    		tree.makeVisible(tp);
	        }
		}

		public void mousePressed(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) 
	    {
	      if(e.isPopupTrigger()) 
	      {
	        //System.out.println(e.getComponent().getClass().getName());
	        if(e == null) return;
	        JTable table= (JTable) e.getComponent();
	        if(table == null) return;
	        JPopupMenu popup = getTablePopup();
	        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
	      }
	    }

	  }

		public JPopupMenu getTablePopup() {
			JPopupMenu pm = new JPopupMenu();
			JMenuItem menuItem = new JMenuItem("Explore from here");
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					exploreFromTablesSelected();
				}
			});
			pm.add(menuItem);
			return pm;
		}

		private void exploreFromTablesSelected() {
			int rowno = table.getSelectedRow();
			if(rowno == -1) {
				return;
			}
			RowNode rn = getNodeAtPlaceForTable(rowno);
			ExplorerWindow ew = null;
			try {
				ew = new ExplorerWindow(env.getClientModelLogic(), null, rn);
				ew.setVisible(true);
			} catch (Exception e) {
				env.logTrace(e);
				UniversalTellUser.error(null, "Unable to create explorer window", "Explorer error");
			}
		}


}
