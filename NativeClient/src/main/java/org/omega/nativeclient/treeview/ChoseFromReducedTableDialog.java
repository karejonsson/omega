package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.connectivity.helpers.GeneralComparator;

import se.modlab.generics.util.Sort;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class ChoseFromReducedTableDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private String sqlmessage = null;
	private JTable table = null;
	private JScrollPane sp = new JScrollPane();
	//private DBPoolObject selectedRow = null;

	private ClientTableLogic env = null;
	//private DBPool pool = null;

	private int sortcolumn = -1;
	private boolean sortInverted = false;
	private Vector<ClientDBObject> tableRowsOrder = null;//new Vector<RowNode>();
	private Vector<ClientDBObject> poolObjectSet = null;
	private boolean modality = true;

	private ClientDBObject selectedRow = null;

	public ChoseFromReducedTableDialog(JDialog parent, Vector<ClientDBObject> poolObjectSet, String title, ClientTableLogic ctl, boolean modality) {
		super(parent, title, true);
		env = ctl;
		//pool = _pool;
		this.poolObjectSet = poolObjectSet;
		this.modality = modality;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		getContentPane().add(sp);
		JPanel jp = new JPanel();
		sp.getViewport().add(jp);
		defineChildNodes();
		if(table != null) {
			table.addMouseListener(new PopupListener());   
			jp.setLayout(new BorderLayout()); // unless already there
			JScrollPane sp = new JScrollPane();
			sp.setViewportView(table);
			jp.add(sp, BorderLayout.CENTER);
			JPanel buttons = new JPanel();
			JButton okbutton = new JButton("OK");
			okbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					okPressed();
				}
			});
			buttons.add(okbutton);
			if(!modality) {
				JButton explorebutton = new JButton("Explore");
				explorebutton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						exploreFromTablesSelected();
					}
				});
				buttons.add(explorebutton);
			}
			JButton cancelbutton = new JButton("Cancel");
			cancelbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelPressed();
				}
			});
			buttons.add(cancelbutton);
			jp.add(buttons, BorderLayout.SOUTH);
		}
		else {
			JTextArea ta = new JTextArea();
			ta.setText(
					"Right side "+getClass().getName()+"\n\nInstance "+this.toString()+
					"\n\nSomething went wrong with getting table."+"\n\n"+
					((sqlmessage != null) ? sqlmessage : ""));
			jp.setLayout(new StringGridBagLayout());
			jp.add(ta);
		}
		pack();
		setModal(modality);

		setVisible(true);
	}

	private void okPressed() {
		//setVisible(false);
		// selectedRow = rows.elementAt(rowno);
		// selectedRow = tableRowsOrder.elementAt(rowno);
		selectedRow = null;
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			UniversalTellUser.info(((MainWindow) env.getClientModelLogic().getMainViewObject()), "Choice -1 = No choice");
			setVisible(false);
			return;
		}
		if(tableRowsOrder.size() == 0) {
			UniversalTellUser.info(((MainWindow) env.getClientModelLogic().getMainViewObject()), "No rows to select = no choice");
			selectedRow = null;
			setVisible(false);
			return;
		}
		// selectedRow = rows.elementAt(rowno);
		selectedRow = tableRowsOrder.elementAt(rowno);
		setVisible(false);
	}

	private void cancelPressed() {
		selectedRow = null;
		setVisible(false);
	}

	private void defineChildNodes() {
		try {
			if(tableRowsOrder == null) {
				tableRowsOrder = new Vector<ClientDBObject>();
			}
			for(int i = 0 ; i < poolObjectSet.size() ; i++) {
				ClientDBObject ro = poolObjectSet.elementAt(i);
				tableRowsOrder.add(ro);
			}

			final ResultSetTableModel model = new ResultSetTableModel();
			if(table == null) {
				table = new JTable(model);
			}
			TableColumnModelListener tableColumnModelListener = new TableColumnModelListener() {
				public void columnAdded(TableColumnModelEvent e) {
					//System.out.println("TableNode.column Added");
				}

				public void columnMarginChanged(ChangeEvent e) {
					//System.out.println("TableNode.column Margin");
				}

				public void columnMoved(TableColumnModelEvent e) {
					int from = e.getFromIndex();
					int to = e.getToIndex();
					//System.out.println("TableNode.column Moved "+from+" -> "+to);
					sortInverted = to == sortcolumn ? !sortInverted : false;
					sortcolumn = to;
					rearrangeTableOrder();
					model.fireTableStructureChanged();
				}

				public void columnRemoved(TableColumnModelEvent e) {
					//System.out.println("TableNode.column Removed");
				}

				public void columnSelectionChanged(ListSelectionEvent e) {
					//System.out.println("TableNode.column Selection Changed");
				}
			};

			TableColumnModel columnModel = table.getColumnModel();
			columnModel.addColumnModelListener(tableColumnModelListener);
		}
		catch(Exception e) {
			sqlmessage = e.getMessage();
			env.log("SQL exception: "+sqlmessage, e);
		}
	}

	private class ResultSetTableModel extends AbstractTableModel {  
		private static final long serialVersionUID = 1L;

		public ResultSetTableModel() {}

		public void clear() {
			rearrangeTableOrder();
			this.fireTableDataChanged();
		}

		public String getColumnName(int c) {
			String name = env.getColumnNameTranslated(c);//colnames.elementAt(c);
			return env.translate(name);
		}

		public int getColumnCount() { 
			return env.getColumnCount();
		}

		public Object getValueAt(int r, int c) {
			return getTableStringToPresent(r, c);
		}

		public int getRowCount() {
			int r = getRowCountForTable();
			return r;
		}
	} 

	private int getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.size();
	}

	private String getTableStringToPresent(int r, int c) {
		if(r >= getRowCountForTable()) {
			return "Out of bounds ("+r+" >= "+getRowCountForTable()+")";
		}
		return getTableStringToPresent(getNodeAtPlaceForTable(r), c);
	}

	private ClientDBObject getNodeAtPlaceForTable(int row) {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.elementAt(row);
	}

	private String getTableStringToPresent(ClientDBObject rn, int c) {
		String obj = null;
		try {
			obj = rn.renderForTableAtPosition(c);
		}
		catch(Exception ie) {
			env.logTrace(ie);
			//System.out.println("2: Scope dump "+s+" when evaluating "+vl);
			obj = "<no information>";
		}
		if(obj == null) {
			obj = "<not set>";
		}
		return obj;
	}

	private void rearrangeTableOrder() {
		if(tableRowsOrder == null) {
			tableRowsOrder = new Vector<ClientDBObject>();
		}
		if(sortcolumn == -1) {
			return;
		}
		Comparator<Object> comp = new Comparator<Object>() {
			private GeneralComparator gp = new GeneralComparator();
			public int compare(Object arg0, Object arg1) {
				String s1 = getTableStringToPresent((ClientDBObject) arg0, sortcolumn);
				String s2 = getTableStringToPresent((ClientDBObject) arg1, sortcolumn);
				int out = gp.compare(s1, s2);
				if(sortInverted) {
					out = -out;
				}
				return out;
			}
		};
		Vector<ClientDBObject> tmp = new Vector<ClientDBObject>();
		for(int i = 0 ; i < tableRowsOrder.size(); i++) {
			//System.out.println("TableNode.rearrangeTableOrder sorterar Vectorn "+tmp.size());
			Sort.add(tmp, getNodeAtPlaceForTable(i), comp);
		}
		tableRowsOrder = tmp;
	}

	public class PopupListener extends MouseAdapter {

		public PopupListener() { }

		public void mouseClicked(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if(e.isPopupTrigger()) {
				if(e == null) return;
				JTable table= (JTable) e.getComponent();
				if(table == null) return;
				JPopupMenu popup = getTablePopup();
				if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}

	}

	public JPopupMenu getTablePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = null;
		if(!modality) {
			menuItem = new JMenuItem("Explore from here");
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					exploreFromTablesSelected();
				}
			});
			pm.add(menuItem);
		}
		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deleteTablesSelected();
			}
		});
		pm.add(menuItem);
		return pm;
	}

	private void exploreFromTablesSelected() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		ClientDBObject po = getNodeAtPlaceForTable(rowno);
		RowNode rn = null;
		try {
			rn = new RowNode(new DataNode(po), null, ChoseFromReducedTableDialog.this);
			ExplorerWindow ew = new ExplorerWindow(env.getClientModelLogic(), null, rn);
			ew.setVisible(true);
		}
		catch(Exception e) {
			UniversalTellUser.error(this, "Unable to explore from the node.");
			return;
		}
	}

	private void deleteTablesSelected() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		ClientDBObject po = getNodeAtPlaceForTable(rowno);
		try {
			po.delete();
		}
		catch(Exception e) {
			sqlmessage = "Failed on delete.\n\n"+
					"Exceptions message was "+e.getMessage();
			UniversalTellUser.error(((MainWindow) env.getClientModelLogic().getMainViewObject()), sqlmessage); 
			return;
		}
	}

	public ClientDBObject getChosenObject() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return null;
		}
		return getNodeAtPlaceForTable(rowno);
	}

}
