package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.omega.clientcommon.editors.CandidateClientDBObject;
import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;
import org.omega.nativeclient.helpers.BlobActionHelper;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.LobTypeEditor;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class NewRowDialog extends RowDialogCommons {

	private boolean setVisibleFinished = false;
	private boolean insideSomeUpdateHappened = false;
	
	private CandidateClientDBObject candidate = null;
	private boolean editAfterNew = false;

	public NewRowDialog(ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, Window previousWindow) throws Exception {
		super(env, onInsert, implicitConstraintname, referred, previousWindow, "New");
		if(referred != null) {
			env.logText("Starts entering new "+env.getTableName()+" with key "+implicitConstraintname+" hidden but set");
		}
		else {
			env.logText("Starts entering new "+env.getTableName()+" with no hidden key");
		}
		candidate = new CandidateClientDBObject(env);
		if(referred != null) {
			candidate.setReference(implicitConstraintname, referred);
		}
	    env.logText("Starts creating object "+candidate);
	}

	public boolean getEditAfterNew() {
		return editAfterNew;
	}

	public ClientDBObject getCreatedClientDBObject() {
		return candidate.getCreatedClientDBObject();
	}
	
	protected void someUpdateHappened() {
		if(!setVisibleFinished) {
			return;
		}
		if(insideSomeUpdateHappened) {
			return;
		}
		insideSomeUpdateHappened = true;
		candidate.executeOnSomeUpdateHappened();
		insideSomeUpdateHappened = false;
	}
	
	public void setVisible(boolean visible) {
		if(!visible) {
			super.setVisible(false);
			dispose();
			return;
		}
		final JScrollPane rightView = new JScrollPane();
		JPanel jp = new JPanel();
		rightView.getViewport().add(jp);
		getContentPane().add(rightView, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());
		try { 
			List<EditorCommons> editors = candidate.getEditorsForEditables(implicitConstraintname, true);
			setupPanelForMembers(jp, editors);
			
			ctr++;
			JButton see = new JButton("Insert+leave");
			jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					insertPressed();
					setVisible(false);
				}
			});
			JPanel leftButtonPanel = new JPanel();
			leftButtonPanel.setLayout(new StringGridBagLayout());
			see = new JButton("Insert+edit");
			leftButtonPanel.add("gridx=2,gridy=0,anchor=EAST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					insertPressed();
					editAfterNew = true;
					setVisible(false);
				}
			});
			see = new JButton("Insert");
			leftButtonPanel.add("gridx=1,gridy=0,anchor=EAST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					insertPressed();
				}
			});
			see = new JButton("Quit");
			leftButtonPanel.add("gridx=0,gridy=0,anchor=EAST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					donePressed();
				}
			});
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, leftButtonPanel);

			pack();
			int h = getSize().height+100*countTextareas+50;
			int w = getSize().width+250;
			setSize(w, h);
			setVisibleFinished = true;
			someUpdateHappened();
			super.setVisible(true);
		}
		catch(Exception ie) {
			env.log(ie.getMessage(), ie);
			UniversalTellUser.error(this, ie.getMessage());  
			ie.printStackTrace();
			return;
		}
		setVisibleFinished = true;
	} 

	private void insertPressed() {
		try {
			candidate.persist();
		}
		catch(Exception e) {
			//e.printStackTrace();
			String message = "Failed to persist!";
			env.log(message, e);
			if(env.isUniqueConstraintViolation(e)) {
				UniversalTellUser.error(this, "Unique constraint violation in the "+env.getDBsName()+" database");
			}
			else if(env.isConstraintViolation(e)) {
				UniversalTellUser.error(this, "Constraint violation in the "+env.getDBsName()+" database");
			}
			else {
				UniversalTellUser.error(this, message);
			}
			return;
		}
		candidate.clear();
		postPersist(candidate.getCreatedClientDBObject());
	}

	public void donePressed() {
		candidate.clear();
		setVisible(false);
		dispose();
	}

	protected void performPostInsertLobUploads(Vector<LobTypeEditor> leds, boolean clear) {
		//System.out.println("NewRowDialog.performPostInsertLobUploads("+leds.size()+", "+clear+")");
		for(int i = 0 ; i < leds.size(); i++) {
			LobTypeEditor lte = leds.elementAt(i);
			//System.out.println("NewRowDialog.performPostInsertLobUploads lte "+lte.getName());
			if(lte.isUnchanged()) {
				//System.out.println("NewRowDialog.performPostInsertLobUploads lte "+lte.getName()+" isUnchanged");
				continue; 
			}
			if(!candidate.performLobUploads(lte.getLobEditorCommons(), leds.size()-1-i)) {
				//System.out.println("NewRowDialog.performPostInsertLobUploads lte "+lte.getName()+" fel");
				return;
			}
			if(clear) {
				//System.out.println("NewRowDialog.performPostInsertLobUploads lte "+lte.getName()+" clear");
				lte.clear();
			}
		}
	}

	protected JComponent getLobButtonsPanel(final LobTypeEditor lte, boolean autoupdate) {

		Font fdef = null;
		Insets idef = null;

		JButton setbutton = new JButton("Set");
		setbutton.setEnabled(!autoupdate);

		fdef = setbutton.getFont();
		idef = setbutton.getInsets();
		setbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		setbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		setbutton.setPreferredSize(new Dimension(50, 20));

		setbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonSetPressed(lte, ((MainWindow) env.getClientModelLogic().getMainViewObject()));//env.getMainWindow());
				}
				catch(Exception e) {
					e.printStackTrace();
					env.log("NewRowDialog.getLobButtonsPanel - Action Set", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});

		JButton nullbutton = new JButton("Null");
		nullbutton.setEnabled(!autoupdate);

		fdef = nullbutton.getFont();
		idef = nullbutton.getInsets();
		nullbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		nullbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		nullbutton.setPreferredSize(new Dimension(50, 20));

		nullbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonNullPressed(lte, ((MainWindow) env.getClientModelLogic().getMainViewObject()));
				}
				catch(Exception e) {
					e.printStackTrace();
					env.log("NewRowDialog.getLobButtonsPanel - Action Null", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});

		JPanel jp = new JPanel(new GridLayout(2,1,1,1));

		jp.add(setbutton);
		jp.add(nullbutton);

		return jp;
	}

}	
