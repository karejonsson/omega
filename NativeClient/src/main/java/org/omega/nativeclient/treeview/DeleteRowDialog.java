package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.helpers.DBTypeManagement;
//import org.omega.nativeclient.helpers.NativeClientTableHandling;
import org.omega.nativeclient.helpers.GetGraphicalEditorVisitor;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import org.omega.nativeclient.typegraphics.LobTypeEditor;
import org.omega.nativeclient.typegraphics.PointerGraphicalEditor;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
 
public class DeleteRowDialog extends JDialog {
	
	private ClientTableLogic env; 
	private RowNode rn;
	private boolean deleteChoice = false;
	
	public DeleteRowDialog(ClientTableLogic env, RowNode rn) { 
		super(((MainWindow) env.getClientModelLogic().getMainViewObject()), "Delete "+env.getTableName(), true);
		this.env = env;
		this.rn = rn;
	    addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent windowevent) {
	          setVisible(false);
	        }
	      });
	    //DBPoolObject ro = rn.getRowObject();
		JPanel jp = new JPanel();
	    getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());
		
		try { 
			List<EditorCommons> editors = rn.getRowObject().getClientDBObject().getEditorsForEditables(null, false);

			int ctr = 0;
			int countTextareas = 0;

			final Hashtable<String, GraphicalEditor> editorLookup = new Hashtable<String, GraphicalEditor>();
			//for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			//	Column col = cs.getColumn(i);
			//	final String colname = col.getName();
			int editorNumber = -1;
			GetGraphicalEditorVisitor ggev = new GetGraphicalEditorVisitor(((MainWindow) env.getClientModelLogic().getMainViewObject()));
			for(EditorCommons editor : editors) {
				editorNumber++;

				/*
				org.omega.configuration.struct.Type t = col.getType();

				final GraphicalEditor ge = GetGraphicalEditorVisitor.getGraphicalEditor(po, colname, t, env);
				if(col.isAutoIncrement()) {
					// Auto incremented field cannot be subject for editing

					continue;
				}
				*/
				editor.accept(ggev);
				GraphicalEditor ge = ggev.getEditor();
				editorLookup.put(editor.getName(), ge);

				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, ge);

				ctr += ge.rowsUsed();

				if(env.hasSeparator(editorNumber)) {
					jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JPanel());
					ctr++;
					jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
					jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
					jp.add("gridx=2,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
					ctr++;
				}
				
				ctr += ge.rowsUsed();

			/*

			CreateStatement cs = env.getEmbeddedEnvironment().ths.getCreateStatement();
			
			int ctr = 0;
			
			for(int i = 0 ; i < cs.getNoColumns() ; i++) {
				Column col = cs.getColumn(i);
				String name = col.getName();
				if(cs.getPrimaryKeyDeclaration().hasMember(name)) { 
					continue;
				}
				if(cs.isForeignKeyDeclarationMember(name)) { //col.isAutoIncrement()) {
					continue;
				}
				JLabel varname = new JLabel(env.translate(name));
				jp.add("gridx=0,gridy="+ctr+",anchor=WEST,insets=[12,12,0,0]", varname);
				Object obj = ro.get(name.toUpperCase());
				Object previousValue = (obj == null) ? null : obj.toString();
				if(previousValue == null) {
					previousValue = DBTypeManagement.getDefault(col);
				}
				JLabel field = new JLabel(previousValue == null ? "" : previousValue.toString());
				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field);
				JLabel formatlabel = new JLabel("Type: "+col.getType().getTypename());
				jp.add("gridx=2,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", formatlabel);
				ctr++;
			}
			for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
				ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
				//DBPoolObject po = rn.getRowObject();
				//DBPool pool = rn.getPool();
				ForeignKey fk = env.getForeignKey(i, po.getHashtable());//po.getForeignKey(i);
				EmbeddedTableHandling otherEnv = env.getOtherEmbeddedEnvironment(fk.getTablenamePointingTo());
				PrimaryKey pk = fk.assemblePrimaryKey(otherEnv.getPrimaryKeyDeclaration());
				//DBPoolObject pointed = pool.getPoolObject(pk);
				JLabel label = null;
				if(pointed == null) {
					label = new JLabel("Unset constraint to "+fkd.toString());
				}
				else {
					label = new JLabel("Set constraint to "+fk);
				}
				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", label);
				ctr++;
			*/
			}
			ctr++;
			JButton deleteButton = new JButton("Delete");
			jp.add("gridx=1,gridy="+ctr+",anchor=EAST,insets=[12,12,0,0]", deleteButton);
			deleteButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					deletePressed();
				}
			});
			JButton cancelButton = new JButton("Cancel");
			jp.add("gridx=0,gridy="+ctr+",anchor=EAST,insets=[12,12,0,0]", cancelButton);
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelPressed();
				}
			});
			pack();
			setVisible(true);
		}
		catch(Exception ie) {
			ie.printStackTrace();
			UniversalTellUser.error(this, ie.getMessage());  
			return;
		}
	}

	private void deletePressed() {
		deleteChoice = true;
		setVisible(false);
		dispose();
	}
		
	private void cancelPressed() {
		deleteChoice = false;
		setVisible(false);
		dispose();
	}
	
	public boolean getDeleteChoice() {
		return deleteChoice;
	}
		
}
