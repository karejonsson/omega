package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.hierarchy.MetaNode;
//import org.omega.configuration.model.ModelConfiguration;
//import org.omega.configuration.model.TableConfiguration;
//import org.omega.configuration.recreatesql.Recreator;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class MetaExplorerWindow extends MainWindow {

	public static final String version = "Dev 1";

	private JScrollPane rightView = new JScrollPane();
	private JScrollPane leftView = new JScrollPane();

	//private ModelConfiguration hs = null;
	private JTree tree = null;
	private NodeBase lastFocusedNode = null;
	//private MetaTopNode mtn = null;
	private NodeBase tn = null;
	private ClientModelLogic cml = null;
	//private Recreator rec;

	public static MetaTopNode getTopNodeForMetaStructure(ClientModelLogic cml) {//ModelConfiguration hs, Recreator rec) {
		MetaTopNode mtn = new MetaTopNode(cml);
		for(ClientTableLogic ctl : cml.getClientTableLogicLoopable()) {
			if(ctl.getNoReferringConstraints() == 0) {
				mtn.add(new MetaTableNode(new MetaNode(ctl)));
			}
		}
		return mtn;
	}

	public static MetaTableNode getTableNodeForMetaStructure(ClientTableLogic ctl) {
		MetaTableNode mtn = new MetaTableNode(new MetaNode(ctl));//new MetaNode(hs, hs.getDBTableHandlingSpecificationOnTableName(tablename), rec));
		return mtn;
	}

	public MetaExplorerWindow(MetaTableNode mtn) { //ModelConfiguration _hs, String frametitle, NodeBase _tn, Recreator _rec) {
		super(mtn.getClientTableLogic().getClientModelLogic().getFrametitle());
		//hs = _hs;
		//tn = _tn;
		//rec = _rec;
		this.tn = mtn;
		this.cml = mtn.getClientModelLogic();
		init();
	}

	public MetaExplorerWindow(MetaTopNode mtn) { //ModelConfiguration _hs, String frametitle, NodeBase _tn, Recreator _rec) {
		super(mtn.getClientModelLogic().getFrametitle());
		//hs = _hs;
		//tn = _tn;
		//rec = _rec;
		this.tn = mtn;
		this.cml = tn.getClientModelLogic();
		init();
	}

	private void init() {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		setMenuBar();
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setTopComponent(leftView);
		splitPane.setBottomComponent(rightView);

		Dimension minimumSize = new Dimension(100, 50);
		rightView.setMinimumSize(minimumSize);
		leftView.setMinimumSize(minimumSize);
		splitPane.setDividerLocation(300);
		splitPane.setPreferredSize(new Dimension(700, 500));

		getContentPane().add(splitPane, BorderLayout.CENTER);

		//rightView.setViewportView(c);
		//leftView.getViewport().setView;
		createTree();
		tree.addMouseListener(new PopupListener());   
		leftView.getViewport().setView(tree);
		//jp.setLayout(new StringGridBagLayout());
		//updateJMenuBar();
		try {
			pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(this, ie.getMessage());
			return;
		}
	}

	private void createTree()
	{
		final JTree _tree = new JTree(tn);

		_tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		_tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				//System.out.println("You selected " + node+" of class "+node.getClass().getName()+" is NodeBase = "+(node instanceof NodeBase));
				if(node instanceof NodeBase) {
					lastFocusedNode = (NodeBase) node;
					lastFocusedNode.gotFocus();
					setRightSide(lastFocusedNode.getRightSide());
				}
			}
		});
		_tree.addTreeWillExpandListener(new TreeWillExpandListener (){

			public void treeWillCollapse(TreeExpansionEvent evt)
					throws ExpandVetoException {
			}

			public void treeWillExpand(TreeExpansionEvent evt)
					throws ExpandVetoException {
				JTree tree = (JTree)evt.getSource();  
				TreePath path = evt.getPath(); 
				tree.setSelectionPath(path);
			}


		}); 

		tree = _tree; 
		tree.setCellRenderer(new IconNodeRenderer());
	}

	private void setMenuBar() {
		JMenuBar mb = new JMenuBar();
		setJMenuBar(mb);
		JMenu file = new JMenu("File");
		mb.add(file);
		JMenuItem exit = new JMenuItem("Exit");
		file.add(exit);
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});
		JMenu allTables = new JMenu("All tables");
		for(final ClientTableLogic ctl : cml.getClientTableLogicLoopable()) {
			JMenuItem table = new JMenuItem(ctl.translate(ctl.getTableName()));
			allTables.add(table);
			table.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MetaTableNode tn = MetaExplorerWindow.getTableNodeForMetaStructure(ctl);
					MetaExplorerWindow mew = new MetaExplorerWindow(tn);
					mew.setVisible(true);
				}
			});

		}
		mb.add(allTables);
	}

	public void setRightSide(Component c) {
		rightView.setViewportView(c);
	}

	public JTree getTree() {
		return tree;
	}

	public static class PopupListener extends MouseAdapter {

		public PopupListener() { }

		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if(e.isPopupTrigger())  {
				//System.out.println(e.getComponent().getClass().getName());
				if(e == null) return;
				JTree tree = (JTree) e.getComponent();
				if(tree == null) return;
				TreePath tp = tree.getClosestPathForLocation(e.getX(), e.getY());
				tree.setSelectionPath(tp);
				Object obj = tree.getLastSelectedPathComponent();
				if(obj instanceof NodeBase) {
					NodeBase nb = (NodeBase) obj;
					if(nb == null) return;
					JPopupMenu popup = nb.getTreePopup();
					if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
					return;
				}
				if(obj instanceof TopNode) {
					TopNode tn = (TopNode) obj;
					JPopupMenu popup = tn.getTreePopup();
					if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
					return;

				}
			}
		}

	}

}
