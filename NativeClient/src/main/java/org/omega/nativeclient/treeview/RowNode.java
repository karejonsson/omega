package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;
import org.omega.clientcommon.editors.ReferrerEditorCommons;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.clientcommon.hierarchy.HierarchyNode;
import org.omega.clientcommon.hierarchy.ReferenceNode;
import org.omega.clientcommon.hierarchy.VoidNode;
import org.omega.nativeclient.helpers.BlobActionHelper;
import org.omega.nativeclient.helpers.GetGraphicalEditorVisitor;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import org.omega.nativeclient.typegraphics.LobTypeEditor;
import org.omega.nativeclient.typegraphics.ReferrerGraphicalEditor;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.gui.util.Clipboard;

public class RowNode extends NodeBase {

	//private DBPool pool = null;
	//private DBPoolObject ro = null;
	//private NativeClientTableHandling env;
	//private PrimaryKey pk = null;
	private String sqlmessage = null;
	//private Hashtable<String, GraphicalEditor> editors = null;//new Hashtable<String, JLabel>();
	Hashtable<String, GraphicalEditor> editorLookup = null;
	//private TextRenderer tr;
	private Hashtable<String, NodeBase> references = new Hashtable<String, NodeBase>();
	private Window currentWindow = null;
	//private ScriptingScope scope = null;
	private ClientModelLogic model = null;
	private ClientTableLogic ctl = null;
	private DataNode ro = null;
	private ClientDBObject current = null;
	private String foreignKeyDeclarationName = null;
	
	public RowNode(DataNode ro, String foreignKeyDeclarationName, Window currentWindow) throws Exception {
		//System.out.println("Nytt radobjekt "+ro);
		//this.pool = pool;
		this.ro = ro;
		current = ro.getClientDBObject();
		this.ctl = ro.getClientTableLogic();
		this.model = ctl.getClientModelLogic();
		this.foreignKeyDeclarationName = foreignKeyDeclarationName;
		this.currentWindow = currentWindow;
	}
	
	public boolean sameObject(ClientDBObject cdbobj) {
		return ro.sameObject(cdbobj);
	}

	/*
	public RowNode(DBPool pool, NativeClientTableHandling env, DBPoolObject ro, TextRenderer tr, Window currentWindow) throws Exception {
		this.pool = pool;
		this.ro = ro;
		this.env = env;
		this.pk = ro.getPrimaryKey();
		this.tr = tr;
		if(tr == null) {
			tr = new DefaultRownodeTextRenderer(env.getEmbeddedEnvironment().ths);
		}
		this.currentWindow = currentWindow;
	    scope = env.getDetails().getScopeFactory().getNodeScope(ro, true);
	}
	*/
	
	public NodeBase cloneForOtherWindow(Window window) {
		try {
			return new RowNode(ro, foreignKeyDeclarationName, currentWindow);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	/*
	public DBPool getPool() {
		return pool;
	}
	
	public EmbeddedTableHandling getEnvironment() {
		return env.getEmbeddedEnvironment();
	}
	*/
	
	public ClientTableLogic getClientTableLogic() {
		return ctl;
	}
	
	/*
	public NativeClientModelHandling getDetails() {
		try {
			return env.getClientDetails();
		} catch (Exception e) {
			env.log("Unable to get details for class ", e);
		}
		return null;
	}
	*/
	
	public String getName() {
		return ctl.getTableName();
	}
	
	/*
	public String getIdFieldName() {
		return idfieldname;
	}
	*/
	
	public DataNode getRowObject() {
		return ro;
	}

	public boolean isLeaf() {
		return(false);
	}

	public int getChildCount() {
		if (!areChildrenDefined)
			defineChildNodes();
		return(super.getChildCount());
	}

	public void initialiseYourOwn() {
		try {
			ro.refresh();
		}
		catch(Exception e) {
			sqlmessage = "Failed on refresh.\n\nObject was "+ro+"\n\n"+
			"Exceptions message was "+e.getMessage();
			ctl.log(sqlmessage, e);
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), sqlmessage); 
			sqlmessage = null;
			return;
		}
	}
	
	public void redefineChildNodes() {
		areChildrenDefined = false;
		defineChildNodes();
	}
	
	private void defineChildNodes() {
		areChildrenDefined = true;
		removeAllChildren();
		try { 
			// Add tree children for referred objects
			List<HierarchyNode> pointedAt = ro.getReferred();
			for(HierarchyNode referred : pointedAt) {
				if(referred instanceof DataNode) {
					RowNode rn = new RowNode((DataNode) referred, ro.getReference(), currentWindow);
					rn.initialiseYourOwn();
					add(rn);
					references.put(((DataNode) referred).getReference(), rn);
					continue;
				}
				if(referred instanceof VoidNode) {
					VoidNode vn = (VoidNode) referred;
					NullPointerNode node = new NullPointerNode(ctl, ctl.translate(vn.getReference()));
					add(node);
					references.put(vn.getReference(), node);
				}
			}
			
			// Add children for referring constraints
			List<ReferenceNode> referenceNodes = current.getReferenceNodesForTree();
			for(ReferenceNode refn : referenceNodes) {
				try {
					ReferrerNode rn = new ReferrerNode(refn, currentWindow); 
					add(rn);
				}
				catch(NumberFormatException nfe) {
					UniversalTellUser.error(((MainWindow) model.getMainViewObject()), ""+refn+" was not interpretable as an indexing object");
					return;
				}
			}

		}
		catch(Exception ie) {
			ie.printStackTrace();
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), "Exception: "+ie.getMessage());
		}
	} 
		
	public void delete() {
		DeleteRowDialog drd = new DeleteRowDialog(ctl, this);
		boolean deleteChoice = drd.getDeleteChoice();
		if(!deleteChoice) {
			return;
		}

		try {
			ro.deleteFromDatabase();
		}
		catch(Exception e) {
			sqlmessage = "Failed on delete.\n\n"+
			"Exceptions message was "+e.getMessage();
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), sqlmessage); 
			return;
		}
		TreeNode tn = getParent();
		if(tn instanceof TableNode) {
			((TableNode) tn).refresh();
			return;
		}
		DefaultTreeModel treemodel = (DefaultTreeModel) ((MainWindow) model.getMainViewObject()).getTree().getModel();
		treemodel.nodeStructureChanged(tn);
	}
	
	/*
	private static ForeignKeyDeclaration getSingleForeignKeyDeclaration(String membername, Vector<ForeignKeyDeclaration> fkds) throws Exception {
		// Since the assumption is as point 1 below states this lookup is simpler than expected.
		 for(int i = 0 ; i < fkds.size() ; i++) {
			 ForeignKeyDeclaration fkd = fkds.elementAt(i);
			 if(fkd.getNoMembers() != 1) {
				 throw new Exception("Found "+fkd+" which does not have one member as the intention was to have verified by here.");
			 }
			 if(fkd.getMember(0).toUpperCase().compareTo(membername.toUpperCase()) == 0) {
				 return fkd;
			 }
		 }
		 return null;
	}
	*/

	public Component getRightSide() {
		//System.out.println("RowNode.getRightSide");
		editorLookup = new Hashtable<String, GraphicalEditor>();
		final JScrollPane rightView = new JScrollPane();
		JPanel jp = new JPanel();
		rightView.getViewport().add(jp);
		jp.setLayout(new StringGridBagLayout());
		try { 
			//	Lägg till medlemmar
			List<EditorCommons> editors = current.getEditorsForEditables(null, false);
			setupPanelForMembers(jp, editors);
			
			// Lägg till referenter
			setupPanelForReferrers(jp);
			
			// Lägg till knapparna
			final Map<String, Runnable> buttons = current.getButtons(); 
			Set<String> keys = buttons.keySet();
			
			if(keys == null && (keys.size() != 0)) {
				JPanel buttonpanel = new JPanel();
				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, buttonpanel);
				int j = 0;
				for(String key : keys) {
					JButton btn = new JButton(key);
					buttonpanel.add("gridx="+j+",gridy=0,anchor=WEST,weightx=1.0,"+GdbappInsets.noinsets, btn);
					btn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							Runnable r = buttons.get(key);
							if(r != null) {
								SwingUtilities.invokeLater(r);
							}
						}
					});	
					j++;
				}
			}
		}
		catch(Exception ie) {
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), ie.getMessage());  
			ctl.logTrace(ie);
			//ie.printStackTrace();
			JTextArea ta = new JTextArea();
			ta.setText("Right side "+getClass().getName()+"\n\nInstance "+this.toString()+"\n");
			//System.out.println("");
			return ta;
		}
		return rightView;
	}
	
	private RowNode lookupRowNodeChild(String name) {
		int noChildren = getChildCount();
		// By invoking the getChildCount method the children will get into memory if it was not there already. 
		// Thus we know that we will find it by looping over them. It is possible that the pointer is null.
		// Is such cases the instance we find is a NullPointerNode. 
		NodeBase node = references.get(name);
		if(node instanceof NullPointerNode) {
			//System.out.println("RowNode.lookupChild("+name+") finds a NullPointerNode. Returns null");
			//continue;
			return null;
		}
		RowNode rn = (RowNode) node;
		return rn;
/*
		String childsIdFieldName = rn.getIdFieldName();
		if(name.compareTo(childsIdFieldName) == 0) {
			System.out.println("RowNode.lookupChild("+name+") finds a valid RowNode. Returns not null");
			return rn;
		}
		else {
			System.out.println("RowNode.lookupChild("+name+") is not the same as "+childsIdFieldName);
		}
		// The text above means that this should not happen. Must reevaluate while working...
		System.out.println("RowNode.lookupChild - got null on illegal exit. Name = "+name+". Is it correct when the present node is a NullPointerNode?");
		return null;
 */
	}
	
	public void editPressed(boolean setRightSide) {
		EditRowDialog erd = null;
		try { // Några skall sättas nedan.
			erd = new EditRowDialog(ctl, null, null, null, current, currentWindow, this);
			erd.setVisible(true);
		}
		catch(Exception e) {
			UniversalTellUser.error(null, "Unable to edit node", "Edit error");
			ctl.logTrace(e);
			return;
		}
		if(erd.getCancelExit()) {
			//universalTellUser.info(env.mainWindow, env.ths.getName()+" "+toString()+" not edited.");
			return; 
		}
		//Column col = env.ths.getCreateStatement().getFirstAutoIncrementColumn();
		//Object obj = getKeyedValue(col.getName());
		try {
			ro.getClientDBObject().persist();
		}
		catch(Exception e) {
			sqlmessage = "Failed on insert.\n\n"+
			"Exceptions message was "+e.getMessage();
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), sqlmessage); 
			return;
		}
		try {
			if(editorLookup != null) {
				Collection<String> keys = editorLookup.keySet();
				for(String key : keys) {
					GraphicalEditor ge = editorLookup.get(key);
					Object value = ge.getValue();
					ro.setValue(key, value);
					//ro.
					/*
					//Object obj = 
					Object obj = ro.getValue(key.toUpperCase());
					//addPairedValues(key, obj);
					GraphicalEditor ge = editorLookup.get(key);
					System.out.println("RowNode.editPressed Editor of "+key+" set to value "+obj+", editors name "+ge.getName()+", value "+ge.getValue());
					if(obj == null) {
						(new Throwable()).printStackTrace();
					}
					ge.setValue(obj);
					//System.out.println("RowNode.editPressed: changes "+key+" -> "+label.getText());
					
					 */
				}
			}
		}
		catch(Exception e) {
			String message = "RowNode.editPressed "+e.getClass().getName()+"\n"+e.getMessage();
			ctl.log(message, e);
			e.printStackTrace();
			UniversalTellUser.error(((MainWindow) model.getMainViewObject()), message); 
		}	
		
		defineChildNodes();
		if(setRightSide) {
			((MainWindow) model.getMainViewObject()).setRightSide(getRightSide());
		}
	}
	
	public void gotFocus() {
		//System.out.println("RowNode - gotFocus - "+toString());
	}

	/*
	public void addPairedValues(String name, Object value) throws Exception {
		//System.out.println("RowNode.addPairedValue("+name+", "+value+")");
		if(value == null) {
			return;
		}
		Column col = ctl.getEmbeddedEnvironment().ths.getCreateStatement().getColumn(name);
		if(col.isAutoIncrement()) {
			return;
		}
		if(DBTypeManagement.typeCorrect(col, value)) {
			ro.set(name.toUpperCase(), value);
		}
		else {
			String message = "Value <"+value+"> not of type "+col.getType().getTypename();
			ctl.logText(message);
			throw new Exception(message);
		}
		//System.out.println("RowNode<type="+env.ths.getName()+">.addPairedValue("+name+", "+value+") Value class is "+value.getClass().getName());
	}
	*/
	
	public String toString() {
		try {
			return current.renderForInstanceTextRenderer();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "rendering failed";
	}
	
	/*
	public String toCompleteString() {
		CreateStatement cs = ctl.getEmbeddedEnvironment().ths.getCreateStatement();
		Vector<String> mems = new Vector<String>();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			String colname = col.getName();
			if(cs.isForeignKeyDeclarationMember(colname)) {
				mems.addElement(cs.getColumn(i).getName()+" (part of constraint)");
				continue;
			}
			if(col.isAutoIncrement()) {
				mems.addElement(cs.getColumn(i).getName()+" (is autoincrement)");
				continue;
			}
			if(cs.isPartOfPrimaryKeyDeclaration(i)) { //col.isAutoIncrement()) {
				mems.addElement(cs.getColumn(i).getName()+" (part of primary key)");
				continue;
			}
			mems.addElement(cs.getColumn(i).getName());
		}
		if(mems.size() == 0) {
			return "one '"+ctl.getName()+"'";
		}
		StringBuffer sb = new StringBuffer();
		try {
			sb.append(getKeyedValue(mems.elementAt(0)));
			for(int i = 1 ; i < mems.size() ; i++) {
				sb.append("/"+getKeyedValue(mems.elementAt(i)));
			}
		}
		catch(Exception e) {
			sb.append(" :: Broken by exception "+e.getMessage());
		}
		String out = sb.toString();
		return out;
	}
	*/
	
	/*
	public Vector<String> getInsertTitles() {
		Vector<String> keys = new Vector<String>();
		CreateStatement cs = ctl.getEmbeddedEnvironment().ths.getCreateStatement();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column c = cs.getColumn(i);
			if(!c.isAutoIncrement()) {
				keys.add(c.getName());
			}
		}
		return keys;
	}
	*/
	
	/*
	public Object getKeyedValue(String key) {
		//System.out.println("RowNode.getKeyedValue("+key+")");
		CreateStatement cs = ro.getDBTableHandlingSpecification().getCreateStatement();
		Column col = cs.getColumn(key);
		if(col != null) {
			return ro.get(key.toUpperCase());
		}
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
			if(fkd.getName().toUpperCase().compareTo(key.toUpperCase()) == 0) {
				try {
					return ctl.getForeignKey(i, ro.getHashtable());//ro.getForeignKey(i);
				}
				catch(Exception e) {
					return null;
				}
			}
		}
		return null;
	}
	*/
	
	/*
	public int getNoMembers() {
		return ro.getHashtable().size();
	}
	
	public Enumeration<String> getKeys() {
		return ro.getHashtable().keys();
	}

	public boolean hasKeyedValue(String key) {
		return ro.getHashtable().containsKey(key);
	}
	*/
	
	public JPopupMenu getTreePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Edit");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editPressed(true);
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromHere();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Graphical meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showGraphicalMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Textual meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showTextualMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Refresh");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				initialiseYourOwn();
				defineChildNodes();
				DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) ctl.getClientModelLogic().getMainViewObject()).getTree().getModel();
				model.nodeStructureChanged(RowNode.this);
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				delete();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("API for "+ctl.getObjectName());
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showAPI();
			}
		});
		pm.add(menuItem);
		return pm;
	}

	private String getMetaDescription() {
		return current.getMetaDescription();
	}
	
	private void exploreFromHere() {
		try {
			ExplorerWindow ew = new ExplorerWindow(ctl.getClientModelLogic(), null, this);
			ew.setVisible(true);
		} catch (Exception e) {
			ctl.logTrace(e);
			UniversalTellUser.error(null, "Unable to explore from here", "Explorer error");
		}
	}

	private void showTextualMeta() {
		Object[] options = { "OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) model.getMainViewObject()), 
				getMetaDescription(), 
				"SQL Create table",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
	}

	private void showGraphicalMeta() {
		MetaTableNode tn = MetaExplorerWindow.getTableNodeForMetaStructure(ctl);
		MetaExplorerWindow mew = new MetaExplorerWindow(tn);
		mew.setVisible(true);
	}

	private JComponent getFileBrowserManagedButtonsPanel(final GraphicalEditor ge) {
		Font fdef = null;
		Insets idef = null;
		
		JButton copybutton = new JButton("Copy");

		fdef = copybutton.getFont();
		idef = copybutton.getInsets();
		copybutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		copybutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		copybutton.setPreferredSize(new Dimension(50, 20));

		copybutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Clipboard.setClipboard(ge.getValue());
				}
				catch(Exception e) {
					UniversalTellUser.error(ge, e.getMessage());
				}
			}
		});
		
		JButton openbutton = new JButton("Open");

		fdef = openbutton.getFont();
		idef = openbutton.getInsets();
		openbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		openbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		openbutton.setPreferredSize(new Dimension(50, 20));

		openbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			    String s = null;
				try {
					s = ge.getValue().toString();
				}
				catch(Exception e) {
					UniversalTellUser.error(ge, "No applicable value in this field for Open action");
					return;
				}
				Clipboard.desktopOpen(s, ge);
			}
		});
		JPanel jp = new JPanel(new GridLayout(2,1,1,1));
		//copybutton.setPreferredSize(new Dimension(50, 20));
		//openbutton.setPreferredSize(new Dimension(50, 20));
		jp.add(copybutton);
		jp.add(openbutton);
		return jp;
	}

	private JComponent getLobButtonsPanel(final LobTypeEditor lte) {

		Font fdef = null;
		Insets idef = null;
		
		JButton metabutton = new JButton("Meta");
		
		fdef = metabutton.getFont();
		idef = metabutton.getInsets();
		metabutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		metabutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		metabutton.setPreferredSize(new Dimension(50, 20));

		metabutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonGetMetaPressed(lte, ro.getClientDBObject());
				}
				catch(Exception e) {
					ctl.log("RowNode Lob downlood Meta", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});
		
		JButton openbutton = new JButton("Open");
		
		fdef = openbutton.getFont();
		idef = openbutton.getInsets();
		openbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		openbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		openbutton.setPreferredSize(new Dimension(50, 20));

		openbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonOpenPressed(lte, ((MainWindow) model.getMainViewObject()), ro);
				}
				catch(Exception e) {
					ctl.log("RowNode Lob downlood Open", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});
		
		JButton getbutton = new JButton("Save");
		
		fdef = getbutton.getFont();
		idef = getbutton.getInsets();
		getbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		getbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		getbutton.setPreferredSize(new Dimension(50, 20));

		getbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonSavePressed(lte, ((MainWindow) model.getMainViewObject()), ro.getClientDBObject());
				}
				catch(Exception e) {
					ctl.log("RowNode Lob downlood Save", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});
		
		/*
		JButton debugbutton = new JButton("Debug");
		
		fdef = debugbutton.getFont();
		idef = debugbutton.getInsets();
		debugbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		debugbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		debugbutton.setPreferredSize(new Dimension(50, 20));

		debugbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonDebugPressed(lte, ro);
				}
				catch(Exception e) {
					env.log("RowNode Lob downlood Debug", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});
		*/
		JPanel jp = new JPanel(new GridLayout(3,1,1,1));
		//metabutton.setPreferredSize(new Dimension(50, 20));
		//openbutton.setPreferredSize(new Dimension(50, 20));
		//getbutton.setPreferredSize(new Dimension(50, 20));
		//debugbutton.setPreferredSize(new Dimension(50, 20));
		jp.add(metabutton);
		jp.add(openbutton);
		jp.add(getbutton);
		//jp.add(debugbutton);
		//jp.add(debugbutton);
		return jp;
	}
	
	private void showAPI() {
		Object[] options = { "OK", "Copy+OK" }; 
		String api = model.getAPI(ctl.getTableName());
		int choice = JOptionPane.showOptionDialog(((MainWindow) model.getMainViewObject()), 
				api,
				"Class API",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
		if(choice == 1) {
			Clipboard.setClipboard(api);
		}
	}

	public Icon getImageIcon(int size, int usedImageBufferType) {
		return ctl.getRowNodeImageIcon(size, usedImageBufferType);
	}
	
	private int ctr = 0;

	private void setupPanelForMembers(JPanel jp, List<EditorCommons> editors) throws Exception {
		int editorNumber = -1;
		GetGraphicalEditorVisitor ggev = new GetGraphicalEditorVisitor(currentWindow);
		for(EditorCommons editor : editors) {
			editorNumber++;

			editor.accept(ggev);
			GraphicalEditor ge = ggev.getEditor();
			//System.out.println("RowNode.setupPanelForMembers editorNumber="+editorNumber+", klass "+ge.getClass().getName());
			editorLookup.put(editor.getName(), ge);

			JLabel varname = new JLabel(ctl.translate(editor.getName()));
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, varname);

			jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, ge);

			//ctr += ge.rowsUsed();
			//System.out.println("RowNode.setupPanelForMembers ge.rowsUsed()="+ge.rowsUsed()+", ctr "+ctr+" innan uppräkning");

			if(ctl.hasFileBrowserField(editor.getName())) {
				JComponent buttonPanel = getFileBrowserManagedButtonsPanel(ge);
				jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, buttonPanel);
			}
			if(ge instanceof LobTypeEditor) {
				JComponent buttonPanel = getLobButtonsPanel((LobTypeEditor) ge);
				jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, buttonPanel);
			}
			
			
			if(ctl.hasSeparator(editorNumber)) {
				//System.out.println("RowNode.setupPanelForMembers hasSeparator");
				jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JPanel());
				ctr++;
				jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				jp.add("gridx=2,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				ctr++;
			}			
			ctr += ge.rowsUsed();
			//System.out.println("RowNode.setupPanelForMembers ctr "+ctr+" efter uppräkning");
		}
	}

	private void setupPanelForReferrers(JPanel jp) throws Exception {
		List<ReferrerEditorCommons> referrerEditors = current.getReferrerEditorsForEditables();
		for(ReferrerEditorCommons rec : referrerEditors) {
			String apperance = rec.renderAsReferrerInTree();
			JLabel varname = new JLabel(apperance);
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, varname);
			ReferrerGraphicalEditor rge = new ReferrerGraphicalEditor(rec, currentWindow);
			rge.setReferredInstance(this);
			jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, rge);
			jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, new JLabel("Referrers"));
			ctr += rge.rowsUsed();
		}
	}

	public ClientModelLogic getClientModelLogic() {
		return model;
	}

}
