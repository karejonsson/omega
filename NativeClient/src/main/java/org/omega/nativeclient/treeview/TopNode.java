package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Comparator;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultTreeModel;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.util.Sort;
 
public class TopNode extends NodeBase /*DefaultMutableTreeNode*/ {

	private boolean areChildrenDefined = false;
	private MainWindow mainWindow;
	private String title;
	private ClientModelLogic cml = null;
	//private DBPool pool = null;
	private JFrame currentFrame = null;
	
	private Comparator comp = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			TableNode rn0 = (TableNode) arg0;
			TableNode rn1 = (TableNode) arg1;
			return rn0.getName().compareTo(rn1.getName());
		}
	};
	
	public ClientModelLogic getClientModelLogic() { 
		return cml;
	}

	public TopNode(MainWindow mainWindow, String title, ClientModelLogic cml, JFrame currentFrame) {
		this.mainWindow = mainWindow;
		this.title = title;
		this.cml = cml;
		this.currentFrame = currentFrame;
		/*
		hs = _hs;
		conn = _conn;
		rec = _rec;
		slw = _slw;
		si = _si;
		*/
		if(cml == null) {
			new Throwable().printStackTrace();
		}
	}
	
	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}

	public boolean isLeaf() {
		//System.out.println("TopNode.isLeaf");
		return false;
	}

	public int getChildCount() {
		//System.out.println("TopNode.getChildCount");
		if(!areChildrenDefined) {
			defineChildNodes();
		}
		int out = super.getChildCount();
		//System.out.println("TopNode.getChildCount -> "+out);
		return out;
	}

	private void defineChildNodes() {
		//System.out.println("TopNode.defineChildNodes");
		// You must set the flag before defining children if you
		// use "add" for the new children. Otherwise you get an infinite
		// recursive loop, since add results in a call to getChildCount.
		// However, you could use "insert" in such a case.
		//pool = new DBPool(details.getEmbeddedDetails());
		areChildrenDefined = true;
		Vector<TableNode> tns = new Vector<TableNode>();
		//int ctr1 = 0;
		for(ClientTableLogic ths : cml.getClientTableLogicLoopable()) {
			//System.out.println("TopNode.defineChildNodes "+ths.getTableName());
			//System.out.println("1 Table "+ths.getName());
			//ctr1++;
			//inHideTableList(String tablename)
			String name = ths.getTableName();
			if(cml.inHideTableList(name)) {
				continue;
			}
			/*
			NativeClientTableHandling env = null;
			try {
				env = details.getEnvironment(ths, mainWindow);
			} catch (Exception e) {
				continue;
			} 
			*/
			Sort.add(tns, new TableNode(ths, currentFrame), comp);
		} 
		//System.out.println("Ctr1 "+ctr1);
		//int ctr2 = 0;
		for(TableNode tn : tns) {
			//System.out.println("2 Table "+tn.getName());
			//ctr2++;
			add(tn);
		} 
		//System.out.println("Ctr2 "+ctr2);
	}

	public String toString() {
		return title;
	}
	
	public void refresh() {
		removeAllChildren();
		defineChildNodes();
		DefaultTreeModel model = (DefaultTreeModel) mainWindow.getTree().getModel();
		model.nodeStructureChanged(this);
	}
	
	public void about() {
		UniversalTellUser.info(mainWindow, "Model version "+cml.getVersion());
	}
	
	/*
	public DBPool getPool() {
		return pool;
	}
	*/
	
	public JPopupMenu getTreePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Refresh");
		pm.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refresh();
			}
		});
		menuItem = new JMenuItem("About");
		pm.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				about();
			}
		});
		menuItem = new JMenuItem("Meta");
		pm.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MetaTopNode tn = MetaExplorerWindow.getTopNodeForMetaStructure(cml);/*
						details.getModelConfiguration(), 
						details.getRecreator());*/
				MetaExplorerWindow mew = new MetaExplorerWindow(tn);
				mew.setVisible(true);
			}
		});
		return pm;
	}

	public Component getRightSide() {
		return null;
	}

	public void gotFocus() {
	}

	public Icon getImageIcon(final int size, final int usedImageBufferType) {
		return cml.getTopNodeImageIcon(size, usedImageBufferType);
	}

}
