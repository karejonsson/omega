package org.omega.nativeclient.treeview;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;

import org.omega.nativeclient.helpers.BlobActionHelper;
import org.omega.nativeclient.helpers.GetGraphicalEditorVisitor;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import org.omega.nativeclient.typegraphics.LobTypeEditor;
import org.omega.nativeclient.typegraphics.PointerGraphicalEditor;
import org.omega.nativeclient.typegraphics.TextAreaGraphicalEditor;
import org.omega.nativeclient.typegraphics.TextLineGraphicalEditor;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.util.Clipboard;

public abstract class RowDialogCommons extends JDialog {

	protected ClientTableLogic env = null; 
	protected int ctr = 0;
	protected int countTextareas = 0;
	protected Window previousWindow = null;
	protected Hashtable<String, GraphicalEditor> referenceLabels = new Hashtable<String, GraphicalEditor>();
	protected Runnable onInsert = null;
	protected String implicitConstraintname = null;
	protected ClientDBObject referred = null;
	//protected Hashtable<String, GraphicalEditor> fields = new Hashtable<String, GraphicalEditor>();
	
	protected RowDialogCommons(ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, Window previousWindow, String verb) {
		super(
				previousWindow, 
				verb+" "+env.translate(env.translate(env.getTableName())), 
				Dialog.ModalityType.APPLICATION_MODAL
				);
		this.env = env;
		this.previousWindow = previousWindow;
		this.onInsert = onInsert;
		this.implicitConstraintname = implicitConstraintname;
		this.referred = referred;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		}); 
	}
	
	protected abstract void someUpdateHappened();

	protected void refereceSetPressed(final String foreignKeyName) {
		ClientTableLogic referred = env.getClientTableLogicForReferredTable(foreignKeyName);
		ChoseFromFullTableDialog rowChoser = new ChoseFromFullTableDialog(this, referred, "Chose "+referred.translate(referred.getTableName()));
		//System.out.println("RowDialogCommons.setReferencePressed() -- setting -- init");
		GraphicalEditor ge = referenceLabels.get(foreignKeyName);//key);
		//System.out.println("ge "+ge.getName()+"\n"+ge);
		if(ge == null) {
			return;
		}
		if(!(ge instanceof PointerGraphicalEditor)) {
			return;
		}
		RowNode rn = rowChoser.getSelectedRow();
		if((rn == null) && (!rowChoser.getNullPressed())) {
			return;
		}
		
		try {
			ClientDBObject cdbo = rn == null? null : rn.getRowObject().getClientDBObject();
			/*
			System.out.println("RowDialogCommons.setReferencePressed() -- setting -- 1");
			if(cdbo != null) {
				System.out.println("RowDialogCommons.setReferencePressed() -- setting -- ej null");
				System.out.println("foreignKeyName "+foreignKeyName);
				System.out.println("Referred table name "+referred.getTableName());
				System.out.println("Object "+cdbo);
			}
			else {
				System.out.println("RowDialogCommons.setReferencePressed() -- setting -- null");
			}
			*/
			//System.out.println("RowDialogCommons.setReferencePressed() -- setting -- 2");
			ge.setValue(cdbo);
			//System.out.println("RowDialogCommons.setReferencePressed() -- setting -- 3");
			someUpdateHappened();
			//System.out.println("RowDialogCommons.setReferencePressed() -- setting -- 4");
		}
		catch(Exception e) {
			String message = "RowDialogCommons.referenceSetPressed("+foreignKeyName+") "+e.getMessage();
			env.log(message, e);
			UniversalTellUser.error(ge, message);
			e.printStackTrace();
		}
	}
	
	protected JComponent getBrowserButtonsPanel(final GraphicalEditor ge) {
		JButton browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browsePressed(ge);
			}
		});

		JButton clearbutton = new JButton("Clear");
		clearbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ge.setValue(null);
				}
				catch(Exception e) {
					UniversalTellUser.error(ge, e.getMessage());
				}
			}
		});
		JButton copybutton = new JButton("Copy");
		copybutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Clipboard.setClipboard(ge.getValue());
				}
				catch(Exception e) {
					UniversalTellUser.error(ge, e.getMessage());
				}
			}
		});
		JButton openbutton = new JButton("Open");
		openbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Clipboard.desktopOpen(ge.getValue().toString(), RowDialogCommons.this);
				}
				catch(Exception e) {
					UniversalTellUser.error(ge, e.getMessage());
				}
			}
		});
		JPanel jp = new JPanel(new GridLayout(2,2,1,1));

		// Defaultstorleken i fonten blev fel p� Idas windowsmaskin (SBF 20130829)
		Font fdef = null;
		Insets idef = null;

		fdef = copybutton.getFont();
		idef = copybutton.getInsets();
		copybutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		copybutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		copybutton.setPreferredSize(new Dimension(50, 20));

		idef = clearbutton.getInsets();
		fdef = clearbutton.getFont();
		clearbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		clearbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		clearbutton.setPreferredSize(new Dimension(50, 20));

		idef = browsebutton.getInsets();
		fdef = browsebutton.getFont();
		browsebutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		browsebutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		browsebutton.setPreferredSize(new Dimension(50, 20));

		idef = openbutton.getInsets();
		fdef = openbutton.getFont();
		openbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		openbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		openbutton.setPreferredSize(new Dimension(50, 20));

		jp.add(copybutton);
		jp.add(browsebutton);
		jp.add(clearbutton);
		jp.add(openbutton);
		return jp;
	}
	
	protected void browsePressed(GraphicalEditor ge) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Choose to file.");
		fc.setDialogType(JFileChooser.OPEN_DIALOG);
		fc.showOpenDialog(this);
		File selFile = fc.getSelectedFile();
		if(selFile == null) {
			try {
				ge.setValue("");
				someUpdateHappened();
			} 
			catch (Exception e) {
				env.log(e.getMessage(), e);
			}
			return;
		}
		try {
			String path = selFile.getCanonicalPath();//.getAbsolutePath();
			ge.setValue(path);
			someUpdateHappened();
			return;
		} 
		catch (Exception e) {
			env.log(e.getMessage(), e);
		}
		try {
			String path = selFile.getAbsolutePath();
			ge.setValue(path);
			someUpdateHappened();
			return;
		} 
		catch (Exception e) {
			env.log(""+e+" : "+e.getMessage(), e);
		}
	}

	protected void setupPanelForMembers(JPanel jp, List<EditorCommons> editors) throws Exception {
		final Hashtable<String, GraphicalEditor> editorLookup = new Hashtable<String, GraphicalEditor>();
		int editorNumber = -1;
		GetGraphicalEditorVisitor ggev = new GetGraphicalEditorVisitor(/*DBPoolObject po, String name,*/ previousWindow);
		for(EditorCommons editor : editors) {
			
			JLabel varname = new JLabel(env.translate(editor.getName()));
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, varname);
			
			editorNumber++;

			editor.accept(ggev);
			GraphicalEditor ge = ggev.getEditor();
			editorLookup.put(editor.getName(), ge);
			
			//System.out.println("RowDialogCommons.setupPanelForMembers: Klass "+ge.getClass().getName());

			if(ge.isEditable()) {
				ge.setNotifier(
						new Runnable() {
							public void run() {
								someUpdateHappened();
							}} 
						);
			}

			jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, ge);

			if(editor.isHasFileBrowserField()) {
				JComponent buttonPanel = getBrowserButtonsPanel(ge);
				jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, buttonPanel);
				ge.setNotifier(
						new Runnable() {
							public void run() {
								try {
									String blobName = env.getFileBrowserFieldBlobMapping(editor.getName());
									LobTypeEditor lte = (LobTypeEditor) editorLookup.get(blobName);
									fileBrowserFieldWithBlobMappingHappened(lte, ge);
								}
								catch(Exception e) {
									e.printStackTrace();
									UniversalTellUser.error(RowDialogCommons.this, "Error on update");
								}
							}} 
						);
			}

			if(ge instanceof LobTypeEditor) {
				boolean autoupdated = editor.isHasAutoUpdate();
				JComponent buttonPanel = getLobButtonsPanel((LobTypeEditor) ge, autoupdated);
				jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, buttonPanel);
			}
			
			if(ge instanceof PointerGraphicalEditor) {
				JButton setbutton = new JButton("Set it");
				setbutton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						refereceSetPressed(ge.getName());
					}
				});
				jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, setbutton);
				setbutton.setEnabled(ge.isEditable());
			}
			
			ctr += ge.rowsUsed();

			if(env.hasSeparator(editorNumber)) {
				jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JPanel());
				ctr++;
				jp.add("gridx=0,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				jp.add("gridx=2,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.noinsets, new JSeparator(JSeparator.HORIZONTAL));
				ctr++;
			}
			
			//System.out.println("RowDialogCommons: Klass "+ge.getClass().getName()+", namn "+ge.getName());
			referenceLabels.put(ge.getName(), ge);
			
			//ctr += ge.rowsUsed();
		}
	}

	protected void fileBrowserFieldWithBlobMappingHappened(LobTypeEditor lte, GraphicalEditor filenameHolder/*, String filenameField, String blobnameField*/) throws Exception {
		//LobTypeEditor lte = (LobTypeEditor) editorLookup.get(blobnameField);
		//GraphicalEditor filenameHolder = editorLookup.get(filenameField);
		Object filename = null;
		if(filenameHolder instanceof TextAreaGraphicalEditor) {
			filename = ((TextAreaGraphicalEditor) filenameHolder).getValue();
		}
		if(filenameHolder instanceof TextLineGraphicalEditor) {
			filename = ((TextLineGraphicalEditor) filenameHolder).getValue();
		}
		BlobActionHelper.blobButtonSetPressed(lte, (String) filename);
	}
	
	protected void postPersist(ClientDBObject cdbo) {
		if(onInsert != null) {
			onInsert.run();
		}
		try {
			//System.out.println("RowDialogCommons.postPersist("+cdbo+") keySet size "+referenceLabels/*fields*/.keySet().size());
			Vector<LobTypeEditor> leds = new Vector<LobTypeEditor>();
			for(String key : referenceLabels/*fields*/.keySet()) {
				GraphicalEditor ge = referenceLabels/*fields*/.get(key);
				//System.out.println("RowDialogCommons.postPersist("+cdbo+") key "+key+", ge "+(ge != null ? ge.getClass().getName() : "NULL"));
				if(ge instanceof LobTypeEditor) {
					leds.add((LobTypeEditor) ge);
				}
			}
			performPostInsertLobUploads(leds, false);
		}
		catch(Exception e) {
			e.printStackTrace();
			String message = "Failed on inserts upload of lobs.\n\nTable "+env.getTableName()+"\n\n"+
					"Exceptions message was "+e.getMessage();
			env.log(message, e);
			if(env.isUniqueConstraintViolation(e)) {
				UniversalTellUser.error(this, "Unique constraint violation in the "+env.getDBsName()+" database.\n"+message);
			}
			else if(env.isConstraintViolation(e)) {
				UniversalTellUser.error(this, "Constraint violation in the "+env.getDBsName()+" database.\n"+message);
			}
			else {
				UniversalTellUser.error(this, message);
			}
			return;
		}
		try {
			cdbo.executeOnInsert();
		}	
		catch(Exception ie) {
			UniversalTellUser.error(this, ie.getMessage(), "On insert execute exception");
			env.logTrace(ie);
		}
	}

	protected abstract void performPostInsertLobUploads(Vector<LobTypeEditor> leds, boolean clear);
	protected abstract JComponent getLobButtonsPanel(final LobTypeEditor lte, boolean autoupdate);

}
