package org.omega.nativeclient.treeview;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.omega.clientcommon.helpers.IconHelper;

public class IconNodeRenderer extends DefaultTreeCellRenderer {
	  
	  public IconNodeRenderer() {
	  }
	  
	  public Component getTreeCellRendererComponent(JTree tree, Object value,
			  boolean sel, boolean expanded, boolean leaf, int row,
			  boolean hasFocus) {

		  super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				  row, hasFocus);
		  if(value instanceof NodeBase) {
			  setIcon(((NodeBase) value).getImageIcon(IconHelper.size, IconHelper.usedImageBufferType));
			  return this;
		  }
		  return this;
	  }
		  
}
