package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;
import org.omega.clientcommon.editors.ReferrerEditorCommons;
import org.omega.nativeclient.helpers.BlobActionHelper;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import org.omega.nativeclient.typegraphics.LobTypeEditor;
import org.omega.nativeclient.typegraphics.ReferrerGraphicalEditor;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
 
public class EditRowDialog extends RowDialogCommons {
	
	private static final long serialVersionUID = 1L;
	
	private boolean setVisibleFinished = false;
	private boolean insideSomeUpdateHappened = false;

	private ClientDBObject current = null;
	private boolean cancelExit = false;
	private Hashtable<String, GraphicalEditor> referenceLabels = new Hashtable<String, GraphicalEditor>();
	private RowNode treeRowNode = null;
	
	public EditRowDialog(ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, ClientDBObject current, Window previousWindow, RowNode treeRowNode) throws Exception {
		super(env, onInsert, implicitConstraintname, referred, previousWindow, "Edit");
		if(referred != null) {
			env.logText("Starts entering edit "+env.getTableName()+" with key "+implicitConstraintname+" hidden but set");
		}
		else {
			env.logText("Starts entering edit "+env.getTableName()+" with no hidden key");
		}
		this.current = current;
		if(referred != null) {
			current.setReference(implicitConstraintname, referred);
		}
	    env.logText("Starts editing object "+current);
	    this.treeRowNode = treeRowNode;
	}
	
	protected void someUpdateHappened() {
		if(!setVisibleFinished) {
			return;
		}
		if(insideSomeUpdateHappened) {
			return;
		}
		insideSomeUpdateHappened = true;
		current.executeOnSomeUpdateHappened();
		insideSomeUpdateHappened = false;
	}
	
	public void setVisible(boolean visible) {
		if(!visible) {
			super.setVisible(false);
			dispose();
			return;
		}
		final JScrollPane rightView = new JScrollPane();
		JPanel jp = new JPanel();
		rightView.getViewport().add(jp);
	    getContentPane().add(rightView, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());
		try { 
			List<EditorCommons> editors = current.getEditorsForEditables(implicitConstraintname, true);
			setupPanelForMembers(jp, editors);
			
			setupPanelForReferrers(jp);

			ctr++;
			JButton cancelButton = new JButton("Cancel");
			jp.add("gridx=1,gridy="+ctr+",anchor=EAST,"+GdbappInsets.labelInsets, cancelButton);
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelPressed();
				}
			});
			JButton okButton = new JButton("Update+leave");
			jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, okButton);
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					updatePressed();
					setVisible(false);
				}
			});
			pack();
			int h = getSize().height+100*countTextareas+50;
			int w = getSize().width+250;
			setSize(w, h);
			setVisibleFinished = true;
			someUpdateHappened();
			super.setVisible(true);
		}
		catch(Exception ie) {
			env.log(ie.getMessage(), ie);
			UniversalTellUser.error(this, ie.getMessage());  
			//ie.printStackTrace();
			return;
		}
		setVisibleFinished = true;
	}
	
	public void updatePressed() {
		try {
			current.persist();
		}
		catch(Exception e) {
			String message = "Failed to persist!";
			env.log(message, e);
			if(env.isUniqueConstraintViolation(e)) {
				UniversalTellUser.error(this, "Unique constraint violation in the "+env.getDBsName()+" database");
			}
			else if(env.isConstraintViolation(e)) {
				UniversalTellUser.error(this, "Constraint violation in the "+env.getDBsName()+" database");
			}
			else {
				UniversalTellUser.error(this, message);
			}
			return;
		}
		current.clear();
		postPersist(current);
	}
	
	public void cancelPressed() {
		//System.out.println("NewRowDialog Cancel");
		setVisible(false);
		if(current != null) {
	 		current.clear();
		}
		cancelExit = true;
	}
	
	public boolean getCancelExit() {
		return cancelExit;
	}
	
	protected void performPostInsertLobUploads(Vector<LobTypeEditor> leds, boolean clear) {
		for(int i = 0 ; i < leds.size(); i++) {
			LobTypeEditor lte = leds.elementAt(i);
			if(lte.isUnchanged()) {
				continue; 
			}
			if(!current.performLobUploads(lte.getLobEditorCommons(), leds.size()-1-i)) {
				return;
			}
			if(clear) {
				lte.clear();
			}
		}
	}
	
	protected void setupPanelForReferrers(JPanel jp) throws Exception {
		List<ReferrerEditorCommons> referrerEditors = current.getReferrerEditorsForEditables();
		for(ReferrerEditorCommons rec : referrerEditors) {
			String apperance = rec.renderAsReferrerInTree();
			JLabel varname = new JLabel(apperance);
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, varname);
			ReferrerGraphicalEditor rge = new ReferrerGraphicalEditor(rec, previousWindow);
			rge.setReferredInstance(treeRowNode);
			jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, rge);
			jp.add("gridx=2,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, new JLabel("Referrers"));
			ctr += rge.rowsUsed();
		}
	}
	
	protected JComponent getLobButtonsPanel(final LobTypeEditor lte, boolean autoupdate) {

		Font fdef = null;
		Insets idef = null;

		JButton metabutton = new JButton("Meta");

		fdef = metabutton.getFont();
		idef = metabutton.getInsets();
		metabutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		metabutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		metabutton.setPreferredSize(new Dimension(50, 20));

		metabutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonGetMetaPressed(lte, referred);
				}
				catch(Exception e) {
					//e.printStackTrace();
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});
		
		JButton setbutton = new JButton("Set");
		setbutton.setEnabled(!autoupdate);

		fdef = setbutton.getFont();
		idef = setbutton.getInsets();
		setbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		setbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		setbutton.setPreferredSize(new Dimension(50, 20));

		setbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonSetPressed(lte, ((MainWindow) env.getClientModelLogic().getMainViewObject()));//env.getMainWindow());
				}
				catch(Exception e) {
					e.printStackTrace();
					env.log("NewRowDialog.getLobButtonsPanel - Action Set", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});

		JButton getbutton = new JButton("Save");
		
		fdef = getbutton.getFont();
		idef = getbutton.getInsets();
		getbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		getbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		getbutton.setPreferredSize(new Dimension(50, 20));

		getbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonSavePressed(lte, ((MainWindow) env.getClientModelLogic().getMainViewObject()), referred);
				}
				catch(Exception e) {
					//e.printStackTrace();
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});

		JButton nullbutton = new JButton("Null");
		nullbutton.setEnabled(!autoupdate);

		fdef = nullbutton.getFont();
		idef = nullbutton.getInsets();
		nullbutton.setFont(new Font(fdef.getName(), fdef.getStyle(), fdef.getSize()-2));
		nullbutton.setMargin(new Insets(idef.top, 0, idef.bottom, 0));
		nullbutton.setPreferredSize(new Dimension(50, 20));

		nullbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					BlobActionHelper.blobButtonNullPressed(lte, ((MainWindow) env.getClientModelLogic().getMainViewObject()));
				}
				catch(Exception e) {
					e.printStackTrace();
					env.log("NewRowDialog.getLobButtonsPanel - Action Null", e);
					UniversalTellUser.error(lte, e.getMessage());
				}
			}
		});

		JPanel jp = new JPanel(new GridLayout(2,2,1,1));

		jp.add(metabutton);
		jp.add(setbutton);
		jp.add(getbutton);
		jp.add(nullbutton);

		return jp;
	}

}
