package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.EditorCommons;
/*
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.TextualType;
import org.omega.connectivity.helpers.DBTypeManagement;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.nativeclient.helpers.NativeClientTableHandling;
*/
import org.omega.nativeclient.helpers.GetGraphicalEditorVisitor;
import org.omega.nativeclient.typegraphics.GdbappInsets;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class SearchRowDialog extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Hashtable<String, GraphicalEditor> fields = new Hashtable<String, GraphicalEditor>();
	private ClientTableLogic env = null; 
	//private DBPool pool = null;
	private boolean editAfterNew = false;
	private ChoseFromReducedTableDialog cfrtd = null;
	private boolean modality = true;
	
	//public SearchRowDialog(GDBAPPClientEnvironment env, DBPool pool) {
	//	this(env, pool, env.getMainWindow(), true);
	//}
	
	public SearchRowDialog(ClientTableLogic env, boolean modality) {
		this(env, ((MainWindow) env.getClientModelLogic().getMainViewObject()), modality);
	}
	
	//public SearchRowDialog(GDBAPPClientEnvironment env, DBPool pool, Window jw) {
	//	this(env, pool, env.getMainWindow(), true);
	//}
		
	public SearchRowDialog(ClientTableLogic env, Window jw, boolean modality) {
		super(jw, "Search "+env.translate(env.getTableName()));
		this.env = env;
		//this.pool = pool;
		this.modality = modality;
	    addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent windowevent) {
	          setVisible(false);
	          dispose();
	        }
	      });
	    setModal(modality);
	    setVisible(true);
	}
	
	public boolean getEditAfterNew() {
		return editAfterNew;
	}
	
	public void setVisible(boolean visible) {
		if(!visible) {
			super.setVisible(false);
			dispose();
			return;
		}
		final JScrollPane rightView = new JScrollPane();
		JPanel jp = new JPanel();
		rightView.getViewport().add(jp);
	    getContentPane().add(rightView, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());
		try { 
			//CreateStatement cs = env.getEmbeddedEnvironment().ths.getCreateStatement();
			List<EditorCommons> editors = env.getEditorsForEditables(null, true);
			
			int ctr = 0;
			int countTextareas = 0;
			
			//for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			int editorNumber = -1;
			GetGraphicalEditorVisitor ggev = new GetGraphicalEditorVisitor(/*DBPoolObject po, String name,*/ this);

			for(EditorCommons editor : editors) {
				editorNumber++;
				//Column col = cs.getColumn(i);
				//String colname = col.getName();
				//org.omega.configuration.struct.Type t = col.getType();
				editor.accept(ggev);
				GraphicalEditor ge = ggev.getEditor();

				//if(t instanceof TextualType) {
				if(ge.isTextual()) {
					JLabel varname = new JLabel(env.translate(ge.getName()));
					jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, varname);
					//final GraphicalEditor ge = t.getGraphicalComponent(colname);
					//GraphicalEditor ge = GetGraphicalEditorVisitor.getGraphicalEditor(null, colname, t, env);
					//Object defaultValue = DBTypeManagement.getDefault(col);
					//ge.setInitialValue(defaultValue);
					jp.add("gridx=1,gridy="+ctr+",fill=HORIZONTAL,weightx=1.0,"+GdbappInsets.insets, ge);
					fields.put(ge.getName(), ge);
					//ge.setEditable(true);
					ctr++;
				}

			}
			JPanel leftButtonPanel = new JPanel();
			leftButtonPanel.setLayout(new StringGridBagLayout());
			JButton see = new JButton("Search");
			leftButtonPanel.add("gridx=1,gridy=0,anchor=EAST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					searchPressed();
				}
			});
			see = new JButton("Quit");
			leftButtonPanel.add("gridx=0,gridy=0,anchor=EAST,"+GdbappInsets.labelInsets, see);
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					quitPressed();
				}
			});
			jp.add("gridx=0,gridy="+ctr+",anchor=WEST,"+GdbappInsets.labelInsets, leftButtonPanel);

			pack();
			int h = getSize().height+100*countTextareas+50;
			int w = getSize().width+250;
			setSize(w, h);
			super.setVisible(true);
		}
		catch(Exception ie) {
			env.log(ie.getMessage(), ie);
			UniversalTellUser.error(this, ie.getMessage());  
			ie.printStackTrace();
			return;
		}
	}
	
	private void searchPressed() {
		Hashtable<String, Object> ht = new Hashtable<String, Object>();
		
		try {
			for(String name : fields.keySet()) {
				GraphicalEditor ge = fields.get(name);
				Object obj = ge.getValue();
				if(obj != null) {
					//Object obj = ge.getValue();
					ht.put(name.toUpperCase(), obj);
				}
			}
		}
		catch(Exception e) {
			String message = "SearchRowDialog.searchPressed Type "+e.getClass().getName()+"\n"+e.getMessage();
			env.log(message, e);
			if(env.isUniqueConstraintViolation(e)) {
				UniversalTellUser.error(this, "Unique constraint violation in the "+env.getDBsName()+" database");
			}
			else if(env.isConstraintViolation(e)) {
				UniversalTellUser.error(this, "Constraint violation in the "+env.getDBsName()+" database");
			}
			else {
				UniversalTellUser.error(this, message);
			}
			return;
		}
		Vector<ClientDBObject> foundPoolObjects = null;
		try {
			foundPoolObjects = env.searchPoolObjects(ht);
		}
		catch(Exception e) {
			String message = "Failed on search.\n\nTable "+env.getTableName()+"\n\n"+
			"Exceptions message was "+e.getMessage();
			env.log(message, e);
			UniversalTellUser.error(((MainWindow) env.getClientModelLogic().getMainViewObject()), message); 
			return;
		}
		cfrtd = new ChoseFromReducedTableDialog(this, foundPoolObjects, "Result from search", env, modality); 
		// DBPoolObject po = cfrtd.getChosenObject(); // No. After setVisibla(false) execution continues at getChosenObejct() below
		if(modality) {
			setVisible(false);
		}
	}
	
	public void quitPressed() {
		//System.out.println("NewRowDialog.donePressed");
		setVisible(false);
		dispose();
		cfrtd = null;
	}
	
	public ClientDBObject getChosenObject() {
		if(cfrtd == null) {
			return null;
		}
		return cfrtd.getChosenObject();
	}
	
	
}
