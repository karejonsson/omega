package org.omega.nativeclient.treeview;
 
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.bsf.BSFException;
import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.connectivity.helpers.GeneralComparator;
/*
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.renderers.DefaultTablenodeTextRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.connectivity.helpers.GeneralComparator;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.GenerateJavaAPI;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.nativeclient.helpers.NativeClientModelHandling;
import org.omega.nativeclient.helpers.NativeClientTableHandling;
*/
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.configuration.model.TableConfiguration;

import se.modlab.generics.util.Sort;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.plot.TimePlotComponent;
import se.modlab.generics.gui.plot.TimePlotDataContainer;
import se.modlab.generics.gui.util.Clipboard;

public class TableNode extends NodeBase {

	private ClientTableLogic env;
	//private DBPool pool = null;
	private String sqlmessage = null; 
	private String othermessage = null; 
	private JTable table = null;
	//private TextRenderer instanceRenderer = null;
	private int sortcolumn = -1;
	private boolean sortInverted = false;
	private Vector<RowNode> tableRowsOrder = null;//new Vector<RowNode>();
	private JFrame currentFrame = null;

	public TableNode(ClientTableLogic env, JFrame currentFrame) {
		this.env = env;
		//pool = _pool;
		this.currentFrame = currentFrame;
		/*
		instanceRenderer = env.getEmbeddedEnvironment().ths.getClassTextRenderer();
		if(instanceRenderer == null) {
			instanceRenderer = new DefaultTablenodeTextRenderer(env.getEmbeddedEnvironment().ths);
		}
		*/
	} 
	
	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}

	public ClientTableLogic getEnvironment() {
		return env;
	}

	public ClientModelLogic getDetails() {
		return env.getClientModelLogic();
		/*
		try {
			return env.getClientDetails();
		} catch (Exception e) {
			env.log("Unable to get details for class ", e);
		}
		return null;
		*/
	}
	
	public String getName() {
		return env.getTableName();
	}

	public boolean isLeaf() {
		return(false);
	}

	public int getChildCount() {
		if (!areChildrenDefined) {
			defineChildNodes();
		}
		return(super.getChildCount());
	}

	private void defineChildNodes() {
		areChildrenDefined = true;
		othermessage = null;
		sqlmessage = null;
		//System.out.println("1");
		try {
			//Vector<DBPoolObject> rows = pool.getPoolObjects(env.getEmbeddedEnvironment().ths);
			List<DataNode> wholeTable = env.getAll();
			//System.out.println("2");
			removeAllChildren();
			//System.out.println("3");
			/*
			for(int i = 0 ; i < rows.size() ; i++) {
				//System.out.println("4");
				DBPoolObject ro = rows.elementAt(i);
				//System.out.println("TableNode.defineChildNodes("+i+") = "+ro);
				RowNode rn = new RowNode(pool, env, ro, currentFrame);
				//System.out.println("6");
				add(rn);
				//System.out.println("7");
			}
			*/
			for(DataNode dn : wholeTable) {
				add(new RowNode(dn, null, currentFrame));
			}
			//System.out.println("8");

			if(table == null) {
				//System.out.println("TableNode.defineChildNodes - table = null ");
				final ResultSetTableModel model = new ResultSetTableModel();
				model.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent arg0) {
						//System.out.println("TableNode.defineChildNodes - tableChanged "+arg0);
					}
				});
				table = new JTable(model);
			    table.addMouseListener(new PopupListener()); 
			    
			    TableColumnModelListener tableColumnModelListener = new TableColumnModelListener() {
			        public void columnAdded(TableColumnModelEvent e) {
			          //System.out.println("TableNode.column Added");
			        }

			        public void columnMarginChanged(ChangeEvent e) {
			          //System.out.println("TableNode.column Margin");
			        }

			        public void columnMoved(TableColumnModelEvent e) {
			          int from = e.getFromIndex();
			          int to = e.getToIndex();
			          sortInverted = to == sortcolumn ? !sortInverted : false;
			          sortcolumn = to;
			          rearrangeTableOrder();
			          model.fireTableStructureChanged();
			        }

			        public void columnRemoved(TableColumnModelEvent e) {
			          //System.out.println("TableNode.column Removed");
			        }

			        public void columnSelectionChanged(ListSelectionEvent e) {
			          //System.out.println("TableNode.column Selection Changed");
			        }
			      };

			      TableColumnModel columnModel = table.getColumnModel();
			      columnModel.addColumnModelListener(tableColumnModelListener);
			}
		}
		catch(SQLException sqle) {
			String message = sqle.getMessage();
			sqlmessage = message;
			if(message == null) {
				message = ""+sqle;
			}
			env.log(message, sqle);
		}
		catch(Exception e) {
			String message = e.getMessage();
			othermessage = message;
			if(message == null) {
				message = ""+e;
			}
			env.log(message, e);
		}
	}


	public String toString() {
		try {
			/*
			ScriptingScope os = env.getDetails().getScopeFactory().getNodelessScope();
			return os.eval(instanceRenderer).toString();
			*/
			return env.renderForClassText();
		}
		catch(Exception e) {
			env.logTrace(e);
			return "<internal error>";
		}
	}
	
	public void onNew() { 
		//System.out.println("Table node on new "+env.ths.getName());
		NewRowDialog nrd = null;
		try {
			nrd = new NewRowDialog(env, null, null, null, currentFrame);
		} catch (Exception e) {
			UniversalTellUser.error(null, "Unable to create new instance", "Internal error");
			env.logTrace(e);
			return;
		}
		nrd.setVisible(true);
		//System.out.println("TableNode.onNew - back in control");
		defineChildNodes();
		DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) env.getClientModelLogic().getMainViewObject()).getTree().getModel();
		model.nodeStructureChanged(this);

		if(table != null) {
			((ResultSetTableModel) table.getModel()).clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}

		((MainWindow) env.getClientModelLogic().getMainViewObject()).invalidate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).validate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).repaint();
		//System.out.println("TableNode.onNew - Redigera efter insert "+nrd.getEditAfterNew());
		if(nrd.getEditAfterNew()) {
			try {
				ClientDBObject createdClientDbObject = nrd.getCreatedClientDBObject();
				//PrimaryKey pk = PrimaryKey.assemblePrimaryKey(nrd.getCreatedHashtable(), env.getEmbeddedEnvironment().ths.getCreateStatement().getPrimaryKeyDeclaration());
				//System.out.println("TableNode.onNew pk="+pk);
				for(int i = 0 ; i < getChildCount() ; i++) {
					RowNode child = (RowNode) getChildAt(i);
					if(child.sameObject(createdClientDbObject)) {//.primaryKeyEquals(pk)) {
						child.editPressed(true);

						if(table != null) {
							((ResultSetTableModel) table.getModel()).clear();
							table.invalidate();
							table.validate();
							table.repaint();
						}

						((MainWindow) env.getClientModelLogic().getMainViewObject()).invalidate();
						((MainWindow) env.getClientModelLogic().getMainViewObject()).validate();
						((MainWindow) env.getClientModelLogic().getMainViewObject()).repaint();
					}
				}
			} 
			catch (Exception e) {
				env.log(e.getMessage(), e);
				//e.printStackTrace();
			}
		}
	}

	public void onEdit() { 
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = (RowNode) getChildAt(rowno);
		rn.editPressed(true);

		if(table != null) {
			((ResultSetTableModel) table.getModel()).clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}

		((MainWindow) env.getClientModelLogic().getMainViewObject()).invalidate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).validate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).repaint();
	}

	public void onOpen() { 
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = getNodeAtPlaceForTable(rowno);
		//int row = rn.
		JTree tree = ((MainWindow) env.getClientModelLogic().getMainViewObject()).getTree();
		TreePath tp = new TreePath(rn.getPath());
		tree.setSelectionPath(tp);
		tree.expandPath(tp);
		tree.makeVisible(tp);
	}

	private void exploreFromTablesSelected() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = getNodeAtPlaceForTable(rowno);
		try {
			ExplorerWindow ew = new ExplorerWindow(env.getClientModelLogic(), null, rn);
			ew.setVisible(true);
		}
		catch(Exception e) {
			env.log("Failed to create explorer window.", e);
			UniversalTellUser.error(null, "Unable to create explorer window", "Error");
		}
	}
	
	private String getSuitableTextualMessage() {
		if(sqlmessage == null && othermessage == null) {
			return "Right side "+getClass().getName()+"\n\nInstance "+this.toString()+
					"\n\nSomething went wrong with getting table."+"\n";
		}
		StringBuffer sb = new StringBuffer(); 
		if(sqlmessage != null) {
			sb.append(sqlmessage);
			if(othermessage != null) {
				sb.append("\n");
			}
		}
		if(othermessage != null) {
			sb.append(othermessage);
		}
		return sb.toString();
	}

	public Component getRightSide() {
		//System.out.println("TableNode - getRightSide");
		if (!areChildrenDefined) {
			defineChildNodes();
		}
		if(table != null) {
			return table;
		}
		JTextArea ta = new JTextArea();
		ta.setText(getSuitableTextualMessage());
		ta.setEditable(false);
		return ta;
	}

	public void refresh() {
		//System.out.println("TableNode.refresh type "+env.ths.getName());
		defineChildNodes();
		if(table != null) {
			table.invalidate();
			table.validate();
			table.repaint();
		}
		MainWindow mw = (MainWindow) env.getClientModelLogic().getMainViewObject();
		JTree t = mw.getTree();
		DefaultTreeModel model = (DefaultTreeModel) t.getModel();
		//DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) env.getClientModelLogic().getMainViewObject()).getTree().getModel();
		model.nodeStructureChanged(this);

		((MainWindow) env.getClientModelLogic().getMainViewObject()).invalidate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).validate();
		((MainWindow) env.getClientModelLogic().getMainViewObject()).repaint();
	}

	/*
	public void onNew() {
		System.out.println("TableNode on new "+env.ths.getName());
	}
    */
	/*
	public DBTableHandlingSpecification get_Type() {
		System.out.println("TableNode - getType - return null");
		return null;
	}
    */
	public void gotFocus() {
		//System.out.println("TableNode - Focus on table node "+this);
		refresh();
	}
	
	private void rearrangeTableOrder() {
		//System.out.println("TableNode.rearrangeTableOrder");
		//(new Throwable()).printStackTrace();
		if(tableRowsOrder == null) {
			tableRowsOrder = new Vector<RowNode>();
		}
		if(tableRowsOrder.size() != getChildCount()) {
			//System.out.println("TableNode.rearrangeTableOrder initierar Vectorn");
			tableRowsOrder.removeAllElements();
			for(int i = 0 ; i < getChildCount() ; i++) {
				tableRowsOrder.addElement((RowNode) getChildAt(i));
			}
		}
		if(sortcolumn == -1) {
			return;
		}
		Comparator<Object> comp = new Comparator<Object>() {
			private GeneralComparator gp = new GeneralComparator();
			public int compare(Object arg0, Object arg1) {
				String s1 = getTableStringToPresent((RowNode) arg0, sortcolumn);
				String s2 = getTableStringToPresent((RowNode) arg1, sortcolumn);
				int out = gp.compare(s1, s2);
				if(sortInverted) {
					out = -out;
				}
				return out;
			}
		};
		Vector<RowNode> tmp = new Vector<RowNode>();
		for(int i = 0 ; i < tableRowsOrder.size(); i++) {
			//System.out.println("TableNode.rearrangeTableOrder sorterar Vectorn "+tmp.size());
			Sort.add(tmp, tableRowsOrder.elementAt(i), /*getNodeAtPlaceForTable(i),*/ comp);
		}
		tableRowsOrder = tmp;
	}
	
	private RowNode getNodeAtPlaceForTable(int row) {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		//return (RowNode) getChildAt(row);
		return tableRowsOrder.elementAt(row);
	}
	
	private int getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
			return tableRowsOrder.size();
		}
		int len = -1;
		try {
			len = env.getTableLength();//pool.getTableLength(env.getEmbeddedEnvironment().ths);
		}
		catch(Exception e) {
		}
		if(tableRowsOrder.size() != len) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.size();
		//return getChildCount();
	}
	
	/*
	private ScriptingScope getScope(int row) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		RowNode rn = getNodeAtPlaceForTable(row);
		return env.getDetails().getScopeFactory().getNodeScope(rn.getRowObject(), true);
	}
	*/
	
	private String getTableStringToPresent(int r, int c) {
		if(r >= getRowCountForTable()) {
			//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> <Out of bounds>");
			return "Out of bounds ("+r+" >= "+getRowCountForTable()+")";
		}
		return getTableStringToPresent(getNodeAtPlaceForTable(r), c);
	}
	
	private String getTableStringToPresent(RowNode rn, int c) {
		//System.out.println("TableNode.getTableStringToPresent( . , "+c+")");
		/*
		ScriptingScope s = null;
		try {
			s = env.getDetails().getScopeFactory().getNodeScope(rn.getRowObject(), true);
		} catch (Exception e) {
			env.logTrace(e);
		}
		if(s == null) {
			return "<no information>";
		}
		*/
		//TextRenderer se = env.getEmbeddedEnvironment().ths.getTableRenderer().getColumnRenderer(c);
		DataNode dn = rn.getRowObject();
		String obj = null;
		try {
			obj = dn.renderForTableAtPosition(c);
		}
		catch(Exception ie) {
			env.log("Cannot render "+rn+" for position "+c, ie);
			obj = "<no information>";
		}
		if(obj == null) {
			obj = "<not set>";
		}
		return obj.toString();
	}

	private class ResultSetTableModel extends AbstractTableModel
	{  		
		public ResultSetTableModel() {
		}
		
		public void clear() {
			//sht.clear();
			rearrangeTableOrder();
			this.fireTableDataChanged();
		}
		
		public String getColumnName(int c) {
			//String name = env..getEmbeddedEnvironment().ths.getTableRenderer().getColumnName(c);//colnames.elementAt(c);
			//return env.translate(name);
			return env.getColumnNameTranslated(c);
		}

		public int getColumnCount() { 
			//int count = env.ths.getCreateStatement().getNoColumns();
			//System.out.println("ResultSetTableModel count = "+count);
			return env.getColumnCount();//.getEmbeddedEnvironment().ths.getTableRenderer().getNoColumns();
		}

		public Object getValueAt(int r, int c) {
			return getTableStringToPresent(r, c);
		}

		public int getRowCount() {
			int r = getRowCountForTable();
			//System.out.println("ResultSetTableModel getRowCount() = "+r);
			return r;
		}
	} 
	
	private void exportToClipboard() {
		Clipboard.export((AbstractTableModel) table.getModel());
	}


  public class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

	public void mouseClicked(MouseEvent e) {
		//System.out.println("TableNode.PopupListener.mouse listener clicked "+e);
        if (e.getComponent().isEnabled() && e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
        	//System.out.println("TableNode.PopupListener.mouse listener dubbelklick ###");
    		int rowno = table.getSelectedRow();
    		if(rowno == -1) {
    			return;
    		}
    		RowNode rn = getNodeAtPlaceForTable(rowno);
    		//int row = rn.
    		JTree tree = ((MainWindow) env.getClientModelLogic().getMainViewObject()).getTree();
    		TreePath tp = new TreePath(rn.getPath());
    		tree.setSelectionPath(tp);
    		tree.expandPath(tp);
    		tree.makeVisible(tp);
        }
	}

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      //System.out.println("TableNode.PopupListener.maybeShowPopup "+e.getComponent().getClass().getName());
      if(e.isPopupTrigger()) 
      {
        //System.out.println("TableNode.PopupListener.maybeShowPopup trigger -> "+e.getComponent().getClass().getName());
        if(e == null) return;
        JTable table= (JTable) e.getComponent();
        if(table == null) return;
        JPopupMenu popup = getTablePopup();
        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
      }
    }

  }

	public JPopupMenu getTablePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("New "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onNew();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Edit this "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onEdit();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Open this "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onOpen();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromTablesSelected();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int rowno = table.getSelectedRow();
				if(rowno == -1) {
					return;
				}
				RowNode rn = (RowNode) getChildAt(rowno);
				/*
				DBRowObject ro = rn.getRowObject();
				try {
					pool.removeRowObject(ro);
				}
				catch(Exception e) {
					universalTellUser.error(table, e.getMessage());
				}
				*/
				rn.delete();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Export to clipboard");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exportToClipboard();
			}
		}); 
		pm.add(menuItem);
		final int datetimeColumnNumber = env.getFirstDatetimeColumnNumer();//env.getEmbeddedEnvironment().ths.getCreateStatement().getFirstDatetimeColumnNumber();
		if(datetimeColumnNumber == -1) {
			return pm;
		}
		final int idxs[] = env.getNumericColumnsNumbers();//.getEmbeddedEnvironment().ths.getCreateStatement().getPossibleValueColumnIndexes();
		if((idxs == null) || (idxs.length == 0)) {
			return pm;
		}
		// We can auto generate some plots
		for(int i = 0 ; i < idxs.length ; i++) {
			/*
			final Column col = env.getEmbeddedEnvironment().ths.getCreateStatement().getColumn(idxs[i]);
			if(!env.getEmbeddedEnvironment().ths.getTableRenderer().hasColumn(col.getName())) {
				continue;
			}
			*/
			final int idx = idxs[i];
			String colname = env.getColumnName(idx);
			if(colname == null) {
				continue;
			}
			menuItem = new JMenuItem("Plot "+env.translate(colname));
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					plot(datetimeColumnNumber, idx, colname);
				}
			});
			pm.add(menuItem);
		}
		return pm;
	} // getPossibleValueColumnIndexes

	private void plot(int datetimeColumnNumber, int idx, String name) {
		TimePlotDataContainer tp = new TimePlotDataContainer(name);
		//...
		ResultSetTableModel model = (ResultSetTableModel) table.getModel();
		int rowcount = model.getRowCount();
		for(int r = 0 ; r < rowcount ; r++) {
			String datetext = model.getValueAt(r, datetimeColumnNumber).toString();
			String valtext = model.getValueAt(r, idx).toString();
			long time = CommonSQLAssembler.getDateFromString(datetext).getTime();
			double value = Double.parseDouble(valtext);
			tp.add(time, value);
		}

		//...
		final JFrame frame = new JFrame(tp.getName());
		frame.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          frame.setVisible(false);
		          frame.dispose();
		        }
		      });
		frame.getContentPane().add(new TimePlotComponent(tp), BorderLayout.CENTER);
		try {
			frame.pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(((MainWindow) env.getClientModelLogic().getMainViewObject()), ie.getMessage());
			return;
		}
		frame.setSize(500,350);
		frame.setVisible(true);
	}
	
	public JPopupMenu getTreePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("New "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onNew();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Graphical meta "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showGraphicalMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Textual meta "+env.translate(env.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showTextualMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Refresh");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refresh();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Delete");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				delete();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("API for "+env.getObjectName());
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showAPI();
			}
		});
		pm.add(menuItem);
		return pm;
	}
	
	private String getMetaDescription() {
		return env.getMetaDescription();
	}

	private void delete() {
		Object[] options = { "OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) env.getClientModelLogic().getMainViewObject()), 
				"You cannot delete a table.\nChange general DB application configuration.", 
				"Delete table",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
	}

	private void showTextualMeta() {
		Object[] options = { "OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) env.getClientModelLogic().getMainViewObject()), 
				getMetaDescription(), 
				"SQL Create table",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
	}
	
	private void showAPI() {
		String api = env.getAPI();
		Object[] options = { "OK", "Copy+OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) env.getClientModelLogic().getMainViewObject()), 
				api,//env.getDetails().getScopeFactory().getPackagename(), env.getEmbeddedEnvironment().ths), 
				"Class API",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
		if(choice == 1) {
			Clipboard.setClipboard(api);
		}
	}

	private void showGraphicalMeta() {
		MetaTableNode tn = MetaExplorerWindow.getTableNodeForMetaStructure(env);
				/*pool.getDetails().getModelConfiguration(), 
				env.getName(), 
				pool.getDetails().getRecreator());*/
		MetaExplorerWindow mew = new MetaExplorerWindow(tn);
		mew.setVisible(true);
	}

	public Icon getImageIcon(int size, int usedImageBufferType) {
		return env.getTableNodeImageIcon(size, usedImageBufferType);
	}

	public ClientModelLogic getClientModelLogic() {
		return env.getClientModelLogic();
	}

}

