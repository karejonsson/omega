package org.omega.nativeclient.treeview;

import java.awt.Component;
import java.awt.Window;

import javax.swing.Icon;

import org.omega.clientcommon.editors.ClientModelLogic;

public class MetaTopNode extends NodeBase {
	
	private ClientModelLogic hs = null;
	
	public MetaTopNode(ClientModelLogic hs) {
		this.hs = hs;
	}
	
	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}

	public Component getRightSide() {
		return null;
	}

	public void gotFocus() {
	}

	public String toString() {
		return "Top model meta";
	}

	public ClientModelLogic getClientModelLogic() {
		return hs;
	}

	public Icon getImageIcon(int size, int usedImageBufferType) {
		//Exception e = new Exception("Borde inte ropa på denna. MetaTopNode.getImageIcon()");
		//e.printStackTrace();
		return null;
	}

}
