package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.nativeclient.helpers.SQLIntermediateLogWindow;
import org.omega.nativeclient.helpers.SQLMemoryTextlogWindow;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class ExplorerWindow extends MainWindow {
	
	final private static String textlogwindowlabel = "LogWindowLabel";
	final private static String sqllogwindowlabel = "SQLWindowLabel";

	private JScrollPane rightView = new JScrollPane();
	private JScrollPane leftView = new JScrollPane();

	private ClientModelLogic details[] = null;
	private JTree tree = null;
	private NodeBase lastFocusedNode = null;
	private NodeBase tn[] = null;
	private JMenu extras[];
	private boolean closeAtEnd = true;
	private SQLIntermediateLogWindow silw = null;
	private SQLMemoryTextlogWindow smtlw = null;

	private void setupLogging(ClientModelLogic cml) {
		silw = (SQLIntermediateLogWindow) cml.getObject(textlogwindowlabel);
		if(silw == null) {
			silw = new SQLIntermediateLogWindow(cml.getSQLIntermediate());
			cml.setObject(textlogwindowlabel, silw);
		}
		
		smtlw = (SQLMemoryTextlogWindow) cml.getObject(sqllogwindowlabel);
		if(smtlw == null) {
			smtlw = new SQLMemoryTextlogWindow(false, cml.getSQLMemoryLog());
			cml.setObject(sqllogwindowlabel, smtlw);
		}
	}
	
	private void setupMainViewObject() {
		for(ClientModelLogic detail : details) {
			detail.setMainViewObject(this);
		}
	}
	
	/**
	 * Constructor for the action "Explore from here"
	 * @param _details
	 * @param _extras
	 * @param _tn
	 * @throws Exception 
	 */
	public ExplorerWindow(ClientModelLogic details, JMenu extras[], NodeBase nb) throws Exception {
		super(details.getFrametitle());
		this.tn = new NodeBase[] { nb.cloneForOtherWindow(this) };
		this.details = new ClientModelLogic[] { details };
		this.extras = extras;
		setupLogging(details);
		setupMainViewObject();
		init();
		closeAtEnd = false;
		//System.out.println("ExplorerWindow.<ctro> 1 - slut");
	}
	
	/**
	 * Constructor for general start action
	 * @param _details
	 * @param _extras
	 * @throws Exception 
	 */
	public ExplorerWindow(ClientModelLogic details, JMenu extras[]) throws Exception {
		super(details.getFrametitle());
		this.tn = new NodeBase[] { new TopNode(this, details.getFrametitle(), details, this) };
		this.details = new ClientModelLogic[] { details };
		this.extras =  extras;
		setupLogging(details);
		setupMainViewObject();
		init();
		//System.out.println("ExplorerWindow.<ctro> 2 - slut");
	}

	public ExplorerWindow(ClientModelLogic details[], JMenu extras[]) {
		super("Omega Multi DB View");
		this.tn = new NodeBase[details.length];
		this.details = new ClientModelLogic[details.length];
		for(int i = 0 ; i < details.length ; i++) {
			this.tn[i] = new TopNode(this, details[i].getFrametitle(), details[i], this);
			this.details[i] = details[i];
		}
		setupLogging(details[0]);
		setupMainViewObject();
		this.extras =  extras;
		init();
		//System.out.println("ExplorerWindow.<ctro> 3 - slut");
	}

	private void init() {
		//System.out.println("ExplorerWindow.init - början");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				try {
					if(closeAtEnd) {
						for(int i = 0 ; i < details.length ; i++) {
							details[i].close();
						}
					}
				}
				catch(Exception e) {
					// Nothing to do. Must quit anyway.
				}
				dispose();
			}
		});
		for(int i = 0 ; i < details.length ; i++) {
			details[i].performPrimaryKeyMissingTablesElimination();
		}
		setMenuBar();
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setTopComponent(leftView);
		splitPane.setBottomComponent(rightView);

		Dimension minimumSize = new Dimension(100, 50);
		rightView.setMinimumSize(minimumSize);
		leftView.setMinimumSize(minimumSize);
		splitPane.setDividerLocation(300);
		splitPane.setPreferredSize(new Dimension(700, 500));

		getContentPane().add(splitPane, BorderLayout.CENTER);

		//rightView.setViewportView(c);
		//leftView.getViewport().setView;
		createTree();
		tree.addMouseListener(new PopupListener());   
		leftView.getViewport().setView(tree);
		//jp.setLayout(new StringGridBagLayout());
		//updateJMenuBar();
		try {
			pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(this, ie.getMessage());
			return;
		}
		//System.out.println("ExplorerWindow.init - slut");
	}

	private void createTree() {
		if(tn.length == 1) {
			tree = new JTree(tn[0]);
		}
		else {
			DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("Omega");
			for(int i = 0 ; i < tn.length ; i++) {
				dmtn.add(tn[i]);
			}
			tree = new JTree(dmtn);
		}
		final JTree _tree = tree;
		
		_tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    _tree.addTreeSelectionListener(new TreeSelectionListener() {
	        public void valueChanged(TreeSelectionEvent e) {
	          DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
	          //System.out.println("You selected " + node+" of class "+node.getClass().getName()+" is NodeBase = "+(node instanceof NodeBase));
	          if(node instanceof NodeBase) {
	        	  lastFocusedNode = (NodeBase) node;
	        	  lastFocusedNode.gotFocus();
	        	  setRightSide(lastFocusedNode.getRightSide());
	          }
	        }
	      });
	    _tree.addTreeWillExpandListener(new TreeWillExpandListener (){

			public void treeWillCollapse(TreeExpansionEvent evt)
					throws ExpandVetoException {
			}

			public void treeWillExpand(TreeExpansionEvent evt)
					throws ExpandVetoException {
				JTree tree = (JTree)evt.getSource();  
				TreePath path = evt.getPath(); 
				tree.setSelectionPath(path);
			}
	    	
	    }); 
	      
	    tree.setCellRenderer(new IconNodeRenderer());
	}
	
	private void setMenuBar() {
		JMenuBar mb = new JMenuBar();
		setJMenuBar(mb);
		JMenu file = new JMenu("File");
		mb.add(file);
		JMenuItem refresh = new JMenuItem("Refresh");
		file.add(refresh);
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//ScopeBuilder.resetDBScope();
				for(int i = 0 ; i < tn.length ; i++) {
					tn[i].refresh();
				}
			}
		});
		JMenuItem exit = new JMenuItem("Exit");
		file.add(exit);
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        setVisible(false);
		        try {
		        	if(closeAtEnd) {
						for(int i = 0 ; i < details.length ; i++) {
							details[i].close();
						}
		        	}
		        }
		        catch(Exception ex) {
		      	  // Nothing to do. Must quit anyway.
		        }
		        dispose();
			}
		});
		JMenu formats = new JMenu("Formats");
		mb.add(formats);
		JMenuItem datetime = new JMenuItem("Datetime");
		formats.add(datetime);
		datetime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StringBuffer sb = new StringBuffer();
				for(int i = 0 ; i < CommonSQLAssembler.formats.length ; i++) {
					sb.append(CommonSQLAssembler.formats[i]+"\n");
				}
				Object[] options = { "OK" }; 
				int choice = JOptionPane.showOptionDialog(ExplorerWindow.this, 
						sb.toString(), 
						"Datetime formats to use",
						JOptionPane.DEFAULT_OPTION, 
						JOptionPane.INFORMATION_MESSAGE,
						null, 
						options, 
						options[0]);

			}
		});
		if(extras != null) {
			for(int i = 0 ; i < extras.length ; i++) {
				mb.add(extras[i]);
			}
		}
		
		final String actionSQLIntermediateLog = "ACTION SQL intermediate log";
		JMenu functions = new JMenu("Functions");
		mb.add(functions);
		if(details.length == 1) {
			// Avoid confusing intermediate node when only one connection
			final JMenuItem openGUI = new JMenuItem(actionSQLIntermediateLog.replaceAll("ACTION", "Open"));
			functions.add(openGUI);
			final JMenuItem closeGUI = new JMenuItem(actionSQLIntermediateLog.replaceAll("ACTION", "Close"));
			functions.add(closeGUI);
			openGUI.setEnabled(!sqlIntermediateLogIsActive());
			closeGUI.setEnabled(sqlIntermediateLogIsActive());
			openGUI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					sqlIntermediateLogOpenGUI();
					openGUI.setEnabled(!sqlIntermediateLogIsActive());
					closeGUI.setEnabled(sqlIntermediateLogIsActive());
				}
			});
			closeGUI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					sqlIntermediateLogCloseGUI();
					openGUI.setEnabled(!sqlIntermediateLogIsActive());
					closeGUI.setEnabled(sqlIntermediateLogIsActive());
				}
			});
		}
		else {
			final JMenu openGUI = new JMenu(actionSQLIntermediateLog.replaceAll("ACTION", "Open"));
			functions.add(openGUI);
			final JMenu closeGUI = new JMenu(actionSQLIntermediateLog.replaceAll("ACTION", "Close"));
			functions.add(closeGUI);
			for(int i = 0 ; i < details.length ; i++) {
				final JMenuItem openOneGUI = new JMenuItem(details[i].getFrametitle());
				openGUI.add(openOneGUI);
				final JMenuItem closeOneGUI = new JMenuItem(details[i].getFrametitle());
				closeGUI.add(closeOneGUI);
				final int _i = i;
				openOneGUI.setEnabled(!sqlIntermediateLogIsActive());
				closeOneGUI.setEnabled(sqlIntermediateLogIsActive());
				openOneGUI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						sqlIntermediateLogOpenGUI();
						openOneGUI.setEnabled(!sqlIntermediateLogIsActive());
						closeOneGUI.setEnabled(sqlIntermediateLogIsActive());
					}
				});
				closeOneGUI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						sqlIntermediateLogCloseGUI();
						openOneGUI.setEnabled(!sqlIntermediateLogIsActive());
						closeOneGUI.setEnabled(sqlIntermediateLogIsActive());
					}
				});
			}
		}
		final String actionTextLog = "ACTION text log";
		if(details.length == 1) {
			// Avoid confusing intermediate node when only one connection
			final JMenuItem openTextGUI = new JMenuItem(actionTextLog.replaceAll("ACTION", "Open"));
			functions.add(openTextGUI);
			final JMenuItem closeTextGUI = new JMenuItem(actionTextLog.replaceAll("ACTION", "Close"));
			functions.add(closeTextGUI);
			openTextGUI.setEnabled(!sqlIntermediateLogIsActive());
			closeTextGUI.setEnabled(sqlIntermediateLogIsActive());
			openTextGUI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textLogOpenGUI();
					openTextGUI.setEnabled(!textLogIsActive());
					closeTextGUI.setEnabled(textLogIsActive());
				}
			});
			closeTextGUI.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textLogCloseGUI();
					openTextGUI.setEnabled(!textLogIsActive());
					closeTextGUI.setEnabled(textLogIsActive());
				}
			});
		}
		else {
			final JMenu openTextGUI = new JMenu(actionTextLog.replaceAll("ACTION", "Open"));
			functions.add(openTextGUI);
			final JMenu closeTextGUI = new JMenu(actionTextLog.replaceAll("ACTION", "Close"));
			functions.add(closeTextGUI);
			for(int i = 0 ; i < details.length ; i++) {
				final int _i = i;
				final JMenuItem openOneGUI = new JMenuItem(details[i].getFrametitle());
				openTextGUI.add(openOneGUI);
				final JMenuItem closeOneGUI = new JMenuItem(details[i].getFrametitle());
				closeTextGUI.add(closeOneGUI);
				openOneGUI.setEnabled(!sqlIntermediateLogIsActive());
				closeOneGUI.setEnabled(sqlIntermediateLogIsActive());
				openOneGUI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						textLogOpenGUI();
						openOneGUI.setEnabled(!textLogIsActive());
						closeOneGUI.setEnabled(textLogIsActive());
					}
				});
				closeOneGUI.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						textLogCloseGUI();
						openOneGUI.setEnabled(!textLogIsActive());
						closeOneGUI.setEnabled(textLogIsActive());
					}
				});
			}
		}
		final String openPoolViewText = "Open pool view";
		if(details.length == 1) {
			// Avoid confusing intermediate node when only one connection
			final JMenuItem openPoolView = new JMenuItem(openPoolViewText);
			functions.add(openPoolView);
			openPoolView.setEnabled(true);
			openPoolView.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					openPoolView(0);
				}
			});
		}
		else {
			final JMenu openPoolView = new JMenu(openPoolViewText);
			functions.add(openPoolView);
			openPoolView.setEnabled(true);
			for(int i = 0 ; i < tn.length ; i++) {
				final int _i = i;
				final JMenuItem onePoolView = new JMenuItem(details[i].getFrametitle());
				openPoolView.add(onePoolView);
				onePoolView.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						openPoolView(_i);
					}
				});
			}
		}

		JMenu search = new JMenu("Search");
		mb.add(search);
		for(int i = 0 ; i < tn.length ; i++) {
			final int _i = i;
			final ClientModelLogic det = tn[i].getClientModelLogic();
			JMenuItem conn = null;
			if(tn.length == 1) {
				// Avoid confusing intermediate node when only one connection
				conn = search;
			}
			else {
				conn = new JMenu(det.getFrametitle());
				search.add(conn);
			}
			//ModelConfiguration hs = det.getModelConfiguration();
			for(final ClientTableLogic ths : det.getClientTableLogicLoopable()) {
				if(!ths.hasTextField()) {
					continue;
				}
				String name = ths.getTableName();
				if(det.inSearchHideTableList(name)) {
					continue;
				}
				JMenuItem searchTable = new JMenuItem(ths.translate(name));
				conn.add(searchTable);
				searchTable.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try { // ClientTableLogic env, Window jw, boolean modality
							new SearchRowDialog(ths, ExplorerWindow.this, false);
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(null,
								    "Failed to open search dialog",
								    "Internal error",
								    JOptionPane.ERROR_MESSAGE);
						}
					}
				});
			}
		}
		JMenu newmenu = new JMenu("New");
		mb.add(newmenu);
		for(int i = 0 ; i < tn.length ; i++) {
			final int _i = i;
			final ClientModelLogic det = tn[i].getClientModelLogic();
			//final NativeClientModelHandling det = tn[i].getDetails();
			JMenuItem conn = null;
			if(tn.length == 1) {
				// Avoid confusing intermediate node when only one connection
				conn = newmenu;
			}
			else {
				conn = new JMenu(det.getFrametitle());
				newmenu.add(conn);
			}
			for(final ClientTableLogic ths : det.getClientTableLogicLoopable()) {
			//for(final TableConfiguration ths : det.getTablesLoopable()) {
				JMenuItem newTable = new JMenuItem(ths.translate(ths.getTableName()));
				conn.add(newTable);
				newTable.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						/*
						ClientTableLogic newenv = null;
						try {
							newenv = det.getEnvironment(ths, ExplorerWindow.this);
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(null,
								    "Failed to open dialog for new "+ths.getObjectName(),
								    "Internal error",
								    JOptionPane.ERROR_MESSAGE);
							return;
						}
						*/
						NewRowDialog nrd = null;
						try { // ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, Window previousWindow
							nrd = new NewRowDialog(ths, null, null, null, ExplorerWindow.this);
						} catch (Exception e1) {
							det.logTrace(e1);
							UniversalTellUser.error(ExplorerWindow.this, "Unable to edit the new node", "Internal error");
							return;
						}
						nrd.setVisible(true);
						if(nrd.getEditAfterNew()) {
							ClientDBObject po = nrd.getCreatedClientDBObject();
							if(po == null) {
								UniversalTellUser.info(ExplorerWindow.this, "Unable to edit the new node", "Internal error");
								return;
							}
							try { // DataNode ro, Window currentWindow
								RowNode rn = new RowNode(new DataNode(po), null, ExplorerWindow.this);
								rn.editPressed(false);
							}
							catch(Exception ex) {
								UniversalTellUser.info(ExplorerWindow.this, "Unable to edit the new node", "Internal error");
							}
							
						}
					}
				});
			}
		}
		JMenu metaConnections = new JMenu("Meta");
		mb.add(metaConnections);
		for(int i = 0 ; i < tn.length ; i++) {
			final ClientModelLogic det = tn[i].getClientModelLogic();
			//final NativeClientModelHandling det = tn[i].getDetails();
			//final ModelConfiguration hs = det.getModelConfiguration();
			JMenuItem conn = null;
			if(tn.length == 1) {
				// Avoid confusing intermediate node when only one connection
				conn = metaConnections;
			}
			else {
				conn = new JMenu(det.getFrametitle());
				metaConnections.add(conn);
			}
			JMenuItem table = new JMenuItem("All summit tables");
			table.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MetaTopNode tn = MetaExplorerWindow.getTopNodeForMetaStructure(det);
/*							hs, 
							det.getRecreator());
					MetaExplorerWindow mew = new MetaExplorerWindow(hs, "Meta view", tn, det.getRecreator());*/
					MetaExplorerWindow mew = new MetaExplorerWindow(tn);
					mew.setVisible(true);
				}
			});
			conn.add(table);
			for(final ClientTableLogic ths : det.getClientTableLogicLoopable()) {
			//for(final TableConfiguration ths : hs.getTablehandlersLoopable()) {
				table = new JMenuItem(ths.translate(ths.getTableName()));
				conn.add(table);
				table.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						MetaTableNode tn = MetaExplorerWindow.getTableNodeForMetaStructure(ths);
								/* hs, 
								ths.getTableName(), 
								det.getRecreator()); */
						MetaExplorerWindow mew = new MetaExplorerWindow(tn);//hs, "Meta view", tn, det.getRecreator());
						mew.setVisible(true);
					}
				});
			}
		}
		/*
		Vector<JMenu> connectionsProcesses = new Vector<JMenu>();
		for(int c = 0 ; c < tn.length ; c++) {
			final int _c = c;
			final GDBAPPClientDetails det = tn[c].getDetails();
			final DBHandlingSpecification hs = det.getDBHandlingSpecification();
//--
			Vector<JMenuItem> processes = new Vector<JMenuItem>();
			for(int i = 0 ; i < hs.getNoCodeDeclarations() ; i++) {
				final CodeDeclaration cd = hs.getCodeDeclaration(i);
				for(int j = 0 ; j < cd.getNoProcedures() ; j++) {
					final UntypedProcedureInstance p = cd.getProcedure(j); 
					if(p.getNoAliases() != 0) {
						continue;
					}
					JMenuItem mi = new JMenuItem(hs.translate(p.getName()));
					mi.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									//scope scopeForProcedure = sf.getInstance(s.getBottomScope(), "procedure "+p.getName());
									GDBAPPScopeFactory sf = new GDBAPPScopeFactory();
									ProgramBlock pr = p.getProgram();
									try {
										GDBAPPScope scopeForProcedure = ScopeBuilder.getProgrammingScope("procedure "+p.getName(), cd, sf);
										pr.execute(scopeForProcedure);
									}
									catch(ReturnException re) {
										// OK
									}
									catch (StopException e) {
										// Impossible.
										UniversalTellUser.error(ExplorerWindow.this, "Illegal stop thrown");
									} 
									catch (BreakException e) {
										UniversalTellUser.error(ExplorerWindow.this, "Illegal break thrown");
									} 
									catch (ContinueException e) {
										UniversalTellUser.error(ExplorerWindow.this, "Illegal continue thrown");
									} 
									catch (IntolerableException e) {
										UniversalTellUser.general(ExplorerWindow.this, e, "Unexpected exception");
									}
								}
							});
						}
					});
					processes.add(mi);
				}
			}
			if(processes.size() != 0) {
				JMenu processMenu = new JMenu(det.getFrametitle());
				connectionsProcesses.add(processMenu);
				for(int idx = 0 ; idx < processes.size() ; idx++) {
					processMenu.add(processes.elementAt(idx));
				}
			}
		//--
		}
		if(connectionsProcesses.size() != 0) {
			JMenu processMenu = new JMenu("Processes");
			mb.add(processMenu);
			if(connectionsProcesses.size() == 1 && tn.length == 1) {
				// Avoid confusing intermediate node when only one connection
				JMenu theSoleMenu = connectionsProcesses.elementAt(0);
				for(int idx = 0 ; idx < theSoleMenu.getItemCount() ; idx++) {
					processMenu.add(theSoleMenu.getItem(idx));
				}
			}
			else {
				for(int idx = 0 ; idx < connectionsProcesses.size() ; idx++) {
					processMenu.add(connectionsProcesses.elementAt(idx));
				}
			}
		}
		*/
		
	}

	private void openPoolView(int idx) {
		//DBPool pool = tn[idx].getPool();
		UniversalTellUser.info(this, details[idx].showPoolState());
	}
	
	public void setRightSide(Component c) {
		rightView.setViewportView(c);
	}

	/*
	public void update() {
		if(lastFocusedNode != null) {
			lastFocusedNode.reset();
			setRightSide(lastFocusedNode.getRightSide());
		}
	}
	*/
	
	public JTree getTree() {
		return tree;
	}
	
	  public static class PopupListener extends MouseAdapter 
	  {
	    
	    public PopupListener()
	    {
	    }

	    public void mousePressed(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) 
	    {
	      if(e.isPopupTrigger()) 
	      {
	        //System.out.println(e.getComponent().getClass().getName());
	        if(e == null) return;
	        JTree tree = (JTree) e.getComponent();
	        if(tree == null) return;
	        TreePath tp = tree.getClosestPathForLocation(e.getX(), e.getY());
	        tree.setSelectionPath(tp);
	        Object obj = tree.getLastSelectedPathComponent();
	        if(obj instanceof NodeBase) {
		        NodeBase nb = (NodeBase) obj;
		        if(nb == null) return;
		        JPopupMenu popup = nb.getTreePopup();
		        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
		        return;
	        }
	        if(obj instanceof TopNode) {
	        	TopNode tn = (TopNode) obj;
		        JPopupMenu popup = tn.getTreePopup();
		        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
		        return;
	        	
	        }
	      }
	    }

	  }

	  private boolean sqlIntermediateLogIsActive() {
		  return silw.isActive();
	  }
	  
		public void sqlIntermediateLogOpenGUI() {
			silw.openGUI();
		}
		
		public boolean textLogIsActive() {
			return smtlw.isActive();
		}
		
		public void textLogOpenGUI() {
			smtlw.openGUI();
		}
		
		public void textLogCloseGUI() {
			smtlw.closeGUI();
		}
		public void sqlIntermediateLogCloseGUI() {
			silw.closeGUI();
		}

}
