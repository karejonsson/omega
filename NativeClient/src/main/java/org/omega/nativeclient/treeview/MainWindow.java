package org.omega.nativeclient.treeview;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JTree;

public abstract class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	public MainWindow(String title) {
		super(title);
	}
	
	abstract public JTree getTree(); 
	
	abstract public void setRightSide(Component c);
	
}
