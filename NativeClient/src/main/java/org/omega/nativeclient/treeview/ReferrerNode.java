package org.omega.nativeclient.treeview;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextArea; 
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.clientcommon.hierarchy.ReferenceNode;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.renderers.DefaultReferrernodeTextRenderer;
import org.omega.configuration.renderers.TableRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.NumericType;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.helpers.GeneralComparator;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.GenerateJavaAPI;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;

import se.modlab.generics.util.Sort;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.plot.TimePlotComponent;
import se.modlab.generics.gui.plot.TimePlotDataContainer;
import se.modlab.generics.gui.util.Clipboard;

public class ReferrerNode extends NodeBase {

	private static final long serialVersionUID = 1L;
	//private DBPool pool = null;
	//private NativeClientTableHandling env = null;
	//private ForeignKey fk;
	private String errormessage = null;
	private JTable table = null;
	//private Vector<RowNode> rows = new Vector<RowNode>();
	private Vector<String> colnames = new Vector<String>();
	//private TextRenderer instanceRenderer = null;
	private int sortcolumn = -1;
	private boolean sortInverted = false;
	private Vector<RowNode> tableRowsOrder = null;//new Vector<RowNode>();
	private Window currentFrame = null;
	private ClientTableLogic ctl = null;
	private ReferenceNode rn = null;

	/*
	public ReferrerNode(Environment _env, Constraint _con, int _idvalue) {
		env = _env;
		con = _con;
		idvalue = _idvalue;
		instanceRenderer = new DefaultReferrernodeTextRenderer(_con);
	}
	*/
	public ReferrerNode(ReferenceNode rn, Window currentFrame) {
		//this.env = env;
		//this.pool = pool;
		//this.fk = fk;
		this.rn = rn;
		this.currentFrame = currentFrame;
		ctl = rn.getClientTableLogic();
		//if(pool == null) {
			//new Throwable().printStackTrace();
			//System.err.println("Referrer node ctor - Pool null");
			//System.exit(-1);
		//}
		//instanceRenderer = env.ths.getReferrerTextRenderer(con.getMember());
		//instanceRenderer = env.getEmbeddedEnvironment().ths.getReferrerTreeTextRenderer(fk.getName());
		//if(instanceRenderer == null) {
			//instanceRenderer = new DefaultReferrernodeTextRenderer(fk.getForeignKeyDeclaration());
		//}
	}
	
	public ReferenceNode getReferenceNode() {
		return rn;
	}
	
	public NodeBase cloneForOtherWindow(Window window) {
		return this;
	}

	public ClientTableLogic getClientTableLogic() {
		return rn.getClientTableLogic();
	}

	/*
	public NativeClientTableHandling getEnvironment() {
		return env;
	}
	*/
	
	/*
	public NativeClientModelHandling getDetails() {
		try {
			return env.getClientDetails();
		} catch (Exception e) {
			env.logTrace(e);
			UniversalTellUser.error(null, "Unable to get the model details", "Internal error");
		}
		return null;
	}
	*/
	/*
	public DBPool getPool() {
		return pool;
	}
	*/
	/*
	public boolean foreignKeyEquals(ForeignKey _fk) {
		return fk.equals(_fk);
	}
	*/
	
	public String getName() {
		return ctl.getTableName();
	} 

	public boolean isLeaf() {
		return(false);
	}

	public int getChildCount() {
		if (!areChildrenDefined)
			defineChildNodes();
		return(super.getChildCount());
	}

	private void defineChildNodes() {
		areChildrenDefined = true;
		try {
			List<DataNode> referred = rn.getReferringObjects();
			//Vector<DBPoolObject> referred = pool.getPoolObjectsHavingPointer(fk);

			int ctr  = 0;
			removeAllChildren();
			//if(!fk.hasCompleteSetOfValues()) {
			//	return;
			//}
			/*
			for(int i = 0 ; i < referred.size() ; i++) {
				DBPoolObject ro = referred.elementAt(i);
				RowNode rn = new RowNode(pool, env, ro, currentFrame);
				RowNode rn = new RowNode()
				add(rn);
				ctr++;
			}
			*/
			for(DataNode dn : referred) {
				RowNode ron = new RowNode(dn, rn.getConstraintName(), currentFrame);
				add(ron);
				ctr++;
			}

			if(table == null) {
				final ResultSetTableModel model = new ResultSetTableModel();
				model.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent arg0) {
						//System.out.println("mod - tableChanged "+arg0);
					}
				});
				table = new JTable(model);
			    table.addMouseListener(new PopupListener());   
			    TableColumnModelListener tableColumnModelListener = new TableColumnModelListener() {
			        public void columnAdded(TableColumnModelEvent e) {
			          //System.out.println("ReferrerNode.column Added");
			        }

			        public void columnMarginChanged(ChangeEvent e) {
			          //System.out.println("ReferrerNode.column Margin");
			        }

			        public void columnMoved(TableColumnModelEvent e) {
			          int from = e.getFromIndex();
			          int to = e.getToIndex();
			          //System.out.println("ReferrerNode.column Moved "+from+" -> "+to);
			          sortInverted = to == sortcolumn ? !sortInverted : false;
			          sortcolumn = to;
			          rearrangeTableOrder();
			          model.fireTableStructureChanged();
			        }

			        public void columnRemoved(TableColumnModelEvent e) {
			          //System.out.println("ReferrerNode.column Removed");
			        }

			        public void columnSelectionChanged(ListSelectionEvent e) {
			          //System.out.println("ReferrerNode.column Selection Changed");
			        }
			      };

			      TableColumnModel columnModel = table.getColumnModel();
			      columnModel.addColumnModelListener(tableColumnModelListener);
				//table.add
			}
			TreeNode tn = getParent();
			if(tn instanceof TableNode) {
				((TableNode) tn).refresh();
				return;
			}
			DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) ctl.getClientModelLogic().getMainViewObject()).getTree().getModel();
			model.nodeStructureChanged(tn);

		}
		catch(SQLException sqle) {
			errormessage = sqle.getMessage();
			sqle.printStackTrace();
		}
		catch(Exception e) {
			errormessage = e.getMessage();
			e.printStackTrace();
		}
	}
	
	public class PopupListener extends MouseAdapter 
	{

		public PopupListener()
		{
		}

		public void mouseClicked(MouseEvent e) {
			//System.out.println("TableNode.PopupListener.mouse listener clicked "+e);
			if (e.getComponent().isEnabled() && e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
				//System.out.println("TableNode.PopupListener.mouse listener dubbelklick ###");
				int rowno = table.getSelectedRow();
				if(rowno == -1) {
					return;
				}
				RowNode rn = getNodeAtPlaceForTable(rowno);
				//int row = rn.
				JTree tree = ((MainWindow) ctl.getClientModelLogic().getMainViewObject()).getTree();
				TreePath tp = new TreePath(rn.getPath());
				tree.setSelectionPath(tp);
				tree.expandPath(tp);
				tree.makeVisible(tp);
			}
		}

		public void mousePressed(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) 
		{
			if(e.isPopupTrigger()) 
			{
				//System.out.println(e.getComponent().getClass().getName());
				if(e == null) return;
				JTable table= (JTable) e.getComponent();
				if(table == null) return;
				JPopupMenu popup = getTablePopup();
				if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}

	}


	public Component getRightSide() {
		if (!areChildrenDefined) {
			defineChildNodes();
		}
		if(table != null) {
			return table;
		}
		JTextArea ta = new JTextArea();
		ta.setText(
				"Right side "+getClass().getName()+"\n\nInstance "+this.toString()+
				"\n\nSomething went wrong with getting table."+"\n\n"+
				((errormessage != null) ? errormessage : ""));
		return ta; 
	}

	public void gotFocus() {
		//System.out.println("ReferrerNode.gotFocus()");
	}

	public String toString() {
		return rn.toString();
	}
	
	/*
	public String toString() {
		if(instanceRenderer == null) return "<NULL Renderer>";
		try {
			ScriptingScopeFactory osf = env.getDetails().getScopeFactory();
			DBPoolObject po = pool.getPoolObject(fk.assemblePrimaryKey());
			ScriptingScope os = osf.getNodeScope(po, true);
			try {
				Object obj = os.eval(instanceRenderer);
				if(obj == null) {
					return "<rendering error>";
				}
				return obj.toString();
			}
			catch(Exception e) {
				return "<no rendering>";
			}
		}
		catch(IntolerableException ie) {
			env.logTrace(ie);
			return "<no information>";
		}
		catch(Exception e) {
			env.logTrace(e);
			return "<internal error>";
		}
	}
	*/
	
	private void rearrangeTableOrder() {
		//System.out.println("TableNode.rearrangeTableOrder col="+sortcolumn+", inv="+sortInverted);
		if(tableRowsOrder == null) {
			tableRowsOrder = new Vector<RowNode>();
		}
		if(tableRowsOrder.size() != getChildCount()) {
			//System.out.println("TableNode.rearrangeTableOrder initierar Vectorn");
			tableRowsOrder.removeAllElements();
			for(int i = 0 ; i < getChildCount() ; i++) {
				tableRowsOrder.addElement((RowNode) getChildAt(i));
			}
		}
		if(sortcolumn == -1) {
			return;
		}
		Comparator<Object> comp = new Comparator<Object>() {
			private GeneralComparator gp = new GeneralComparator();
			public int compare(Object arg0, Object arg1) {
				String s1 = getTableStringToPresent((RowNode) arg0, sortcolumn);
				String s2 = getTableStringToPresent((RowNode) arg1, sortcolumn);
				int out = gp.compare(s1, s2);
				if(sortInverted) {
					out = -out;
				}
				return out;
			}
		};
		Vector<RowNode> tmp = new Vector<RowNode>();
		for(int i = 0 ; i < tableRowsOrder.size(); i++) {
			//System.out.println("TableNode.rearrangeTableOrder sorterar Vectorn "+tmp.size());
			Sort.add(tmp, tableRowsOrder.elementAt(i), /*getNodeAtPlaceForTable(i),*/ comp);
		}
		tableRowsOrder = tmp;
	}
	
	private RowNode getNodeAtPlaceForTable(int row) {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		//return (RowNode) getChildAt(row);
		return tableRowsOrder.elementAt(row);
	}
	
	private int _getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		//return getChildCount();
		return tableRowsOrder.size();
	}
	
	private int getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
			return tableRowsOrder.size();
		}
		List<DataNode> children = null;//rn.getReferringObjects();
		//Vector<DBPoolObject> children = null;
		try {
			//children = pool.getPoolObjectsHavingPointer(fk);
			children = rn.getReferringObjects();
		}
		catch(Exception e) {
		}
		if(tableRowsOrder.size() != (children != null ? children.size() : -1)) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.size();
		//return getChildCount();
	}
	
	/*
	private OmegaScope getScope(int row) {
		RowNode rn = getNodeAtPlaceForTable(row);
		try {
			return env.getScopeFactory().getNodeScope(rn.getRowObject(), true);
		}
		catch(Exception e) {
			return null;
		}
	}
	*/
	
	private String getTableStringToPresent(int r, int c) {
		if(r >= getRowCountForTable()) {
			//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> <Out of bounds>");
			return "Out of bounds ("+r+" >= "+getRowCountForTable()+")";
		}
		return getTableStringToPresent(getNodeAtPlaceForTable(r), c);
	}
	
	private String getTableStringToPresent(RowNode rn, int c) {
		String rendering = null;
		try {
			rendering = rn.getRowObject().renderForTableAtPosition(c);//s.eval(se);//se.render(s.getNode());
		}
		catch(Exception ie) {
			ctl.log(""+rn, ie);
			//System.out.println("2: Scope dump "+s+" when evaluating "+vl);
			rendering = "<no information>";
		}
		if(rendering == null) {
			rendering = "<not set>";
		}
		//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> "+obj);
		return rendering;
	}

	private class ResultSetTableModel extends AbstractTableModel
	{  
		private static final long serialVersionUID = 1L;
		//Hashtable<Integer, scope> sht = new Hashtable<Integer, scope>();

		public ResultSetTableModel() {
		}
		
		public void clear() {
			//sht.clear();
			rearrangeTableOrder();
			this.fireTableDataChanged();
		}

		public String getColumnName(int c) { 
			return ctl.getColumnNameTranslated(c);
		}

		public int getColumnCount() { 
			return ctl.getColumnCount();
		}

		public Object getValueAt(int r, int c) {
			return getTableStringToPresent(r, c);
		}

		public int getRowCount() {
			int r = getRowCountForTable();
			//System.out.println("ResultSetTableModel getRowCount() = "+r);
			return r;
		}
	}
	
	private int getActualTableColumnIndex(String name) {
		for(int i = 0 ; i < colnames.size() ; i++) {
			if(name.toUpperCase().compareTo(colnames.elementAt(i).toUpperCase()) == 0) {
				return i;
			}
		}
		return -1;
	}
	
	private int getFirstDatetimeColumnNumer() {
		return ctl.getFirstDatetimeColumnNumer();
	}
	
	private int[] getNumericColumnsNumbers() {
		return ctl.getNumericColumnsNumbers();
	}
	
	public JPopupMenu getTablePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Export to clipboard");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exportToClipboard();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Open");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				onOpen();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Add new "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewInstance();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromTablesSelected();
			}
		});
		pm.add(menuItem);
		final int datetimeColumnNumber = getFirstDatetimeColumnNumer();
		if(datetimeColumnNumber == -1) {
			return pm;
		}
		final int idxs[] = getNumericColumnsNumbers();
		if((idxs == null) || (idxs.length == 0)) {
			return pm;
		}
		// We can auto generate some plots
		for(int i = 0 ; i < idxs.length ; i++) {
			menuItem = new JMenuItem("Plot "+ctl.translate(rn.getTableColumnName(idxs[i])));
			final int x = i;
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					plot(datetimeColumnNumber, idxs[x], ctl.translate(rn.getTableColumnName(idxs[x])));
				}
			});
			pm.add(menuItem);
		}

		return pm;
	}
	
	public void onOpen() { // Ropa hit
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = getNodeAtPlaceForTable(rowno);
		//int row = rn.
		JTree tree = ((MainWindow) ctl.getClientModelLogic().getMainViewObject()).getTree();
		TreePath tp = new TreePath(rn.getPath());
		tree.setSelectionPath(tp);
		tree.expandPath(tp);
		tree.makeVisible(tp);
	}
	
	private void exploreFromTablesSelected() {
		int rowno = table.getSelectedRow();
		if(rowno == -1) {
			return;
		}
		RowNode rn = getNodeAtPlaceForTable(rowno);
		try {
			ExplorerWindow ew = new ExplorerWindow(ctl.getClientModelLogic(), null, rn);
			ew.setVisible(true);
		} catch (Exception e) {
			ctl.logTrace(e);
			UniversalTellUser.error(null, "Unable to explore from selected table row", "Explorer error");
		}
	}
	
	private void addNewInstance() {
		//System.out.println("ReferrerNode.addNewInstance(Type = "+env.ths.getName()+" constraint member "+con.getMember());
		//System.out.println("ReferrerNode.addNewInstance(Type = "+env.ths.getName()+" foreign key="+fk);
		NewRowDialog nrd = null;;
		try { // ClientTableLogic env, Runnable onInsert, String implicitConstraintname, ClientDBObject referred, Window previousWindow
			nrd = new NewRowDialog(ctl, null, rn.getConstraintName(), rn.getReferredobject(), currentFrame);
		} catch (Exception e1) {
			UniversalTellUser.error(null, "Unable to create instance", "Internal error");
			ctl.logTrace(e1);
			return;
		}
		nrd.setVisible(true);
		defineChildNodes(); 
		DefaultTreeModel model = (DefaultTreeModel) ((MainWindow) ctl.getClientModelLogic().getMainViewObject()).getTree().getModel();
		model.nodeStructureChanged(this);

		if(table != null) {
			((ResultSetTableModel) table.getModel()).clear();
			table.invalidate();
			table.validate();
			table.repaint();
		}

		((MainWindow) ctl.getClientModelLogic().getMainViewObject()).invalidate();
		((MainWindow) ctl.getClientModelLogic().getMainViewObject()).validate();
		((MainWindow) ctl.getClientModelLogic().getMainViewObject()).repaint();
		if(nrd.getEditAfterNew()) {
			ClientDBObject createdClientDbObject = nrd.getCreatedClientDBObject();
			try {
				//PrimaryKey pk = fk.assemblePrimaryKey();
				for(int i = 0 ; i < getChildCount() ; i++) {
					RowNode child = (RowNode) getChildAt(i);
					if(child.sameObject(createdClientDbObject)) {
						child.editPressed(true);

						if(table != null) {
							((ResultSetTableModel) table.getModel()).clear();
							table.invalidate();
							table.validate();
							table.repaint();
						}

						((MainWindow) ctl.getClientModelLogic().getMainViewObject()).invalidate();
						((MainWindow) ctl.getClientModelLogic().getMainViewObject()).validate();
						((MainWindow) ctl.getClientModelLogic().getMainViewObject()).repaint();
					}
				}
			} 
			catch (Exception e) {
				ctl.log(e.getMessage(), e);
				//e.printStackTrace();
			}
		}
	}

	
	private void exportToClipboard() {
		Clipboard.export((AbstractTableModel) table.getModel());
	}

	public JPopupMenu getTreePopup() {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("New "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewInstance();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromHere();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Graphical meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showGraphicalMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Textual meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showTextualMeta();
			}
		});
		pm.add(menuItem);
		return pm;
	}

	public JPopupMenu getPopup(JTree tree) {
		JPopupMenu pm = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Graphical meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showGraphicalMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Textual meta "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showTextualMeta();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Add new "+ctl.translate(ctl.getTableName()));
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNewInstance();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("Explore from here");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exploreFromHere();
			}
		});
		pm.add(menuItem);
		menuItem = new JMenuItem("API for "+ctl.getObjectName());
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showAPI();
			}
		});
		pm.add(menuItem);
		return pm;
	}

	private String getMetaDescription() {
		return ctl.getMetaDescription();
	}
	
	private void exploreFromHere() {
		try {
			ExplorerWindow ew = new ExplorerWindow(ctl.getClientModelLogic(), null, this);
			ew.setVisible(true);
		} catch (Exception e) {
			ctl.logTrace(e);
			UniversalTellUser.error(null, "Unable to explore from here", "Explorer error");
		}
	}

	private void showGraphicalMeta() {
		MetaTableNode tn = MetaExplorerWindow.getTableNodeForMetaStructure(ctl);
				/*pool.getDetails().getModelConfiguration(), 
				ctl.getTableName(), 
				pool.getDetails().getRecreator()); */
		//MetaExplorerWindow mew = new MetaExplorerWindow(pool.getDetails().getModelConfiguration(), "Meta view", tn, pool.getDetails().getRecreator());
		MetaExplorerWindow mew = new MetaExplorerWindow(tn);
		mew.setVisible(true);
	}
	
	private void showTextualMeta() {
		Object[] options = { "OK" }; 
		int choice = JOptionPane.showOptionDialog(((MainWindow) ctl.getClientModelLogic().getMainViewObject()), 
				getMetaDescription(), 
				"SQL Create table",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
	}
	
	private void plot(int datetimeColumnNumber, int idx, String name) {
		TimePlotDataContainer tp = new TimePlotDataContainer(name);
		//...
		ResultSetTableModel model = (ResultSetTableModel) table.getModel();
		int rowcount = model.getRowCount();
		for(int r = 0 ; r < rowcount ; r++) {
			String datetext = model.getValueAt(r, datetimeColumnNumber).toString();
			String valtext = model.getValueAt(r, idx).toString();
			long time = CommonSQLAssembler.getDateFromString(datetext).getTime();
			double value = Double.parseDouble(valtext);
			tp.add(time, value);
		}

		//...
		final JFrame frame = new JFrame(tp.getName());
		frame.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          frame.setVisible(false);
		          frame.dispose();
		        }
		      });
		frame.getContentPane().add(new TimePlotComponent(tp), BorderLayout.CENTER);
		try {
			frame.pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(((MainWindow) ctl.getClientModelLogic().getMainViewObject()), ie.getMessage());
			return;
		}
		frame.setSize(500,350);
		frame.setVisible(true);
	}
	
	private void showAPI() {
		Object[] options = { "OK", "Copy+OK" }; 
		String api = ctl.getClientModelLogic().getAPI(ctl.getTableName());
		int choice = JOptionPane.showOptionDialog(((MainWindow) ctl.getClientModelLogic().getMainViewObject()), 
				api, 
				"Class API",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[0]);
		if(choice == 1) {
			Clipboard.setClipboard(api);
		}
	}

	public Icon getImageIcon(int size, int usedImageBufferType) {
		return ctl.getReferrerNodeImageIcon(size, usedImageBufferType);
	}

	public ClientModelLogic getClientModelLogic() {
		return ctl.getClientModelLogic();
	}

}


