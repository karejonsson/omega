package org.omega.nativeclient.helpers;

import java.awt.Component;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.ProgressMonitorInputStream;

import org.omega.clientcommon.editors.CandidateClientDBObject;
import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.LobEditorCommons;
import org.omega.clientcommon.hierarchy.DataNode;
/*
 
 */
import org.omega.configuration.values.LobValue;
import org.omega.groovyintegration.pool.DBPoolObject;

import org.omega.nativeclient.typegraphics.LobTypeEditor;

import se.modlab.generics.crypto.Services;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.util.Clipboard;
import se.modlab.generics.util.FilesAndBytes;
import se.modlab.generics.util.HexVisualizer;

public class BlobActionHelper {
	
	private static String getFilenameFromBlobEditor(LobTypeEditor lte, Component parent) {
		Object obj = lte.getValue();
		if(obj == null) {
			UniversalTellUser.error(parent, "No file to open.");
			return null;
		}
		if(!(obj instanceof LobValue)) {
			UniversalTellUser.error(parent, "Internal error. Type is "+obj.getClass().getName());
			return null;
		}

		LobValue lv = (LobValue) obj;
		byte[] out = lv.getFilename();
		if(out == null) {
			return null;
		}
		return new String(out);
	}
	
	private static void handleMetaAfterDatabaseRead(byte[] meta, final LobTypeEditor lte, ClientDBObject po) throws Exception {
		//lte.clear(); // Den här var med i förra versionen men 2017-jan-25 tycks det funka och jag förstår inte längre varför det skulle vara så.
		//System.out.println("BlobActionHelper.handleMetaAfterDatabaseRead()");
		lte.setCheckedWithDatabase(true);
		lte.setText();
		if(meta != null) {
			lte.setInitialValue(meta, true);
			//po.set(lte.getName(), lv);
			//System.out.println("RowNode - Meta - "+new String(filename)+" storlek "+length);
		}
		//lte.setWritten(true);
		lte.invalidate();
		lte.validate();
		lte.repaint();
	}
	
	private static InputStream addProgress(InputStream is) {
		return new BufferedInputStream(new ProgressMonitorInputStream(null, "Downloading", is));
	}
	
	public static void blobButtonGetMetaPressed(final LobTypeEditor lte, ClientDBObject cdbo) throws Exception {
		InputStream is = null;
		byte[] meta = null;
		try {
			is = addProgress(cdbo.downloadFromBlob_with_executeQuery(lte.getName()));
			meta = FilesAndBytes.readMeta(is);
		}
		catch(Exception e) {
			is = null;
			meta = null;
			// This is when DB-cell holds null.
		}
		try {
			if(is != null) {
				is.close();
			}
		}
		catch(Exception e) {
		}
		//lte.setWritten(true);
		handleMetaAfterDatabaseRead(meta, lte, cdbo);
	}

	/*
	public static void blobButtonDebugPressed(final LobTypeEditor lte, DBPoolObject po) throws Exception {
		InputStream is = null;
		byte[] meta = null;
		try {
			is = addProgress(po.downloadFromBlob_with_executeQuery(lte.getName()));
			meta = FilesAndBytes.readMeta(is);
		}
		catch(Exception e) {
			is = null;
			meta = null;
			// This is when DB-cell holds null.
		}
		try {
			if(is != null) {
				is.close();
			}
		}
		catch(Exception e) {
		}
		//lte.setWritten(true);
		System.out.println("BlobActionHelper.blobButtonDebugPressed : meta = "+HexVisualizer.visualize(meta));
	}
	*/
	
	public static void blobButtonOpenFromDB(final LobTypeEditor lte, JFrame parent, DataNode po) throws Exception {
		InputStream is = null;
		byte[] meta = null;
		byte[] wholeFile = null;
		try {
			is = addProgress(po.downloadFromBlob_with_executeQuery(lte.getName()));
			if(is == null) {
				LobValue lv = new LobValue();
				lv.setDatabaseBased(true);
				lte.setValue(lv);
				//lte.setWritten(true);
				return;
			}
			wholeFile = Services.getCleartextFromFile(is);
			//System.out.println("BlobActionHelper.blobButtonOpenFromDB - läste "+wholeFile.length+" byte");
			meta =  FilesAndBytes.getFilemetaFromArray(wholeFile);
		}
		catch(Exception e) {
			try {
				if(is != null) {
					is.close();
				}
			}
			catch(Exception e2) {
			}
			meta = null;
			throw e;
		}
		try {
			if(is != null) {
				is.close();
			}
		}
		catch(Exception e) {
		}
		is = null;
		handleMetaAfterDatabaseRead(meta, lte, po.getClientDBObject());
		//lte.setWritten(true);
		String filename = new String(FilesAndBytes.getFilenameFromMeta(meta));
		File f = File.createTempFile("tmp", filename);
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(FilesAndBytes.getFilecontentsFromArray(wholeFile));
		fos.close();
		//System.out.println("BlobActionHelper.blobButtonOpenFromDB - öppnar "+f.getAbsolutePath()+" byte");
		Clipboard.desktopOpen(f.getAbsolutePath(), lte);
		f.deleteOnExit(); // deletes the file on exit
	}
	
	public static void openFromFilesystem(final LobTypeEditor lte, JFrame parent) throws Exception {
		String filename = getFilenameFromBlobEditor(lte, parent);
		//System.out.println("BlobActionhelper.openFromFilesystem - öppnar "+filename);
		Clipboard.desktopOpen(filename, lte);
	}

	public static void blobButtonOpenPressed(final LobTypeEditor lte, JFrame parent, DataNode po) throws Exception {
		if(lte.isUnchanged()) {
			blobButtonOpenFromDB(lte, parent, po);
			return;
		}
		openFromFilesystem(lte, parent);
	}

	public static void blobButtonSetPressed(final LobTypeEditor lte, JFrame parent) throws Exception {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Chose file to write");
		chooser.setCurrentDirectory(lastSaveDirectory) ;
		String filename = getFilenameFromBlobEditor(lte, parent);
		if(filename != null) {
			File f = new File(filename);
			chooser.setSelectedFile(f);
		}

		int returnVal = chooser.showSaveDialog(parent);;
		if(returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}
		File myFile = chooser.getSelectedFile();
		lastSaveDirectory = myFile.getParentFile();
		
		LobValue lv = new LobValue();
		lv.setLength((int) myFile.length());
		lv.setFilename(myFile.getAbsolutePath().getBytes());
		lv.setDatabaseBased(false);
		lte.setValue(lv);

		lte.invalidate();
		lte.validate();
		lte.repaint();
	}
	
	public static void blobButtonSetPressed(final LobTypeEditor lte, final String filename) throws Exception {
		File myFile = new File(filename);
		
		LobValue lv = new LobValue();
		lv.setLength((int) myFile.length());
		lv.setFilename(myFile.getAbsolutePath().getBytes());
		lv.setDatabaseBased(false);
		lte.setValue(lv);

		lte.invalidate();
		lte.validate();
		lte.repaint();
	}
	
	private static void blobButtonDownloadPressed(final LobTypeEditor lte, JFrame parent, ClientDBObject po) throws Exception {
		try {
			blobButtonDownloadPressed_(lte, parent, po); 
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void blobButtonDownloadPressed_(final LobTypeEditor lte, JFrame parent, ClientDBObject po) throws Exception {

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Save");
		chooser.setCurrentDirectory(lastSaveDirectory);
		
		String filename = getFilenameFromBlobEditor(lte, parent);
		
		chooser.setSelectedFile(new File(filename == null ? "" : filename));

		//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = chooser.showSaveDialog(parent);
		if(returnVal != JFileChooser.APPROVE_OPTION) {
			throw new Exception("Download aborted");
		}
		File myFile = chooser.getSelectedFile();
		lastSaveDirectory = myFile.getParentFile();
		//System.out.println("RowNode - Get");
		InputStream is = null;
		byte[] meta = null;
		byte[] wholeFile = null;
		try {
			is = addProgress(po.downloadFromBlob_with_executeQuery(lte.getName()));
			if(is == null) {
				LobValue lv = new LobValue();
				lv.setLength((int) myFile.length());
				lv.setFilename(myFile.getAbsolutePath().getBytes());
				lv.setDatabaseBased(true);
				lte.setValue(lv);
				//lte.setWritten(true);
				return;
			}
			//inputStream_out = new BufferedInputStream(
            //        new ProgressMonitorInputStream(parent,"Downloading", is));
			wholeFile = Services.getCleartextFromFile(is);
			//System.out.println("BlobActionHelper.blobButtonDownloadPressed - läste "+wholeFile.length+" byte från DB");
			meta =  FilesAndBytes.getFilemetaFromArray(wholeFile);
		}
		catch(Exception e) {
			try {
				if(is != null) {
					is.close();
				}
			}
			catch(Exception e2) {
			}
			meta = null;
			throw e;
		}
		try {
			if(is != null) {
				is.close();
			}
			is = null;
		}
		catch(Exception e) {
			is = null;
		}
		handleMetaAfterDatabaseRead(meta, lte, po);
		filename = new String(FilesAndBytes.getFilenameFromMeta(meta));
		File f = null;
		if(myFile.isDirectory()) {
			f = new File(myFile, filename);
		}
		else {
			f = myFile;
		}
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(FilesAndBytes.getFilecontentsFromArray(wholeFile));
		fos.close();
		//System.out.println("BlobActionHelper.blobButtonDownloadPressed - filen att öppna är "+f.getAbsolutePath());
		if(UniversalTellUser.info_ask_for_yes_propose_no(parent, "Do you want to open the file?", "Open")) {
			Clipboard.desktopOpen(f.getAbsolutePath(), lte);
		}
	}

	public static void blobButtonSavePressed(final LobTypeEditor lte, JFrame parent, ClientDBObject po) throws Exception {
		if(lte.isUnchanged()) {
			blobButtonDownloadPressed(lte, parent, po);
			return;
		}
		saveToFilesystem(lte, parent, po);
		
	}
	
	private static File lastSaveDirectory = null;
	//private static File lastOpenDirectory = null;
	
	public static void saveToFilesystem(final LobTypeEditor lte, JFrame parent, ClientDBObject po) throws Exception {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select file to write");
		chooser.setCurrentDirectory(lastSaveDirectory);
		
		String filename = getFilenameFromBlobEditor(lte, parent);

		chooser.setSelectedFile(new File(filename));
		int returnVal = chooser.showSaveDialog(parent);
		if(returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}
		File myFile = chooser.getSelectedFile();
		lastSaveDirectory = myFile.getParentFile();
		
		//--
		InputStream is = null;
		byte[] meta = null;
		byte[] wholeFile = null;
		try {
			is = addProgress(po.downloadFromBlob_with_executeQuery(lte.getName()));
			if(is == null) {
				LobValue lv = new LobValue();
				lv.setLength((int) myFile.length());
				lv.setFilename(myFile.getAbsolutePath().getBytes());
				lv.setDatabaseBased(true);
				lte.setValue(lv);
				//lte.setWritten(true);
				return;
			}
			wholeFile = Services.getCleartextFromFile(is);
			//System.out.println("BlobActionHelper.saveToFilesystem - läste "+wholeFile.length+" byte från DB");
			meta =  FilesAndBytes.getFilemetaFromArray(wholeFile);
		}
		catch(Exception e) {
			try {
				if(is != null) {
					is.close();
				}
			}
			catch(Exception e2) {
			}
			meta = null;
			throw e;
		}
		try {
			if(is != null) {
				is.close();
			}
		}
		catch(Exception e) {
		}
		is = null;
		handleMetaAfterDatabaseRead(meta, lte, po);
		try {
			FileOutputStream fos = new FileOutputStream(myFile.getAbsolutePath());
			fos.write(wholeFile);
			fos.close();
		}
		catch(Exception e) {
		}

		//--

		/*
		String filename = new String(lte.getName());
		FileOutputStream fos = new FileOutputStream(filename);
		FileInputStream fis = new FileInputStream(myFile);
		byte[] buf = new byte[10000];
		int len = -1;
		int lensum = 0;
		while((len = fis.read(buf)) != -1) {
			fos.write(buf, 0, len);
			System.out.println("BlobActionhelper.saveToFilesystem - läser "+len+" bytes");
			lensum += len;
		}
		System.out.println("BlobActionhelper.saveToFilesystem - läste sammanlagt "+lensum+" bytes");
		try {
			fis.close();
		}
		catch(Exception e) {
		}
		try {
			fos.close();
		}
		catch(Exception e) {
		}
		if(UniversalTellUser.info_ask_for_yes_propose_no(parent, "Do you want to open the file?", "Open")) {
			Clipboard.desktopOpen(myFile.getAbsolutePath(), lte);
		}
		*/
	}


	public static void blobButtonNullPressed(final LobTypeEditor lte, JFrame parent) throws Exception {
		LobValue lv = new LobValue();
		lv.setDatabaseBased(true);
		lte.setValue(lv);
	}
	
}
