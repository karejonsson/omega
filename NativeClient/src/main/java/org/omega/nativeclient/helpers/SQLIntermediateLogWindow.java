package org.omega.nativeclient.helpers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLIssued;

public class SQLIntermediateLogWindow {

	private JFrame f = null;
	private JTable t = null;
	private SQLIntermediateLogger si = null;
	
	public SQLIntermediateLogWindow(SQLIntermediateLogger si) {
		this.si = si;
		//(new Throwable()).printStackTrace();
	}
	
	public void graphicalUpdates() {
		if(f == null) {
			return;
		}
		f.invalidate();
		f.validate();
		f.repaint();
		t.invalidate();
		t.validate();
		t.repaint();
	}
	
	public int getLength() {
		return si.getLength();
	}
	
	public void openGUI() {
		if(f == null) {
			f = new JFrame("SQL intermediate graphical log");
		    f.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          f.setVisible(false);
		          f.dispose();
		          f = null;
		        }
		      });
		}
		JScrollPane sp = new JScrollPane();
		f.getContentPane().add(sp);
		if(t == null) {
			ResultSetTableModel model = new ResultSetTableModel();
			t = new JTable(model);
		}
		sp.setViewportView(t);

		JMenuBar mb = new JMenuBar();
		f.setJMenuBar(mb);
		JMenu file = new JMenu("File");
		mb.add(file);
		JMenuItem refresh = new JMenuItem("Copy to clipboard");
		file.add(refresh);
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				f.invalidate();
				f.validate();
				f.repaint();
			}
		});
		
		
		f.pack();
		f.setVisible(true);
	}

	private class ResultSetTableModel extends AbstractTableModel {  
		private static final long serialVersionUID = 1L;

		public ResultSetTableModel() {
		}

		public String getColumnName(int c) {
			if(c == 0) {
				return "SQL statement";
			}
			if(c == 1) {
				return "Time";
			}
			if(c == 2) {
				return "Outcome";
			}
			return "COL "+c+" ???";
		}

		public int getColumnCount() { 
			return 3;
		}

		public Object getValueAt(int r, int c) {
			if(r >= si.getLength()) {
				return "ROW = "+r+"< size = "+si.getLength()+" ???";
			}
			SQLIssued sqli = si.getElementAt(r);
			if(c == 0) {
				return sqli.getSQL();
			}
			if(c == 1) {
				return sqli.getTime();
			}
			if(c == 2) {
				return sqli.getOutcome();
			}
			return "COL = "+c+" ???";
		}

		public int getRowCount() {
			return si.getLength();
		}
	}

	public void closeGUI() {
		if(f == null) return;
		f.setVisible(false);
		f.dispose();
		f = null;
	}

	public boolean isActive() {
		return f != null;
	}

}
