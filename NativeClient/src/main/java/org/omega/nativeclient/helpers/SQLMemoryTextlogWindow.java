package org.omega.nativeclient.helpers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.omega.connectivity.logging.SQLMemoryLogger;

import se.modlab.generics.gui.util.Clipboard;

public class SQLMemoryTextlogWindow extends JFrame {

	private JTextArea log = new JTextArea();
	private JScrollPane sp = null;
	private org.omega.connectivity.logging.SQLMemoryLogger slw = null;
	private boolean logSQL = false;
	private boolean active = false;

	public SQLMemoryTextlogWindow(boolean logSQL, SQLMemoryLogger slw) {
		super("SQL Log window");
		this.slw = slw;
		this.logSQL = logSQL;
		sp = new JScrollPane();
		getContentPane().add(sp);
		sp.getViewport().add(log);
		setVisible(logSQL);
		log.setEditable(false);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		setMenuBar();
	}
	
	public int getLength() {
		return slw.getLength();
	}
	
	private void setMenuBar() {
		JMenuBar mb = new JMenuBar();
		setJMenuBar(mb);
		JMenu file = new JMenu("File");
		mb.add(file);
		JMenuItem refresh = new JMenuItem("Copy to clipboard");
		file.add(refresh);
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clipboard.setClipboard(slw.getTextContent());
			}
		});
	}
	
	private void setTextContent() {
		log.setText(slw.getTextContent());
	}
	
	public void logText(String text) {
		slw.logText(text);
		setTextContent();
		sp.getVerticalScrollBar().setValue(MAXIMIZED_VERT);
	}

	public void openGUI() {
		logSQL = true;
		setTextContent();
		if(!active) {
			active = true;
		}
		setVisible(active);
		pack();
		invalidate();
		validate();
		setSize(300, 500);
	}

	public void closeGUI() {
		if(!active) return;
		setVisible(false);
		dispose();
		active = false;
	}

	public boolean isActive() {
		return active;
	}

	public void reset() {
		if(!logSQL) {
			return;
		}
		log.setText("");
	}

}
