package org.omega.nativeclient.helpers;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class SystemProgrammingResourceAccessImplementation implements SystemProgrammingResourceAccess {
	
	private Component component = null;
	
	public SystemProgrammingResourceAccessImplementation() {
		this(null);
	}

	public SystemProgrammingResourceAccessImplementation(Component component) {
		this.component = component;
	}

	@Override
	public void infoMessageDialog_OK(String frametitle, String message) {
		System.out.println("SystemProgrammingResourceAccess.infoDialog_OK(\""+frametitle+"\", \""+message+"\")");
		JOptionPane.showMessageDialog(component, message, frametitle, JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void errorMessageDialog_OK(String frametitle, String message) {
		System.out.println("SystemProgrammingResourceAccess.errorDialog_OK(\""+frametitle+"\", \""+message+"\")");
		JOptionPane.showMessageDialog(component, message, frametitle, JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void warningMessageDialog_OK(String frametitle, String message) {
		System.out.println("SystemProgrammingResourceAccess.warningDialog_OK(\""+frametitle+"\", \""+message+"\")");
		JOptionPane.showMessageDialog(component, message, frametitle, JOptionPane.WARNING_MESSAGE);
	}

	@Override
	public Boolean infoChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
		return choiceDialog(frametitle, question, JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public Boolean warningChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
		return choiceDialog(frametitle, question, JOptionPane.WARNING_MESSAGE);
	}

	@Override
	public Boolean errorChoiceDialog_YES_NO_Cancel(String frametitle, String question) {
		return choiceDialog(frametitle, question, JOptionPane.ERROR_MESSAGE);
	}
	
	private Boolean choiceDialog(String frametitle, String question, int messageType) {
		System.out.println("SystemProgrammingResourceAccess.choiceDialog_OK_Cancel(\""+frametitle+"\", \""+question+"\")");
		int out = JOptionPane.showConfirmDialog(component, question, frametitle,
				JOptionPane.YES_NO_CANCEL_OPTION, messageType);
		if(out == JOptionPane.CANCEL_OPTION) {
			return null;
		}
		return out == JOptionPane.YES_OPTION;
	}

	@Override
	public String textDialog_OK_Cancel(String frametitle, String question) {
		System.out.println("SystemProgrammingResourceAccess.textDialog_OK_Cancel(\""+frametitle+"\", \""+question+"\")");
		return JOptionPane.showInputDialog(component, question, frametitle, JOptionPane.PLAIN_MESSAGE);
	}

	@Override
	public void announceRuntimeError(String message) {
		UniversalTellUser.error(null, message);
	}

}
