package org.omega.nativeclient.helpers;

import java.awt.Window;

import org.omega.clientcommon.editors.DatetimeEditorCommons;
import org.omega.clientcommon.editors.DoubleEditorCommons;
import org.omega.clientcommon.editors.EditorCommonsVisitor;
import org.omega.clientcommon.editors.IntegerEditorCommons;
import org.omega.clientcommon.editors.LobEditorCommons;
import org.omega.clientcommon.editors.PointerEditorCommons;
import org.omega.clientcommon.editors.ReferrerEditorCommons;
import org.omega.clientcommon.editors.TextareaEditorCommons;
import org.omega.clientcommon.editors.TextlineEditorCommons;
import org.omega.clientcommon.editors.UnmanageableEditorCommons;
import org.omega.nativeclient.typegraphics.DatetimeGraphicalEditor;
import org.omega.nativeclient.typegraphics.DoubleGraphicalEditor;
import org.omega.nativeclient.typegraphics.GraphicalEditor;
import org.omega.nativeclient.typegraphics.IntGraphicalEditor;
import org.omega.nativeclient.typegraphics.LobTypeEditor;
import org.omega.nativeclient.typegraphics.PointerGraphicalEditor;
import org.omega.nativeclient.typegraphics.ReferrerGraphicalEditor;
import org.omega.nativeclient.typegraphics.TextAreaGraphicalEditor;
import org.omega.nativeclient.typegraphics.TextLineGraphicalEditor;
import org.omega.nativeclient.typegraphics.UnmanageableTypeEditor;

import se.modlab.generics.exceptions.IntolerableException;

public class GetGraphicalEditorVisitor implements EditorCommonsVisitor {
	
	private GraphicalEditor ge = null;
	private Window currentWindow = null;
	
	public GetGraphicalEditorVisitor(Window currentWindow) {
		this.currentWindow = currentWindow;
	}
	
	public GraphicalEditor getEditor() {
		return ge;
	}

	public void visit(IntegerEditorCommons integerEditorCommons) throws Exception {
		ge = new IntGraphicalEditor(integerEditorCommons);
	}

	public void visit(DoubleEditorCommons doubleEditorCommons) throws Exception {
		ge = new DoubleGraphicalEditor(doubleEditorCommons);
	}

	public void visit(DatetimeEditorCommons datetimeEditorCommons) throws Exception {
		ge = new DatetimeGraphicalEditor(datetimeEditorCommons);
	}

	public void visit(LobEditorCommons lobEditorCommons) throws Exception {
		ge = new LobTypeEditor(lobEditorCommons);
	}

	public void visit(PointerEditorCommons pointerEditorCommons) throws Exception {
		ge = new PointerGraphicalEditor(pointerEditorCommons);
	}

	public void visit(ReferrerEditorCommons referrerEditorCommons) throws Exception {
		ge = new ReferrerGraphicalEditor(referrerEditorCommons, currentWindow);
	}

	public void visit(TextareaEditorCommons textareaEditorCommons) throws Exception {
		ge = new TextAreaGraphicalEditor(textareaEditorCommons);
	}

	public void visit(TextlineEditorCommons textlineEditorCommons) throws Exception {
		ge = new TextLineGraphicalEditor(textlineEditorCommons);
	}

	public void visit(UnmanageableEditorCommons unmanageableEditorCommons) throws Exception {
		ge = new UnmanageableTypeEditor(unmanageableEditorCommons);
	}

}
