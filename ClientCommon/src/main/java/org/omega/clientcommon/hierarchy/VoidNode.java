package org.omega.clientcommon.hierarchy;

import org.omega.clientcommon.editors.ClientTableLogic;

public class VoidNode extends HierarchyNode {
	
	private ClientTableLogic ctl = null;
	
	public VoidNode(ClientTableLogic ctl) {
		this.ctl = ctl;
	}
	
	private String reference = null;
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}
