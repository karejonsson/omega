package org.omega.clientcommon.hierarchy;

import java.util.ArrayList;
import java.util.List;

import org.omega.clientcommon.editors.ClientTableLogic;

//import org.omega.configuration.model.ModelConfiguration;
//import org.omega.configuration.model.TableConfiguration;
//import org.omega.configuration.recreatesql.Recreator;
//import org.omega.configuration.struct.Column;
//import org.omega.configuration.struct.ForeignKeyDeclaration;

public class MetaNode {
	
	//private ModelConfiguration hs;
	//private TableConfiguration ths;
	//private Recreator rec = null;

	/*
	public MetaNode(ModelConfiguration hs, TableConfiguration ths, Recreator rec) {
		this.hs = hs;
		this.ths = ths;
		this.rec = rec;
	}
	*/
	
	private ClientTableLogic ctl = null;
	
	public MetaNode(ClientTableLogic ctl) {
		this.ctl = ctl;
	}
	
	public String toString() {
		return ctl.translate(ctl.getTableName())+" ("+ctl.getLevel()+")";
	}

	public String getTableName() {
		return ctl.getTableName();
	}

	public List<MetaNode> getChildren() {
		List<MetaNode> out = new ArrayList<MetaNode>();
		int fkcount = ctl.getNoForeignKeyDeclarations();
		//System.out.println("MetaNode.getChildren ="+fkcount+", tabell "+ctl.getTableName());
		for(int i = 0 ; i < fkcount ; i++) {
			String foreignKeyName = ctl.getNumeratedReferenceName(i);
			ClientTableLogic otherCtl = ctl.getClientTableLogicForReferredTable(foreignKeyName);
			out.add(new MetaNode(otherCtl));
		}
		return out;
	}
	
	public Object getValueAt(int r, int c) {
		return ctl.getMetaValueAt(r, c);
		/*
		Column col = ths.getCreateStatement().getColumn(r);
		if(c == 0) {
			return ths.translate(col.getName());
		}
		if(c == 1) {
			return rec.columnType(col);
		}
		return col.getType().getUntypedDefault(rec);
		*/
	}

	public int getRowCount() {
		return ctl.getMetaRowCount();
	}

	public ClientTableLogic getClientTableLogic() {
		return ctl;
	}

}
