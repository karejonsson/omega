package org.omega.clientcommon.hierarchy;

import java.util.List;
import java.util.Vector;

import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.clientcommon.editors.ReferrerEditorCommons;
import org.omega.configuration.renderers.DefaultReferrernodeTextRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.groovyintegration.pool.DBPoolObject;

import se.modlab.generics.exceptions.IntolerableException;

public class ReferenceNode extends HierarchyNode {
	
	private ClientDBObject cdbo = null;
	private ForeignKeyDeclaration fkd = null;
	private TextRenderer tr = null;
	
	public ReferenceNode(ClientDBObject cdbo, ForeignKeyDeclaration fkd) {
		this.cdbo = cdbo;
		this.fkd = fkd;
	}
	
	public boolean referencesSame(ReferrerEditorCommons rec) {
		return rec.referencesSame(fkd);
	}
	
	public String toStringDebug() {
		return "ReferenceNode: "+cdbo.toString()+", ForeignKeyDeclaration "+fkd;
	}
	
	public ClientDBObject getReferredobject() {
		return cdbo;
	}
	
	public ClientTableLogic getClientTableLogic() {
		return cdbo.getClientTableLogic();
	}
	
	public String getConstraintName() {
		return fkd.getConstraintname();
	}
	
	public List<DataNode> getReferringObjects() throws Exception {
		return cdbo.getUpstreamPoolObjectsHavingPointer(fkd);
	}

	public String getTableColumnName(int pos) {
		return cdbo.getTablesColumnName(pos);
	}
	
	public int getColumnCount() {
		return cdbo.getTablesNoColumns();
	}
	
	public String toString() {
		try {
			ClientModelLogic cml = cdbo.getClientTableLogic().getClientModelLogic();
			ClientTableLogic ctl = cml.getTableLogic(fkd.getReferringTablename());
			//System.out.println("ReferenceNode.toString() fdk="+fkd);
			return ctl.renderForClassText(); // FEL! .rectl.recdbo.renderForTreeReferrer(fkd.getName());
		}
		catch(Exception e) {
			cdbo.getClientTableLogic().logTrace(e);
			return "<no rendering>";
		}
	}
}
