package org.omega.clientcommon.hierarchy;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.bsf.BSFException;
import org.omega.clientcommon.editors.ClientDBObject;
import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.groovyintegration.pool.DBPoolObject;

public class DataNode extends HierarchyNode {
	
	private ClientDBObject cdbo = null;
	
	public DataNode(ClientDBObject cdbo) {
		this.cdbo = cdbo;
	}
	
	public void deleteFromDatabase() throws Exception {
		cdbo.delete();
	}
	
	public ClientDBObject getClientDBObject() {
		return cdbo;
	}
	
	private String reference = null;
	
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
	public List<HierarchyNode> getReferred() throws Exception {
		List<HierarchyNode> out = new ArrayList<HierarchyNode>();
		for(int i = 0 ; i < cdbo.getNoReferences() ; i++) {
			String name = cdbo.getNameOfReference(i);
			ClientDBObject obj = cdbo.getReferredObject(name);
			if(obj != null) {
				if(obj.isNullObject()) {
					VoidNode vn = new VoidNode(obj.getClientTableLogic());
					vn.setReference(name);
					out.add(vn);
				}
				else {
					DataNode dn = new DataNode(obj);
					dn.setReference(name);
					out.add(dn);
				}
			}
			else {
				ClientTableLogic ctl = cdbo.getClientTableLogic().getClientTableLogicForReferredTable(name);
				VoidNode vn = new VoidNode(ctl);
				vn.setReference(name);
				out.add(vn);
			}
		}
		return out;
	}

	public List<ReferenceNode> getReferrers() throws Exception {
		List<ReferenceNode> out = new ArrayList<ReferenceNode>();
		for(int i = 0 ; i < cdbo.getNoReferences() ; i++) {
			out.add(cdbo.getReferrerNode(i));
		}
		return out;
	}
	
	public boolean sameObject(DataNode dn) {
		return cdbo.sameObject(dn.cdbo);
	}

	public boolean sameObject(ClientDBObject dn) {
		return cdbo.sameObject(dn);
	}
	
	public ClientTableLogic getClientTableLogic() {
		return cdbo.getClientTableLogic();
	}
	
	public void refresh() throws Exception {
		cdbo.refresh();
	}
	
	public String renderForTableAtPosition(int pos) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		return cdbo.renderForTableAtPosition(pos);
	}
	
	public InputStream downloadFromBlob_with_executeQuery(String colname) throws Exception {
		return cdbo.downloadFromBlob_with_executeQuery(colname);
	}

	public Object getValue(String key) throws Exception {
		if(cdbo.hasColumn(key)) {
			return cdbo.getValue(key);
		}
		return cdbo.getReferredObject(key);
	}

	public void setValue(String key, Object value) throws Exception {
		//System.out.println("DataNode.setValue("+key+", "+value+")");
		if(cdbo.hasColumn(key)) {
			cdbo.setValue(key, value);
			return;
		}
		cdbo.setReference(key, (ClientDBObject) value);
	}
	
}
