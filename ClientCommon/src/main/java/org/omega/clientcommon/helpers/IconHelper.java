package org.omega.clientcommon.helpers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import java.awt.Graphics;

import java.awt.Image;

import org.omega.clientcommon.editors.ClientTableLogic;
import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
//import org.omega.nativeclient.treeview.IconNodeRenderer;

public class IconHelper {

    public static final int size = 26;
    public static final int usedImageBufferType = BufferedImage.TYPE_3BYTE_BGR;//.TYPE_INT_ARGB;

	public static ImageIcon getImageIconBestEffort(TableConfiguration ths, String specifier) {
		try {
			ImageIconByteArrayHolder bh =  ths.getImageIconBestEffort(specifier);
			if(bh == null) {
				return null;
			}
			InputStream in = new ByteArrayInputStream(bh.getBytes());
			BufferedImage image = ImageIO.read(in);
			return new ImageIcon(image);
		}
		catch(IOException ioe) {
			return null;
		}
	}

	public static ImageIcon getImageIconBestEffort(ModelConfiguration hs, String specifier) {
		try {
			ImageIconByteArrayHolder bh =  hs.getImageIconBestEffort(specifier);
			if(bh == null) {
				return null;
			}
			InputStream in = new ByteArrayInputStream(bh.getBytes());
			BufferedImage image = ImageIO.read(in);
			return new ImageIcon(image);
		}
		catch(IOException ioe) {
			return null;
		}
	}

	/*
	public static void setImageIconAsImageIcon(DBTableHandlingSpecification ths, String specifier, BufferedImage bi) {
		WritableRaster raster = bi.getRaster();
		DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
		ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(data.getData());
		ths.setImageIconAsImageIcon(specifier, bh);
	}
	*/
	
	public static void setImageIconAsImageIcon(TableConfiguration ths, String specifier, BufferedImage bi) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi, "jpeg", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(imageInByte);
			ths.setImageIconAsImageIcon(specifier, bh);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/*
	public static void setImageIconAsImageIcon(DBTableHandlingSpecification ths, String specifier, byte[] imagebytes) {
		ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(imagebytes);
		ths.setImageIconAsImageIcon(specifier, bh);
	}
	*/
	
	public static ImageIconByteArrayHolder getByteArray(BufferedImage bi) {
		byte[] imageInByte = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi , "jpg", baos );
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		}
		catch(Exception e) {
			return null;
		}
		return new ImageIconByteArrayHolder(imageInByte);
		/*
		WritableRaster raster = bi.getRaster();
		DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
		return new ImageIconByteArrayHolder(data.getData());
		*/
	}

	/*
	public static void setImageIconAsImageIcon(DBHandlingSpecification hs, String specifier, BufferedImage bi) {
		WritableRaster raster = bi.getRaster();
		DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
		ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(data.getData());
		hs.setImageIconAsImageIcon(specifier, bh);
	}
	
	BufferedImage originalImage = ImageIO.read(new File("c:\\image\\mypic.jpg"));
ByteArrayOutputStream baos = new ByteArrayOutputStream();
ImageIO.write( originalImage, "jpg", baos );
baos.flush();
byte[] imageInByte = baos.toByteArray();
baos.close();

	*/
	public static void setImageIconAsImageIcon(ModelConfiguration hs, String specifier, BufferedImage bi) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( bi, "jpg", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(imageInByte);
			hs.setImageIconAsImageIcon(specifier, bh);
		}
		catch(Exception e) {
			
		}
	}

	
	/*
	public static void setImageIconAsImageIcon(DBHandlingSpecification hs, String specifier, byte[] imagebytes) {
		ImageIconByteArrayHolder bh = new ImageIconByteArrayHolder(imagebytes);
		hs.setImageIconAsImageIcon(specifier, bh);
	}
	*/

	public static void updateCacheWithNullPointerImage(ClientTableLogic env) {
		if(env.getCache(ModelConfiguration.skipNullPointerIconFile_P) == null) {
			try {
				if(env.getCache(ModelConfiguration.nullPointerIconFile_S) != null) {
					String filename = (String) env.getCache(ModelConfiguration.nullPointerIconFile_S);
					byte imageByteArrayReferrer[] = env.getClientModelLogic().getFilesBytearray(filename);
					ImageIcon imageicon = new ImageIcon(imageByteArrayReferrer);
					Image img = imageicon.getImage();
					env.setCache(ModelConfiguration.nullPointerImage_I, img);
				}
				else {
					env.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
				}
			}
			catch(Exception e) {
				env.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
			}
		}
	}
	
	public static ImageIconByteArrayHolder createAndInsertImageIcon(final ClientTableLogic env, final String tablePointedAt, final int size, byte imageByteArray[]) {
		ImageIcon imageicon = null;
		try {
			imageicon = new ImageIcon(imageByteArray);
		}
		catch(Exception e) {
			env.setProblem(tablePointedAt, TableConfiguration.typeNullPointerIconTrouble_P, true);
			return null;
		}
		Image img = imageicon.getImage();
		Image ref = (Image) env.getCache(ModelConfiguration.nullPointerImage_I);
		if(ref != null) {
			BufferedImage nullpointer = new BufferedImage(2*size, size, usedImageBufferType);
			Graphics g_nullpointer = nullpointer.createGraphics();
			g_nullpointer.drawImage(img, 0, 0, size, size, null);
			g_nullpointer.drawImage(ref, size, 0, size, size, null);
			ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			ImageIconByteArrayHolder iibah = IconHelper.getByteArray(nullpointer);
			env.setImageIconAsImageIcon(tablePointedAt, TableConfiguration.nullPointerIcon_I, iibah); 
		}
		else {
			BufferedImage nullpointer = new BufferedImage(size, size, usedImageBufferType);
			Graphics g_nullpointer = nullpointer.createGraphics();
			g_nullpointer.drawImage(img, 0, 0, size, size, null);
			ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			ImageIconByteArrayHolder iibah = IconHelper.getByteArray(nullpointer);
			env.setImageIconAsImageIcon(tablePointedAt, TableConfiguration.normalIcon_I, iibah);
		}
		return env.getImageIconBestEffort(tablePointedAt, TableConfiguration.nullPointerIcon_I);
	}
	
}

