package org.omega.clientcommon.editors;

import org.omega.configuration.struct.UnmanageableType;

public class UnmanageableEditorCommons extends EditorCommons {
	
	private UnmanageableType t = null;
	
	public UnmanageableEditorCommons(String name, UnmanageableType t) {
		super(name, null);
		this.t = t;
	}
	
	public String getTypename() {
		return t.getTypename();
	}
	
	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

	@Override
	public Object getValue() throws Exception {
		throw new Exception("Must not call getValue for type Unmanaged");
	}

	public void setValueUntyped(Object obj) throws Exception {
		throw new Exception("Unmanaged type editor cannot take value "+obj+" of type "+obj.getClass());
	}

}
