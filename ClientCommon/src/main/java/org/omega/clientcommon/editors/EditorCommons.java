package org.omega.clientcommon.editors;

import org.omega.groovyintegration.pool.DBPoolObject;

public abstract class EditorCommons {
	
	private String errorMessage = null;
	protected DBPoolObject objectEdited = null;
    protected String name = null;
    private boolean isEdited = false;
    private boolean isNulled = false;

	EditorCommons(String name, DBPoolObject objectEdited) {
		this.name = name;
		this.objectEdited = objectEdited;
	}
	
	public interface UpdateVisibleField {
		boolean isEditable();
		void setText(String text);
	}
	
	public boolean isTextual() {
		return false;
	}
	
	protected UpdateVisibleField uvf = null;
	
	public String getName() {
		return name;
	}

	public boolean isUnchanged() {
		return !isEdited;
	}
	 
	public boolean isEdited() {
		return isEdited;
	}
	
	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}
	
	public boolean isNulled() {
		return isNulled;
	}

	public void setNulled(boolean isNulled) {
		this.isNulled = isNulled;
	}

	public void edited() {
		isEdited = true;
		isNulled = false;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public abstract void accept(EditorCommonsVisitor ecv) throws Exception; 
	
	public abstract Object getValue() throws Exception;
	public abstract void setValueUntyped(Object obj) throws Exception;

	private boolean editable = true;
	
	public boolean isEditable() {
		return editable;
	}

	void setEditable(boolean editable) {
		this.editable = editable;
	}

	private boolean hasAutoUpdate = false;

	public boolean isHasAutoUpdate() {
		return hasAutoUpdate;
	}

	public void setHasAutoUpdate(boolean hasAutoUpdate) {
		this.hasAutoUpdate = hasAutoUpdate;
	}

	private boolean hasFileBrowserField = false;

	public boolean isHasFileBrowserField() {
		return hasFileBrowserField;
	}

	public void setHasFileBrowserField(boolean hasFileBrowserField) {
		this.hasFileBrowserField = hasFileBrowserField;
	}

}
