package org.omega.clientcommon.editors;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.bsf.BSFException;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.clientcommon.hierarchy.MetaNode;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.renderers.DefaultRownodeTextRenderer;
import org.omega.configuration.renderers.DefaultTablenodeTextRenderer;
import org.omega.configuration.renderers.TableRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.ProgramBlock;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.groovyintegration.pool.DBPoolObject;

import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

import org.omega.connectivity.helpers.DBTypeManagement;

public class ClientTableLogic {
	
	private EmbeddedTableHandling eth = null;
	private ClientModelLogic cml = null;

	ClientTableLogic(EmbeddedTableHandling eth, ClientModelLogic cml) {
		this.eth = eth;
		this.cml = cml;
	}
	
	public String getDBsName() {
		return eth.getDBsName();
	}
	
	public String getAPI() {
		return cml.getAPI(getTableName());
	}
	
	public boolean hasTextField() {
		return eth.ths.hasTextField();
	}
	
	/*
	instanceRenderer = env.getEmbeddedEnvironment().ths.getClassTextRenderer();
	if(instanceRenderer == null) {
		instanceRenderer = new DefaultTablenodeTextRenderer(env.getEmbeddedEnvironment().ths);
	}
	*/
	public String renderForClassText() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer tr = eth.ths.getClassTextRenderer();
		if(tr == null) {
			tr = new DefaultTablenodeTextRenderer(eth.ths);
		}
		return cml.render(tr);
	}
	
	public List<DataNode> getAll() throws Exception {
		return cml.getAll(getTableName());
	}
	
	public int getTableLength() throws Exception {
		return cml.getTableLength(eth);
	}
	
	public boolean isUniqueConstraintViolation(Exception e) {
		return eth.isUniqueConstraintViolation(e);
	}
	
	public boolean isConstraintViolation(Exception e) {
		return eth.isConstraintViolation(e);
	}
	
	public int getColumnCount() {
		int count =  eth.ths.getTableRenderer().getNoColumns();
		return (count == 0) ? 1 : count;
	}
	
	public String getColumnNameTranslated(int c) { 
		String name = eth.ths.getTableRenderer().getColumnName(c);//colnames.elementAt(c);
		return translate(name);
	}
	
	public String translate(String s) {
		return eth.translate(s);
	}
	
	public String getObjectName() {
		return eth.getObjectName();
	}
	
	public String getTableName() {
		return eth.getTableName();
	}
	
	public ImageIconByteArrayHolder getImageIconBestEffort(String table, String key) {
		return eth.getImageIconBestEffort(table, key);
	}
	
	public ImageIconByteArrayHolder getImageIconBestEffort(String key) {
		return eth.getImageIconBestEffort(key);
	}
	
	public ImageIconByteArrayHolder getIconFileByteArrayHolder() {
		return getIconFileByteArrayHolder(getIconFile());
	}
	
	public ImageIconByteArrayHolder getIconFileByteArrayHolderOfSpecifiedTable(String table) {
		return getIconFileByteArrayHolder(getIconFile(table));
	}
	
	private ImageIconByteArrayHolder getIconFileByteArrayHolder(String iconfile) {
		if(iconfile == null) {
			return null;
		}
		byte imageByteArray[] = null;
		try {
			imageByteArray = cml.getFilesBytearray(iconfile);
		}
		catch(Exception e) {
			logText("No icon file for null reference for table "+getTableName());
			return null;
		}
		return new ImageIconByteArrayHolder(imageByteArray);		
	}
	
	public Object getCache(String flag) {
		return eth.getCache(flag);
	}
	
	public void setCache(String flag, Object value) {
		eth.setCache(flag, value);
	}
	
	public String getIconFile(String table) {
		return eth.getIconfile(table);
	}
	
	public String getIconFile() {
		return eth.getIconfile();
	}
	
	public boolean getProblem(String table, String key) {
		return eth.getProblem(table, key);
	}
	
	public ClientModelLogic getClientModelLogic() {
		return cml;
	}
	
	public void logTrace(Exception e) {
		cml.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		cml.log(message, e);
	}
	
	public void logText(String message) {
		cml.logText(message);
	}
	
	TextRenderer getReferenceDialogTextRenderer(String name) {
		return eth.getReferenceDialogTextRenderer(name);
	}

	TextRenderer getColumnRenderer(int c) {
		return eth.ths.getTableRenderer().getColumnRenderer(c);
	}
		
	EmbeddedTableHandling getEmbeddedTableHandling() {
		return eth;
	}
	
	public ImageIconByteArrayHolder getImageIconBestEffort() {
		return getImageIconBestEffort(eth.getObjectName(), TableConfiguration.nullPointerIcon_I);		
	}
	
	public void setProblem(String table, String key, boolean problem) {
		eth.setProblem(table, key, problem);
	}
	
	public void setProblem(String key, boolean problem) {
		eth.setProblem(key, problem);
	}

	public void setImageIconAsImageIcon(String table, String key, ImageIconByteArrayHolder icon) {
		eth.setImageIconAsImageIcon(table, key, icon);
	}

	public void setImageIconAsImageIcon(String key, ImageIconByteArrayHolder icon) {
		eth.setImageIconAsImageIcon(key, icon);
	}
	
	public String getMetaDescription() {
		return eth.getMetaDescription();
	}

	String getMetaDescription(DBPoolObject po) {
		return eth.getMetaDescription(po.getHashtable());
	}

	public int getNoReferringConstraints() {
		return eth.ths.getNoReferringConstraints();
	}
	
	public int getFirstDatetimeColumnNumer() {
		TableRenderer tr = eth.ths.getTableRenderer();
		for(int i = 0 ; i < tr.getNoColumns() ; i++) {
			String colname = tr.getColumnName(i);
			Column col = eth.ths.getColumn(colname);
			if(col == null) {
				continue;
			}
			if(col.getType() instanceof DatetimeType) {
				return i;
			}
		}
		return -1;
	}

	public int[] getNumericColumnsNumbers() {
		Vector<Integer> outs = new Vector<Integer>();
		TableRenderer tr = eth.ths.getTableRenderer();
		for(int i = 0 ; i < tr.getNoColumns() ; i++) {
			String colname = tr.getColumnName(i);
			Column col = eth.ths.getColumn(colname);
			if(col == null) {
				continue;
			}
			if(col.getType() instanceof NumericType) {
				outs.add(i);
			}
		}
		int out[] = new int[outs.size()];
		for(int i = 0 ; i < outs.size(); i++) {
			out[i] = outs.elementAt(i);
		}
		return out;
	}
	
	public String getColumnName(int i) {
		TableRenderer tr = eth.ths.getTableRenderer();
		try {
			return tr.getColumnName(i);
		}
		catch(Exception e) {}
		return null;
	}

	public MetaNode getMetaNode() {
		return cml.getMetaNode(getTableName());
	}
	
	void executeSilentOnUpdate(DBPoolObject po, boolean persisted) {
		for(int i = 0 ; i < eth.ths.getNoOnNewUpdate() ; i++) {
			ProgramBlock prg = eth.ths.getOnNewUpdate(i);
			try {
				cml.executeInNodeScope(po, prg, persisted);
				//scope.exec(prg);
			}	
			catch(Exception ie) {
				logTrace(ie);
			}
		}
		for(int i = 0 ; i < eth.ths.getNoOnAllUpdate() ; i++) {
			ProgramBlock prg = eth.ths.getOnAllUpdate(i);
			try {
				//scope.exec(prg);
				cml.executeInNodeScope(po, prg, persisted);
			}	
			catch(Exception ie) {
				logTrace(ie);
			}
		}
		po.alertListeners();
	}

	void executeOnInsert(DBPoolObject po, boolean persisted) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		for(int i = 0 ; i < eth.ths.getNoOnInsertExecute() ; i++) {
			ProgramBlock prg = eth.ths.getOnInsertExecute(i);
			cml.executeInNodeScope(po, prg, persisted);
		}
		for(int i = 0 ; i < eth.ths.getNoOnInsertAndUpdateExecute() ; i++) {
			ProgramBlock prg = eth.ths.getOnInsertAndUpdateExecute(i);
			cml.executeInNodeScope(po, prg, persisted);
		}
	}
	
	public int getNoForeignKeyDeclarations() {
		return eth.ths.getNoForeignKeyDeclarations();
	}

	public String getNumeratedReferenceName(int idx) {
		CreateStatement cs = eth.ths.getCreateStatement();
		final ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(idx);
		if(fkd == null) {
			logText("ClientTableLogic.getNumeratedReferenceName("+idx+") = null, Typen är "+getTableName());
		}
		return fkd.getConstraintname();
	}

	public ClientTableLogic getClientTableLogicForReferredTable(String foreignKeyName) {
		CreateStatement cs = eth.ths.getCreateStatement();
		final ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(foreignKeyName);
		return cml.getTableLogic(fkd.getReferencedTablename());
	}

	public String getFileBrowserFieldBlobMapping(String name) {
		return eth.ths.getFileBrowserFieldBlobMapping(name);
	}
	
	public Object getMetaValueAt(int r, int c) {
		Column col = eth.ths.getCreateStatement().getColumn(r);
		if(c == 0) {
			return eth.ths.translate(col.getName());
		}
		if(c == 1) {
			return eth.columnType(r);
		}
		return eth.getUntypedDefault(col.getType());
		//return col.getType().getUntypedDefault(rec);
	}

	public int getMetaRowCount() {
		return eth.ths.getCreateStatement().getNoColumns();
	}

	public boolean hasSeparator(int linenr) {
		return eth.ths.hasSeparator(linenr);
	}

	public int getLevel() {
		return eth.ths.getLevel();
	}

	public List<EditorCommons> getEditorsForEditables(String implicitConstraintname, boolean forEditing) throws Exception {
		return getEditorsForEditables(implicitConstraintname, null, forEditing);
	}

	List<EditorCommons> getEditorsForEditables(String implicitConstraintname, DBPoolObject po, boolean forEditing) throws Exception {
		CreateStatement cs = getEmbeddedTableHandling().ths.getCreateStatement();

		Vector<ForeignKeyDeclaration> singleMemberFkds = new Vector<ForeignKeyDeclaration>();
		Vector<ForeignKeyDeclaration> multipleMemberFkds = new Vector<ForeignKeyDeclaration>();
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
			if(fkd.getNoReferringMembers() == 1) {
				singleMemberFkds.add(fkd);
			}
			else {
				multipleMemberFkds.add(fkd);
			}
		}
		List<EditorCommons> out = new ArrayList<EditorCommons>();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			final String colname = col.getName();
			org.omega.configuration.struct.Type t = col.getType();

			Object initialValue = po == null ? null: po.get(colname); 
			
			if(col.isAutoIncrement()) {
				continue;
			}

			int noForeignKeyMemberships = cs.getNumberOfForeignKeyDeclarationMemberships(colname);
			if(noForeignKeyMemberships == 0) {
				final EditorCommons ge = GetEditorCommonsVisitor.getEditorCommons(po, col, t, this);

				boolean editable = !getEmbeddedTableHandling().ths.hasFileBrowserField(colname) && 
								   !getEmbeddedTableHandling().ths.hasAutoUpdate(colname);

				ge.setEditable(forEditing && editable);
				ge.setValueUntyped(initialValue != null ? initialValue : DBTypeManagement.getDefault(col));
				ge.setHasAutoUpdate(getEmbeddedTableHandling().ths.hasAutoUpdate(colname));
				ge.setHasFileBrowserField(getEmbeddedTableHandling().ths.hasFileBrowserField(colname));

				out.add(ge);
			}
			else {
				final ForeignKeyDeclaration fkd = getSingleForeignKeyDeclaration(colname, singleMemberFkds);
				if(fkd == null) {
					// This means that this pointer member only appears in a combined foreign key declaration
					continue;
				}
				
				if(implicitConstraintname != null && fkd.getConstraintname().equals(implicitConstraintname)) {
					// For the case when a new is done on a referenced object. Then this may not change.
					continue;
				}

				//final PointerEditorCommons pge = new PointerEditorCommons(fkd.getName(), pool, po, ctl); 
				final PointerEditorCommons pge = cml.getPointerEditorCommons(fkd.getConstraintname(), po); 
				//pge.setReferenceEnumeration(ctl.);
				
				boolean editable = 
						!getEmbeddedTableHandling().ths.hasFileBrowserField(colname) && 
						!getEmbeddedTableHandling().ths.hasAutoUpdate(colname) && 
						!getEmbeddedTableHandling().ths.hasAutoUpdate(fkd.getConstraintname());
				
				boolean hasAutoUpdate = 
						getEmbeddedTableHandling().ths.hasAutoUpdate(colname) ||
						getEmbeddedTableHandling().ths.hasAutoUpdate(fkd.getConstraintname());

				pge.setEditable(forEditing && editable);
				pge.setHasAutoUpdate(hasAutoUpdate);
				if(implicitConstraintname != null) {
					ForeignKey fk = po.getForeignKeyDownstreams(implicitConstraintname);
					PrimaryKey pk = fk.assemblePrimaryKey();	
					pge.setPointersReferredInstance(pk);
				}
				pge.setHasFileBrowserField(getEmbeddedTableHandling().ths.hasFileBrowserField(colname));
				out.add(pge);
			}
		}
		for(int i = 0 ; i < multipleMemberFkds.size() ; i++) {
			ForeignKeyDeclaration fkd = multipleMemberFkds.elementAt(i);

			if(implicitConstraintname != null && fkd.getConstraintname().equals(implicitConstraintname)) {
				// For the case when a new is done on a referenced object. Then this may not change.
				continue;
			}
			
			final PointerEditorCommons pge = cml.getPointerEditorCommons(fkd.getConstraintname(), po); 
			
			boolean editable = 
					!getEmbeddedTableHandling().ths.hasAutoUpdate(fkd.getConstraintname());
			
			boolean hasAutoUpdate = 
					getEmbeddedTableHandling().ths.hasAutoUpdate(fkd.getConstraintname());

			pge.setEditable(editable);
			pge.setHasAutoUpdate(hasAutoUpdate);
			pge.setHasFileBrowserField(false);
			out.add(pge);
		}
		return out;
	}
	
	private static ForeignKeyDeclaration getSingleForeignKeyDeclaration(String membername, Vector<ForeignKeyDeclaration> fkds) throws Exception {
		// Since the assumption is as point 1 below states this lookup is simpler than expected.
		 for(int i = 0 ; i < fkds.size() ; i++) {
			 ForeignKeyDeclaration fkd = fkds.elementAt(i);
			 if(fkd.getNoReferringMembers() != 1) {
				 throw new Exception("Found "+fkd+" which does not have one member as the intention was to have verified by here.");
			 }
			 if(fkd.getReferringMember(0).toUpperCase().compareTo(membername.toUpperCase()) == 0) {
				 return fkd;
			 }
		 }
		 return null;
	}

	public Vector<ClientDBObject> searchPoolObjects(Hashtable<String, Object> ht) throws Exception {
		return cml.searchPoolObjects(this, ht);
	}

	public Icon getMetaTableNodeImageIcon(int size, int usedImageBufferType) {
		  //String typename = getName(); 
		  TableConfiguration ths = eth.ths;
		  //DBTableHandlingSpecification ths = hs.getDBTableHandlingSpecification(typename);
		  if(ths.getProblem(TableConfiguration.typeIconTrouble_P)) {
			  return null;
		  }
		  String iconfile = ths.getIconfile();
		  //ImageIcon icon = ths.getImageIconBestEffort(DBTableHandlingSpecification.normalIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(ths, TableConfiguration.normalIcon_I);
		  if(icon != null) {
			  return icon;
		  }
		  if(iconfile == null) {
			  return null;
		  }
		  //DBHandlingSpecification hs = ths.getDBHandlingSpecification();
		  byte imageByteArray[] = ths.getIconfileBytes();//((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  //String fullFilename = hs.getFileSystemReferenceCatalog()+File.separator+iconfile;
		  ImageIcon imageicon = null;
		  try {
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  ths.setProblem(TableConfiguration.typeIconTrouble_P, true);
			  return null;
		  }
		  Image img = imageicon.getImage();
		  BufferedImage straight = new BufferedImage(size, size, usedImageBufferType);
		  Graphics g_straight = straight.createGraphics();
		  g_straight.drawImage(img, 0, 0, size, size, null);
		  ImageIcon straightIcon = new ImageIcon(straight);
		  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.normalIcon_I, straightIcon);
		  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.normalIcon_I, straight);
		  return straightIcon;
	}

	public Icon getTableNodeImageIcon(int size, int usedImageBufferType) {
		  //String typename = tn.getName();
		  TableConfiguration ths = eth.ths;//tn.getEnvironment().getEmbeddedEnvironment().ths; 
		  //DBTableHandlingSpecification ths = hs.getDBTableHandlingSpecification(typename);
		  if(ths.getProblem(TableConfiguration.typeIconTrouble_P)) {
			  return null;
		  }
		  String iconfile = ths.getIconfile();
		  //ImageIcon icon = ths.getImageIconBestEffort(DBTableHandlingSpecification.normalIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(ths, TableConfiguration.normalIcon_I);
		  if(icon != null) {
			  return icon;
		  }
		  if(iconfile == null) {
			  return null;
		  }
		  //DBHandlingSpecification hs = ths.getDBHandlingSpecification();
		  byte imageByteArray[] = ths.getIconfileBytes();//((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  //String fullFilename = hs.getFileSystemReferenceCatalog()+File.separator+iconfile;
		  ImageIcon imageicon = null;
		  try {
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  ths.setProblem(TableConfiguration.typeIconTrouble_P, true);
			  return null;
		  }
		  Image img = imageicon.getImage();
		  BufferedImage straight = new BufferedImage(IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
		  Graphics g_straight = straight.createGraphics();
		  g_straight.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
		  ImageIcon straightIcon = new ImageIcon(straight);
		  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.normalIcon_I, straightIcon);
		  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.normalIcon_I, straight);
		 return straightIcon;
	}

	public Icon getReferrerNodeImageIcon(int size, int usedImageBufferType) {
		  //String typename = rn.getName();
		  EmbeddedTableHandling env = eth;//rn.getEnvironment().getEmbeddedEnvironment();
		  TableConfiguration ths = env.ths;
		  //DBTableHandlingSpecification ths = hs.getDBTableHandlingSpecification(typename);
		  if(ths.getProblem(TableConfiguration.typeIconTrouble_P)) {
			  return null;
		  }
		  String iconfile = ths.getIconfile();
		  //ImageIcon icon = ths.getImageIconBestEffort(DBTableHandlingSpecification.referrerIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(ths, TableConfiguration.referrerIcon_I);
		  if(icon != null) {
			  return icon;
		  }
		  if(iconfile == null) {
			  return null;
		  }
		  ModelConfiguration hs = ths.getDBHandlingSpecification();
		  //hs.printCache("IconNodeRenderer.handle referrer");
		  byte imageByteArray[] = ths.getIconfileBytes();//((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  //String fullFilename = hs.getFileSystemReferenceCatalog()+File.separator+iconfile;
		  
		  if(hs.getCache(ModelConfiguration.skipReferrerIconFile_P) == null) {
			  try {
				  if(hs.getCache(ModelConfiguration.referrerIconFile_S) != null) {
					  String filename = (String) hs.getCache(ModelConfiguration.referrerIconFile_S);
					  hs.getFileSystemReferenceCatalog();
					  HierarchyObject ho = ((HierarchyLeaf) (hs.getFileSystemReferenceCatalog()).getChild(filename));
					  if(ho == null) {
						  env.logText("No icon data available from "+filename);
						  System.out.println("Ingen ikondata tillgänglig via "+filename);
					  }
					  if(ho != null) {
						  byte imageByteArrayReferrer[] = ((HierarchyLeaf) ho).getByteArray();
						  ImageIcon imageicon = new ImageIcon(imageByteArrayReferrer);
						  Image img = imageicon.getImage();
						  hs.setCache(ModelConfiguration.referrerImage_I, img);
					  }
				  }
				  else {
					  //System.out.println("IconNodeRenderer.handle referrer: Type "+typename+" -> "+fullFilename+" (skip 1)");
					  hs.setCache(ModelConfiguration.skipReferrerIconFile_P, new Object());
				  }
			  }
			  catch(Exception e) {
				  hs.setCache(ModelConfiguration.skipReferrerIconFile_P, new Object());
				  e.printStackTrace();
				  
			  }
		  }
		  else {
			  //System.out.println("IconNodeRenderer.handle referrer: Type "+typename+" -> "+fullFilename+" (skip 2)");
		  }

		  ImageIcon imageicon = null;
		  try {
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  ths.setProblem(TableConfiguration.typeIconTrouble_P, true);
			  //System.out.println("IconNodeRenderer.handle referrer: Type "+typename+" -> "+fullFilename+" (skip 3)");
			  return null;
		  }
		  Image img = imageicon.getImage();
		  Image ref = (Image) hs.getCache(ModelConfiguration.referrerImage_I);
		  if(ref != null) {
			  BufferedImage referrer = new BufferedImage(2*IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
			  Graphics g_referrer = referrer.createGraphics();
			  g_referrer.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
			  g_referrer.drawImage(ref, IconHelper.size, 0, IconHelper.size, IconHelper.size, null);
			  ImageIcon referrerIcon = new ImageIcon(referrer);
			  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.referrerIcon_I, referrerIcon);
			  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.referrerIcon_I, referrer);
		  }
		  else {
			  BufferedImage referrer = new BufferedImage(IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
			  Graphics g_referrer = referrer.createGraphics();
			  g_referrer.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
			  ImageIcon referrerIcon = new ImageIcon(referrer);
			  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.referrerIcon_I, referrerIcon);
			  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.referrerIcon_I, referrer);
		  }
		  //setIcon(ths.getImageIconBestEffort(DBTableHandlingSpecification.referrerIcon_I));
		  ImageIcon ii = IconHelper.getImageIconBestEffort(ths, TableConfiguration.referrerIcon_I);
		  return ii;
	}

	public Icon getNullPointerNodeImageIcon(int size, int usedImageBufferType) {
		  //String typename = npn.getName();
		  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" Entry");
		  //DBTableHandlingSpecification ths2 = npn.get
		  EmbeddedTableHandling env = eth;//rn.getEnvironment().getEmbeddedEnvironment();
		  TableConfiguration ths = env.ths;
		  //TableConfiguration ths = enpn.getEnvironment().getEmbeddedEnvironment().ths;
		  if(ths.getProblem(TableConfiguration.typeNullPointerIconTrouble_P)) { //typeNullPointerIconTrouble_P = "typenullpointericontrouble"
			  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" typenullpointericontrouble");
			  return null;
		  }
		  String iconfile = ths.getIconfile();
		  //ImageIcon icon = ths.getImageIconBestEffort(DBTableHandlingSpecification.nullPointerIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(ths, TableConfiguration.nullPointerIcon_I);
		  if(icon != null) {
			  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" icon");
			  return icon;
		  }
		  if(iconfile == null) {
			  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" iconfile");
			  return null;
		  }
		  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" & iconfile = "+iconfile);

		  ModelConfiguration hs = ths.getDBHandlingSpecification();
		  byte imageByteArray[] = ths.getIconfileBytes();//((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  //String fullFilename = hs.getFileSystemReferenceCatalog()+File.separator+iconfile;
		  
		  if(hs.getCache(ModelConfiguration.skipNullPointerIconFile_P) == null) {
			  try {
				  if(hs.getCache(ModelConfiguration.nullPointerIconFile_S) != null) {
					  String filename = (String) hs.getCache(ModelConfiguration.nullPointerIconFile_S);
					  byte imageByteArrayReferrer[] =  ((HierarchyLeaf) (hs.getFileSystemReferenceCatalog()).getChild(iconfile)).getByteArray();
					  //String fullReferrerFileName = hs.getFileSystemReferenceCatalog()+File.separator+filename;
					  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" -> "+fullReferrerFileName);
					  ImageIcon imageicon = new ImageIcon(imageByteArrayReferrer);
					  Image img = imageicon.getImage();
					  hs.setCache(ModelConfiguration.nullPointerImage_I, img);
				  }
				  else {
					  //System.out.println("IconNodeRenderer.handle nullpointer: Type "+typename+" -> "+fullFilename+" (skip 1)");
					  hs.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
				  }
			  }
			  catch(Exception e) {
				  hs.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
			  }
		  }
		  else {
			  //System.out.println("IconNodeRenderer.handle referrer: Type "+typename+" -> "+fullFilename+" (skip 2)");
		  }

		  ImageIcon imageicon = null;
		  try {
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  ths.setProblem(TableConfiguration.typeNullPointerIconTrouble_P, true);
			  return null;
		  }
		  Image img = imageicon.getImage();
		  Image ref = (Image) hs.getCache(ModelConfiguration.nullPointerImage_I);
		  if(ref != null) {
			  BufferedImage nullpointer = new BufferedImage(2*IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
			  Graphics g_nullpointer = nullpointer.createGraphics();
			  g_nullpointer.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
			  g_nullpointer.drawImage(ref, IconHelper.size, 0, IconHelper.size, IconHelper.size, null);
			  ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.nullPointerIcon_I, nullpointerIcon);
			  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.nullPointerIcon_I, nullpointer);
		  }
		  else {
			  BufferedImage nullpointer = new BufferedImage(IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
			  Graphics g_nullpointer = nullpointer.createGraphics();
			  g_nullpointer.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
			  ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.nullPointerIcon_I, nullpointerIcon);
			  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.nullPointerIcon_I, nullpointer);
		  }
		  //setIcon(ths.getImageIconBestEffort(DBTableHandlingSpecification.nullPointerIcon_I));
		  ImageIcon ii = IconHelper.getImageIconBestEffort(ths, TableConfiguration.nullPointerIcon_I);
		  return ii;	  
	}

	public Icon getRowNodeImageIcon(int size, int usedImageBufferType) {
		  //String typename = rn.getName();
		  EmbeddedTableHandling env = eth;//rn.getEnvironment().getEmbeddedEnvironment();
		  TableConfiguration ths = env.ths;
		  //DBTableHandlingSpecification ths = hs.getDBTableHandlingSpecification(typename);
		  if(ths.getProblem(TableConfiguration.typeIconTrouble_P)) {  //typeicontrouble
			  return null;
		  }
		  String iconfile = ths.getIconfile();
		  //ths.getImageIconBestEffort(DBTableHandlingSpecification.normalIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(ths, TableConfiguration.normalIcon_I);
		  if(icon != null) {
			  return icon;
		  }
		  if(iconfile == null) {
			  return null;
		  }
		  byte imageByteArray[] = ths.getIconfileBytes(); //((HierarchyLeaf) env.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  ImageIcon imageicon = null;
		  try {
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  ths.setProblem(TableConfiguration.typeIconTrouble_P, true); //typeicontrouble
			  return null;
		  }
		  Image img = imageicon.getImage();
		  BufferedImage straight = new BufferedImage(IconHelper.size, IconHelper.size, IconHelper.usedImageBufferType);
		  Graphics g_straight = straight.createGraphics();
		  g_straight.drawImage(img, 0, 0, IconHelper.size, IconHelper.size, null);
		  ImageIcon straightIcon = new ImageIcon(straight);
		  //ths.setImageIconAsImageIcon(DBTableHandlingSpecification.normalIcon_I, straightIcon); 
		  IconHelper.setImageIconAsImageIcon(ths, TableConfiguration.normalIcon_I, straight);
		  return straightIcon; // Något måste vara fel
	}

	public TextRenderer getDefaultRownodeTextRenderer() {
		return new DefaultRownodeTextRenderer(eth.ths);
	}

	public Map<String, Runnable> getButtons(DBPoolObject po) {
		Map<String, Runnable> out = new HashMap<String, Runnable>();
		if(eth.ths.getNoButtons() != 0) {
			for(int i = 0 ; i < eth.ths.getNoButtons() ; i++) {
				final ProgramBlock pb = eth.ths.getButtonProgram(i);
				final String buttonText = eth.ths.getButtonText(i);
				final Runnable r = new Runnable() {
					public void run() {
						try {
							cml.executeInNodeScope(po, pb, true);
						} catch (Exception e) {
							cml.announceRuntimeError("Execution for button "+buttonText+" failed.");
						}
					}
				};
				out.put(eth.ths.getButtonText(i), r);
			}
		}
		return out;
	}

	DBPoolObject insert(Hashtable<String, Object> poht, boolean persited) throws Exception {
		return cml.insertPoolObject(eth.ths, poht);
	}

	public boolean hasFileBrowserField(String name) {
		return eth.ths.hasFileBrowserField(name);
	}
	
}
