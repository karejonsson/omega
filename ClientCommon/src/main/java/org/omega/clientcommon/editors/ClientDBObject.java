package org.omega.clientcommon.editors;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.bsf.BSFException;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.clientcommon.hierarchy.ReferenceNode;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.renderers.DefaultReferrernodeTextRenderer;
import org.omega.configuration.renderers.DefaultTextRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.groovyintegration.pool.DBPoolObject;

public class ClientDBObject {
	
	private DBPoolObject po = null;
	private ClientTableLogic ctl = null;
	private ClientModelLogic cml = null;
	public ClientDBObject(DBPoolObject po, ClientTableLogic ctl) {
		this.po = po;
		this.ctl = ctl;
		cml = ctl.getClientModelLogic();
	}
	
	public String toString() {
		return "ClientDBObject: instance "+po.toString()+", table "+ctl.getTableName();
	}
	
	public void delete() throws Exception {
		cml.delete(this);
	}
	
	public boolean isNullObject() {
		return po == null;
	}
	
	public ClientTableLogic getClientTableLogic() {
		return ctl;
	}
	
	PrimaryKey getPrimaryKey() {
		return po.getPrimaryKey();
	}
	
	public void refresh() throws Exception {
		po.refresh();
	}
	
	public boolean sameObject(ClientDBObject cdbobj) {
		return this.po.getPrimaryKey().equals(cdbobj.po.getPrimaryKey());
	}
	
	public int getNoReferences() {
		return po.getNoForeignKeys();
	}
	
	public String getNameOfReference(int i) throws Exception {
		return po.getForeignKeyDeclaration(i).getConstraintname();
	}
	
	public ClientDBObject getReferredObject(String name) throws Exception {
		ForeignKey fk = po.getForeignKeyDownstreams(name);
		if(fk == null) {
			return null;
		}
		PrimaryKey pk = fk.assemblePrimaryKey();
		ClientDBObject cdbobj = cml.getDBObject(pk);
		return cdbobj;
	}
	 
	public int getNoReferringConstraints() {
		return ctl.getNoReferringConstraints();
	}
	
	public ReferenceNode getReferrerNode(int i) throws Exception {
		return new ReferenceNode(this, po.getEnvironment().ths.getReferringConstraint(i));
	}
	
	EmbeddedTableHandling getEnvironment() {
		return po.getEnvironment();
	}
	
	DBPoolObject getDBPoolObject() {
		return po;
	}
	
	public List<DataNode> getUpstreamPoolObjectsHavingPointer(ForeignKeyDeclaration fkd) throws Exception {
		List<DBPoolObject> raws = cml.getUpstreamsPoolObjectsHavingPointer(po, fkd);
		ClientTableLogic ctl = cml.getTableLogic(fkd.getReferringTablename());
		List<DataNode> out = new ArrayList<DataNode>();
		for(DBPoolObject po : raws) {
			out.add(new DataNode(new ClientDBObject(po, ctl)));
		}
		return out;
	}
	
	public String getTablesColumnName(int pos) {
		return po.getEnvironment().ths.getTableRenderer().getColumnName(pos);
	}
	
	public int getTablesNoColumns() {
		return po.getEnvironment().ths.getTableRenderer().getNoColumns();
	}
	
	public String renderForTreeReferrer(String foreignKeyDeclarationName) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer treeReferrerRenderer = po.getEnvironment().ths.getReferrerTreeTextRenderer(foreignKeyDeclarationName);
		if(treeReferrerRenderer == null) {
			treeReferrerRenderer = new DefaultReferrernodeTextRenderer(foreignKeyDeclarationName);
		}
		//System.out.println("ClientDBObject.renderForTreeReferrer class "+treeReferrerRenderer.getClass().getName());
		return cml.render(po, treeReferrerRenderer, true);
	}
	
	public String renderForTreeReference(String foreignKeyDeclarationName) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer treeReferrerRenderer = po.getEnvironment().ths.getReferenceTreeTextRenderer(foreignKeyDeclarationName);
		if(treeReferrerRenderer == null) {
			treeReferrerRenderer = ctl.getDefaultRownodeTextRenderer();
		}
		return cml.render(po, treeReferrerRenderer, true);
	}
	
	public String renderForDialogReference(String foreignKeyDeclarationName) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer treeReferrerRenderer = po.getEnvironment().ths.getReferenceDialogTextRenderer(foreignKeyDeclarationName);
		if(treeReferrerRenderer == null) {
			treeReferrerRenderer = ctl.getDefaultRownodeTextRenderer();
		}
		return cml.render(po, treeReferrerRenderer, true);
	}
	
	public String renderForInstanceTextRenderer() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer treeInstanceRenderer = po.getEnvironment().ths.getInstanceTextRenderer();
		if(treeInstanceRenderer == null) {
			treeInstanceRenderer = ctl.getDefaultRownodeTextRenderer();
		}
		return cml.render(po, treeInstanceRenderer, true);
	}
	
	public String renderForTableAtPosition(int pos) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer tablesPositionRenderer = po.getEnvironment().ths.getTableRenderer().getColumnRenderer(pos);
		if(tablesPositionRenderer == null) {
			CreateStatement cs = po.getEnvironment().ths.getCreateStatement();
			tablesPositionRenderer = new DefaultTextRenderer("Default script", cs, cs.getColumn(pos));
		}
		return cml.render(po, tablesPositionRenderer, true);
	}
	
	public InputStream downloadFromBlob_with_executeQuery(String colname) throws Exception {
		return po.downloadFromBlob_with_executeQuery(colname);
	}
	
	public void executeOnSomeUpdateHappened() {
		ctl.executeSilentOnUpdate(po, true);
	}

	public void uploadToBlob(String columnName, InputStream inputStream_in, byte[] uploadable, String inFilename) throws Exception {
		po.uploadToBlob(columnName, inputStream_in, uploadable, inFilename);
	}
	
	public List<EditorCommons> getEditorsForEditables(String implicitConstraintname, boolean forEditing) throws Exception {
		return ctl.getEditorsForEditables(implicitConstraintname, po, forEditing);
	}
	
	public void setReference(String constraintname, ClientDBObject referred) throws Exception {
		EmbeddedTableHandling eth = ctl.getEmbeddedTableHandling();
		ForeignKeyDeclaration fkd = eth.getForeignKeyDeclaration(constraintname);
		ForeignKey fk = ForeignKey.assembleForeignKey(referred.getPrimaryKey(), fkd);
		po.setForeignKey(fk);
	}

	public void executeOnInsert() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		ctl.executeOnInsert(po, false);
	}

	public void persist() throws Exception {
		po.write();
	}
	
	public void clear() {
		if(po != null) {
			po.removeAllListeners();
		}
	}
	
	public boolean performLobUploads(LobEditorCommons lte, int remaining) {
		return cml.performLobUploads(po, lte, ctl, remaining);
	}

	public List<ReferrerEditorCommons> getReferrerEditorsForEditables() throws Exception {
		return cml.getReferrerEditorsForEditables(po);
	}
	
	public List<ReferenceNode> getReferenceNodesForTree() throws Exception {
		return cml.getReferenceNodesForTree(po);
	}
	
	public String getMetaDescription() {
		return ctl.getMetaDescription(po);
	}

	public Map<String, Runnable> getButtons() {
		return ctl.getButtons(po);
	}
	
	public Object getValue(String key) {
		return po.get(key);
	}
	
	public boolean hasColumn(String key) {
		return po.hasColumn(key);
	}

	public void setValue(String key, Object value) throws Exception {
		po.set(key, value);
	}

}

