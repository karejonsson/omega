package org.omega.clientcommon.editors;

import java.awt.Component;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;
import java.util.List;

import org.apache.bsf.BSFException;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.values.LobValue;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.groovyintegration.pool.DBPoolObject;

import se.modlab.generics.util.FilesAndBytes;

public class CandidateClientDBObject {
	
	private DBPoolObject po = null;
	private ClientTableLogic ctl = null;
	private ClientModelLogic cml = null;
	private Hashtable<String, Object> poht = null;
	public CandidateClientDBObject(ClientTableLogic ctl) throws Exception {
		this.ctl = ctl;
		po = new DBPoolObject(null, ctl.getEmbeddedTableHandling());
		cml = ctl.getClientModelLogic();
		poht = po.getHashtable();
	}
	
	public void setReference(String constraintname, ClientDBObject referred) throws Exception {
		EmbeddedTableHandling eth = ctl.getEmbeddedTableHandling();
		ForeignKeyDeclaration fkd = eth.getForeignKeyDeclaration(constraintname);
		ForeignKey fk = ForeignKey.assembleForeignKey(referred.getPrimaryKey(), fkd);
		po.setForeignKey(fk);
	}
	
	private ClientDBObject created = null;
	
	public void persist() throws Exception {
		po = ctl.insert(poht, false);
		created = new ClientDBObject(po, ctl);
	}
	
	public ClientDBObject getCreatedClientDBObject() {
		return created;
	}
	
	public void executeOnSomeUpdateHappened() {
		ctl.executeSilentOnUpdate(po, false);
	}

	public void executeOnInsert() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		ctl.executeOnInsert(po, false);
	}

	public void clear() {
		if(po != null) {
			po.removeAllListeners();
		}
	}
	
	/*
	PrimaryKey getPrimaryKey() {
		return po.getPrimaryKey();
	}
	*/
	
	/*
	public boolean sameObject(ClientDBObject cdbobj) {
		return this.po.getPrimaryKey().equals(cdbobj.getPrimaryKey());
	}
	
	public int getNoReferences() {
		return po.getNoForeignKeys();
	}
	
	public String getNameOfReference(int i) throws Exception {
		return po.getForeignKeyDeclaration(i).getName();
	}
	
	public ClientDBObject getReferredObject(String name) throws Exception {
		ForeignKey fk = po.getForeignKey(name);
		PrimaryKey pk = fk.assemblePrimaryKey();
		ClientDBObject cdbobj = cml.getDBObject(pk);
		return cdbobj;
	}
	*/
	
	EmbeddedTableHandling getEnvironment() {
		return po.getEnvironment();
	}
	
	public void set(String key, Object value) {
		poht.put(key, value);
	}

	/*
	DBPoolObject getDBPoolObject() {
		return po;
	}
	
	public List<DataNode> getPoolObjectsHavingPointer(ForeignKeyDeclaration fkd) throws Exception {
		List<DBPoolObject> raws = cml.getPoolObjectsHavingPointer(po, fkd.getName());
		ClientTableLogic ctl = cml.getTableLogic(fkd.getTable());
		List<DataNode> out = new ArrayList<DataNode>();
		for(DBPoolObject po : raws) {
			out.add(new DataNode(new ClientDBObject(po, ctl)));
		}
		return out;
	}
	
	public String getTablesColumnName(int pos) {
		return po.getEnvironment().ths.getTableRenderer().getColumnName(pos);
	}
	
	public int getTablesNoColumns() {
		return po.getEnvironment().ths.getTableRenderer().getNoColumns();
	}
	
	public String renderForTreeReferrer(ForeignKeyDeclaration fkd) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer treeReferrerRenderer = null;
		if(treeReferrerRenderer == null) {
			treeReferrerRenderer = po.getEnvironment().ths.getReferrerTreeTextRenderer(fkd.getName());
			if(treeReferrerRenderer == null) {
				treeReferrerRenderer = new DefaultReferrernodeTextRenderer(fkd);
			}
		}
		return cml.render(po, treeReferrerRenderer, true);
	}
	
	public String renderForTableAtPosition(int pos) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		TextRenderer tablesPositionRenderer = null;
		if(tablesPositionRenderer == null) {
			tablesPositionRenderer = po.getEnvironment().ths.getTableRenderer().getColumnRenderer(pos);
			if(tablesPositionRenderer == null) {
				CreateStatement cs = po.getEnvironment().ths.getCreateStatement();
				tablesPositionRenderer = new DefaultTextRenderer("Default script", cs, cs.getColumn(pos));
			}
		}
		return cml.render(po, tablesPositionRenderer, true);
	}
	
	public InputStream downloadFromBlob_with_executeQuery(String colname) throws Exception {
		return po.downloadFromBlob_with_executeQuery(colname);
	}
	
	*/
	
	public void uploadToBlob(String columnName, InputStream inputStream_in, byte[] uploadable, String inFilename) throws Exception {
		po.uploadToBlob(columnName, inputStream_in, uploadable, inFilename);
	}
	
	public List<EditorCommons> getEditorsForEditables(String implicitConstraintname, boolean forEditing) throws Exception {
		return ctl.getEditorsForEditables(implicitConstraintname, po, forEditing);
	}
	
	public boolean performLobUploads(LobEditorCommons lte, int remaining) {
		return cml.performLobUploads(po, lte, ctl, remaining);
	}
		
	public List<ReferrerEditorCommons> getReferrerEditorsForEditables() throws Exception {
		return cml.getReferrerEditorsForEditables(po);
	}
	
}
