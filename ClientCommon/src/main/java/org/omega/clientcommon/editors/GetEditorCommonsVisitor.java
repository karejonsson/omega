package org.omega.clientcommon.editors;

import java.util.Date;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.TypeVisitor;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.struct.VarcharType;
import org.omega.connectivity.helpers.DBTypeManagement;
import org.omega.groovyintegration.pool.DBPoolObject;

import se.modlab.generics.exceptions.IntolerableException;

public class GetEditorCommonsVisitor implements TypeVisitor {

	public static EditorCommons getEditorCommons(DBPoolObject po, Column col, Type t, ClientTableLogic ctl) throws IntolerableException {
		
		GetEditorCommonsVisitor gtcv = new GetEditorCommonsVisitor(po, col, t, ctl);
		t.accept(gtcv);
		return gtcv.out;
	}
	
	private EditorCommons out = null;
	private DBPoolObject po = null;
	private Column col = null;
	private Type t = null;
	private ClientTableLogic ctl = null;
	
	private GetEditorCommonsVisitor(DBPoolObject po, Column col, Type t, ClientTableLogic ctl) {
		this.col = col;
		this.po = po;
		this.t = t;
		this.ctl = ctl;
	}

	@Override
	public void visit(DatetimeType dtt) { 
		out = new DatetimeEditorCommons(po, col.getName(), ctl);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((DatetimeEditorCommons) out).setValue((Date) DBTypeManagement.getDefault(col));
		}
		catch(Exception e) {
			ctl.log("Could not set datetime defasult value", e);
		}
	}

	@Override
	public void visit(DecimalType dt) {
		out = new DoubleEditorCommons(po, col.getName(), ctl);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((DoubleEditorCommons) out).setValue((Double) DBTypeManagement.getDefault(col));
		}
		catch(Exception e) {
			ctl.log("Could not set datetime default value", e);
		}
	}

	@Override
	public void visit(DoubleType dt) {
		out = new DoubleEditorCommons(po, col.getName(), ctl);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((DoubleEditorCommons) out).setValue((Double) value);
		}
		catch(Exception e) {
			ctl.log("Could not set datetime default value", e);
		}
	}

	@Override
	public void visit(IntType it) {
		out = new IntegerEditorCommons(po, col.getName(), ctl);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((IntegerEditorCommons) out).setValue((Integer) value);
		}
		catch(Exception e) {
			ctl.log("Could not set integer default value", e);
		}
	}

	@Override
	public void visit(TextType tt) {
		out = new TextlineEditorCommons(po, col.getName(), ctl, -1);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((TextlineEditorCommons) out).setValue((String) value);
		}
		catch(Exception e) {
			ctl.log("Could not set textual default value", e);
		}
	}

	@Override
	public void visit(TinytextType ttt) {
		out = new TextlineEditorCommons(po, col.getName(), ctl, -1);
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			Object value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
			((TextlineEditorCommons) out).setValue((String) value);
		}
		catch(Exception e) {
			ctl.log("Could not set datetime defasult value", e);
		}
	}

	@Override
	public void visit(UnmanageableType ut) {
		out = new UnmanageableEditorCommons(col.getName(), ut);
	}

	@Override
	public void visit(VarcharType vct) {
		Object value = null;
		try {
			Object defaultValue = DBTypeManagement.getDefault(col);
			value = po.get(col.getName());
			if(value == null && defaultValue != null) {
				value = defaultValue;
			}
		}
		catch(Exception e) {
			ctl.log("Could not set datetime default value", e);
			return;
		}
		if(vct.getLength() > 500) {
			out = new TextareaEditorCommons(po, col.getName(), ctl, vct.getLength());
			try {
				((TextareaEditorCommons) out).setValue((String) value);
			}
			catch(Exception e) {
				ctl.log("Could not set datetime default value", e);
			}
		}
		else {
			out = new TextlineEditorCommons(po, col.getName(), ctl, vct.getLength());
			try {
				((TextlineEditorCommons) out).setValue((String) value);
			}
			catch(Exception e) {
				ctl.log("Could not set datetime default value", e);
			}
		}
	}

	@Override
	public void visit(LobType lt) throws IntolerableException {
		out = new LobEditorCommons(col.getName(), po);
	}

}
