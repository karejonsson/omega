package org.omega.clientcommon.editors;

import java.lang.String;

import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.pool.DBPoolObjectListener;

public class TextlineEditorCommons extends EditorCommons {

	private ClientTableLogic env = null;
	private int maxlen = -1;
	public TextlineEditorCommons(DBPoolObject po, String name, ClientTableLogic env, int maxlen) {
		super(name, po);
		this.env = env;
		this.maxlen = maxlen;
	}
	
	public boolean isTextual() {
		return true;
	}
	
	public void setUpdaterOfVisibleField(UpdateVisibleField uvf) throws Exception {
		this.uvf = uvf;
		if(uvf == null) {
			objectEdited.removeAllListeners();
			return;
		}
		objectEdited.addListener(new DBPoolObjectListener() {
			@Override
			public void rowWasManipulated(DBPoolObject ro) {
				if(uvf.isEditable()) {
					return;
				}
				String value = (String) ro.get(getName());
				try {
					uvf.setText(value != null ? value : "");
					setValue(value);
				} catch (Exception e) {
					log("Error on updateing listened value "+value, e);
					e.printStackTrace();
				}
			}
		});
		uvf.setText(this.getValue());
	} 
	
	public void setValueUntyped(Object obj) throws Exception {
		if(obj == null) {
			setValue("");
			return;
		}
		if(obj instanceof String) {
			setValue((String) obj);
			return;
		}
		throw new Exception("String value editor cannot take value "+obj+" of type "+obj.getClass());
	}
 
	public void setValue(String value) throws Exception {
		//System.out.println("TextlineEditorCommons.setValue("+value+")"+((value == null) ? "#############################" : "----")+" "+name);
		/*
		if(value == null) {
			(new Throwable()).printStackTrace();
			//System.out.println("------------------------------------------------");
		}
		if(value.trim().length() == 0) {
			(new Throwable()).printStackTrace();
			//System.out.println("----------------ooooooooooooo-------------------");
		}
		*/
		objectEdited.set(name, value);
	}
	
	public String getValue() throws Exception {
		return (String) objectEdited.get(name);
	}
	
	public void logTrace(Exception e) {
		env.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		env.log(message, e);
	}
	
	public String errorStateDescription() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < CommonSQLAssembler.formats.length ; i++) {
			sb.append(CommonSQLAssembler.formats[i]+"\n");
		}
		return "not correct.\n\nMessage: "+getErrorMessage()+
				"\n\nAllowed formats are:\n"+sb.toString();
	}
	
	public int rowsUsed() {
		return 1;
	}
	
	public int getMaxlen() {
		return maxlen;
	}

	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}
	
}