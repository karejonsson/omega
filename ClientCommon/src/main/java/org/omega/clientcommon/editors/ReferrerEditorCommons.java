package org.omega.clientcommon.editors;

import java.awt.Window;
import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.Vector;

import org.apache.bsf.BSFException;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.connectivity.helpers.GeneralComparator;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;

import se.modlab.generics.util.Sort;

public class ReferrerEditorCommons extends EditorCommons {
	
	private ClientTableLogic referred = null;
	private ClientModelLogic cml = null;
	private ClientTableLogic referring = null;
	private ForeignKeyDeclaration fkd = null;
	private ClientDBObject cdbo = null;
	private ForeignKey fk;
	private DBPool pool;
	
	public ReferrerEditorCommons(ForeignKeyDeclaration fkd, DBPool pool, DBPoolObject referredObject, ClientTableLogic referred) throws Exception {
		super(fkd.getConstraintname(), referredObject);
		this.referred = referred;
		this.fkd = fkd;
		cml = referred.getClientModelLogic();
		referring = cml.getTableLogic(fkd.getReferringTablename());
		cdbo = new ClientDBObject(referredObject, referred);
		this.pool = pool;
		fk = referredObject.getForeignKeyUpstreams(fkd);
	}
	
	public ClientTableLogic getReferringClientTableLogic() {
		return referring;
	}
	
	public ClientTableLogic getReferredClientTableLogic() {
		return referred;
	}
	
	public String getReferenceName() {
		return fkd.getConstraintname();
	}
	
	private Vector<DBPoolObject> tableRowsOrder = null;
	
	private int sortcolumn = -1;
	private boolean sortInverted = false;

	public void columnMoved(int to) {
		sortInverted = to == sortcolumn ? !sortInverted : false;
		sortcolumn = to;
		rearrangeTableOrder();
	}
	
	public void rearrangeTableOrder() {
		//System.out.println("TableNode.rearrangeTableOrder col="+sortcolumn+", inv="+sortInverted);
		Vector<DBPoolObject> children = null;
		try {
			children = pool.getPoolObjectsHavingPointer(fk);
		}
		catch(Exception e) {
			tableRowsOrder = new Vector<DBPoolObject>();
			return;
		}
		//System.out.println("ReferrerGraphicalEditor.rearrangeTableOrder() fk = "+fk);
		if(tableRowsOrder == null) {
			tableRowsOrder = new Vector<DBPoolObject>();
		}
		
		if(tableRowsOrder.size() != children.size()) {
			//System.out.println("TableNode.rearrangeTableOrder initierar Vectorn");
			tableRowsOrder.removeAllElements();
			for(int i = 0 ; i < children.size() ; i++) {
				tableRowsOrder.addElement(children.elementAt(i));
			}
		}
		if(sortcolumn == -1) {
			return;
		}
		Comparator<Object> comp = new Comparator<Object>() {
			private GeneralComparator gp = new GeneralComparator();
			public int compare(Object arg0, Object arg1) {
				String s1 = getTableStringToPresent((DBPoolObject) arg0, sortcolumn);
				String s2 = getTableStringToPresent((DBPoolObject) arg1, sortcolumn);
				int out = gp.compare(s1, s2);
				if(sortInverted) {
					out = -out;
				}
				return out;
			}
		};
		Vector<DBPoolObject> tmp = new Vector<DBPoolObject>();
		for(int i = 0 ; i < tableRowsOrder.size(); i++) {
			//System.out.println("TableNode.rearrangeTableOrder sorterar Vectorn "+tmp.size());
			Sort.add(tmp, getNodeAtPlaceForTable(i), comp);
		}
		tableRowsOrder = tmp;
	}

	private DBPoolObject getNodeAtPlaceForTable(int row) {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
		}
		if(getRowCountForTable() == 0 && row == 0) {
			return null;
		}
		//return (RowNode) getChildAt(row);
		return tableRowsOrder.elementAt(row);
	}
	
	public ClientDBObject getClientDBObjectAtPlaceForTable(int row) {
		DBPoolObject out = getNodeAtPlaceForTable(row);
		if(out == null) {
			return null;
		}
		return new ClientDBObject(out, referred);
	}

	public int getRowCountForTable() {
		if(tableRowsOrder == null) {
			rearrangeTableOrder();
			return tableRowsOrder.size();
		}
		Vector<DBPoolObject> children = null;
		try {
			//System.out.println("ReferrerGraphicalEditor.getRowCountForTable() fk = "+fk);
			children = pool.getPoolObjectsHavingPointer(fk);
		}
		catch(Exception e) {
			//System.out.println("ReferrerGraphicalEditor");
			e.printStackTrace();
			tableRowsOrder = new Vector<DBPoolObject>();
		}
		//System.out.println("ReferrerGraphicalEditor.getRowCountForTable() table length = "+children.size());
		if(tableRowsOrder.size() != children.size()) {
			rearrangeTableOrder();
		}
		return tableRowsOrder.size();
		//return getChildCount();
	}
	
	public String getTableStringToPresent(int r, int c) {
		if(r >= getRowCountForTable()) {
			//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> <Out of bounds>");
			return "Out of bounds ("+r+" >= "+getRowCountForTable()+")";
		}
		return getTableStringToPresent(getNodeAtPlaceForTable(r), c);
	}
	
	private final static boolean persisted = true;

	private String getTableStringToPresent(DBPoolObject ro, int c) {
		TextRenderer se = referring.getColumnRenderer(c);
		String rendering = null;
		try {
			Object obj = referring.getClientModelLogic().render(ro, se, persisted);
			if(obj == null) {
				rendering = "<no rendering>";
			}
			else {
				rendering = obj.toString();
			}
		}
		catch(Exception ie) {
			referring.logTrace(ie);
			//System.out.println("2: Scope dump "+s+" when evaluating "+vl);
			rendering = "<no information>";
		}
		if(rendering == null) {
			rendering = "<not set>";
		}
		//System.out.println("TableNode.ResultSetTableModel.getValueAt("+r+","+c+") -> "+obj);
		return rendering;
	}
	
	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}
	
	@Override
	public Object getValue() throws Exception {
		throw new Exception("Must not call getValue for type Unmanaged");
	}
	
	public void setValueUntyped(Object obj) throws Exception {
		throw new Exception("Referrer editor cannot take value "+obj+" of type "+obj.getClass());
	}

	public String renderAsReferrerInTree() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		return cdbo.renderForTreeReferrer(fkd.getConstraintname());
	}

	public String renderAsReferenceInTree() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		return cdbo.renderForTreeReference(fkd.getConstraintname());
	}

	public String renderAsReferenceInDialog() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		return cdbo.renderForDialogReference(fkd.getConstraintname());
	}

	public String renderAsInstance() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		return cdbo.renderForInstanceTextRenderer();
	}

	public ClientDBObject getClientDBObject() {
		return cdbo;
	}

	public boolean referencesSame(ForeignKeyDeclaration fkdOther) {
		// TODO Auto-generated method stub
		return fkd.equals(fkdOther);
	}

}
