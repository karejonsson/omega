package org.omega.clientcommon.editors;

public interface EditorCommonsVisitor {

	public void visit(IntegerEditorCommons integerEditorCommons) throws Exception;

	public void visit(DoubleEditorCommons doubleEditorCommons) throws Exception;

	public void visit(DatetimeEditorCommons datetimeEditorCommons) throws Exception;

	public void visit(LobEditorCommons lobEditorCommons) throws Exception;

	public void visit(PointerEditorCommons pointerEditorCommons) throws Exception;

	public void visit(ReferrerEditorCommons referrerEditorCommons) throws Exception;
	
	public void visit(TextareaEditorCommons textareaEditorCommons) throws Exception;

	public void visit(TextlineEditorCommons textlineEditorCommons) throws Exception;

	public void visit(UnmanageableEditorCommons unmanageableEditorCommons) throws Exception;
	
}
