package org.omega.clientcommon.editors;

import org.omega.configuration.values.LobValue;
import org.omega.groovyintegration.pool.DBPoolObject;

import se.modlab.generics.util.FilesAndBytes;

public class LobEditorCommons extends EditorCommons {

	public static String getFilenameFromMeta(byte[] meta) {
		byte[] filename_b = FilesAndBytes.getFilenameFromMeta(meta);
		String filename_s = new String(filename_b);
		int idx_foreslash = filename_s.indexOf("/");
		int idx_backslash = filename_s.indexOf("\\");
		int idx = Math.max(idx_foreslash, idx_backslash);
		String out = new String(filename_s.getBytes(), idx+1, filename_s.length());
		return out;
	}
	
	private boolean checkedWithDatabase = false;
	public boolean isCheckedWithDatabase() {
		return checkedWithDatabase;
	}
	
	public void setCheckedWithDatabase(boolean state) {
		checkedWithDatabase = state;
	}

	public LobValue getValue() {
		return value;
	}

	public void setValueUntyped(Object obj) throws Exception {
		if(obj instanceof LobValue) {
			setValue((LobValue) obj);
			return;
		}
		throw new Exception("Lob value editor cannot take value "+obj+" of type "+obj.getClass());
	}
	
	public void setValue(LobValue value) {
		this.value = value;
	}

	private LobValue value = null;

	public LobEditorCommons(String name, DBPoolObject po) {
		super(name, po);
	}
	
	public Object getCachedValue() {
		return objectEdited.get(name);
	}

	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

	public void setLobValue(String name, LobValue lv) throws Exception {
		objectEdited.set(name, lv);
	}

}
