package org.omega.clientcommon.editors;

import java.lang.Integer;

import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.pool.DBPoolObjectListener;

public class IntegerEditorCommons extends EditorCommons {

	private ClientTableLogic env = null;
	public IntegerEditorCommons(DBPoolObject objectEdited, String name, ClientTableLogic env) {
		super(name, objectEdited);
		this.env = env;
	}
	
	public void setUpdaterOfVisibleField(UpdateVisibleField uvf) throws Exception {
		this.uvf = uvf;
		if(uvf == null) {
			objectEdited.removeAllListeners();
			return;
		}
		objectEdited.addListener(new DBPoolObjectListener() {
			@Override
			public void rowWasManipulated(DBPoolObject ro) {
				if(uvf.isEditable()) {
					return;
				}
				Integer value = (Integer) ro.get(getName());
				try {
					uvf.setText(value != null ? getStringFromInteger(value) : "");
					setValue(value);
				} catch (Exception e) {
					log("Error on updateing listened value "+value, e);
					e.printStackTrace();
				}
			}
		});
		uvf.setText(this.getValue() == null ? "" : this.getValue().toString());
	}
	
	public void setValueUntyped(Object obj) throws Exception {
		if(obj instanceof Integer) {
			setValue(obj);
		}
		if(obj instanceof Long) {
			setValue(obj);
		}
		throw new Exception("Integer value editor cannot take value "+obj+" of type "+obj.getClass());
	}
	
	public void setValue(Object value) throws Exception {
		if(!(value instanceof Integer) && (!(value instanceof Long))) {
			throw new Exception("Integer field set to type "+value.getClass());
		}
		objectEdited.set(name, value);
	}
	
	public Object getValue() throws Exception {
		return (Object) objectEdited.get(name);
	}
	
	public void logTrace(Exception e) {
		env.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		env.log(message, e);
	}
	
	public String errorStateDescription() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < CommonSQLAssembler.formats.length ; i++) {
			sb.append(CommonSQLAssembler.formats[i]+"\n");
		}
		return "not correct.\n\nMessage: "+getErrorMessage()+
				"\n\nAllowed formats are:\n"+sb.toString();
	}
	
	public Integer getIntegerFromString(String s) {
		return Integer.parseInt(s);
	}
		
	public String getStringFromInteger(Integer i) throws Exception {
		return (i == null) ? "" : i.toString();
	}
		
	public String getStringFromLong(Long l) throws Exception {
		return (l == null) ? "" : l.toString();
	}
		
	public String getStringFromObject(Object o) throws Exception {
		return (o == null) ? "" : o.toString();
	}
		
	public int rowsUsed() {
		return 1;
	}
	
	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

}