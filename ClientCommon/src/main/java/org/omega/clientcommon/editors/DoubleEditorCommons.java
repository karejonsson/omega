package org.omega.clientcommon.editors;

import java.lang.Double;

import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.pool.DBPoolObjectListener;

public class DoubleEditorCommons extends EditorCommons {

	private ClientTableLogic env = null;
	public DoubleEditorCommons(DBPoolObject objectEdited, String name, ClientTableLogic env) {
		super(name, objectEdited);
		this.env = env;
	}
	
	public void setUpdaterOfVisibleField(UpdateVisibleField uvf) throws Exception {
		this.uvf = uvf;
		if(uvf == null) {
			objectEdited.removeAllListeners();
			return;
		}
		objectEdited.addListener(new DBPoolObjectListener() {
			@Override
			public void rowWasManipulated(DBPoolObject ro) {
				if(uvf.isEditable()) {
					return;
				}
				Double value = (Double) ro.get(getName());
				try {
					uvf.setText(value != null ? getStringFromDouble(value) : "");
					setValue(value);
				} catch (Exception e) {
					log("Error on updateing listened value "+value, e);
					e.printStackTrace();
				}
			}
		});
		uvf.setText(this.getValue() != null ? this.getValue().toString() : "");
	}
	
	public void setValueUntyped(Object obj) throws Exception {
		if(obj instanceof Double) {
			setValue((Double) obj);
		}
		throw new Exception("Double value editor cannot take value "+obj+" of type "+obj.getClass());
	}
	
	public void setValue(Double value) throws Exception {
		objectEdited.set(name, value);
	}
	
	public Double getValue() throws Exception {
		return (Double) objectEdited.get(name);
	}
	
	public void logTrace(Exception e) {
		env.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		env.log(message, e);
	}
	
	public String errorStateDescription() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < CommonSQLAssembler.formats.length ; i++) {
			sb.append(CommonSQLAssembler.formats[i]+"\n");
		}
		return "not correct.\n\nMessage: "+getErrorMessage()+
				"\n\nAllowed formats are:\n"+sb.toString();
	}
	
	public Double getDoubleFromString(String s) {
		return Double.parseDouble(s.replace(",", "."));
	}
		
	public String getStringFromDouble(Double d) throws Exception {
		return (d == null) ? "" : d.toString();
	}
		
	public int rowsUsed() {
		return 1;
	}

	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

}