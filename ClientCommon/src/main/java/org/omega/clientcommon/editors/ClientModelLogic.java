package org.omega.clientcommon.editors;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.bsf.BSFException;
import org.omega.clientcommon.helpers.IconHelper;
import org.omega.clientcommon.hierarchy.DataNode;
import org.omega.clientcommon.hierarchy.MetaNode;
import org.omega.clientcommon.hierarchy.ReferenceNode;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.ProgramBlock;
import org.omega.configuration.values.LobValue;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.scope.ScriptingScope;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;

import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.util.FilesAndBytes;

public class ClientModelLogic {
	
	private ScriptingScopeFactory ssf = null;
	private DBPool pool = null;
	private EmbeddedModelHandling emh = null;
	private SystemProgrammingResourceAccess sysobject = null;
	private Object mainViewObject = null;
	
	public ClientModelLogic(ScriptingScopeFactory ssf) {
		this(ssf, null);
	}
	
	public String showPoolState() {
		return pool.toString();
	}
	
	public void announceRuntimeError(String message) {
		sysobject.announceRuntimeError(message);
	}
	
	public ClientModelLogic(ScriptingScopeFactory ssf, Object mainViewObject) {
		this.ssf = ssf;
		sysobject = ssf.getSysObject();
		pool = ssf.getDBPool();
		emh = pool.getDetails();
		this.mainViewObject = mainViewObject;
		Collection<TableConfiguration> tcs = emh.getTablehandlersLoopable();
		for(TableConfiguration tc : tcs) {
			getTableLogic(tc.getTableName());
		}
	}
	
	public List<DataNode> getAll(String tablename) throws Exception {
		Vector<DBPoolObject> children = pool.getPoolObjects(emh.getDBTableHandlingSpecificationOnTableName(tablename));
		List<DataNode> out = new ArrayList<DataNode>();
		ClientTableLogic ctl = getTableLogic(tablename);
		for(DBPoolObject po : children) {
			out.add(new DataNode(new ClientDBObject(po, ctl)));
		}
		return out;
	}
	
	int getTableLength(EmbeddedTableHandling eth) throws Exception {
		return pool.getTableLength(eth.ths);
	}
	
	public int getTableLength(String tablename) throws Exception {
		return pool.getTableLength(emh.getDBTableHandlingSpecificationOnTableName(tablename));
	}
	
	void delete(ClientDBObject cdbo) throws Exception {
		pool.removePoolObject(cdbo.getDBPoolObject());
	}
	
	public String getPackagename() {
		return ssf.getPackagename();
	}
	
	public String getAPI(String tablename) {
		return ssf.getAPI(tablename);
	}
	
	public Object getMainViewObject() {
		return mainViewObject;
	}
	
	public void setMainViewObject(Object mainViewObject) {
		this.mainViewObject = mainViewObject;
	}
	
	public ClientModelLogic(EmbeddedModelHandling emh, SystemProgrammingResourceAccess sysobject) throws Exception {
		this(new ScriptingScopeFactory(emh, "omega", sysobject), null);
	}
	
	public ClientModelLogic(EmbeddedModelHandling emh, SystemProgrammingResourceAccess sysobject, Object mainViewObject) throws Exception {
		this(new ScriptingScopeFactory(emh, "omega", sysobject), mainViewObject);
	}
	
	private Hashtable<String, ClientTableLogic> lookupClientTableLogic = new Hashtable<String, ClientTableLogic>();
	
	public List<ClientTableLogic> getClientTableLogicLoopable() {
		List<ClientTableLogic> out = new ArrayList<ClientTableLogic>();
		for(String key : lookupClientTableLogic.keySet()) {
			ClientTableLogic ctl = getTableLogic(key);
			if(!out.contains(ctl)) {
				out.add(ctl);
			}
		}
		return out;
	}
	
	public ClientTableLogic getTableLogic(String name) {
		//System.out.println("ClientModelLogic.getTableLogic("+name+")");
		ClientTableLogic out = lookupClientTableLogic.get(name);
		if(out != null) {
			//System.out.println("ClientModelLogic.getTableLogic("+name+") - 1");
			return out;
		}
		TableConfiguration tc = emh.getDBTableHandlingSpecificationOnTableName(name);
		if(tc == null) {
			tc = emh.getDBTableHandlingSpecificationOnObjectName(name);
			if(tc == null) {
				//System.out.println("ClientModelLogic.getTableLogic("+name+") - 2");
				return null;
			}
		}
		String tablename = tc.getTableName();
		String objectname = tc.getObjectName();
		EmbeddedTableHandling eth = emh.getEnvironment(tc);
		out = new ClientTableLogic(eth, this);
		lookupClientTableLogic.put(tablename, out);
		lookupClientTableLogic.put(objectname, out);
		return out;
	}
	
	public Object getObject(String label) {
		return emh.getCacheObject(label);
	}
	
	public void setObject(String label, Object obj) {
		emh.setCacheObject(label, obj);
	}
	
	public SQLIntermediateLogger getSQLIntermediate() {
		return emh.getSQLIntermediate();
	}
	
	public SQLMemoryLogger getSQLMemoryLog() {
		return emh.getSQLMemoryLog();
	}	
	
	public String getFrametitle() {
		return emh.getFrametitle();
	}

	public Enumeration<String> getTableNames() {
		return emh.getTableNames();
	}
	
	public boolean inHideTableList(String name) {
		return emh.inHideTableList(name);
	}
	
	public String getVersion() {
		return emh.getVersion();
	}

	public void logText(String str) {
		emh.logText(str);
	}
	
	public void log(String str, Exception e) {
		emh.log(str, e);
	}
	
	public void logTrace(Exception e) {
		emh.logTrace(e);
	}

	public Connection getConnection() {
		return emh.getConnection();
	}
	
	public byte[] getFilesBytearray(String iconfile) {
		return ((HierarchyLeaf) emh.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
	}
	
	String render(DBPoolObject po, TextRenderer tr, boolean persisted) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		if(po == null) {
			return "<Not set>";
		}
		ScriptingScope s = ssf.getNodeScope(po, persisted);//.getScope("ReferrerNode scope", pool, po);
		Object o = s.eval(tr);
		return o.toString();
	}
	
	String render(TextRenderer tr) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		ScriptingScope s = ssf.getNodelessScope();//.getScope("ReferrerNode scope", pool, po);
		Object o = s.eval(tr);
		return o.toString();
	}
	
	ClientDBObject getDBObject(PrimaryKey pk) throws Exception {
		DBPoolObject po = pool.getPoolObject(pk);
		if(po == null) {
			return null;
		}
		return new ClientDBObject(pool.getPoolObject(pk), getTableLogic(pk.getTablename()));
	}
	
	List<DBPoolObject> getUpstreamsPoolObjectsHavingPointer(DBPoolObject po, String constraintname) throws Exception {
		EmbeddedTableHandling eth = po.getEnvironment();
		TableConfiguration tc = eth.ths;
		ForeignKeyDeclaration fkd = tc.getReferringConstraint(constraintname);
		return getUpstreamsPoolObjectsHavingPointer(po, fkd);
	}

	List<DBPoolObject> getUpstreamsPoolObjectsHavingPointer(DBPoolObject po, ForeignKeyDeclaration fkd) throws Exception {
		PrimaryKey pk = po.getPrimaryKey();
		//System.out.println("ClientModelLogic.getUpstreamsPoolObjectsHavingPointer pk = "+pk+", constraintname = "+constraintname+" fkd = "+fkd);
		ForeignKey fk = ForeignKey.assembleForeignKey(pk, fkd);
		return pool.getPoolObjectsHavingPointer(fk);
	}

	public MetaNode getMetaNode(String tablename) {
		return new MetaNode(this.getTableLogic(tablename));
		//return new MetaNode(emh.getModelConfiguration(), emh.getDBTableHandlingSpecificationOnTableName(tablename), emh.getRecreator());
	}

	void executeInNodeScope(DBPoolObject po, ProgramBlock pb, boolean persisted) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, BSFException {
		ScriptingScope ss = ssf.getNodeScope(po, persisted);
		ss.exec(pb);
	}

	public PointerEditorCommons getPointerEditorCommons(String name, DBPoolObject po) throws Exception {
		ClientTableLogic ctl = getTableLogic(po.getEnvironment().getTableName());
		PointerEditorCommons out = new PointerEditorCommons(name, pool, po, ctl);
		return out;
	}
	
	public void performPrimaryKeyMissingTablesElimination() {
		emh.performPrimaryKeyMissingTablesElimination();
	}
	
	public void close() throws IntolerableException, SQLException {
		emh.close();
	}

	public boolean inSearchHideTableList(String name) {
		return emh.inHideTableList(name);
	}

	public Vector<ClientDBObject> searchPoolObjects(ClientTableLogic clientTableLogic, Hashtable<String, Object> ht) throws Exception {
		Vector<DBPoolObject> pos = pool.searchPoolObjects(clientTableLogic.getEmbeddedTableHandling().ths, ht);
		Vector<ClientDBObject> out = new Vector<ClientDBObject>();
		for(DBPoolObject po : pos) {
			out.add(new ClientDBObject(po, clientTableLogic));
		}
		return null;
	}
	
	boolean performLobUploads(DBPoolObject po, LobEditorCommons lte, ClientTableLogic ctl, int remaining) {
		//System.out.println("ClientModelLogic.performLobUploads lte "+lte.name+", ctl "+ctl.getTableName()+", remaining "+remaining+", po "+po);
		String name = lte.getName();
		String nametranslation = ctl.translate(name);
		if(lte.isNulled()) {
			try {
				po.uploadToBlob(name, null, null, null);
				po.set(name, lte.getValue());
			}
			catch(Exception e) {
				if(!sysobject.infoChoiceDialog_YES_NO_Cancel(
						"Upload error", 
						"Field "+nametranslation+" failed to be set to null."+
						((remaining == 0) ? "\nContinue uploads?" : ""))) {
					return false;
				}
				return true;
			}
			//System.out.println("BlobActionHelper.performLobUploads lte "+name+" is written to null.");
		}
		else {
			File f = null;
			String filenames = null;
			try {
				LobValue lv = (LobValue) lte.getValue();
				filenames = new String(lv.getFilename());
				f = new File(filenames);
				if(!f.exists()) {
					if(!sysobject.infoChoiceDialog_YES_NO_Cancel(
							"Upload error", 
							"File "+filenames+" could not be uploaded to field "+filenames+"."+
							"\nThe file does not exist!"+
							((remaining == 0) ? "\nContinue uploads?" : ""))) {
						return false;
					}
					return false;
				}

			}
			catch(Exception e) {
				if(!sysobject.infoChoiceDialog_YES_NO_Cancel(
						"Upload error", 
						"File "+filenames+" could not be uploaded to field "+filenames+"."+
						"\nInternal error occurred!"+
						((remaining == 0) ? "\nContinue uploads?" : ""))) {
					return false;
				}
			}
			try {
				byte[] uploadable = FilesAndBytes.byteArrayFromFile(f);
				String inFilename = FilesAndBytes.getLastSectionOfFilename(f.getAbsolutePath());
				// When the sysobject has some mean to show progress, get the following back.
				//InputStream inputStream_in = new BufferedInputStream(
		        //              new ProgressMonitorInputStream(null, "Uploading "+inFilename, new ByteArrayInputStream(uploadable)));
				ByteArrayInputStream inputStream_in = new ByteArrayInputStream(uploadable);
				po.uploadToBlob(name, inputStream_in, uploadable, inFilename);
				po.set(name, lte.getValue());
			}
			catch(Exception e) {
				if(!sysobject.infoChoiceDialog_YES_NO_Cancel(
						"Upload error", 
						"Field "+nametranslation+" failed to be set to null."+ 
						((remaining == 0) ? "\nContinue uploads?" : ""))) {
					return false;
				}
				return true;
			}
			//System.out.println("NewRowDialoig.performPostInsertLobUploads lob "+name+" is written to not null.");
		}
		return true;
	}

	List<ReferrerEditorCommons> getReferrerEditorsForEditables(DBPoolObject referredObject) throws Exception {
		List<ReferrerEditorCommons> out = new ArrayList<ReferrerEditorCommons>();
		
		EmbeddedTableHandling eth = referredObject.getEnvironment();

		for(int i = 0 ; i < eth.ths.getNoReferringConstraints() ; i++) {
			ForeignKeyDeclaration fkd = eth.ths.getReferringConstraint(i);
			String tablename = fkd.getReferringTablename();
			ClientTableLogic newEnv = getTableLogic(tablename);//new Environment(env, _ths);
			ForeignKey fk = new ForeignKey(fkd);
			for(int j = 0 ; j < fkd.getNoReferredTableMembers() ; j++) {
				Object obj = referredObject.get(fkd.getReferredTableMember(j));
				if(obj == null) {
					String mess = "Member "+fkd.getReferredTableMember(j)+" was not present as keyed value in table "+eth.getTableName();
					throw new Exception (mess);
				}
				fk.set(fkd.getReferringMember(j), obj);
			}
			
			ReferrerEditorCommons rec = new ReferrerEditorCommons(fkd, pool, referredObject, newEnv);
			out.add(rec);
		}

		return out;
	}
	
	List<ReferenceNode> getReferenceNodesForTree(DBPoolObject referredObject) throws Exception {
		List<ReferenceNode> out = new ArrayList<ReferenceNode>();
		
		EmbeddedTableHandling eth = referredObject.getEnvironment();

		for(int i = 0 ; i < eth.ths.getNoReferringConstraints() ; i++) {
			ForeignKeyDeclaration fkd = eth.ths.getReferringConstraint(i);
			/*
			PrimaryKey pk = referredObject.getPrimaryKeyFromForeignKeyName(fkd.getName());
			if(!pk.hasCompleteSetOfValues()) {
				continue;
			}
			DBPoolObject otherPo = pool.getPoolObject(pk);
			ClientTableLogic otherCTL = this.getTableLogic(pk.getTablename());
			ClientDBObject referrred = new ClientDBObject(otherPo, otherCTL);
			*/
			//ReferenceNode rn = new ReferenceNode(referrred, fkd);
			ClientTableLogic otherCTL = this.getTableLogic(eth.getTableName());
			ClientDBObject referredCDBO = new ClientDBObject(referredObject, otherCTL);
			ReferenceNode rn = new ReferenceNode(referredCDBO, fkd);
			//System.out.println("ClientModelLogic.getReferenceNodesForTree "+rn.toStringDebug());
			out.add(rn);
		}

		return out;
	}

	public Icon getTopNodeImageIcon(int size, int usedImageBufferType) {
		  //System.out.println("IconNodeRenderer.handle(TopNode)");
		  //String typename = tn.getName();
		  //NativeClientModelHandling details = tn.getDetails();
		  ModelConfiguration hs = emh.getModelConfiguration();//details.getModelConfiguration();
		  //DBTableHandlingSpecification ths = hs.getDBTableHandlingSpecification(typename);
		  if(hs.getProblem(TableConfiguration.typeIconTrouble_P)) {
			  //System.out.println("IconNodeRenderer.handle(TopNode) - 1");
			  return null;
		  }
		  //ImageIcon icon = hs.getImageIconBestEffort(DBTableHandlingSpecification.normalIcon_I);
		  ImageIcon icon = IconHelper.getImageIconBestEffort(hs, TableConfiguration.normalIcon_I);
		  if(icon != null) {
			  //System.out.println("IconNodeRenderer.handle(TopNode) - 2");
			  return icon;
		  }
		  String iconfile = hs.getIconfile();
		  if(iconfile == null) {
			  //System.out.println("IconNodeRenderer.handle(TopNode) - 3");
			  return null;
		  }
		  byte imageByteArray[] = hs.getIconfileBytes();//((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		  //String fullFilename = hs.getFileSystemReferenceCatalog()+File.separator+iconfile;
		  ImageIcon imageicon = null;
		  try {
			  //System.out.println("IconNodeRenderer.handle(TopNode) - 4");
			  imageicon = new ImageIcon(imageByteArray);
		  }
		  catch(Exception e) {
			  //System.out.println("IconNodeRenderer.handle(TopNode) - 5");
			  hs.setProblem(TableConfiguration.typeIconTrouble_P, true);
			  return null;
		  }
		  //System.out.println("IconNodeRenderer.handle(TopNode) - 6");
		  Image img = imageicon.getImage();
		  BufferedImage straight = new BufferedImage(size, size, usedImageBufferType);
		  Graphics g_straight = straight.createGraphics();
		  g_straight.drawImage(img, 0, 0, size, size, null);
		  ImageIcon straightIcon = new ImageIcon(straight);
		  //hs.setImageIconAsImageIcon(DBTableHandlingSpecification.normalIcon_I, straightIcon);
		  IconHelper.setImageIconAsImageIcon(hs, TableConfiguration.normalIcon_I, straight);
		  return straightIcon;
	}

	public DBPoolObject insertPoolObject(TableConfiguration ths, Hashtable<String, Object> poht) throws Exception {
		return pool.insertPoolObject(ths, poht);
	}

}
