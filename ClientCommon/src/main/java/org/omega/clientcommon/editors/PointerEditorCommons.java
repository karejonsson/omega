package org.omega.clientcommon.editors;

import javax.swing.ImageIcon;

import org.omega.clientcommon.helpers.IconHelper;
import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.renderers.DefaultRownodeTextRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.groovyintegration.pool.DBPool;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.pool.DBPoolObjectListener;

public class PointerEditorCommons extends EditorCommons {
	
	private static final boolean persisted = true;

	private ClientTableLogic referringTable = null;
	private DBPool pool = null;
	private String tableNamePointingFrom = null;
	private ForeignKeyDeclaration fkd = null;
	private TextRenderer tr = null;
	private ImageIconByteArrayHolder nullIcon = null;
	private ImageIconByteArrayHolder tableIcon = null;
	private ForeignKey currentFk = null;

	public PointerEditorCommons(String foreignKeyName, DBPool pool, DBPoolObject objectEdited, ClientTableLogic referringTable) throws Exception {
		super(foreignKeyName, objectEdited);
		this.referringTable = referringTable;
		this.pool = pool;
		fkd = objectEdited.getForeignKeyDeclarationFromName(foreignKeyName);
		tableNamePointingFrom = fkd.getReferringTablename();
		ForeignKey fk = ForeignKey.assembleForeignKey(objectEdited.getPrimaryKey(), fkd);
		if(fk.hasCompleteSetOfValues()) {
			currentFk = fk;
		}
		tr = referringTable.getReferenceDialogTextRenderer(foreignKeyName);//ths.getReferenceDialogTextRenderer(fk.getName());
		if(tr == null) {
			String referredTable = fkd.getReferencedTablename();
			TableConfiguration ths = referringTable.getClientModelLogic().getTableLogic(referredTable).getEmbeddedTableHandling().ths;
			tr = ths.getInstanceTextRenderer();
			if(tr == null) {
				tr = new DefaultRownodeTextRenderer(ths);
			}
		}
		tableIcon = referringTable.getImageIconBestEffort(fkd.getReferencedTablename(), TableConfiguration.normalIcon_I);
		//nullIcon = getNullIcon();//env.hs.getDBTableHandlingSpecification(fk.getTablenamePointingTo()).getImageIcon(DBTableHandlingSpecification.nullPointerIcon_I);
	}
	
	public void setUpdaterOfVisibleField(UpdateVisibleField uvf) {
		this.uvf = uvf;
		if(uvf == null) {
			objectEdited.removeAllListeners();
			return;
		}
		objectEdited.addListener(new DBPoolObjectListener() {
			@Override
			public void rowWasManipulated(DBPoolObject ro) {
				if(uvf.isEditable()) {
					return;
				}
				DBPoolObject po = null;
				try {
					ForeignKey fk = objectEdited.getForeignKeyDownstreams(getName());
					po = pool.getPoolObject(fk.assemblePrimaryKey());
					String rendering = referringTable.getClientModelLogic().render(po, tr, persisted);
					uvf.setText(rendering);
					setValue(po);
				} catch (Exception e) {
					log("Error on updateing listened value "+po, e);
					e.printStackTrace();
				}
			}
		});
	}
	
	public void init() {
		try {
			setText();
		}
		catch(Exception e) {
			logText("Unable to set pointers value initial to edit.");
		}
	}
	
	public void setPointersReferredInstance(ClientDBObject rn) throws Exception {
		//System.out.println("PointerGraphicalEditor.setPointersReferredInstance(rn) got "+rn);
		if(rn == null) {
			//System.out.println("PointerGraphicalEditor got null which only should when an editor sets it to null");
			//po = null;
			currentFk.clear(); 
			objectEdited.setForeignKey(currentFk);
			setText();//lab.setText(pointerisnull);
			return;
		}
		//po = rn.getRowObject();
		setPointersReferredInstance(rn.getPrimaryKey());
	}

	void setPointersReferredInstance(PrimaryKey pk) {
		//System.out.println("PointerGraphicalEditor.setPointersReferredInstance() got "+po);
		try {
			currentFk = ForeignKey.assembleForeignKey(pk, fkd);
			//System.out.println("PointerGraphicalEditor.setPointersReferredInstance fk="+fk+" Not cleared");
		}
		catch(Exception e) {
			currentFk.clear();
		}
		setText();
	}
	
	public static final String pointernotset = "Pointer not set";

	public void setText() {
		//System.out.println("PoiinterGraphicalEditor.setText - fk="+fk);
		if(currentFk == null) {
			uvf.setText(pointernotset);
			return;
		}
		/*
		If the own table exists is a matter for anaysis in the constructor.
		if(!env.getClientModelLogic().hasTable(tableNamePointingFrom)) {
			env.logText("Pointer editor Set text has ths == null");
		}
		*/
		try {
			//(new Throwable()).printStackTrace();
			String text = "("+pointernotset+")";
			
			DBPoolObject objectToRender = getObjectPointed();

			if(objectToRender == null) {
				uvf.setText(text);
			}
			else {
				//ScriptingScopeFactory osf = env.getDetails().getScopeFactory();
				//ScriptingScope s = osf.getNodeScope(objectEdited, persisted);//.getScope("ReferrerNode scope", pool, po);
				//Object o = s.eval(tr);
				String rendering = referringTable.getClientModelLogic().render(objectToRender, tr, persisted);
				uvf.setText(rendering);//tr.render(s));
			}
		} 
		catch(Exception e) {
			uvf.setText("<no information>");
		}
        return;		
	}

	private DBPoolObject getObjectPointed() throws Exception {
		if(currentFk == null) {
			return null;
		}
		if(!currentFk.hasCompleteSetOfValues()) {
			return null;
		}
		PrimaryKey pk = currentFk.assemblePrimaryKey();
		DBPoolObject out = pool.getPoolObject(pk);
		return out;
	}

	public String toString() {
		return "Pointer editor "+((currentFk != null) ? currentFk.toString() : "No foreign key");
	}
	
	public boolean hasCompleteSetOfValues() throws Exception {
		return objectEdited.getForeignKeyDownstreams(name).hasCompleteSetOfValues();
	}
	
	public String getNameOfTablePointedAt() {
		return fkd.getReferencedTablename();
	}
	
	public ClientTableLogic getClientTableLogic() {
		return referringTable;
	}
	
	public void setNullIcon(ImageIconByteArrayHolder nullIcon) {
		this.nullIcon = nullIcon;
	}
	
	public ImageIconByteArrayHolder getNullIcon() {
		return nullIcon;
	}
	
	public void setTableIcon(ImageIconByteArrayHolder tableIcon) {
		this.tableIcon = tableIcon;
	}
	
	public ImageIconByteArrayHolder getTableIcon() {
		return tableIcon;
	}
	
	public ImageIconByteArrayHolder getNullIconByteArrayHolder() {
		if(referringTable.getProblem(tableNamePointingFrom, TableConfiguration.typeNullPointerIconTrouble_P)) {
			return null;
		} 
		String iconfile = referringTable.getIconFile(fkd.getReferencedTablename());
		if(iconfile == null) {
			return null;
		}
		ImageIconByteArrayHolder icon = referringTable.getImageIconBestEffort(fkd.getReferencedTablename(), TableConfiguration.nullPointerIcon_I);//env.hs.getDBTableHandlingSpecification(fk.getTablenamePointingTo()).getImageIcon(DBTableHandlingSpecification.nullPointerIcon_I);
		if(icon != null) {
			return icon;
		}
		byte imageByteArray[] = null;
		try {
			imageByteArray = referringTable.getClientModelLogic().getFilesBytearray(iconfile);
		}
		catch(Exception e) {
			referringTable.logText("No icon file for null reference for table "+referringTable.getTableName());
			return null;
		}
		if(imageByteArray == null) {
			return null;
		}
		return new ImageIconByteArrayHolder(imageByteArray);
		/*
		if(env.getCache(ModelConfiguration.skipNullPointerIconFile_P) == null) {
			try {
				if(env.getCache(ModelConfiguration.nullPointerIconFile_S) != null) {
					String filename = (String) env.getCache(ModelConfiguration.nullPointerIconFile_S);
					byte imageByteArrayReferrer[] = env.getClientModelLogic().getFilesBytearray(filename);
					ImageIcon imageicon = new ImageIcon(imageByteArrayReferrer);
					Image img = imageicon.getImage();
					env.setCache(ModelConfiguration.nullPointerImage_I, img);
				}
				else {
					env.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
				}
			}
			catch(Exception e) {
				env.setCache(ModelConfiguration.skipNullPointerIconFile_P, new Object());
			}
		}

		ImageIcon imageicon = null;
		try {
			imageicon = new ImageIcon(imageByteArray);
		}
		catch(Exception e) {
			env.setProblem(fkd.getTable(), TableConfiguration.typeNullPointerIconTrouble_P, true);
			return null;
		}
		Image img = imageicon.getImage();
		Image ref = (Image) env.getCache(ModelConfiguration.nullPointerImage_I);
		if(ref != null) {
			BufferedImage nullpointer = new BufferedImage(2*size, size, IconNodeRenderer.usedImageBufferType);
			Graphics g_nullpointer = nullpointer.createGraphics();
			g_nullpointer.drawImage(img, 0, 0, size, size, null);
			g_nullpointer.drawImage(ref, size, 0, size, size, null);
			ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			ImageIconByteArrayHolder iibah = IconHelper.getByteArray(nullpointer);
			env.setImageIconAsImageIcon(fkd.getTable(), TableConfiguration.nullPointerIcon_I, iibah);
		}
		else {
			BufferedImage nullpointer = new BufferedImage(size, size, IconNodeRenderer.usedImageBufferType);
			Graphics g_nullpointer = nullpointer.createGraphics();
			g_nullpointer.drawImage(img, 0, 0, size, size, null);
			ImageIcon nullpointerIcon = new ImageIcon(nullpointer);
			ImageIconByteArrayHolder iibah = IconHelper.getByteArray(nullpointer);
			env.setImageIconAsImageIcon(fkd.getTable(), TableConfiguration.normalIcon_I, iibah);
		}
		return (ImageIcon) env.getImageIconBestEffort(fkd.getTable(), TableConfiguration.nullPointerIcon_I);
		*/
	}

	
	/*
	public void setValue(Double value) throws Exception {
		objectEdited.set(name, value);
	}
	
	public Double getValue() throws Exception {
		return (Double) objectEdited.get(name);
	}
	*/
	
	/*
	public void setForeignKey(ForeignKey value) throws Exception {
		if(!value.getName().equals(name)) {
			throw new Exception("Name of key was "+value.getName()+" when the name "+name+" was expected");
		}
		objectEdited.setForeignKey(value);
	}
	
	public ForeignKey getForeignKey() throws Exception {
		return objectEdited.getForeignKey(name);
	}
	*/
	
	public void logTrace(Exception e) {
		referringTable.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		referringTable.log(message, e);
	}
	
	public void logText(String message) {
		referringTable.logText(message);
	}
	
	public String errorStateDescription() {
		return "not correct.\n\nMessage: "+getErrorMessage();
	}
	
	public int rowsUsed() {
		return 1;
	}
	
	public ClientDBObject getClientDBObject() {
		return new ClientDBObject(objectEdited, referringTable);
	}
	
	public void setValueUntyped(Object obj) throws Exception {
		if(obj instanceof ClientDBObject) {
			setValue((ClientDBObject) obj);
		}
		throw new Exception("Pointer value editor cannot take value "+obj+" of type "+obj.getClass());
	}
	
	public void setValue(ClientDBObject value) throws Exception {
		if(value == null) {
			currentFk = ForeignKey.assembleForeignKey((PrimaryKey)null, fkd);
			objectEdited.setForeignKey(currentFk);
			return;
		}
		PrimaryKey pk = value.getPrimaryKey();
		currentFk = ForeignKey.assembleForeignKey(pk, fkd);
		objectEdited.setForeignKey(currentFk);
	}
	
	void setValue(DBPoolObject value) throws Exception {
		if(value == null) {
			currentFk = ForeignKey.assembleForeignKey((PrimaryKey)null, fkd);
			objectEdited.setForeignKey(currentFk);
			return;
		}
		PrimaryKey pk = value.getPrimaryKey();
		currentFk = ForeignKey.assembleForeignKey(pk, fkd);
		objectEdited.setForeignKey(currentFk);
	}
	
	public ClientDBObject getValue() throws Exception {
		//PrimaryKey pk = ForeignKey..assemblePrimaryKey(objectEdited.getHashtable(), fkd.getPrimaryKeyDeclaration());
		ForeignKey fk = ForeignKey.assembleForeignKey(objectEdited.getHashtable(), fkd);
		//System.out.println("PointerEditorCommons.getValue() - ForeignKey fk = "+fk);
		PrimaryKey pk = fk.assemblePrimaryKey();
		//System.out.println("PointerEditorCommons.getValue() - PrimaryKey pk = "+pk);
		DBPoolObject out = pool.getPoolObject(pk);
		//System.out.println("PointerEditorCommons.getValue() - out = "+out);
		return new ClientDBObject(out, referringTable.getClientModelLogic().getTableLogic(fkd.getReferencedTablename()));
	}
	
	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

	/*
	private int referenceEnumeration = -1;

	public int getReferenceEnumeration() {
		return referenceEnumeration;
	}

	public void setReferenceEnumeration(int referenceEnumeration) {
		this.referenceEnumeration = referenceEnumeration;
	}
	*/
	
	public ImageIcon getIcon(final int size) throws Exception {
		ImageIconByteArrayHolder rawNullIconByteArray = getNullIconByteArrayHolder();//env.hs.getDBTableHandlingSpecification(fk.getTablenamePointingTo()).getImageIcon(DBTableHandlingSpecification.nullPointerIcon_I);
		//System.out.println("PointerEditorCommons.getIcon() "+rawNullIconByteArray);
		IconHelper.updateCacheWithNullPointerImage(referringTable);
		ImageIconByteArrayHolder fixedNullIconByteArray = IconHelper.createAndInsertImageIcon(referringTable, getNameOfTablePointedAt(), size, rawNullIconByteArray != null ? rawNullIconByteArray.getBytes() : null);
		setNullIcon(fixedNullIconByteArray);
		ImageIconByteArrayHolder iibac = null;
		if(hasCompleteSetOfValues()) {
			iibac = getTableIcon();
		}
		else {
			iibac = getNullIcon();
		}
		if(iibac == null) {
			return null;
		}
		return new ImageIcon(iibac.getBytes());
	}
	
}
