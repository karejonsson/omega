package org.omega.clientcommon.editors;

import java.util.Date;

import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.groovyintegration.pool.DBPoolObject;
import org.omega.groovyintegration.pool.DBPoolObjectListener;

public class DatetimeEditorCommons extends EditorCommons {

	private ClientTableLogic env = null;
	public DatetimeEditorCommons(DBPoolObject objectEdited, String name, ClientTableLogic env) {
		super(name, objectEdited);
		this.env = env;
	}
	
	public void setUpdaterOfVisibleField(UpdateVisibleField uvf) throws Exception {
		this.uvf = uvf;
		if(uvf == null) {
			objectEdited.removeAllListeners();
			return;
		}
		objectEdited.addListener(new DBPoolObjectListener() {
			@Override
			public void rowWasManipulated(DBPoolObject ro) {
				if(uvf.isEditable()) {
					return;
				}
				Date value = (Date) ro.get(getName());
				try {
					uvf.setText(value != null ? getStringFromDate(value) : "");
					setValue(value);
				} catch (Exception e) {
					log("Error on updateing listened value "+value, e);
					e.printStackTrace();
				}
			}
		});
		uvf.setText(getValue() != null ? getValue().toString() : "");
	}
	
	public void setValueUntyped(Object obj) throws Exception {
		if(obj instanceof Date) {
			setValue((Date) obj); 
		}
		throw new Exception("String value editor cannot take value "+obj+" of type "+obj.getClass());
	}

	public void setValue(Date value) throws Exception {
		objectEdited.set(name, value);
	}
	
	public Date getValue() throws Exception {
		return (Date) objectEdited.get(name);
	}
	
	public void logTrace(Exception e) {
		env.logTrace(e);
	}
	
	public void log(String message, Exception e) {
		env.log(message, e);
	}
	
	public String errorStateDescription() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < CommonSQLAssembler.formats.length ; i++) {
			sb.append(CommonSQLAssembler.formats[i]+"\n");
		}
		return "not correct.\n\nMessage: "+getErrorMessage()+
				"\n\nAllowed formats are:\n"+sb.toString();
	}
	
	public Date getDateFromString(String s) {
		return CommonSQLAssembler.getDateFromString(s);
	}
		
	public String getStringFromDate(Date d) throws Exception {
		return CommonSQLAssembler.getDate(d);
	}
		
	public int rowsUsed() {
		return 1;
	}
	
	public void accept(EditorCommonsVisitor ecv) throws Exception {
		ecv.visit(this);
	}

}