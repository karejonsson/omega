import java.awt.Frame;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.omega.connectivity.properties.InstallationProperties;
import org.omega.nativeclientadmin.fileview.FrameTracker;
import org.omega.nativeclientadmin.fileview.IdeMenuHandler;
import org.omega.nativeclientadmin.fileview.SimpleIdeFrame;
import org.omega.nativeclientadmin.menu.OmegaNativeMenu;

import se.modlab.generics.bshro.ifc.HierarchyObject;

public class OmegaClient {

	public static void main(String args[]) throws IOException {

		File fi = null;
		if(args.length == 1) {
			fi = new File(args[0]);
		}
		else {
			fi = new File(System.getProperty("user.dir"));
		}
		FrameTracker tracker = new FrameTracker();
		File above = fi.getParentFile();
		try {
			HierarchyObject.setReferenceFile(new File(above, "files"));
		}
		catch(Exception e) {
		}
	    Frame sif = new SimpleIdeFrame(
	    	      "Truly versatile modeling tool",
	    	      new IdeMenuHandler[] { 
					  new OmegaNativeMenu(), 
	    	        }, 
	    	      tracker,
	    	      new File(above, "files"));
	    File misc = new File(fi, ".."+File.separator+"misc");
	    File properties = new File(misc, "omeganative.properties");
	    InstallationProperties.pathToConfigFile_default = properties.getCanonicalPath();
	    Image image = ImageIO.read(new File(misc, "omega.png"));
	    sif.setIconImage(image);
		sif.setVisible(true);
	}

}
