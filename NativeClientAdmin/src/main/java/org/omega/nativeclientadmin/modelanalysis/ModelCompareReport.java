package org.omega.nativeclientadmin.modelanalysis;

import java.io.IOException;
import java.util.Vector;

import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;

public interface ModelCompareReport {
	
	public void modelsParseable(String reference, String compare) throws IOException;
	public void verifySameTables() throws IOException;
	public void missingTable(TableConfiguration ths) throws IOException;
	public void amountMissingTable() throws IOException;
	public void extraTable(TableConfiguration ths) throws IOException;
	public void amountExtraTable(Vector<String> commonTables) throws IOException;
	public void verifyingColumnsInEachCommonTable() throws IOException;
	public void missingColumn(TableConfiguration ths, Column col) throws IOException;
	public void amountMissingColumn() throws IOException;
	public void extraColumn(TableConfiguration ths, Column col) throws IOException;
	public void amountExtraColumn(TableConfiguration ths) throws IOException;
	public void verifyingColumnsTypesInEachCommonTable() throws IOException;
	public void columnTypeDeviation(TableConfiguration ths, int index, Column col) throws IOException;
	public void amountDeviatingColumns() throws IOException;
	public void verifyingPrimaryKeysInEachCommonTable() throws IOException;
	public void primaryKeyComparison(TableConfiguration ths, PrimaryKeyDeclaration pkd1, PrimaryKeyDeclaration pkd2) throws IOException;
	public void autoIncrementComparison(TableConfiguration ths, Column col1, Column col2) throws IOException;
	public void amountPrimaryKeys() throws IOException;
	public void verifyingForeignKeysInEachCommonTable() throws IOException;
	public void numberOfForeignKeyDeclartations(CreateStatement cs1, CreateStatement cs2) throws IOException;
	public void coherenceOfForeignKeyDeclartations(TableConfiguration ths1, ForeignKeyDeclaration fkd1, ForeignKeyDeclaration fkd2, int index) throws IOException;
	public void amountForeignKeyDeviations() throws IOException;
	public void done() throws IOException;
	
}
