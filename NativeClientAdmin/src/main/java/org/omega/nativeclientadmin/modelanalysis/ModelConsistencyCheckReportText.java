package org.omega.nativeclientadmin.modelanalysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;

public class ModelConsistencyCheckReportText implements ModelConsistencyCheckReport {

	private String filename = null;
	private FileWriter fw = null; // new FileWriter(report_tmp)
	private String cr = null;

	public ModelConsistencyCheckReportText(String filename, String carrigeReturn) {
		this.filename = filename;
		cr = carrigeReturn;
	}
	
	private String model = null;
	private ModelConfiguration hs = null;

	public void modelParseable(String model, ModelConfiguration hs) {
		this.model = model;
		this.hs = hs;
	}
	
	private Vector<String> missingPrimaryKeys = new Vector<String>();

	public void missingPrimaryKey(TableConfiguration ths) {
		missingPrimaryKeys.add(ths.getTableName());
	}

	private Vector<String> missingAutoIncrementColumn = new Vector<String>();

	public void missingAutoIncrementColumn(TableConfiguration ths) {
		missingAutoIncrementColumn.add(ths.getTableName());
	}
	
	private Vector<ForeignKeyDeclaration> missingTargetTableForForeignKeys = new Vector<ForeignKeyDeclaration>();

	public void missingTargetTableForForeignKey(ForeignKeyDeclaration fkd) {
		missingTargetTableForForeignKeys.add(fkd);		
	}

	private Vector<ForeignKeyDeclaration> missingSourceTableForForeignKeys = new Vector<ForeignKeyDeclaration>();

	public void missingTargetColumnsKeyForForeignKey(ForeignKeyDeclaration fkd) {
		missingSourceTableForForeignKeys.add(fkd);		
	}
	
	private static class HoldDeviatingColumnTypes {
		public CreateStatement csSource;
		public Column clSource;
		public CreateStatement csTarget;
		public Column clTarget;
		public ForeignKeyDeclaration fkd;
		public String message;
		public HoldDeviatingColumnTypes(CreateStatement csSource, Column clSource, CreateStatement csTarget, Column clTarget, ForeignKeyDeclaration fkd, String message) {
			this.csSource = csSource;
			this.clSource = clSource;
			this.csTarget = csTarget;
			this.clTarget = clTarget;
			this.fkd = fkd;
			this.message = message;
		}
	}

	private Vector<HoldDeviatingColumnTypes> deviatingColumnTypes = new Vector<HoldDeviatingColumnTypes>();

	public void deviatingColumnType(CreateStatement csSource, Column clSource, CreateStatement csTarget, Column clTarget, ForeignKeyDeclaration fkd, String message) {
		deviatingColumnTypes.add(new HoldDeviatingColumnTypes(csSource, clSource, csTarget, clTarget, fkd, message));
	}

	private static class HoldMissingColumn {
		public CreateStatement csSource;
		public String clSource;
		public ForeignKeyDeclaration fkd;
		public HoldMissingColumn(CreateStatement csSource, String clSource, ForeignKeyDeclaration fkd) {
			this.csSource = csSource;
			this.clSource = clSource;
			this.fkd = fkd;
		}
	}

	private Vector<HoldMissingColumn> missingForeignKeySourceColumns = new Vector<HoldMissingColumn>();

	public void missingForeignKeySourceColumn(CreateStatement csSource, String clSource, ForeignKeyDeclaration fkd) {
		missingForeignKeySourceColumns.add(new HoldMissingColumn(csSource, clSource, fkd));
	}

	private Vector<HoldMissingColumn> missingForeignKeyTargetColumns = new Vector<HoldMissingColumn>();

	public void missingForeignKeyTargetColumn(CreateStatement csTarget, String clTarget, ForeignKeyDeclaration fkd) {
		missingForeignKeySourceColumns.add(new HoldMissingColumn(csTarget, clTarget, fkd));
	}
	
	private String makeNice(Vector<String> v) {
		if(v == null) {
			return "<NULL>";
		}
		if(v.size() == 0) {
			return "[]";
		}
		if(v.size() == 1) {
			return "["+v.elementAt(0)+"]";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("["+v.elementAt(0));
		for(int i = 1 ; i < v.size() ; i++) {
			sb.append(", "+v.elementAt(i));
		}
		sb.append("]");
		return sb.toString();
	}
	
	private void finishWrite() throws IOException {
		fw.flush();
		fw.close();
	}

	public void done() throws IOException {
		fw = new FileWriter(filename);
		fw.write("Comparison executed on "+(new java.util.Date())+cr);
		fw.write(cr);
		if(hs != null) {
			fw.write("Model "+model+" is parseable. There are "+hs.getNoTables()+" tables."+cr);
		}
		else {
			fw.write("It is not confirmed the model has been parsed."+cr);
			finishWrite();
			return;
		}
		fw.write(cr);
		if(missingPrimaryKeys.size() == 0) {
			fw.write("No tables miss a primary key."+cr);
		}
		else {
			fw.write(missingPrimaryKeys.size()+" tables miss a primary key. They are"+cr+makeNice(missingPrimaryKeys)+cr+cr);
		}

		fw.write(cr);

		if(missingAutoIncrementColumn.size() == 0) {
			fw.write("No tables miss an auto increment column."+cr);
		}
		else {
			fw.write(missingAutoIncrementColumn.size()+" tables miss miss an auto increment column. They are"+cr+makeNice(missingAutoIncrementColumn)+cr+cr);
		}

		fw.write(cr);

		if(missingTargetTableForForeignKeys.size() == 0) {
			fw.write("No foreign key declarations point to a nonexisting table."+cr);			
		}
		else {
			fw.write(missingTargetTableForForeignKeys.size()+" foreign key declarations point to a nonexisting table. They are"+cr);
			for(int i = 0 ; i < missingTargetTableForForeignKeys.size() ; i++) {
				fw.write(missingTargetTableForForeignKeys.elementAt(i).toString()+cr);
			}
		}

		fw.write(cr);

		if(missingSourceTableForForeignKeys.size() == 0) {
			fw.write("No foreign key declarations point from a nonexisting table."+cr);			
		}
		else {
			fw.write(missingSourceTableForForeignKeys.size()+" foreign key declarations point from a nonexisting table. They are"+cr);
			for(int i = 0 ; i < missingSourceTableForForeignKeys.size() ; i++) {
				fw.write(missingSourceTableForForeignKeys.elementAt(i).toString()+cr);
			}
		}

		fw.write(cr);

		if(deviatingColumnTypes.size() == 0) {
			fw.write("No foreign key declarations has members pointing to a deviating type."+cr);			
		}
		else {
			fw.write(deviatingColumnTypes.size()+" foreign key declarations point to a deviating type. They are"+cr);
			for(int i = 0 ; i < deviatingColumnTypes.size() ; i++) {
				fw.write(deviatingColumnTypes.elementAt(i).fkd+" signals "+deviatingColumnTypes.elementAt(i).message+cr);
			}
		}

		fw.write(cr);

		if(missingForeignKeySourceColumns.size() == 0) {
			fw.write("No foreign key declarations has source column that does not exist in the own table."+cr);			
		}
		else {
			fw.write(missingForeignKeySourceColumns.size()+" foreign key declarations has source column that does not exist in the own table. They are"+cr);
			for(int i = 0 ; i < missingForeignKeySourceColumns.size() ; i++) {
				fw.write(missingForeignKeySourceColumns.elementAt(i).fkd+" misses column  "+missingForeignKeySourceColumns.elementAt(i).clSource+cr);
			}
		}

		fw.write(cr);

		if(missingForeignKeyTargetColumns.size() == 0) {
			fw.write("No foreign key declarations has target column that does not exist in the other table."+cr);			
		}
		else {
			fw.write(missingForeignKeyTargetColumns.size()+" foreign key declarations has source column that does not exist in the target table. They are"+cr);
			for(int i = 0 ; i < missingForeignKeyTargetColumns.size() ; i++) {
				fw.write(missingForeignKeyTargetColumns.elementAt(i).fkd+" misses column  "+missingForeignKeyTargetColumns.elementAt(i).clSource+cr);
			}
		}
		
		fw.write(cr);
		finishWrite();
	}

}
