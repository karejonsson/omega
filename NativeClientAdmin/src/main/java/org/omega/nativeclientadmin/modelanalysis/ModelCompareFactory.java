package org.omega.nativeclientadmin.modelanalysis;

import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReport;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReportCorrectionalScript;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReportText;

public class ModelCompareFactory {
	
	public static String[] getAlgos() {
		return new String[] {
				"Exhaustive model compare, windows file", 
				"Exhaustive model compare, unix file", 
				"Correctional script for MSSQL, windows file", 
				"Correctional script for MSSQL, unix file", 
				"Correctional script for MySQL, windows file", 
				"Correctional script for MySQL, unix file",
				"Correctional script for Oracle, windows file", 
				"Correctional script for Oracle, unix file",
				"Correctional script for PostGreSQL, windows file", 
				"Correctional script for PostGre, unix file",
				"Correctional script for HSQLDB, windows file", 
				"Correctional script for HSQLDB, unix file"
				}; 
	}
	
	public static ModelCompareReport getReport(String algo, String filename) {
		if(algo.contains("Exhaustive model compare")) {
			if(algo.contains("windows")) {
				return new ModelCompareReportText(filename, "\r\n");
			}
			if(algo.contains("unix")) {
				return new ModelCompareReportText(filename, "\n");
			}
			return null;
		}
		if(algo.contains("Correctional script for")) {
			if(algo.contains("MSSQL")) {
				if(algo.contains("windows")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("MSSQL"), "\r\n", filename);
				}
				if(algo.contains("unix")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("MSSQL"), "\n", filename);
				}
			}
			if(algo.contains("MySQL")) {
				if(algo.contains("windows")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("MYSQL"), "\r\n", filename);
				}
				if(algo.contains("unix")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("MYSQL"), "\n", filename);
				}
			}
			if(algo.contains("HSQLDB")) {
				if(algo.contains("windows")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("HSQLDB"), "\r\n", filename);
				}
				if(algo.contains("unix")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("HSQLDB"), "\n", filename);
				}
			}
			if(algo.contains("PostGreSQL")) {
				if(algo.contains("windows")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("PostGreSQL"), "\r\n", filename);
				}
				if(algo.contains("unix")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("PostGreSQL"), "\n", filename);
				}
			}
			if(algo.contains("Oracle")) {
				if(algo.contains("windows")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("Oracle"), "\r\n", filename);
				}
				if(algo.contains("unix")) {
					return new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("Oracle"), "\n", filename);
				}
			}
		}
		return null;
	}

}
