package org.omega.nativeclientadmin.modelanalysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReport;

public class ModelCompareReportText implements ModelCompareReport {
	
	private String filename = null;
	private FileWriter fw = null; // new FileWriter(report_tmp)
	private String cr = null;
	
	public ModelCompareReportText(String _filename, String carrigeReturn) {
		filename = _filename;
		cr = carrigeReturn;
	}

	public void modelsParseable(String reference, String compare) throws IOException {
		fw = new FileWriter(filename);
		fw.write("Comparison executed on "+(new java.util.Date())+cr);
		fw.write("Reference model "+reference+cr);
		fw.write("Compared model "+compare+cr+cr);
	}
	
	public void verifySameTables() throws IOException {
		fw.write("Verifying the tables in the reference model are the same in compare model:"+cr+cr);
	}
	
	private int tableMissing = 0;
	
	public void missingTable(TableConfiguration ths) throws IOException {
		if(tableMissing == 0) {
			fw.write("Missing in compared: "+ths.getTableName());
		}
		else {
			fw.write(", "+ths.getTableName());
		}
		tableMissing++;
	}
	
	public void amountMissingTable() throws IOException {
		fw.write(cr+"Number of missing tables "+tableMissing+cr+cr);
	}

	private int tableExtra = 0;
	
	public void extraTable(TableConfiguration ths) throws IOException {
		if(tableExtra == 0) {
			fw.write("Extras in reference: "+ths.getTableName());
		}
		else {
			fw.write(", "+ths.getTableName());
		}
		tableExtra++;
	}
	
	public void amountExtraTable(Vector<String> commonTables) throws IOException {
		fw.write(cr+"Number of extra tables "+tableExtra+cr+cr);
		fw.write("Number of tables in common "+commonTables.size()+cr+cr);
		tableMissing = 0;
		tableExtra = 0;
	}

	public void verifyingColumnsInEachCommonTable() throws IOException {
		fw.write(cr+"Verifying the columns in each common table:"+cr+cr);
	}
	
	private int columnMissing = 0;
	
	public void missingColumn(TableConfiguration ths, Column col) throws IOException {
		if(columnMissing == 0) {
			fw.write("------- Missing columns in compared table "+ths.getTableName()+": "+col.getName());
		}
		else {
			fw.write(", "+col.getName());
		}
		columnMissing++;
	}
	
	public void amountMissingColumn() throws IOException {
		if(columnMissing != 0) {
			fw.write(" ("+columnMissing+")"+cr);
		}
	}

	private int columnExtra = 0;
	
	public void extraColumn(TableConfiguration ths, Column col) throws IOException {
		if(columnExtra == 0) {
			fw.write("------- Extra columns in reference table "+ths.getTableName()+": "+col.getName());
		}
		else {
			fw.write(", "+col.getName());
		}
		columnExtra++;
	}
	
	public void amountExtraColumn(TableConfiguration ths) throws IOException {
		if(columnExtra != 0) {
			fw.write(" ("+columnExtra+")"+cr);
		}
		int total = columnMissing+columnExtra;
		if(total == 0) {
			fw.write("Table "+ths.getTableName()+" was OK"+cr);
		}
		columnMissing = 0;
		columnExtra = 0;
	}

	public void verifyingColumnsTypesInEachCommonTable() throws IOException {
		fw.write(cr+"Verifying the columns types in each common table:"+cr+cr);
	}

	private int columnTypeDeviations = 0;

	public static String compareTypes(Column col1, Column col2) {
		Type t1 = col1.getType();
		Type t2 = col2.getType();
		if(t1 == null && t2 == null) {
			return "Null for type in both models";
		}
		if(t1 == null && t2 != null) {
			return "Null for type in reference model but "+t2.getTypename()+" in compare";
		}
		if(t1 != null && t2 == null) {
			return "Null for type in compare model but "+t1.getTypename()+" in referece";
		}
		if(t1 instanceof DatetimeType) {
			if(t2 instanceof DatetimeType) {
				return null;
			}
			return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
		}
		if(t1 instanceof UnmanageableType) {
			if(t2 instanceof UnmanageableType) {
				if(t1.getTypename().compareTo(t2.getTypename()) == 0) {
					return null;
				}
			}
			return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
		}
		if(t1 instanceof NumericType) {
			if(!(t2 instanceof NumericType)) {
				return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
			}
			NumericType t1n = (NumericType) t1;
			NumericType t2n = (NumericType) t2;
			if((t1n instanceof DecimalType || t1n instanceof DoubleType) &&
			   (t2n instanceof DecimalType || t2n instanceof DoubleType)) {
				return null;
			}
			if((t1n instanceof IntType) &&
			   (t1n instanceof IntType)) {
				IntType t1i = (IntType) t1;
				IntType t2i = (IntType) t2;
				if(t1i.getPrecision() != t2i.getPrecision()) {
					return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
				}
				else {
					return null;
				}
			}
			else {
				return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
			}
		}
		if(t1 instanceof TextualType) {
			if(!(t2 instanceof TextualType)) {
				return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
			}
			TextualType t1t = (TextualType) t1;
			TextualType t2t = (TextualType) t2;
			if(t1t.getTypename().compareTo(t2t.getTypename()) != 0) {
				return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
			}
			return null;
		}
		return "Type "+t1.getTypename()+" not compatible with "+t2.getTypename();
	}

	public void columnTypeDeviation(TableConfiguration ths1, int index, Column col2) throws IOException {
		Column col1 = ths1.getCreateStatement().getColumn(index);
		String message = compareTypes(col1, col2);
		if(message != null) {
			if(columnTypeDeviations == 0) {
				fw.write("Deviations in table "+ths1.getTableName()+cr);
				fw.write("Column "+col1.getName()+": "+message+cr);
			}
			else {
				fw.write("Column "+col1.getName()+": "+message+cr);
			}
			columnTypeDeviations++;
		}
	}

	public void amountDeviatingColumns() throws IOException {
		if(columnTypeDeviations != 0) {
			fw.write("Deviations: "+columnTypeDeviations+cr+cr);
		}
		columnTypeDeviations = 0;
	}

	public void verifyingPrimaryKeysInEachCommonTable() throws IOException {
		fw.write(cr+"Verifying the primary keys in each common table:"+cr+cr);
	}
	
	public static String primaryComparison(PrimaryKeyDeclaration pkd1, PrimaryKeyDeclaration pkd2) {
		if(pkd1 == null && pkd2 == null) {
			return null;
		}
		if(pkd1 == null || pkd2 == null) {
			return "Difference in primary key in declarations. In reference model it is "+pkd1+" and in compare model it is "+pkd2;
		}
		if(pkd1.equals(pkd2)) {
			return null;
		}
		return "Deviation in primary key declarations. Reference is "+pkd1+" and compare is "+pkd2;
	}

	public void primaryKeyComparison(TableConfiguration ths, PrimaryKeyDeclaration pkd1, PrimaryKeyDeclaration pkd2) throws IOException {
		String primaryCompared = primaryComparison(pkd1, pkd2);
		if(primaryCompared != null) {
			fw.write("Primary key deviation in table "+ths.getTableName()+": "+primaryCompared+cr);	
		}
	}
	
	public static String compareAutoIncrement(Column col1, Column col2) {
		if(col1.isAutoIncrement() == col2.isAutoIncrement()) {
			return null;
		}
		return "Difference in auto increment / identity aspect of column "+col1.getName()+". In reference model it "+((col1.isAutoIncrement() ? "is": "isn't")+" and in compare model it "+((col2.isAutoIncrement() ? "is": "isn't")));
	}
	
	private int primaryKeyTypeDeviations = 0;

	public void autoIncrementComparison(TableConfiguration ths, Column col1, Column col2) throws IOException {
		String message = compareAutoIncrement(col1, col2);
		if(message != null) {
			if(primaryKeyTypeDeviations == 0) {
				fw.write("Deviations in table "+ths.getTableName()+cr);
				fw.write("Column "+col1.getName()+": "+message+cr);
			}
			else {
				fw.write("Column "+col1.getName()+": "+message+cr);
			}
			primaryKeyTypeDeviations++;
		}
	}

	public void amountPrimaryKeys() throws IOException {
		if(primaryKeyTypeDeviations != 0) {
			fw.write("Deviations: "+primaryKeyTypeDeviations+cr+cr);
		}
		primaryKeyTypeDeviations = 0;
	}

	public void verifyingForeignKeysInEachCommonTable() throws IOException {
		fw.write(cr+"Verifying the foreign keys in each common table:"+cr+cr);
	}
	
	private int foreignKeyDeviations = 0;
	boolean tableSpecified = false;
	
	public void numberOfForeignKeyDeclartations(CreateStatement cs1, CreateStatement cs2) throws IOException {
		if(cs1.getNoForeignKeyDeclarations() != cs2.getNoForeignKeyDeclarations()) {
			fw.write("Problems of foreign keys in table "+cs1.getName()+cr);
			tableSpecified = true;
			foreignKeyDeviations++;
			fw.write("The number of foreign keys are different. In reference "+cs1.getNoForeignKeyDeclarations()+" and in compare "+cs2.getNoForeignKeyDeclarations()+"\n");	
		}
	}
	
	public void coherenceOfForeignKeyDeclartations(TableConfiguration ths1, ForeignKeyDeclaration fkd1, ForeignKeyDeclaration fkd2, int index) throws IOException {
		if(fkd2 == null) {
			if(!tableSpecified) {
				fw.write("Problems of foreign keys in table "+ths1.getTableName()+cr);
				tableSpecified = true;
			}
			foreignKeyDeviations++;
			fw.write("Foreign key "+fkd1.getConstraintname()+" of reference model has no equivalent to the name in compare model"+cr);	
			return;
		}
		if(!fkd1.equals(fkd2)) {
			if(!tableSpecified) {
				fw.write("Problems of foreign keys in table "+ths1.getTableName()+cr);
				tableSpecified = true;
			}
			foreignKeyDeviations++;
			fw.write("Foreign key "+fkd1.getConstraintname()+" on "+index+" place on reference model has scewy counterpart. "+fkd1+" != "+fkd2+cr);	
		}
	}
	
	public void amountForeignKeyDeviations() throws IOException {
		if(foreignKeyDeviations != 0) {
			fw.write("Deviations: "+foreignKeyDeviations+cr+cr);
		}
		foreignKeyDeviations = 0;
		tableSpecified = false;
	}
	
	public void done() throws IOException {
		fw.flush();
		fw.close();
	}


}
