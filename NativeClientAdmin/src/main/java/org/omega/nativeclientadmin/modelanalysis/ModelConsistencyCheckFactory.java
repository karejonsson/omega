package org.omega.nativeclientadmin.modelanalysis;

public class ModelConsistencyCheckFactory {
	
	public static String[] getAlgos() {
		return new String[] {
				"Exhaustive model consistency check, windows file", 
				"Exhaustive model consistency check, unix file"
				}; 
	}
	public static ModelConsistencyCheckReport getReport(String algo, String filename) {
		if(algo.contains("windows")) {
			return new ModelConsistencyCheckReportText(filename, "\r\n");
		}
		if(algo.contains("unix")) {
			return new ModelConsistencyCheckReportText(filename, "\n");
		}
		return null;
	}


}
