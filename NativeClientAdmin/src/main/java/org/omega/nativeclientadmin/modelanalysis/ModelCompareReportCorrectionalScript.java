package org.omega.nativeclientadmin.modelanalysis;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReport;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReportText;

public class ModelCompareReportCorrectionalScript implements ModelCompareReport {
	
	private String filename = null;
	private FileWriter fw = null; // new FileWriter(report_tmp)
	private String cr = null;
	private Recreator rec = null;

	public ModelCompareReportCorrectionalScript(Recreator _rec, String carrigeReturn, String _filename) {
		filename = _filename;
		cr = carrigeReturn;
		rec = _rec;
	}

	public void modelsParseable(String reference, String compare)
			throws IOException {
		fw = new FileWriter(filename);
	}

	public void verifySameTables() throws IOException {
	}

	public void missingTable(TableConfiguration ths)
			throws IOException {
		CreateStatement cs = ths.getCreateStatement();
		String scs[] = rec.create(cs);
		StringBuffer sb = new StringBuffer();
		if(scs.length == 1) {
			sb.append(scs[0]+"\n\n");
		}
		else {
			for(int i = 0 ; i < scs.length ; i++) {
				sb.append(""+i+":\n"+scs[i]+"\n");
			}
			sb.append("\n");
		}
		fw.write(sb.toString().replaceAll("\n", cr)+cr+cr);
	}

	public void amountMissingTable() throws IOException {
	}

	public void extraTable(TableConfiguration ths) throws IOException {
		CreateStatement cs = ths.getCreateStatement();
		String scss[] = rec.dropTable(cs);
		for(int i = 0 ; i < scss.length ; i++) {
			fw.write(scss[i].replaceAll("\n", cr)+cr+cr);
		}
	}

	public void amountExtraTable(Vector<String> commonTables)
			throws IOException {
	}

	public void verifyingColumnsInEachCommonTable() throws IOException {
	}

	public void missingColumn(TableConfiguration ths, Column col)
			throws IOException {
		CreateStatement cs = ths.getCreateStatement();
		String scs = rec.addColumn(cs, col);
		fw.write(scs.replaceAll("\n", cr)+cr+cr);
	}

	public void amountMissingColumn() throws IOException {
	}

	public void extraColumn(TableConfiguration ths, Column col)
			throws IOException {
		CreateStatement cs = ths.getCreateStatement();
		String scs = rec.deleteColumn(cs, col);
		fw.write(scs.replaceAll("\n", cr)+cr+cr);
	}

	public void amountExtraColumn(TableConfiguration ths)
			throws IOException {
	}

	public void verifyingColumnsTypesInEachCommonTable() throws IOException {
	}

	public void columnTypeDeviation(TableConfiguration ths,
			int index, Column col2) throws IOException {
		Column col1 = ths.getCreateStatement().getColumn(index);
		String message = ModelCompareReportText.compareTypes(col1, col2);
		if(message != null) {
			CreateStatement cs = ths.getCreateStatement();
			String scs = rec.changeColumn(cs, col2);
			fw.write(scs.replaceAll("\n", cr)+cr+cr);
		}
	}

	public void amountDeviatingColumns() throws IOException {
	}

	public void verifyingPrimaryKeysInEachCommonTable() throws IOException {
	}

	public void primaryKeyComparison(TableConfiguration ths,
			PrimaryKeyDeclaration pkd1, PrimaryKeyDeclaration pkd2)
			throws IOException {
	}

	public void autoIncrementComparison(TableConfiguration ths,
			Column col1, Column col2) throws IOException {
	}

	public void amountPrimaryKeys() throws IOException {
	}

	public void verifyingForeignKeysInEachCommonTable() throws IOException {
	}

	public void numberOfForeignKeyDeclartations(CreateStatement cs1,
			CreateStatement cs2) throws IOException {
	}

	public void coherenceOfForeignKeyDeclartations(
			TableConfiguration ths1, ForeignKeyDeclaration fkd1,
			ForeignKeyDeclaration fkd2, int index) throws IOException {
	}

	public void amountForeignKeyDeviations() throws IOException {
	}

	public void done() throws IOException {
		fw.flush();
		fw.close();
	}

}
