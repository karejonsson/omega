package org.omega.nativeclientadmin.modelanalysis;

import java.io.IOException;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;

public interface ModelConsistencyCheckReport {
	
	public void modelParseable(String reference, ModelConfiguration hs);
	public void missingAutoIncrementColumn(TableConfiguration ths);
	public void missingPrimaryKey(TableConfiguration ths);
	public void done() throws IOException;
	public void missingTargetTableForForeignKey(ForeignKeyDeclaration fkd);
	public void missingTargetColumnsKeyForForeignKey(ForeignKeyDeclaration fkd);
	public void missingForeignKeySourceColumn(CreateStatement csSource, String clSource, ForeignKeyDeclaration fkd);
	public void missingForeignKeyTargetColumn(CreateStatement csTarget, String clTarget, ForeignKeyDeclaration fkd);
	public void deviatingColumnType(CreateStatement csSource, Column clSource, CreateStatement csTarget, Column clTarget, ForeignKeyDeclaration fkd, String message);
}
