package org.omega.nativeclientadmin.menu;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectionspecification.struct.DBConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.helpers.ModelOrderCalculator;
import org.omega.connectivity.logging.RetainingSQLIntermediateLogger;
import org.omega.connectivity.logging.RetainingSQLMemoryLogger;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;
import org.omega.nativeclient.treeview.ExplorerWindow;
import org.omega.nativeclient.treeview.MetaExplorerWindow;
import org.omega.nativeclient.treeview.MetaTopNode;
import org.omega.nativeclientadmin.dialogs.ConnectDialog;
import org.omega.nativeclientadmin.integration.GenFile_yEd_tgf;

import se.modlab.generics.bshro.fs.DiskFilesystemHierarchy;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.files.OneFileNotepad;
import se.modlab.generics.gui.util.Clipboard;

public class IdeEditorsMenuCreator {

	public static JMenu[] getEditorsMenu(final OneFileNotepad ofn) { 
		final JMenu menu = new JMenu(OmegaNativeMenu.appname);
		final File f = ofn.getFile();
		final File p = f.getParentFile();
		final DiskFilesystemHierarchy dfsh = new DiskFilesystemHierarchy("root", null, p);
		final HierarchyLeaf hl = (HierarchyLeaf) dfsh.getChild(f.getName());
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaconfigpostfix)) {
			JMenuItem menuItem = new JMenuItem("Parse "+OmegaNativeMenu.appname+" config");
			menu.add(menuItem);
			menuItem.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							try {
								IdeFilesMenuCreator.parseGDBAPPConfig(menu, f, OmegaNativeMenu.editor_executed, new ByteArrayInputStream(ofn.getText().getBytes("UTF-8")));
								//universalTellUser.info(menu, "Parse successful");
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Parse "+OmegaNativeMenu.appname+" config - error",
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
			menuItem = new JMenuItem("Connect");
			menu.add(menuItem);
			menuItem.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							new ConnectDialog(OmegaNativeMenu.fullappname+" connect dialog", f.getAbsolutePath());
						}
					}
					);
			menuItem = new JMenuItem("Data meta view");
			menu.add(menuItem);
			menuItem.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ModelConfiguration hs = null;
								try {
									hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(
											dfsh, 
											hl.getName(), 
											new ByteArrayInputStream(ofn.getText().getBytes("UTF-8")), 
											true);
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
									return;
								}
								
								Recreator rec = RecreatorManager.getRecreator("MYSQL"); // MYSQL as default when it is only for meta.
								SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
								SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
								ConnectionOperations conn = null; // When it is only for meta.

								// ConnectionOperations conn, Recreator rec, ModelConfiguration hs, SQLMemoryLogger slw, SQLIntermediateLogger si, String frametitle
								EmbeddedModelHandling emh = new EmbeddedModelHandling(conn, rec, hs, slw, si, "Notepad started");
								ScriptingScopeFactory ssf = new ScriptingScopeFactory(emh, "omega", new SystemProgrammingResourceAccessImplementation());
								ClientModelLogic cml = new ClientModelLogic(ssf);
								MetaTopNode tn = MetaExplorerWindow.getTopNodeForMetaStructure(cml);//.getTopNodeForMetaStructure(hs, rec);
								MetaExplorerWindow mew = new MetaExplorerWindow(tn);
								mew.setVisible(true);
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Data meta view - error",
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
			menuItem = new JMenuItem("Generate yEd .tgf");
			menu.add(menuItem);
			menuItem.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ModelConfiguration hs = null;
								try {
									hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(dfsh, hl.getName(), new ByteArrayInputStream(ofn.getText().getBytes("UTF-8")), true);
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
									return;
								}
								try {
									GenFile_yEd_tgf.generate(f.getAbsoluteFile(), hs);
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Error on yEd - .tgf generation: "+ee.getMessage());
									return;
								}
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Generate yEd .tgf - error",
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
		}
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaapplicationpostfix)) {
			JMenuItem menuItem = new JMenuItem("Parse "+OmegaNativeMenu.omegaapplicationpostfix+" config");
			menu.add(menuItem);
			menuItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								IdeFilesMenuCreator.parseDBAPPConfig(menu, f.getParentFile().getAbsolutePath(), OmegaNativeMenu.editor_executed, new ByteArrayInputStream(ofn.getText().getBytes("UTF-8")));
							}
							catch(UnsupportedEncodingException ee) {
								UniversalTellUser.error(menu, "Encoding error: "+ee.getMessage());
								return;
							}
						}
					}
					);

			menuItem = new JMenuItem("Connect");
			menu.add(menuItem);
			menuItem.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try { 
								DBConnectionSpecification dbcs = IdeFilesMenuCreator.parseDBAPPConfig(menu, f);
								EmbeddedModelHandling emh = dbcs.getDetails();
								ExecuteScript.launcher.launch(emh, "omega", false);
							}
							catch(Exception ex) {
								UniversalTellUser.error(menu, ex.getMessage());
								ex.printStackTrace();
							}
						}
					}
					);
			addInsertTemplatesToOAPP(menu, ofn);
		}
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaexecutablepostfix)) {
			JMenuItem menuItem = new JMenuItem("Run");
			menu.add(menuItem);
			menuItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Runnable r = new Runnable() {
								public void run() {
									try {
										ExecuteScript.execute(f, menu);
										UniversalTellUser.info(menu, "Execution OK");
									}
									catch(UnsupportedEncodingException ee) {
										UniversalTellUser.error(menu, "Encoding error: "+ee.getMessage());
										return;
									}
									catch(Exception ee) {
										UniversalTellUser.error(menu, ee.getMessage());
										return;
									}
								}
							};
							Thread t = new Thread(r);
							t.start();
						}
					}
					);
			menuItem = new JMenuItem("Verify");
			menu.add(menuItem);
			menuItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							Runnable r = new Runnable() {
								public void run() {
									try {
										ExecuteScript.verify(f, menu);
									}
									catch(Exception ee) {
										UniversalTellUser.error(menu, ee.getMessage());
										return;
									}
								}
							};
							Thread t = new Thread(r);
							t.start();
						}
					}
					);
			menuItem = new JMenuItem("Parse "+OmegaNativeMenu.omegaexecutablepostfix+" config");
			menu.add(menuItem);
			menuItem.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								if(null != IdeFilesMenuCreator.parseOEXEConfig(menu, f.getParentFile().getAbsolutePath(), OmegaNativeMenu.editor_executed, new ByteArrayInputStream(ofn.getText().getBytes("UTF-8")))) {
									UniversalTellUser.info(menu, "Parse OK");
								};
							}
							catch(UnsupportedEncodingException ee) {
								UniversalTellUser.error(menu, "Encoding error: "+ee.getMessage());
								return;
							}
						}
					}
					);
			addInsertTemplatesToOEXE(menu, ofn);
		}
		return new JMenu[] { menu };
	}

	private static final String sshpart = 
			"sshtunnel { \n"+
					"  // Equivalent of the unix ssh-command\n"+
					"  // $ ssh utv@domain.se -L 2345:omega.com:5432 \n"+
					"  // The ssh { ... } passage shall be removed if not needed.\n"+
					"  login_user \"utv\"\n"+
					"  login_domain \"domain.se\"\n"+
					"  login_password \"pwd\"\n"+
					"  tunnel_lport \"2345\"\n"+
					"  tunnel_rhost \"omega.com\"\n"+
					"  tunnel_rport \"5432\"\n"+
					"  ssh_port \"22\"\n"+
					"}\n";

	private static final String flowtemplate = 
			"flow id { \n"+
					"  connection mem;\n"+
					"  script setup;\n"+
					"  explore mem;\n"+
					"}\n";

	private static final String scripttemplate = 
			"script Printout \"\"\" \n"+
					"  println \"Groovy code\"\n"+
					"\"\"\"\n";

	private static final String hsqldbmemorytemplate = 
			"hsqldbmemory (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"mymemdb\"\n"+
					"username \"SA\"\n"+
					"password \"\"\n"+
					"frametitle \"Some title\"\n"+
					")\n";

	private static final String hsqldbfiletemplate = 
			"hsqldbfile (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"file \"\"\n"+
					"username \"SA\"\n"+
					"password \"\"\n"+
					"frametitle \"Some title\"\n"+
					")\n";

	private static final String hsqldbremotetemplate =
			"hsqldbremote (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"SA\"\n"+
					"password \"\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"9001\"\n"+
					"protocol \"hsql\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";
	
	private static final String mysqltemplate =
			"mysql (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"\"\n"+
					"password \"\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"3306\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";
	private static final String mssqlservertemplate = 
			"mssqlserver (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"SA\"\n"+
					"password \"server\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"1433\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";

	private static final String oracletemplate = 
			"oracle (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"\"\n"+
					"password \"\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"1521\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";

	private static final String ibmdb2template = 
			"ibmdb2 (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"\"\n"+
					"password \"\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"1527\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";

	private static final String postgrestemplate = 
			"postgres (\n"+
					"configfile \""+OmegaNativeMenu.omegaconfigpostfix+"\"\n"+
					"dbname \"\"\n"+
					"username \"SA\"\n"+
					"password \"server\"\n"+
					"ipresolvable \"127.0.0.1\"\n"+
					"port \"1521\"\n"+
					"frametitle \"Some title\"\n"+
					sshpart+
					")\n";
	
	public final static String[][] connectiontemplates = { 
			{"Hsqldb memory", hsqldbmemorytemplate}, 
			{"Hsqldb file", hsqldbfiletemplate}, 
			{"Hsqldbremote", hsqldbremotetemplate}, 
			{"Mysql", mysqltemplate}, 
			{"Mssqlserver", mssqlservertemplate}, 
			{"Oracle", oracletemplate}, 
			{"Ibmdb2", ibmdb2template}, 
			{"Postgres", postgrestemplate} 
		};

	private static void addInsertTemplatesToOEXE(final JMenu menu, OneFileNotepad ofn) {
		final OneFileNotepad _ofn = ofn;
		JMenu templates = new JMenu("Insert templates");
		menu.add(templates);
		JMenuItem menuItem = new JMenuItem("Flow");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						_ofn.insertAtCaretPosition(flowtemplate);
					}
				});
		menuItem = new JMenuItem("Script");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						_ofn.insertAtCaretPosition(scripttemplate);
					}
				});
		menuItem = new JMenu("Connections");
		templates.add(menuItem);
		for(int i = 0 ; i < connectiontemplates.length ; i++) {
			final int i_ = i;
			JMenuItem connectionItem = new JMenuItem(connectiontemplates[i][0]);
			menuItem.add(connectionItem);
			connectionItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Clipboard.setClipboard("connection somename \"\"\"\n"+connectiontemplates[i_][1]+"\n\"\"\"\n");
					UniversalTellUser.info(menu, "In your clipboard");
				}
			});
		}

	}
	
	private static void addInsertTemplatesToOAPP(final JMenu menu, OneFileNotepad ofn) {
		final OneFileNotepad _ofn = ofn;
		JMenu templates = new JMenu("Insert templates");
		menu.add(templates);
		JMenuItem menuItem = new JMenuItem("HSQLDB memory");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(hsqldbmemorytemplate);
			}
		});
		menuItem = new JMenuItem("HSQLDB file");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(hsqldbfiletemplate);
			}
		});
		menuItem = new JMenuItem("HSQLDB remote");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(hsqldbremotetemplate);
			}
		});
		menuItem = new JMenuItem("MySQL");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(mysqltemplate);
			}
		});
		menuItem = new JMenuItem("MSSQL2008");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				_ofn.insertAtCaretPosition(mssqlservertemplate);
			}
		});
		menuItem = new JMenuItem("Oracle");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(oracletemplate);
			}
		});
		menuItem = new JMenuItem("IBM DB2");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(ibmdb2template);
			}
		});
		menuItem = new JMenuItem("PostgreSQL");
		templates.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_ofn.insertAtCaretPosition(postgrestemplate);
			}
		});
	}

}
