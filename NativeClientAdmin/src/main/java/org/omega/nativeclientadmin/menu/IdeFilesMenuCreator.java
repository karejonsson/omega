package org.omega.nativeclientadmin.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectionspecification.parse.OmegaConnectionSpecificationParser;
import org.omega.connectionspecification.struct.DBConnectionSpecification;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.helpers.ModelOrderCalculator;
import org.omega.connectivity.logging.RetainingSQLIntermediateLogger;
import org.omega.connectivity.logging.RetainingSQLMemoryLogger;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.executablescript.parse.OmegaExecutableScriptParser;
import org.omega.executablescript.struct.CombinedFileStruct;
import org.omega.executablescript.struct.FlowExploreStruct;
import org.omega.executablescript.struct.OneFileStruct;
import org.omega.executablescript.struct.ParseRecursively;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;
import org.omega.nativeclient.treeview.ExplorerWindow;
import org.omega.nativeclient.treeview.MetaExplorerWindow;
import org.omega.nativeclient.treeview.MetaTopNode;
import org.omega.nativeclientadmin.dialogs.ConnectDialog;
import org.omega.nativeclientadmin.dialogs.ModelConsistencyCheckTool;
import org.omega.nativeclientadmin.integration.GenFile_yEd_tgf;

import se.modlab.generics.bshro.fs.DiskFilesystemHierarchy;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.util.Clipboard;

public class IdeFilesMenuCreator {

	private static void parseGDBAPPConfig(JMenu menu, File f) {
		//String x = f.getParentFile().getAbsolutePath();
		try {
			parseGDBAPPConfig(menu, f, f.getName(), new FileInputStream(f));
		}
		catch(Exception e) {
			UniversalTellUser.error(menu, e.getMessage());
		}
	}

	public static DBConnectionSpecification parseDBAPPConfig(JMenu menu, File f) {
		String x = f.getParentFile().getAbsolutePath();
		try {
			return parseDBAPPConfig(menu, x, f.getName(), new FileInputStream(f));
		}
		catch(Exception e) {
			UniversalTellUser.error(menu, e.getMessage());
		}
		return null;
	}

	public static void parseGDBAPPConfig(JMenu menu, final File f, String filename, InputStream is) {
		try
		{
			File p = f.getParentFile();
			DiskFilesystemHierarchy dfsh = new DiskFilesystemHierarchy("root", null, p);
			//final HierarchyLeaf hl = (HierarchyLeaf) dfsh.getChild(f.getName());
			//motherCatalogue.setCatalogue(new File(fileSystemReferenceCatalog));
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
			ModelConfiguration hs = new ModelConfiguration();
			OmegaConfigParser.parseOmegaConfig(hs, isr, "File "+f.getCanonicalPath());
			//.parseGdbappConfigNativeEncoding(dfsh, filename, is, new GDBAPPScopeFactory(), null, new UserScriptingIntegrationGDBAPP());
			UniversalTellUser.info(menu, "Parse successful!");
		}
		catch(IntolerableException ie)
		{ 
			UniversalTellUser.general(menu, ie);
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
					new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotGDBAPP f - run - ex",
							_e);
			UniversalTellUser.general(menu, ie);
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
					new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotGDBAPP f - run - th",
							_e);
			UniversalTellUser.general(menu, ie);
		}
	}

	public static DBConnectionSpecification parseDBAPPConfig(JMenu menu, String fileSystemReferenceCatalog, String filename, InputStream is) {
		try
		{
			HierarchyObject.setReferenceFile(new File(fileSystemReferenceCatalog));

			ModelConfiguration hs = new ModelConfiguration();
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
			DBConnectionSpecification spec = OmegaConnectionSpecificationParser.parseDBConnectionSpecification(isr);
			return spec;
		}
		catch(IntolerableException ie)
		{ 
			UniversalTellUser.general(menu, ie);
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
					new UnclassedError("GdbAppMenu.parseDBAPPConfig",
							_e);
			UniversalTellUser.general(menu, ie);
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
					new UnclassedError("GdbAppMenu.parseDBAPPConfig",
							_e);
			UniversalTellUser.general(menu, ie);
		}
		return null;
	}
	
	public static OneFileStruct parseOEXEConfig(JMenu menu, String fileSystemReferenceCatalog, String filename, InputStream is) {
		try
		{
			HierarchyObject.setReferenceFile(new File(fileSystemReferenceCatalog));

			ModelConfiguration hs = new ModelConfiguration();
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
		
			OneFileStruct ofs = OmegaExecutableScriptParser.parseExecutableScript(isr, ExecuteScript.launcher);

			return ofs;
		}
		catch(IntolerableException ie)
		{ 
			UniversalTellUser.general(menu, ie);
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
					new UnclassedError("Unspecified intolerable error",
							_e);
			UniversalTellUser.general(menu, ie);
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
					new UnclassedError("Unspecified error",
							_e);
			UniversalTellUser.general(menu, ie);
		}
		return null;
	}

	public static void addConfigMenu(final JMenu menu, final File f) {
		JMenuItem menuItem = null;
		menuItem = new JMenuItem("Connect");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							new ConnectDialog(OmegaNativeMenu.appname+" connect dialog", f.getCanonicalPath());
							return;
						}
						catch(Exception ee) {}
						try {
							new ConnectDialog(OmegaNativeMenu.appname+" connect dialog", f.getAbsolutePath());
							return;
						}
						catch(Exception ee) {}
					}
				}
				);
		menuItem = new JMenuItem("Parse "+OmegaNativeMenu.appname+" config");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							parseGDBAPPConfig(menu, f);
						}
						catch(Exception _e) {
							IntolerableException ie = 
									new UnclassedError("Parse "+OmegaNativeMenu.appname+" config - error",
											_e);
							UniversalTellUser.general(menu, ie);
						}
					}
				}
				);

		menuItem = new JMenuItem("Consistency check");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							new ModelConsistencyCheckTool("Compare "+OmegaNativeMenu.fullappname+" models", f.getCanonicalPath());
							return;
						}
						catch(Exception ee) {}
						try {
							new ModelConsistencyCheckTool("Compare "+OmegaNativeMenu.fullappname+" models", f.getAbsolutePath());
							return;
						}
						catch(Exception ee) {}
					}
				}
				);

		JMenu menuItemBranch = new JMenu("Data meta view");
		menu.add(menuItemBranch);
		final String symbols[] = RecreatorManager.getNames();
		for(int i = 0 ; i < symbols.length ; i++) {
			final String symbol = symbols[i];
			JMenuItem misym = new JMenuItem(symbol);
			menuItemBranch.add(misym);
			misym.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ModelConfiguration hs = null;
								try {
									hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(f, true); 
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
									return;
								}
								
								Recreator rec = RecreatorManager.getRecreator(symbol);	
								SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
								SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
								ConnectionOperations conn = null; // When it is only for meta.

								// ConnectionOperations conn, Recreator rec, ModelConfiguration hs, SQLMemoryLogger slw, SQLIntermediateLogger si, String frametitle
								EmbeddedModelHandling emh = new EmbeddedModelHandling(conn, rec, hs, slw, si, "Notepad started");

								String packagename = "omega";
								SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
								ScriptingScopeFactory ssf = new ScriptingScopeFactory(emh, packagename, sysobject);
								ClientModelLogic cml = new ClientModelLogic(ssf);
								MetaTopNode tn = MetaExplorerWindow.getTopNodeForMetaStructure(cml);
								MetaExplorerWindow mew = new MetaExplorerWindow(tn);
								mew.setVisible(true);
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Data meta view, "+symbol,
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
		}
		menuItemBranch = new JMenu("Create drop tables script");
		menu.add(menuItemBranch);
		for(int i = 0 ; i < symbols.length ; i++) {
			final String symbol = symbols[i];
			JMenuItem misym = new JMenuItem(symbol);
			menuItemBranch.add(misym);
			misym.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ModelConfiguration hs = null;
								try {
									hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(f, true); 
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
									return;
								}
								Recreator rec = RecreatorManager.getRecreator(symbol);
								int topLevel = hs.getMaxLevelValue();
								StringBuffer sb = new StringBuffer();
								for(int j = topLevel ; j >= 0 ; j--) {
									Vector<TableConfiguration> thss = hs.getTablehandlersWithLevel(j);
									for(int jj = 0 ; jj < thss.size() ; jj++) {
										String droppers[] = rec.dropTable(thss.elementAt(jj).getCreateStatement());
										for(int jjj = 0 ; jjj < droppers.length ; jjj++) {
											sb.append(droppers[jjj]+" \n");
										}
										sb.append(" \n");
									}
								}
								String out = sb.toString();
								//System.out.println("SQL=\n"+out);
								Clipboard.setClipboard(out);
								UniversalTellUser.info(menu, "The script is in your clipboard");
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Create drop tables script - error, "+symbol,
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
		}
		menuItemBranch = new JMenu("Create script with create-table-statements");
		menu.add(menuItemBranch);
		for(int i = 0 ; i < symbols.length ; i++) {
			final String symbol = symbols[i];
			JMenuItem misym = new JMenuItem(symbol);
			menuItemBranch.add(misym);
			misym.addActionListener(
					new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ModelConfiguration hs = null;
								try {
									hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(f, true); 
								}
								catch(Exception ee) {
									UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
									return;
								}
								Recreator rec = RecreatorManager.getRecreator(symbol);
								int topLevel = hs.getMaxLevelValue();
								StringBuffer sb = new StringBuffer();
								for(int j = 0 ; j <= topLevel ; j++) {
									Vector<TableConfiguration> thss = hs.getTablehandlersWithLevel(j);
									for(int jj = 0 ; jj < thss.size() ; jj++) {
										String droppers[] = rec.create(thss.elementAt(jj).getCreateStatement());
										for(int jjj = 0 ; jjj < droppers.length ; jjj++) {
											sb.append(droppers[jjj]+" \n");
										}
										sb.append(" \n");
									}
								}
								String out = sb.toString();
								//System.out.println("SQL=\n"+out);
								Clipboard.setClipboard(out);
								UniversalTellUser.info(menu, "The script is in your clipboard");
							}
							catch(Exception _e) {
								IntolerableException ie = 
										new UnclassedError("Create create tables script - error, "+symbol,
												_e);
								UniversalTellUser.general(menu, ie);
							}
						}
					}
					);
		}
		menuItem = new JMenuItem("Generate yEd .tgf");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							ModelConfiguration hs = null;
							try {
								hs = ModelOrderCalculator.configTableOrderCalcule_NativeEncoding(f, true);
							}
							catch(Exception ee) {
								UniversalTellUser.error(menu, "Parse Error: "+ee.getMessage());
								return;
							}
							try {
								GenFile_yEd_tgf.generate(f.getAbsolutePath(), hs);
							}
							catch(Exception ee) {
								UniversalTellUser.error(menu, "Error on yEd - .tgf generation: "+ee.getMessage());
								return;
							}
						}
						catch(Exception _e) {
							IntolerableException ie = 
									new UnclassedError("Generate yEd .tgf - error",
											_e);
							UniversalTellUser.general(menu, ie);
						}
					}
				}
				);
		//}
	}
	
	public static void addExecutableMenu(final JMenu menu, final File f) {
		JMenuItem menuItem = null;
		menuItem = new JMenuItem("Execute");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Runnable r = new Runnable() {
							public void run() {
								try { 
									ExecuteScript.execute(f, menu);
								}
								catch(Exception ex) {
									UniversalTellUser.error(menu, ex.getMessage());
									ex.printStackTrace();
								}
							}
						};
						Thread t = new Thread(r);
						t.start();
					}
				});
		menuItem = new JMenuItem("Verify "+OmegaNativeMenu.omegaexecutable+" config");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Runnable r = new Runnable() {
							public void run() {
								try { 
									ExecuteScript.verify(f, menu);
								}
								catch(Exception ex) {
									UniversalTellUser.error(menu, ex.getMessage());
									ex.printStackTrace();
								}
							}
						};
						Thread t = new Thread(r);
						t.start();
					}
				}
				);
	}

	public static void addConnectionMenu(final JMenu menu, final File f) {
		JMenuItem menuItem = null;
		menuItem = new JMenuItem("Connect");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try { 
							DBConnectionSpecification dbcs = parseDBAPPConfig(menu, f);
							//NativeClientModelHandling det = new NativeClientModelHandling(dbcs.getDetailsImplementation());
							
							Recreator rec = RecreatorManager.getRecreator(dbcs.getSymbol()); 						
							EmbeddedModelHandling emh = dbcs.getDetailsImplementation();
							String packagename = "omega";
							SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
							ScriptingScopeFactory ssf = new ScriptingScopeFactory(emh, packagename, sysobject);
							ClientModelLogic cml = new ClientModelLogic(ssf);

							ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
							ew.setVisible(true);
						}
						catch(Exception ex) {
							UniversalTellUser.error(menu, ex.getMessage());
							ex.printStackTrace();
						}
					}
				}
				);
		menuItem = new JMenuItem("Parse "+OmegaNativeMenu.omegaapplication+" config");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try {
							DBConnectionSpecification dbcs = parseDBAPPConfig(menu, f);
							if(dbcs != null) {
								UniversalTellUser.info(menu, "Parse successful");
							}
							else {
								UniversalTellUser.error(menu, "Parse "+OmegaNativeMenu.connectname+" without errors but still null. Internal error!!!");
							}
						}
						catch(Exception _e)
						{
							IntolerableException ie =
									new UnclassedError("Parse "+OmegaNativeMenu.connectname+" config - error",
											_e);
							UniversalTellUser.general(menu, ie);
						}
					}
				}
				);
	}

	public static JMenu[] getFilesMenu(final File f) { 
		//System.out.println("getFilesMenu "+f.getAbsolutePath());
		final JMenu menu = new JMenu(OmegaNativeMenu.appname);
		File p = f.getParentFile();
		DiskFilesystemHierarchy dfsh = new DiskFilesystemHierarchy("root", null, p);
		final HierarchyLeaf hl = (HierarchyLeaf) dfsh.getChild(f.getName());
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaconfigpostfix)) {
			//System.out.println("getFilesMenu config "+f.getAbsolutePath());
			addConfigMenu(menu, f);
		}
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaapplicationpostfix)) {
			//System.out.println("getFilesMenu connection "+f.getAbsolutePath());
			addConnectionMenu(menu, f);
		}
		if(f.getAbsolutePath().endsWith(OmegaNativeMenu.omegaexecutablepostfix)) {
			//System.out.println("getFilesMenu connection "+f.getAbsolutePath());
			addExecutableMenu(menu, f);
		}
		return new JMenu[] { menu };
	}

}
