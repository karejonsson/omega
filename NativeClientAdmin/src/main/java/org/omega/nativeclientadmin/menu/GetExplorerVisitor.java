package org.omega.nativeclientadmin.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.hsqldb.util.DatabaseManagerSwing;
import org.hsqldb.util.FontDialogSwing;
import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.connectionspecification.struct.DBConnectionSpecification;
import org.omega.connectionspecification.struct.DBConnectionSpecificationVisitorTypecast;
import org.omega.connectionspecification.struct.DBHSQLDBFileConnectionSpecification;
import org.omega.connectionspecification.struct.DBHSQLDBMemoryConnectionSpecification;
import org.omega.connectionspecification.struct.DBHSQLDBRemoteConnectionSpecification;
import org.omega.connectionspecification.struct.DBIBMDB2ConnectionSpecification;
import org.omega.connectionspecification.struct.DBMSSQLSERVERConnectionSpecification;
import org.omega.connectionspecification.struct.DBMySQLConnectionSpecification;
import org.omega.connectionspecification.struct.DBOracleConnectionSpecification;
import org.omega.connectionspecification.struct.DBPOSTGRESConnectionSpecification;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;
import org.omega.nativeclient.treeview.ExplorerWindow;

import se.modlab.generics.exceptions.IntolerableException;

public class GetExplorerVisitor implements DBConnectionSpecificationVisitorTypecast {

	private ExplorerWindow ew = null;
	private IntolerableException ie = null;
	
	public ExplorerWindow getExplorer() throws IntolerableException {
		if(ie != null) {
			throw ie;
		}
		return ew;
	}
	
	private void doit(DBConnectionSpecification cs) {
		try {
			//NativeClientModelHandling details = new NativeClientModelHandling(cs.getDetailsImplementation());
			ScriptingScopeFactory ssf = new ScriptingScopeFactory(cs.getDetails(), "omega", new SystemProgrammingResourceAccessImplementation());
			ClientModelLogic cml = new ClientModelLogic(ssf);
			ew = new ExplorerWindow(cml, getHSQLDBExtraMenu(cml.getConnection()));
		}
		catch(IntolerableException e) {
			ie = e;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public static JMenu[] getHSQLDBExtraMenu(Connection c) {
		if(c == null) {
			return null;
		}
		JMenu db = new JMenu("HSQLDB");
		JMenuItem manager = new JMenuItem("Manager");
		db.add(manager);
		manager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DatabaseManagerSwing m =
					new DatabaseManagerSwing();
				m.main();
				m.setWaiting("Initializing");
				m.setWaiting(null);
				m.connect(c);
				FontDialogSwing.creatFontDialog(m);
				m.start();
			}
		});
		JMenu extraMenu[] = new JMenu[] { db };
		return extraMenu;
	}


	
	public void visit(DBHSQLDBFileConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBHSQLDBMemoryConnectionSpecification cs) { 
		doit(cs);
	}

	public void visit(DBHSQLDBRemoteConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBMSSQLSERVERConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBMySQLConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBOracleConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBIBMDB2ConnectionSpecification cs) {
		doit(cs);
	}

	public void visit(DBPOSTGRESConnectionSpecification cs) {
		doit(cs);
	}

	/*
	public void visit(RestfulConnectionSpecification cs) {
		doit(cs);
	}
	*/
}
