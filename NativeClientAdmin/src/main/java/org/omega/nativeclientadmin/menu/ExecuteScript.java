package org.omega.nativeclientadmin.menu;

import java.awt.Component;
import java.io.File;
import java.util.Set;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.executablescript.struct.CombinedFileStruct;
import org.omega.executablescript.struct.Flow;
import org.omega.executablescript.struct.FlowExploreStruct;
import org.omega.executablescript.struct.FlowStruct;
import org.omega.executablescript.struct.ParseRecursively;
import org.omega.groovyintegration.scope.ScriptingScopeFactory;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.groovyscripting.GroovyShellScriptingScope;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;
import org.omega.nativeclient.treeview.ExplorerWindow;
import org.omega.nativeclientadmin.dialogs.ChoseFlowToExecuteDialog;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class ExecuteScript { 
	
	public final static FlowExploreStruct.ExplorerLauncher launcher = new FlowExploreStruct.ExplorerLauncher() {

		public void launch(EmbeddedModelHandling emh, String packagename, boolean hold) {
			//System.out.println("ExecuteScript.launcher. launch hold="+hold);
			try {
				SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
				ScriptingScopeFactory ssf = new ScriptingScopeFactory(emh, packagename, sysobject);
				ClientModelLogic cml = new ClientModelLogic(ssf);
	
				final ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));

				SwingUtilities.invokeLater(new Runnable() {
	                public void run() {
	    				ew.setVisible(true);
	                }
	            });
				if(hold) {
					while(!ew.isVisible()) {
						Thread.sleep(100);
						//System.out.println("Loop 1");
					}
					while(hold) {
						Thread.sleep(300);
						//System.out.println("Loop 2");
						hold = ew.isVisible();
					}
				}
			}
			catch(Exception e) {
			}
		}
	};


		
	public static boolean verify(File f, Component comp) {
		CombinedFileStruct cfs = null; 
		try {
			cfs = ParseRecursively.parse(f, launcher);
		}
		catch(Exception e) {
			UniversalTellUser.error(comp, "Unable to parse the file.\n"+e.getMessage());
			return false;
		}
		Set<String> flowKeys = cfs.getFlowKeys();
		if(flowKeys.size() == 0) {
			UniversalTellUser.error(comp, "No flows to verify declared");
			return false;
		}
		if(flowKeys.size() > 1) {
			String one = flowKeys.toArray()[0].toString();
			String two = flowKeys.toArray()[1].toString();
			UniversalTellUser.error(comp, "Multiple flows to verify declared. ("+one+", "+two+", ...)");
			return false;
		}

		String one = flowKeys.toArray()[0].toString();
		Vector<FlowStruct> flows = cfs.getFlows(one);
		if(flows.size() == 0) {
			UniversalTellUser.error(comp, "No flows under the name "+one+" declared.");
			return false;
		}
		if(flows.size() > 1) {
			UniversalTellUser.error(comp, "Multiple flows under the name "+one+" declared. \n("+
		        "1: "+flows.get(0)+",\n"+
				"2: "+flows.get(1)+",\n"+
		        "...\n"+
		        flows.size()+": "+flows.get(flows.size()-1));
			return false;
		}

		FlowStruct flowStruct = flows.get(0);
		
		Flow flow = flowStruct.getFlow();
		
		SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
		GroovyShellScriptingScope mmss = null;
		
		try {
			mmss = new GroovyShellScriptingScope(sysobject);
		}
		catch(Exception e) {
			UniversalTellUser.error(comp, "Unable to create scripting scope.\n"+e.getMessage());
			return false;
		}
		
		String errorDescription = flow.verifyReturnErrorDescription(cfs, mmss);

		if(errorDescription != null) {
			UniversalTellUser.error(comp, errorDescription);
			return false;
		}
		UniversalTellUser.info(comp, "Verified OK!");
		return true;
	}

	public static void execute(File f, Component comp) throws Exception {
		CombinedFileStruct cfs = null; 
		try {
			cfs = ParseRecursively.parse(f, launcher);
		}
		catch(Exception e) {
			UniversalTellUser.error(comp, "Unable to parse the file.\n"+e.getMessage());
			return;
		}
		Set<String> flowKeys = cfs.getFlowKeys();
		if(flowKeys.size() == 0) {
			UniversalTellUser.error(comp, "No flows to execute declared");
			return;
		}
		if(flowKeys.size() > 1) {
			/*
			String one = flowKeys.toArray()[0].toString();
			String two = flowKeys.toArray()[1].toString();
			UniversalTellUser.error(comp, "Multiple flows to execute declared. ("+one+", "+two+", ...)");
			return;
			*/
			ChoseFlowToExecuteDialog cfte = new ChoseFlowToExecuteDialog("Handle flows", cfs);
			cfte.setVisible(true);
			return;
		}

		String one = flowKeys.toArray()[0].toString();
		Vector<FlowStruct> flows = cfs.getFlows(one);
		if(flows.size() == 0) {
			UniversalTellUser.error(comp, "No flows under the name "+one+" declared.");
			return;
		}
		if(flows.size() > 1) {
			/*
			UniversalTellUser.error(comp, "Multiple flows under the name "+one+" declared. \n("+
		        "1: "+flows.get(0)+",\n"+
				"2: "+flows.get(1)+",\n"+
		        "...\n"+
		        flows.size()+": "+flows.get(flows.size()-1));
		        */
			ChoseFlowToExecuteDialog cfte = new ChoseFlowToExecuteDialog("Handle flows", cfs);
			cfte.setVisible(true);
			return;
		}

		FlowStruct flowStruct = flows.get(0);
		
		Flow flow = flowStruct.getFlow();
		
		SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
		GroovyShellScriptingScope mmss = null;
		
		try {
			mmss = new GroovyShellScriptingScope(sysobject);
		}
		catch(Exception e) {
			UniversalTellUser.error(comp, "Unable to create scripting scope.\n"+e.getMessage());
			return;
		}
		
		flow.execute(cfs, mmss);
	}
	
	public static void main(String args[]) throws Exception {
		File prg = new File("/Users/karejonsson/Documents/workspace/omega/GroovyIntegration/src/test/resources/executables/5.oexe");
		File multi = new File("/Users/karejonsson/Documents/workspace/omega/NativeClientAdmin/src/test/resources/executables/2.oexe");
		execute(multi, null);
	}
	
	public static void _main(String args[]) throws Exception {
		String x[] = new String[7];
		String c1 = "acbethwdfs";
		String c2 = c1+"sagbsergserbs";
		String c3 = c1+"jdhrgjkaksfkj";
		String c4 = c2+"kjhdrg";
		String c5 = c2+"grgklk";
		String c6 = c3+"inckajaf";
		String c7 = c3+"maseofji";
		x[0] = c1;
		x[1] = c2;
		x[2] = c3;
		x[3] = c4;
		x[4] = c5;
		x[5] = c6;
		x[6] = c7;
		int min = Integer.MAX_VALUE;
		for(int i = 0 ; i < x.length -1 ; i++) {
			min = Math.min(min, nrLeadingCommons(x[i], x[i+1]));
		}
		System.out.println(min);
		min = Integer.MAX_VALUE;
		for(int i = x.length -1 ; i > 0 ; i--) {
			min = Math.min(min, nrLeadingCommons(x[i], x[i-1]));
		}
		System.out.println(min);
	}
	
	
	
	public static int nrLeadingCommons(String a, String b) {
		if(a == null) {
			return 0;
		}
		if(b == null) {
			return 0;
		}
		int ctr = 0;
		int max = Math.min(a.length(), b.length());
		while(ctr < max && a.charAt(ctr) == b.charAt(ctr)) {
			ctr++;
		}
		return ctr;
	}
	
}
