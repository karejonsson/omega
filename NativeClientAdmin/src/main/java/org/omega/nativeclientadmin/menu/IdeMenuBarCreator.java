package org.omega.nativeclientadmin.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.omega.nativeclientadmin.dialogs.ConnectDialog;
import org.omega.nativeclientadmin.dialogs.ModelCalcDialog;
import org.omega.nativeclientadmin.dialogs.ModelCompareTool;
import org.omega.nativeclientadmin.dialogs.ModelConsistencyCheckTool;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class IdeMenuBarCreator {

	public static JMenu[] getMenubar() {
		final JMenu menu = new JMenu(OmegaNativeMenu.appname);
		JMenuItem menuItem = new JMenuItem("About");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						UniversalTellUser.info(menu, "Version "+OmegaNativeMenu.version+"\n"+"Compiled "+OmegaNativeMenu.date);
					}
				}
				);
		menuItem = new JMenuItem("Connect");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new ConnectDialog("General DB application connect dialog");
					}
				}
				);
		menuItem = new JMenuItem("Extract model");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new ModelCalcDialog(OmegaNativeMenu.appname+" model calculation");
					}
				}
				);
		menuItem = new JMenuItem("Compare models");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new ModelCompareTool("Compare "+OmegaNativeMenu.appname+" models");
					}
				}
				);
		menuItem = new JMenuItem("Consistency check");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						new ModelConsistencyCheckTool("Compare "+OmegaNativeMenu.appname+" models", "");
					}
				}
				);
		menuItem = new JMenuItem("Open HSQLDB manager");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						org.hsqldb.util.DatabaseManagerSwing.main(new String[] {});
					}
				}
				);
		return new JMenu[] { menu };
	}

}
