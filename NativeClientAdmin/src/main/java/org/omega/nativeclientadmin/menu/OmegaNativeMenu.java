package org.omega.nativeclientadmin.menu;

import javax.swing.JMenu;

import org.omega.nativeclientadmin.fileview.DefaultIdeMenuHandler;

import java.io.File;

import se.modlab.generics.gui.files.OneFileNotepad;
 
public class OmegaNativeMenu extends DefaultIdeMenuHandler {

	public static final String appname = "Omega";
	public static final String fullappname = "Object model embedding groovy application";
	public static final String connectname = "database application";
	public static final String version = "0.1";
	public static final String date = "2017-feb-13";
	public static final String editor_executed = "Editor executed";
	public static final String omegaconfigpostfix = ".omega";
	public static final String omegaapplication = "OAPP";
	public static final String omegaapplicationpostfix = "."+omegaapplication.toLowerCase();
	public static final String omegaexecutable = "OEXE";
	public static final String omegaexecutablepostfix = "."+omegaexecutable.toLowerCase();
	// private static boolean logSQL = false;

	public OmegaNativeMenu() {
	}

	public JMenu[] getEditorsMenu(final OneFileNotepad ofn) {
		return IdeEditorsMenuCreator.getEditorsMenu(ofn);
	}
	
	public JMenu[] getFilesMenu(final File f) {
		return IdeFilesMenuCreator.getFilesMenu(f);
	}

	public JMenu[] getMenubar() {
		return IdeMenuBarCreator.getMenubar();
	}
	
	public JMenu[] getFoldersMenu(final File f) {
		return null;
	}

}
