package org.omega.nativeclientadmin.fileview;

import javax.swing.*;
import se.modlab.generics.gui.progress.*;
import java.awt.*;
import java.awt.event.*;

public class FrameTracker
{

  JMenu menu = new JMenu("editors");

  public FrameTracker()
  {
  }

  public JMenu getBarsEditorMenu()
  {
    return menu;
  }

  public void addTrackedFrame(Frame f)
  {
    if(f == null) return;
    //frames.addElement(f);
    //printFramesTitles("Add");
    int similar = 0;
    for(int i = 0 ; i < menu.getItemCount() ; i++)
    {
      if(menu.getItem(i).toString().compareTo(
        f.getTitle()) == 0)
      {
        similar++;
      }
    }
    //System.out.println("Similar = "+similar);
    if(similar == 0)
    {
      menu.add(new frameHandler(f));
      ProgressDumpAndMonitor.addText(
        "Filehandler",
        "Opened editor for file "+f.getTitle());
      return;
    }
    
    if(similar != 0)
    {
      Object[] options = { "YES", "NO" };
      String message = null;
      if(similar == 1)
      {
        message = 
          "There is "+similar+" similar frame opened.\n"+
          "Do you want to revert to the old one?";
      }
      else
      {
        message = 
          "There are "+similar+" similar frames opened.\n"+
          "Do you want to revert to the first of the old ones?";
      }
      int choice = 
        JOptionPane.showOptionDialog(f, 
                                     message, 
                                     "Multiple editor warning",
                                     JOptionPane.DEFAULT_OPTION, 
                                     JOptionPane.INFORMATION_MESSAGE,
                                     null, 
                                     options, 
                                     options[0]);
      if(choice != 0)
      {
        menu.add(new frameHandler(f));
        ProgressDumpAndMonitor.addText(
          "Filehandler",
          "Opened editor for file "+f.getTitle());
        return;
      }
      for(int i = 0 ; i < menu.getItemCount() ; i++)
      {
        if(menu.getItem(i).toString().compareTo(
          f.getTitle()) == 0)
        {
          f.dispose();
          ProgressDumpAndMonitor.addText(
            "Filehandler",
            "Closed editor for file "+f.getTitle()+" on revert");
          Frame revertTo = ((frameHandler) menu.getItem(i)).getFrame();
          revertTo.setVisible(true);
          revertTo.toFront();
          revertTo.setFocusable(true);
          revertTo.requestFocus();
          return;
        }
      }
    }
  }

  public void removeTrackedFrame(Frame f)
  {
    if(f == null) return;
    f.dispose();
    for(int i = 0 ; i < menu.getItemCount() ; i++)
    {
      if(((frameHandler) menu.getItem(i)).getFrame() == f)
      {
        menu.remove((frameHandler) menu.getItem(i));
        ProgressDumpAndMonitor.addText(
          "Filehandler",
          "Closed editor for file "+f.getTitle());
        return;
      }
    }
  }

  private class frameHandler extends JMenuItem
                             implements ActionListener
  { 
    private Frame f;
    public frameHandler(Frame _f)
    { 
      super(_f.getTitle()); 
      f = _f;
      addActionListener(this);
    }
    public void actionPerformed(ActionEvent e)
    {
      f.setVisible(true);
      f.toFront();
    }
    public Frame getFrame()
    {
      return f;
    }
    public String getName()
    {
      return f.getTitle();
    }
    public String toString()
    {
      return getName();
    }
  }

}