package org.omega.nativeclientadmin.fileview;

import se.modlab.generics.gui.files.*;

import java.io.*;
import java.util.*;
import javax.swing.*;

public class MultiIdeMenuHandler extends DefaultIdeMenuHandler
{

  private String packname;
  private IdeMenuHandler children[];

  public MultiIdeMenuHandler(String name, IdeMenuHandler _children[])
  {
    packname = name;
    children = _children;
  }

  public JMenu[] getFilesMenu(File _f)
  {
    Vector<JMenu> menues = new Vector<JMenu>();
    for(int i = 0 ; i < children.length ; i++) 
    {
      JMenu tmp[] = children[i].getFilesMenu(_f);
      if(tmp == null) continue;
      for(int j = 0 ; j < tmp.length ; j++) 
      {
        menues.addElement(tmp[j]);
      }
    }
    JMenu out[] = new JMenu[menues.size()];
    for(int i = 0 ; i < menues.size() ; i++)
    {
      out[i] = menues.elementAt(i);
    }
    return out;
  }

  public JMenu[] getEditorsMenu(OneFileNotepad ofn)
  { 
    Vector<JMenu> menues = new Vector<JMenu>();
    for(int i = 0 ; i < children.length ; i++) 
    {
      JMenu tmp[] = children[i].getEditorsMenu(ofn);
      if(tmp == null) continue;
      for(int j = 0 ; j < tmp.length ; j++) 
      {
        menues.addElement(tmp[j]);
      }
    }
    JMenu out[] = new JMenu[menues.size()];
    for(int i = 0 ; i < menues.size() ; i++)
    {
      out[i] = menues.elementAt(i);
    }
    return out;
  }

  public JMenu[] getFoldersMenu(File f)
  {
    Vector<JMenu> menues = new Vector<JMenu>();
    for(int i = 0 ; i < children.length ; i++) 
    {
      JMenu tmp[] = children[i].getFoldersMenu(f);
      if(tmp == null) continue;
      for(int j = 0 ; j < tmp.length ; j++) 
      {
        menues.addElement(tmp[j]);
      }
    }
    JMenu out[] = new JMenu[menues.size()];
    for(int i = 0 ; i < menues.size() ; i++)
    {
      out[i] = menues.elementAt(i);
    }
    return out;
  }

  public JMenu[] getMenubar()
  {
    Vector<JMenu> menues = new Vector<JMenu>();
    for(int i = 0 ; i < children.length ; i++) 
    {
      JMenu tmp[] = children[i].getMenubar();
      if(tmp == null) continue;
      for(int j = 0 ; j < tmp.length ; j++) 
      {
        menues.addElement(tmp[j]);
      }
    }
    JMenu out = new JMenu(packname);
    for(int i = 0 ; i < menues.size() ; i++)
    {
      out.add(menues.elementAt(i));
    }
    return new JMenu[] {out};
  }

}
