package org.omega.nativeclientadmin.fileview;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.awt.event.*;

import java.io.IOException;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import java.io.*;
public class DnDExplorerTree extends JTree
    implements DragGestureListener, DragSourceListener 
{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
public static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  /** the selected node */
  protected TreePath _selectedTreePath;

  /** dnd source */
  private DragSource _dragSource;

  /** Creates a new instance of DnDArgoJTree */
  public DnDExplorerTree(TreeNode tn)
  {
    super(tn);
    this.addTreeSelectionListener(new DnDTreeSelectionListener());
    _dragSource = DragSource.getDefaultDragSource();

    DragGestureRecognizer dgr =
      _dragSource.createDefaultDragGestureRecognizer(
        this,
        DnDConstants.ACTION_COPY_OR_MOVE, //specifies valid actions
        this);

    // Eliminates right mouse clicks as valid actions
    dgr.setSourceActions(dgr.getSourceActions() & ~InputEvent.BUTTON3_MASK);

    // First argument:  Component to associate the target with
    // Second argument: DropTargetListener
    DropTarget dropTarget =
      new DropTarget(this, new ArgoDropTargetListener());
  }

  private void doCopy(DefaultMutableTreeNode src, DefaultMutableTreeNode dest)
  {
    System.out.println("## Copy src = "+src+", dest  = "+dest);
  }

  private void doMove(DefaultMutableTreeNode src, DefaultMutableTreeNode dest)
  {
    System.out.println("## Move src = "+src+", dest  = "+dest);
  }

  /**
   * recognises the start of the drag
   */
  public void dragGestureRecognized(DragGestureEvent dragGestureEvent) 
  {
    //Get the selected node from the JTree
    _selectedTreePath = getSelectionPath();
    if(_selectedTreePath == null) return;
    Object dragNode = ((DefaultMutableTreeNode) _selectedTreePath
      .getLastPathComponent()).getUserObject();
    if(dragNode != null) 
    {
      //Get the Transferable Object
      Transferable transferable = new TransferableModelElement(dragNode);
      //Select the appropriate cursor;
      Cursor cursor = DragSource.DefaultCopyNoDrop;
      int action = dragGestureEvent.getDragAction();
      if(action == DnDConstants.ACTION_MOVE)
      {
        cursor = DragSource.DefaultMoveDrop;
      }

      //begin the drag
      _dragSource.startDrag(dragGestureEvent, cursor, transferable, this);
    }
  }

  /**
   *
   */
  class ArgoDropTargetListener implements DropTargetListener 
  {
    public void dragEnter(DropTargetDragEvent dropTargetDragEvent) { }

    public void dragExit(DropTargetEvent dropTargetEvent) { }

    public void dragOver(DropTargetDragEvent dropTargetDragEvent) 
    {
      //set cursor location. Needed in setCursor method
      Point cursorLocationBis = dropTargetDragEvent.getLocation();
      TreePath destinationPath =
        getPathForLocation(cursorLocationBis.x, cursorLocationBis.y);

      // if destination path is okay accept drop...
      // can't drag to a descendant
      // there will be other rules.
      if(!_selectedTreePath.isDescendant(destinationPath)) 
      {
        dropTargetDragEvent
          .acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
      }
      // ...otherwise reject drop
      else
      {
        dropTargetDragEvent.rejectDrag();
      }
    }

    /**
     * what is done when drag is released.
     */
    public void drop(java.awt.dnd.DropTargetDropEvent dropTargetDropEvent) 
    {
      try 
      {
        Transferable tr = dropTargetDropEvent.getTransferable();
        //flavor not supported, reject drop
        if(!tr.isDataFlavorSupported(TransferableModelElement.ELEM_FLAVOR)) 
        {
          //_cat.debug("! isDataFlavorSupported");
          dropTargetDropEvent.rejectDrop();
        }

        //get the model element that is being transfered.
        Object modelElement = tr.getTransferData(TransferableModelElement.ELEM_FLAVOR );

        //get new parent node
        Point loc = dropTargetDropEvent.getLocation();
        TreePath destinationPath = getPathForLocation(loc.x, loc.y);

        final String msg = isValidDropTarget(destinationPath, _selectedTreePath);
        if(msg != null)
        {
          dropTargetDropEvent.rejectDrop();

          SwingUtilities.invokeLater(new Runnable() 
            {
              public void run()
              {
                JOptionPane.showMessageDialog(
                  null,
                  msg,
                  "Error Dialog",
                  JOptionPane.ERROR_MESSAGE);
              }
            });
          // reset the cursor.
          dropTargetDropEvent.getDropTargetContext()
            .getComponent().setCursor(Cursor.getDefaultCursor());
          return;
        }

        Object destinationModelElement =
          ((DefaultMutableTreeNode) destinationPath
            .getLastPathComponent()).getUserObject();

        //get old parent node
        Object oldParentME =
          ((DefaultMutableTreeNode) _selectedTreePath.getParentPath()
            .getLastPathComponent()).getUserObject();

        Object src =
          ((DefaultMutableTreeNode) _selectedTreePath
            .getLastPathComponent()).getUserObject();

        int action = dropTargetDropEvent.getDropAction();
        boolean copyAction = (action == DnDConstants.ACTION_COPY);
        boolean moveAction = (action == DnDConstants.ACTION_MOVE);

        try
        {
          if(copyAction)
          {
            doCopy((DefaultMutableTreeNode) _selectedTreePath
              .getLastPathComponent(), (DefaultMutableTreeNode) destinationPath
              .getLastPathComponent());

            dropTargetDropEvent.acceptDrop(DnDConstants.ACTION_COPY);
          }
          else
          {
            doMove((DefaultMutableTreeNode) _selectedTreePath
              .getLastPathComponent(), (DefaultMutableTreeNode) destinationPath
              .getLastPathComponent());
            dropTargetDropEvent.acceptDrop(DnDConstants.ACTION_MOVE);
          }
        }
        catch (java.lang.IllegalStateException ils)
        {
          //_cat.debug("drop IllegalStateException");
          dropTargetDropEvent.rejectDrop();
        }
        dropTargetDropEvent.getDropTargetContext().dropComplete(true);
      }
      catch (IOException io)
      {
        //_cat.debug("drop IOException");
        dropTargetDropEvent.rejectDrop();
      }
      catch (UnsupportedFlavorException ufe)
      {
        //_cat.debug("drop UnsupportedFlavorException");
        dropTargetDropEvent.rejectDrop();
      }
      catch (Exception e)
      {
        //_cat.debug("drop Exception");
        dropTargetDropEvent.rejectDrop();
      }
    }

    /** @return a string message if dest is valid,
     *          else returns null.
     */
    private String isValidDropTarget(TreePath destinationPath,
                                     TreePath sourceTreePath) 
    {
      if(destinationPath == null || sourceTreePath == null)
      {
        return null;
      }

      Object dest =
        ((DefaultMutableTreeNode) destinationPath
          .getLastPathComponent()).getUserObject();
      Object src =
        ((DefaultMutableTreeNode) sourceTreePath
          .getLastPathComponent()).getUserObject();
      System.out.println("isDropValid src = "+src+", "+dest+" = "+dest);

      if(!(dest instanceof File)) return null;
      if(((File) dest).isDirectory()) return null;
      return "you can't drag there.";
    }
    public void dropActionChanged(DropTargetDragEvent dropTargetDragEvent) { }
  }

  /** DragSourceListener empty implementation - not used */
  public void dragDropEnd(DragSourceDropEvent dragSourceDropEvent) { }

  /** DragSourceListener empty implementation - not used */
  public void dragEnter(DragSourceDragEvent dragSourceDragEvent) { }

  /** DragSourceListener empty implementation - not used */
  public void dragExit(DragSourceEvent dragSourceEvent) { }

  /** DragSourceListener empty implementation - not used */
  public void dragOver(DragSourceDragEvent dragSourceDragEvent) { }

  /** DragSourceListener empty implementation - not used */
  public void dropActionChanged(DragSourceDragEvent dragSourceDragEvent) { }

    /**
     * records the selected path for later use during dnd.
     */
    class DnDTreeSelectionListener implements TreeSelectionListener {

        public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
            _selectedTreePath = treeSelectionEvent.getNewLeadSelectionPath();
        }

    }

    public static class PopupListener extends MouseAdapter
    {

      public PopupListener()
      {
      }

      public void mousePressed(MouseEvent e)
      {
        maybeShowPopup(e);
      }

      public void mouseReleased(MouseEvent e)
      {
        maybeShowPopup(e);
      }

      private void maybeShowPopup(MouseEvent e)
      {
        if(e.isPopupTrigger())
        {
          //System.out.println(e.getComponent().getClass().getName());
          if(e == null) return;
          JTree tree = (JTree) e.getComponent();
          if(tree == null) return;
          TreePath tp = tree.getClosestPathForLocation(e.getX(), e.getY());
          tree.setSelectionPath(tp);
          System.out.println("popup");
          /*
          fileSystemNode fsn = (fileSystemNode) tree.getLastSelectedPathComponent();
          if(fsn == null) return;
          JPopupMenu popup = fsn.getPopup(tree);
          if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
           */
        }
      }

    }

    public static void main(String args[])
    {
      DefaultMutableTreeNode tn = new DefaultMutableTreeNode("TOP");
      DefaultMutableTreeNode ttn = new DefaultMutableTreeNode("Green 1");
      DefaultMutableTreeNode _tn = null;
      ttn.setUserObject("uGreen 1");
      tn.add(ttn);
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 1"));
      _tn.setUserObject("gu 1 1");
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 2"));
      _tn.setUserObject("gu 1 2");
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 3"));
      _tn.setUserObject("gu 1 3");

      ttn = new DefaultMutableTreeNode("Green 2");
      ttn.setUserObject("uGreen 2");
      tn.add(ttn);
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 1"));
      _tn.setUserObject("gu 2 1");
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 2"));
      _tn.setUserObject("gu 2 2");
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 3"));
      _tn.setUserObject("gu 2 3");
      DnDExplorerTree tree = new DnDExplorerTree(tn);
      //tree.s add(tn);

      tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
      JScrollPane view = new JScrollPane(tree);
      view.setMinimumSize(new Dimension(100, 50));
      view.setPreferredSize(new Dimension(700, 500));
      JFrame frame = new JFrame ("test");
      frame.getContentPane().removeAll();
      frame.getContentPane().add(view, BorderLayout.CENTER);
      tree.addMouseListener(new PopupListener());
      frame.pack();
      frame.setLocation(new Point(0,0));
      frame.setSize(new Dimension((int)(SCREEN_WIDTH/3),  (int)(SCREEN_HEIGHT/2)));
      frame.addWindowListener(
        new WindowAdapter()
        {
          public void windowClosing(WindowEvent event)
          {
            System.exit(0);
          }
          public void windowClosed(WindowEvent event)
          {
            System.exit(0);
          }
        }
      );
      frame.setVisible(true);
      System.out.println("SLUT");

    }
}

/**
 * Encaosulates a UML Model element for data transfer.
 */
class TransferableModelElement implements Transferable
{

  public static final DataFlavor ELEM_FLAVOR =
    new DataFlavor(Object.class, "UML Model Element");

  static DataFlavor flavors[] = {ELEM_FLAVOR };

  Object _modelElement;

  public TransferableModelElement(Object modelElement) {
    _modelElement = modelElement;
  }

  public Object getTransferData(java.awt.datatransfer.DataFlavor dataFlavor)
    throws UnsupportedFlavorException,
           java.io.IOException
  {
    if(dataFlavor.match(ELEM_FLAVOR))
    {
      return _modelElement;
    }
    else throw new UnsupportedFlavorException(dataFlavor);
  }

  public java.awt.datatransfer.DataFlavor[] getTransferDataFlavors() {
    return flavors;
  }

  public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
    return dataFlavor.match(ELEM_FLAVOR);
  }

}
