package org.omega.nativeclientadmin.fileview;

import java.io.*;
import java.util.*;
import javax.swing.tree.*;

public class FileTreeCorrectionVisitor extends FileVisitorAdapter
{

  private FileSystemNode nodeToRefresh = null;

  public FileSystemNode getNodeToRefresh()
  {
    return nodeToRefresh;
  }

  public void visit(FileNode fn) 
    throws Exception 
  {
    //System.out.println("visit(fileNode fn)"+fn);
  }
 
  public void visit(FolderNode fn) 
    throws Exception 
  {
    File f = fn.getFile();   
    File children[] = f.listFiles();
    for(int i = 0 ; i < children.length ; i++)
    {
      File child = children[i];
      if(!fn.contains(child))
      {
        if(child.isDirectory())
        {
          FolderNode node = 
            new FolderNode(child,
                           fn.getIdeMenuHandlers(),
                           fn.getFrameTracker());
          fn.add(node); 
          node.setParent(fn);
          if(nodeToRefresh == null) nodeToRefresh = fn;
          continue;
        } 
        if(child.isFile())
        {
          FileNode node = 
            new FileNode(child,
                         fn.getIdeMenuHandlers(),
                         fn.getFrameTracker());
          fn.add(node); 
          node.setParent(fn);
          if(nodeToRefresh == null) nodeToRefresh = fn;
          continue;
        } 
      } 
    }
    Vector toRemove = new Vector();
    for(int i = 0 ; i < fn.getChildCount() ; i++)
    {
      FileSystemNode fsn = (FileSystemNode) fn.getChildAt(i);
      if(!isWithin(fsn.getFile(), children))
      {
        if(nodeToRefresh == null) nodeToRefresh = fn;
        toRemove.addElement(fsn);
      }
    }
    for(int i = 0 ; i < toRemove.size() ; i++)
    {
      fn.remove((FileSystemNode) toRemove.elementAt(i));
    }
    for(int i = 0 ; i < fn.getChildCount() ; i++)
    {
      ((FileSystemNode) fn.getChildAt(i)).accept(this);
    }
  }
 
  public void visit(TopNode fn) 
    throws Exception 
  {
    File f = fn.getFile();   
    File children[] = f.listFiles();
    for(int i = 0 ; i < children.length ; i++)
    {
      File child = children[i];
      if(!fn.contains(child))
      {
        if(child.isDirectory())
        {
          FolderNode node = 
            new FolderNode(child,
                           fn.getIdeMenuHandlers(),
                           fn.getFrameTracker());
          fn.add(node); 
          node.setParent(fn);
          if(nodeToRefresh == null) nodeToRefresh = fn;
          continue;
        } 
        if(child.isFile())
        {
          FileNode node = 
            new FileNode(child,
                         fn.getIdeMenuHandlers(),
                         fn.getFrameTracker());
          fn.add(node); 
          node.setParent(fn);
          if(nodeToRefresh == null) nodeToRefresh = fn;
          continue;
        } 
      } 
    }
    Vector toRemove = new Vector();
    for(int i = 0 ; i < fn.getChildCount() ; i++)
    {
      FileSystemNode fsn = (FileSystemNode) fn.getChildAt(i);
      if(!isWithin(fsn.getFile(), children))
      {
        if(nodeToRefresh == null) nodeToRefresh = fn;
        toRemove.addElement(fsn);
      }
    }
    for(int i = 0 ; i < toRemove.size() ; i++)
    {
      fn.remove((FileSystemNode) toRemove.elementAt(i));
    }
    for(int i = 0 ; i < fn.getChildCount() ; i++)
    {
      ((FileSystemNode) fn.getChildAt(i)).accept(this);
    }
  }

  private boolean isWithin(File one, File within[])
  {
    for(int i = 0 ; i < within.length ; i++)
    {
      if(one.compareTo(within[i]) == 0)
      {
        return true;
      }
    }
    return false;
  }

}