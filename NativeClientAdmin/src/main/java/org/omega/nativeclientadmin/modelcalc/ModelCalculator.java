package org.omega.nativeclientadmin.modelcalc;

import java.io.OutputStream;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.UnmanageableType;

import se.modlab.generics.gui.progress.MonitoredWorker;

public abstract class ModelCalculator implements MonitoredWorker {
	
	protected String style = null;
	protected String extension = null;

	public void setOutstyle(String _style, String _extension) {
		style = _style;
		extension = _extension;
	}
	
	public abstract void printModelToStream(OutputStream os) throws Exception;
	
	protected String getValidObjectName(String name) {
		boolean hyphens = false;
		if(name.contains("#")) {
			hyphens = true;
		}
		if(name.contains(" ")) {
			hyphens = true;
		}
		if(name.contains("-")) {
			hyphens = true;
		}
		if(name.contains("/")) {
			hyphens = true;
		}
		if(name.contains("$")) {
			hyphens = true;
		}
		if(name.contains(":")) {
			hyphens = true;
		}
		String mod = name;
		if(hyphens) {
			mod = "`"+name+"`";
		}
		return mod;
	}

	protected String verifyColumnNamesValidity(String colname, Column col) {
		String mod = getValidObjectName(colname);
		if(mod.compareTo(colname) != 0) {
			col.setComment("Name changed from "+colname);
		}
		return mod;
	}

	private String taskmessage = null;
	public void setTaskMessgage(String _taskmessage) {
		taskmessage = _taskmessage;
	}
	public String getTasksMessage() {
		return taskmessage;
	}

	private String barmessage = null;
	public void setBarMessgage(String _barmessage) {
		barmessage = _barmessage;
	}
	public String getMessageForBar() {
		return barmessage;
	}

	private String dumpmessage = null;
	public void setDumpMessgage(String _dumpmessage) {
		dumpmessage = _dumpmessage;
	}
	public String getMessageForDump() {
		return dumpmessage;
	}

	private boolean done = false;
	public void setDone(boolean _done) {
		done = _done;
	}
	public boolean isDone() {
		return done;
	}
	
	public void stop() {
	}

	private int current = 0;
	public void setCurrent(int _current) {
		current = _current;
	}
	public int getCurrent() {
		return current;
	}

	private int lengthOftask = 100;
	public void setLengthOftask(int _lengthOftask) {
		lengthOftask = _lengthOftask;
	}
	public int getLengthOfTask() {
		return lengthOftask;
	}

	public String getName() {
		return "Name";
	}
	
	public boolean isDatetimeVariation(String typedef) {
		typedef = typedef.toLowerCase();
		if(typedef.startsWith("date")) {
			return true;
		}
		if(typedef.startsWith("datetime")) {
			return true;
		}
		if(typedef.startsWith("datetime2")) {
			return true;
		}
		return false;
	}

	public boolean isVarcharVariation(String typedef) {
		typedef = typedef.toLowerCase();
		if(typedef.startsWith("varchar")) {
			return true;
		}
		if(typedef.startsWith("nvarchar")) {
			return true;
		}
		if(typedef.startsWith("varchar2")) {
			return true;
		}
		return false;
	}

	public boolean isCharVariation(String typedef) {
		typedef = typedef.toLowerCase();
		if(typedef.startsWith("char")) {
			return true;
		}
		if(typedef.startsWith("nchar")) {
			return true;
		}
		return false;
	}

	public boolean isUnmanageable(String typedef) {
		typedef = typedef.toLowerCase();
		UnmanageableType out = null;
		if(typedef.startsWith("longblob")) {
			out = new UnmanageableType("longblob");
		}
		if(typedef.startsWith("blob")) {
			out = new UnmanageableType("blob");
		}
		if(typedef.startsWith("time")) {
			out = new UnmanageableType("time");
		}
		if(typedef.startsWith("date")) {
			out = new UnmanageableType("date");
		}
		if(typedef.startsWith("longtext")) {
			out = new UnmanageableType("longtext");
		}
		if(typedef.startsWith("mediumblob")) {
			out = new UnmanageableType("mediumblob");
		}
		if(typedef.startsWith("mediumtext")) {
			out = new UnmanageableType("mediumtext"); 
		}
		if(typedef.startsWith("nchar")) {
			out = new UnmanageableType("nchar");
		}
		if(typedef.startsWith("binary")) {
			out = new UnmanageableType("binary");
		}
		if(typedef.startsWith("varbinary")) {
			out = new UnmanageableType("varbinary");
		}
		if(typedef.startsWith("bit")) {
			out = new UnmanageableType("bit");
		}
		if(typedef.startsWith("datetime2")) {
			out = new UnmanageableType("datetime2");
		}
		if(typedef.startsWith("image")) {
			out = new UnmanageableType("image");
		}
		if(typedef.startsWith("money")) {
			out = new UnmanageableType("money");
		}
		if(typedef.startsWith("datetime2")) {
			out = new UnmanageableType("datetime2");
		}
		return out == null;
	}
	
	protected String assembleErrorMessage(CreateStatement cs) {
		StringBuffer sb = new StringBuffer();
		boolean commentStarted = false;
		if(cs.getPrimaryKeyDeclaration() == null) {
			sb.append("/*\nErrors in model\n\n");
			commentStarted = true;
			sb.append("Table has no primary key field.\n");
		}
		if(cs.getNoErrors() != 0) {
			if(!commentStarted) {
				commentStarted = true;
				sb.append("/*\nErrors in model\n\n");
			}
			for(int i = 0 ; i < cs.getNoErrors() ; i++) {
				commentStarted = true;
				sb.append(cs.getError(i)+"\n");
			}
			if(commentStarted) {
				commentStarted = false;
				sb.append("*/\n");
			}
		}
		if(commentStarted) {
			commentStarted = false;
			sb.append("*/\n");
		}
		return sb.toString();
	}

}
