package org.omega.nativeclientadmin.modelcalc;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.BigintType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntIntType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.SmallintType;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TinyintType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.struct.VarcharType;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.resultset.SQLResult;
import org.omega.nativeclientadmin.modelcalc.ModelCalculator;

public class MySQLModelCalculator extends ModelCalculator {
	
	private ConnectionOperations conn = null;
	private Recreator rec = null;
	
	public MySQLModelCalculator(ConnectionOperations _conn) throws SQLException {
		conn = _conn;
	}

	public Type getType(String typedef) {
		Type out = null;
		if(typedef.startsWith("int")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new IntIntType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new IntIntType();
			}
		}
		if(typedef.startsWith("smallint")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new SmallintType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new SmallintType();
			}
		}
		if(typedef.startsWith("tinyint")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new TinyintType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new TinyintType();
			}
		}
		if(typedef.startsWith("bigint")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new BigintType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new BigintType();
			}
		}
		if(isDatetimeVariation(typedef)) {
			out = new DatetimeType();
		}
		if(typedef.startsWith("float")) { // FEL?
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DecimalType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DecimalType();
			}
		}
		if(isVarcharVariation(typedef)) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(0);
			}
		}
		if(isCharVariation(typedef)) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
		}
		if(typedef.startsWith("double")) {
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DoubleType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DoubleType();
			}
		}
		if(typedef.startsWith("decimal")) {
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DecimalType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DecimalType();
			}
		}
		if(typedef.startsWith("float")) { // FEL?
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DecimalType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DecimalType();
			}
		}
		if(typedef.startsWith("varchar")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(0);
			}
		}
		if(typedef.startsWith("char")) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
		}
		if(typedef.startsWith("text")) {
			out = new TextType();
		}
		if(typedef.startsWith("tinytext")) {
			out = new TinytextType();
		}
		if(out == null) {
			//System.out.println("Ingen typ f�r "+typedef);
			out = new UnmanageableType(typedef);
		}
		return out;
	}
	
	private String getFirstOnlyFromSQL(String sql) throws SQLException {
		SQLResult sqlrt = conn.select_via_executeQuery(sql);
		sqlrt.next();
		String firstOnly = sqlrt.getString(1);
		sqlrt.close();
		return firstOnly;
	}
	
	private CreateStatement getCreateStatement(String tablename) throws Exception {
		//System.out.println("MySQLModelCalculator.getCreateStatement: Tabellnamn "+tablename);
		CreateStatement out = new CreateStatement();
		out.setName(getValidObjectName(tablename));
		String columns_sql = "show columns in "+tablename;
		SQLResult sqlrt = conn.select_via_executeQuery(columns_sql);
		
		//Vector<String> foreigns = new Vector<String>();
		
		PrimaryKeyDeclaration pkd = new PrimaryKeyDeclaration(tablename);
		while(sqlrt.next()) {
			Column col = new Column();
			String name = verifyColumnNamesValidity(sqlrt.getString(1), col);
			col.setName(name);
			String typedef = sqlrt.getString(2);
			//String default_ = sqlrt.getString(5);
			String extra = sqlrt.getString(6);
			String isnullable = sqlrt.getString(3);
			String columnkey = sqlrt.getString(4);
			if(columnkey.toLowerCase().contains("pri")) {
				pkd.addMember(name);
			}
			Type type = getType(typedef);
			if(isnullable.toLowerCase().contains("no")) {
				type.setNotNull();
			}

			col.setType(type);
			if(extra.contains("auto_increment")) {
				col.setAutoIncrement();
			}
			out.addColumn(col);
		}
		if(pkd.getNoMembers() != 0) {
			out.setPrimaryKeyDeclaration(pkd);
		}
		sqlrt.close();
		String fk_sql = "select constraint_name, column_name, referenced_table_name, referenced_column_name "+
				"from information_schema.key_column_usage where constraint_schema = '"+conn.getDBName()+"' and table_name = '"+tablename+"' and constraint_name <> 'PRIMARY'";
		sqlrt = conn.select_via_executeQuery(fk_sql);
		while(sqlrt.next()) {
			Object foreignTable_obj = sqlrt.getObject(3);
			String foreignTable_str = sqlrt.getString(3);
			if(foreignTable_obj != null) {
				ForeignKeyDeclaration fkd = new ForeignKeyDeclaration(sqlrt.getString(1));
				fkd.setReferringTablename(tablename);
				fkd.addReferringMember(sqlrt.getString(2));					
				fkd.setReferencedTablename(foreignTable_str);
				fkd.addReferredTableMember(sqlrt.getString(4)); 
				out.addForeignKeyDeclarationForgiving(fkd);
				//System.out.println("MySQLModelCalculator.getCreateStatement: fkd = "+fkd+", foreignTable_obj="+foreignTable_obj+", foreignTable_str="+foreignTable_str);
			}
			else {
				KeyDeclaration kd = new KeyDeclaration();
				kd.setKeyName(sqlrt.getString(1));
				kd.setUnique();
				kd.setTablename(tablename);
				kd.addMember(sqlrt.getString(2));
				out.addKey(kd);
				//System.out.println("MySQLModelCalculator.getCreateStatement: kd = "+kd+", foreignTable_obj="+foreignTable_obj+", foreignTable_str="+foreignTable_str);
			}
		}
		sqlrt.close();
		return out;
	}
	
	private void printStatementToStream(OutputStream os, CreateStatement cs) throws Exception {
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("table {"+"\n").getBytes());
			os.write(("  definition {"+"\n").getBytes());
		}
		String creates[] = rec.create(cs);
		for(int i = 0 ; i < creates.length ; i++) {
			os.write((creates[i]+"\n").getBytes());
		}
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("  }"+"\n").getBytes());
			os.write(assembleErrorMessage(cs).getBytes());
			os.write(("}"+"\n\n").getBytes());
		}
	}
	
	public void printModelToStream(OutputStream os) throws Exception {
		rec = RecreatorManager.getRecreator(style);
		setBarMessgage("Request table set");
		setDumpMessgage("Requesting the full set of tables in the database");
		String tables = "show tables";
		SQLResult sqlrt = conn.select_via_executeQuery(tables);
		Vector<String> tablenames = new Vector<String>();
		while(sqlrt.next()) {
			String tablename = sqlrt.getString(1);
			tablenames.add(tablename);
		}
		sqlrt.close();
		setLengthOftask(tablenames.size());
		for(int i = 0 ; i < tablenames.size() ; i++) {
			String tablename = tablenames.elementAt(i);
			setCurrent(i);
			setBarMessgage(tablename);
			setDumpMessgage("Exploring all details of table "+tablename+" which is number "+i+" of "+tablenames.size());
			CreateStatement cs = getCreateStatement(tablename);
			setDumpMessgage("Printing all details of table "+tablename);
			printStatementToStream(os, cs);
		}
		setDone(true);
		//System.out.println("MySQLModelCalculator.printModelToStream - done");
	}
	
	public static void main(String args[]) {
		String ipresolvable = "192.168.100.119";
		String port = "3306";
		String dbname = "fast2000_test";//"fast8poseidon";//"fast2000_test";
		String user = "root";
		String password = "";
		
		MySQLModelCalculator mc = null;
		try {
			mc = new MySQLModelCalculator(OmegaEmbeddedInternal.getPureMYSQLConnection(ipresolvable, port, dbname, user, password, null));
			FileOutputStream fos = new FileOutputStream("alings�sprod.out.sql");
			mc.printModelToStream(fos);
			fos.flush();
			fos.close();
		}
		catch(Exception e) {
			System.err.println("Kunde inte skapa ren MySQL-uppkoppling msg="+e.getMessage());
			e.printStackTrace();
			Throwable t = e.getCause();
			System.out.println("Underliggande "+t.getMessage());
			t.printStackTrace();
			System.exit(0);
		}
		System.out.println("OK");
	}

}
