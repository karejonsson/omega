package org.omega.nativeclientadmin.modelcalc;

import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.BigintType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntIntType;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.SmallintType;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TinyintType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.struct.VarcharType;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.resultset.SQLResult;

public class IBMDB2ModelCalculator extends ModelCalculator {
	
	private ConnectionOperations conn = null;
	private Recreator rec = null;
	
	public IBMDB2ModelCalculator(ConnectionOperations _conn) {
		conn = _conn;
	}

	private void printStatementToStream(OutputStream os, CreateStatement cs) throws Exception {
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("table {"+"\n").getBytes());
			os.write(("  definition {"+"\n").getBytes());
		}
		String creates[] = rec.create(cs);
		for(int i = 0 ; i < creates.length ; i++) {
			os.write((creates[i]+"\n").getBytes());
		}
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("  }"+"\n").getBytes());
			os.write(assembleErrorMessage(cs).getBytes());
			os.write(("}"+"\n\n").getBytes());
		}
	}
	
	public Type getType(String typedef, String length) {
		typedef = typedef.toLowerCase();
		Type out = null;
		if(typedef.startsWith("int")) {
			out = new IntIntType();
			return out;
		}
		if(typedef.startsWith("smallint")) {
			out = new SmallintType();
			return out;
		}
		if(typedef.startsWith("tinyint")) {
			out = new TinyintType();
			return out;
		}
		if(typedef.startsWith("bigint")) {
			out = new BigintType();
			return out;
		}
		if(isDatetimeVariation(typedef)) {
			out = new DatetimeType();
			return out;
		}
		if(isVarcharVariation(typedef)) {
			try {
				out = new VarcharType(Integer.parseInt(length));
			}
			catch(Exception e) {
				out = new VarcharType(0);
			}
			return out;
		}
		if(isCharVariation(typedef)) {
			try {
				out = new VarcharType(Integer.parseInt(length));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
			return out;
		}
		if(typedef.startsWith("double")) {
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DoubleType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DoubleType();
			}
			return out;
		}
		if(typedef.startsWith("decimal")) {
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DecimalType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DecimalType();
			}
		}
		if(typedef.startsWith("float")) { // FEL?
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			int idxpr = typedef.indexOf(")");
			try {
				out = new DecimalType(Integer.parseInt(typedef.substring(idxpl+1, idxc).trim()), 
								     Integer.parseInt(typedef.substring(idxc+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new DecimalType();
			}
		}
		if(typedef.startsWith("varchar")) {
			try {
				out = new VarcharType(Integer.parseInt(length));
			}
			catch(Exception e) {
				out = new VarcharType(0);
			}
			return out;
		}
		if(typedef.startsWith("char")) {
			try {
				out = new VarcharType(Integer.parseInt(length));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
			return out;
		}
		if(typedef.startsWith("text")) {
			out = new TextType();
			return out;
		}
		if(typedef.startsWith("tinytext")) {
			out = new TinytextType();
			return out;
		}
		if(out == null) {
			//System.out.println("Ingen typ f�r "+typedef);
			out = new UnmanageableType(typedef);
		}
		return out;
	}


	private CreateStatement getCreateStatement(String tablename) throws Exception {
		CreateStatement out = new CreateStatement();
		out.setName(getValidObjectName(tablename));
		String columns_sql = "select name, coltype, length, identity, nulls from sysibm.syscolumns where tbname = '"+tablename+"'";
		SQLResult sqlrt = conn.select_via_executeQuery(columns_sql);
		
		Vector<String> foreigns = new Vector<String>();
		
		while(sqlrt.next()) {
			Column col = new Column();
			String name = verifyColumnNamesValidity(sqlrt.getString(1), col);
			col.setName(name);
			String typedef = sqlrt.getString(2);
			String length = sqlrt.getString(3);
			//String default_ = sqlrt.getString(5);
			String identity = sqlrt.getString(4);
			String nulls = sqlrt.getString(5);
			Type type = getType(typedef, length);
			if(nulls.toLowerCase().contains("n")) {
				type.setNotNull();
			}

			col.setType(type);
			if(identity.toLowerCase().contains("y")) {
				col.setAutoIncrement();
			}
			out.addColumn(col);
		}
		sqlrt.close();

		String primaryKeySql = 
				"SELECT NAME "+
				"FROM SYSIBM.SYSCOLUMNS "+
				"WHERE TBNAME = '"+tablename+"' "+
				"AND KEYSEQ > 0 "+
				"ORDER BY KEYSEQ ASC ";

		sqlrt = conn.select_via_executeQuery(primaryKeySql);

		PrimaryKeyDeclaration pkd = new PrimaryKeyDeclaration(tablename);
		
		while(sqlrt.next()) {
			String membername = sqlrt.getString(1);
			pkd.addMember(membername);
		}
		if(pkd.getNoMembers() != 0) {
			out.setPrimaryKeyDeclaration(pkd);
		}
		sqlrt.close();

		String foreignKeySql = 
				"SELECT "+
		        "   pkcolnames, "+
				"   fkcolnames, "+
				"   relname, "+
				"   reftbname "+
	            " FROM "+
				"   sysibm.sysrels "+
				" WHERE "+
				"   TBNAME = '"+tablename+"' "+
				"";
		
		Hashtable<String, ForeignKeyDeclaration> fkdl = new Hashtable<String, ForeignKeyDeclaration>();
		
		sqlrt = conn.select_via_executeQuery(foreignKeySql);
		while(sqlrt.next()) {
			String constraintName = sqlrt.getString(3).trim();
			String closeMember = sqlrt.getString(2).trim();
			String remoteMember = sqlrt.getString(1).trim();
			String remoteTable = sqlrt.getString(4).trim();
				
			ForeignKeyDeclaration fkd = fkdl.get(constraintName);
			if(fkd == null) {
				fkd = new ForeignKeyDeclaration(constraintName);
				fkdl.put(constraintName, fkd);
				fkd.setReferringTablename(tablename);
				fkd.setReferencedTablename(remoteTable);
			}
			fkd.addReferringMember(closeMember);					
			fkd.addReferredTableMember(remoteMember); 
		}
		sqlrt.close();
		
		for(String key : fkdl.keySet()) {
			ForeignKeyDeclaration fkd = fkdl.get(key);
			out.addForeignKeyDeclarationForgiving(fkd);
		}

		return out;
	}

	public void printModelToStream(OutputStream os) throws Exception {
		rec = RecreatorManager.getRecreator(style);
		setBarMessgage("Request table set");
		setDumpMessgage("Requesting the full set of tables in the database");
		String tables = "select name from sysibm.systables where type = 'T' and creator = '"+conn.getUser().toUpperCase()+"'";
		SQLResult sqlrt = conn.select_via_executeQuery(tables);
		Vector<String> tablenames = new Vector<String>();
		while(sqlrt.next()) {
			String tablename = sqlrt.getString(1);
			//System.out.println("Table "+tablename);
			tablenames.add(tablename);
		}
		sqlrt.close();
		//tablenames.addElement("OBJECTMODEL".toUpperCase());
		setLengthOftask(tablenames.size());
		for(int i = 0 ; i < tablenames.size() ; i++) {
			String tablename = tablenames.elementAt(i);
			setCurrent(i);
			setBarMessgage(tablename);
			setDumpMessgage("Exploring all details of table "+tablename+" which is number "+i+" of "+tablenames.size());
			CreateStatement cs = getCreateStatement(tablename);
			setDumpMessgage("Printing all details of table "+tablename);
			printStatementToStream(os, cs);
		}
		setDone(true);
	}

	public static void main(String args[]) {
		String ipresolvable = "192.168.1.86";
		String port = "50000";
		String dbname = "sample";//"fast8poseidon";//"fast2000_test";
		String user = "db2admin";
		String password = "qwerty";
		
		IBMDB2ModelCalculator mc = null;
		try {
			ConnectionOperations conn = OmegaEmbeddedInternal.getPureIBMDB2Connection(ipresolvable, port, dbname, user, password, null);
			mc = new IBMDB2ModelCalculator(conn);
			mc.setOutstyle("MySQL", ".gdbapp");
			//FileOutputStream fos = new FileOutputStream("XE.out.sql");
			mc.printModelToStream(System.out);
			//fos.flush();
			//fos.close();
			/*
			String sql = "select * from all_tab_columns where table_name  = '"+"contractedobjectsaddition".toUpperCase()+"' and column_name = 'FINALPRICE' ";
			SQLResult sqlrt = conn.select_via_executeQuery(sql);
			SQLResultMetaData rmd = sqlrt.getMetaData();
			final int colcount = rmd.getColumnCount();
			for(int i = 1 ; i < colcount ; i++) {
				//System.out.println("Kolumn "+i+" = "+rmd.getColumnName(i));
			}
			while(sqlrt.next()) {
				for(int i = 1 ; i < colcount ; i++) {
					System.out.println("Nr "+i+" = "+sqlrt.getObject(i)+", namn "+rmd.getColumnName(i));
				}
			}
			sqlrt.close();
			*/
		}
		catch(Exception e) {
			System.err.println("Kunde inte skapa ren IBMDB2-uppkoppling msg="+e.getMessage());
			e.printStackTrace();
			Throwable t = e.getCause();
			System.out.println("Underliggande "+t.getMessage());
			t.printStackTrace();
			System.exit(0);
		}
		System.out.println("OK");
	}

}
