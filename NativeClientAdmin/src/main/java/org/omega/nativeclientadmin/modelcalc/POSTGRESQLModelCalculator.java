package org.omega.nativeclientadmin.modelcalc;

import java.io.OutputStream;

import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.nativeclientadmin.modelcalc.ModelCalculator;

public class POSTGRESQLModelCalculator extends ModelCalculator {
	
	private ConnectionOperations conn = null;
	
	public POSTGRESQLModelCalculator(ConnectionOperations _conn) {
		conn = _conn;
	}

	public void printModelToStream(OutputStream os) throws Exception {
	}

	public void setOutstyle(String style, String extension) {
	}

}
