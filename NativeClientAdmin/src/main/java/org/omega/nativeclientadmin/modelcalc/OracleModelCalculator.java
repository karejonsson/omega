package org.omega.nativeclientadmin.modelcalc;

import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.BigintType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntIntType;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.SmallintType;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TinyintType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.struct.VarcharType;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.recreatesql.OracleRecreator;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.resultset.SQLResult;

public class OracleModelCalculator extends ModelCalculator {
	
	private ConnectionOperations conn = null;
	private Recreator rec = null;
	
	public OracleModelCalculator(ConnectionOperations _conn) throws SQLException {
		conn = _conn;
	}

	public Type getType(String typedef) {
		//System.out.println("OracleModelCalculator.getType("+typedef+")");
		typedef = typedef.toLowerCase();
		Type out = null;
		if(typedef.startsWith("number")) {
			int idxpl = typedef.indexOf("(");
			int idxc = typedef.indexOf(",");
			String precisionPart = typedef.substring(idxpl+1, idxc).trim();
			int idxpr = typedef.indexOf(")");
			String scalePart = typedef.substring(idxc+1, idxpr).trim();
			double pre = -1;
			double sca = -1;
			try {
				pre = Double.parseDouble(precisionPart);
			}
			catch(Exception e) {
				pre = 8.0;
			}
			try {
				sca = Double.parseDouble(scalePart);
			}
			catch(Exception e) {
			}
			if(scalePart.toLowerCase().compareTo("null") == 0 || !scalePart.contains(".")) {
				if(sca > 0) {
					// Double
					if(pre == -1) {
						out = new DoubleType(8, (int) Math.round(sca));
					}
					else {
						out = new DoubleType((int) Math.round(pre), (int) Math.round(sca));
					}
					return out;
				}
				// Int
				if(pre > 15.9) {
					out = new BigintType(10);
				}
				else if(pre > 7.9) {
					out = new IntIntType(10);
				}
				else if(pre > 5.9) {
					out = new SmallintType(6);
				}
				else {
					out = new TinyintType(4);
				}
				return out;
			}
			else {
				// Double
				if(pre == -1) {
					out = new DoubleType(8, (int) Math.round(sca));
				}
				else {
					out = new DoubleType((int) Math.round(pre), (int) Math.round(sca));
				}
				return out;
			}
		}
		if(isDatetimeVariation(typedef)) {
			out = new DatetimeType();
		}
		if(isVarcharVariation(typedef)) {
			//System.out.println("OracleModelCalculator.getType - isVarcharVariation "+typedef);
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(0);
			}
			return out;
		}
		else {
			//System.out.println("OracleModelCalculator.getType - isVarcharVariation FALSE : "+typedef);
		}
		if(isCharVariation(typedef)) {
			int idxpl = typedef.indexOf("(");
			int idxpr = typedef.indexOf(")");
			try {
				out = new VarcharType(Integer.parseInt(typedef.substring(idxpl+1, idxpr).trim()));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
			return out;
		}
		if(typedef.startsWith("text")) {
			out = new TextType();
			return out;
		}
		if(typedef.startsWith("tinytext")) {
			out = new TinytextType();
			return out;
		}
		if(isUnmanageable(typedef)) {
			out = new UnmanageableType(typedef);
		}
		if(out == null) {
			//System.out.println("Ingen typ f�r "+typedef);
			out = new UnmanageableType(typedef);
		}
		return out;
	}
	
	private CreateStatement getCreateStatement(String tablename) throws Exception {
		//System.out.println("Tabellnamn "+tablename);
		CreateStatement out = new CreateStatement();
		out.setName(getValidObjectName(tablename));
		//String columns_sql = "desc '"+tablename+"';";
		String columns_sql = "select column_name, data_type, data_length, nullable, data_default, data_precision, data_scale from all_tab_columns where table_name = '"+tablename+"'";
		//System.out.println("Column SQL: "+columns_sql);
		SQLResult sqlrt = conn.select_via_executeQuery(columns_sql);
		OracleRecreator orec = new OracleRecreator(); 

		while(sqlrt.next()) {
			Column col = new Column();
			String name = verifyColumnNamesValidity(sqlrt.getString(1), col);
			col.setName(name);
			//System.out.println("Name "+name);
			String data_type = sqlrt.getString(2);
			//System.out.println("data_type "+data_type);
			String data_length = sqlrt.getString(3);
			//System.out.println("data_length "+data_length);
			String nullable = sqlrt.getString(4);
			String data_default = sqlrt.getString(5);
			//System.out.println("data_default "+data_default);
			String data_precision = sqlrt.getString(6);
			String data_scale = sqlrt.getString(7);
			//System.out.println("data_precision="+data_precision+", data_scale="+data_scale);
			String typeCoded = null;
			if(data_type.toUpperCase().startsWith("NUMBER")) {
				typeCoded = data_type+"("+data_precision+", "+data_scale+")";
			}
			else {
				typeCoded = data_type+"("+data_length+")";
			}
			Type type = getType(typeCoded);
			//System.out.println("type "+type);
			if(nullable.toLowerCase().compareTo("n") == 0) {
				type.setNotNull();
			}
			if(data_default == null) {
				//System.out.println("data_default "+data_default+" = null");
				type.setNullDefault();
			}
			else if(data_default.trim().length() != 0) {
				type.setUntypedDefault(rec, data_default.trim());
			}
			col.setType(type);
			out.addColumn(col);
		}
		sqlrt.close();

		String primaryKeySql = 
				"SELECT cols.column_name, cons.generated "+
				"FROM all_constraints cons, all_cons_columns cols "+
				"WHERE cols.table_name = '"+tablename+"' "+
				"AND cons.constraint_type = 'P' "+
				"AND cons.constraint_name = cols.constraint_name "+
				"AND cons.owner = cols.owner "+
				"ORDER BY cols.table_name, cols.position ";

		sqlrt = conn.select_via_executeQuery(primaryKeySql);

		PrimaryKeyDeclaration pkd = new PrimaryKeyDeclaration(tablename);
		
		while(sqlrt.next()) {
			String membername = sqlrt.getString(1);
			pkd.addMember(membername);
			String generated = sqlrt.getString(2).toUpperCase();
			if(generated.contains("GENER")) {
				Column col = out.getColumn(membername); {
					col.setAutoIncrement();
				}
			}
		}
		if(pkd.getNoMembers() != 0) {
			out.setPrimaryKeyDeclaration(pkd);
		}
		sqlrt.close();
		
		String foreignKeySql = 
				"SELECT "+
		        "   a.table_name child_table, "+
				"   b.table_name parent_table, "+
				"   a.constraint_name child_constraint, "+
				"   c.column_name child_column, "+
				"   d.column_name parent_column "+
	            " FROM "+
				"   user_constraints a, "+
				"	user_constraints b, "+
				"	user_cons_columns c, "+
				"   user_cons_columns d "+
				" WHERE "+
				"       a.constraint_type = 'R' "+
				"   AND b.constraint_type = 'P' "+
				"   AND b.constraint_name = a.r_constraint_name "+
				"   AND A.table_name = c.table_name "+
				"   AND b.table_name = d.table_name "+
				"   AND a.constraint_name = c.constraint_name "+
				"   AND b.constraint_name = d.constraint_name "+
				"   AND c.position = d.position "+
				"   AND a.table_name = '"+tablename+"' "+
				"";
		
		Hashtable<String, ForeignKeyDeclaration> fkdl = new Hashtable<String, ForeignKeyDeclaration>();
		
		sqlrt = conn.select_via_executeQuery(foreignKeySql);
		while(sqlrt.next()) {
			String thisTable = sqlrt.getString(1);
			String remoteTable = sqlrt.getString(2);
			String constraintName = sqlrt.getString(3);
			String closeMember = sqlrt.getString(4);
			String remoteMember = sqlrt.getString(5);
				
			ForeignKeyDeclaration fkd = fkdl.get(constraintName);
			if(fkd == null) {
				fkd = new ForeignKeyDeclaration(constraintName);
				fkdl.put(constraintName, fkd);
				fkd.setReferringTablename(tablename);
				fkd.setReferencedTablename(remoteTable);
			}
			fkd.addReferringMember(closeMember);					
			fkd.addReferredTableMember(remoteMember); 
		}
		sqlrt.close();
		
		for(String key : fkdl.keySet()) {
			ForeignKeyDeclaration fkd = fkdl.get(key);
			out.addForeignKeyDeclarationForgiving(fkd);
		}
		return out;
	}
	
	private void printStatementToStream(OutputStream os, CreateStatement cs) throws Exception {
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("table {"+"\n").getBytes());
			os.write(("  definition {"+"\n").getBytes());
		}
		String creates[] = rec.create(cs);
		for(int i = 0 ; i < creates.length ; i++) {
			os.write((creates[i]+"\n").getBytes());
		}
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("  }"+"\n").getBytes());
			os.write(assembleErrorMessage(cs).getBytes());
			os.write(("}"+"\n\n").getBytes());
		}
	}
	
	public void printModelToStream(OutputStream os) throws Exception {
		rec = RecreatorManager.getRecreator(style);
		setBarMessgage("Request table set");
		setDumpMessgage("Requesting the full set of tables in the database");
		String tables = "select table_name from tabs";
		SQLResult sqlrt = conn.select_via_executeQuery(tables);
		Vector<String> tablenames = new Vector<String>();
		while(sqlrt.next()) {
			String tablename = sqlrt.getString(1);
			//System.out.println("Table "+tablename);
			tablenames.add(tablename);
		}
		sqlrt.close();
		//tablenames.addElement("OBJECTMODEL".toUpperCase());
		setLengthOftask(tablenames.size());
		for(int i = 0 ; i < tablenames.size() ; i++) {
			String tablename = tablenames.elementAt(i);
			setCurrent(i);
			setBarMessgage(tablename);
			setDumpMessgage("Exploring all details of table "+tablename+" which is number "+i+" of "+tablenames.size());
			CreateStatement cs = getCreateStatement(tablename);
			setDumpMessgage("Printing all details of table "+tablename);
			printStatementToStream(os, cs);
		}
		setDone(true);
	}
	
	public static void main(String args[]) {
		String ipresolvable = "192.168.1.86";
		String port = "1521";
		String dbname = "XE";//"fast8poseidon";//"fast2000_test";
		String user = "system";
		String password = "pwdoracle";
		
		OracleModelCalculator mc = null;
		try {
			ConnectionOperations conn = OmegaEmbeddedInternal.getPureOracleConnection(ipresolvable, port, dbname, user, password, null);
			mc = new OracleModelCalculator(conn);
			mc.setOutstyle("Oracle", ".gdbapp");
			//FileOutputStream fos = new FileOutputStream("XE.out.sql");
			mc.printModelToStream(System.out);
			//fos.flush();
			//fos.close();
			/*
			String sql = "select * from all_tab_columns where table_name  = '"+"contractedobjectsaddition".toUpperCase()+"' and column_name = 'FINALPRICE' ";
			SQLResult sqlrt = conn.select_via_executeQuery(sql);
			SQLResultMetaData rmd = sqlrt.getMetaData();
			final int colcount = rmd.getColumnCount();
			for(int i = 1 ; i < colcount ; i++) {
				//System.out.println("Kolumn "+i+" = "+rmd.getColumnName(i));
			}
			while(sqlrt.next()) {
				for(int i = 1 ; i < colcount ; i++) {
					System.out.println("Nr "+i+" = "+sqlrt.getObject(i)+", namn "+rmd.getColumnName(i));
				}
			}
			sqlrt.close();
			*/
		}
		catch(Exception e) {
			System.err.println("Kunde inte skapa ren Oracle-uppkoppling msg="+e.getMessage());
			e.printStackTrace();
			Throwable t = e.getCause();
			System.out.println("Underliggande "+t.getMessage());
			t.printStackTrace();
			System.exit(0);
		}
		System.out.println("OK");
	}

}
