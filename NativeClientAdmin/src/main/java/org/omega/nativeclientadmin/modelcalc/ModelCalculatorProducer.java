package org.omega.nativeclientadmin.modelcalc;

public interface ModelCalculatorProducer {
	
	public ModelCalculator getModelCalculator() throws Exception;
	
}
