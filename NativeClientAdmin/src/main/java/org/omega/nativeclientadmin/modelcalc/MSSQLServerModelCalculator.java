package org.omega.nativeclientadmin.modelcalc;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JFrame;

import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.BigintType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntIntType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.SmallintType;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TinyintType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.struct.VarcharType;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.resultset.SQLResult;

import se.modlab.generics.gui.progress.ProgressDumpAndMonitor;

public class MSSQLServerModelCalculator extends ModelCalculator {
	
	private ConnectionOperations conn = null;
	private Hashtable<String, String> ht = new Hashtable<String, String>();
	private Recreator rec = null;
	
	public MSSQLServerModelCalculator(ConnectionOperations _conn) throws SQLException {
		conn = _conn;
	}
	
	public Type getType(String typename, String precision, String maxlength) {
		Type out = null;
		if(typename.toLowerCase().compareTo("char") == 0) {
			try {
				out = new VarcharType(Integer.parseInt(maxlength));
			}
			catch(Exception e) {
				out = new VarcharType(1);
			}
		}
		if(typename.toLowerCase().compareTo("varchar") == 0) {
			try {
				out = new VarcharType(Integer.parseInt(maxlength));
			}
			catch(Exception e) {
			}
		}
		if(typename.toLowerCase().compareTo("nvarchar") == 0) {
			try {
				out = new VarcharType(Integer.parseInt(maxlength));
			}
			catch(Exception e) {
			}
		}
		if(typename.toLowerCase().startsWith("int")) {
			try {
				out = new IntIntType(Integer.parseInt(precision));
			}
			catch(Exception e) {
				out = new IntIntType();
			}
		}
		if(typename.toLowerCase().startsWith("smallint")) {
			try {
				out = new SmallintType(Integer.parseInt(precision));
			}
			catch(Exception e) {
				out = new SmallintType();
			}
		}
		if(typename.toLowerCase().startsWith("tinyint")) {
			try {
				out = new TinyintType(Integer.parseInt(precision));
			}
			catch(Exception e) {
				out = new TinyintType();
			}
		}
		if(typename.toLowerCase().startsWith("bigint")) {
			try {
				out = new BigintType(Integer.parseInt(precision));
			}
			catch(Exception e) {
				out = new BigintType();
			}
		}
		if(typename.toLowerCase().startsWith("numeric")) {
			out = new UnmanageableType("numeric");
		}
		if(typename.toLowerCase().startsWith("ntext")) {
			out = new TextType();
		}
		if(typename.toLowerCase().startsWith("text")) {
			out = new TextType();
		}
		if(typename.toLowerCase().compareTo("date") == 0) {
			out = new UnmanageableType("date");
		}
		if(typename.toLowerCase().compareTo("longtext") == 0) {
			out = new UnmanageableType("longtext");
		}
		if(typename.toLowerCase().compareTo("mediumblob") == 0) {
			out = new UnmanageableType("mediumblob");
		}
		if(typename.toLowerCase().compareTo("mediumtext") == 0) {
			out = new UnmanageableType("mediumtext");
		}
		if(typename.toLowerCase().compareTo("nchar") == 0) {
			out = new UnmanageableType("nchar");
		}
		if(typename.toLowerCase().compareTo("datetime2") == 0) {
			out = new UnmanageableType("datetime2");
		}
		if(typename.toLowerCase().compareTo("time") == 0) {
			out = new UnmanageableType("time");
		}
		if(typename.toLowerCase().compareTo("datetime") == 0) {
			out = new DatetimeType();
		}
		if(typename.toLowerCase().compareTo("double") == 0) {
			out = new DoubleType();
		}
		if(typename.toLowerCase().compareTo("decimal") == 0) {
			out = new DecimalType();
		}
		if(typename.toLowerCase().compareTo("float") == 0) {
			out = new UnmanageableType("float");
		}
		if(out == null) {
			//System.out.println("No type for typename "+typename+", precision "+precision);
			out = new UnmanageableType(typename);
		}
		return out;
	}
	
	private String getFirstOnlyFromSQL(String sql) throws SQLException {
		SQLResult sqlrt = conn.select_via_executeQuery(sql);
		sqlrt.next();
		String firstOnly = sqlrt.getString(1);
		sqlrt.close();
		return firstOnly;
	}
	
	private CreateStatement getCreateStatement(String tablename) throws Exception {
		//System.out.println("Tabellnamn "+tablename);
		CreateStatement out = new CreateStatement();
		out.setName(getValidObjectName(tablename));
		String table_object_id_sql = "select sys.tables.object_id from sys.tables where sys.tables.name = '"+tablename+"'";
		SQLResult sqlrt = conn.select_via_executeQuery(table_object_id_sql);
		sqlrt.next();
		int table_object_id = sqlrt.getInt(1);
		sqlrt.close();
		String columns_sql = "select sys.columns.name, sys.columns.user_type_id, sys.columns.is_nullable, sys.columns.is_identity, sys.columns.precision, sys.columns.column_id, sys.columns.max_length from sys.columns where sys.columns.object_id = "+table_object_id;
		//System.out.println("SQL="+columns_sql);
		sqlrt = conn.select_via_executeQuery(columns_sql);
		
		Hashtable<String, String> cm = new Hashtable<String, String>();

		while(sqlrt.next()) {
			Column col = new Column();
			String name = verifyColumnNamesValidity(sqlrt.getString(1), col);
			col.setName(name);
			String user_type_id = sqlrt.getString(2);
			String typename = ht.get(user_type_id);
			String precision = sqlrt.getString(5);
			String maxlength = sqlrt.getString(7);
			String isnullable = sqlrt.getString(3);
			String isidentity = sqlrt.getString(4);
			String column = sqlrt.getString(6);
			cm.put(column, name);
			Type type = getType(typename, precision, maxlength);
			if(!isnullable.contains("1")) {
				type.setNotNull();
			}
			col.setType(type);
			if(isidentity.contains("1")) {
				col.setAutoIncrement();
			}
			out.addColumn(col);
		}
		sqlrt.close();
		String foreignKey_sql = "select sys.foreign_key_columns.constraint_object_id, sys.foreign_key_columns.referenced_object_id, sys.foreign_key_columns.referenced_column_id, sys.foreign_key_columns.parent_column_id from sys.foreign_key_columns where sys.foreign_key_columns.parent_object_id = "+table_object_id;
		sqlrt = conn.select_via_executeQuery(foreignKey_sql);
		while(sqlrt.next()) {
			String constraint_object_id = sqlrt.getString(1);
			String referenced_object_id = sqlrt.getString(2);
			String referenced_column_id = sqlrt.getString(3);
			String parent_column_id = sqlrt.getString(4);
			String columnname = cm.get(parent_column_id);
			String constraintname_sql = "select sys.objects.name from sys.objects where sys.objects.object_id = "+constraint_object_id;
			String constraintname = getFirstOnlyFromSQL(constraintname_sql);
			String referredTable_sql = "select sys.objects.name from sys.objects where sys.objects.object_id = "+referenced_object_id;
			String referredTable = getFirstOnlyFromSQL(referredTable_sql);
			String referred_column_name_sql = "select sys.columns.name from sys.columns where sys.columns.object_id = "+referenced_object_id+" and sys.columns.column_id = "+referenced_column_id;
			String referred_column_name = getFirstOnlyFromSQL(referred_column_name_sql);
			ForeignKeyDeclaration fkd = new ForeignKeyDeclaration(constraintname);
			fkd.setReferringTablename(tablename);
			fkd.setReferencedTablename(referredTable);
			fkd.addReferringMember(columnname);
			fkd.addReferredTableMember(referred_column_name);
			out.addForeignKeyDeclarationForgiving(fkd);
		}
		sqlrt.close();
		String primaryAndUnique_sql = 
				"select kcu.TABLE_SCHEMA, kcu.TABLE_NAME, kcu.CONSTRAINT_NAME, tc.CONSTRAINT_TYPE, kcu.COLUMN_NAME, kcu.ORDINAL_POSITION \n"+
				"  from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc \n"+
				"  join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu \n"+
				"    on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA \n"+
				"   and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME \n"+
				"   and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA \n"+
				"   and kcu.TABLE_NAME = tc.TABLE_NAME \n"+
				" where tc.CONSTRAINT_TYPE in ( 'PRIMARY KEY', 'UNIQUE' ) and tc.TABLE_NAME = '"+tablename+"'\n"+
				" order by kcu.TABLE_SCHEMA, kcu.TABLE_NAME, tc.CONSTRAINT_TYPE, kcu.CONSTRAINT_NAME, kcu.ORDINAL_POSITION;";
		sqlrt = conn.select_via_executeQuery(primaryAndUnique_sql);
		PrimaryKeyDeclaration pkd = new PrimaryKeyDeclaration(tablename);
		KeyDeclaration kd = new KeyDeclaration();
		while(sqlrt.next()) {
			String type = sqlrt.getString(4);
			String field = sqlrt.getString(5);
			if(type.toLowerCase().contains("primary")) {
				pkd.addMember(field);
			}
			if(type.toLowerCase().contains("uniq")) {
				kd.addMember(field);
			}
		}
		if(pkd.getNoMembers() != 0) {
			out.setPrimaryKeyDeclaration(pkd);
		}
		if(kd.getNoMembers() != 0) {
			out.addKey(kd);
		}
		sqlrt.close();
		return out;
	}
	
	private void printStatementToStream(OutputStream os, CreateStatement cs) throws Exception {
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("table {"+"\n").getBytes());
			os.write(("  definition {"+"\n").getBytes());
		}
		String creaters[] = rec.create(cs);
		for(int i = 0 ; i < creaters.length ; i++) {
			os.write((creaters[i]+"\n").getBytes());
		}
		if(extension.toLowerCase().contains("gdbapp")) {
			os.write(("  }"+"\n").getBytes());
			os.write(assembleErrorMessage(cs).getBytes());
			os.write(("}"+"\n\n").getBytes());
		}
	}
	
	public void printModelToStream(OutputStream os) throws Exception {
		//setTaskMessgage("Task 1");
		setBarMessgage("Initiating types");
		setDumpMessgage("Initiating the typenames of database");
		setLengthOftask(100);
		setCurrent(50);
		String types_sql = "select sys.types.name, sys.types.user_type_id from sys.types";
		SQLResult sqlrt = conn.select_via_executeQuery(types_sql);
		while(sqlrt.next()) {
			ht.put(sqlrt.getString(2), sqlrt.getString(1));
		}
		sqlrt.close();
		rec = RecreatorManager.getRecreator(style);
		//setTaskMessgage("Task 2");
		setBarMessgage("Request table set");
		setDumpMessgage("Requesting the full set of tables in the database");
		String tables = "select name from sys.tables order by name";
		sqlrt = conn.select_via_executeQuery(tables);
		int ctr = 0;
		Vector<String> tablenames = new Vector<String>();
		while(sqlrt.next()) {
			String tablename = sqlrt.getString(1);
			tablenames.add(tablename);
		}
		sqlrt.close();
		setLengthOftask(tablenames.size());
		for(int i = 0 ; i < tablenames.size() ; i++) {
			String tablename = tablenames.elementAt(i);
			setCurrent(i);
			//setTaskMessgage("Task 3");
			setBarMessgage(tablename);
			setDumpMessgage("Exploring all details of table "+tablename+" which is number "+i+" of "+tablenames.size());
			CreateStatement cs = getCreateStatement(tablename);
			//setTaskMessgage("Task 4");
			//setBarMessgage("Bar 4");
			setDumpMessgage("Printing all details of table "+tablename);
			printStatementToStream(os, cs);
			ctr++;
			if(ctr > 2) {
				//break;
			}
			Thread.currentThread().sleep(20);
		}
		setDone(true);
	}
	
	public static void main(String args[]) {
		String ipresolvable = "172.16.99.136";
		String port = "1433";
		String dbname = "FAST2000_sssb_01";
		String user = "SA";
		String password = "server";
		
		MSSQLServerModelCalculator mc = null;
		try {
			mc = new MSSQLServerModelCalculator(OmegaEmbeddedInternal.getPureMSSQLServerConnection(ipresolvable, port, dbname, user, password, null));
			mc.setOutstyle("MYSQL", ".gdbapp");
		    ProgressDumpAndMonitor pmd = ProgressDumpAndMonitor.createGUI(mc); 
		    JFrame frame = pmd.getFrame();
		    System.out.println("Frame "+frame);
		    frame.setVisible(true);
		    pmd.monitoredWorkStarted();

			FileOutputStream fos = new FileOutputStream("sssb.out.sql");
			mc.printModelToStream(fos);
			fos.flush();
			fos.close();
		}
		catch(Exception e) {
			System.err.println("Kunde inte skapa ren MSSQL2008-uppkoppling");
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("OK");

	}

}
