package org.omega.nativeclientadmin.dialogs;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.omega.executablescript.struct.CombinedFileStruct;

import se.modlab.generics.gui.find.StringGridBagLayout;

public class ChoseFlowToExecuteDialog extends JDialog {
	
	private ChoseFlowToExecutePanel cont = null;

	public ChoseFlowToExecuteDialog(String title, CombinedFileStruct cfs) throws IOException {
		cont = new ChoseFlowToExecutePanel(cfs);
		final JScrollPane view = new JScrollPane();
		ChoseFlowToExecutePanel jp = new ChoseFlowToExecutePanel(cfs);
		view.getViewport().add(jp);
		add(view);

		setTitle(title);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		try { 
			setContentPane(view);
			pack();
		} 
		catch(Throwable t) {
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}

}
