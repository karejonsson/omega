package org.omega.nativeclientadmin.dialogs;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

import org.omega.nativeclientadmin.modelcalc.ModelCalculator;

public class ModelCalcDialog extends JDialog {
	
	private ModelCalcPanel cont = null;
 
	public ModelCalcDialog(String title) {
		cont = new ModelCalcPanel();
		setTitle(title);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		try { 
			setContentPane(cont);
			pack();
			Dimension d = getSize();
			d.width = 700;
			d.height = 400;
			setSize(d);
			setVisible(true);
		} 
		catch(Throwable t) {
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}
	
	public ModelCalculator getModelCalculator() throws Exception {
		return cont.getModelCalculator();
	}

	public static void main(String args[]) {
		new ModelCalcDialog("Titel");
	}
	
}

