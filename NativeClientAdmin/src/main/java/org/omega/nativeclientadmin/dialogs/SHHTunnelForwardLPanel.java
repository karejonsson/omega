package org.omega.nativeclientadmin.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.omega.connectivity.properties.InstallationProperties;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

import se.modlab.generics.gui.find.StringGridBagLayout;

public class SHHTunnelForwardLPanel extends JPanel {
	
    public static void main(String[] args) {
 
    	SHHTunnelForwardLPanel cp = new SHHTunnelForwardLPanel();
 
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new JScrollPane(cp));
        f.setSize(360, 400);
        f.setLocation(200, 100);
        f.setVisible(true);
    }
    
    private JTextField userfield = new JTextField(InstallationProperties.getString(InstallationProperties.sshtunnel_username, ""));
    private JTextField domainfield = new JTextField(InstallationProperties.getString(InstallationProperties.sshtunnel_ipresolvable, "127.0.0.1"));
    private JPasswordField pwdfield = new JPasswordField();
    private JTextField localportfield = new JTextField(InstallationProperties.getString(InstallationProperties.sshtunnel_localport, ""));
    private JTextField remoteDomainfield = new JTextField(InstallationProperties.getString(InstallationProperties.sshtunnel_remoteipresolvable, ""));
    private JTextField remotePortfield = new JTextField(InstallationProperties.getString(InstallationProperties.sshtunnel_remoteport, ""));
    private JTextField sshPortfield = new JTextField(InstallationProperties.getString(InstallationProperties.ssh_port, "22"));
    
    public TunnelForwardLParameters getParameters() {
    	if(!hiddenPanel.isShowing()) {
    		return null;
    	}
    	TunnelForwardLParameters out = new TunnelForwardLParameters();
    	out.login_user = userfield.getText();
    	out.login_domain = domainfield.getText();
    	out.login_password = new String(pwdfield.getPassword());
    	out.tunnel_lport = Integer.parseInt(localportfield.getText());
    	out.tunnel_rhost = remoteDomainfield.getText();
    	out.tunnel_rport = Integer.parseInt(remotePortfield.getText());
    	out.ssh_port = Integer.parseInt(sshPortfield.getText());
    	return out;
    }
 
    public void buildPanel() {
        hiddenPanel = new JPanel(new StringGridBagLayout());
        hiddenPanel.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", new JLabel("User"));
        hiddenPanel.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", userfield);
        hiddenPanel.add("gridx=0,gridy=1,anchor=WEST,insets=[12,12,0,0]", new JLabel("Domain"));
        hiddenPanel.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", domainfield);
        hiddenPanel.add("gridx=0,gridy=2,anchor=WEST,insets=[12,12,0,0]", new JLabel("Password"));
        hiddenPanel.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", pwdfield);
        hiddenPanel.add("gridx=0,gridy=3,anchor=WEST,insets=[12,12,0,0]", new JLabel("Local port"));
        hiddenPanel.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", localportfield);
        hiddenPanel.add("gridx=0,gridy=4,anchor=WEST,insets=[12,12,0,0]", new JLabel("Remote domain"));
        hiddenPanel.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", remoteDomainfield);
        hiddenPanel.add("gridx=0,gridy=5,anchor=WEST,insets=[12,12,0,0]", new JLabel("Remote port"));
        hiddenPanel.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", remotePortfield);
        hiddenPanel.add("gridx=0,gridy=6,anchor=WEST,insets=[12,12,0,0]", new JLabel("SSH port"));
        hiddenPanel.add("gridx=1,gridy=6,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", sshPortfield);
    }

    private boolean selected;
    private JPanel hiddenPanel;
    private HeaderPanel visual;
 
    private class HeaderPanel extends JPanel implements MouseListener {
    	private String collapsed;
    	private String expanded;
    	private String current;
    	private Font font;
    	private final int OFFSET = 30, PAD = 5;
 
        public HeaderPanel(String collapsed, String expanded) {
            addMouseListener(this);
            this.collapsed = collapsed;
            current = collapsed;
            this.expanded = expanded;
            font = new Font("sans-serif", Font.PLAIN, 12);
            setPreferredSize(new Dimension(200, 20));
        }

        private float x = 0;
        private float y = 0;
        private Graphics2D g2 = null;
        
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            int h = getHeight();
            g2.setFont(font);
            FontRenderContext frc = g2.getFontRenderContext();
            LineMetrics lm = font.getLineMetrics(collapsed, frc);
            float height = lm.getAscent() + lm.getDescent();
            x = OFFSET;
            y = (h + height) / 2 - lm.getDescent();
            setText();
        }
        
        private void setText() {
            g2.drawString(current, x, y);        	
        }
 
        public void mouseClicked(MouseEvent e) {
            toggleSelection();
        }
 
        public void mouseEntered(MouseEvent e) {
        }
 
        public void mouseExited(MouseEvent e) {
        }
 
        public void mousePressed(MouseEvent e) {
        }
 
        public void mouseReleased(MouseEvent e) {
        }

		public void setCollapsedText() {
			current = collapsed;
		}

		public void setExpandedText() {
			current = expanded;
		}
 
    }
    
    public SHHTunnelForwardLPanel() {
    	this("Tunnel not used. Expand to combine with SSH tunnel.", "Tunnel used. Collapse to not combine with SSH tunnel.");
    }
    
    public SHHTunnelForwardLPanel(String collapsed, String expanded) {
        super(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(1, 3, 0, 3);
        gbc.weightx = 1.0;
        gbc.fill = gbc.HORIZONTAL;
        gbc.gridwidth = gbc.REMAINDER;
 
        selected = false;
        visual = new HeaderPanel(collapsed, expanded);
 
        setBackground(new Color(200, 200, 220));
        buildPanel();
 
        add(visual, gbc);
        add(hiddenPanel, gbc);
        hiddenPanel.setVisible(false);
 
        JLabel padding = new JLabel();
        gbc.weighty = 1.0;
        add(padding, gbc);
    }
 
    public void toggleSelection() {
        selected = !selected;
 
        if(hiddenPanel.isShowing()) {
            hiddenPanel.setVisible(false);
            visual.setCollapsedText();
            visual.repaint();
            notifyListenersOnCollapse();
        }
        else {
            hiddenPanel.setVisible(true);
            visual.setExpandedText();
            visual.repaint();
            notifyListenersOnExpansion();
        }
 
        validate();
 
        visual.repaint();
    }
    
    private Vector<SHHTunnelForwardLPanelStateNotificationReciever> listeners = new Vector<SHHTunnelForwardLPanelStateNotificationReciever>(); 
    
    public void addSHHTunnelForwardLPanelStateNotificationReciever(SHHTunnelForwardLPanelStateNotificationReciever listener) {
    	if(!listeners.contains(listener)) {
    		listeners.add(listener);
    	}
    }
 
    public void clearSHHTunnelForwardLPanelStateNotificationRecievers() {
    	listeners.clear();
    }

    public interface SHHTunnelForwardLPanelStateNotificationReciever {
    	public void onExpansion();
    	public void onCollapse();
    }
    
    private void notifyListenersOnCollapse() {
    	for(SHHTunnelForwardLPanelStateNotificationReciever listener : listeners) {
    		listener.onCollapse();
    	}
    }
 
    private void notifyListenersOnExpansion() {
    	for(SHHTunnelForwardLPanelStateNotificationReciever listener : listeners) {
    		listener.onExpansion();
    	}
    }
 
}