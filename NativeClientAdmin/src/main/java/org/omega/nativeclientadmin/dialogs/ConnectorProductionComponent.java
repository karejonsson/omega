package org.omega.nativeclientadmin.dialogs;

import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.helpers.ConnectorProducer;

public class ConnectorProductionComponent extends JScrollPane implements ConnectorProducer {
	private static final long serialVersionUID = 1L; // Make eclipse shut up
	private ConnectorProducer cp;
	private JPanel embeddedPanel = new JPanel();
	public ConnectorProductionComponent() {
		this.getViewport().add(embeddedPanel);
	}
	public void setConnectorProducer(ConnectorProducer cp) {
		this.cp = cp;	
	}
	public EmbeddedModelHandling getOmegaDetails() throws Exception {
		return cp.getOmegaDetails();
	}
	public JPanel getPanel() {
		return embeddedPanel;
	}
	
    private Vector<ConnectorProductionComponentPanelStateNotificationReciever> listeners = new Vector<ConnectorProductionComponentPanelStateNotificationReciever>(); 
    
    public void addConnectorProductionComponentPanelStateNotificationReciever(ConnectorProductionComponentPanelStateNotificationReciever listener) {
    	if(!listeners.contains(listener)) {
    		listeners.add(listener);
    	}
    }
 
    public void clearConnectorProductionComponentPanelStateNotificationReciever() {
    	listeners.clear();
    }

    public interface ConnectorProductionComponentPanelStateNotificationReciever {
    	public void onExpansion();
    	public void onCollapse();
    }
    
    private void notifyListenersOnCollapse() {
    	for(ConnectorProductionComponentPanelStateNotificationReciever listener : listeners) {
    		listener.onCollapse();
    	}
    }
 
    private void notifyListenersOnExpansion() {
    	for(ConnectorProductionComponentPanelStateNotificationReciever listener : listeners) {
    		listener.onExpansion();
    	}
    }
    
    private boolean expanded = false;
	public void onExpansion() {
		expanded = true;
		notifyListenersOnExpansion();
	}
	public void onCollapse() {
		expanded = false;
		notifyListenersOnCollapse();
	}
	public boolean isExpanded() {
		return expanded;
	}

}

