package org.omega.nativeclientadmin.dialogs;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

import org.omega.connectivity.abstraction.EmbeddedModelHandling;

public class ConnectDialog extends JDialog {
	
	private ConnectPanel cont = null;
 
	public ConnectDialog(String title) {
		this(title, "");
	}
	
	public ConnectDialog(String title, String gdbappconfig) {
		cont = new ConnectPanel(gdbappconfig, new ConnectPanel.ConnectPanelResizeEventReciever() {
			public void onExpansion() {
				ConnectDialog.this.onPanelExpansion();
			}
			public void onCollapse() {
				ConnectDialog.this.onPanelCollapse();
			}
		});
		setTitle(title);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		try { 
			setContentPane(cont);
			pack();
			onPanelCollapse();
			setVisible(true);
		} 
		catch(Throwable t) {
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}
	
	private void onPanelExpansion() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension d = getSize();
		d.width = Math.min(700, (int) screenSize.getWidth());
		d.height = Math.min(800, (int) screenSize.getHeight());
		setSize(d);
	}
	
	private void onPanelCollapse() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension d = getSize();
		d.width = Math.min(700, (int) screenSize.getWidth());
		d.height = Math.min(520, (int) screenSize.getHeight());
		setSize(d);
	}
	
	public EmbeddedModelHandling getConnectedGDBAPPDetails() throws Exception {
		return cont.getConnectedGDBAPPDetails();
	}
	
    public static void main(String[] args) {
    	ConnectDialog cp = new ConnectDialog("");
    	cp.setVisible(true);
     }


}

