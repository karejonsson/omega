package org.omega.nativeclientadmin.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.bsf.BSFException;
import org.omega.executablescript.struct.CombinedFileStruct;
import org.omega.executablescript.struct.FlowStruct;
import org.omega.groovyintegration.scope.SystemProgrammingResourceAccess;
import org.omega.groovyscripting.GroovyShellScriptingScope;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class ChoseFlowToExecutePanel extends JPanel {
	
	private Vector<FlowStruct> all = null;
	private int charsInCommon = Integer.MAX_VALUE;

	public ChoseFlowToExecutePanel(CombinedFileStruct cfs) throws IOException {
		all = new Vector<FlowStruct>(); 
		for(String flowname : cfs.getFlowKeys()) {
			all.addAll(cfs.getFlows(flowname));
			/*
			for(FlowStruct fs : cfs.getFlows(flowname)) {
				all.add(fs);
			}
			*/
		}
		charsInCommon = nrLeadingCommons(all.get(0).getOrigin().getCanonicalPath(), all.get(1).getOrigin().getCanonicalPath());
		for(int i = 1 ; i < all.size()-1 ; i++) {
			charsInCommon = Math.min(charsInCommon, nrLeadingCommons(all.get(i).getOrigin().getCanonicalPath(), all.get(i+1).getOrigin().getCanonicalPath()));
		}
		setLayout(new StringGridBagLayout());
		for(int i = 0 ; i < all.size() ; i++) {
			final FlowStruct fs = all.get(i);
			add("gridx=0,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", new JLabel(fs.getIdentifier().getText()));
			add("gridx=1,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", new JLabel(fs.getOrigin().getCanonicalPath().substring(charsInCommon)));
			add("gridx=2,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", new JLabel("Line "+fs.getIdentifier().getBeginLine()));
			JButton verifyBtn = new JButton("Verify");
			add("gridx=3,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", verifyBtn);
			verifyBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
					GroovyShellScriptingScope scope = null;
					try {
						scope = new GroovyShellScriptingScope(sysobject);
					} 
					catch(BSFException e1) {
						UniversalTellUser.error(ChoseFlowToExecutePanel.this, "Unable to create scope to verification\n"+e1.getMessage());;
					}
					fs.getFlow().verifyReturnErrorDescription(cfs, scope);
					//System.out.println("Verify "+fs);
				}
			});
			JButton executeBtn = new JButton("Execute");
			add("gridx=4,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", executeBtn);
			executeBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SystemProgrammingResourceAccess sysobject = new SystemProgrammingResourceAccessImplementation();
					GroovyShellScriptingScope scope = null;
					try {
						scope = new GroovyShellScriptingScope(sysobject);
					} 
					catch(BSFException e1) {
						UniversalTellUser.error(ChoseFlowToExecutePanel.this, "Unable to create scope to execution\n"+e1.getMessage());;
					}
					try {
						fs.getFlow().execute(cfs, scope);
						UniversalTellUser.info(ChoseFlowToExecutePanel.this, "Execution OK");
					} 
					catch(Exception e1) {
						UniversalTellUser.error(ChoseFlowToExecutePanel.this, e1.getMessage());
					}
					//System.out.println("Execute "+fs);
				}
			});
		}
		// add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", text);
	}

	public static int nrLeadingCommons(String a, String b) {
		if(a == null) {
			return 0;
		}
		if(b == null) {
			return 0;
		}
		int ctr = 0;
		int max = Math.min(a.length(), b.length());
		while(ctr < max && a.charAt(ctr) == b.charAt(ctr)) {
			ctr++;
		}
		return ctr;
	}

}
