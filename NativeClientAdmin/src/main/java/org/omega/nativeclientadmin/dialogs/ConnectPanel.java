package org.omega.nativeclientadmin.dialogs;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.omega.clientcommon.editors.ClientModelLogic;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.helpers.ConnectorProducer;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;
import org.omega.nativeclient.helpers.SystemProgrammingResourceAccessImplementation;
import org.omega.nativeclient.treeview.ExplorerWindow;
import org.omega.nativeclientadmin.dialogs.ConnectorProductionComponent.ConnectorProductionComponentPanelStateNotificationReciever;
import org.omega.nativeclientadmin.dialogs.SHHTunnelForwardLPanel.SHHTunnelForwardLPanelStateNotificationReciever;
import org.omega.nativeclientadmin.fileview.SimpleIdeFrame;
import org.omega.nativeclientadmin.menu.OmegaNativeMenu;
import org.omega.nativeclientadmin.menu.GetExplorerVisitor;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;

public class ConnectPanel extends JPanel { 
	
	private String placedefault = "";
	private JTabbedPane jtp = new JTabbedPane();
	private final JTextField gdbappconfig = new JTextField();;
	private final JTextField encoding = new JTextField(InstallationProperties.getString(InstallationProperties.encoding, "UTF-8"));
	private static final String encodingDefaultText = "Platform native";
	private ConnectorProductionComponentPanelStateNotificationReciever sizeNotificationReciever = null;
 
	private boolean expanded = true;
	private void performExpansion() {
		//System.out.println("ConnectPanel.performExpansion()");
		if(listener != null) {
			listener.onExpansion();
		}
	}
	private void performCollapse() {
		//System.out.println("ConnectPanel.performCollapse()");
		if(listener != null) {
			listener.onCollapse();
		}
	}
	public void onExpanded() {
		if(expanded) {
			return;
		}
		performExpansion();
		expanded = true;
	}
	public void onCollapsed() {
		if(!expanded) {
			return;
		}
		performCollapse();
		expanded = false;
	}
	private void onStateChanged(boolean expanded) {
		if(expanded) {
			onExpanded();
		}
		else {
			onCollapsed();
		}
	}
	
	public interface ConnectPanelResizeEventReciever {
		public void onExpansion();
		public void onCollapse();
	}
	
	private ConnectPanelResizeEventReciever listener = null;

	public ConnectPanel(String _gdbappconfig) {
		this(_gdbappconfig, null);
	}

	public ConnectPanel(String _gdbappconfig, ConnectPanelResizeEventReciever cprer) {
		gdbappconfig.setText(_gdbappconfig);
		if(_gdbappconfig == null) {
			gdbappconfig.setText("");
		}
		if(gdbappconfig.getText().trim().length() != 0) {
			File f = new File(gdbappconfig.getText());
			try {
				gdbappconfig.setText(f.getCanonicalPath());
			}
			catch(Exception e) {
				gdbappconfig.setText(f.getAbsolutePath());
			}
			placedefault = gdbappconfig.getText().substring(0, gdbappconfig.getText().lastIndexOf(File.separator)+1);
			HierarchyObject.setReferenceFile(new File(placedefault));
		}
		sizeNotificationReciever = new ConnectorProductionComponentPanelStateNotificationReciever() {
			public void onExpansion() {
				ConnectPanel.this.onExpanded();
			}
			public void onCollapse() {
				ConnectPanel.this.onCollapsed();
			}
		};
		listener = cprer;
		try 
		{ 
			//Container cont = getContentPane();
			setLayout(new StringGridBagLayout());
			JPanel admin = new JPanel();
		    add(admin);
			admin.setLayout(new StringGridBagLayout()); 
			JLabel text = new JLabel("DB config");
			admin.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", text);
			admin.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", gdbappconfig);
			JButton browsebutton = new JButton("Browse");
			browsebutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String startpath = gdbappconfig.getText();
					if((startpath == null) || (startpath.trim().length() == 0)) {
						startpath = HierarchyObject.getReferenceFilePath();
					}
					JFileChooser fc = new JFileChooser(startpath);
					fc.setDialogTitle("Point to the file. (With extension "+OmegaNativeMenu.omegaconfigpostfix+")");
					fc.setDialogType(JFileChooser.SAVE_DIALOG);
					fc.showSaveDialog(ConnectPanel.this);
					File selFile = fc.getSelectedFile();
					if(selFile == null) return;
					String path = selFile.getAbsolutePath();
					if(path == null) return;
					if(!(path.endsWith(OmegaNativeMenu.omegaconfigpostfix)))
					{
						JOptionPane.showMessageDialog(
								ConnectPanel.this, 
								"The filename must end with "+OmegaNativeMenu.appname, 
								"File not valid", 
								JOptionPane.ERROR_MESSAGE); 
						return;
					}
					gdbappconfig.setText(selFile.getAbsolutePath());
					try {
						placedefault = gdbappconfig.getText().substring(0, gdbappconfig.getText().lastIndexOf(File.separator)+1);
					}
					catch(Exception e) {}
				}
			});
			admin.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);
			
			// Encoding here
			JLabel encodingL = new JLabel("Encoding");
			admin.add("gridx=0,gridy=1,anchor=WEST,insets=[12,12,0,0]", encodingL);
			admin.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", encoding);
			JButton setencodingbutton = new JButton("Set");
			setencodingbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String opts[] = SimpleIdeFrame.getListOfEncodings();
					int choice = JOptionPane.showOptionDialog(ConnectPanel.this, "Chose encoding for the config files", "Encoding", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, opts, "");
					if(choice == JOptionPane.CLOSED_OPTION) {
						return;
					}
					if(choice < 0 || choice >= opts.length) {
						encoding.setText(encodingDefaultText);
						return;
					}
					encoding.setText(opts[choice]);
				}
			});
			admin.add("gridx=2,gridy=1,anchor=EAST,insets=[12,7,0,11]", setencodingbutton);
			
			add("gridx=0,gridy=0,fill=HORIZONTAL,insets=[12,7,0,11]", admin);

			//jtp = new JTabbedPane();
			add("gridx=0,gridy=1,fill=BOTH,weightx=1.0,weighty=1.0,insets=[12,7,0,11]", jtp);
			
			final JTabbedPane hsqldb = new JTabbedPane();
			try { hsqldb.addTab("Memory", getHSQLDBMemPanel()); } catch(Exception e) {}
			try { hsqldb.addTab("File", getHSQLDBFilePanel()); } catch(Exception e) {}
			try { hsqldb.addTab("Remote", getHSQLDBRemotePanel()); } catch(Exception e) {}
			
			hsqldb.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
			        //int index = hsqldb.getSelectedIndex();
			        ConnectorProductionComponent choice = (ConnectorProductionComponent) hsqldb.getSelectedComponent();
			        //System.out.println("HSQLDB tab changed to: " + hsqldb.getTitleAt(index)+", expanderat tillstånd "+choice.isExpanded());
			        ConnectPanel.this.onStateChanged(choice.isExpanded());
				}
			});
			
			//hsqldbremote.add("gridx=1,gridy=7,fill=HORIZONTAL,insets=[12,7,0,11]", new SHHTunnelForwardLPanel());

			jtp.addTab("HSQLDB", hsqldb);

			JTabbedPane internal = new JTabbedPane();
			try { internal.addTab("MySQL", getMySQLPanel()); } catch(Exception e) {}
			try { internal.addTab("MSSQLServer", getMSSQLServerPanel()); } catch(Exception e) {} 
			try { internal.addTab("Oracle", getOraclePanel()); } catch(Exception e) {} 
			try { internal.addTab("IBM DB2", getIBMDB2Panel()); } catch(Exception e) {} 
			try { internal.addTab("POSTGRES", getPOSTGRESPanel()); } catch(Exception e) {} 

			internal.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
			        //int index = internal.getSelectedIndex();
			        ConnectorProductionComponent choice = (ConnectorProductionComponent) internal.getSelectedComponent();
			        //System.out.println("Internal tab changed to: " + internal.getTitleAt(index)+", expanderat tillstånd "+choice.isExpanded());
			        ConnectPanel.this.onStateChanged(choice.isExpanded());
				}
			});

			jtp.addTab("Internal", internal);
			jtp.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
			        //int index = jtp.getSelectedIndex();
			        JTabbedPane upperChoice = (JTabbedPane) jtp.getSelectedComponent();
			        //System.out.println("Upper tab changed to: " + upperChoice.getTitleAt(index));
			        ConnectorProductionComponent choice = (ConnectorProductionComponent) upperChoice.getSelectedComponent();
			        //System.out.println("Lower tab changed to: " + internal.getTitleAt(index)+", expanderat tillstånd "+choice.isExpanded());
			        ConnectPanel.this.onStateChanged(choice.isExpanded());
				}
			});
			
			//JTabbedPane network = new JTabbedPane();
			//network.addTab("Restful", getRestfulPanel()); 
			//jtp.addTab("Network", network);
		} 
		catch(Throwable t) 
		{
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}

	private static void printEncodingToFile(FileWriter fw, JTextField tf) throws IOException {
		String enc = tf.getText();
		if(enc == null) {
			return;
		}
		enc = enc.trim();
		if(enc.length() < 3) {
			// Assume this is a blanc or a typo or wrong anyway
			return;
		}
		fw.write("encoding \""+enc+"\"\n");
	}
	
	private static ClientModelLogic getClientModelLogic(EmbeddedModelHandling emh) throws Exception {
		return new ClientModelLogic(emh, new SystemProgrammingResourceAccessImplementation());
	}
	
	private ConnectorProductionComponent getHSQLDBFilePanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("HSQLDB file");
		jp.add("gridx=0,gridy=0,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(placedefault);
		jp.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);
		JButton browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String startpath = field1.getText();
				if((startpath == null) || (startpath.trim().length() == 0)) {
					startpath = HierarchyObject.getReferenceFilePath();
				}
				JFileChooser fc = new JFileChooser(startpath);
				String end = ".script";
				fc.setDialogTitle("Point to the file. (With extension "+end+")");
				fc.setDialogType(JFileChooser.SAVE_DIALOG);
				fc.showSaveDialog(ConnectPanel.this);
				File selFile = fc.getSelectedFile();
				if(selFile == null) return;
				String path = selFile.getAbsolutePath();
				if(path == null) return;
				if(!(path.endsWith(end)))
				{
					JOptionPane.showMessageDialog(
							ConnectPanel.this, 
							"The filename must end with "+end, 
							"File not valid", 
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				String x = selFile.getAbsolutePath();
				int idx = x.lastIndexOf(".");
				field1.setText(selFile.getAbsolutePath().substring(0, idx));
			}
		});
		jp.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("SA");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);

					FileWriter fw = new FileWriter(x);
					fw.write("hsqldbfile (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("file \""+fixAsFile(field1.getText())+"\"\n");
					fw.write("username \""+field2.getText().trim()+"\"\n");
					fw.write("password \""+field3.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n)\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=3,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedHSQL.connectHSQLDBFile(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), "Omega Explorer HSQLDB File", "hsqldbname");
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=3,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedHSQL.initializeHSQLDBFile(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB File");
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				return OmegaEmbeddedHSQL.connectHSQLDBFile(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB File", "hsqldbname");
			} 
		});
		return cpc;
	}
	
	private ConnectorProductionComponent getHSQLDBMemPanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("Database name");
		jp.add("gridx=0,gridy=0,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbmemory_dbname, "memdb"));
		jp.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);
		
		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbmemory_username, "SA"));
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		//---
		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);

					FileWriter fw = new FileWriter(x);
					fw.write("hsqldbmemory (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+fixAsFile(field1.getText())+"\"\n");
					fw.write("username \""+field2.getText().trim()+"\"\n");
					fw.write("password \""+field3.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n)\n");
					fw.close();
					/*
    <HSQLDBMEMORY> { out = new DBHSQLDBMemoryConnectionSpecification(); } <LPAREN>
    <CONFIGFILE> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setConfigfile(s); }
    [ <ENCODING> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setEncoding(s); }  ]
    <DBNAME> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setDbname(s); }
    <USERNAME> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setUsername(s); }
    <PASSWORD> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setPassword(s); }
    [ <FRAMETITLE> s = parseName() { ((DBHSQLDBMemoryConnectionSpecification) out).setFrametitle(s); } ]
    <RPAREN>
					 */
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		//---
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=2,gridy=3,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB Memory");
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//initializeAndConnectHSQLDBMemory(field1.getText(), field2.getText(), field3.getText());
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				return OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB Memory");
			} 
		});
		return cpc;
	}

	private ConnectorProductionComponent getHSQLDBRemotePanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbremote_ipresolvable, "127.0.0.1"));
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbremote_port, "9001"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Protocol");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JComboBox protocol = new JComboBox(new String[] {InstallationProperties.getString(InstallationProperties.hsqldbremote_protocol, ""), "hsql", "http", "hsqls", "https"});
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", protocol);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbremote_username, "SA"));
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);

		varname = new JLabel("Database name");
		jp.add("gridx=0,gridy=6,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField(InstallationProperties.getString(InstallationProperties.hsqldbremote_dbname, "somedb"));
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);
		
		final SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=7,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=8,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("hsqldbremote (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field5.getText().trim()+"\"\n");
					fw.write("username \""+field3.getText().trim()+"\"\n");
					fw.write("password \""+field4.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("protocol \""+((String) protocol.getSelectedItem()).trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
					
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=8,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedHSQL.connectHSQLDBRemote(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), (String) protocol.getSelectedItem(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB Remote", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectHSQLDBRemote(field1.getText(), field2.getText(), (String) protocol.getSelectedItem(), field3.getText(), field4.getText(), field5.getText());
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=8,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedHSQL.initializeHSQLDBRemote(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), (String) protocol.getSelectedItem(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB Remote", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//initializeAndConnectHSQLDBRemote(field1.getText(), field2.getText(), (String) protocol.getSelectedItem(), field3.getText(), field4.getText(), field5.getText());
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				return OmegaEmbeddedHSQL.connectHSQLDBRemote(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), (String) protocol.getSelectedItem(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer HSQLDB Remote", sshtflp.getParameters());
			} 
		});
		return cpc;
	}
	
	private String getPlaceToPointFileChooser() throws Exception {
		String startpath = placedefault;
		if((startpath == null) || (startpath.trim().length() == 0)) {
			startpath = HierarchyObject.getReferenceFilePath();
		}
		JFileChooser fc = new JFileChooser(startpath);
		//String end = GdbAppMenu.omegaconnectionpostfix;
		fc.setDialogTitle("Choose "+OmegaNativeMenu.omegaapplicationpostfix+" file");
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.showSaveDialog(ConnectPanel.this);
		File selFile = fc.getSelectedFile();
		if(selFile == null) return null;
		String path = selFile.getAbsolutePath();
		if(path == null) return null;
		if(!(path.endsWith(OmegaNativeMenu.omegaapplicationpostfix))) {
			UniversalTellUser.error(ConnectPanel.this, "The filename must end with "+OmegaNativeMenu.omegaapplicationpostfix);
			return null;
		}
		String x = selFile.getAbsolutePath();
		File f = new File(x);
		if(!f.createNewFile()) {
			UniversalTellUser.error(ConnectPanel.this, "Unable to create file "+x);
			return null;
		}
		return x;
	}
	
	private String fixAsFile(String file) {
		//System.out.println("ConnectPanel.fixAsFile: parameter:::::: "+file);
		//System.out.println("ConnectPanel.fixAsFile: placedefault::: "+placedefault);
		String out = file;
		if(out.startsWith(placedefault)) {
			out = file.substring(placedefault.length());
		}
		return out.trim().replace("\\", File.separator+File.separator);
	}

	private ConnectorProductionComponent getMySQLPanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.mysql_ipresolvable, "127.0.0.1"));
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.mysql_port, "3306"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=7,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("mysql (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field3.getText().trim()+"\"\n");
					fw.write("username \""+field4.getText().trim()+"\"\n");
					fw.write("password \""+field5.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("MySQL Connect");
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try { 
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.connectMYSQL(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MySQL", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", false);
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=7,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.initializeMYSQL(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MySQL", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, GetExplorerVisitor.getHSQLDBExtraMenu(cml.getConnection()));
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", true);
				//System.out.println("MySQL Initialize and connect");
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				return OmegaEmbeddedInternal.connectMYSQL(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MySQL", sshtflp.getParameters());
			} 
		});
		return cpc;
	}

	private ConnectorProductionComponent getOraclePanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.oracle_ipresolvable, "127.0.0.1"));//"127.0.0.1");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.oracle_port, "1521"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=7,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("oracle (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field3.getText().trim()+"\"\n");
					fw.write("username \""+field4.getText().trim()+"\"\n");
					fw.write("password \""+field5.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("MySQL Connect");
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try { 
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.connectOracle(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer Oracle", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", false);
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=7,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.initializeOracle(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer Oracle", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", true);
				//System.out.println("MySQL Initialize and connect");
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				return OmegaEmbeddedInternal.connectOracle(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer Oracle", sshtflp.getParameters());
			} 
		});
		return cpc;
	}

	private ConnectorProductionComponent getIBMDB2Panel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.ibmdb2_ipresolvable, "127.0.0.1"));//"127.0.0.1");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.ibmdb2_port, "50000"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=7,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("ibmdb2 (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field3.getText().trim()+"\"\n");
					fw.write("username \""+field4.getText().trim()+"\"\n");
					fw.write("password \""+field5.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("MySQL Connect");
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try { 
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.connectIBMDB2(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer IBM DB2", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", false);
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=7,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.initializeIBMDB2(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer IBM DB2", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", true);
				//System.out.println("MySQL Initialize and connect");
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				return OmegaEmbeddedInternal.connectIBMDB2(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer IBM DB2", sshtflp.getParameters());
			} 
		});
		return cpc;
	}

	private ConnectorProductionComponent getPOSTGRESPanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.postgres_ipresolvable, "127.0.0.1"));
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.postgres_port, "5432"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField();
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField();
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=7,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("postgres (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field3.getText().trim()+"\"\n");
					fw.write("username \""+field4.getText().trim()+"\"\n");
					fw.write("password \""+field5.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("MySQL Connect");
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try { 
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.connectPOSTGRES(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer POSTGRESQL", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", false);
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=7,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.initializePOSTGRES(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer POSTGRESQL", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", true);
				//System.out.println("MySQL Initialize and connect");
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				return OmegaEmbeddedInternal.connectPOSTGRES(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer POSTGRESQL", sshtflp.getParameters());
			} 
		});
		return cpc;
	}

	private ConnectorProductionComponent getMSSQLServerPanel() {
		ConnectorProductionComponent cpc = new ConnectorProductionComponent();
		cpc.addConnectorProductionComponentPanelStateNotificationReciever(sizeNotificationReciever);
		JPanel jp = cpc.getPanel();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(InstallationProperties.getString(InstallationProperties.mssqlserver_ipresolvable, "127.0.0.1"));
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField(InstallationProperties.getString(InstallationProperties.mssqlserver_port, "1433"));
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField(InstallationProperties.getString(InstallationProperties.mssqlserver_username, "SA"));
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		SHHTunnelForwardLPanel sshtflp = new SHHTunnelForwardLPanel();
		sshtflp.addSHHTunnelForwardLPanelStateNotificationReciever(new SHHTunnelForwardLPanelStateNotificationReciever() {
			public void onExpansion() {
				cpc.onExpansion();
			}
			public void onCollapse() {
				cpc.onCollapse();
			}
		});
		
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,insets=[0,0,0,0]", sshtflp);

		JButton saveButton = new JButton("Save "+OmegaNativeMenu.omegaapplicationpostfix);
		jp.add("gridx=0,gridy=7,anchor=WEST,insets=[12,7,0,11]", saveButton);
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String x = getPlaceToPointFileChooser();
					if(x == null) {
						return;
					}
					String out = JOptionPane.showInputDialog(null,
		                    "Type a frame titel for this connection if you want",
		                    "Request for frame title",
		                    JOptionPane.PLAIN_MESSAGE);
					FileWriter fw = new FileWriter(x);
					fw.write("mssqlserver (\n");
					fw.write("configfile \""+fixAsFile(gdbappconfig.getText())+"\"\n");
					printEncodingToFile(fw, encoding);
					fw.write("dbname \""+field3.getText().trim()+"\"\n");
					fw.write("username \""+field4.getText().trim()+"\"\n");
					fw.write("password \""+field5.getText().trim()+"\"\n");
					fw.write("ipresolvable \""+field1.getText().trim()+"\"\n");
					fw.write("port \""+field2.getText().trim()+"\"\n");
					fw.write("frametitle \""+((out == null) ? "" : out.trim())+"\"\n");
					TunnelForwardLParameters tfp = sshtflp.getParameters();
					if(tfp != null && tfp.hasAll()) {
						fw.write("  sshtunnel {\n"+tfp.toString()+"\n  }\n");
					}
					fw.write(")\n");
					fw.close();
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("MySQL Connect");
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try { 
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.connectMSSQLServer(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MSSQLServer", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
				//connectMYSQL(dburl, field4.getText(), field5.getText(), "MySQL", false);
			}
		});
		JButton iacButton = new JButton("Initialize and connect");
		jp.add("gridx=1,gridy=7,anchor=EAST,insets=[12,7,0,11]", iacButton);
		iacButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//String dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				try {
					EmbeddedModelHandling detailsEmb = OmegaEmbeddedInternal.initializeMSSQLServer(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MSSQLServer", sshtflp.getParameters());
					if(detailsEmb.hasErrors()) {
						if(!continueWithConnect(detailsEmb)) {
							return;
						}
					}
					ClientModelLogic cml = getClientModelLogic(detailsEmb);
					//NativeClientModelHandling details = new NativeClientModelHandling(cml);
					ExplorerWindow ew = new ExplorerWindow(cml, null);;
					ew.setVisible(true);
				}
				catch(Exception e) {
					UniversalTellUser.error(ConnectPanel.this, e.getMessage());
					e.printStackTrace();
				}
			}
		});
		cpc.setConnectorProducer(new ConnectorProducer() {
			public EmbeddedModelHandling getOmegaDetails() throws Exception {
				//String _dburl = "jdbc:mysql://"+field1.getText().trim()+":"+field2.getText().trim()+"/"+field3.getText().trim();
				return OmegaEmbeddedInternal.connectMSSQLServer(gdbappconfig.getText(), encoding.getText(), field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), OmegaNativeMenu.appname+" Explorer MSSQLServer", sshtflp.getParameters());
			} 
		});
		return cpc;
	}
 
	public EmbeddedModelHandling getConnectedGDBAPPDetails() throws Exception {
		int sel = jtp.getSelectedIndex();
		Component comp = jtp.getSelectedComponent();
		//System.out.println("ConnectionComponent.getGDBAPPDetails comps typ �r "+comp.getClass().getName());
		ConnectorProductionComponent c = (ConnectorProductionComponent) comp;//jtp.getSelectedComponent();//getTabComponentAt(sel);
		if(c == null) {
			//System.out.println("ConnectionComponent.getGDBAPPDetails ger null. Index = "+sel);
			return null;
		}
		//System.out.println("ConnectionComponent.getGDBAPPDetails ger icke null. Index = "+sel);
		return c.getOmegaDetails();
	}
	
	private boolean continueWithConnect(EmbeddedModelHandling detailsEmb) {
		Vector<String> msgs = detailsEmb.getErrors(5);
		StringBuffer sb = new StringBuffer();
		sb.append("There are problems with this schema/datamodel.\n\n");
		for(int i = 0 ; i < msgs.size(); i++) {
			sb.append(msgs.elementAt(i)+"\n");
		}
		sb.append("\nFor more details use the Consistency Check functionality.\n"+
		"Confirm to proceed with connect");
		String msg = sb.toString();
		return UniversalTellUser.info_ask_for_yes_propose_no(ConnectPanel.this, msg, "Confirm connect");
	}
	
}
