package org.omega.nativeclientadmin.dialogs;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.manager.OmegaEmbeddedInternal;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;
import org.omega.nativeclientadmin.menu.OmegaNativeMenu;
import org.omega.nativeclientadmin.modelcalc.HSQLDBModelCalculator;
import org.omega.nativeclientadmin.modelcalc.IBMDB2ModelCalculator;
import org.omega.nativeclientadmin.modelcalc.MSSQLServerModelCalculator;
import org.omega.nativeclientadmin.modelcalc.ModelCalculator;
import org.omega.nativeclientadmin.modelcalc.ModelCalculatorProducer;
import org.omega.nativeclientadmin.modelcalc.MySQLModelCalculator;
import org.omega.nativeclientadmin.modelcalc.OracleModelCalculator;
import org.omega.nativeclientadmin.modelcalc.POSTGRESQLModelCalculator;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.gui.progress.ProgressDumpAndMonitor;

public class ModelCalcPanel extends JPanel {
	
	private String placedefault = "";
	private JTabbedPane jtp = new JTabbedPane();
	//private PKIManager pkiman = null;
	private final JTextField targetfile = new JTextField("");
	private final JTextField comment = new JTextField();
	private JComboBox sqlstyle = null;
	
	private static TunnelForwardLParameters TUNNELFORWARDLPARAMETERSNULL = null;
 
	public ModelCalcPanel() {
		//pkiman = _pkiman;
		targetfile.setText("");//"/Users/karejonsson/program/eclipse/workspace/Alternative_DataTracker/testarea/data/x.gdbapp");
		if(targetfile.getText().trim().length() != 0) {
			File f = new File(targetfile.getText());
			try {
				targetfile.setText(f.getCanonicalPath());
			}
			catch(Exception e) {
				targetfile.setText(f.getAbsolutePath());
			}
			placedefault = targetfile.getText().substring(0, targetfile.getText().lastIndexOf(File.separator)+1);
			HierarchyObject.setReferenceFile(new File(placedefault));
		}
		try 
		{ 
			//Container cont = getContentPane();
			setLayout(new StringGridBagLayout());
			JPanel admin = new JPanel();
		    add(admin);
			admin.setLayout(new StringGridBagLayout()); 
			JLabel text = new JLabel("Output file");
			admin.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", text);
			admin.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", targetfile);
			JButton browsebutton = new JButton("Browse");
			browsebutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String startpath = targetfile.getText();
					if((startpath == null) || (startpath.trim().length() == 0)) {
						startpath = HierarchyObject.getReferenceFilePath();
					}
					JFileChooser fc = new JFileChooser(startpath);
					fc.setDialogTitle("Point to the file. (With extension .sql/.gdbapp)");
					fc.setDialogType(JFileChooser.SAVE_DIALOG);
					fc.showSaveDialog(ModelCalcPanel.this);
					File selFile = fc.getSelectedFile();
					if(selFile == null) return;
					String path = selFile.getAbsolutePath();
					if(path == null) return;
					if(!path.endsWith(".sql") && !path.endsWith(OmegaNativeMenu.appname))
					{
						JOptionPane.showMessageDialog(
								ModelCalcPanel.this, 
								"The filename must end with .sql or "+OmegaNativeMenu.appname, 
								"File not valid", 
								JOptionPane.ERROR_MESSAGE); 
						return;
					}
					targetfile.setText(selFile.getAbsolutePath());
				}
			});
			admin.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);
			add("gridx=0,gridy=0,fill=HORIZONTAL,insets=[12,7,0,11]", admin);
			JLabel sqlstylelabel = new JLabel("SQL Style");
			admin.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", sqlstylelabel);
			sqlstyle = new JComboBox(new String[] {"MYSQL", "MSSQL"});
			admin.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", sqlstyle);
			
			JLabel commentlabel = new JLabel("Comment");
			admin.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", commentlabel); 
			admin.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", comment);
			
			//jtp = new JTabbedPane();
			add("gridx=0,gridy=2,fill=BOTH,weightx=1.0,weighty=1.0,insets=[12,7,0,11]", jtp);
			
			//jtp.addTab("HSQLDB file", getHSQLDBFilePanel());
			//jtp.addTab("HSQLDB Remote", getHSQLDBRemotePanel());
			jtp.addTab("MySQL", getMySQLPanel()); 
			jtp.addTab("MSSQL2008", getMSSQL2008Panel()); 
			jtp.addTab("Oracle", getOraclePanel()); 
			jtp.addTab("IBM DB2", getIBMDB2Panel()); 
			//jtp.addTab("POSTGRESQL", getPOSTGRESQLPanel()); 
			//jtp.addTab("Restful", getRestfulPanel()); 
		} 
		catch(Throwable t) 
		{
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}

	private static class ModelCalculatorProductionComponent extends JPanel implements ModelCalculatorProducer {
		private static final long serialVersionUID = 1L; // Make eclipse shut up
		private ModelCalculatorProducer mcp;
		public void setModelCalculatorProducer(ModelCalculatorProducer _mcp) {
			mcp = _mcp;	
		}
		public ModelCalculator getModelCalculator() throws Exception {
			return mcp.getModelCalculator();
		}
	}
	
	private ModelCalculatorProductionComponent getHSQLDBFilePanel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("HSQLDB file");
		jp.add("gridx=0,gridy=0,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField(placedefault);
		jp.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);
		JButton browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String startpath = field1.getText();
				if((startpath == null) || (startpath.trim().length() == 0)) {
					startpath = HierarchyObject.getReferenceFilePath();
				}
				JFileChooser fc = new JFileChooser(startpath);
				String end = ".script";
				fc.setDialogTitle("Point to the file. (With extension "+end+")");
				fc.setDialogType(JFileChooser.SAVE_DIALOG);
				fc.showSaveDialog(ModelCalcPanel.this);
				File selFile = fc.getSelectedFile();
				if(selFile == null) return;
				String path = selFile.getAbsolutePath();
				if(path == null) return;
				if(!(path.endsWith(end)))
				{
					JOptionPane.showMessageDialog(
							ModelCalcPanel.this, 
							"The filename must end with "+end, 
							"File not valid", 
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				String x = selFile.getAbsolutePath();
				int idx = x.lastIndexOf(".");
				field1.setText(selFile.getAbsolutePath().substring(0, idx));
			}
		});
		jp.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("SA");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=3,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Look on close example
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new HSQLDBModelCalculator(OmegaEmbeddedHSQL.getPureHSQLDBFileConnection(field1.getText(), field2.getText(), field3.getText(), "hsqldbfile"));
			} 
		});
		return jp;
	}
	
	private ModelCalculatorProductionComponent getHSQLDBRemotePanel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("127.0.0.1");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("9001");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Protocol");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JComboBox protocol = new JComboBox(new String[] {"hsql", "http", "hsqls", "https"});
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", protocol);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("SA");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);

		varname = new JLabel("Database name");
		jp.add("gridx=0,gridy=6,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("somedb");
		jp.add("gridx=1,gridy=6,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=7,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Look on close example
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new HSQLDBModelCalculator(OmegaEmbeddedHSQL.getPureHSQLDBFileConnection(field1.getText(), field2.getText(), field3.getText(), "hsqldbfile"));
			} 
		});
		return jp;
	}
	
	private String fixAsFile(String file) {
		String out = file;
		if(out.startsWith(placedefault)) {
			out = file.substring(placedefault.length());
		}
		return out.trim().replace("\\", File.separator+File.separator);
	}

	private ModelCalculatorProductionComponent getMySQLPanel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("127.0.0.1");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("3306");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField();
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField();
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		JButton connectButton = new JButton("Extract");
		jp.add("gridx=2,gridy=6,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MySQLModelCalculator mc = null;
				try {
					mc = new MySQLModelCalculator(OmegaEmbeddedInternal.getPureMYSQLConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
				}
				catch(Exception e) {
					UniversalTellUser.error(ModelCalcPanel.this, e.getMessage());
					e.printStackTrace();
					return;
				}
				runWithProgress(mc, targetfile.getText(), comment.getText());
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new MySQLModelCalculator(OmegaEmbeddedInternal.getPureMYSQLConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
			} 
		});
		return jp;
	}

	private ModelCalculatorProductionComponent getOraclePanel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("192.168.1.86");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("1521");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("XE");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("system");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("pwdoracle");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		JButton connectButton = new JButton("Extract");
		jp.add("gridx=2,gridy=6,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OracleModelCalculator mc = null;
				try {
					mc = new OracleModelCalculator(OmegaEmbeddedInternal.getPureOracleConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
				}
				catch(Exception e) {
					UniversalTellUser.error(ModelCalcPanel.this, e.getMessage());
					e.printStackTrace();
					return;
				}
				runWithProgress(mc, targetfile.getText(), comment.getText());
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new OracleModelCalculator(OmegaEmbeddedInternal.getPureOracleConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
			} 
		});
		return jp;
	}

	private ModelCalculatorProductionComponent getIBMDB2Panel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("192.168.1.86");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("1521");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("XE");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("system");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("pwdoracle");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		JButton connectButton = new JButton("Extract");
		jp.add("gridx=2,gridy=6,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IBMDB2ModelCalculator mc = null;
				try {
					mc = new IBMDB2ModelCalculator(OmegaEmbeddedInternal.getPureIBMDB2Connection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
				}
				catch(Exception e) {
					UniversalTellUser.error(ModelCalcPanel.this, e.getMessage());
					e.printStackTrace();
					return;
				}
				runWithProgress(mc, targetfile.getText(), comment.getText());
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new IBMDB2ModelCalculator(OmegaEmbeddedInternal.getPureIBMDB2Connection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
			} 
		});
		return jp;
	}

	private ModelCalculatorProductionComponent getPOSTGRESQLPanel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("127.0.0.1");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("5432");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField();
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField();
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);
		
		JButton connectButton = new JButton("Connect");
		jp.add("gridx=2,gridy=6,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Look on close example
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new POSTGRESQLModelCalculator(OmegaEmbeddedInternal.getPurePOSTGRESConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
			} 
		});
		return jp;
	}

	private ModelCalculatorProductionComponent getMSSQL2008Panel() {
		ModelCalculatorProductionComponent jp = new ModelCalculatorProductionComponent();
	    //getContentPane().add(jp, BorderLayout.CENTER);
		jp.setLayout(new StringGridBagLayout());

		JLabel varname = new JLabel("IP resolvable");
		jp.add("gridx=0,gridy=1,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field1 = new JTextField("127.0.0.1");
		//final JTextField field1 = new JTextField("172.16.99.136");
		jp.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field1);

		varname = new JLabel("Port");
		jp.add("gridx=0,gridy=2,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field2 = new JTextField("1433");
		jp.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field2);

		varname = new JLabel("Database");
		jp.add("gridx=0,gridy=3,anchor=WEST,insets=[12,7,0,11]", varname);
		//final JTextField field3 = new JTextField("FAST2000_sssb_01");
		final JTextField field3 = new JTextField("");
		jp.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field3);

		varname = new JLabel("User id");
		jp.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field4 = new JTextField("SA");
		jp.add("gridx=1,gridy=4,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field4);
		
		varname = new JLabel("Password");
		jp.add("gridx=0,gridy=5,anchor=WEST,insets=[12,7,0,11]", varname);
		final JTextField field5 = new JTextField("server");
		jp.add("gridx=1,gridy=5,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", field5);

		JButton connectButton = new JButton("Extract");
		jp.add("gridx=2,gridy=6,anchor=EAST,insets=[12,7,0,11]", connectButton);
		connectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MSSQLServerModelCalculator mc = null;
				try {
					mc = new MSSQLServerModelCalculator(OmegaEmbeddedInternal.getPureMSSQLServerConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
				}
				catch(Exception e) {
					UniversalTellUser.error(ModelCalcPanel.this, e.getMessage());
					e.printStackTrace();
					return;
				} 
				runWithProgress(mc, targetfile.getText(), comment.getText());
			}
		});
		jp.setModelCalculatorProducer(new ModelCalculatorProducer() {
			public ModelCalculator getModelCalculator() throws Exception {
				return new MSSQLServerModelCalculator(OmegaEmbeddedInternal.getPureMSSQLServerConnection(field1.getText(), field2.getText(), field3.getText(), field4.getText(), field5.getText(), TUNNELFORWARDLPARAMETERSNULL));
			} 
		});
		return jp;
	}
	
	private void runWithProgress(final ModelCalculator mc, final String filename, final String comment) {
		Runnable r = new Runnable() {
			public void run() {
				try { 
					String extension = filename.substring(filename.lastIndexOf('.'));
					mc.setOutstyle((String) sqlstyle.getSelectedItem(), extension);
				    ProgressDumpAndMonitor pmd = ProgressDumpAndMonitor.createGUI(mc); 
				    JFrame frame = ProgressDumpAndMonitor.getFrame();
				    //System.out.println("Frame "+frame);
				    if(!frame.isVisible()) {
				    	frame.setVisible(true);
				    }
				    pmd.monitoredWorkStarted();

					FileOutputStream fos = new FileOutputStream(filename);
					fos.write(("\n/*\nCreated "+(new java.util.Date())+"\n"+comment+"\n*/\n\n").getBytes());
					//System.out.println("ModelCalcPanel.runWithProgress.run - 1");
					mc.printModelToStream(fos);
					//System.out.println("ModelCalcPanel.runWithProgress.run - 2");
					fos.flush();
					//System.out.println("ModelCalcPanel.runWithProgress.run - 3");
					fos.close();
					//System.out.println("ModelCalcPanel.runWithProgress.run - 4");
					//pmd.monitoredWorkFinished();
				}
				catch(Exception e) {
					//System.out.println("ModelCalcPanel.runWithProgress.run - 5");
					UniversalTellUser.error(ModelCalcPanel.this, e.getMessage());
					//System.out.println("ModelCalcPanel.runWithProgress.run - 6");
					e.printStackTrace();
					//System.out.println("ModelCalcPanel.runWithProgress.run - 7");
				}
				//System.out.println("ModelCalcPanel.runWithProgress.run - 8");
			}
		};
		//System.out.println("ModelCalcPanel.runWithProgress - filename "+filename+", 1");
		Thread t = new Thread(r);
		//System.out.println("ModelCalcPanel.runWithProgress - filename "+filename+", 2");
		t.start();
		//System.out.println("ModelCalcPanel.runWithProgress - filename "+filename+", 3");
	}

	public ModelCalculator getModelCalculator() throws Exception {
		int sel = jtp.getSelectedIndex();
		Component comp = jtp.getSelectedComponent();
		//System.out.println("ConnectionComponent.getGDBAPPDetails comps typ �r "+comp.getClass().getName());
		ModelCalculatorProductionComponent c = (ModelCalculatorProductionComponent) comp;//jtp.getSelectedComponent();//getTabComponentAt(sel);
		if(c == null) {
			//System.out.println("ConnectionComponent.getGDBAPPDetails ger null. Index = "+sel);
			return null;
		}
		//System.out.println("ConnectionComponent.getGDBAPPDetails ger icke null. Index = "+sel);
		return c.getModelCalculator();
	}
	

	
}
