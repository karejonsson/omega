package org.omega.nativeclientadmin.dialogs;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.nativeclientadmin.menu.OmegaNativeMenu;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReportText;
import org.omega.nativeclientadmin.modelanalysis.ModelConsistencyCheckFactory;
import org.omega.nativeclientadmin.modelanalysis.ModelConsistencyCheckReport;
import org.omega.nativeclientadmin.modelanalysis.ModelConsistencyCheckReportText;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;

import se.modlab.generics.bshro.fs.DiskFilesystemHierarchy;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.gui.util.Clipboard;

public class ModelConsistencyCheckTool extends JFrame {
	
	private final JTextField model1 = new JTextField();
	private final JTextField reportfile = new JTextField();


	public ModelConsistencyCheckTool(String title, String initialFilename) {
		JPanel cont = getPanel(initialFilename);
		setTitle(title);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		try { 
			setContentPane(cont);
			pack();
			Dimension d = getSize();
			d.width = 700;
			d.height = 250;
			setSize(d);
			setVisible(true);
		} 
		catch(Throwable t) {
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}
	
	private void browse(String extension, JTextField field) {
		String startpath = field.getText();
		if((startpath == null) || (startpath.trim().length() == 0)) {
			startpath = HierarchyObject.getReferenceFilePath();
		}
		JFileChooser fc = new JFileChooser(startpath);
		fc.setDialogTitle("Point to the file. (With extension "+extension+")");
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.showSaveDialog(ModelConsistencyCheckTool.this);
		File selFile = fc.getSelectedFile();
		if(selFile == null) return;
		String path = selFile.getAbsolutePath();
		if(path == null) return;
		if(!path.endsWith(extension))
		{
			JOptionPane.showMessageDialog(
					ModelConsistencyCheckTool.this, 
					"The filename must end with "+extension, 
					"File not valid", 
					JOptionPane.ERROR_MESSAGE); 
			return;
		}
		field.setText(selFile.getAbsolutePath());
	}
	
	private JPanel getPanel(String initialFilename) {
		JPanel admin = new JPanel();
		admin.setLayout(new StringGridBagLayout()); 
		admin.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", new JLabel("Model ("+OmegaNativeMenu.appname+")"));
		if(initialFilename != null) {
			if(initialFilename.trim().length() > 0) {
				model1.setText(initialFilename.trim());
				reportfile.setText(initialFilename.trim()+".txt");
			}
		}
 		admin.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", model1);
		JButton browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browse(OmegaNativeMenu.omegaconfigpostfix, model1);
			}
		});
		admin.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);
		
		admin.add("gridx=0,gridy=1,anchor=WEST,insets=[12,12,0,0]", new JLabel("Output"));
		final JComboBox<String> algo = new JComboBox<String>(ModelConsistencyCheckFactory.getAlgos());
		admin.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", algo);
		
		admin.add("gridx=0,gridy=2,anchor=WEST,insets=[12,12,0,0]", new JLabel("Report"));
		admin.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", reportfile);
		browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browse(".txt", reportfile);
			}
		});
		admin.add("gridx=2,gridy=2,anchor=EAST,insets=[12,7,0,11]", browsebutton);

		JButton comparebutton = new JButton("Perform checks");
		comparebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							//ModelCompareReportText mcrt = new ModelCompareReportText(reportfile.getText());
							ModelConsistencyCheckReport mcr = ModelConsistencyCheckFactory.getReport(((String) algo.getSelectedItem()).trim(), reportfile.getText());
							performChecks(model1.getText(), mcr);
						}
						catch(Exception e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(
									ModelConsistencyCheckTool.this, 
									e.getMessage(), 
									"File not valid", 
									JOptionPane.ERROR_MESSAGE); 
							
						}
					}
				});
			}
		});
		admin.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", comparebutton);
		JButton openbutton = new JButton("Open");
		openbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							Clipboard.desktopOpen(reportfile.getText(), ModelConsistencyCheckTool.this);
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(
									ModelConsistencyCheckTool.this, 
									"Unable to open "+reportfile.getText(), 
									"File not valid", 
									JOptionPane.ERROR_MESSAGE); 
						}
					}
				});
			}
		});
		admin.add("gridx=1,gridy=4,anchor=WEST,insets=[12,7,0,11]", openbutton);

		return admin;
	}
	
	private static void actualPerformChecks(String reference, ModelConsistencyCheckReport report) throws Exception  {
		File infile1 = new File(reference);
		if(!infile1.exists()) {
			throw new Exception("File "+reference+" does not exist");
		}

		File infilep1 = infile1.getParentFile();
		DiskFilesystemHierarchy dfsh1 = new DiskFilesystemHierarchy("root", null, infilep1);
		String name1 = reference.substring(Math.max(reference.lastIndexOf("/"),reference.lastIndexOf("\\")));
		final HierarchyLeaf hl1 = (HierarchyLeaf) dfsh1.getChild(name1);
		
		ModelConfiguration hs = new ModelConfiguration();
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(infilep1), "UTF-8");
			OmegaConfigParser.parseOmegaConfig(hs, isr, "File "+infilep1.getCanonicalPath());

			/*
			hs = GdbappConfigParser.parseGdbappConfigNativeEncoding(
					hl1,//infile1, 
					//reference.substring(Math.max(reference.lastIndexOf("/"),reference.lastIndexOf("\\"))), 
					//new FileInputStream(infile1), 
					new GDBAPPScopeFactory(),
					null, new UserScriptingIntegrationGDBAPP());
			 */
		}
		catch(Exception e) {
			throw new Exception("Error when parsing reference model", e);
		}

		report.modelParseable(reference, hs);
		
		// Check for missing auto increment columns
		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			CreateStatement csCur = ths.getCreateStatement();
			Column col = csCur.getAutoIncrementColumn();
			if(col == null) {
				report.missingAutoIncrementColumn(ths);
			}
		}

		// Check for missing primary keys
		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			CreateStatement csCur = ths.getCreateStatement();
			PrimaryKeyDeclaration pkd = csCur.getPrimaryKeyDeclaration();
			if(pkd == null) {
				report.missingPrimaryKey(ths);
			}
		}

		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			CreateStatement csSource = ths.getCreateStatement();
			for(int j = 0 ; j < csSource.getNoForeignKeyDeclarations() ; j++) {
				ForeignKeyDeclaration fkd = csSource.getForeignKeyDeclaration(j);
				String pointedTableName = fkd.getReferencedTablename();
				CreateStatement csTarget = null;
				try {
					csTarget = hs.getDBTableHandlingSpecificationOnTableName(pointedTableName).getCreateStatement();
				}
				catch(Exception e) {
					
				}
				if(csTarget == null) {
					report.missingTargetTableForForeignKey(fkd);
				}
				else {
					PrimaryKeyDeclaration pkd = csTarget.getPrimaryKeyDeclaration();
					
					String[] memberset = fkd.getAllReferredTableMembers();
					
					if((pkd != null) && (!pkd.hasMembers(memberset)) && !csTarget.hasKeyDeclaration(memberset)) {
						report.missingTargetColumnsKeyForForeignKey(fkd);
					}
					else {
						for(int i = 0 ; i < fkd.getNoReferringMembers() ; i++) {
							Column clSource = null;
							Column clTarget = null;
							clSource = csSource.getColumn(fkd.getReferringMember(i));
							clTarget = csTarget.getColumn(fkd.getReferredTableMember(i));
							boolean canCompare = true;
							if(clSource == null) {
								canCompare = false;
								report.missingForeignKeySourceColumn(csSource, fkd.getReferringMember(i), fkd);
							}
							if(clTarget == null) {
								canCompare = false;
								report.missingForeignKeyTargetColumn(csTarget, fkd.getReferredTableMember(i), fkd);
							}
							if(canCompare) {
								String message = ModelCompareReportText.compareTypes(clSource, clTarget);
								if(message != null) {
									report.deviatingColumnType(csSource, clSource, csTarget, clTarget, fkd, message);
								}
							}
						}
					}
				}
				
			}
		}

	}
	
	private static void performChecks(String reference, ModelConsistencyCheckReport report) throws Exception {
		Exception eChecks = null;
		Exception eWrite = null;
		try {
			actualPerformChecks(reference, report);			
		}
		catch(Exception e) {
			eChecks = e;
			//e.printStackTrace();
		}
		try {
			report.done();
		}
		catch(Exception e) {
			eWrite = e;
		}
		if(eChecks == null && eWrite == null) {
			return;
		}
		if(eChecks != null && eWrite != null) {
			String msg = "Both performing checks and writing the report failed\n"+
							"Checks: "+eChecks.getMessage()+"\n"+
							"Write: "+eWrite.getMessage();
			throw new Exception(msg);
		}
		if(eChecks != null) {
			String msg = "Performing checks failed\n"+
					eChecks.getMessage();
			Exception out = new Exception(msg, eChecks);
			throw out;
		}
		if(eWrite != null) {
			String msg = "Writing report failed\n"+
					eWrite.getMessage();
			throw new Exception(msg);
		}
		throw new Exception("Obsolete logic in ModelConsistencyCheck.preformChecks");
	}
	
	public static void main(String args[]) {
		//new ModelCompareTool("hej");
		
		String folder = "/Users/karejonsson/tmp/svk/modlab/files";
		String infilename = "usr2_20130513"+OmegaNativeMenu.omegaconfigpostfix;
		//String infilename = "y.gdbapp";
		String outfilename = "out.txt";
		File outfile = new File(new File(folder), outfilename);
		if(outfile.exists()) {
			outfile.delete();
		}
		try {
			performChecks(folder+"/"+infilename, new ModelConsistencyCheckReportText(folder+"/"+outfilename, "\r\n"));
		}
		catch(Exception e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
			Throwable cause = e.getCause();
			if(cause != null) {
				System.out.println("Reason; "+cause.getMessage());
			}
		}
		Clipboard.desktopOpen(folder+"/"+outfilename, null);
		
	}

}