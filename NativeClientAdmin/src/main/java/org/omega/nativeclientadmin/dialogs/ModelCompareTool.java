package org.omega.nativeclientadmin.dialogs;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.nativeclientadmin.menu.OmegaNativeMenu;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareFactory;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReport;
import org.omega.nativeclientadmin.modelanalysis.ModelCompareReportCorrectionalScript;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;

import se.modlab.generics.bshro.fs.DiskFilesystemHierarchy;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.gui.util.Clipboard;

public class ModelCompareTool extends JFrame {
	
	private final JTextField model1 = new JTextField();
	private final JTextField model2 = new JTextField();
	private final JTextField reportfile = new JTextField();


	public ModelCompareTool(String title) {
		JPanel cont = getPanel();
		setTitle(title);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				setVisible(false);
				dispose();
			}
		});
		try { 
			setContentPane(cont);
			pack();
			Dimension d = getSize();
			d.width = 700;
			d.height = 250;
			setSize(d);
			setVisible(true);
		} 
		catch(Throwable t) {
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		} 
	}
	
	private void browse(String extension, JTextField field) {
		String startpath = field.getText();
		if((startpath == null) || (startpath.trim().length() == 0)) {
			startpath = HierarchyObject.getReferenceFilePath();
		}
		JFileChooser fc = new JFileChooser(startpath);
		fc.setDialogTitle("Point to the file. (With extension "+extension+")");
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.showSaveDialog(ModelCompareTool.this);
		File selFile = fc.getSelectedFile();
		if(selFile == null) return;
		String path = selFile.getAbsolutePath();
		if(path == null) return;
		if(!path.endsWith(extension))
		{
			JOptionPane.showMessageDialog(
					ModelCompareTool.this, 
					"The filename must end with "+extension, 
					"File not valid", 
					JOptionPane.ERROR_MESSAGE); 
			return;
		}
		field.setText(selFile.getAbsolutePath());
	}
	
	private JPanel getPanel() {
		JPanel admin = new JPanel(); 
		admin.setLayout(new StringGridBagLayout()); 
		admin.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", new JLabel("Reference"));
		admin.add("gridx=1,gridy=0,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", model1);
		JButton browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browse(OmegaNativeMenu.omegaconfigpostfix, model1);
			}
		});
		admin.add("gridx=2,gridy=0,anchor=EAST,insets=[12,7,0,11]", browsebutton);
		
		admin.add("gridx=0,gridy=1,anchor=WEST,insets=[12,12,0,0]", new JLabel("Compare"));
		admin.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", model2);
		browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browse(OmegaNativeMenu.omegaconfigpostfix, model2);
			}
		});
		admin.add("gridx=2,gridy=1,anchor=EAST,insets=[12,7,0,11]", browsebutton);

		// 2
		admin.add("gridx=0,gridy=2,anchor=WEST,insets=[12,12,0,0]", new JLabel("Output"));
		final JComboBox<String> algo = new JComboBox<String>(ModelCompareFactory.getAlgos());
		admin.add("gridx=1,gridy=2,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", algo);
		
		admin.add("gridx=0,gridy=3,anchor=WEST,insets=[12,12,0,0]", new JLabel("Report"));
		admin.add("gridx=1,gridy=3,fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", reportfile);
		browsebutton = new JButton("Browse");
		browsebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				browse(".txt", reportfile);
			}
		});
		admin.add("gridx=2,gridy=3,anchor=EAST,insets=[12,7,0,11]", browsebutton);

		JButton comparebutton = new JButton("Compare");
		comparebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							//ModelCompareReportText mcrt = new ModelCompareReportText(reportfile.getText());
							ModelCompareReport mcr = ModelCompareFactory.getReport(((String) algo.getSelectedItem()).trim(), reportfile.getText());
							compare(model1.getText(), model2.getText(), mcr);
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(
									ModelCompareTool.this, 
									e.getMessage(), 
									"File not valid", 
									JOptionPane.ERROR_MESSAGE); 
						}
					}
				});
			}
		});
		admin.add("gridx=0,gridy=4,anchor=WEST,insets=[12,7,0,11]", comparebutton);
		JButton openbutton = new JButton("Open");
		openbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							Clipboard.desktopOpen(reportfile.getText(), ModelCompareTool.this);
						}
						catch(Exception e) {
							JOptionPane.showMessageDialog(
									ModelCompareTool.this, 
									"Unable to open "+reportfile.getText(), 
									"File not valid", 
									JOptionPane.ERROR_MESSAGE); 
						}
					}
				});
			}
		});
		admin.add("gridx=1,gridy=4,anchor=WEST,insets=[12,7,0,11]", openbutton);

		return admin;
	}
	
	private static void compare(String reference, String compare, ModelCompareReport report) throws Exception  {
		File infile1 = new File(reference);
		if(!infile1.exists()) {
			throw new Exception("File "+reference+" does not exist");
		}

		File infilep1 = infile1.getParentFile();
		DiskFilesystemHierarchy dfsh1 = new DiskFilesystemHierarchy("root", null, infilep1);
		String name1 = reference.substring(Math.max(reference.lastIndexOf("/"),reference.lastIndexOf("\\")));
		final HierarchyLeaf hl1 = (HierarchyLeaf) dfsh1.getChild(name1);
		
		File infile2 = new File(compare);
		if(!infile2.exists()) {
			throw new Exception("File "+compare+" does not exist");
		}

		File infilep2 = infile2.getParentFile();
		DiskFilesystemHierarchy dfsh2 = new DiskFilesystemHierarchy("root", null, infilep2);
		String name2 = compare.substring(Math.max(reference.lastIndexOf("/"),reference.lastIndexOf("\\")));
		final HierarchyLeaf hl2 = (HierarchyLeaf) dfsh2.getChild(name2);

		ModelConfiguration hs1 = new ModelConfiguration();
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(infilep1), "UTF-8");
			OmegaConfigParser.parseOmegaConfig(hs1, isr, "File "+infilep1.getCanonicalPath());
			/*
			hs1 = GdbappConfigParser.parseGdbappConfigNativeEncoding(
					hl1,//infile1, 
					//reference.substring(Math.max(reference.lastIndexOf("/"),reference.lastIndexOf("\\"))), 
					//new FileInputStream(infile1), 
					new GDBAPPScopeFactory(), 
					null, new UserScriptingIntegrationGDBAPP());
		    */
		}
		catch(Exception e) {
			throw new Exception("Error when parsing reference model", e);
		}

		ModelConfiguration hs2 = new ModelConfiguration();
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(infilep2), "UTF-8");
			OmegaConfigParser.parseOmegaConfig(hs2, isr, "File "+infilep2.getCanonicalPath());
			/*
			hs2 = GdbappConfigParser.parseGdbappConfigNativeEncoding(
				hl2,//infile2, 
				//compare.substring(Math.max(compare.lastIndexOf("/"),compare.lastIndexOf("\\"))), 
				//new FileInputStream(infile2), 
				new GDBAPPScopeFactory(),
				null, new UserScriptingIntegrationGDBAPP());
			 */
		}
		catch(Exception e) {
			throw new Exception("Error when parsing compared model", e);
		}

		report.modelsParseable(reference, compare);
		
		Vector<String> commonTables = new Vector<String>();
		
		tablesInCommon(commonTables, hs1, hs2, report);
		Hashtable<String, String> translation1 = getTableTranslation(hs1);
		Hashtable<String, String> translation2 = getTableTranslation(hs2);
		
		report.verifyingColumnsInEachCommonTable();
		
		for(int i = 0 ; i < commonTables.size() ; i++) {
			String tablename = commonTables.elementAt(i);
			TableConfiguration ths1 = hs1.getDBTableHandlingSpecificationOnTableName(translation1.get(tablename));
			TableConfiguration ths2 = hs2.getDBTableHandlingSpecificationOnTableName(translation2.get(tablename));
			columnsInCommon(ths1, ths2, report);
		}

		report.verifyingColumnsTypesInEachCommonTable();
		
		for(int i = 0 ; i < commonTables.size() ; i++) {
			String tablename = commonTables.elementAt(i);
			TableConfiguration ths1 = hs1.getDBTableHandlingSpecificationOnTableName(translation1.get(tablename));
			TableConfiguration ths2 = hs2.getDBTableHandlingSpecificationOnTableName(translation2.get(tablename));
			typesOfColumnsInCommon(ths1, ths2, report);
		}

		report.verifyingPrimaryKeysInEachCommonTable();
		
		for(int i = 0 ; i < commonTables.size() ; i++) {
			String tablename = commonTables.elementAt(i);
			TableConfiguration ths1 = hs1.getDBTableHandlingSpecificationOnTableName(translation1.get(tablename));
			TableConfiguration ths2 = hs2.getDBTableHandlingSpecificationOnTableName(translation2.get(tablename));
			primaryKeysAndIdentityInCommon(ths1, ths2, report);
		}

		report.verifyingForeignKeysInEachCommonTable();
		
		for(int i = 0 ; i < commonTables.size() ; i++) {
			String tablename = commonTables.elementAt(i);
			TableConfiguration ths1 = hs1.getDBTableHandlingSpecificationOnTableName(translation1.get(tablename));
			TableConfiguration ths2 = hs2.getDBTableHandlingSpecificationOnTableName(translation2.get(tablename));
			foreignKeysAndIdentityInCommon(ths1, ths2, report);
		}

		report.done();
	}
	
	private static Hashtable<String, String> getTableTranslation(ModelConfiguration hs) {
		Hashtable<String, String> out = new Hashtable<String, String>();
		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			String name = ths.getTableName();
			out.put(name.toLowerCase(), name);
		}
		return out;
	}
	
	private static Hashtable<String, String> getColumnTranslation(TableConfiguration ths) {
		Hashtable<String, String> out = new Hashtable<String, String>();

		for(int i = 0 ; i < ths.getCreateStatement().getNoColumns() ; i++) {
			Column col = ths.getCreateStatement().getColumn(i);
			String name = col.getName().toLowerCase();
			out.put(name.toLowerCase(), name);
		}
		return out;
	}
	
	private static void tablesInCommon(Vector<String> commonTables, ModelConfiguration hs1, ModelConfiguration hs2, ModelCompareReport report) throws IOException {
		
		report.verifySameTables();
		
		Hashtable<String, String> translation2 = getTableTranslation(hs2);
		for(TableConfiguration ths1 : hs1.getTablehandlersLoopable()) {
			String ths1name = ths1.getTableName();
			String ths2name = translation2.get(ths1name.toLowerCase());
			if(ths2name == null) {
				report.missingTable(ths1);
				continue;
			}
			TableConfiguration ths2 = hs2.getDBTableHandlingSpecificationOnTableName(ths2name);
			if(ths2 == null) {
				report.missingTable(ths1);
				continue;
			}
			commonTables.add(ths1name.toLowerCase());
		}
		
		report.amountMissingTable();
		
		Hashtable<String, String> translation1 = getTableTranslation(hs1);
		for(TableConfiguration ths2 : hs2.getTablehandlersLoopable()) {
			String ths2name = ths2.getTableName();
			String ths1name = translation1.get(ths2name.toLowerCase());
			if(ths1name == null) {
				report.extraTable(ths2);
				continue;
			}
			TableConfiguration ths1 = hs1.getDBTableHandlingSpecificationOnTableName(ths1name);
			if(ths1 == null) {
				report.extraTable(ths2);
				continue;
			}
		}
		
		report.amountExtraTable(commonTables);
	}
	
	private static void columnsInCommon(TableConfiguration ths1, TableConfiguration ths2, ModelCompareReport report) throws IOException {
		Hashtable<String, String> translation2 = getColumnTranslation(ths2);
		for(int i = 0 ; i < ths1.getCreateStatement().getNoColumns() ; i++) {
			Column col1 = ths1.getCreateStatement().getColumn(i);
			String name1 = col1.getName();
			String name2 = translation2.get(name1.toLowerCase());
			if(name2 == null) {
				report.missingColumn(ths2, col1);
				continue;
			}
			Column col2 = ths1.getCreateStatement().getColumn(name2);
			if(col2 == null) {
				report.missingColumn(ths2, col1);
				continue;
			}
		}
		
		report.amountMissingColumn();
		
		Hashtable<String, String> translation1 = getColumnTranslation(ths1);
		for(int i = 0 ; i < ths2.getCreateStatement().getNoColumns() ; i++) {
			Column col2 = ths2.getCreateStatement().getColumn(i);
			String name2 = col2.getName();
			String name1 = translation1.get(name2.toLowerCase());
			if(name1 == null) {
				report.extraColumn(ths1, col2);
				continue;
			}
			Column col1 = ths2.getCreateStatement().getColumn(name1);
			if(col1 == null) {
				report.extraColumn(ths1, col2);
				continue;
			}
		}
		report.amountExtraColumn(ths1);
	}
	
	
	private static void typesOfColumnsInCommon(TableConfiguration ths1, TableConfiguration ths2, ModelCompareReport report) throws IOException {
		Hashtable<String, String> translation2 = getColumnTranslation(ths2);
		int deviations = 0;
		for(int i = 0 ; i < ths1.getCreateStatement().getNoColumns() ; i++) {
			Column col1 = ths1.getCreateStatement().getColumn(i);
			String name1 = col1.getName();
			String name2 = translation2.get(name1.toLowerCase());
			if(name2 == null) {
				continue;
			}
			Column col2 = ths2.getCreateStatement().getColumn(name2);
			if(col2 == null) {
				continue;
			}
			report.columnTypeDeviation(ths1, i, col2);
		}
		
		report.amountDeviatingColumns();
	}
	
	private static void primaryKeysAndIdentityInCommon(TableConfiguration ths1, TableConfiguration ths2, ModelCompareReport report) throws IOException {
		PrimaryKeyDeclaration pkd1 = ths1.getCreateStatement().getPrimaryKeyDeclaration();
		PrimaryKeyDeclaration pkd2 = ths2.getCreateStatement().getPrimaryKeyDeclaration();
		report.primaryKeyComparison(ths1, pkd1, pkd2);
		Hashtable<String, String> translation2 = getColumnTranslation(ths2);
		for(int i = 0 ; i < ths1.getCreateStatement().getNoColumns() ; i++) {
			Column col1 = ths1.getCreateStatement().getColumn(i);
			String name1 = col1.getName();
			String name2 = translation2.get(name1.toLowerCase());
			if(name2 == null) {
				continue;
			}
			Column col2 = ths2.getCreateStatement().getColumn(name2);
			if(col2 == null) {
				continue;
			}
			report.autoIncrementComparison(ths1, col1, col2);
		}
		
		report.amountPrimaryKeys();
	}
	
	private static void foreignKeysAndIdentityInCommon(TableConfiguration ths1, TableConfiguration ths2, ModelCompareReport report) throws IOException {
		CreateStatement cs1 = ths1.getCreateStatement();
		CreateStatement cs2 = ths2.getCreateStatement();
		report.numberOfForeignKeyDeclartations(cs1, cs2);
		
		for(int i = 0 ; i < cs1.getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration fkd1 = cs1.getForeignKeyDeclaration(i);
			ForeignKeyDeclaration fkd2 = cs2.getForeignKeyDeclarationIgnoreCase(fkd1.getConstraintname());
			report.coherenceOfForeignKeyDeclartations(ths1, fkd1, fkd2, i);
		}
		
		report.amountForeignKeyDeviations();
	}
	
	public static void main(String args[]) {
		//new ModelCompareTool("hej");
		
		String folder = "/Users/karejonsson/program/eclipse/workspace/Alternative_DataTracker/testarea/data/";
		String m1 = folder+"ali"+OmegaNativeMenu.omegaconfigpostfix;
		String m2 = folder+"sssb"+OmegaNativeMenu.omegaconfigpostfix;
		String rf = folder+"out.corr";
		try {
			compare(m1, m2, new ModelCompareReportCorrectionalScript(RecreatorManager.getRecreator("MySQL"), "\r\n", rf));
		}
		catch(Exception e) {
			System.out.println("Error: "+e.getMessage());
			e.printStackTrace();
			Throwable cause = e.getCause();
			if(cause != null) {
				System.out.println("Reason; "+cause.getMessage());
			}
		}
	}

}

