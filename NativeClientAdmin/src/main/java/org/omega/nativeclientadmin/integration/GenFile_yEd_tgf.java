package org.omega.nativeclientadmin.integration;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Hashtable;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.ForeignKeyDeclaration;

public class GenFile_yEd_tgf {
	
	public static void generate(File f, ModelConfiguration hs) throws Exception {
		Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
		FileOutputStream fos = new FileOutputStream(f);
		int ctr = 0;
		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			String tname = ths.getTableName();
			ht.put(tname, ++ctr);
			//fos.write((""+ctr+" "+tname+"\n").getBytes());
			fos.write((""+ctr+" "+ths.translate(tname)+"\n").getBytes());
		}
		fos.write("#\n".getBytes());
		for(TableConfiguration ths : hs.getTablehandlersLoopable()) {
			for(int i = 0 ; i < ths.getNoForeignKeyDeclarations() ; i++) {
				ForeignKeyDeclaration fkd = ths.getForeignKeyDeclaration(i);
				String fromtable = fkd.getReferringTablename();
				String totable = fkd.getReferencedTablename();
				//fos.write((""+ht.get(fromtable)+" "+ht.get(totable)+" "+fkd.getName()+"\n").getBytes());
				fos.write((""+ht.get(fromtable)+" "+ht.get(totable)+" "+ths.translate(fkd.getConstraintname())+"\n").getBytes());
			}
		}
		fos.close();
	}
	
	public static void generate(String oldfile, ModelConfiguration hs) throws Exception {
		//System.out.println("OLD "+oldfile);
		String toDot = oldfile.substring(0, oldfile.lastIndexOf("."));
		//System.out.println("toDot "+toDot);
		String fileToBe = toDot+".tgf";
		//System.out.println("fileToBe "+fileToBe);
		File f = new File(fileToBe);
		generate(f, hs);
	}

	/*
	public static void main(String args[]) {
		String dir = "/Users/karejonsson/program/eclipse/workspace/Alternative_DataTracker/testarea/data/";
		String gdbappconfig = dir+"datatracker.gdbapp";
		String outfile = dir+"datatracker.tgf";
		DBHandlingSpecification hs = null;
		try {
			hs = GdbAppMenu.configTableOrderCalcule(new File(gdbappconfig), true); 
		}
		catch(Exception e) {
			System.out.println("1: Nej "+e.getMessage());
			return;
		}
		File f = null;
		try {
			f = new File(outfile);
			if(f.exists()) {
				f.delete();
			}
		}
		catch(Exception e) {
			System.out.println("2: Nej "+e.getMessage());
			return;
		}
		try {
			generate(gdbappconfig, hs);
		}
		catch(Exception e) {
			System.out.println("3: Nej "+e.getMessage());
			return;
		}
	}
	*/
}
