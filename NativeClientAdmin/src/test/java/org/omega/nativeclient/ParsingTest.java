package org.omega.nativeclient;

import org.junit.Test;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.properties.InstallationProperties;

import se.modlab.generics.exceptions.IntolerableException;

public class ParsingTest {

	@Test
	public void t1() throws Exception {
		String project = System.getProperty("user.dir");

		InstallationProperties.pathToConfigFile_default = project+"/../release/native/resources/omeganative.properties";

		EmbeddedModelHandling details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/lob.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");
		
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
	}
	
	@Test
	public void t2() throws Exception {
		String project = System.getProperty("user.dir");

		EmbeddedModelHandling details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/button.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");
		
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
	}
	
	@Test
	public void t3() throws Exception {
		String project = System.getProperty("user.dir");

		EmbeddedModelHandling details = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(
				project+"/src/test/resources/autoupdate.omega", 
				"UTF-8", "mymemdb", "SA", "", "Frames title");
		
		OmegaEmbeddedHSQL.clearMemory(details.getConnection());
	}
	
}
