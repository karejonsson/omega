package se.modlab.generics.gui.plot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.JpegEncoder;

public class TimePlotComponent extends JComponent {

	private TimePlottable sequence;
	private static final int margin = 20;
	private int w_pix;
	private int h_pix;
	private double min_val;
	private double max_val;
	private double h_val;
	private long startTime;
	private long endTime;

	public TimePlotComponent(TimePlottable sequence) {
		this.sequence = sequence;
	    addMouseListener(new PopupListener());
	    addMouseMotionListener(new ToolTipMouseMotionListener());
	}

	public void paint(Graphics g) {
		paint(g, getSize());
	}

	protected double getValue(int i) throws IntolerableException {
		return sequence.getValue(i);
	}

	protected void drawText(Graphics g, Dimension d, int h_pix) throws IntolerableException {
		g.drawString("Min value: "+sequence.getMinValue(), margin, h_pix+((int)(1.5*margin)));
		g.drawString("Max value: "+sequence.getMaxValue(), margin, h_pix+((int)(2.5*margin)));

		g.drawString("Start value: "+sequence.getValue(0), 
				d.width/2, h_pix+((int)(1.5*margin)));
		g.drawString("End value: "+sequence.getValue(sequence.getSize()-1), 
				d.width/2, h_pix+((int)(2.5*margin)));

		g.drawString("Start time: "+getTimeStringFromLong(sequence.getTime(0)), 
				margin, h_pix+((int)(3.5*margin)));
		g.drawString("End time: "+getTimeStringFromLong(sequence.getTime(sequence.getSize()-1)), 
				d.width/2, h_pix+((int)(3.5*margin)));
	}

	protected void drawFrame(Graphics g, Dimension d, int h_pix) throws IntolerableException {
		// Frame upper
		g.drawLine((int) margin/2, (int) margin/2,
				(int) (d.width - margin/2), (int) margin/2); 
		// Frame lower
		g.drawLine((int) margin/2, h_pix+(int) margin/2,
				(int) (d.width - margin/2), h_pix+(int) margin/2); 
		// Frame left
		g.drawLine((int) margin/2, (int) margin/2,
				(int) margin/2, h_pix+(int) margin/2); 
		// Frame right
		g.drawLine((int) (d.width - margin/2), (int) margin/2,
				(int) (d.width - margin/2), h_pix+(int) margin/2); 
	}
	
	public static void drawDotRectangle(Graphics g, int hor, int ver) {
		g.drawRect(hor, ver, 3, 3);
	}
	
	public static void drawDotCircle(Graphics g, int hor, int ver) {
		g.drawOval(hor, ver, 3, 3);
	}
	
	public static void drawDotCross(Graphics g, int hor, int ver) {
		g.drawLine(hor, ver, hor+3, ver+3);
		g.drawLine(hor+3, ver, hor, ver+3);
	}
	
	public static void drawDot(Graphics g, int hor, int ver, int shape) {
		if(shape == TimePlottable.BOX) {
			drawDotRectangle(g, hor, ver);
		}
		if(shape == TimePlottable.CROSS) {
			drawDotCross(g, hor, ver);
		}
		if(shape == TimePlottable.CIRCLE) {
			drawDotCircle(g, hor, ver);
		}
	}

	public void paint(Graphics g, Dimension d) {
		try {
			min_val = sequence.getMinValue();
			max_val = sequence.getMaxValue();
			h_val = max_val - min_val;
			int linesBelow = 4;
			h_pix = d.height-margin*linesBelow;
			w_pix = margin;
			if(sequence.getSize() == 1) 
			{
				g.drawString("The initial value "+getValue(0)+" is never changed",
						margin, margin);
				drawText(g, d, h_pix);
				return;
			}
			startTime = sequence.getTime(0);
			endTime = sequence.getTime(sequence.getSize()-1);

			drawFrame(g, d, h_pix);

			for(int i = 0 ; i < sequence.getSize() ; i++) {
				long time = sequence.getTime(i);
				double val = sequence.getValue(i);
				drawDot(g, 
						((int) (w_pix + (d.width-2*w_pix)*(((double)(time-startTime))/(endTime-startTime))))-1, 
						((int) (h_pix - (val-min_val)/(h_val)*(h_pix-margin)))-1, 
						sequence.getShape(i));
			}

			drawText(g, d, h_pix);
		}
		catch(IntolerableException ie)
		{
			UniversalTellUser.general(null, ie);
		}

	}

	private long getTimeLongFromString(String s) throws IntolerableException{
		String tmp = s;
		int colons = s.indexOf("::");
		if(colons != -1) tmp = s.substring(0, colons-1);
		try
		{
			long l = Date.parse(tmp);
			//System.out.println("getTimeLongFromString: Fick "+s+" svarar "+l);
			return l;
		}
		catch(IllegalArgumentException e)
		{
			throw new InternalProgrammingError("Unable to handle Date \""+s+"\"");
		}
	}

	private String getTimeStringFromLong(long l) {
		try
		{
			Date d = new Date(l);
			int year = d.getYear()+1900;
			int month = d.getMonth()+1;
			int day = d.getDate();
			int hours = d.getHours();
			int minutes = d.getMinutes();
			int seconds = d.getSeconds();
			int millis = (int)(d.getTime()-
					1000*((int)(Math.floor(d.getTime()/1000))));

			return 
			""+year+"/"+
			((month < 10)?"0":"")+month+"/"+
			((day < 10)?"0":"")+day+" "+
			((hours < 10)?"0":"")+hours+":"+
			((minutes < 10)?"0":"")+minutes+":"+
			((seconds < 10)?"0":"")+seconds+"::"+millis;

		}
		catch(IllegalArgumentException e)
		{
			System.out.println("Gick ej med "+l);
			System.exit(0);
		}
		return null;
	}

	protected void mouseMoved(MouseEvent e)
	{
		try {
			if(sequence.getSize() == 1) return;
			Dimension d = getSize();
			int w_pix = margin;
			int x = e.getX();
			double time = (x - w_pix)*(endTime-startTime)/(d.width-2*w_pix);
			String sttext = getTimeStringFromLong(sequence.getTime(0)); 
			int colons = sttext.indexOf(" ");
			if(colons != -1) sttext = sttext.substring(0, colons-1);
			long stlong = getTimeLongFromString(sttext);
			String ettext = getTimeStringFromLong(sequence.getTime(sequence.getSize()-1));
			colons = ettext.indexOf(" ");
			if(colons != -1) ettext = ettext.substring(0, colons-1);
			long etlong = getTimeLongFromString(ettext);
			long epochtime = stlong+((long) ((etlong-stlong)*time/(endTime-startTime)));
			String timeInChart = getTimeStringFromLong(epochtime);

			double min_val = sequence.getMinValue();
			double max_val = sequence.getMaxValue();
			double h_val = max_val - min_val;
			int h_pix = d.height-margin*4;
			int y = e.getY();

			double variable = min_val-(y - h_pix)*h_val/(h_pix-margin);
			setToolTipText("time = "+timeInChart+", "+
					sequence.getName()+
					" = "+variable);
		}
		catch(IntolerableException ie) {
			setToolTipText(""+ie.getMessage());
		}
		catch(Exception ex) {
			double y = e.getY();
			double variable = min_val-(y - h_pix)*h_val/(h_pix-margin);
			setToolTipText(""+variable);
		}
	}

	private class PopupListener extends MouseAdapter 
	{

		public PopupListener()
		{
		}

		public void mousePressed(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) 
		{
			if(sequence.getSize() == 1) return;
			if(!e.isPopupTrigger()) return;
			JPopupMenu popup = getPopup();
			if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
		}

	}
	
	  private class ToolTipMouseMotionListener implements MouseMotionListener
	  {

	    public void mouseDragged(MouseEvent e)
	    {
	    }

	    public void mouseMoved(MouseEvent e)
	    {
	      TimePlotComponent.this.mouseMoved(e);
	    }

	  }



	protected void createJPG()
	{
		try
		{
			JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
			fc.setDialogTitle("Point to the file. (With extension .jpg)");
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(this));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return;
			String path = selFile.getAbsolutePath();
			if(path == null) return;
			if(!(path.endsWith(".jpg")))
			{
				JOptionPane.showMessageDialog(
						UniversalTellUser.getFrame(this), 
						"The filename must end with .jpg", 
						"File not valid", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			Dimension d = getSize();
			BufferedImage image = new BufferedImage(d.width, 
					d.height, 
					BufferedImage.TYPE_USHORT_GRAY);
			Graphics2D graphics = image.createGraphics();
			graphics.setPaint(Color.BLACK);
			graphics.setBackground(Color.WHITE);
			graphics.clearRect(0, 0, d.width, d.height);
			paint(graphics, d);
			FileOutputStream fos = new FileOutputStream(selFile);
			JpegEncoder jpg = new JpegEncoder(image, 50, fos);
			jpg.Compress();
			fos.close();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("File not found", fnf));
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("I/O error", ioe));
		}
		catch(Exception e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), 
					new UnclassedError("monitoredSimVariable - jpg-generation", e));
		}
	}

	protected JPopupMenu getPopup()
	{
		if(sequence.getSize() == 1) return null;;
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Create .jpg-file");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						createJPG();
					}
				});
		popup.add(menuItem);
		menuItem = new JMenuItem("Export to clipboard");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						exportToClipboard();
					}
				});
		popup.add(menuItem);
		return popup;
	}
	
	private void exportToClipboard() {
		try {
			StringBuffer sb = new StringBuffer();
			int rowcount = sequence.getSize();//model.getRowCount();
			for(int r = 0 ; r < rowcount ; r++) {
				sb.append(getTimeStringFromLong(sequence.getTime(r)));
				sb.append("\t");
				sb.append(sequence.getValue(r));
				sb.append("\n");
			}
			StringSelection ss = new StringSelection(sb.toString()); 
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null); 
		}
		catch(IntolerableException ie) {
			UniversalTellUser.general(this, ie);
		}
	}
	
	public static void main(String args[]) {
		TimePlotDataContainer tp = new TimePlotDataContainer("Test name ctor");

		for(int r = 0 ; r < 628 ; r += 8) {
			Date d = new Date(2010,1,r);
			long time = d.getTime();
			double value = Math.sin(((double) r) / 100);
			System.out.println("Time "+time+", value "+value);
			tp.add(time, value, TimePlottable.CROSS);
		}

		//...
		final JFrame frame = new JFrame(tp.getName());
		frame.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          frame.setVisible(false);
		          frame.dispose();
		        }
		      });
		frame.getContentPane().add(new TimePlotComponent(tp), BorderLayout.CENTER);
		try {
			frame.pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(frame, ie.getMessage());
			return;
		}
		frame.setSize(500,350);
		frame.setVisible(true);
	}

}
