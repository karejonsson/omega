package se.modlab.generics.gui.progress;

public interface MonitoredWorker
{
  public String getTasksMessage();
  public String getMessageForBar();
  public String getMessageForDump();
  public boolean isDone();
  public void stop();
  public int getCurrent();
  public int getLengthOfTask();
  public String getName();
}