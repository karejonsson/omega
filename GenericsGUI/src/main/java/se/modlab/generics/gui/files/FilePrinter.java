package se.modlab.generics.gui.files;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;

import javax.swing.*;

import java.io.*;

public class FilePrinter extends JFrame
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public static void main(String arg[])
  {
    new FilePrinter("En fil", "en text\nmera\noch mer");
    //new FilePrinter();
  }

  private PageFormat mPageFormat;
  private FilePageRenderer mPageRenderer;
  String mTitle;
  JMenu page;
  JMenuItem close;
  JMenuItem pri;

  public FilePrinter()
  {
    this(null, null);
  }

  public FilePrinter(String title, String text)
  {
    PrinterJob pj = PrinterJob.getPrinterJob();
    mPageFormat = pj.defaultPage();
    createUI(title, text);
    setVisible(true);
  }

  protected void createUI(String title, String text)
  {
    setSize(300,300);
    center();
    Container c = getContentPane();
    c.setLayout(new FlowLayout());
    JMenuBar mb = new JMenuBar();
    JMenu file = new JMenu("File");

    if(text == null)
    {
      JMenuItem open = new JMenuItem("open");
      open.addActionListener(new FileOpenAction());
      file.add(open);
      close = new JMenuItem("close");
      close.addActionListener(new FileCloseAction());
      close.setEnabled(false);
      file.add(close);
    }
    pri = new JMenuItem("print");
    pri.addActionListener(new FilePrintAction());
    pri.setEnabled(false);
    file.add(pri);
    JMenuItem quit = new JMenuItem("quit");
    quit.addActionListener(new FileQuitAction(this));
    file.add(quit);
    mb.add(file);

    page = new JMenu("Page");
    page.setEnabled(false);

    JMenuItem setup = new JMenuItem("setup");
    setup.addActionListener(new FilePageSetupAction());
    page.add(setup);
    JMenuItem next = new JMenuItem("next");
    next.addActionListener(new PageNextPageAction());
    page.add(next);
    JMenuItem prev = new JMenuItem("previous");
    prev.addActionListener(new PagePreviousPageAction());
    page.add(prev);
    mb.add(page);

    setJMenuBar(mb);
    if(text != null)
    {
      try
      {
        mPageRenderer=new FilePageRenderer(text, mPageFormat);
        if(title == null) title = "No name";
        mTitle="[ "+title+" Being edited ]";
        showTitle();
        JScrollPane jsp=new JScrollPane(mPageRenderer);
        getContentPane().removeAll();
        getContentPane().add(jsp,BorderLayout.CENTER);
        validate();
        page.setEnabled(true);
        pri.setEnabled(true);
      }
      catch(IOException ioe)
      {
        return;
      }
    }
  }

  protected void center()
  {
    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension framesize = getSize();

    int x= (screen.width - framesize.width)/2;
    int y= (screen.height - framesize.height)/2;

    setLocation(x,y);
  }

  public void showTitle()
  {
    int currentPage = mPageRenderer.getCurrentPage() +1;
    int numPages = mPageRenderer.getNumPages();

    setTitle(mTitle+" -page "+currentPage+ " of "+numPages);
  }

  public class FileOpenAction extends AbstractAction
  { 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileOpenAction()
    {
      super("Open");
    }

    public void actionPerformed(ActionEvent ae)
    {
      JFileChooser fc=new JFileChooser(".");
      int result=fc.showOpenDialog(FilePrinter.this);
      if(result!=0)
      {
        return;
      }
      java.io.File f = fc.getSelectedFile();
      if(f==null){return;}
      try
      {
        mPageRenderer=new FilePageRenderer(f,mPageFormat);
        mTitle="["+f.getName()+"]";
        showTitle();
        JScrollPane jsp=new JScrollPane(mPageRenderer);
        getContentPane().removeAll();
        getContentPane().add(jsp,BorderLayout.CENTER);
        validate();
        page.setEnabled(true);
        pri.setEnabled(true);
        if(close != null) close.setEnabled(true);
      }
      catch(java.io.IOException e)
      {
        System.out.println(e);
      }
    }
  }

  public class FileCloseAction extends AbstractAction
  { 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FileCloseAction()
    {
      super("Close");
    }

    public void actionPerformed(ActionEvent ae)
    {
      getContentPane().removeAll();

      validate();
      repaint();
      page.setEnabled(false);
      pri.setEnabled(false);
      if(close != null) close.setEnabled(false);
    }
  }


  public class FilePrintAction extends AbstractAction
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FilePrintAction()
    {
      super("print");
    }

    public void actionPerformed(ActionEvent ae)
    {
      PrinterJob pj=PrinterJob.getPrinterJob();
      pj.setPrintable(mPageRenderer, mPageFormat);
      if(pj.printDialog())
      {
        try
        {
          pj.print();
        }
        catch(PrinterException pe)
        {
          System.out.println(pe);
        }
      }
    }
  }

  public class FilePageSetupAction extends AbstractAction
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FilePageSetupAction()
    {
      super("PageSetup");
    }

    public void actionPerformed(ActionEvent ae)
    {
      PrinterJob pj=PrinterJob.getPrinterJob();
      mPageFormat=pj.pageDialog(mPageFormat);
      if(mPageRenderer!=null)
      {
        mPageRenderer.paginate(mPageFormat);
        showTitle();
      }
    }
  }

  public class FileQuitAction extends AbstractAction
  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Frame f;

    public FileQuitAction(Frame f)
    {
      super("Quit");
      this.f = f;
    }

    public void actionPerformed(ActionEvent ae)
    {
      //System.exit(0);
      f.dispose();
    }
  }

  public class PageNextPageAction extends AbstractAction
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageNextPageAction()
    {
      super("NextPage");
    }

    public void actionPerformed(ActionEvent ae)
    {
      if(mPageRenderer!=null)
        mPageRenderer.nextPage();
      showTitle();
    }
  }

  public class PagePreviousPageAction extends AbstractAction
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PagePreviousPageAction()
    {
      super("PreviousPage");
    }
 
    public void actionPerformed(ActionEvent ae)
    {
      if(mPageRenderer!=null)
      mPageRenderer.previousPage();
      showTitle();
    }
  }
}

