package se.modlab.generics.gui.files;


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.ScrollPane;
import java.awt.Toolkit;

public class MessageDialog
{
  
  public static final int YES = 1;
  public static final int NO = 2;
  public static final int CANCEL = 3;
  public static final int HELP = 4;
  public static final int OK = 5;

  public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height;

  private static FontMetrics metric;
  private static int rowHeight = 0;
  private static int letterWidth = 0;

  public static void setMetric(FontMetrics fm)
  {
    metric = fm;
    rowHeight = metric.getHeight();
    System.out.println("rowHeight "+rowHeight);
    letterWidth = metric.charWidth('W');
    System.out.println("letterWidth "+letterWidth);
  }

  private static int answer = -1;

  public static void setAnswer(Integer i, Dialog d)
  {
    answer = i.intValue();
    d.setVisible(false);
  }

  public static int getOk(Frame parent, String message)
  {
    setMetric(parent.getFontMetrics(new Font("Monospaced",Font.PLAIN,12)));
    int[] a_i = new int[] { OK };
    String[] a_s = new String[] { "Ok" };
    return get(parent, message, a_i, a_s);
  }
  
  public static int getYesNo(Frame parent, String message)
  {
    setMetric(parent.getFontMetrics(new Font("Monospaced",Font.PLAIN,12)));
    int[] a_i = new int[] { YES, NO };
    String[] a_s = new String[] { "Yes", "No" };
    return get(parent, message, a_i, a_s);
  }
  
  public static int getYesNoCancel(Frame parent, String message)
  {
    setMetric(parent.getFontMetrics(new Font("Monospaced",Font.PLAIN,12)));
    int[] a_i = new int[] { YES, NO, CANCEL };
    String[] a_s = new String[] { "Yes", "No", "Cancel" };
    return get(parent, message, a_i, a_s);
  }
  
  public static int getYesNoCancelHelp(Frame parent, String message)
  {
    setMetric(parent.getFontMetrics(new Font("Monospaced",Font.PLAIN,12)));
    int[] a_i = new int[] { YES, NO, CANCEL, HELP };
    String[] a_s = new String[] { "Yes", "No", "Cancel", "Help"};
    return get(parent, message, a_i, a_s);
  }
  
  public static int get(Frame parent, String message, int[] a_i, String[] a_s)
  {
    if(a_i.length != a_s.length)
      {
	System.out.println("MessageDialog get (a_i.length != a_s.length). a_i.length = "+a_i.length+
			   " and a_s.length = "+a_s.length+". Arguments where");
	for(int i = 0 ; i < a_s.length ; i++)
	  System.out.println("Arg "+i+" = "+a_s[i]);
	(new Throwable()).printStackTrace();
	System.exit(1);
      }

    int len = a_i.length;
    int max_len_btn_txt = -1;
    
    for(int i = 0 ; i < len ; i++)
      {
	max_len_btn_txt = (a_s[i].length() > max_len_btn_txt) ? a_s[i].length() : max_len_btn_txt;
      }
    
    Dialog d = new Dialog(parent, "Message", true);
    Panel p_buttons  = new Panel();

    for(int i = 0 ; i < len ; i++)
      {
	Button b = new Button(a_s[i]);
	b.addActionListener(new UniversalActionListener(MessageDialog.class, 
							   "setAnswer", 
							   new Object[] { new Integer(a_i[i]), d }));
	p_buttons.add(b);
      }

    int pos = -1;
    int lines = 0;
    //int maxLength = pos-1;
    Panel p_text  = new Panel();
    p_text.setLayout(null);
    int max_len = -1;

    do
      {
	lines++;
	int pos_prev = pos+1;
	pos = message.indexOf('\n', pos_prev);
	if(pos < 0) pos = message.length();
	int _len = pos - pos_prev;
	max_len = (max_len > _len) ? max_len : _len;
	Label l = new Label(message.substring(pos_prev,pos_prev+_len));
	l.setBounds(letterWidth, 
		    lines*rowHeight,
		    _len*letterWidth,
		    rowHeight);
	l.setFont(metric.getFont());
	p_text.add(l);
      }
    while((pos > 0) && (pos < message.length()));

    p_text.setSize((max_len+2)*letterWidth, (lines+2)*rowHeight);
    //int w_1 = (((max_len > (max_len_btn_txt*len)) ? max_len : max_len_btn_txt*len)+len+1)*letterWidth;
    //int w = (w_1 < parent.getSize().width) ? w_1 : parent.getSize().width ;
    
    ScrollPane sp = new ScrollPane();
    sp.setSize(0,0);

    Panel p_d = new Panel();
    p_d.setLayout(new BorderLayout());
  
    sp.add(p_text);
    p_d.add("Center", sp);
    p_d.add("South", p_buttons);
    d.add(p_d);
    d.pack();
    
    int toGo = parent.getSize().height - d.getSize().height;
    int toAddHeight = 0;
    if(toGo > 0)
      {
	toAddHeight = p_text.getSize().height;
	if(toAddHeight > toGo) toAddHeight = toGo;
      }
    
    toGo = parent.getSize().width - p_buttons.getSize().width;
    int toAddWidth = 0;
    if(toGo > 0)
      {
	toAddWidth = p_text.getSize().width - p_buttons.getSize().width;
	if(toAddWidth > toGo) toAddWidth = toGo;
      }
    
    sp.setSize(toAddWidth, toAddHeight);

    d.setSize(d.getSize().width+toAddWidth+5, d.getSize().height+toAddHeight+5);
    
    int p_y = parent.getLocation().y;
    int p_x = parent.getLocation().x;
    int p_w = parent.getSize().width;
    int p_h = parent.getSize().height;
    int d_w = d.getSize().width;
    int d_h = d.getSize().height;
    d.setLocation(p_x+(p_w-d_w)/2, p_y+(p_h-d_h)/2);
    d.setVisible(true);
    return answer;
  }

}
