package se.modlab.generics.gui.find;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.event.*;

import javax.swing.*;

/** 
 * A sample find dialog
 */
public class FindDialog extends JDialog 
{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/**
  * Command string for a cancel action (e.g., a button or menu item). 
  * This string is never presented to the user and should 
  * not be internationalized.
  */
  private String CMD_CANCEL = "cmd.cancel"/*NOI18N*/;

  /**
   * Command string for a close action (e.g., a button or menu item). 
   * This string is never presented to the user and should 
   * not be internationalized.
   */
  private String CMD_CLOSE = "cmd.close"/*NOI18N*/;

  /**
   * Command string for find action (e.g.,a button or menu item). 
   * This string is never presented to the user and should 
   * not be internationalized.
   */
  private String CMD_FIND = "cmd.find"/*NOI18N*/;

  // Components we need to manipulate after creation
  private JButton findButton = null;
  private JButton closeButton = null;

  private FindAction fa = null;
  private JCheckBox wholeWordCheckBox = null;
  private JCheckBox matchCaseCheckBox = null;
  private JRadioButton wrapAroundRadio = null;
  private JRadioButton startAtTopRadio = null;
  private JTextField textField = null;

 /** 
  * Creates new FindDialog
  */
  public FindDialog(Frame parent, boolean modal, FindAction fa) 
  {
    super(parent, modal);
    this.fa = fa;
    initResources();
    initComponents();
    pack();
  }

 /**
  * Loads locale-specific resources: strings, images, et cetera
  */
  private void initResources() 
  {
  }

 /**
  * 
  * Called from the constructor to initialize this window
  * 
  * 
  * We use dynamic layout managers, so that layout is dynamic and will
  * adapt properly to user-customized fonts and localized text. The
  * GridBagLayout makes it easy to line up components of varying
  * sizes along invisible vertical and horizontal grid lines. It
  * is important to sketch the layout of the interface and decide
  * on the grid before writing the layout code. 
  * 
  * 
  * Here we actually use
  * our own subclass of GridBagLayout called StringGridBagLayout,
  * which allows us to use strings to specify constraints, rather
  * than having to create GridBagConstraints objects manually.
  * 
  * 
  * We use the JLabel.setLabelFor() method to connect
  * labels to what they are labeling. This allows mnemonics to work
  * and assistive to technologies used by persons with disabilities
  * to provide much more useful information to the user.
  * 
  */
  private void initComponents() 
  {
    // Set properties on the dialog, itself
    setTitle("Find dialog");
    addWindowListener (new WindowAdapter () {
            public void windowClosing(WindowEvent event) {
		// user hit window manager close button
                windowAction(CMD_CANCEL);
            }
        });
    // accessibility - all frames, dialogs, and applets should have
    // a description
    getAccessibleContext().setAccessibleDescription("Find dialog");

    // Build the contents
    Container contents = getContentPane();
    contents.setLayout(new StringGridBagLayout());

    // create here, because we need it immediately below
    textField = new JTextField();

    // "find" label
    JLabel findLabel = new JLabel();
    findLabel.setDisplayedMnemonic('F');
    findLabel.setText("Find:");
    findLabel.setLabelFor(textField);
    contents.add("insets=[12,12,0,0]",findLabel);

    // text field for string to be found
    textField.setToolTipText("Text to find here");
    contents.add(
            "gridwidth=2,fill=HORIZONTAL,anchor=WEST,weightx=1.0"
	    +",insets=[12,12,0,11]", 
       	    textField);

    // This checkbox is needed by the "match case" checkbox
    wholeWordCheckBox = new JCheckBox (); 

    matchCaseCheckBox = new JCheckBox ();
    matchCaseCheckBox.setMnemonic('M');
    //matchCaseCheckBox.setNextFocusableComponent(wholeWordCheckBox);
    matchCaseCheckBox.setMargin(new Insets(0, 2, 0, 2));
    matchCaseCheckBox.setToolTipText("Match upper or lower case");
    matchCaseCheckBox.setText("Match case");
    matchCaseCheckBox.setSelected(true);
    matchCaseCheckBox.setEnabled(false);
    contents.add(
	    "gridx=1,gridy=1,anchor=WEST,insets=[12,12,0,0]",
	    matchCaseCheckBox);

    // needed below
    startAtTopRadio = new JRadioButton();

	// "whole case" checkbox
        wholeWordCheckBox.setMnemonic('W');
        //wholeWordCheckBox.setNextFocusableComponent(startAtTopRadio);
        wholeWordCheckBox.setMargin(new Insets(0, 2, 0, 2));
        wholeWordCheckBox.setToolTipText("Match whole word");
        wholeWordCheckBox.setText("Whole word");
        wholeWordCheckBox.setSelected(true);
        wholeWordCheckBox.setEnabled(false);
        contents.add(
	    "gridx=1,gridy=2,anchor=WEST,insets=[0,12,0,0]",
	    wholeWordCheckBox);
    
	// needed below
        wrapAroundRadio = new JRadioButton();

	// "start at top"
        startAtTopRadio.setMnemonic('S');
        //startAtTopRadio.setNextFocusableComponent(wrapAroundRadio);
        startAtTopRadio.setMargin(new Insets(0, 2, 0, 2));
        startAtTopRadio.setToolTipText("Start at Top imperatively");
        startAtTopRadio.setSelected(true);
        startAtTopRadio.setText("Start at Top");
        contents.add(
	    "gridx=2,gridy=1,anchor=WEST,insets=[12,12,0,11]",
	    startAtTopRadio);

	// "wrap around"
        wrapAroundRadio.setMnemonic('a');
        wrapAroundRadio.setMargin(new Insets(0, 2, 0, 2));
        wrapAroundRadio.setToolTipText("Wrap around if strikes end");
        wrapAroundRadio.setText("Wrap around");
        contents.add(
	    "gridx=2,gridy=2,anchor=WEST,insets=[0,12,0,11]",
	    wrapAroundRadio);

        // group radio buttons
        ButtonGroup group1 = new ButtonGroup();
        group1.add(startAtTopRadio);
        group1.add(wrapAroundRadio);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, 0));

	// find button
        findButton = new JButton();
        findButton.setText("Find");
	findButton.setActionCommand(CMD_FIND);
	findButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
	        windowAction(event);
	    }
	});
        buttonPanel.add(findButton);

	// space
        buttonPanel.add(Box.createRigidArea(new Dimension(5,0)));
  
        // close button
        closeButton = new JButton();
        closeButton.setMnemonic('C');
        closeButton.setText("Close");
	closeButton.setActionCommand(CMD_CLOSE);
	closeButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
	        windowAction(event);
	    }
	});
        buttonPanel.add(closeButton);

	// add the button panel
	contents.add(
	    "gridx=0,gridy=3,gridwidth=3,anchor=EAST,insets=[17,12,11,11]",
	    buttonPanel);

	// Make all the buttons the same size
        equalizeButtonSizes();

	// Set up the default button for the dialog
        getRootPane().setDefaultButton(findButton);

    } // initComponents()

    /**
     * Sets all of the buttons to be the same size. This is done
     * dynamically by setting each button's preferred and maximum
     * sizes after the buttons are created. This way, the layout
     * automatically adjusts to the locale-specific strings.
     */
    private void equalizeButtonSizes() {

        String[] labels = new String[] { 
            findButton.getText(), 
	    closeButton.getText()
	};
      
	// Get the largest width and height
	Dimension maxSize= new Dimension(0,0);
	Rectangle2D textBounds = null;
	//Dimension textSize = null;
        FontMetrics metrics = 
		findButton.getFontMetrics(findButton.getFont());
	Graphics g = getGraphics();
	for (int i = 0; i < labels.length; ++i) {
	    textBounds = metrics.getStringBounds(labels[i], g);
	    maxSize.width = 
	        Math.max(maxSize.width, (int)textBounds.getWidth());
	    maxSize.height = 
	        Math.max(maxSize.height, (int)textBounds.getHeight());
	}
      
        Insets insets = 
	    findButton.getBorder().getBorderInsets(findButton);
        maxSize.width += insets.left + insets.right;
        maxSize.height += insets.top + insets.bottom;
      
        // reset preferred and maximum size since BoxLayout takes both 
	// into account 
        findButton.setPreferredSize((Dimension)maxSize.clone());
        findButton.setMaximumSize((Dimension)maxSize.clone());
        closeButton.setPreferredSize((Dimension)maxSize.clone());
        closeButton.setMaximumSize((Dimension)maxSize.clone());
    } // equalizeButtonSizes()

    /**
     * The user has selected an option. Here we close and dispose the dialog.
     * If actionCommand is an ActionEvent, getCommandString() is called,
     * otherwise toString() is used to get the action command.
     *
     * @param actionCommand may be null
     */
    private void windowAction(Object actionCommand) 
    {
      String cmd = null;
      if(actionCommand != null) 
      {
	if(actionCommand instanceof ActionEvent) 
        {
	  cmd = ((ActionEvent)actionCommand).getActionCommand();
	} 
        else 
        {
	  cmd = actionCommand.toString();
	}
      }
      if(cmd == null) 
      {
        return;
      } 
      if(cmd.equals(CMD_CANCEL)) 
      {
	//System.out.println("your 'cancel' code here...");
	setVisible(false);
	//dispose();
        fa.cancelAction();
        return;
      } 
      if(cmd.equals(CMD_CLOSE)) 
      {
	//System.out.println("your 'close' code here...");
	setVisible(false);
	//dispose();
        fa.closeAction();
        return;
      } 
      if(cmd.equals(CMD_FIND)) 
      {
	//System.out.println("your 'find' code here...");
        fa.findAction(textField.getText(),
                      wholeWordCheckBox.isSelected(),
                      matchCaseCheckBox.isSelected(),
                      wrapAroundRadio.isSelected(),
                      startAtTopRadio.isSelected());
        if(startAtTopRadio.isSelected()) wrapAroundRadio.doClick();
        return;
      }
    } 

    /**
     * This main() is provided for debugging purposes, to display a 
     * sample dialog.
     */
    public static void main(String args[]) 
    {
	JFrame frame = new JFrame() {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public Dimension getPreferredSize() {
	        return new Dimension(200,100);
	    }
	};
	frame.setTitle("Debugging frame");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.pack();
	frame.setVisible(false);
        FindAction fa = 
          new FindAction()
          { 
            public void cancelAction()
            {
              System.out.println("cancelAction - FindAction");
            }

            public void closeAction()
            {
              System.out.println("closeAction - FindAction");
            }

            public void findAction(String s,
                                   boolean wholeWord,
                                   boolean matchCase,
                                   boolean wrapAround,
                                   boolean startAtTop)
            {
              System.out.println("findAction - FindAction '"+s+"'\n"+
                                 "whole word "+wholeWord+"\n"+
                                 "match case "+matchCase+"\n"+
                                 "wrap around "+wrapAround+"\n"+
                                 "start at top "+startAtTop);
            }
          };

        FindDialog dialog = new FindDialog(frame, true, fa);
	dialog.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent event) {
	        System.exit(1);
	    }
	    public void windowClosed(WindowEvent event) {
	        System.exit(1);
	    }
	});
	dialog.pack();
	dialog.setVisible(true);
    } // main()
  
} // class FindDialog

