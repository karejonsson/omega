package se.modlab.generics.gui.files;

import se.modlab.generics.gui.find.FindAction;
import se.modlab.generics.gui.find.FindDialog;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.*;
import java.util.*;

import javax.swing.text.*;
import javax.swing.undo.*;
import javax.swing.event.*;
import javax.swing.*;

public class OneFileNotepad extends JPanel implements UndoableEditListener {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
private String filename;
  private File f;
  private final JTextArea editor = new JTextArea();

  private UndoAction undoAction = new UndoAction();
  private RedoAction redoAction = new RedoAction();
  private DefaultEditorKit.CutAction cutAction = null;
  private DefaultEditorKit.CopyAction copyAction = null;
  private DefaultEditorKit.PasteAction pasteAction = null;
  private SaveAction saveAction = new SaveAction();
  private SaveasAction saveasAction = new SaveasAction();
  private PrintAction printAction = new PrintAction();
  private ExitAction exitAction = new ExitAction();
  private LineAction lineAction = new LineAction();
  private SearchAction searchAction = new SearchAction();

  private JMenuBar menubar = null;
  protected FileDialog fileDialog;
  protected UndoManager undo = new UndoManager();

  OneFileNotepad(String filename) 
  {
    super(true);
    this.filename = filename;
    try 
    {
      UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
    } 
    catch(Exception exc) 
    {
      System.err.println("Error loading L&F: " + exc);
    }

    setBorder(BorderFactory.createEtchedBorder());
    setLayout(new BorderLayout());
    editor.setFont(new Font("Courier", Font.PLAIN, 12));
    editor.getDocument().addUndoableEditListener(this);

    Action[] actions = editor.getActions();
    for (int i = 0; i < actions.length; i++) 
    {
      Action a = actions[i];
      if(a instanceof DefaultEditorKit.CutAction) 
        cutAction = (DefaultEditorKit.CutAction) a;
      if(a instanceof DefaultEditorKit.CopyAction) 
        copyAction = (DefaultEditorKit.CopyAction) a;
      if(a instanceof DefaultEditorKit.PasteAction) 
        pasteAction = (DefaultEditorKit.PasteAction) a;
    }
	
    JScrollPane scroller = new JScrollPane();
    JViewport port = scroller.getViewport();
    port.add(editor);
    try 
    {
      port.setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
    } 
    catch(MissingResourceException mre) 
    {
      // just use the viewport default
    }

    menubar = createMenubar();
    add("North", menubar);
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());	
    panel.add("Center", scroller);
    add("Center", panel);

    File _f = new File(filename);
    if (_f.exists()) 
    {
      try
      {
        f = _f;
        PlainDocument pd = new PlainDocument();
        editor.setDocument(pd);
        pd.addUndoableEditListener(OneFileNotepad.this);
        FileReader in = new FileReader(f);
        char[] buff = new char[4096];
        int nch;
        while((nch = in.read(buff, 0, buff.length)) != -1) 
        {
          pd.insertString(pd.getLength(), new String(buff, 0, nch), null);
        }
        pd.addUndoableEditListener(OneFileNotepad.this);
      }
      catch(BadLocationException be)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While reading file\n"+f.getAbsolutePath(), 
                                      "Bad location exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      catch(FileNotFoundException e) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to read file\n"+f.getAbsolutePath(), 
                                      "File not found exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      catch(IOException e) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to read file\n"+f.getAbsolutePath(), 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
    else
    {
      PlainDocument pd = new PlainDocument();
      pd.addUndoableEditListener(OneFileNotepad.this);
      editor.setDocument(pd);
    }
  }

  public static void main(String[] args) 
  {
    OneFileNotepad ofn = note("NISSEFIE");
    ofn.setText("<sdgmnaklsdgaklsg<ksdg");
  }

  public static OneFileNotepad note(String filename) 
  {
    JFrame frame = null;
    OneFileNotepad ofn = null;    
    try 
    { 
      ofn = new OneFileNotepad(filename);
      frame = new JFrame();
      frame.setTitle(filename);
      frame.setBackground(Color.lightGray);
      frame.getContentPane().setLayout(new BorderLayout());
      frame.getContentPane().add("Center", ofn);
      frame.pack();
      frame.setSize(500, 600);
      frame.setVisible(true);
    } 
    catch(Throwable t) 
    {
      System.out.println("uncaught exception: " + t);
      t.printStackTrace();
    }
    return ofn;
  }

  public void undoableEditHappened(UndoableEditEvent e) 
  {/*AbstractDocument.DefaultDocumentEvent(int offs, int len, DocumentEvent.EventType
    
    AbstractDocument.DefaultDocumentEvent addde = (AbstractDocument.DefaultDocumentEvent) e.getEdit();
    
    System.out.println(e.getEdit().getClass().getName());
    System.out.println(addde.getType().getClass().getName()); */
    undo.addEdit(e.getEdit());
    undoAction.update();
    redoAction.update();
  }

  public JFrame getFrame() 
  {
    for (Container p = getParent(); p != null; p = p.getParent()) 
    {
      if (p instanceof Frame) 
      {
	return (JFrame) p;
      }
    }
    return null;
  }

  protected JMenuBar createMenubar() 
  {
    JMenuItem mi;
    JMenuBar mb = new JMenuBar();
    
    JMenu file = new JMenu("File");
    mb.add(file);
    JMenu edit = new JMenu("Edit");
    mb.add(edit);
    mi = new JMenuItem("save");
    mi.addActionListener(saveAction);
    file.add(mi);
    mi = new JMenuItem("save as");
    mi.addActionListener(saveasAction);
    file.add(mi);
    mi = new JMenuItem("print");
    mi.addActionListener(printAction);
    file.add(mi);
    file.addSeparator();
    mi = new JMenuItem("exit");
    mi.addActionListener(exitAction);
    file.add(mi);
    
    mi = new JMenuItem("cut");
    mi.addActionListener(cutAction);
    cutAction.addPropertyChangeListener(new ActionChangedListener(mi));
    edit.add(mi);
    mi = new JMenuItem("copy");
    mi.addActionListener(copyAction);
    copyAction.addPropertyChangeListener(new ActionChangedListener(mi));
    edit.add(mi);
    mi = new JMenuItem("paste");
    mi.addActionListener(pasteAction);
    pasteAction.addPropertyChangeListener(new ActionChangedListener(mi));
    edit.add(mi);
    edit.addSeparator();
    mi = new JMenuItem("line");
    mi.addActionListener(lineAction);
    edit.add(mi);
    mi = new JMenuItem("search");
    mi.addActionListener(searchAction);
    edit.add(mi);
    edit.addSeparator();
    mi = new JMenuItem("undo");
    mi.addActionListener(undoAction);
    undoAction.addPropertyChangeListener(new ActionChangedListener(mi));
    mi.setEnabled(false);
    edit.add(mi);
    mi = new JMenuItem("redo");
    mi.addActionListener(redoAction);
    redoAction.addPropertyChangeListener(new ActionChangedListener(mi));
    mi.setEnabled(false);
    edit.add(mi);

    return mb;
  }

  // Yarked from JMenu, ideally this would be public.
  private class ActionChangedListener implements PropertyChangeListener 
  {
    JMenuItem menuItem;
        
    ActionChangedListener(JMenuItem mi) 
    {
      super();
      this.menuItem = mi;
    }

    public void propertyChange(PropertyChangeEvent e) 
    {
      String propertyName = e.getPropertyName();
      //System.out.println("ActionChangedListener-propertyChange "+propertyName+" - "+Action.NAME);
      if(e.getPropertyName().equals(Action.NAME)) 
      {
	String text = (String) e.getNewValue();
	menuItem.setText(text);
        return;
      }
      if(propertyName.equals("enabled")) 
      {
	Boolean enabledState = (Boolean) e.getNewValue();
	menuItem.setEnabled(enabledState.booleanValue());
      }
    }
  }

/*
 Some sets and gets
 */
  public void setText(String s)
  {
    Document doc = editor.getDocument();
    if(doc == null) return;
    try 
    {
      doc.removeUndoableEditListener(OneFileNotepad.this);
      doc.remove(0, doc.getLength());
      doc.insertString(0, s, null);
      doc.addUndoableEditListener(OneFileNotepad.this);
      undo.discardAllEdits();
    }
    catch(BadLocationException _e)
    {
    }
  }

  public void insertAtCaretPosition(String s)
  {
    try 
    {
      Document doc = editor.getDocument();
      doc.insertString(editor.getCaretPosition(), s, null);
    }
    catch(BadLocationException _e)
    {
    }
  }

  public String getText()
  {
    Document doc = editor.getDocument();
    if(doc == null) return null;
    try 
    {
      return doc.getText(0, doc.getLength());
    }
    catch(BadLocationException _e)
    {
    }
    return null;
  }
   
  public File getFile()
  {
    return f;
  }

  public JMenuBar getMenubar()
  {
    return menubar;
  }

  class UndoAction extends AbstractAction 
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UndoAction() 
    {
      super("Undo");
      setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) 
    {
      try 
      {
	undo.undo();
      } 
      catch(CannotUndoException ex) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "Undo not possible right now", 
                                      "Cannot undo exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      update();
      redoAction.update();
    }

    protected void update() 
    {
      if(undo.canUndo())
      {
	setEnabled(true);
	putValue(Action.NAME, undo.getUndoPresentationName());
      }
      else 
      {
	setEnabled(false);
	putValue(Action.NAME, "Undo");
      }
    }
  }

  class RedoAction extends AbstractAction 
  {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RedoAction() 
    {
      super("Redo");
      setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) 
    {
      try 
      {
	undo.redo();
      } 
      catch(CannotRedoException ex) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "Redo not possible right now", 
                                      "Cannot redo exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      update();
      undoAction.update();
    }

    protected void update()
    {
      if(undo.canRedo())
      {
	setEnabled(true);
	putValue(Action.NAME, undo.getRedoPresentationName());
      }
      else
      {
	setEnabled(false);
	putValue(Action.NAME, "Redo");
      }
    }
  }

  class LineAction implements ActionListener 
  {

    public void actionPerformed(ActionEvent e) 
    {
      String inputValue = 
        JOptionPane.showInputDialog(getFrame(), 
          "Type lines number (0 to "+OneFileNotepad.this.editor.getLineCount()+")");
      if(inputValue == null) return;
      try
      {
        int line = Integer.parseInt(inputValue);
        activateLine(line);
      }
      catch(NumberFormatException _e)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While analysing '"+inputValue+"' as a number.", 
                                      "Number format exception", 
                                      JOptionPane.ERROR_MESSAGE);
        return;
      }
    }  

    public void activateLine(int line)
    {
      int pos = 0;
      if(!((line <= OneFileNotepad.this.editor.getLineCount()) && (line >= 0)))
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "Lines ranges from 0 to "+OneFileNotepad.this.editor.getLineCount()+".", 
                                      "Line lookup failed", 
                                      JOptionPane.ERROR_MESSAGE);
        return;
      }
      String text = OneFileNotepad.this.editor.getText();
      for(int i = 0 ; ((i < line-1) && (pos < text.length())) ; i++)
      {
        pos = text.indexOf('\n', pos+1);
      }
      OneFileNotepad.this.editor.setCaretPosition(pos+1);
    }

  }

  class PrintAction implements ActionListener 
  {
    public void actionPerformed(ActionEvent e) 
    {
      new FilePrinter(OneFileNotepad.this.filename, 
                      OneFileNotepad.this.getText());
    }  
  }

  class SearchAction implements ActionListener 
  {
    FindAction fa = 
          new FindAction()
          { 
            public void cancelAction() { }
            public void closeAction() { }
            public void findAction(String s,
                                   boolean wholeWord,
                                   boolean matchCase,
                                   boolean wrapAround,
                                   boolean startAtTop)
            {
              SearchAction.this.nextAction(s, startAtTop);
            }
          };
    FindDialog d = new FindDialog(OneFileNotepad.this.getFrame(), true, fa);

    public void actionPerformed(ActionEvent e) 
    {
      d.pack();
      d.setVisible(true);
    }

    private void nextAction(String text, boolean startattop)
    {
      JTextArea ed = OneFileNotepad.this.editor;
      if(startattop) 
      {
        ed.setCaretPosition(0);
      }
      //int old_pos = ed.getCaretPosition();
      int pos = ed.getCaretPosition();
      String str = ed.getText();
      int new_pos = str.indexOf(text, pos);
      if((new_pos < 0) && (startattop))
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "Sequence not found!", 
                                      "Find message", 
                                      JOptionPane.OK_OPTION);
        return;
      }
      if(new_pos == -1)
      {
        JOptionPane.showConfirmDialog(getFrame(),
                                      "Find wrapps to start from top again!", 
                                      "Find message", 
                                      JOptionPane.OK_OPTION);
        nextAction(text, true);
        return;
      }
      else
      {
        ed.setCaretPosition(new_pos);
        ed.setSelectionStart(new_pos);
        ed.setSelectionEnd(new_pos+text.length());
      }
    }
  }

  class ExitAction implements ActionListener 
  {
    public void actionPerformed(ActionEvent e) 
    {
      OneFileNotepad.this.getFrame().setVisible(false);
    }
  }

  class SaveAction implements ActionListener 
  {

    public void actionPerformed(ActionEvent e) 
    {
      Document doc = OneFileNotepad.this.editor.getDocument();
      String text = null;
      
      try 
      {
	text = doc.getText(0, doc.getLength());
      }
      catch(BadLocationException _e)
      {
      }
      
      if(filename == null)
      {
	FileDialog fd = new FileDialog(OneFileNotepad.this.getFrame());
	//fd.show();
	if(fd.getDirectory() == null) return;
	OneFileNotepad.this.filename = fd.getDirectory();
	if(OneFileNotepad.this.filename != null) OneFileNotepad.this.filename += fd.getFile();
      }

      if(OneFileNotepad.this.filename == null) return;

      java.io.FileOutputStream fos = null;
      try
      {
	fos = new java.io.FileOutputStream(OneFileNotepad.this.filename);
	fos.write(text.getBytes());
	fos.close();
	return;
      }
      catch(java.io.IOException __e)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to write file "+OneFileNotepad.this.filename, 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      try
      {
	if(fos != null) fos.close();
      }
      catch(java.io.IOException ___e)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to close file "+OneFileNotepad.this.filename, 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  class SaveasAction implements ActionListener
  {

    public void actionPerformed(ActionEvent e) 
    {
      Document doc = OneFileNotepad.this.editor.getDocument();
      String text = null;
      
      try 
      {
        text = doc.getText(0, doc.getLength());
      }
      catch(BadLocationException _e)
      {
      }
      
      FileDialog fd = new FileDialog(OneFileNotepad.this.getFrame());
      fd.setVisible(true);
      if(fd.getDirectory() == null) return;
      filename = fd.getDirectory();
      if(filename != null)
        filename += fd.getFile();

      if(filename == null)
	return;

      java.io.FileOutputStream fos = null;
      try
      {
        fos = new java.io.FileOutputStream(filename);
        fos.write(text.getBytes());
        fos.close();
        return;
      }
      catch(java.io.IOException __e)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to write file "+filename, 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      try
      {
        if(fos != null) fos.close();
      }
      catch(java.io.IOException ___e)
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to close file "+filename, 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }


  /**
   * Thread to load a file into the text storage model
   */
  class FileLoader extends Thread 
  {

    Document doc;
    File f;

    FileLoader(File f, Document doc) 
    {
      setPriority(4);
      this.f = f;
      this.doc = doc;
    }

    public void run() 
    {
      try 
      {/*
	JProgressBar progress = new JProgressBar();
	progress.setMinimum(0);
	progress.setMaximum((int) f.length());*/

	Reader in = new FileReader(f);
	char[] buff = new char[4096];
	int nch;
	while ((nch = in.read(buff, 0, buff.length)) != -1) 
        {
	  doc.insertString(doc.getLength(), new String(buff, 0, nch), null);
	  //progress.setValue(progress.getValue() + nch);
	}

	// we are done... get rid of progressbar
	doc.addUndoableEditListener(OneFileNotepad.this);
      }
      catch(FileNotFoundException e) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to read file\n"+f.getAbsolutePath(), 
                                      "File not found exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      catch(IOException e) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to read file\n"+f.getAbsolutePath(), 
                                      "I/O exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
      catch(BadLocationException e) 
      {
        JOptionPane.showMessageDialog(getFrame(),
                                      "While attempting to read file\n"+f.getAbsolutePath(), 
                                      "Bad location exception", 
                                      JOptionPane.ERROR_MESSAGE);
      }
    }
  }

}
