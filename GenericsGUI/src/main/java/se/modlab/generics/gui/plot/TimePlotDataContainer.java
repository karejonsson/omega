package se.modlab.generics.gui.plot;

import java.util.Comparator;
import java.util.Vector;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.util.Sort;

public class TimePlotDataContainer implements TimePlottable {
	
	private String name;
	
	public TimePlotDataContainer(String _name) {
		name = _name;
	}
	
	private static class Pairs {
		public long time;
		public double value;
		public int shape;
		public Pairs(long time, double value, int shape) {
			this.time = time;
			this.value = value;
			this.shape = shape;
		}
	}
	 
	private Comparator comp = new Comparator() {
		public int compare(Object arg0, Object arg1) {
			Pairs rn0 = (Pairs) arg0;
			Pairs rn1 = (Pairs) arg1;
			if(rn1.time < rn0.time) {
				return 1; 
			}
			return -1;
		}
	};
	
	private double max;
	private double min;
	private Vector<Pairs> pairs = null;

	public void add(long time, double value) {
		add(time, value, BOX); 
	}

	public void add(long time, double value, int shape) {
		if(pairs == null) {
			pairs = new Vector<Pairs>();
			max = value;
			min = value;
		}
		Sort.add(pairs, new Pairs(time, value, shape), comp);
		max = Math.max(max, value);
		min = Math.min(min, value);
	}

	public double getMaxValue() {
		return max;
	}

	public double getMinValue() {
		return min;
	}

	public int getSize() {
		return pairs.size();
	}

	public long getTime(int i) throws IntolerableException {
		return pairs.elementAt(i).time;
	}

	public double getValue(int i) throws IntolerableException {
		return pairs.elementAt(i).value;
	}

	public int getShape(int i) throws IntolerableException {
		return pairs.elementAt(i).shape;
	}

	public String getName() {
		return name;
	}

}
