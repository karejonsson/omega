package se.modlab.generics.gui.find;

public interface FindAction
{
  public void cancelAction();
  public void closeAction();
  public void findAction(String s,
                         boolean wholeWord,
                         boolean matchCase,
                         boolean wrapAround,
                         boolean startAtTop);
}
