package se.modlab.generics.gui.find;

public interface AskForVisibleTextsAction
{
  public void cancelAction();
  public void helpAction();
  public void askforvisibletextsAction(String texts[]);
}
