package se.modlab.generics.gui.plot;

import se.modlab.generics.exceptions.IntolerableException;

public interface CrossPlottable {

	public static final int BOX = 1;
	public static final int CROSS = 2;
	public static final int CIRCLE = 3;
	
	public int getSize();
 	  
	public double getMaxValueAxis1();
	public double getMinValueAxis1();
	public double getMaxValueAxis2();
	public double getMinValueAxis2();
 		
	public double getValueAx1(int i) throws IntolerableException;
	public double getValueAx2(int i) throws IntolerableException;
	public int getShape(int i) throws IntolerableException;

	public String getNameAx1();
	public String getNameAx2();

}
