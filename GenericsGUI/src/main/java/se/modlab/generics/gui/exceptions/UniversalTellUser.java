package se.modlab.generics.gui.exceptions;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.exceptions.UserBreak;
import se.modlab.generics.exceptions.UserError;
import se.modlab.generics.files.*;
import se.modlab.generics.gui.files.OneFileNotepad;

import java.awt.*;
import java.io.*;

import javax.swing.*;

public class UniversalTellUser
{

  private static final String comment = 
    "<COMMENT>\n"+
    "This is the place to put your own commentary. If you wish to get\n"+
    "support for this problem save this file and send it by mail\n"+
    "(encrypted if you wish) without manipulation. This makes it more\n"+
    "likely that the problem can be found. If you cannot share\n"+
    "your data with the support make a copy of all your files and change\n"+
    "the sensitive data and try to reproduce this problem with them but\n"+
    "make sure that what you send still has the problem.\n"+
    "</COMMENT>\n"; 

  /*
  private static Exception getException(Throwable e)
  {
    if(!(e instanceof InvocationTargetException))
    {  
      return (Exception)e;
    }
    while(e instanceof InvocationTargetException)
    {
      e = ((InvocationTargetException) e).getTargetException();
    }
    return (Exception)e;
  }
	*/
  
  private static String getCallstack(Throwable t)
  {
    StackTraceElement ets[] = t.getStackTrace();
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < ets.length ; i++)
    {
      sb.append(ets[i].toString()+"\n");
    }
    return sb.toString();
  }

/*
  public static Frame getFrame(Component c)
  {
    if(c == null) return null;
    if(c instanceof Frame) return (Frame) c;
    return getFrame(c.getParent());
  }
 */

  public static Component getFrame(Component c)
  {
    if(c == null) return null;
    if(c instanceof Frame) return c;
    if(c instanceof JFrame) return c;
    return getFrame(c.getParent());
  }

  public static void info(Component c, String message)
  {
    Component f = getFrame(c);
    info(f, message, "Information");
  }

  public static void info(Component c, String message, String title)
  {
    Component f = getFrame(c);
    JOptionPane.showMessageDialog(f, message, title, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void error(Component c, String message)
  {
    Component f = getFrame(c);
    error(f, message, "Error");
  }

  public static void error(Component c, String message, String title)
  {
    Component f = getFrame(c);
    JOptionPane.showMessageDialog(f, message, title, JOptionPane.ERROR_MESSAGE);
  }

  private static boolean error_intern(Component c, String message, String title)
  {
    Component f = getFrame(c);
    Object[] options = { "OK", "NO" };
    //String trueMessage = message+"\n\nDo you want to see the support message?";
    int choice = JOptionPane.showOptionDialog(f, 
                                              message, 
                                              title,
                                              JOptionPane.DEFAULT_OPTION, 
                                              JOptionPane.ERROR_MESSAGE,
                                              null, 
                                              options, 
                                              options[1]);
    return choice == 0;
  }

  public static boolean info_ask_for_yes_propose_no(Component c, String message, String title) {
	  return info_ask_for_yes_propose(c, message, title, 1, JOptionPane.INFORMATION_MESSAGE);
  }

  public static boolean error_ask_for_yes_propose_no(Component c, String message, String title) {
	  return info_ask_for_yes_propose(c, message, title, 1, JOptionPane.ERROR_MESSAGE);
  }

  public static boolean info_ask_for_yes_propose_yes(Component c, String message, String title) {
	  return info_ask_for_yes_propose(c, message, title, 0, JOptionPane.INFORMATION_MESSAGE);
  }

  public static boolean error_ask_for_yes_propose_yes(Component c, String message, String title) {
	  return info_ask_for_yes_propose(c, message, title, 0, JOptionPane.ERROR_MESSAGE);
  }

  private static boolean info_ask_for_yes_propose(Component c, String message, String title, int _default, int degree)
  {
    Component f = getFrame(c);
    Object[] options = { "OK", "NO" };
    //String trueMessage = message+"\n\nDo you want to see the support message?";
    int choice = JOptionPane.showOptionDialog(f, 
                                              message, 
                                              title,
                                              JOptionPane.DEFAULT_OPTION, 
                                              degree,
                                              null, 
                                              options, 
                                              options[_default]);
    return choice == 0;
  }

  private static String getShort(String s)
  {
    int endpos = s.indexOf('\n', 0);
    //System.out.println("endpos = "+endpos);
    for(int i = 0 ; (i < 20) && ((endpos != -1) && (s.length() > endpos)); i++)
    {
      endpos = s.indexOf('\n', endpos+1);
      //System.out.println("endpos = "+endpos);
    }
    if(endpos == -1) return s;
    return s.substring(0, endpos-1)+"\n\n<This message has been cut.>";
  }

  public static void general(Component c, IntolerableException ie)
  {
    Component f = getFrame(c);
    general(f, ie, null);
  }

  public static void general(Component c, IntolerableException ie, String title)
  {
    if(ie instanceof UserError) general(c, (UserError) ie, title);
    if(ie instanceof UserBreak) general(c, (UserBreak) ie, title);
    if(ie instanceof InternalProgrammingError) general(c, (InternalProgrammingError) ie, title);
    if(ie instanceof SystemError) general(c, (SystemError) ie, title);
    if(ie instanceof UnclassedError) general(c, (UnclassedError) ie, title);
  }

  private static final String uemsg = 
      "\n\nThis is an error type that means that the user made some kind of\n"+
      "error. It may not be so and if you feel that this is not correct,\n"+
      "please see the full error dump and send it to the support function\n"+
      "of your supplier. Thanks in advance for any help to improve Simplesim.\n\n"+
      "Do you want to see the full error dump?";

  private static final String calmmsg = 
      "The editor behind shows an error dump. It it not written to your file\n"+
      "system unless you do that after inspection. If you do save it from the\n"+
      "menubar it will be saved as\n";

  private static void general(Component c, UserBreak ub, String title)
  {
    info(null, "Terminated on users request", "User break");
  }

  private static void general(Component c, UserError ue, String title)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(ue.getMessage()+"\n");
    Throwable t = ue.getThrowable();
    if(t != null)
    {
      sb.append(getGentleUnderlyingReport(t));
    }
    
    if(title == null) title = "User error";
    String firstMsg = getShort(sb.toString())+uemsg;
    if(!error_intern(c, getShort(sb.toString())+uemsg, title)) return;
    firstMsg = sb.toString()+uemsg;

    sb = new StringBuffer();
    sb.append("<ERRORDUMP>\n");
    sb.append("<TITLE>\n"+title+"\n</TITLE>\n");
    sb.append("<MESSAGE>\n"+firstMsg+"\n</MESSAGE>\n");
    sb.append("<ACTION>\n"+ue.getAction()+"\n</ACTION>\n");
    sb.append(comment+"\n");
    sb.append("<CALLSTACK>\n"+getCallstack(ue)+"\n</CALLSTACK>\n");
    if(ue.getThrowable() != null)
    {
      sb.append("<MESSAGEUNDER>\n"+
                ue.getThrowable().getMessage()+
                "\n</MESSAGEUNDER>\n");
      sb.append("<MESSAGETYPEUNDER>\n"+
                ue.getThrowable().getClass().getName()+
                "\n</MESSAGETYPEUNDER>\n");
      sb.append("<CALLSTACKUNDER>\n"+
                getCallstack(ue.getThrowable())+
                "\n</CALLSTACKUNDER>\n");
    }
    FileCollector fc[] = ue.getCollectors();
    if(fc != null)
    {
      for(int i = 0 ; i < fc.length ; i++)
      {
        sb.append("<FILE>\n"+ 
                  "<FILENAME>\n"+fc[i].getFilename()+"\n</FILENAME>\n"+
                  "<FILECONTENTS>\n"+fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                  "</FILE>\n");
      }
    }
    sb.append("</ERRORDUMP>\n");

    String filename = getDumpsFilename(ue.getCollectors());
    OneFileNotepad ofn = OneFileNotepad.note(filename);
    //System.out.println(sb.toString());
    ofn.setText(sb.toString());
    info(ofn, calmmsg+filename, "About this editor");
  }

  private static final String iemsg = 
      "\n\nThis is an undesired event for Simplesim. It was never intended\n"+
      "to happen. You are kindly asked to see the full error dump and\n"+
      "send it (encrypted if you wish) to the support function of your\n"+
      "supplier. Thanks in advance for any help to improve Simplesim.\n\n"+
      "Do you want to se the full report?";

  private static void general(Component c, InternalProgrammingError ie, String title)
  {
    if(title == null) title = "Internal error";
    if(!error_intern(c, getShort(ie.getMessage())+iemsg, title)) return;
    
    StringBuffer sb = new StringBuffer();
    sb.append("<ERRORDUMP>\n");
    sb.append("<TITLE>\n"+title+"\n</TITLE>\n");
    sb.append("<MESSAGE>\n"+ie.getMessage()+iemsg+"\n</MESSAGE>\n");
    sb.append("<ACTION>\n"+ie.getAction()+"\n</ACTION>\n");
    sb.append(comment+"\n");
    sb.append("<CALLSTACK>\n"+getCallstack(ie)+"\n</CALLSTACK>\n");
    if(ie.getThrowable() != null)
    {
      sb.append("<MESSAGEUNDER>\n"+
                ie.getThrowable().getMessage()+
                "\n</MESSAGEUNDER>\n");
      sb.append("<MESSAGETYPEUNDER>\n"+
                ie.getThrowable().getClass().getName()+
                "\n</MESSAGETYPEUNDER>\n");
      sb.append("<CALLSTACKUNDER>\n"+
                getCallstack(ie.getThrowable())+
                "\n</CALLSTACKUNDER>\n");
    }

    FileCollector fc[] = ie.getCollectors();
    if(fc != null)
    {
      for(int i = 0 ; i < fc.length ; i++)
      {
        sb.append("<FILE>\n"+ 
                  "<FILENAME>\n"+fc[i].getFilename()+"\n</FILENAME>\n"+
                  "<FILECONTENTS>\n"+fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                  "</FILE>\n");
      }
    }
    sb.append("</ERRORDUMP>\n");

    String filename = getDumpsFilename(null);
    OneFileNotepad ofn = OneFileNotepad.note(filename);
    ofn.setText(sb.toString());
    //System.out.println(sb.toString());
    info(ofn, calmmsg+filename, "About this editor");
  }
  
  public static String general_textual(IntolerableException ie)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(ie.getMessage()+"\n");
    Throwable t = ie.getCause();
    if(t != null)
    {
      sb.append(getGentleUnderlyingReport(t));
    }
    return sb.toString();
  }

  private static final String semsg = 
      "\n\n"+
      "This is an error type that means that Simplesim had some kind of\n"+
      "problem when interacting with your system (Operating system and\n"+
      "hardware)\n\n"+
      "Do you want to se the full error dump?";

  private static void general(Component c, SystemError se, String title)
  {
    StringBuffer sb = new StringBuffer(se.getMessage());
    Throwable t = se.getThrowable();
    if(t != null)
    {
      sb.append(getGentleUnderlyingReport(t));
    }

    if(title == null) title = "System error";
    String firstMsg = getShort(sb.toString())+semsg;
    if(!error_intern(c, firstMsg, title)) return;
    firstMsg = sb.toString()+semsg;

    sb = new StringBuffer();
    sb.append("<ERRORDUMP>\n");
    sb.append("<TITLE>\n"+title+"\n</TITLE>\n");
    sb.append("<MESSAGE>\n"+firstMsg+"\n</MESSAGE>\n");
    sb.append("<ACTION>\n"+se.getAction()+"\n</ACTION>\n");
    sb.append(comment+"\n");
    sb.append("<CALLSTACK>\n"+getCallstack(se)+"\n</CALLSTACK>\n");
    if(se.getThrowable() != null)
    {
      sb.append("<MESSAGEUNDER>\n"+
                se.getThrowable().getMessage()+
                "\n</MESSAGEUNDER>\n");
      sb.append("<MESSAGETYPEUNDER>\n"+
                se.getThrowable().getClass().getName()+
                "\n</MESSAGETYPEUNDER>\n");
      sb.append("<CALLSTACKUNDER>\n"+
                getCallstack(se.getThrowable())+
                "\n</CALLSTACKUNDER>\n");
    }
    FileCollector fc[] = se.getCollectors();
    if(fc != null)
    {
      for(int i = 0 ; i < fc.length ; i++)
      {
        sb.append("<FILE>\n"+ 
                  "<FILENAME>\n"+fc[i].getFilename()+"\n</FILENAME>\n"+
                  "<FILECONTENTS>\n"+fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                  "</FILE>\n");
      }
    }
    sb.append("</ERRORDUMP>\n");

    String filename = getDumpsFilename(se.getCollectors());
    OneFileNotepad ofn = OneFileNotepad.note(filename);
    ofn.setText(sb.toString());
    //System.out.println(sb.toString());
    info(ofn, calmmsg+filename, "About this editor");
  }

  private static final String ucemsg = 
      "\n\n"+
      "This is an error type that means that a very unexpected problem\n"+
      "occurred to which there is no preprogrammed diagnosis. If you\n"+
      "feel that this is not justified please see the full error dump\n"+
      "and send it (encrypted if you wish) to the support function of\n"+
      "your supplier. Thanks in advance for any help to improve Simplesim.\n\n"+
      "Do you want to se the full error dump?";

  private static void general(Component c, UnclassedError ue, String title)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(ue.getMessage()+"\n");
    Throwable t = ue.getThrowable();
    if(t != null)
    {
      sb.append(getGentleUnderlyingReport(t));
    }

    if(title == null) title = "Unclassed error";
    String firstMsg = getShort(sb.toString())+ucemsg;
    if(!error_intern(c, firstMsg, title)) return;
    firstMsg = sb.toString()+ucemsg;

    sb = new StringBuffer();
    sb.append("<ERRORDUMP>\n");
    sb.append("<TITLE>\n"+title+"\n</TITLE>\n");
    sb.append("<MESSAGE>\n"+firstMsg+"\n</MESSAGE>\n");
    sb.append("<ACTION>\n"+ue.getAction()+"\n</ACTION>\n");
    sb.append(comment+"\n");
    sb.append("<CALLSTACK>\n"+getCallstack(ue)+"\n</CALLSTACK>\n");
    if(ue.getThrowable() != null)
    {
      sb.append("<MESSAGEUNDER>\n"+
                ue.getThrowable().getMessage()+
                "\n</MESSAGEUNDER>\n");
      sb.append("<MESSAGETYPEUNDER>\n"+
                ue.getThrowable().getClass().getName()+
                "\n</MESSAGETYPEUNDER>\n");
      sb.append("<CALLSTACKUNDER>\n"+
                getCallstack(ue.getThrowable())+
                "\n</CALLSTACKUNDER>\n");
    }
    FileCollector fc[] = ue.getCollectors();
    if(fc != null)
    {
      for(int i = 0 ; i < fc.length ; i++)
      {
        sb.append("<FILE>\n"+ 
                  "<FILENAME>\n"+fc[i].getFilename()+"\n</FILENAME>\n"+
                  "<FILECONTENTS>\n"+fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                  "</FILE>\n");
      }
    }
    sb.append("</ERRORDUMP>\n");

    String filename = getDumpsFilename(ue.getCollectors());
    OneFileNotepad ofn = OneFileNotepad.note(filename);
    ofn.setText(sb.toString());
    info(ofn, calmmsg+filename, "About this editor");
  }

  private static String getDumpsFilename(FileCollector fc[])
  {
    if(fc == null)
    {
      return HierarchyObject.getReferenceFilePath()+"errordump.edq";
    }
    if(fc.length == 0)
    {
      return HierarchyObject.getReferenceFilePath()+"errordump.edq";
    }
    String fn = fc[0].getFilename();
    int separatorsPlace = fn.lastIndexOf(File.separator);
    if(separatorsPlace != -1)
    {
      fn = fn.substring(separatorsPlace+1);
    }
    int dotsPlace = fn.lastIndexOf('.');
    if(dotsPlace == -1) return HierarchyObject.getReferenceFilePath()+fn+".edq";
    return HierarchyObject.getReferenceFilePath()+fn.substring(0, dotsPlace)+".edq";
  }

  private static String getGentleUnderlyingReport(Throwable t)
  {
    return "\nThe underlying exception was\n"+t.getMessage();
  }

  public static void main(String args[])
  {
    //String s = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n1\n2\n3\n4\n5\n6\n7\n8\n9\n20\n1\n2\n3\n4\n5\n6\n7\n8\n9\n30\n1\n2\n3\n4\n5\n6\n7\n8\n9\n40\n1\n2\n3\n4\n5\n6\n7\n8\n9\n50";
    String s = "\n\n\n\n\n\n\n\n\n10\n1\n2\n3\n4\n5\n6\n7\n8\n9";
    System.out.println("Svar = "+  error_intern(null, getShort(s), "title"));
  }

}


