package se.modlab.generics.gui.jpegcoding;

public class JPEGFileException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JPEGFileException( String msg )
	{
		super(msg);
	}
}
