package se.modlab.generics.gui.progress;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class MemoryMonitorDialog
    extends JDialog {
    private static final long serialVersionUID = -6067451756047283278L;
        private MemoryMonitor memoryMonitorPanel;

    public MemoryMonitorDialog(Dialog parent) {
        super(parent, false);
        init();
    }

    public MemoryMonitorDialog(Frame parent) {
        super(parent, false);
        init();
    }

    private void init() {
        setTitle("aj Memory Monitor");
        memoryMonitorPanel = new MemoryMonitor(300, 90);
        memoryMonitorPanel.startMemoryMonitor();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                memoryMonitorPanel.stopMemoryMonitor();
            }
        });
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(memoryMonitorPanel, BorderLayout.CENTER);
        pack();
        setSize(new Dimension(200, 200));
    }

    public void setVisible(boolean display) {
        super.setVisible(display);
        memoryMonitorPanel.startMemoryMonitor();
    }

    public static void main(String args[])
    {
      new MemoryMonitorDialog(new Frame());
    }
}
