package se.modlab.generics.gui.plot;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.JpegEncoder;

public class CrossPlotComponent extends JComponent {

	private CrossPlottable sequence;
	private static final int margin = 20;
	private int w_pix;
	private int h_pix;
	private double min_val_ax1;
	private double max_val_ax1;
	private double h_val;
	private double min_val_ax2;
	private double max_val_ax2;

	public CrossPlotComponent(CrossPlottable sequence) {
		this.sequence = sequence;
	    addMouseListener(new PopupListener());
	    addMouseMotionListener(new ToolTipMouseMotionListener());
	}

	public void paint(Graphics g) {
		paint(g, getSize());
	}

	protected void drawText(Graphics g, Dimension d, int h_pix) throws IntolerableException {
		g.drawString("Vertical variable: "+sequence.getNameAx2(), 
				margin, h_pix+((int)(1.5*margin)));
		g.drawString("Min value: "+min_val_ax2, 
				margin, h_pix+((int)(2.5*margin)));
		g.drawString("Max value: "+max_val_ax2, 
				margin, h_pix+((int)(3.5*margin)));

		g.drawString("Horizontal variable: "+sequence.getNameAx1(),  
				d.width/2, h_pix+((int)(1.5*margin)));
		g.drawString("Min value: "+min_val_ax1, 
				d.width/2, h_pix+((int)(2.5*margin)));
		g.drawString("Max value: "+max_val_ax1, 
				d.width/2, h_pix+((int)(3.5*margin)));
	}

	protected void drawFrame(Graphics g, Dimension d, int h_pix) throws IntolerableException {
		// Frame upper
		g.drawLine((int) margin/2, (int) margin/2,
				(int) (d.width - margin/2), (int) margin/2); 
		// Frame lower
		g.drawLine((int) margin/2, h_pix+(int) margin/2,
				(int) (d.width - margin/2), h_pix+(int) margin/2); 
		// Frame left
		g.drawLine((int) margin/2, (int) margin/2,
				(int) margin/2, h_pix+(int) margin/2); 
		// Frame right
		g.drawLine((int) (d.width - margin/2), (int) margin/2,
				(int) (d.width - margin/2), h_pix+(int) margin/2); 
	}

	public void paint(Graphics g, Dimension d) {
		try
		{
			min_val_ax1 = sequence.getMinValueAxis1();
			max_val_ax1 = sequence.getMaxValueAxis1();
			h_val = max_val_ax1 - min_val_ax1;
			int linesBelow = 4;
			h_pix = d.height-margin*linesBelow;
			w_pix = margin;
			min_val_ax2 = sequence.getMinValueAxis2();
			max_val_ax2 = sequence.getMaxValueAxis2();
			if(sequence.getSize() == 1) 
			{
				g.drawString("One value is not enough for cross plot ("+min_val_ax1+", "+min_val_ax2+")", margin, margin);
				drawText(g, d, h_pix);
				return;
			}

			drawFrame(g, d, h_pix);

			for(int i = 0 ; i < sequence.getSize() ; i++) {
				double ax2val = sequence.getValueAx2(i);
				double ax1val = sequence.getValueAx1(i);
				TimePlotComponent.drawDot(g,
						((int) (w_pix + (d.width-2*w_pix)*(((double)(ax2val-min_val_ax2))/(max_val_ax2-min_val_ax2))))-1, 
						((int) (h_pix - (ax1val-min_val_ax1)/(h_val)*(h_pix-margin)))-1, 
						sequence.getShape(i));
			}

			drawText(g, d, h_pix);
		}
		catch(IntolerableException ie)
		{
			UniversalTellUser.general(null, ie);
		}

	}

	protected void mouseMoved(MouseEvent e)
	{
		try {
			if(sequence.getSize() == 1) return;
			Dimension d = getSize();
			int w_pix = margin;

			double min_val_ax1 = sequence.getMinValueAxis1();
			double max_val_ax1 = sequence.getMaxValueAxis1();
			double h_val = max_val_ax1 - min_val_ax1;
			int h_pix = d.height-margin*4;
			int y = e.getY();

			double variable_ax1 = min_val_ax1-(y - h_pix)*h_val/(h_pix-margin);

			double min_val_ax2 = sequence.getMinValueAxis2();
			double max_val_ax2 = sequence.getMaxValueAxis2();
			double v_val = max_val_ax2 - min_val_ax2;
			int v_pix = d.width-margin*4;
			int x = e.getX();

			double variable_ax2 = min_val_ax2-(x - v_pix)*v_val/(v_pix-margin);

			setToolTipText(sequence.getNameAx2()+" = "+variable_ax2+", "+sequence.getNameAx1()+" = "+variable_ax1);
		}
		catch(Exception ex) {
			setToolTipText("Plot error: "+ex.getMessage());
		}
	}

	private class PopupListener extends MouseAdapter 
	{

		public PopupListener()
		{
		}

		public void mousePressed(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) 
		{
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) 
		{
			if(sequence.getSize() == 1) return;
			if(!e.isPopupTrigger()) return;
			JPopupMenu popup = getPopup();
			if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
		}

	}
	
	  private class ToolTipMouseMotionListener implements MouseMotionListener
	  {

	    public void mouseDragged(MouseEvent e)
	    {
	    }

	    public void mouseMoved(MouseEvent e)
	    {
	      CrossPlotComponent.this.mouseMoved(e);
	    }

	  }



	protected void createJPG() {
		try {
			JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
			fc.setDialogTitle("Point to the file. (With extension .jpg)");
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(this));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return;
			String path = selFile.getAbsolutePath();
			if(path == null) return;
			if(!(path.endsWith(".jpg")))
			{
				JOptionPane.showMessageDialog(
						UniversalTellUser.getFrame(this), 
						"The filename must end with .jpg", 
						"File not valid", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			Dimension d = getSize();
			BufferedImage image = new BufferedImage(d.width, 
					d.height, 
					BufferedImage.TYPE_USHORT_GRAY);
			Graphics2D graphics = image.createGraphics();
			graphics.setPaint(Color.BLACK);
			graphics.setBackground(Color.WHITE);
			graphics.clearRect(0, 0, d.width, d.height);
			paint(graphics, d);
			FileOutputStream fos = new FileOutputStream(selFile);
			JpegEncoder jpg = new JpegEncoder(image, 50, fos);
			jpg.Compress();
			fos.close();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("File not found", fnf));
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("I/O error", ioe));
		}
		catch(Exception e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(this), 
					new UnclassedError("monitoredSimVariable - jpg-generation", e));
		}
	}

	protected JPopupMenu getPopup()
	{
		if(sequence.getSize() == 1) return null;;
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Create .jpg-file");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						createJPG();
					}
				});
		popup.add(menuItem);
		menuItem = new JMenuItem("Export to clipboard");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						exportToClipboard();
					}
				});
		popup.add(menuItem);
		return popup;
	}
	
	private void exportToClipboard() {
		try {
			StringBuffer sb = new StringBuffer();
			int rowcount = sequence.getSize();//model.getRowCount();
			for(int r = 0 ; r < rowcount ; r++) {
				sb.append(sequence.getValueAx1(r));
				sb.append("\t");
				sb.append(sequence.getValueAx2(r));
				sb.append("\n");
			}
			StringSelection ss = new StringSelection(sb.toString()); 
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null); 
		}
		catch(IntolerableException ie) {
			UniversalTellUser.general(this, ie);
		}
	}
	
	public static void main(String args[]) {
		CrossPlotDataContainer tp = new CrossPlotDataContainer("Sin", "Cos");

		for(int r = 0 ; r < 157 ; r += 4) {
			double vx1 = Math.sin(((double) r) / 100);
			double vx2 = Math.cos(((double) r) / 100);
			tp.add(vx1, vx2, CrossPlottable.BOX);
		}

		for(int r = 0 ; r < 157 ; r += 4) {
			double vx1 = Math.random();
			double vx2 = Math.random();
			tp.add(vx1, vx2, CrossPlottable.CROSS);
		}

		for(int r = 0 ; r < 157 ; r += 4) {
			double vx1 = Math.random();
			double vx2 = Math.random();
			tp.add(vx1, vx2, CrossPlottable.CIRCLE);
		}

		//...
		final JFrame frame = new JFrame(tp.getNameAx1()+"/"+tp.getNameAx2());
		frame.addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent windowevent) {
		          frame.setVisible(false);
		          frame.dispose();
		        }
		      });
		frame.getContentPane().add(new CrossPlotComponent(tp), BorderLayout.CENTER);
		try {
			frame.pack();
		}
		catch(Exception ie) {
			UniversalTellUser.error(frame, ie.getMessage());
			return;
		}
		frame.setSize(500,350);
		frame.setVisible(true);
	}


}
