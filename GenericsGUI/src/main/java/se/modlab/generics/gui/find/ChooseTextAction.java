package se.modlab.generics.gui.find;

public interface ChooseTextAction
{
  public void cancelAction();
  public void textChosen(String text);
}
