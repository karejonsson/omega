package se.modlab.generics.gui.find;

import java.io.PrintWriter;

public interface Listable
{

    public abstract void list();

    public abstract void list(PrintWriter printwriter);

    public abstract void list(PrintWriter printwriter, int i);
}
