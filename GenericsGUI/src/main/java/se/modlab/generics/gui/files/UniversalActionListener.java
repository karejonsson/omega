package se.modlab.generics.gui.files;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.*;

// About this class. Se page 222 in Java in a Nutshell

public class UniversalActionListener implements ActionListener
{
  protected Object target;
  protected Object[] arguments;
  protected Method m;
  
  public UniversalActionListener(Object target, String methodname, Object arg)
       throws /*NoSuchMethodException,*/ SecurityException
  {
    this.target = target;
    //this.arg = arg;
    if(target == null) System.out.println("(target == null) "+methodname);
    
    // Now look up and save the Method to invoke on that target object
    Class<?> c, parameters[];
    if(target instanceof Class)
      c = (Class<?>) target;
    else
      c = target.getClass();

    if(arg == null) 
      {
	parameters = new Class[0];
	arguments = new Object[0];
      }
    else
      {
	if(arg instanceof Object[])
	  {
	    arguments = (Object[]) arg;
	    int len = arguments.length;
	    parameters = new Class[len];
	    for(int i = 0 ; i < len ; i++)
	      {
		parameters[i] = (arguments[i] != null) ? arguments[i].getClass() : null;
	      }
	  }
	else
	  {
	    parameters = new Class[] { arg.getClass() };
	    arguments = new Object[] { arg };
	  }
      }
    try 
      {
	m = c.getMethod(methodname, parameters);
      }
    catch(NoSuchMethodException e)
      {
	System.out.print("UniversalActionListener - NoSuchMethodException "+c.getName()+"."+methodname+"(");
	for(int i = 0 ; i < parameters.length-1 ; i++)
	  {
	    System.out.print(((parameters[i] != null) ? parameters[i].getName() : "null")+", ");
	  }
	if(parameters.length > 0)
	  System.out.println(((parameters[parameters.length-1] != null) ? parameters[parameters.length-1].getName() : "null")+")");
	else
	  System.out.println(")");
	e.printStackTrace();
	System.exit(1);
      }
  }
  
  public void actionPerformed(ActionEvent event) 
  {
    if(m != null) doBefore(event);
    try 
      { 
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalActionListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalActionListener: "+e+" : "+e.getTargetException());
	e.printStackTrace();
      }
    if(m != null) doAfter(event);
  }

  public void doBefore(ActionEvent event)
  {
  }
  
  public void doAfter(ActionEvent event)
  {
  }
  

  public static void main(String[] args)
  {
    java.awt.Frame f = new java.awt.Frame("UniversalActionListener test");
    f.setLayout(new java.awt.FlowLayout());
    java.awt.Button b1 = new java.awt.Button("tick");
    java.awt.Button b2 = new java.awt.Button("tick");
    java.awt.Button b3 = new java.awt.Button("Close Window");
    f.add(b1);
    f.add(b2);
    f.add(b3);
    UniversalActionListener al;
    
    b1.addActionListener(new UniversalActionListener(b1, "setLabel", "tick"));
    b1.addActionListener(new UniversalActionListener(b2, "setLabel", "tack"));
    b1.addActionListener(new UniversalActionListener(b3, "hide", null));
    b2.addActionListener(new UniversalActionListener(b1, "setLabel", "tack"));
    b2.addActionListener(new UniversalActionListener(b2, "setLabel", "tick"));
    b2.addActionListener(new UniversalActionListener(b3, "show", null));
    b3.addActionListener(al = new UniversalActionListener(f, "dispose", null));
    b3.addActionListener(new UniversalActionListener(al, "aaa", new Object[] {"Ett - un","Tv� - deux"}));
    f.pack();
    f.setVisible(true);
  }

  public void aaa(String ett, String tva)
  {
    System.out.println("aaa:"+ett+":"+tva);
    System.exit(1);
  }

} 
