package org.omega.memorycompiler.teststruct;

public class Base {
	private Sub s1 = null;
	private Sub s2 = null;

	public void setS1(Sub s1) {
		this.s1 = s1;
	}

	public Sub getS1() {
		return s1;
	}

	public void setS2(Sub s2) {
		this.s2 = s2;
	}

	public Sub getS2() {
		return s2;
	}
}
