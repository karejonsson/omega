package org.omega.memorycompiler.teststruct;

public class Any {
	public static void main(String args[]) {
		Base b = getIt();

		// Add instance b to the scope of a Jython execution. Python script shall be able to
		// manipulate like so

		// b.s1.m1 = ”Hulo” 

		// Back to java
		System.out.println("b.s1.m1 = "+b.getS1().getM1()); // Prints Hulo

	}
	
	public static Base getIt() {
		Sub s1 = new Sub();
		s1.setM1("Hello");
		s1.setM2("World");

		Base b = new Base();
		b.setS1(s1);

		Sub s2 = new Sub();
		s2.setM1("Salut");
		s2.setM2("Monde");

		b.setS2(s2);
		return b;
	}
}
