package org.omega.memorycompiler.groovyembedding;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.teststruct.Any;
import org.omega.memorycompiler.teststruct.Base;

import org.junit.Test;
import junit.framework.Assert;

public class Sample3Test {

	public static final String classdefinition = 
			"package se.abc;\n"+
		    "import org.omega.memorycompiler.teststruct.*; \n"+
			"public class Someclass { \n"+
		    " private Base b = null; \n"+
			" public Someclass(Base b) { \n"+
			"  this.b = b; \n"+
			" } \n"+
			" public void setValue(String val) { \n"+
			"  b.getS1().setM1(val); \n"+
			" } \n"+
			" public String getValue() { \n"+
			"  return b.getS1().getM1(); \n"+
			" } \n"+
			"}\n";

	@Test
	public void main() throws Exception {
		Base b = Any.getIt();
		System.out.println("1: b.getS1().getM1() = "+b.getS1().getM1());	
		
		Class<?> helloClass = InMemoryJavaCompiler.compile("se.abc.Someclass", classdefinition);
		
		Constructor<?> cons = helloClass.getConstructor(Base.class);
		Object o = cons.newInstance(b);
		 
		String myScript = "o.value = \"Ankare liksom\"";
		BSFManager manager = new BSFManager();
		manager.declareBean("o", o, Object.class);
		manager.eval("groovy", "test.groovy", 0, 0, myScript);

		System.out.println("2: b.getS1().getM1() = "+b.getS1().getM1());		
		Assert.assertTrue(b.getS1().getM1().contains("liksom"));
	}

}
