package org.omega.memorycompiler.groovyembedding;

import org.junit.Test;
import org.omega.memorycompiler.InMemoryJavaCompiler;
import org.omega.memorycompiler.SourceCode;

import junit.framework.Assert;

public class Sample4Test {
	public static final String cdA = 
			"package se.abc;\n"+
			"public class SomeA { \n"+
		    " private SomeB sb = null; \n"+
			" public SomeA(SomeB sb) { \n"+
			"  this.sb = sb; \n"+
			" } \n"+
			"}\n";

	public static final String cdB = 
			"package se.abc;\n"+
			"public class SomeB { \n"+
		    " private SomeA sa = null; \n"+
			" public SomeB(SomeA sa) { \n"+
			"  this.sa = sa; \n"+
			" } \n"+
			"}\n";

	public static final String cdC = 
			"package se.abc;\n"+
			"public class SomeC { \n"+
		    " private SomeA sa = null; \n"+
		    " private SomeB sb = null; \n"+
			" public SomeC(SomeA sa, SomeB sb) { \n"+
			"  this.sa = sa; \n"+
			"  this.sb = sb; \n"+
			" } \n"+
			"}\n";

	@Test
	public void main() throws Exception {
		SourceCode scA = new SourceCode("se.abc.SomeA", cdA);
		SourceCode scB = new SourceCode("se.abc.SomeB", cdB);
		SourceCode scC = new SourceCode("se.abc.SomeC", cdC);
		Class<?> classes[] = InMemoryJavaCompiler.compile(scC, scA, scB);
		Assert.assertTrue(true);
	}
	
}
