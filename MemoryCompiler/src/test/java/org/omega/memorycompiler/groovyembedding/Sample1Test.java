package org.omega.memorycompiler.groovyembedding;

import java.util.List;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;

import junit.framework.Assert;
import org.junit.Test;

public class Sample1Test {

	@Test
	public void main() throws BSFException {
		String myScript = "println('Hello World')\n  return [1, 2, 3]";
		BSFManager manager = new BSFManager();
		List answer = (List) manager.eval("groovy", "myScript.groovy", 0, 0, myScript);
		System.out.println("Len = "+answer.size());	
		Assert.assertEquals(3, answer.size());
	}
	
}
