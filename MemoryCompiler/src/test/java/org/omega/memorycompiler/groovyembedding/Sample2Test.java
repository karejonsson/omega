package org.omega.memorycompiler.groovyembedding;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.junit.Test;
import org.omega.memorycompiler.teststruct.Any;
import org.omega.memorycompiler.teststruct.Base;

import junit.framework.Assert;

public class Sample2Test {
	
	@Test
	public void main() throws BSFException {
		Base b = Any.getIt();
		System.out.println("1: b.getS1().getM1() = "+b.getS1().getM1());	
		
		String myScript = "b.s1.m1 = \"Ankarkätting\"";
		BSFManager manager = new BSFManager();
		manager.declareBean("b", b, Object.class);
		manager.eval("groovy", "test.groovy", 0, 0, myScript);

		System.out.println("2: b.getS1().getM1() = "+b.getS1().getM1());		
		Assert.assertTrue(b.getS1().getM1().contains("Ankar"));
	}

}
