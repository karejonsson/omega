package org.omega.memorycompiler;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject; 
import javax.tools.ToolProvider;
import java.util.Arrays;

public class InMemoryJavaCompiler {
    static JavaCompiler javac = ToolProvider.getSystemJavaCompiler();

    public static Class<?> compile(String className, String sourceCodeInText) throws Exception {
        SourceCode sourceCode = new SourceCode(className, sourceCodeInText);
        CompiledCode compiledCode = new CompiledCode(className);
        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(sourceCode);
        DynamicClassLoader cl = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
        ExtendedStandardJavaFileManager fileManager = new ExtendedStandardJavaFileManager(javac.getStandardFileManager(null, null, null), compiledCode, cl);
        JavaCompiler.CompilationTask task = javac.getTask(null, fileManager, null, null, null, compilationUnits);
        boolean result = task.call();
        return cl.loadClass(className);
    }
    
    public static Class<?>[] compile(SourceCode... scs) throws Exception {
        DynamicClassLoader cl = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
        return compile(cl, scs);
    }
    
    public static Class<?>[] compile(DynamicClassLoader cl, SourceCode... scs) throws Exception {
        CompiledCode compiledCodes[] = new CompiledCode[scs.length];
        int pointer = 0;
        for(SourceCode sc : scs) {
        	compiledCodes[pointer++] = new CompiledCode(sc.getName());
        }

        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(scs);
        ExtendedStandardJavaFileManager fileManager = new ExtendedStandardJavaFileManager(javac.getStandardFileManager(null, null, null), compiledCodes, cl);
        JavaCompiler.CompilationTask task = javac.getTask(null, fileManager, null, null, null, compilationUnits);
        boolean result = task.call();
        Class<?>[] out = new Class<?>[scs.length];
        for(int i = 0 ; i < out.length ; i++) {
        	out[i] = cl.loadClass(scs[i].getName());
        }
        return out;
    }
    
}
