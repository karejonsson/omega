package org.omega.memorycompiler;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by trung on 5/3/15.
 */
public class ExtendedStandardJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {

    private Map<String, CompiledCode> compiledCodeMap = new HashMap<String, CompiledCode>();
    private DynamicClassLoader cl;

    /**
     * Creates a new instance of ForwardingJavaFileManager.
     *
     * @param fileManager delegate to this file manager
     * @param cl
     */
    protected ExtendedStandardJavaFileManager(JavaFileManager fileManager, CompiledCode compiledCode, DynamicClassLoader cl) {
        super(fileManager);
        //System.out.println("ExtendedStandardJavaFileManager<ctor> putting "+compiledCode.getName());
        compiledCodeMap.put(compiledCode.getName(), compiledCode);
        //this.compiledCode = compiledCode;
        this.cl = cl;
        this.cl.setCode(compiledCode);
    }

    protected ExtendedStandardJavaFileManager(JavaFileManager fileManager, CompiledCode[] compiledCodes, DynamicClassLoader cl) {
        super(fileManager);
        this.cl = cl;
        for(CompiledCode compiledCode : compiledCodes) {
        	String cleanName = SourceCode.clean(compiledCode.getName());
            //System.out.println("ExtendedStandardJavaFileManager<ctor> putting "+cleanName);
            compiledCodeMap.put(cleanName, compiledCode);
            this.cl.setCode(compiledCode);
        }
    }

    @Override
    public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
    	String cleanName = SourceCode.clean(className);
    	//System.out.println("ExtendedStandardJavaFileManager.getJavaFileForOutput getting "+cleanName);
        return compiledCodeMap.get(cleanName);
    }

    @Override
    public ClassLoader getClassLoader(JavaFileManager.Location location) {
        return cl;
    }
    
}