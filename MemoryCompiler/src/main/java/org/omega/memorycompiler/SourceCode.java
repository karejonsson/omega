package org.omega.memorycompiler;

import javax.tools.SimpleJavaFileObject;
import java.io.IOException;
import java.net.URI;

/**
 * Created by trung on 5/3/15.
 */
public class SourceCode extends SimpleJavaFileObject {
    private String contents = null;
    //private String className = null;
    public SourceCode(String className, String contents) throws Exception {
        super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
        this.contents = contents;
        //this.className = className;
    }

    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
        return contents;
    }
    
    public static String clean(String s) {
    	String out = s;
    	if(out.endsWith(Kind.SOURCE.extension)) {
    		out = out.substring(0, out.length()-5);
    	}
    	out = out.replace('/', '.');
    	if(out.startsWith(".")) {
    		out = out.substring(1);
    	}
    	return out;
    }
    
    public String getSource() {
    	return contents;
    }

}