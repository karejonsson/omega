package org.omega.memorycompiler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by trung on 5/3/15.
 */
public class DynamicClassLoader extends ClassLoader {

    private Map<String, CompiledCode> customCompiledCode = new HashMap<String, CompiledCode>();

    public DynamicClassLoader(ClassLoader parent) {
        super(parent);
    }
    
    public void setCode(CompiledCode cc) {
    	String cleaned = SourceCode.clean(cc.getName());
    	//System.out.println("DynamicClassLoader.setCode("+cleaned+")");
        customCompiledCode.put(cleaned, cc);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
    	String cleanName = SourceCode.clean(name);
    	//System.out.println("DynamicClassLoader.findClass("+cleanName+")");
        CompiledCode cc = customCompiledCode.get(cleanName);
        if (cc == null) {
            return super.findClass(cleanName);
        }
        byte[] byteCode = cc.getByteCode();
        return defineClass(cleanName, byteCode, 0, byteCode.length);
    }
}