package org.omega.configuration.icons;

public class ImageIconByteArrayHolder {
	
	private byte[] theBytes;
	
	public ImageIconByteArrayHolder(byte[] theBytes) {
		this.theBytes = theBytes;
	}
	
	public byte[] getBytes() {
		return theBytes;
	}

}
