package org.omega.configuration.model;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;

import se.modlab.generics.bshro.ifc.HierarchyBranch;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.exceptions.IntolerableException;

public class ModelConfiguration {
	
	public final static String referrerIconFile_S = "referrericonfile";
	public final static String nullPointerIconFile_S = "nullpointericonfile";
	public final static String skipNullPointerIconFile_P = "skipnullpointericonfile";
	public final static String nullPointerImage_I = "nullpointerimage";
	public final static String referrerImage_I = "referrerimage";
	public final static String skipReferrerIconFile_P = "skipreferrericonfile";
	
	private Hashtable<String, TableConfiguration> lookupObjectname = new Hashtable<String, TableConfiguration>();
	private Hashtable<String, TableConfiguration> lookupTablename = new Hashtable<String, TableConfiguration>();
	//private Hashtable<String, Icon> icons = new Hashtable<String, Icon>();
	//private Hashtable<String, Boolean> problems = new Hashtable<String, Boolean>();
	private String iconfile;
	private Hashtable<String, Object> cache = new Hashtable<String, Object>();
	private Hashtable<String, byte[]> icons = new Hashtable<String, byte[]>();
	private Hashtable<String, ImageIconByteArrayHolder> imageIcons = new Hashtable<String, ImageIconByteArrayHolder>();
	//private Vector<DBMany2ManyRelation> m2mrs = new Vector<DBMany2ManyRelation>();
	private Hashtable<String, String> translations = new Hashtable<String, String>();
	private String version = null;
	private Hashtable<String, Boolean> problems = new Hashtable<String, Boolean>();
	
	public ModelConfiguration() {
	}
	
	public boolean valueEquals(ModelConfiguration other) throws IntolerableException {
		if(lookupObjectname.size() != other.lookupObjectname.size()) {
			return false;
		}
		for(String key : lookupObjectname.keySet()) {
			if(other.lookupObjectname.get(key) == null) {
				return false;
			}
		}
		return true;
	}
	
	public void setIconfile(String iconfile) {
		this.iconfile = iconfile;
	}
	
	public String getIconfile() {
		return iconfile;
	}
	
	public byte[] getIconfileBytes() {
		try {
			return ((HierarchyLeaf) getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public ImageIconByteArrayHolder getImageIconBestEffort(String name) {
		ImageIconByteArrayHolder out = imageIcons.get(name);
		if(out != null) {
			return out;
		}
		byte[] bytes = icons.get(name);
		if(bytes == null) {
			return null;
		}
		return new ImageIconByteArrayHolder(icons.get(name));
	}
	
	//public void setImageIconAsBytes(String name, byte[] imageIcon) {
	//	icons.put(name, imageIcon);
	//}

	public void setImageIconAsImageIcon(String name, ImageIconByteArrayHolder imageIcon) {
		imageIcons.put(name, imageIcon);
	}

	public void setProblem(String name, boolean problem) {
		problems.put(name, problem);
	}

	public boolean getProblem(String name) {
		Boolean out = problems.get(name);
		if(out == null) {
			return false;
		}
		return out;
	}
	
	public void addTableSpecification(TableConfiguration ths) {
		lookupObjectname.put(ths.getObjectName(), ths);
		lookupTablename.put(ths.getTableName(), ths);
	}
	
	public void setCache(String flag, Object obj) {
		//System.out.println("DBHandlingSpecification.setCache("+flag+", "+obj);
		cache.put(flag, obj);
	}
	
	public Object getCache(String flag) {
		return cache.get(flag);
	}
	
	public void setVersion(String _version) {
		version = _version;
	}
	
	public String getVersion() {
		if(version == null) {
			return "No version set";
		}
		return version;
	}
	
	public int getNoTables() {
		return lookupObjectname.size();
	}
	
	public Enumeration<String> getTableNames() {
		return lookupTablename.keys();
	}

	public Enumeration<String> getObjectNames() {
		return lookupObjectname.keys();
	}

	public TableConfiguration getDBTableHandlingSpecificationOnObjectName(String name) {
		return lookupObjectname.get(name);
	}
	
	public TableConfiguration getDBTableHandlingSpecificationOnTableName(String name) {
		return lookupTablename.get(name);
	}
	
	public void removeDBTableHandlingSpecification(String name) {
		TableConfiguration ths = lookupObjectname.get(name);
		if(ths != null) {
			String tablename = ths.getTableName();
			lookupObjectname.remove(name);
			lookupTablename.remove(tablename);
			return;
		}
		ths = lookupTablename.get(name);
		if(ths != null) {
			String objectname = ths.getObjectName();
			lookupObjectname.remove(objectname);
			lookupTablename.remove(name);
			return;
		}
	}
	
	public Enumeration<TableConfiguration> getTablehandlers() {
		return lookupObjectname.elements();
	}
	
	public Collection<TableConfiguration> getTablehandlersLoopable() {
		return lookupObjectname.values();
	}
	
	public int getMaxLevelValue() {
		int out = 0;
		for(TableConfiguration ths : lookupObjectname.values()) {
			out = Math.max(out,  ths.getLevel());
		}
		return out;
	}
	
	public Vector<TableConfiguration> getTablehandlersWithLevel(int level) {
		Vector<TableConfiguration> out = new Vector<TableConfiguration>();
		for(TableConfiguration ths : lookupObjectname.values()) {
			if(ths.getLevel() == level) {
				out.add(ths);
			}
		}
		return out;
	}
	
	public Vector<TableConfiguration> getTablehandlersWithoutLevel() {
		Vector<TableConfiguration> out = new Vector<TableConfiguration>();
		for(TableConfiguration table : lookupObjectname.values()) {
			if(!table.hasKnownLevel()) {
				out.add(table);
			}
		}
		return out;
	}
	
	Set<String> hideList = new HashSet<String>();
	
	public void addToHideTableList(String tablename) {
		hideList.add(tablename);
	}
	
	public boolean inHideTableList(String tablename) {
		return hideList.contains(tablename);
	}

	Set<String> searchHideList = new HashSet<String>();
	
	public void addToSearchHideTableList(String tablename) {
		searchHideList.add(tablename);
	}
	
	public boolean inSearchHideTableList(String tablename) {
		return searchHideList.contains(tablename);
	}

    public void addTranslation(String name, String other) {
    	translations.put(name, other);
    }
    
    public String translate(String name) {
    	String out = translations.get(name);
    	if(out == null) {
    		return name;
    	}
    	return out;
    }
    
    public boolean hasErrors() {
		for(TableConfiguration table : lookupObjectname.values()) {
			if(table.hasErrors()) {
				return true;
			}
		}
		return false;
    }
    
    public Vector<String> getFirstErrors(int no) {
    	Vector<String> out = new Vector<String>();
		for(TableConfiguration table : lookupObjectname.values()) {
			if(!table.hasErrors()) {
				continue;
			}
			CreateStatement cse = table.getCreateStatement();
			for(int i = 0 ; i < cse.getNoErrors() ; i++) {
				if(out.size() >= no) {
					return out;
				}
				out.add(cse.getError(i));
			}
		}
    	return out;
    }

    private HierarchyBranch hb = null;
    
	public void setFileSystemReferenceCatalog(HierarchyBranch hb) {
		this.hb = hb;
	}
    
	public HierarchyBranch getFileSystemReferenceCatalog() {
		return hb;
	}
	
	public void performTableOrderCalcule() throws Exception {
		performTableOrderCalcule(true, new StringBuffer());
	}

	public void performTableOrderCalcule(boolean shutup, StringBuffer em) throws Exception {
		Enumeration<String> names = getTableNames();
		Vector<String> unmarkedNames = new Vector<String>();

		while(names.hasMoreElements()) {
			String name = names.nextElement();
			unmarkedNames.addElement(name);
		}
		Vector<String> markedNames = new Vector<String>();
		int unmarkedInSequence = 0;
		while((unmarkedNames.size() != 0) && (unmarkedInSequence < unmarkedNames.size() + 4)) {
			String name = unmarkedNames.elementAt(0);
			TableConfiguration ths = getDBTableHandlingSpecificationOnTableName(name);
			if(ths == null) {
				String mess = "Table "+name+" is not known";
				if(!shutup) System.out.println(mess);
				em.append(mess+"\n");
				throw new Exception("Unknown table "+name);
				//unmarkedNames.remove(0);
				//continue;
			}
			CreateStatement cs = ths.getCreateStatement();
			int level = 0;
			boolean subjacentsHasLevel = true;
			for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
				ForeignKeyDeclaration c = cs.getForeignKeyDeclaration(i);
				String tablename = c.getReferencedTablename();
				TableConfiguration referencedths = getDBTableHandlingSpecificationOnTableName(tablename);
				if(referencedths == null) {
					String mess = "############################## Table "+tablename+" is not known";
					if(!shutup) System.out.println(mess);
					em.append(mess+"\n");
					throw new Exception("Unknown table "+tablename+" is referred by constraint number "+i+" in table "+cs.getName());
					//unmarkedNames.remove(0);
					//continue;
				}
				if(referencedths.hasKnownLevel() || ths == referencedths) {
					level = Math.max(level, referencedths.getLevel() + 1);
				}
				else {
					subjacentsHasLevel = false;
				}
			}
			if(subjacentsHasLevel) {
				unmarkedNames.remove(0);
				if(!shutup) System.out.println("Table "+name+" was marked with level "+level+". Tables remaining are "+unmarkedNames.size());
				markedNames.add(name);
				ths.setLevel(level);
				unmarkedInSequence = 0;
				for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
					ForeignKeyDeclaration c = cs.getForeignKeyDeclaration(i);
					String tablename = c.getReferencedTablename();
					TableConfiguration referencedths = getDBTableHandlingSpecificationOnTableName(tablename);
					if(referencedths == null) {
						String mess = "############################## Table "+tablename+" is not known";
						if(!shutup) System.out.println(mess);
						em.append(mess+"\n");
						throw new Exception("Unknown table "+tablename+" is referred by constraint number "+i+" in table "+cs.getName());
						//unmarkedNames.remove(0);
						//continue;
					}
					referencedths.addReferringConstraint(c);
				}
			}
			else {
				if(!shutup) System.out.println("Table "+name+" was not marked this round. Tables remaining are "+unmarkedNames.size());
				name = unmarkedNames.remove(0);
				unmarkedNames.add(name);
				unmarkedInSequence++;
			}
		}
		String mess = "--- The following tables where not marked. -------------------------------";
		if(!shutup) System.out.println(mess);
		em.append(mess+"\n");
		for(int i = 0 ; i < unmarkedNames.size() ; i++) {
			if(!shutup) System.out.println(unmarkedNames.elementAt(i));
		}
		int maxlevel = -1;
		mess = "--- The following tables where marked. -----------------------------------";
		if(!shutup) System.out.println(mess);
		em.append(mess+"\n");
		for(int i = 0 ; i < markedNames.size() ; i++) {
			String name = markedNames.elementAt(i);
			TableConfiguration markedths = getDBTableHandlingSpecificationOnTableName(name);
			mess = "Table "+name+" marked with "+markedths.getLevel()+", referrers ";
			if(!shutup) System.out.print(mess);
			em.append(mess+"\n");
			for(int j = 0 ; j < markedths.getNoReferringConstraints() ; j++) {
				if(!shutup) System.out.print(markedths.getReferringConstraint(j).getReferringTablename()+" ");
			}
			if(!shutup) System.out.println();
			em.append("\n");
			maxlevel = Math.max(maxlevel, markedths.getLevel());
		}
		for(int i = 0 ; i <= maxlevel ; i++) {
			mess = ""+i+": ";
			if(!shutup) System.out.print(""+i+": ");
			em.append(mess);
			int count = 0;
			for(int j = 0 ; j < markedNames.size() ; j++) {
				String name = markedNames.elementAt(j);
				TableConfiguration markedths = getDBTableHandlingSpecificationOnTableName(name);
				if(markedths.getLevel() == i) {
					mess = markedths.getTableName()+" ";
					if(!shutup) System.out.print(markedths.getTableName()+" ");
					em.append(mess);
					count++;
				}
			}
			mess = ""+count;
			if(!shutup) System.out.println(mess);
			em.append(mess);
		}
	}

}
