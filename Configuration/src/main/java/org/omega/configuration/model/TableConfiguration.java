package org.omega.configuration.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.renderers.DefaultRownodeTextRenderer;
import org.omega.configuration.renderers.DefaultTableRenderer;
import org.omega.configuration.renderers.ExplicitTextTextRenderer;
import org.omega.configuration.renderers.TableRenderer;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.ProgramBlock;

import se.modlab.generics.bshro.ifc.HierarchyLeaf;

public class TableConfiguration {
	
	public static final String normalIcon_I = "normalIcon";
	public static final String nullPointerIcon_I = "nullpointericon";
	public static final String typeNullPointerIconTrouble_P = "typenullpointericontrouble";
	public static final String typeIconTrouble_P = "typeicontrouble";
	public static final String referrerIcon_I = "referrericon";
	
	private CreateStatement cs;
	private int level = -1;
	private Vector<ForeignKeyDeclaration> referringConstraints = new Vector<ForeignKeyDeclaration>();
	private Hashtable<String, Object> ht = new Hashtable<String, Object>();
	private String iconfile;
	private TextRenderer classRenderer = null;
	private TextRenderer instanceRenderer = null;
	private Hashtable<String, TextRenderer> referrerTreeRenderers = new Hashtable<String, TextRenderer>();
	private Hashtable<String, TextRenderer> referenceTreeRenderers = new Hashtable<String, TextRenderer>();
	private Hashtable<String, TextRenderer> referenceDialogRenderers = new Hashtable<String, TextRenderer>();
	//private Hashtable<String, ccContainer> literalAutoUpdates = new Hashtable<String, ccContainer>();
	//private Hashtable<String, arithmeticEvaluable> arithmeticAutoUpdates = new Hashtable<String, arithmeticEvaluable>();
	private HashSet<String> autoupdate = new HashSet<String>();
	private Vector<ProgramBlock> onAllUpdates = new Vector<ProgramBlock>();
	private Vector<ProgramBlock> onNewUpdates = new Vector<ProgramBlock>();
	private Vector<ProgramBlock> onInsertExecutes = new Vector<ProgramBlock>();
	private Vector<ProgramBlock> onUpdateExecutes = new Vector<ProgramBlock>();
	private Vector<ProgramBlock> onInsertAndUpdateExecutes = new Vector<ProgramBlock>();
	private Hashtable<String, byte[]> icons = new Hashtable<String, byte[]>();
	private Hashtable<String, ImageIconByteArrayHolder> iconsAsImages = new Hashtable<String, ImageIconByteArrayHolder>();
	private Hashtable<String, Boolean> problems = new Hashtable<String, Boolean>();
	private Hashtable<String, String> translations = new Hashtable<String, String>();
	private TableRenderer tr = null;
	
	private ModelConfiguration hs = null;
	
	public void setDBHandlingSpecification(ModelConfiguration _hs) {
		hs = _hs;
	}

	public ModelConfiguration getDBHandlingSpecification() {
		return hs;
	}
	
	private String objectName;
	
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	
	public String getObjectName() {
		return objectName;
	}
	
	public String getTableName() {
		return cs.getName();
	}
	
	/*
	public void mergeIntoMyself(DBTableHandlingSpecification ths) {
		onAllUpdates.addAll(ths.onAllUpdates);
		autoupdate.addAll(ths.autoupdate);
		onNewUpdates.addAll(ths.onNewUpdates);
		onInsertExecutes.addAll(ths.onInsertExecutes);
		onUpdateExecutes.addAll(ths.onUpdateExecutes);
		onInsertAndUpdateExecutes.addAll(ths.onInsertAndUpdateExecutes);
		icons.putAll(ths.icons);
		iconsAsImages.putAll(ths.iconsAsImages);
		problems.putAll(ths.problems);
		referringConstraints.addAll(ths.referringConstraints);
		ht.putAll(ths.ht);
		referrerTreeRenderers.putAll(ths.referrerTreeRenderers);
		referenceTreeRenderers.putAll(ths.referenceTreeRenderers);
		referenceDialogRenderers.putAll(ths.referenceDialogRenderers);
		for(String o : ths.fileBrowserFields.keySet()) {
			fileBrowserFields.put(o, ths.fileBrowserFields.get(o));
		}
		//.addAll(ths.);
	}
	*/
	
	public void setProblem(String name, boolean problem) {
		problems.put(name, problem);
	}

	public boolean getProblem(String name) {
		Boolean out = problems.get(name);
		if(out == null) {
			return false;
		}
		return out;
	}
	
	public byte[] getImageIconAsBytes(String name) {
		return icons.get(name);
	}
	
	public ImageIconByteArrayHolder getImageIconBestEffort(String name) {
		ImageIconByteArrayHolder out = iconsAsImages.get(name);
		if(out != null) {
			return out;
		}
		byte[] bytes = icons.get(name);
		if(bytes == null) {
			return null;
		}
		return new ImageIconByteArrayHolder(icons.get(name));
	}
	
	public void setImageIconAsImageIcon(String name, ImageIconByteArrayHolder imageIcon) {
		//System.out.println("DBTableHandlingSpecification<"+getTableName()+">.setImageIconAsImageIcon("+name+", image)");
		iconsAsImages.put(name, imageIcon);
	}

	public void setClassTextRenderer(TextRenderer _tr) {
		classRenderer = _tr;
	}
	
	public TextRenderer getClassTextRenderer() {
		if(classRenderer == null) {
			classRenderer = new ExplicitTextTextRenderer("Default renderer. Undefined.", cs.getName(), -1, -1, -1, -1);
		}
		return classRenderer;
	}
		
	public void setInstanceTextRenderer(TextRenderer _tr) {
		instanceRenderer = _tr;
	}
	
	public TextRenderer getInstanceTextRenderer() {
		if(instanceRenderer == null) {
			return new DefaultRownodeTextRenderer(this);
		}
		return instanceRenderer;
	}
		
	public void addReferrerTreeTextRenderer(String name, TextRenderer tr) {
		//System.out.println("-> Type "+getName()+" got referrer renderer for name "+name+" of instance "+_tr);
		referrerTreeRenderers.put(name.toUpperCase(), tr);
	}
	
	public TextRenderer getReferrerTreeTextRenderer(String name) {
		TextRenderer out = referrerTreeRenderers.get(name.toUpperCase());
		if(out == null) {
			return new ExplicitTextTextRenderer("Default renderer. Undefined.", cs.getName(), -1, -1, -1, -1);
		}
		else {
			//System.out.println("<- Type "+getName()+" found referrer renderer on name "+name+" namely "+out);
		}
		return out;
	}
		
	public void addReferenceTreeTextRenderer(String name, TextRenderer _tr) {
		//System.out.println("-> Type "+getName()+" got reference renderer for name "+name+" of instance "+_tr);
		referenceTreeRenderers.put(name.toUpperCase(), _tr);
	}
	
	public TextRenderer getReferenceTreeTextRenderer(String name) {
		if(name == null) {
			return null;
		}
		TextRenderer out = referenceTreeRenderers.get(name.toUpperCase());
		if(out == null) {
			//return new ExplicitTextTextRenderer(getName());
		}
		else {
			//System.out.println("<- Type "+getName()+" found reference renderer on name "+name+" namely "+out);
		}
		return out;
	}
		
	public void addReferenceDialogTextRenderer(String name, TextRenderer _tr) {
		//System.out.println("-> Type "+getName()+" got reference renderer for name "+name+" of instance "+_tr);
		referenceDialogRenderers.put(name.toUpperCase(), _tr);
	}
	
	public TextRenderer getReferenceDialogTextRenderer(String name) {
		TextRenderer out = referenceDialogRenderers.get(name.toUpperCase());
		if(out != null) {
			return out;
		}
		return getReferenceTreeTextRenderer(name);
	}
		
	public void setCreateStatement(CreateStatement _cs) {
		cs = _cs;
	}
	
	public CreateStatement getCreateStatement() {
		return cs;
	}
	
	public void setIconfile(String iconfile) {
		//System.out.println("DBTableHandlingSpecification<"+getName()+">.setIconfile("+iconfile+")");
		this.iconfile = iconfile;
	}
	
	public String getIconfile() {
		//System.out.println("DBTableHandlingSpecification<"+getName()+">.getIconfile() -> "+iconfile);
		return iconfile;
	}
	
	public byte[] getIconfileBytes() {
		//System.out.println("DBTableHandlingSpecification.getIconfileBytes ej implementerad");
		try {
			return ((HierarchyLeaf) hs.getFileSystemReferenceCatalog().getChild(iconfile)).getByteArray();
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public void setCache(String flag, Object obj) {
		ht.put(flag, obj);
	}
	
	public Object getCache(String flag) {
		return ht.get(flag);
	}
	
	
	public boolean hasKnownLevel() {
		return level != -1;
	}
	
	public void setLevel(int _level) {
		level = _level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void addReferringConstraint(ForeignKeyDeclaration c) throws Exception {
		if(referringConstraints.contains(c)) {
			return;
		}
		for(int i = 0 ; i < c.getNoReferredTableMembers() ; i++) {
			if(!hasColumn(c.getReferredTableMember(i))) {
				throw new Exception(
						"Table "+cs.getName()+" got referring constraint from table "+c.getReferringTablename()+
						"\npointing to member "+c.getReferredTableMember(i)+" which does not exist.");
			}
		}
		referringConstraints.addElement(c);
	}
	
	public int getNoReferringConstraints() {
		return referringConstraints.size();
	} 
	
	public ForeignKeyDeclaration getReferringConstraint(int idx) {
		if(idx < 0) {
			return null;
		}
		if(idx >= referringConstraints.size()) {
			return null;
		}
		return referringConstraints.elementAt(idx);
	}
	
	public boolean hasColumn(String name) {
		return cs.hasColumn(name);
	}
	
	public Column getColumn(String name) {
		return cs.getColumn(name);
	}
	
	public void addAutoUpdate(String name) {
		autoupdate.add(name);
	}
	
    public void addOnAllUpdate(ProgramBlock prog) {
    	onAllUpdates.add(prog);
    }
    
    public ProgramBlock getOnAllUpdate(int idx) {
    	return onAllUpdates.get(idx);
    }
    
    public int getNoOnAllUpdate() {
    	return onAllUpdates.size();
    }
    
    public void addOnNewUpdate(ProgramBlock prog) {
    	onNewUpdates.add(prog);
    }
    
    public ProgramBlock getOnNewUpdate(int idx) {
    	return onNewUpdates.get(idx);
    }
    
    public int getNoOnNewUpdate() {
    	return onNewUpdates.size();
    }
    
// -----
    
    public void addOnInsertExecute(ProgramBlock prog) {
    	onInsertExecutes.add(prog);
    }
    
    public ProgramBlock getOnInsertExecute(int idx) {
    	return onInsertExecutes.get(idx);
    }
    
    public int getNoOnInsertExecute() {
    	return onInsertExecutes.size();
    }
    
    public void addOnUpdateExecute(ProgramBlock prog) {
    	onUpdateExecutes.add(prog);
    }
    
    public ProgramBlock getOnUpdateExecute(int idx) {
    	return onUpdateExecutes.get(idx);
    }
    
    public int getNoOnUpdateExecute() {
    	return onUpdateExecutes.size();
    }
    
    public void addOnInsertAndUpdateExecute(ProgramBlock prog) {
    	onInsertAndUpdateExecutes.add(prog);
    }
    
    public ProgramBlock getOnInsertAndUpdateExecute(int idx) {
    	return onInsertAndUpdateExecutes.get(idx);
    }
    
    public int getNoOnInsertAndUpdateExecute() {
    	return onInsertAndUpdateExecutes.size();
    }
    
	public boolean hasAutoUpdate(String name) {
		return autoupdate.contains(name);
	}
	
	public Set<String> getAutoUpdateNameSet() {
		return autoupdate;
	}
	
	private HashMap<String, String> fileBrowserFields = new HashMap<String, String>();

    public void addFileBrowserManaged(String filenameFieldname, String blobFiledname) {
    	fileBrowserFields.put(filenameFieldname, blobFiledname);
    }
    
    public boolean hasFileBrowserField(String fieldname) {
    	return fileBrowserFields.keySet().contains(fieldname);
    }
    
    public boolean hasFileBrowserFieldBlobMapping(String fieldname) {
    	return fileBrowserFields.get(fieldname) != null;
    }
    
    public String getFileBrowserFieldBlobMapping(String fieldname) {
    	return fileBrowserFields.get(fieldname);
    }
    
    public void addTranslation(String name, String other) {
    	translations.put(name, other);
    }
    
    public String translate(String name) {
    	String out = translations.get(name);
    	if(out == null) {
    		return name;
    	}
    	return out;
    }
    
    public void setTableRenderer(TableRenderer _tr) {
    	tr = _tr;
    }
    
    public TableRenderer getTableRenderer() {
    	if(tr == null) {
    		tr = new DefaultTableRenderer(null, cs);
    	}
    	return tr;
    }
    
    private Vector<Integer> separatorLines = new Vector<Integer>();
    
    public void addSeparatorLine(int line) {
    	separatorLines.add(line);
    }
    
    public boolean hasSeparator(int linenr) {
    	return separatorLines.contains(linenr);
    }
    
    public static class ButtonProgramCouple {
    	public String buttontext;
    	public ProgramBlock prog;
    	public ButtonProgramCouple(String _buttontext, ProgramBlock _prog) {
    		buttontext = _buttontext;
    		prog = _prog;
    	}
    }
    
    private Vector<ButtonProgramCouple> buttons = new Vector<ButtonProgramCouple>();
    
    public void addOnButtonExecute(String buttontext, ProgramBlock prog) {
    	buttons.add(new ButtonProgramCouple(buttontext, prog));
    }
    
    public int getNoButtons() {
    	return buttons.size();
    }
    
    public String getButtonText(int i) {
    	return buttons.elementAt(i).buttontext;
    }
    
    public ProgramBlock getButtonProgram(int i) {
    	return buttons.elementAt(i).prog;
    }
    
    public boolean hasTextField() {
    	return cs.hasTextField();
    }
    
    public int getNoForeignKeyDeclarations() {
    	/*
    	System.out.println("TableConfiguration.getNoForeignKeyDeclarations start");
    	for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
    		System.out.println("FKD "+i+" . "+cs.getForeignKeyDeclaration(i));
    	}
    	System.out.println("TableConfiguration.getNoForeignKeyDeclarations slut");
    	*/
    	return cs.getNoForeignKeyDeclarations();
    }

    public ForeignKeyDeclaration getForeignKeyDeclaration(int idx) {
    	return cs.getForeignKeyDeclaration(idx);
    }
    
    public boolean hasErrors() {
    	return cs.hasErrors();
    }

	public ForeignKeyDeclaration getReferringConstraint(String constraintname) {
		//System.out.println("TableConfiguration.getReferringConstraint constraintname = "+constraintname+", tablename "+getTableName());
		for(ForeignKeyDeclaration fkd : referringConstraints) {
			if(fkd.getConstraintname().equals(constraintname)) {
				return fkd;
			}
			//System.out.println("TableConfiguration.getReferringConstraint constraintname = "+constraintname+" != "+fkd.getName());
		}
		return null;
	}

}
