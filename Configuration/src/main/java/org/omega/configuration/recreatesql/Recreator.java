package org.omega.configuration.recreatesql;

import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;

public interface Recreator {
	
	public String[] create(CreateStatement cs); 
	
	public String selectAll(CreateStatement cs);
	
	public String getInsert(CreateStatement cs, Hashtable<String, Object> ht) throws Exception;
	
	public String selectIndexed(CreateStatement cs, PrimaryKey pk) throws Exception;
	
	public String selectIndexed(CreateStatement cs, ForeignKey fk) throws Exception;
	
	public String selectIndexed(CreateStatement cs, Key k) throws Exception;
	
	public String selectSought(CreateStatement cs, Hashtable<String, Object> ht) throws Exception;
	
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht, PrimaryKey pk) throws Exception;

	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht) throws Exception;
	
	public String getDelete(CreateStatement cs, PrimaryKey pk) throws Exception;
	
	public String getDelete(CreateStatement cs, int key) throws Exception;
	
	public String formDate(String default_) throws Exception;
	
	public boolean isIntegrityConstraintViolation(SQLException sqle) throws Exception;
	
	public boolean isConstraintViolation(Exception e);

	public boolean isUniqueConstraintViolation(Exception e);
	
	public String getDBsName();
	
	public String tableLength(String tablename, CreateStatement cs);
	
	public String getMatchingOperator();

	public String getWildcard();

	public String[] dropTable(CreateStatement cs); 
	
	public String deleteColumn(CreateStatement cs, Column col);
	
	public String addColumn(CreateStatement cs, Column col);
	
	public String changeColumn(CreateStatement cs, Column col);
	
	public String columnType(Column col);
	
	public String getNameCut(String name, CreateStatement cs);
	
	public String[] getCurrentDateMacros();

	public String getUploadBlob(CreateStatement cs, String columnName, PrimaryKey pk) throws Exception;
	
	public String getDownloadBlob(CreateStatement cs, String columnName, PrimaryKey pk) throws Exception;
	
	public String[] allNonBlobMembers(CreateStatement cs);
	
}
