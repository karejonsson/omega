package org.omega.configuration.dbreferences;

import java.util.Hashtable;
import java.util.Set;

import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;

public class Key {
	
	private KeyDeclaration kd = null;
	private Hashtable<String, Object> ht = new Hashtable<String, Object>();
	
	public Key(KeyDeclaration _kd) {
		kd = _kd;
	}
	
	public KeyDeclaration getKeyDeclaration() {
		return kd;
	}
	
	public void set(String name, Object value) {
		if(value != null) {
			ht.put(name.toUpperCase(), value);
		}
		else {
			ht.remove(name.toUpperCase());
		}
	}
	
	public Object get(String name) throws Exception {
		//System.out.println("PrimaryKey.get("+name+") 1");
		if(!kd.hasMember(name)) {
			//System.out.println("PrimaryKey.get("+name+") 2");
			throw new Exception("Member "+name+" unknown to key "+kd+". Get attempted.");
		}
		//System.out.println("PrimaryKey.get("+name+") 3");
		return ht.get(name.toUpperCase());
	}
	
	public Set<String> getNames() {
		return kd.getNames();
	}
	
	public boolean hasName(String name) {
		return kd.hasMember(name);
	}

	public boolean equals(Object obj) {
		if(obj == null) {
			//System.out.println("Key.equals 1");
			return false;
		}
		if(obj == this) {
			//System.out.println("Key.equals 2");
			return true;
		}
		if(!(obj instanceof Key)) {
			//System.out.println("Key.equals 3 type "+obj.getClass().getName());
			return false;
		}
		Key k = (Key) obj;
		if(!kd.equals(k.getKeyDeclaration())) {
			//System.out.println("Key.equals 4 deklarationer 1"+kd+", 2"+k.getKeyDeclaration());
			return false;
		}
		try {
			for(String name : k.getNames()) {
				Object pkobj = k.get(name);
				Object okobj = get(name);
				if(pkobj == null && okobj == null) {
					continue;
				}
				if(pkobj != null && okobj == null) {
					//System.out.println("Key.equals 5");
					return false;
				}
				if(pkobj == null && okobj != null) {
					//System.out.println("Key.equals 6");
					return false;
				}
				if(!pkobj.equals(okobj)) {
					if(pkobj instanceof Integer) {
						if((new Long((Integer)pkobj)).equals(okobj)) {
							continue;
						}
					}
					if(okobj instanceof Integer) {
						if((new Long((Integer)okobj)).equals(pkobj)) {
							continue;
						}
					}
					//System.out.println("Key.equals 7, 1 "+pkobj+" typ "+pkobj.getClass().getName()+", 2 "+okobj+" typ "+okobj.getClass().getName());
					return false;
				}
			}
		}
		catch(Exception e) {
			//System.out.println("Key.equals 8 ");
			e.printStackTrace();
			return false;
		}
		//System.out.println("Key.equals 9 this"+this+", other "+obj);
		return true;
	}

	public static PrimaryKey assemblePrimaryKey(Hashtable<String, Object> ht, PrimaryKeyDeclaration pkd) throws Exception {
		//System.out.println("PrimaryKey.assemblePrimaryKey ht = "+ht+", pkd="+pkd);
		
		PrimaryKey out = new PrimaryKey(pkd);
		for(String name : pkd.getNames()) {
			//System.out.println("PrimaryKey.assemblePrimaryKey name = "+name+", ht = "+ht);
			Object obj = null;
			try {
				obj = ht.get(name.toUpperCase());
			}
			catch(NullPointerException npe) {
			}
			//System.out.println("PrimaryKey.assemblePrimaryKey name = "+name+", ht = "+ht+", obj = "+obj);
			out.set(name, obj);
		}
		return out;
	}
	
	public int hashCode() {
		int out = 0;
		for(String name : getNames()) {
			try {
				Object fkobj = get(name.toUpperCase());
				if(fkobj != null) {
					out +=  fkobj.hashCode();
				}
				out += name.toUpperCase().hashCode();
			}
			catch(Exception e) {
				
			}
		}
		
		return out;
	}
	
	public boolean hasCompleteSetOfValues() {
		try {
			for(int i = 0 ; i < kd.getNoMembers() ; i++) {
				Object obj = get(kd.getMember(i));
				if(obj == null) {
					return false;
				}
			}
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	public String getTablename() {
		return kd.getTablename();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("Key "+getTablename()+":("+kd.getMember(0)+"="+get(kd.getMember(0)));
			for(int i = 1 ; i < kd.getNoMembers() ; i++) {
				sb.append(", "+kd.getMember(i)+"="+get(kd.getMember(i)));
			}
			sb.append(")");
			if(!hasCompleteSetOfValues()) {
				sb.append(" Set of values is not complete!");
			}
		}
		catch(Exception e) {
			return "toString exception "+e+" after assembling "+sb.toString();
		}
		return sb.toString()+")";
	}

	
}
