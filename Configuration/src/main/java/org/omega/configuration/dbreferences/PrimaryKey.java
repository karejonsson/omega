package org.omega.configuration.dbreferences;

import java.util.Hashtable;
import java.util.Set;

import org.omega.configuration.struct.PrimaryKeyDeclaration;

public class PrimaryKey {
	
	private PrimaryKeyDeclaration pkd = null;
	private Hashtable<String, Object> ht = new Hashtable<String, Object>();
	
	public PrimaryKey(PrimaryKeyDeclaration _pkd) {
		pkd = _pkd;
	}
	
	public PrimaryKeyDeclaration getPrimaryKeyDeclaration() {
		return pkd;
	}
	
	public void set(String name, Object value) {
		if(value != null) {
			ht.put(name.toUpperCase(), value);
		}
		else {
			ht.remove(name.toUpperCase());
		}
	}
	
	public Object get(String name) throws Exception {
		//System.out.println("PrimaryKey.get("+name+") 1");
		if(!pkd.hasMember(name)) {
			//System.out.println("PrimaryKey.get("+name+") 2");
			throw new Exception("Member "+name+" unknown to Primary key "+pkd+". Get attempted.");
		}
		//System.out.println("PrimaryKey.get("+name+") 3");
		return ht.get(name.toUpperCase());
	}
	
	public Set<String> getNames() {
		return pkd.getNames();
	}
	
	public boolean hasName(String name) {
		return pkd.hasMember(name);
	}

	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		if(!(obj instanceof PrimaryKey)) {
			return false;
		}
		PrimaryKey pk = (PrimaryKey) obj;
		if(!pkd.equals(pk.getPrimaryKeyDeclaration())) {
			return false;
		}
		try {
			for(String name : pk.getNames()) {
				Object pkobj = pk.get(name);
				Object fkobj = get(name);
				if(pkobj == null && fkobj == null) {
					continue;
				}
				if(pkobj != null && fkobj == null) {
					return false;
				}
				if(pkobj == null && fkobj != null) {
					return false;
				}
				if(!pkobj.equals(fkobj)) {
					return false;
				}
			}
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}

	public static PrimaryKey assemblePrimaryKey(Hashtable<String, Object> ht, PrimaryKeyDeclaration pkd) throws Exception {
		//System.out.println("PrimaryKey.assemblePrimaryKey ht = "+ht+", pkd="+pkd);
		
		PrimaryKey out = new PrimaryKey(pkd);
		for(String name : pkd.getNames()) {
			//System.out.println("PrimaryKey.assemblePrimaryKey name = "+name+", ht = "+ht);
			Object obj = null;
			try {
				obj = ht.get(name.toUpperCase());
			}
			catch(NullPointerException npe) {
			}
			//System.out.println("PrimaryKey.assemblePrimaryKey name = "+name+", ht = "+ht+", obj = "+obj);
			out.set(name, obj);
		}
		return out;
	}
	
	public int hashCode() {
		int out = 0;
		for(String name : getNames()) {
			try {
				Object fkobj = get(name.toUpperCase());
				if(fkobj != null) {
					out +=  fkobj.hashCode();
				}
				out += name.toUpperCase().hashCode();
			}
			catch(Exception e) {
				
			}
		}
		
		return out;
	}
	
	public boolean hasCompleteSetOfValues() {
		try {
			for(int i = 0 ; i < pkd.getNoMembers() ; i++) {
				Object obj = get(pkd.getMember(i));
				if(obj == null) {
					return false;
				}
			}
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	public String getTablename() {
		return pkd.getTablename();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("Primary key "+getTablename()+":("+pkd.getMember(0)+"="+get(pkd.getMember(0)));
			for(int i = 1 ; i < pkd.getNoMembers() ; i++) {
				sb.append(", "+pkd.getMember(i)+"="+get(pkd.getMember(i)));
			}
			sb.append(")");
			if(!hasCompleteSetOfValues()) {
				sb.append(" Set of values is not complete!");
			}
		}
		catch(Exception e) {
			return "toString exception "+e+" after assembling "+sb.toString();
		}
		return sb.toString()+")";
	}

	
}
