package org.omega.configuration.dbreferences;

import java.util.Hashtable;
import java.util.Set;

import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;

public class ForeignKey {
	
	private ForeignKeyDeclaration fkd = null;
	private Hashtable<String, Object> ht = new Hashtable<String, Object>();
	
	public ForeignKey(ForeignKeyDeclaration _fkd) {
		fkd = _fkd;
	}
	
	public void clear() {
		ht.clear();
	}
	
	public ForeignKey copy() {
		ForeignKey out = new ForeignKey(fkd);
		for(String key : ht.keySet()) {
			out.ht.put(key, ht.get(key));
		}
		return out;
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclaration() {
		return fkd;
	}
	
	public void set(String name, Object value) throws Exception {
		if(!fkd.hasReferringMember(name)) {
			String message = "Member "+name+" unknown to Foreign key "+fkd+". Set to "+value;
			throw new Exception(message);
		}
		//System.out.println("ForeignKey.set : name = "+name+", value "+value);
		if(value != null) {
			ht.put(name.toUpperCase(), value);
		}
		else {
			ht.remove(name.toUpperCase());
		}
	}
	
	public Object get(String name) throws Exception {
		if(!fkd.hasReferringMember(name)) {
			String message = "Member "+name+" unknown to Foreign key "+fkd+". Get attempted.";
			throw new Exception(message);
		}
		Object out = ht.get(name.toUpperCase());
		return out;
	}
	
	public Set<String> getNames() {
		return fkd.getReferringMemberNames();
	}
	
	public String getTablenamePointingTo() {
		return fkd.getReferencedTablename();
	}
	
	public String getTablenamePointingFrom() {
		return fkd.getReferringTablename();
	}
	
	public String getName() {
		return fkd.getConstraintname();
	}
	
	public boolean hasMember(String name) {
		return fkd.hasReferringMember(name);
	}
	
	public static ForeignKey assembleForeignKey(Hashtable<String, Object> ht, ForeignKeyDeclaration fkd) throws Exception {
		ForeignKey out = new ForeignKey(fkd);
		for(String name : fkd.getReferringMemberNames()) {
			out.set(name, ht.get(name.toUpperCase()));
			//System.out.println("ForeignKey.assembleForeignKey("+name+", "+ht.get(name.toUpperCase())+")");
		}
		return out;
	}
	
	public static ForeignKey assembleForeignKey(PrimaryKey pk, ForeignKeyDeclaration fkd) throws Exception {
		ForeignKey out = new ForeignKey(fkd);
		if(pk == null) {
			return out;
		}
		for(int i = 0 ; i < fkd.getNoReferredTableMembers() ; i++) { 
			out.set(fkd.getReferringMember(i), pk.get(fkd.getReferredTableMember(i)));
		}
		return out;
	}
	
	public static ForeignKey assembleForeignKey(Key k, ForeignKeyDeclaration fkd) throws Exception {
		ForeignKey out = new ForeignKey(fkd);
		if(k == null) {
			return out;
		}
		if(!k.getKeyDeclaration().isUnique()) {
			return null;
		}
		for(int i = 0 ; i < fkd.getNoReferredTableMembers() ; i++) { 
			out.set(fkd.getReferringMember(i), k.get(fkd.getReferredTableMember(i)));
		}
		return out;
	}
	
	public PrimaryKey assemblePrimaryKey(PrimaryKeyDeclaration pkd) throws Exception {
		//System.out.println("ForeignKey.assemblePrimaryKey "+toString()+" -> structure "+pkd);
		PrimaryKey out = new PrimaryKey(pkd);
		for(int i = 0 ; i < fkd.getNoReferringMembers() ; i++) {
			/*
			System.out.println("-------- "+System.currentTimeMillis());
			System.out.println("ForeignKey.assemblePrimaryKey pkd.getMember("+i+")="+pkd.getMember(i));
			System.out.println("ForeignKey.assemblePrimaryKey fkd.getMember("+i+")="+fkd.getMember(i));
			System.out.println("ForeignKey.assemblePrimaryKey get(fkd.getMember("+i+"))="+get(fkd.getMember(i)));
			System.out.println("ForeignKey.assemblePrimaryKey fkd="+fkd);
			System.out.println("ForeignKey.assemblePrimaryKey this="+this);
			*/
			out.set(pkd.getMember(i), get(fkd.getReferringMember(i)));
		}
		//System.out.println("ForeignKey.assemblePrimaryKey "+toString()+" -> instance "+out);
		return out;
	}
	
	public PrimaryKey assemblePrimaryKey() throws Exception {
		return assemblePrimaryKey(fkd.getPrimaryKeyDeclaration());
	}
	
	public boolean compliant(PrimaryKey pk) throws Exception {
		//System.out.println("ForeignKey.compliant (1)");
		if(!fkd.compliant(pk)) {
			//System.out.println("ForeignKey.compliant NOK (2)");
			return false;
		}
		for(int i = 0 ; i < pk.getPrimaryKeyDeclaration().getNoMembers() ; i++) { // String name : pk.getNames()) {
			String pkname = pk.getPrimaryKeyDeclaration().getMember(i);
			String fkname = fkd.getReferringMember(i);
			Object pkobj = pk.get(pkname);
			Object fkobj = get(fkname);
			if(pkobj == null && fkobj == null) {
				System.out.println("ForeignKey.compliant values null");
				continue;
			}
			if(pkobj != null && fkobj == null) {
				//System.out.println("ForeignKey.compliant value difference "+pkobj+" != "+fkobj);
				return false;
			}
			if(pkobj == null && fkobj != null) {
				//System.out.println("ForeignKey.compliant value difference "+pkobj+" != "+fkobj);
				return false;
			}
			if(!pkobj.equals(fkobj)) {
				//System.out.println("ForeignKey.compliant value difference "+pkobj+" != "+fkobj);
				return false;
			}
			if(pkname.toUpperCase().compareTo(fkd.getReferredTableMember(i).toUpperCase()) != 0) {
				//System.out.println("ForeignKey.compliant member namne differs "+pkname+" != "+fkd.getTableMember(i));
				return false;
			}
		}
		//System.out.println("ForeignKey.compliant OK");
		return true;
	}
	
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		if(!(obj instanceof ForeignKey)) {
			return false;
		}
		ForeignKey fk = (ForeignKey) obj;
		if(!fkd.equals(fk.getForeignKeyDeclaration())) {
			//System.out.println("ForeignKey.compliant NOK (2)");
			return false;
		}
		//System.out.println("ForeignKey.compliant (1)");
		for(int i = 0 ; i < fkd.getNoReferringMembers() ; i++) { // String name : pk.getNames()) {
			String membername = fkd.getReferringMember(i);
			//String othersname = fk.getForeignKeyDeclaration().getMember(i);
			
			Object ownobject = null;
			Object othersobject = null;
			try {
				ownobject = get(membername);
				othersobject = fk.get(membername);
			}
			catch(Exception e) {
				return false;
			}
			if(ownobject == null && othersobject == null) {
				//System.out.println("ForeignKey.compliant values null");
				continue;
			}
			if(ownobject != null && othersobject == null) {
				//System.out.println("ForeignKey.compliant value difference "+ownobject+" != "+othersobject);
				return false;
			}
			if(ownobject == null && othersobject != null) {
				//System.out.println("ForeignKey.compliant value difference "+ownobject+" != "+othersobject);
				return false;
			}
			if(!ownobject.equals(othersobject)) {
				//System.out.println("ForeignKey.compliant value difference "+ownobject+" != "+othersobject);
				return false;
			}
		}
		//System.out.println("ForeignKey.compliant OK");
		return true;
	}
	
	public boolean hasCompleteSetOfValues() {
		try {
			for(int i = 0 ; i < fkd.getNoReferringMembers() ; i++) {
				Object obj = get(fkd.getReferringMember(i));
				if(obj == null) {
					return false;
				}
			}
			return true;
		}
		catch(Exception e) {
			String message = e.getMessage();
			return false;
		}
	}
	
	public String toString() {
		if(fkd == null) {
			return "Foreign key without definition";
		}
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("Foreign key "+fkd.getConstraintname()+" "+fkd.getReferringTablename()+":("+fkd.getReferringMember(0)+"="+get(fkd.getReferringMember(0)));
			for(int i = 1 ; i < fkd.getNoReferringMembers() ; i++) {
				sb.append(", "+fkd.getReferringMember(i)+"="+get(fkd.getReferringMember(i)));
			}
			sb.append(") -> "+fkd.getReferencedTablename()+":("+fkd.getReferredTableMember(0));
			for(int i = 1 ; i < fkd.getNoReferredTableMembers() ; i++) {
				sb.append(", "+fkd.getReferredTableMember(i));
			}
			sb.append(")");
			if(!hasCompleteSetOfValues()) {
				sb.append(" Set of values is not complete!");
			}
			if(fkd.getNoReferringMembers() != fkd.getNoReferredTableMembers()) {
				sb.append(" Field number mismatch!");
			}
		}
		catch(Exception e) {
			return "toString exception "+e+" after assembling "+sb.toString();
		}
		return sb.toString();
	}
	
}