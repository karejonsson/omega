package org.omega.configuration.values;

import java.util.Date;

public class LobValue {

	//private boolean isNulled = true;
	private byte[] filename;
	private int length;

	public LobValue() {
		//System.out.println("LobValue.<ctor> ");
	}
	
	public byte[] getFilename() {
		return filename;
	}
	
	public void setFilename(byte[] filename) {
		//System.out.println("LobValue.setFilename "+(new String(filename)));
		/*
		if(filename == null) {
			System.out.println("LobValue.setFilename - null");
			(new Throwable()).printStackTrace();
		}
		*/
		this.filename = filename;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	private boolean databaseBased = true;
	
	public boolean isDatabaseBased() {
		return databaseBased;
	}
	
	public void setDatabaseBased(boolean databaseBased) {
		this.databaseBased = databaseBased;
	}
	
	public Long getLong()
	{
		return null;
	}

	public Double getDouble()
	{
		return null;
	}

	public Boolean getBoolean()
	{
		return null;
	}

	public Date getDate()
	{
		return null;
	}

	public String getString() {
		return null;
	}

	public Object getObject() {
		if(filename == null) {
			return "<Lob not set>";
		}
		return new String(filename)+" size "+length;
	}
	
	public String state() {
		String stFilename = new String(filename != null ? filename : "no bytes".getBytes());
		return "LobValue (Filename "+stFilename+", length "+length+/*", baseAware "+baseAware+*/", databaseBased "+databaseBased+") ";
	}

	public String toString() { 
		return "Lob value: length "+length+", filename "+(new String(filename));
	}
	
}