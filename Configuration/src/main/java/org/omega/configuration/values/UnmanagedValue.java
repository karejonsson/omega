package org.omega.configuration.values;

import org.omega.configuration.struct.UnmanageableType;

public class UnmanagedValue {
	
	private static final String defaultName = "Unmanaged type";
	private String name = null;
	
	public UnmanagedValue() {
		name = defaultName;
	}

	public UnmanagedValue(String _name) {
		name = _name;
	}
	
	public UnmanagedValue(UnmanageableType ut) {
		name = "Unmanaged type "+ut.getTypename();
	}
	
	public String toString() {
		return name;
	}

}
