package org.omega.configuration.renderers;

public class SpecifiedTextRenderer implements TextRenderer {

	private String sourceSpecification = null;
	private String script = null;
	private int beginLine;
	private int beginColumn;
	private int endLine;
	private int endColumn;

	public SpecifiedTextRenderer(String sourceSpecification, String script, int beginLine, int beginColumn, int endLine, int endColumn) {
		this.sourceSpecification = sourceSpecification;
		this.script = script;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public String getScript() {
		return script;
	}
	
	public int getBeginLine() {
		return beginLine;
	}
	
	public int getBeginColumn() {
		return beginColumn;
	}

	public int getEndLine() {
		return endLine;
	}
	
	public int getEndColumn() {
		return endColumn;
	}

	public String getScriptFromDescription() {
		return 
				"from "+sourceSpecification+"\n"+
				"Line "+beginLine+", column "+beginColumn+" to line "+endLine+", column "+endColumn;
	}

}	
