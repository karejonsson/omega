package org.omega.configuration.renderers;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;

public class DefaultTextRenderer implements TextRenderer {

	private String sourceSpecification = null;
	private CreateStatement cs = null;
	private Column column;

	public DefaultTextRenderer(String sourceSpecification, CreateStatement cs, Column column) {
		this.sourceSpecification = sourceSpecification;
		this.cs = cs;
		this.column = column;
	}
	
	public int getBeginLine() {
		return -1;
	}
	
	public int getBeginColumn() {
		return -1;
	}

	public int getEndLine() {
		return -1;
	}
	
	public int getEndColumn() {
		return -1;
	}

	
	public String getScript() {
		return "return node."+column.getName()+";";
	}

	public String getScriptFromDescription() {
		return 
				"from "+sourceSpecification+"\n"+
				"Default for table "+cs.getName()+", column "+column.getName();
	}

}
