package org.omega.configuration.renderers;

public interface TableRenderer {
	
	public int getNoColumns();
	public String getColumnName(int i);
	public TextRenderer getColumnRenderer(int i);
	public String getScriptFromDescription();
	public boolean hasColumn(String colname);
	
}
