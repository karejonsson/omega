package org.omega.configuration.renderers;

public interface TextRenderer {
	
	public String getScript();
	public String getScriptFromDescription();
	public int getBeginLine();	
	public int getBeginColumn() ;
	public int getEndLine();
	public int getEndColumn();

}
