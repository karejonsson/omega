package org.omega.configuration.renderers;

import org.omega.configuration.struct.CreateStatement;

public class DefaultTableRenderer implements TableRenderer {

	private String sourceSpecifcation = null;
	private CreateStatement cs = null;
	
	public DefaultTableRenderer(String sourceSpecifcation,  CreateStatement cs) {
		this.sourceSpecifcation = sourceSpecifcation;
		this.cs = cs;
	}
	
	public String getScriptFromDescription() {
		return "from "+sourceSpecifcation+". Default for table "+cs.getName();
	}

	public String getColumnName(int i) {
		return cs.getColumn(i).getName();
	}
	
	public TextRenderer getColumnRenderer(int i) {
		return new DefaultTextRenderer(sourceSpecifcation, cs, cs.getColumn(i));
	}

	public int getNoColumns() {
		return cs.getNoColumns();
	}

	public boolean contains(String s) {
		return cs.hasColumn(s);
	}
	
	public boolean hasColumn(String colname) {
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			if(cs.getColumn(i).getName().equals(colname)) {
				return true;
			}
		}
		return false;
	}
}
