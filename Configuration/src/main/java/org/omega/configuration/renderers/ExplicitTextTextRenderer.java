package org.omega.configuration.renderers;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;

public class ExplicitTextTextRenderer implements TextRenderer {

	private String sourceSpecification = null;
	private String explicitText;
	private int beginLine;
	private int beginColumn;
	private int endLine;
	private int endColumn;

	public ExplicitTextTextRenderer(String sourceSpecification, String explicitText, int beginLine, int beginColumn, int endLine, int endColumn) {
		this.sourceSpecification = sourceSpecification;
		this.explicitText = explicitText;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public int getBeginLine() {
		return beginLine;
	}
	
	public int getBeginColumn() {
		return beginColumn;
	}

	public int getEndLine() {
		return endLine;
	}
	
	public int getEndColumn() {
		return endColumn;
	}

	public String getScript() {
		return "return \""+explicitText+"\";";
	}

	public String getScriptFromDescription() {
		return 
				"from "+sourceSpecification+" with explicit text.\n"+
				"Line "+beginLine+", column "+beginColumn+" to line "+endLine+", column "+endColumn;
	}

}