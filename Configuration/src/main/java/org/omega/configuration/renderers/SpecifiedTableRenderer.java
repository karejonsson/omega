package org.omega.configuration.renderers;

import java.util.Vector;

public class SpecifiedTableRenderer implements TableRenderer  {

	private String sourceSpecifcation = null;
	private int beginLine;
	private int beginColumn;
	private int endLine;
	private int endColumn;

	private Vector<String> names = new Vector<String>();
	private Vector<TextRenderer> columns = new Vector<TextRenderer>();
	
	public SpecifiedTableRenderer(String sourceSpecifcation, int beginLine, int beginColumn, int endLine, int endColumn) {
		this.sourceSpecifcation = sourceSpecifcation;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public String getScriptFromDescription() {
		return 
				"from "+sourceSpecifcation+"\n"+
				"Line "+beginLine+", column "+beginColumn+" to line "+endLine+", column "+endColumn;
	}

	public void addSegment(String columnName, TextRenderer colevaluable) {
		names.add(columnName); 
		columns.add(colevaluable);
	}

	@Override
	public int getNoColumns() {
		return names.size();
	}

	@Override
	public String getColumnName(int i) {
		return names.get(i);
	}

	@Override
	public TextRenderer getColumnRenderer(int i) {
		return columns.get(i);
	}

	public boolean hasColumn(String colname) {
		for(int i = 0 ; i < columns.size() ; i++) {
			if(columns.get(i).equals(colname)) {
				return true;
			}
		}
		return false;
	}

}
