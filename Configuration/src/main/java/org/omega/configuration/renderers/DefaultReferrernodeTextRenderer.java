package org.omega.configuration.renderers;

import org.omega.configuration.struct.ForeignKeyDeclaration;

public class DefaultReferrernodeTextRenderer implements TextRenderer {
	
	private String foreignKeyDeclarationName;
	
	public DefaultReferrernodeTextRenderer(String foreignKeyDeclarationName) {
		this.foreignKeyDeclarationName = foreignKeyDeclarationName;
	}

	@Override
	public String getScript() {
		return "return \"Referrers "+foreignKeyDeclarationName+"\";";
	}

	@Override
	public String getScriptFromDescription() {
		return null;
	}
	
	public int getBeginLine() {
		return -1;
	}

	public int getBeginColumn() {
		return -1;
	}

	public int getEndLine() {
		return -1;
	}
	
	public int getEndColumn() {
		return -1;
	}

}
