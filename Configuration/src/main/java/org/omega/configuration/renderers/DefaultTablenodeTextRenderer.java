package org.omega.configuration.renderers;

import org.omega.configuration.model.TableConfiguration;

public class DefaultTablenodeTextRenderer implements TextRenderer {
	
	private TableConfiguration tc;
	
	public DefaultTablenodeTextRenderer(TableConfiguration tc) {
		this.tc = tc;
	}

	public int getBeginLine() {
		return -1;
	}
	
	public int getBeginColumn() {
		return -1;
	}

	public int getEndLine() {
		return -1;
	}
	
	public int getEndColumn() {
		return -1;
	}

	public String render(Object s) {
		return tc.getObjectName()+" Level "+tc.getLevel();
	}

	@Override
	public String getScript() {
		return "return \""+tc.getObjectName()+" Level "+tc.getLevel()+"\";";
	}

	@Override
	public String getScriptFromDescription() {
		return "Default table node renderer for table "+tc.getTableName();
	}
	
}
