package org.omega.configuration.renderers;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.LobType;

import se.modlab.generics.exceptions.IntolerableException;

public class DefaultRownodeTextRenderer implements TextRenderer {

	private TableConfiguration ths = null;
	private String script = null;
	
	public DefaultRownodeTextRenderer(TableConfiguration ths) {
		this.ths = ths;
		script = makeScript();
	}
	
	private BSFManager manager = new BSFManager();
	
	public String makeScript() {
		List<String> used = new ArrayList<String>();
		List<String> primarys = new ArrayList<String>();
		List<String> foreign = new ArrayList<String>();
		List<String> lobs = new ArrayList<String>();
		CreateStatement cs = ths.getCreateStatement();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			String colname = col.getName();
			if(cs.isForeignKeyDeclarationMember(colname)) {
				foreign.add(cs.getColumn(i).getName());
				continue;
			}
			if(cs.isPartOfPrimaryKeyDeclaration(i)) { 
				primarys.add(cs.getColumn(i).getName());
				continue;
			}
			if(col.getType() instanceof LobType) {
				lobs.add(cs.getColumn(i).getName());
				continue;
			}
			used.add(cs.getColumn(i).getName());
		}

		if(used.size() < 1) {
			used.addAll(primarys);
		}

		if(used.size() < 1) {
			used.addAll(foreign);
		}

		if(used.size() < 1) {
			used.addAll(lobs);
		}

		StringBuffer out = new StringBuffer("return node."+used.get(0));

		try {
			for(int i = 1 ; i < used.size() ; i++) {
				try {
					out.append("+\"/\"+node."+used.get(i));
				}
				catch(Exception e) {
					out.append("+\"/null\"");
				}
			}
		}
		catch(Exception ie) {
			out.append("<no information>");
		}
		return out.toString()+";";
	}

	public int getBeginLine() {
		return -1;
	}
	
	public int getBeginColumn() {
		return -1;
	}

	public int getEndLine() {
		return -1;
	}
	
	public int getEndColumn() {
		return -1;
	}

	@Override
	public String getScript() {
		return script;
	}

	@Override
	public String getScriptFromDescription() {
		return "Default row node renderer for table "+ths.getTableName();
	}
	
}
