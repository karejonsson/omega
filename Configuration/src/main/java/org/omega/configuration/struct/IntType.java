package org.omega.configuration.struct;

import java.util.Comparator;

import org.omega.configuration.recreatesql.Recreator;

public abstract class IntType extends NumericType {
	
	private Integer def = 0;

	public IntType() {
		super();
	}
	
	public IntType(int precision) {
		super(precision);
	}
	
	final public void setDefault(int i) {
		hasDefault = true;
		def = i;
	}
	
	final public int getDefault() {
		return def;
	}
	
	public String getDefaultAsString() {
		return ""+def;
	}

	public Object getUntypedDefault(Recreator rec) {
		if(def == null) {
			return null;
		}
		return new Integer(def);
	}
	
	public void setUntypedDefault(Recreator rec, Object obj) throws Exception {
		hasDefault = true;
		if(obj == null) {
			setNullDefault();
			return;
		}
		if(obj instanceof Integer) {
			def = (Integer) obj;
			return;
		}
		if(obj instanceof String) {
			String s = (String) obj;
			s = s.replace("(", "").replace(")", "");
			if(s.toLowerCase().compareTo("null") == 0) {
				setNullDefault();
				def = null;
				return;
			}
			try {
				def = Integer.parseInt(s);
				return;
			}
			catch(Exception e) {
				throw new Exception("Integer type recieved default value "+obj+" as string.");
			}
		}
		throw new Exception("Integer type recieved default value "+obj);
	}

	public String getTypename() {
		if(hasPrecision()) {
			return "int("+getPrecision()+")";
		}
		return "int";
	}
	
	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

}
