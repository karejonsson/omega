package org.omega.configuration.struct;

public class BlobType extends LobType {
	
	private String image = null;

	public BlobType(String image) {
		this.image = image;
	}

	public String getTypename() {
		return image;
	}

}
