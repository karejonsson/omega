package org.omega.configuration.struct;

import java.util.Vector;

public class CreateStatement {
	
	private String name;
	private Vector<Column> columns = new Vector<Column>();
	private PrimaryKeyDeclaration pkd = null;
	private Vector<KeyDeclaration> keyDeclarations = new Vector<KeyDeclaration>();
	private Vector<IndexDeclaration> indexDeclarations = new Vector<IndexDeclaration>();
	private Vector<ForeignKeyDeclaration> foreignKeyDeclarations = new Vector<ForeignKeyDeclaration>();
	private Vector<Parameter> parameters = new Vector<Parameter>();
	private Vector<String> errorsForgiven = new Vector<String>();
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Table name "+name+"\n");
		sb.append("Column count "+columns.size()+"\n");
		for(int i = 0 ; i < columns.size(); i++) {
			sb.append("Column "+columns.elementAt(i)+"\n");
		}
		sb.append("Key count "+keyDeclarations.size()+"\n");
		for(int i = 0 ; i < keyDeclarations.size(); i++) {
			sb.append("Key "+keyDeclarations.elementAt(i)+"\n");
		}
		sb.append("Primary key "+pkd+"\n");
		sb.append("Foreign key declaration count "+foreignKeyDeclarations.size()+"\n");
		for(int i = 0 ; i < foreignKeyDeclarations.size(); i++) {
			sb.append("Foreign key declaration "+foreignKeyDeclarations.elementAt(i)+"\n");
		}
		sb.append("Parameter count "+parameters.size()+"\n");
		for(int i = 0 ; i < parameters.size(); i++) {
			sb.append("Parameter "+parameters.elementAt(i)+"\n");
		}
		return sb.toString();
	}
	
	private String sourceSpecifcation = null;
	private int beginLine;
	private int beginColumn;
	private int endLine;
	private int endColumn;
	
	public void setOrigin(String sourceSpecifcation, int beginLine, int beginColumn, int endLine, int endColumn) {
		this.sourceSpecifcation = sourceSpecifcation;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public String getOriginString() {
		return sourceSpecifcation+", line "+beginLine+" column "+beginColumn+" to line "+endLine+" column "+endColumn;
	}
	
	public void setName(String _name) {
		name = _name;
	}
	
	public String getName() {
		return name;
	}

	public void addColumn(Column col) {
		columns.addElement(col);
	} 
	
	public int getNoColumns() {
		return columns.size();
	}
	
	public Column getColumn(int idx) {
		if(idx < 0) return null;
		if(idx >= columns.size()) return null;
		return columns.elementAt(idx);
	}
	
	public Column getColumn(String name) {
		for(Column col : columns) {
			if(col.getName().toUpperCase().compareTo(name.toUpperCase()) == 0) {
				return col;
			}
		}
		return null;
	}
	
	public boolean isPartOfPrimaryKeyDeclaration(int i) {
		return isPartOfPrimaryKeyDeclaration(getColumn(i));
	}
	
	public boolean isPartOfPrimaryKeyDeclaration(Column col) {
		String name = col.getName();
		return pkd.hasMember(name);
	}
	
	public boolean isPartOfKeyDeclaration(String name) {
		for(int i = 0 ; i < keyDeclarations.size() ; i++) {
			KeyDeclaration k = keyDeclarations.elementAt(i);
			if(k.hasMember(name)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasKeyDeclaration(String[] members) {
		for(int i = 0 ; i < keyDeclarations.size() ; i++) {
			KeyDeclaration k = keyDeclarations.elementAt(i);
			if(k.hasMembers(members)) {
				return true;
			}
		}
		return false;
	}
	
	public Column getAutoIncrementColumn() {
		for(Column col : columns) {
			if(col.isAutoIncrement()) {
				return col;
			}
		}
		return null;
	}
	
	public Vector<Column> getIntegerTypeNotNullColumns() {
		Vector<Column> out = new Vector<Column>();
		for(Column col : columns) {
			Type t = col.getType();
			if(t instanceof IntType) {
				if(t.getNotNull()) {
					out.add(col);
				}
			}
		}
		return out;
	}
	
	public boolean hasTextField() { 
		for(Column col : columns) {
			Type t = col.getType();
			if(t instanceof TextualType) {
				return true;
			}
		}
		return false;
	}
	
	public int getFirstDatetimeColumnNumber() {
		int colnr = 0;
		for(Column col : columns) {
			if(col.getType() instanceof DatetimeType) {
				return colnr;
			}
			colnr++;
		}
		return -1;
	}
	
	public int[] getPossibleValueColumnIndexes() {
		Vector<Integer> v = new Vector<Integer>();
		int colnr = -1;
		for(Column col : columns) {
			colnr++;
			if(col.getType() instanceof NumericType) {
				if(col.isAutoIncrement()) {
					// It is just the unique number for the row. Cant be meaningsful to plot.
					continue;
				}
				if(getFirstForeignKeyDeclarationFromMember(col.getName()) != null) {
					// It is a reference to another table. Cant be meaningsful to plot
					continue;
				}
				v.add(colnr);
			}
		}
		int out[] = new int[v.size()];
		for(int i = 0 ; i < v.size(); i++) {
			out[i] = v.elementAt(i);
		}
		return out;
	}
	
	public ForeignKeyDeclaration getFirstForeignKeyDeclarationFromName(String name) {
		for(ForeignKeyDeclaration con : foreignKeyDeclarations) {
			if(con.getConstraintname().toUpperCase().compareTo(name.toUpperCase()) == 0) {
				return con;
			}
		}
		return null;
	}
	
	public Vector<ForeignKeyDeclaration> getAllForeignKeyDeclarationFromName(String name) {
		Vector<ForeignKeyDeclaration> out = new Vector<ForeignKeyDeclaration>();
		for(ForeignKeyDeclaration con : foreignKeyDeclarations) {
			if(con.getConstraintname().toUpperCase().compareTo(name.toUpperCase()) == 0) {
				out.add(con);
			}
		}
		return out;
	}
	
	public ForeignKeyDeclaration getFirstForeignKeyDeclarationFromMember(String name) {
		for(ForeignKeyDeclaration con : foreignKeyDeclarations) {
			if(con.hasReferringMember(name)) {
				return con;
			}
		}
		return null;
	}
	
	public boolean hasColumn(String name) {
		for(Column col : columns) {
			if(col.getName().toUpperCase().compareTo(name.toUpperCase()) == 0) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isForeignKeyDeclarationMember(String name) {
		for(ForeignKeyDeclaration con : foreignKeyDeclarations) {
			if(con.hasReferringMember(name)) {
				return true;
			}
		}
		return false;
	}
	
	public int getIndexOfForeignKeyDeclaration(ForeignKeyDeclaration fkd) {
		for(int i = 0 ; i < getNoForeignKeyDeclarations() ; i++) {
			ForeignKeyDeclaration indexedFkd = getForeignKeyDeclaration(i);
			if(fkd.equals(indexedFkd)) {
				return i;
			}
		}
		return -1;
	}
	

	
	public int getNumberOfForeignKeyDeclarationMemberships(String name) {
		int out = 0;
		for(ForeignKeyDeclaration con : foreignKeyDeclarations) {
			if(con.hasReferringMember(name)) {
				out++;
			}
		}
		return out;
	}
	
	public void setPrimaryKeyDeclaration(PrimaryKeyDeclaration pkd) {
		this.pkd = pkd;
	}
	
	public PrimaryKeyDeclaration getPrimaryKeyDeclaration() {
		return pkd;
	}
	
	public void addKey(KeyDeclaration k) {
		k.setTablename(name);
		keyDeclarations.addElement(k);
	}
	
	public int getNoKeys() {
		return keyDeclarations.size();
	} 
	
	public KeyDeclaration getKeyDeclaration(int idx) {
		if(idx < 0) return null;
		if(idx >= keyDeclarations.size()) return null;
		return keyDeclarations.elementAt(idx);
	}
	
	public void addIndex(IndexDeclaration id) {
		id.setTablename(name);
		indexDeclarations.addElement(id);
	}
	
	public int getNoIndexes() {
		return indexDeclarations.size();
	}
	
	public IndexDeclaration getIndexDeclaration(int idx) {
		if(idx < 0) return null;
		if(idx >= indexDeclarations.size()) return null;
		return indexDeclarations.elementAt(idx);
	}
	
	public void addForeignKeyDeclaration(ForeignKeyDeclaration fkd) throws Exception {
		for(String membername : fkd.getReferringMemberNames()) {
			Column col = getColumn(membername);
			if(col == null) {
				String msg = "Member "+membername+" part of foreign key added but that column does not exist.\n"+
						"Happened on table "+name+". FK="+fkd;
				errorsForgiven.add(msg);
				throw new Exception(msg);
			}
			col.setForeignKey();
		}
		foreignKeyDeclarations.addElement(fkd);
		fkd.setReferringTablename(name);
	}
	
	public void addForeignKeyDeclarationForgiving(ForeignKeyDeclaration fkd) throws Exception {
		for(String membername : fkd.getReferringMemberNames()) {
			Column col = getColumn(membername);
			if(col == null) {
				String msg = "Member "+membername+" part of foreign key added but that column does not exist.\n"+
						"Happened on table "+name+". FK="+fkd;
				errorsForgiven.add(msg);
				continue;
			}
			col.setForeignKey();
		}
		foreignKeyDeclarations.addElement(fkd);
		fkd.setReferringTablename(name);
	}
	
	public boolean hasErrors() {
		return errorsForgiven.size() != 0;
	}
	
	public int getNoErrors() {
		return errorsForgiven.size();
	}
	
	public String getError(int idx) {
		return errorsForgiven.elementAt(idx);
	}
	
	public int getNoForeignKeyDeclarations() {
		return foreignKeyDeclarations.size();
	}
	
	public int getNoKeyDeclarations() {
		return keyDeclarations.size();
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclaration(int idx) {
		if(idx < 0) return null;
		if(idx >= foreignKeyDeclarations.size()) return null;
		return foreignKeyDeclarations.elementAt(idx);
	}

	public ForeignKeyDeclaration getForeignKeyDeclaration(String name) {
		for(int i = 0 ; i < foreignKeyDeclarations.size() ; i++) {
			ForeignKeyDeclaration fkd = foreignKeyDeclarations.elementAt(i);
			if(fkd.getConstraintname().compareTo(name) == 0) {
				return fkd;
			}
		}
		return null;
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclarationIgnoreCase(String name) {
		for(int i = 0 ; i < foreignKeyDeclarations.size() ; i++) {
			ForeignKeyDeclaration fkd = foreignKeyDeclarations.elementAt(i);
			if(fkd.getConstraintname().toLowerCase().compareTo(name.toLowerCase()) == 0) {
				return fkd;
			}
		}
		return null;
	}
	
	public void addParameter(Parameter p) {
		if(parameters.contains(p)) {
			return;
		}
		parameters.addElement(p);
	}
	
	public int getNoParameters() {
		return parameters.size();
	}
	
	public Parameter getParameter(int idx) {
		if(idx < 0) return null;
		if(idx >= parameters.size()) return null;
		return parameters.elementAt(idx);
	}

}
