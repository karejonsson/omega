package org.omega.configuration.struct;

public class TextType extends TextualType {
	
	public TextType() {
	}
	
	public String getTypename() {
		return "text";
	}

	/*
	public GraphicalEditor getGraphicalComponent(String name) {
		return new TextAreaGraphicalEditor(name, 2000);
	}
	*/
	
	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}
	
	public void setForeignKey() throws Exception {
		throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}
