package org.omega.configuration.struct;

public class BigintType extends IntType {
	
	public BigintType() {
		super();
	}

	public BigintType(int i) {
		super(i);
	}
	
	public String getTypename() {
		if(hasPrecision()) {
			return "bigint("+getPrecision()+")";
		}
		return "bigint";
	}

}

