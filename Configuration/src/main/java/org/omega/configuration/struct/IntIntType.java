package org.omega.configuration.struct;

public class IntIntType extends IntType {
	
	public IntIntType() {
		super();
	}

	public IntIntType(int i) {
		super(i);
	}

	public String getTypename() {
		if(hasPrecision()) {
			return "int("+getPrecision()+")";
		}
		return "int";
	}

}
