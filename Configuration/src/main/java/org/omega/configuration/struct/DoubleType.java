package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

public class DoubleType extends NumericType {
	
	private int decimals = -1;
	private Double def = 0.0;

	public DoubleType() {
	}
	
	public DoubleType(int precision, int j) {
		super(precision);
		decimals = j;
	}
	
	public int getDecimals() {
		return decimals;
	}
	
	public void setDecimals(int _decimals) {
		decimals = _decimals;
	}
	
	public void setDefault(int i) {
		hasDefault = true;
		def = new Double(i);
	}
	
	public void setDefault(double d) {
		hasDefault = true;
		def = d;
	}
	
	public double getDefault() {
		return def;
	}
	
	public String getDefaultAsString() {
		return ""+def;
	}
	
	public Object getUntypedDefault(Recreator rec) {
		return def;
	}

	public void setUntypedDefault(Recreator rec, Object obj) throws Exception {
		hasDefault = true;
		if(obj == null) {
			def = null;
			return;
		}
		if(obj instanceof String) {
			String sobj = (String) obj;
			if(sobj.toUpperCase().compareTo("NULL") == 0) {
				setNullDefault();
				return;
			}
			try {
				def = Double.parseDouble(obj.toString());
			}
			catch(Exception e) {
				throw new Exception("Double type recieved default value "+obj+" as string.");
			}
			return;
		}
		if(obj instanceof Integer) {
			def = new Double((Integer) obj);
			return;
		}
		if(obj instanceof Double) {
			def = (Double) obj;
			return;
		}
		throw new Exception("Double type recieved default value "+obj);
	}

	public String getTypename() {
		if(hasPrecision()) {
			return "double("+getPrecision()+", "+getDecimals()+")";
		}
		return "double";
	}
	
	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public void setForeignKey() throws Exception {
		throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}
