package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

public class UnmanageableType extends Type {
	
	private String name;
	private Object def = null;
	
	public UnmanageableType(String _name) {
		name = _name;
	}

	public String toString() {
		return super.toString()+" type is unmanageable and named "+name+" ";
	}

	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public String getTypename() {
		return name;
	}

	public Object getUntypedDefault(Recreator rec) {
		return def;
	}
	
	public void setUntypedDefault(Recreator rec, Object obj) {
		def = obj;
	}

	public boolean hasDefault() {
		return def != null;
	}
	
	public String getDefault() {
		return def.toString();
	}
	
	final public String getDefaultAsString() {
		return getDefault();
	}

	public void setDefault(String s) {
	}
	
}
