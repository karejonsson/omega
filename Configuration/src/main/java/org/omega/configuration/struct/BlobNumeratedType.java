package org.omega.configuration.struct;

public class BlobNumeratedType extends BlobType {
	
	public BlobNumeratedType(String image, int numeration) {
		super(image);
	}
	
	public int getNumeration() {
		return 0;
	}
	
}
