package org.omega.configuration.struct;

public class SmallintType extends IntType {
	
	public SmallintType() {
		super();
	}

	public SmallintType(int i) {
		super(i);
	}

	public String getTypename() {
		if(hasPrecision()) {
			return "smallint("+getPrecision()+")";
		}
		return "smallint";
	}

}