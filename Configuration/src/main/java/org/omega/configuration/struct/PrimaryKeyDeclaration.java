package org.omega.configuration.struct;

import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;
import java.util.Vector;

public class PrimaryKeyDeclaration {
	
	private Vector<String> members = new Vector<String>();
	private boolean usingBtree = false;
	private String tablename = null;
	private String keyName = null;
	
	public PrimaryKeyDeclaration(String _tablename) {
		tablename = _tablename;
	}
	
	public String getKeyName() {
		return keyName;
	}
	
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getTablename() {
		return tablename;
	}
	
	public void addMember(String name) {
		members.addElement(name);
	}
	
	public int getNoMembers() {
		return members.size();
	}
	
	public String getMember(int idx) {
		if(idx < 0) return null;
		if(idx >= members.size()) return null;
		return members.elementAt(idx);
	}
	
	public void setUsingBtree() {
		usingBtree = true;
	}
	
	public boolean isUsingBtree() {
		return usingBtree;
	}
	
	public boolean hasMember(String membername) {
		for(int i = 0 ; i < members.size(); i++) {
			if(members.elementAt(i).toUpperCase().compareTo(membername.toUpperCase()) == 0) {
				return true;
			}
		}
		return false;
	}
	
	public int getPlaceOfMember(String membername) {
		for(int i = 0 ; i < members.size(); i++) {
			String aMember = members.elementAt(i);
			if(membername.toUpperCase().compareTo(aMember.toUpperCase()) == 0) {
				return i;
			}
		}
		return -1;
	}
	

	
	public Set<String> getNames() {
		Set<String> out = new HashSet<String>();
		for(int i = 0 ; i < members.size(); i++) {
			out.add(members.elementAt(i));
		}
		return out;//members.listIterator();
	}
	
	public String[] getColumnNames() {
		String out[] = new String[members.size()];
		for(int i = 0 ; i < members.size(); i++) {
			out[i] = members.elementAt(i);
		}
		return out;//members.listIterator();
	}
	
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		if(!(obj instanceof PrimaryKeyDeclaration)) {
			return false;
		}
		PrimaryKeyDeclaration pkd = (PrimaryKeyDeclaration) obj;
		String otherstablename = pkd.getTablename();
		String owntablename = getTablename();
		if(otherstablename == null && owntablename != null) {
			return false;
		}
		if(otherstablename != null && owntablename == null) {
			return false;
		}
		if(otherstablename != null && owntablename != null) {
			if(otherstablename.toUpperCase().compareTo(owntablename.toUpperCase()) != 0) {
				return false;
			}
		}
		if(getNoMembers() != pkd.getNoMembers()) {
			return false;
		}
		//System.out.println("PrimaryKeyDeclaration.equals (1)");
		for(int i = 0 ; i < pkd.getNoMembers() ; i++) { // String name : pk.getNames()) {
			String othersmembername = pkd.getMember(i);
			String ownmembername = getMember(i);
			if(ownmembername == null && othersmembername == null) {
				continue;
			}
			if(ownmembername != null && othersmembername == null) {
				return false;
			}
			if(ownmembername == null && othersmembername != null) {
				return false;
			}
			if(othersmembername.toUpperCase().compareTo(ownmembername.toUpperCase()) != 0) {
				return false;
			}
		}
		//System.out.println("PrimaryKeyDeclaration.equals OK");
		return true;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("PrimaryKeyDeclaration "+getTablename()+"("+getMember(0));
		for(int i = 1 ; i < getNoMembers() ; i++) {
			sb.append(", "+getMember(i));
		}
		return sb.toString()+")";
	}
	
	public boolean hasMembers(String[] memberset) {
		for(int i = 0 ; i < memberset.length ; i++) {
			if(!members.contains(memberset[i])) {
				return false;
			}
		}
		return true;
	}


}
