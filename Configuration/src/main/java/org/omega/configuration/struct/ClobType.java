package org.omega.configuration.struct;

public class ClobType extends LobType {

	private String image = null;
	
	public ClobType(String image) {
		this.image = image;
	}
	
	public String getTypename() {
		return image;
	}

}
