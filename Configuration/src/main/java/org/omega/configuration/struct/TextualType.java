package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

public abstract class TextualType extends Type {

	private String def = null;
	private boolean hasDefault = false;

	public String toString() {
		return super.toString()+(def != null ? " default="+def : "No default")+" ";
	}

	final public void setDefault(String d) {
		hasDefault = true;
		def = d;
	}

	final public boolean hasDefault() {
		return hasDefault;
	}
	
	final public String getDefault() {
		return def;
	}

	final public String getDefaultAsString() {
		return def;
	}

	public Object getUntypedDefault(Recreator rec) {
		return getDefault();
	}

	public void setUntypedDefault(Recreator rec, Object obj) throws Exception {
		System.out.println("TextualType.setUntypedDefault("+rec.getDBsName()+", "+obj+")");
		hasDefault = true;
		if(obj == null) {
			setNullDefault();
			def = null;
			return;
		}
		if(obj instanceof String) {
			String s = (String) obj;
			if(s.toLowerCase().compareTo("null") == 0) {
				setNullDefault();
				def = null;
				return;
			}
		}
		def = obj.toString();
	}
	
	public abstract String getTypename();

}
