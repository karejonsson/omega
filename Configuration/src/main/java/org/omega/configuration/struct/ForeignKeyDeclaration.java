package org.omega.configuration.struct;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.omega.configuration.dbreferences.PrimaryKey;

public class ForeignKeyDeclaration {
	
	private String constraintname;
	private Vector<String> referringMembers = new Vector<String>();
	private String referencedTablename;
	private Vector<String> referredTablemembers = new Vector<String>(); 
	private String onDelete = null;
	private String onUpdate = null;
	private String referringTablename = null;
	private PrimaryKeyDeclaration pkd = null;
	
	//Constraint(name, member, table, tablemember);
	public ForeignKeyDeclaration(String constraintname) {//, String _member, String _table, String _tablemember) {
		this.constraintname = constraintname;
		//member = _member;
		//table = _table;
		//tablemember = _tablemember;
	}
	
	public String getConstraintname() {
		if(constraintname != null) {
			return constraintname;
		}
		if(getNoReferringMembers() == 1) {
			constraintname = referringTablename+"_"+getReferringMember(0)+"_con";
			return constraintname;
		}
		StringBuffer sb = new StringBuffer();
		sb.append(getReferringMember(0));
		for(int i = 1 ; i < getNoReferringMembers() ; i++) {
			sb.append("_"+getReferringMember(i));
		}
		constraintname = referringTablename+"_"+sb.toString()+"_con";
		return constraintname;
	}

	public int getNoReferringMembers() {
		return referringMembers.size();
	}
	
	public String[] getAllReferringMembers() {
		String[] out = new String[referringMembers.size()];
		for(int i = 0 ; i < out.length ; i++) {
			out[i] = referringMembers.elementAt(i);
		}
		return out;
	}
	
	public String getReferringMember(int idx) {
		return referringMembers.elementAt(idx);
	}

	public void addReferringMember(String _member) {
		referringMembers.addElement(_member);
	}
	
	public String getReferencedTablename() {
		return referencedTablename;
	}
	
	public void setReferencedTablename(String table) {
		this.referencedTablename = table;
	}
	
	public int getNoReferredTableMembers() {
		return referredTablemembers.size();
	}
	
	public String[] getAllReferredTableMembers() {
		String[] out = new String[referredTablemembers.size()];
		for(int i = 0 ; i < out.length ; i++) {
			out[i] = referredTablemembers.elementAt(i);
		}
		return out;
	}
	
	public String getReferredTableMember(int idx) {
		return referredTablemembers.elementAt(idx);
	}
	
	public void addReferredTableMember(String member) {
		referredTablemembers.addElement(member);
	}
	
	public Set<String> getReferringMemberNames() {
		Set<String> out = new HashSet<String>();
		for(String aMember : referringMembers) {
			out.add(aMember);
		}
		return out;//members.listIterator();
	}
	
	public int getPlaceOfReferringMember(String membername) {
		for(int i = 0 ; i < referringMembers.size(); i++) {
			String aMember = referringMembers.elementAt(i);
			if(membername.toUpperCase().compareTo(aMember.toUpperCase()) == 0) {
				return i;
			}
		}
		return -1;
	}
	
	public PrimaryKeyDeclaration getPrimaryKeyDeclaration() {
		if(pkd != null) {
			return pkd;
		}
		PrimaryKeyDeclaration out = new PrimaryKeyDeclaration(referencedTablename);
		for(int i = 0 ; i < getNoReferredTableMembers() ; i++) {
			out.addMember(getReferredTableMember(i));
		}
		pkd = out;
		return out;
	}

	public boolean hasReferringMember(String membername) {
		for(String aMember : referringMembers) {
			if(membername.toUpperCase().compareTo(aMember.toUpperCase()) == 0) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasReferredMember(String membername) {
		for(String aMember : referredTablemembers) {
			if(membername.toUpperCase().compareTo(aMember.toUpperCase()) == 0) {
				return true;
			}
		}
		return false;
	}
	
	public void setOnDelete(String action) {
		onDelete = action;
	}
	
	public String getOnDelete() {
		return onDelete;
	}

	public void setOnUpdate(String action) {
		onUpdate = action;
	} 
	
	public String getOnUpdate() {
		return onUpdate;
	} 
	
	public void setReferringTablename(String referringTablename) {
		this.referringTablename = referringTablename;
	}
	
	public String getReferringTablename() {
		return referringTablename;
	}
	
	public boolean equals(Object obj) {
		//System.out.println("ForeignKeyDeclaration.equals (enter)");
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			//System.out.println("ForeignKeyDeclaration.equals samma -> true");
			return true;
		}
		if(!(obj instanceof ForeignKeyDeclaration)) {
			return false;
		}
		ForeignKeyDeclaration fkd = (ForeignKeyDeclaration) obj;
		String otherstablename = fkd.getConstraintname();
		String owntablename = getConstraintname();
		if(otherstablename == null && owntablename != null) {
			//System.out.println("ForeignKeyDeclaration.equals 1 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && owntablename == null) {
			//System.out.println("ForeignKeyDeclaration.equals 2 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && owntablename != null) {
			if(otherstablename.toUpperCase().compareTo(owntablename.toUpperCase()) != 0) {
				//System.out.println("ForeignKeyDeclaration.equals 3 false. "+otherstablename+"!="+owntablename);
				return false;
			}
		}
		otherstablename = fkd.getReferringTablename();
		owntablename = getReferringTablename();
		if(otherstablename == null && owntablename != null) {
			//System.out.println("ForeignKeyDeclaration.equals 4 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && owntablename == null) {
			//System.out.println("ForeignKeyDeclaration.equals 5 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && owntablename != null) {
			if(otherstablename.toUpperCase().compareTo(owntablename.toUpperCase()) != 0) {
				//System.out.println("ForeignKeyDeclaration.equals 6 false. "+otherstablename+"!="+owntablename);
				return false;
			}
		}
		if(getNoReferringMembers() != fkd.getNoReferringMembers()) {
			return false;
		}
		//System.out.println("KeyDeclaration.equals (1)");
		for(int i = 0 ; i < fkd.getNoReferringMembers() ; i++) { // String name : pk.getNames()) {
			String othersmembername = fkd.getReferringMember(i);
			String ownmembername = getReferringMember(i);
			if(ownmembername == null && othersmembername == null) {
				continue;
			}
			if(ownmembername != null && othersmembername == null) {
				return false;
			}
			if(ownmembername == null && othersmembername != null) {
				return false;
			}
			if(ownmembername.toUpperCase().compareTo(othersmembername.toUpperCase()) != 0) {
				return false;
			}
			String otherstablemembername = fkd.getReferredTableMember(i);
			String owntablemembername = getReferredTableMember(i);
			if(owntablemembername == null && otherstablemembername == null) {
				continue;
			}
			if(owntablemembername != null && otherstablemembername == null) {
				return false;
			}
			if(owntablemembername == null && otherstablemembername != null) {
				return false;
			}
			if(owntablemembername.toUpperCase().compareTo(otherstablemembername.toUpperCase()) != 0) {
				return false;
			}
		}
		//System.out.println("KeyDeclaration.equals OK");
		return true;
	}
	
	public boolean compliant(PrimaryKeyDeclaration pkd) {
		for(int i = 0 ; i < pkd.getNoMembers() ; i++) {
			if(!hasReferredMember(pkd.getMember(i))) {
				return false;
			}
		}
		return true;
	}

	public boolean compliant(PrimaryKey pk) {
		return compliant(pk.getPrimaryKeyDeclaration());
	}
	
	public String getMemberList() {
		StringBuffer mem = new StringBuffer();
		mem.append(getReferringMember(0));
		for(int i = 1 ; i < getNoReferringMembers() ; i++) {
			mem.append(", "+getReferringMember(i));
		}
		return mem.toString();
	}
	
	public String getTableMemberList() {
		StringBuffer mem = new StringBuffer();
		mem.append(getReferredTableMember(0));
		for(int i = 1 ; i < getNoReferredTableMembers() ; i++) {
			mem.append(", "+getReferredTableMember(i));
		}
		return mem.toString();
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("Foreign key declaration "+getConstraintname()+" "+getReferringTablename()+":("+getReferringMember(0));
			for(int i = 1 ; i < getNoReferringMembers() ; i++) {
				sb.append(", "+getReferringMember(i));
			}
			sb.append(") -> "+getReferencedTablename()+".("+getReferredTableMember(0));
			for(int i = 1 ; i < getNoReferredTableMembers() ; i++) {
				sb.append(", "+getReferredTableMember(i));
			}
			sb.append(")");
			if(getNoReferringMembers() != getNoReferredTableMembers()) {
				sb.append(" Field number mismatch!");
			}
		}
		catch(Exception e) {
			return "toString exception "+e+" after assembling "+sb.toString();
		}
		return sb.toString();
	}
	
	public boolean hasMemberMatch(KeyDeclaration kd) {
		if(kd.getNoMembers() != getNoReferringMembers()) {
			return false;
		}
		for(int i = 0 ; i < getNoReferringMembers() ; i++) {
			String name = kd.getMember(i);
			if(!hasReferredMember(name)) {
				return false;
			}
		}
		return true;
	}
	
}
