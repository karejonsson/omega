package org.omega.configuration.struct;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class KeyDeclaration {
	
	private String keyname;
	private Vector<String> members = new Vector<String>();
	private boolean usingBtree = false;
	private boolean unique = false;
	private String tablename = null;
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append("Name "+keyname);
		sb.append(", Btree "+usingBtree);
		sb.append(", Unique "+unique);
		sb.append(", Table "+tablename);
		sb.append(", members ["+members.elementAt(0));
		for(int i = 1 ; i < members.size() ; i++) {
			sb.append(", "+members.elementAt(i));
		}
		sb.append("]");
		sb.append("}");
		return sb.toString();
	}

	public void setKeyName(String _keyname) {
		keyname = _keyname;
	}
	
	public String getKeyName() {
		return keyname;
	}
	
	public void addMember(String name) {
		members.addElement(name);
	}
	
	public boolean hasMember(String name) {
		return members.contains(name);
	}
	
	public int getNoMembers() {
		return members.size();
	}
	
	public String getMember(int idx) {
		if(idx < 0) return null;
		if(idx >= members.size()) return null;
		return members.elementAt(idx);
	}
    
	public void setUsingBtree() {
		usingBtree = true;
	}

	public boolean isUsingBtree() {
		return usingBtree;
	}
	
	public void setUnique() {
		unique = true;
	}

	public boolean isUnique() {
		return unique;
	}
	
	public void setTablename(String name) {
		tablename = name;
	}
	
	public String getTablename() {
		return tablename;
	}
	
	public Set<String> getNames() {
		Set<String> out = new HashSet<String>();
		for(int i = 0 ; i < members.size(); i++) {
			out.add(members.elementAt(i));
		}
		return out;//members.listIterator();
	}
	
	public boolean equals(Object obj) {
		//System.out.println("ForeignKeyDeclaration.equals (enter)");
		if(obj == null) {
			return false;
		}
		if(obj == this) {
			//System.out.println("ForeignKeyDeclaration.equals samma -> true");
			return true;
		}
		if(!(obj instanceof KeyDeclaration)) {
			return false;
		}
		KeyDeclaration kd = (KeyDeclaration) obj;
		String otherstablename = kd.getTablename();
		if(otherstablename == null && tablename != null) {
			//System.out.println("ForeignKeyDeclaration.equals 1 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && tablename == null) {
			//System.out.println("ForeignKeyDeclaration.equals 2 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && tablename != null) {
			if(otherstablename.toUpperCase().compareTo(tablename.toUpperCase()) != 0) {
				//System.out.println("ForeignKeyDeclaration.equals 3 false. "+otherstablename+"!="+owntablename);
				return false;
			}
		}
		if(otherstablename == null && tablename != null) {
			//System.out.println("ForeignKeyDeclaration.equals 4 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && tablename == null) {
			//System.out.println("ForeignKeyDeclaration.equals 5 false. "+otherstablename+"!="+owntablename);
			return false;
		}
		if(otherstablename != null && tablename != null) {
			if(otherstablename.toUpperCase().compareTo(tablename.toUpperCase()) != 0) {
				//System.out.println("ForeignKeyDeclaration.equals 6 false. "+otherstablename+"!="+owntablename);
				return false;
			}
		}
		if(getNoMembers() != kd.getNoMembers()) {
			return false;
		}
		//System.out.println("ForeignKeyDeclaration.equals (1)");
		for(int i = 0 ; i < kd.getNoMembers() ; i++) { // String name : pk.getNames()) {
			String othersmembername = kd.getMember(i);
			String ownmembername = getMember(i);
			if(ownmembername == null && othersmembername == null) {
				continue;
			}
			if(ownmembername != null && othersmembername == null) {
				return false;
			}
			if(ownmembername == null && othersmembername != null) {
				return false;
			}
			if(ownmembername.toUpperCase().compareTo(othersmembername.toUpperCase()) != 0) {
				return false;
			}
		}
		//System.out.println("ForeignKeyDeclaration.equals OK");
		return true;
	}
	
	public boolean hasMembers(String[] memberset) {
		for(int i = 0 ; i < memberset.length ; i++) {
			if(!members.contains(memberset[i])) {
				return false;
			}
		}
		return true;
	}
	
}
