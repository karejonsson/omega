package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

public class DecimalType extends NumericType {
	
	private Integer decimals = null;
	private Double def = -0.0;

	public DecimalType() {
	}
	
	public DecimalType(int precision, Integer j) {
		super(precision);
		decimals = j;
	} 
	
	public Integer getDecimals() {
		return decimals;
	}
	
	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}
	
	public void setDefault(int i) {
		hasDefault = true;
		def = new Double(i);
	}
	
	public void setDefault(double d) {
		hasDefault = true;
		def = d;
	}
	
	public double getDefault() {
		return def;
	}
	
	public String getDefaultAsString() {
		return ""+def;
	}
	
	public Object getUntypedDefault(Recreator rec) {
		return def;
	}
	
	public void setUntypedDefault(Recreator rec, Object obj) throws Exception {
		if(obj == null) {
			def = null;
			return;
		}
		if(obj instanceof String) {
			try {
				def = Double.parseDouble(obj.toString());
			}
			catch(Exception e) {
				throw new Exception("Decimal type recieved default value "+obj+" as string.");
			}
			return;
		}
		if(obj instanceof Integer) {
			def = new Double((Integer) obj);
			return;
		}
		if(obj instanceof Double) {
			def = (Double) obj;
			return;
		}
		throw new Exception("Decimal type recieved default value "+obj);
	}

	public String getTypename() {
		if(hasPrecision()) {
			Integer decimals = getDecimals();
			if(decimals != null) {
				return "decimal("+getPrecision()+", "+decimals+")";
			}
			return "decimal("+getPrecision()+")";
		}
		return "decimal";
	}
	
	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public void setForeignKey() throws Exception {
		throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}