package org.omega.configuration.struct;

public class TinyintType extends IntType {
	
	public TinyintType() {
		super();
	}

	public TinyintType(int i) {
		super(i);
	}

	public String getTypename() {
		if(hasPrecision()) {
			return "tinyint("+getPrecision()+")";
		}
		return "tinyint";
	}

}