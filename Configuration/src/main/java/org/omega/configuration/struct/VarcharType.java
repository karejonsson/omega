package org.omega.configuration.struct;

public class VarcharType extends TextualType {
	
	private int len;
	
	public VarcharType(int i) {
		len = i;
	}
	
	public int getLength() {
		return len;
	}
	
	public String getTypename() { 
		return "varchar("+len+")";
	}

	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public void setForeignKey() throws Exception {
		//throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}
