package org.omega.configuration.struct;

import org.apache.bsf.BSFException;
import org.apache.bsf.BSFManager;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;

public class ProgramBlock {
	
	private String sourceSpecification = null;
	private String groovyScript = null;
	private int beginLine;
	private int beginColumn;
	private int endLine;
	private int endColumn;

	public ProgramBlock(String sourceSpecification, String groovyScript, int beginLine, int beginColumn, int endLine, int endColumn) {
		this.sourceSpecification = sourceSpecification;
		this.groovyScript = groovyScript;
		this.beginLine = beginLine;
		this.beginColumn = beginColumn;
		this.endLine = endLine;
		this.endColumn = endColumn;
	}
	
	public String getScript() {
		return groovyScript;
	}

	public int getBeginLine() {
		return beginLine;
	}
	
	public int getBeginColumn() {
		return beginColumn;
	}

	public int getEndLine() {
		return endLine;
	}
	
	public int getEndColumn() {
		return endColumn;
	}

	public String getScriptFromDescription() {
		return 
				"from "+sourceSpecification+"\n"+
				"Line "+beginLine+", column "+beginColumn+" to line "+endLine+", column "+endColumn;
	}

	/*
	public void execute(Object node, Object sys) throws IntolerableException {
		try {
			BSFManager manager = new BSFManager();
			manager.declareBean("node", node, Object.class);
			manager.declareBean("sys", sys, Object.class);
			manager.eval("groovy", "script.groovy", 0, 0, groovyScript);			
		}
		catch(BSFException e) {
			throw new UserRuntimeError("Exekveringsproblem", e);
		}
	}
	*/
}
