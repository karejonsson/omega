package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

public class DatetimeType extends Type {

	protected static final String Calendar = null;
	private String def;

	public DatetimeType() {
	}
	
	public String toString() {
		return super.toString()+" default="+def+" ";
	}
	
	public void setDefault(String d) {
		def = d;
	}

	public boolean hasDefault() {
		return def != null;
	}
	
	public String getDefault() {
		return def;
	}
	
	public String getDefaultAsString() {
		return getDefault();
	}
	
	public String getTypename() {
		return "datetime";
	}
	
	public Object getUntypedDefault(Recreator rec) {
		try {
			if(getNotNull()) {
				return rec.formDate("");
			}
			return rec.formDate(def);
		}
		catch(Exception e) {
			return "NULL";
		}
	}
	
	public void setUntypedDefault(Recreator rec, Object obj) throws Exception {
		String datemacros[] = rec.getCurrentDateMacros();
		String sobj = obj.toString();
		for(int i = 0 ; i < datemacros.length ; i++) {
			String actual = datemacros[i];
			actual = actual.replace('(', ' ');
			actual = actual.replace(')', ' ');
			actual = actual.replaceAll(" ", "");
			System.out.println("DatetimeType.setUntypedDefault - "+sobj+" contAins "+actual);
			if(sobj.toLowerCase().contains(actual.toLowerCase())) {
				def = datemacros[i];
				return;
			}
			System.out.println("DatetimeType.setUntypedDefault - "+sobj+" contains "+actual+" -> Nej");
		}
		def = obj != null ? rec.formDate(sobj) : null;
	}

	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public void setForeignKey() throws Exception {
		throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}
