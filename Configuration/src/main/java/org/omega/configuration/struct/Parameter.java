package org.omega.configuration.struct;

public class Parameter {
	
	private String name;
	private String value;
	
	public Parameter(String _name , String _value) {
		name = _name;
		value = _value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}

}
