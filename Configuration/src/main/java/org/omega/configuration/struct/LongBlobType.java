package org.omega.configuration.struct;

public class LongBlobType extends LobType {

	private int length = -1;
	private String image = null;
	
	
	public LongBlobType() {
	}
	
	public LongBlobType(String image) {
		this.image = image;
	}
	
	public LongBlobType(String image, int length) {
		this.image = image;
		this.length = length;
	}
	
	public String getTypename() {
		if(image == null) {
			return "longblob";
		}
		if(length == -1) {
			return image;
		}
		return "longblob("+length+")";
	}
	
	public boolean hasImage() {
		return image != null;
	}
	
	public String getImage() {
		return image;
	}
	
	public boolean hasLength() {
		return length != -1;
	}
	
	public int getLength() {
		return length;
	}

}
