package org.omega.configuration.struct;

public class ClobNumeratedType extends ClobType {
	
	private int numeration;

	public ClobNumeratedType(String image, int numeration) {
		super(image);
		this.numeration = numeration;
	}

	public int getNumeration() {
		return numeration;
	}

}
