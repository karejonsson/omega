package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;

public abstract class LobType extends Type {
	
	private Object defaultValue = null;

	public boolean hasDefault() {
		return false;
	}

	public String getDefaultAsString() {
		return null;
	}

	public Object getUntypedDefault(Recreator rec) {
		return defaultValue;
	}

	public void setUntypedDefault(Recreator rec, Object obj) throws IntolerableException {
		if(obj == null) {
			return;
		}
		throw new UserRuntimeError("Cannot set default value on Lob type");
	}

	public void accept(TypeVisitor tv) throws IntolerableException {
		tv.visit(this);
	}

}
