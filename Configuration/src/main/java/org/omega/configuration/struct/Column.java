package org.omega.configuration.struct;

public class Column {
	
	private String name;
	private Type type;
	private boolean autoIncrement = false;
	private String comment;
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Name "+name);
		sb.append(" Type "+type);
		sb.append(" Auto increment "+autoIncrement);
		if(comment != null) {
			sb.append(" Comment "+comment);
		}
		else {
			sb.append(" No comment");
		}
		return "("+sb.toString()+")";
	}

	final public void setComment(String _comment) {
		comment = _comment;
	}

	final public String getComment() {
		return comment;
	}
	
	final public boolean hasComment() {
		return comment != null;
	}

	public void setName(String _name) {
		name = _name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setType(Type _type) {
		type = _type;
	}
	
	public Type getType() {
		return type;
	}
	
    public void setAutoIncrement() {
    	autoIncrement = true;
    }
    
    public boolean isAutoIncrement() {
    	return autoIncrement;
    }
    
    public void setForeignKey() throws Exception {
    	type.setForeignKey();
    }
    
}
