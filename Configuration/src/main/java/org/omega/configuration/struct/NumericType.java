package org.omega.configuration.struct;

public abstract class NumericType extends Type {

	private int precision = -1;
	private boolean hasPrecision = false;
	private boolean unsigned = false;
	protected boolean hasDefault = false;
	private boolean zerofill = false;
	
	public NumericType() {
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		if(hasPrecision) {
			sb.append("Precision "+precision);
		}
		else {
			sb.append("No precision");
		}
		sb.append(" unsigned "+unsigned);
		sb.append(" has default "+hasDefault);
		sb.append(" zerofill "+zerofill);
		return super.toString()+" "+sb.toString()+" ";
	}
	
	public abstract String getTypename();
	
	public NumericType(int _precision) {
		precision = _precision;
		hasPrecision = true;
	}

	final public boolean hasPrecision() {
		return hasPrecision;
	}
		
	final public int getPrecision() {
		return precision;
	}
		
	final public void setPrecision(int _precision) {
		precision = _precision;
		hasPrecision = true;
	}
		
	final public void setUnsigned() {
		unsigned = true;
	}
	
	final public boolean isUnsigned() {
		return unsigned;
	}

	final public void setNullDefault() {
		hasDefault = true;
		super.setNullDefault();
	}
	
	final public boolean hasDefault() {
		return hasDefault;
	}
	
	final public void setHasDefault(boolean value) {
		hasDefault = value;
	}
	
	final public void setZerofill() {
		zerofill = true;
	}

	final public boolean getZerofill() {
		return zerofill;
	}

}
