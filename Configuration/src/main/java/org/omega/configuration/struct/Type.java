package org.omega.configuration.struct;

import org.omega.configuration.recreatesql.Recreator;

import se.modlab.generics.exceptions.IntolerableException;

public abstract class Type {
	
	private boolean notNull = false;
	private boolean hasNullDefault = false;
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Null "+(!notNull));
		if(hasNullDefault) {
			sb.append(" Has null default");
		}
		else {
			sb.append(" Does not have null default");
		}
		return "("+sb.toString()+")";
	}
	
	final public void setNotNull() {
		setNotNull(true);
	}

	final public void setNotNull(boolean value) {
		hasNullDefault = false;
		notNull = value;
	}

	final public boolean getNotNull() {
		return notNull;
	}
	
	final public boolean hasNullDefault() {
		return hasNullDefault;
	}
	
	public void setNullDefault() {
		setNullDefault(true);
	}
	
	public void setNullDefault(boolean val) {
		hasNullDefault = val;
	}
	
	public abstract boolean hasDefault();
	public abstract String getDefaultAsString();

	public abstract Object getUntypedDefault(Recreator rec);
	
	public abstract void setUntypedDefault(Recreator rec, Object obj) throws Exception;
	
	public abstract String getTypename();
	
	//abstract public Object _getGraphicalComponent(String name);
	
	public abstract void accept(TypeVisitor tv) throws IntolerableException;
	
	private boolean foreignKey = false;
    public void setForeignKey() throws Exception {
    	foreignKey = true;
    }
    public boolean getForeignKey() {
    	return foreignKey;
    };


}
