package org.omega.configuration.struct;

public class DBMany2ManyRelation {
	
	private String tablename = null;
	private String t1;
	private String t2;

	public DBMany2ManyRelation(String _t1, String _t2) {
		t1 = _t1;
		t2 = _t2;
		tablename = t1+"_"+t2;
	}
	
	public static DBMany2ManyRelation createReciproque(DBMany2ManyRelation m2mr) {
		DBMany2ManyRelation out = new DBMany2ManyRelation(m2mr.t2, m2mr.t1);
		out.tablename = m2mr.tablename;
		return out;
	}
	
	public String getT1() {
		return t1;
	}
	
	public String getT2() {
		return t2;
	}
	
	public String getRefT1() {
		return t1+"_id";
	}
	
	public String getRefT2() {
		return t2+"_id";
	}
	
	public String getTablename() {
		return tablename;
	}
	
}
