package org.omega.configuration.struct;

public class TinytextType extends TextualType {
	
	public TinytextType() {
	}

	public String getTypename() {
		return "tinytext";
	}
	
	/*
	public GraphicalEditor getGraphicalComponent(String name) {
		return new TextLineGraphicalEditor(name, 30);
	}
	*/
	
	public void accept(TypeVisitor tv) {
		tv.visit(this);
	}

	public void setForeignKey() throws Exception {
		throw new Exception("Cannot set foreign key on "+getTypename()+" field");
	}

}
