package org.omega.configuration.struct;

public class MediumBlobType extends LobType {

	private int length = -1;
	private String image = null;
	
	
	public MediumBlobType() {
	}
	
	public MediumBlobType(String image) {
		this.image = image;
	}
	
	public MediumBlobType(String image, int length) {
		this.image = image;
		this.length = length;
	}
	
	public String getTypename() {
		if(image == null) {
			return "mediumblob";
		}
		if(length == -1) {
			return image;
		}
		return "mediumblob("+length+")";
	}
	
	public boolean hasImage() {
		return image != null;
	}
	
	public String getImage() {
		return image;
	}
	
	public boolean hasLength() {
		return length != -1;
	}
	
	public int getLength() {
		return length;
	}

}
