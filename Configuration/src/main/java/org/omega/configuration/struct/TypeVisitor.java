package org.omega.configuration.struct;

import se.modlab.generics.exceptions.IntolerableException;

public interface TypeVisitor {
	
	public void visit(DatetimeType dtt);
	public void visit(DecimalType dt);
	public void visit(DoubleType dt);
	public void visit(IntType it);
	public void visit(TextType tt);
	public void visit(TinytextType ttt);
	public void visit(UnmanageableType ut);
	public void visit(VarcharType vct);
	public void visit(LobType lt) throws IntolerableException;
	
}
