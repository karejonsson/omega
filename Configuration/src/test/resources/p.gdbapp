
CREATE TABLE unit (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(35),
   code varchar(15)
) 
 
CREATE TABLE documentcategory (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50)
) 
 
CREATE TABLE country (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50),
   code varchar(5)
) 
 
CREATE TABLE commonsubstratenames (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   substratename varchar(50),
   substratecode varchar(10),
   commentary varchar(2000)
) 
 
CREATE TABLE keyword (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   word varchar(50),
   commentary varchar(2000)
) 
 
CREATE TABLE analysis (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50),
   unitId int default null,
   constraint theunit foreign key (unitId) references unit (id)
) 
 
CREATE TABLE infocard (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(80),
   address varchar(2000),
   contactinfo varchar(2000),
   countryId int default null,
   commentary varchar(2000),
   constraint incountry foreign key (countryId) references country (id)
) 
 
CREATE TABLE companygroup (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50),
   infocardId int default null,
   commentary varchar(2000),
   constraint thegroupinfocard foreign key (infocardId) references infocard (id)
) 
 
CREATE TABLE person (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(45),
   title varchar(120),
   email varchar(100),
   phone1 varchar(45),
   phone2 varchar(45),
   commentary varchar(2000),
   companygroupId int default null,
   constraint employment foreign key (companygroupId) references companygroup (id)
) 
 
CREATE TABLE site (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50),
   code varchar(8),
   companygroupId int default null,
   infocardId int default null,
   contactpersonId int default null,
   commentary varchar(2000),
   constraint thegroup foreign key (companygroupId) references companygroup (id),
   constraint thesiteinfocard foreign key (infocardId) references infocard (id),
   constraint sbfcontactperson foreign key (contactpersonId) references person (id)
) 
 
CREATE TABLE companypersonrelation (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   relationtype varchar(50),
   companygroupId int default null,
   personId int default null,
   commentary varchar(2000),
   constraint cprcomp foreign key (companygroupId) references companygroup (id),
   constraint cprpers foreign key (personId) references person (id)
) 
 
CREATE TABLE sbfuser (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   personId int default null,
   commentary varchar(2000),
   constraint fysicalpers foreign key (personId) references person (id)
) 
 
CREATE TABLE personsiterelation (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   relationtype varchar(50),
   personId int default null,
   siteId int default null,
   commentary varchar(2000),
   constraint psrpers foreign key (personId) references person (id),
   constraint psrsite foreign key (siteId) references site (id)
) 
 
CREATE TABLE substrate (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   code varchar(40),
   originsiteId int default null,
   commonsubstratenamesId int default null,
   commentary varchar(2000),
   constraint originsite foreign key (originsiteId) references site (id),
   constraint substratecommonname foreign key (commonsubstratenamesId) references commonsubstratenames (id)
) 
 
CREATE TABLE signature (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   signatureuserId int default null,
   signaturedate datetime,
   accountantuserId int default null,
   registrationdate datetime,
   constraint theaccountant foreign key (accountantuserId) references sbfuser (id),
   constraint thesigner foreign key (signatureuserId) references sbfuser (id)
) 
 
CREATE TABLE storage (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   name varchar(50),
   siteId int default null,
   commentary varchar(2000),
   constraint location foreign key (siteId) references site (id)
) 
 
CREATE TABLE sampling (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   substrateId int default null,
   samplingdate datetime,
   samplingtime varchar(6),
   amount decimal(10, 3) default null,
   unitId int default null,
   nrofpackages int,
   substrateandsamplingdetails varchar(2000),
   commentary varchar(2000),
   samplerpersonId int default null,
   samplingsiteId int default null,
   sbfcontactprocessId int default null,
   sbfcontactrndId int default null,
   samplingnumber int default '1',
   sbfaccountantId int default null,
   constraint amountsunit foreign key (unitId) references unit (id),
   constraint sbfcontactrnd foreign key (sbfcontactrndId) references sbfuser (id),
   constraint sbfcontactprocess foreign key (sbfcontactprocessId) references sbfuser (id),
   constraint samplerperson foreign key (samplerpersonId) references person (id),
   constraint samplingsbfaccountant foreign key (sbfaccountantId) references sbfuser (id),
   constraint substratesampled foreign key (substrateId) references substrate (id),
   constraint samplingsite foreign key (samplingsiteId) references site (id)
) 
 
CREATE TABLE keywordsubstraterelation (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   keywordId int default null,
   substrateId int default null,
   constraint keywordptr foreign key (keywordId) references keyword (id),
   constraint substrateptr foreign key (substrateId) references substrate (id)
) 
 
CREATE TABLE storageitem (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   samplingId int default null,
   itemnumber int default '1',
   splititemId int default '1',
   code varchar(50),
   originalamount decimal(10, 3) default null,
   currentamount decimal(10, 3) default null,
   unitId int default null,
   storageId int default null,
   sbfaccountantId int default null,
   commentary varchar(2000),
   constraint itemsampling foreign key (samplingId) references sampling (id),
   constraint splititem foreign key (splititemId) references storageitem (id),
   constraint theitemunit foreign key (unitId) references unit (id),
   constraint itemstorage foreign key (storageId) references storage (id),
   constraint sbfaccountant foreign key (sbfaccountantId) references sbfuser (id)
) 
 
CREATE TABLE samplingnotes (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   title varchar(50),
   note varchar(2000),
   notesdate datetime,
   samplingId int default null,
   sbfuserId int default null,
   constraint samplingconcerned foreign key (samplingId) references sampling (id),
   constraint sbfuserbehindsamplingnote foreign key (sbfuserId) references sbfuser (id)
) 
 
CREATE TABLE storageitemnotes (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   title varchar(50),
   note varchar(2000),
   notesdate datetime,
   storageitemId int default null,
   sbfuserId int default null,
   constraint storageitemconcerned foreign key (storageitemId) references storageitem (id),
   constraint sbfuserbehindstorageitemnote foreign key (sbfuserId) references sbfuser (id)
) 
 
CREATE TABLE document (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   storageitemId int default null,
   samplingId int default null,
   siteId int default null,
   documentdate datetime,
   filename varchar(550),
   documentcategoryId int default null,
   commentary varchar(2000),
   documentfile varbinary(max),
   constraint originalstorageitem foreign key (storageitemId) references storageitem (id),
   constraint originalsampling foreign key (samplingId) references sampling (id),
   constraint anarepsite foreign key (siteId) references site (id),
   constraint documentcategoryptr foreign key (documentcategoryId) references documentcategory (id)
) 
 
CREATE TABLE tsvsvalues (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   storageitemId int default null,
   cruciblenumber1 int,
   crucibleempty1 decimal(10, 3),
   cruciblesample1 decimal(10, 3),
   crucibleplusdryweight1 decimal(10, 3),
   crucibleplusash1 decimal(10, 3),
   ts1 decimal(10, 3),
   vs1 decimal(10, 3),
   ashw1 decimal(10, 3),
   cruciblenumber2 int,
   crucibleempty2 decimal(10, 3),
   cruciblesample2 decimal(10, 3),
   crucibleplusdryweight2 decimal(10, 3),
   crucibleplusash2 decimal(10, 3),
   ts2 decimal(10, 3),
   vs2 decimal(10, 3),
   ashw2 decimal(10, 3),
   averagets decimal(10, 3),
   averagevs decimal(10, 3),
   differencets decimal(10, 3),
   differencevs decimal(10, 3),
   signatureId int default null,
   commentary varchar(2000),
   constraint storageitemptr foreign key (storageitemId) references storageitem (id),
   constraint tsvsvalsignature foreign key (signatureId) references signature (id)
) 
 
CREATE TABLE singlepointanalysis (
   id int not null IDENTITY(1,1) PRIMARY KEY,
   documentId int default null,
   result decimal(10, 3),
   analysisId int default null,
   constraint genanarep foreign key (documentId) references document (id),
   constraint analysisptr foreign key (analysisId) references analysis (id)
) 
 
