package org.omega.configparsing;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;
import org.omega.configuration.parse.ParseException;

import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ParseTest {
	
	private ClassLoader cl = null;
	
	@Before
	public void setup() {
		cl = this.getClass().getClassLoader();		
	}

	@After
	public void teardown() {
		cl = this.getClass().getClassLoader();		
	}

	@Test
	public void testSample1File() throws ParseException, Exception {
		InputStream is = cl.getResourceAsStream("sample1.omegaconf");
		OmegaConfigParser parser = new OmegaConfigParser(is);
		ModelConfiguration config = new ModelConfiguration();
		try {
			parser.parseDataModelConfig(config, "Resource file sample1.omegaconf");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		System.out.println("Run compiler");
	}
	
	@Test
	public void testSample2File() throws ParseException, Exception {
		InputStream is = cl.getResourceAsStream("notnull.omegaconf");
		OmegaConfigParser parser = new OmegaConfigParser(is);
		ModelConfiguration config = new ModelConfiguration();
		try {
			parser.parseDataModelConfig(config, "Resource file notnull.omegaconf");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		System.out.println("Run compiler");
	}

	@Test
	public void testSample3File() throws ParseException, Exception {
		InputStream is = cl.getResourceAsStream("defaultvals.omegaconf");
		OmegaConfigParser parser = new OmegaConfigParser(is);
		ModelConfiguration config = new ModelConfiguration();
		try {
			parser.parseDataModelConfig(config, "Resource file defaultvals.omegaconf");
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		System.out.println("Run compiler");
	}

}
