SET CLASSPATH=""
FOR /R ../lib %%a in (*.jar) DO CALL :AddToPath %%a
echo %CLASSPATH%

java -cp %CLASSPATH% StartModlab ..\files

pause

:AddToPath
SET CLASSPATH="%*";%CLASSPATH%
GOTO :EOF
