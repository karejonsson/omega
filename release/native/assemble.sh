rm -rf ../omega
mkdir -p ../omega/lib
cp ../../NativeClientAdmin/target/*.jar ../omega/lib/
cp ../../NativeClientAdmin/target/lib/*.jar ../omega/lib/
mkdir ../omega/bin
cp start.sh ../omega/bin/
cp start.bat ../omega/bin/
mkdir ../omega/files
mkdir ../omega/misc
cp omega.png ../omega/misc/

cp ../../GroovyIntegration/src/test/resources/countrycity-swe.omega ../omega/files/
cp ../../GroovyIntegration/src/test/resources/countrycity-eng.omega ../omega/files/
cp ../../GroovyIntegration/src/test/resources/countrycitypersonparty.omega ../omega/files/

cp ../../NativeClientAdmin/src/test/resources/lob.omega ../omega/files/
cp ../../NativeClientAdmin/src/test/resources/autoupdate.omega ../omega/files/

cp resources/omeganative.properties ../omega/misc/omeganative.properties
cp resources/example-small.oexe ../omega/files/
cp resources/example-large.oexe ../omega/files/

if [ ! -d "../artifacts" ]; then
  mkdir ../artifacts
fi

( cd .. ; tar -zcf artifacts/omeganative.tar.gz omega/ )

