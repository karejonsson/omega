package org.omega.connectivity.connections;

import se.modlab.generics.exceptions.IntolerableException;


public interface DBConnectionSpecificationNamecastVisitorAcceptor {

	public void accept(DBConnectionSpecificationNamecastVisitor visitor) throws IntolerableException;
	
}
