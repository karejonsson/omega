package org.omega.connectivity.connections;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.omega.connectivity.resultset.SQLResult;

import se.modlab.generics.exceptions.IntolerableException;

public interface ConnectionOperations {
	
	// Select statements. With and without where-clauses. 
	public SQLResult select_via_executeQuery(String sql) throws SQLException;
	
	// Binary Large OBject download
	public InputStream downloadFromBlob_with_executeQuery(String sql) throws SQLException;
	
	// Insert rows when there is no primary key. GDBAPP will not be able to handle this.
	public SQLResult insert_via_executeQuery(String sql) throws SQLException;
	
	// Create table statements.
	public boolean create_execute(String sql) throws SQLException;
	
	// Delete statements
	public int delete_via_executeUpdate(String sql) throws SQLException;
	
	// Update statements
	public int update_via_executeUpdate(String sql) throws SQLException;
	
	// Binary Large OBject upload
	public int uploadToBlob_via_executeUpdate(String sql, InputStream is) throws SQLException;
	
	// Insert statements where the generated key value is returned.
	public SQLResult insert_GetGeneratedKeys(String sql, String pkfields[]) throws SQLException;
	
	// Close the connection
	public void close() throws SQLException;
	
	// Says weahter the connection is closed
	public boolean isClosed() throws SQLException;
	
	// Return actual JDBC connection. Used for HSQLDB to be able to launch the built in manager.
	public Connection getConnection();
	
	public String getUser();
	
	public String getDBName();
	
	public boolean valueEquals(ConnectionOperations other) throws IntolerableException;
	
}
