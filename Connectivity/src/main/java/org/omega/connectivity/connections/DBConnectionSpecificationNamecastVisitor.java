package org.omega.connectivity.connections;

import java.util.HashMap;

import se.modlab.generics.exceptions.IntolerableException;

public abstract class DBConnectionSpecificationNamecastVisitor {
	
	public static final String CONFIGFILE = "configfile";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String FILE = "file";
	public static final String CREATE = "create";
	public static final String DBNAME = "dbname";
	public static final String FRAMETITLE = "frametitle";
	public static final String ENCODING = "encoding";
	public static final String ENDPOINT = "endpoint";
	public static final String IPRESOLVABLE = "ipresolvable";
	public static final String PROTOCOL = "protocol";
	public static final String PORT = "port";
	public static final String SENDER = "sender";
	public static final String RECEIVER = "reciever";
	public static final String SYMBOL = "symbol";
	public static final String SSHTUNNELPARAMETERS = "sshtunnelparameters";

	abstract public void visitDBHSQLDBFile() throws IntolerableException;
	abstract public void visitDBHSQLDBMemory() throws IntolerableException;
	abstract public void visitDBHSQLDBRemote() throws IntolerableException;
	abstract public void visitDBMSSQLServer() throws IntolerableException;
	abstract public void visitDBMySQL() throws IntolerableException;
	abstract public void visitDBOracle() throws IntolerableException;
	abstract public void visitDBIBMDB2() throws IntolerableException;
	abstract public void visitDBPOSTGRES() throws IntolerableException;
	
	private HashMap<String, Object> hash = new HashMap<String, Object>();
	
	public void setAttribute(String label, Object obj) {
		hash.put(label, obj);
	}
	
	protected String getAttributeAsString(String key) {
		return hash.get(key).toString();
	}
	
	protected Object getAttribute(String key) {
		return hash.get(key);
	}
	
	protected Object out = null;
	public Object getConnection() {
		return out;
	}

}
