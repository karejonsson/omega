package org.omega.connectivity.properties;

import java.io.File;
import org.omega.generics.properties.CommandLineProperties;
import org.omega.generics.properties.PropertiesAssemble;

public class InstallationProperties {
	
	private static final String test = "/Users/karejonsson/Documents/workspace/omega/tmp/omeganative.properties";
	 
	private static String pathToConfigFile = null;
	private static final String pathToConfigFile_property = "omega.configfile";
	public static String pathToConfigFile_default = "../misc/omeganative.properties";
	
	private static String getPathToConfigFile() throws Exception {
		try {
			if(pathToConfigFile != null) {
				System.out.println("InstallationProperties.getPathToConfigFile Fanns sedan tidigare "+pathToConfigFile);
				return pathToConfigFile;
			}
			String system = System.getProperty(pathToConfigFile_property);
			if(system == null) {
				System.out.println("InstallationProperties.getPathToConfigFile Fanns ingen property");
				File defaultLocation = new File(pathToConfigFile_default);
				if(defaultLocation.exists()) {
					pathToConfigFile = defaultLocation.getCanonicalPath();
					System.out.println("InstallationProperties.getPathToConfigFile Kör defaulten. den finns. "+pathToConfigFile);
					return pathToConfigFile;
				}
				else {
					System.out.println("InstallationProperties.getPathToConfigFile Default konfig filen "+pathToConfigFile+" finns ej");
				}
			}
			File configLocation = new File(system);
			if(configLocation.exists()) {
				System.out.println("InstallationProperties.getPathToConfigFile konfade filen "+system+".");
				pathToConfigFile = configLocation.getCanonicalPath();
				return pathToConfigFile;
			}
			System.out.println("InstallationProperties.getPathToConfigFile konfade filen "+system+" finns ej.");
			return null;
		}
		catch(Exception e) {
			throw new Exception("No configuration file found.", e);
		}
	}
	
	private static CommandLineProperties clp = null;
	
	public static CommandLineProperties getCLPProperties() throws Exception {
		if(clp != null) {
			return clp;
		}
		String pathToConfig = getPathToConfigFile();
		System.out.println("InstallationProperties.getCLPProperties - pathToConfig="+pathToConfig);
		if(pathToConfig == null) {
			System.out.println("Cannot deduce property file to read");
			return null;
		}
		clp = PropertiesAssemble.collectFromFile(pathToConfig);
		return clp;
	}
	
	public static String getString(String propertyname) throws Exception {
		CommandLineProperties clp = getCLPProperties();
		if(clp == null) {
			return null;
		}
		return clp.getString(propertyname);
	}

	public static String getString(String propertyname, final String defaultValue) {
		return getCLPProperty(propertyname, defaultValue);
	}
	
	private static String getCLPProperty(String propertyname, final String defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		}
		catch(Exception e) {
		}
		if(clp == null) {
			return null;
		}
		String out = clp.getString(propertyname, defaultValue);
		if(out == defaultValue) {
			System.out.println("Parametern "+propertyname+" är inte satt i konfigfilen");
		}
		return out;
	}
	
  	public static void setUserValue(String property, String value) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		}
		catch(Exception e) {
		}
		if(clp == null) {
			return;
		}
		clp.setUserValue(property, value);
  	}
  	
  	public static String getUserValue(String property) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		}
		catch(Exception e) {
		}
		if(clp == null) {
			return null;
		}
		return clp.getUserValue(property);
  	}

	
	public static final String sshtunnel_username = "sshtunnel.username";
	public static final String sshtunnel_ipresolvable = "sshtunnel.ipresolvable";
	public static final String sshtunnel_localport = "sshtunnel.localport";
	public static final String sshtunnel_remoteipresolvable = "sshtunnel.remoteipresolvable";
	public static final String sshtunnel_remoteport = "sshtunnel.remoteport";
	public static final String ssh_port = "ssh.port";
	
	public static final String encoding = "encoding";

	public static final String hsqldb_driverclass = "hsqldb.driverclass"; 

	public static final String hsqldbmemory_dbname = "hsqldbmemory.dbname";
	public static final String hsqldbmemory_username = "hsqldbmemory.username";
	public static final String hsqldbmemory_urltemplate = "hsqldbmemory.urltemplate"; 

	public static final String hsqldbfile_username = "hsqldbfile.username";
	public static final String hsqldbfile_urltemplate = "hsqldbfile.urltemplate"; 

	public static final String hsqldbremote_ipresolvable = "hsqldbremote.ipresolvable";
	public static final String hsqldbremote_port = "hsqldbremote.port";
	public static final String hsqldbremote_protocol = "hsqldbremote.protocol";
	public static final String hsqldbremote_username = "hsqldbremote.username";
	public static final String hsqldbremote_dbname = "hsqldbremote.dbname";
	public static final String hsqldbremote_urltemplate = "hsqldbremote.urltemplate"; 

	public static final String mysql_ipresolvable = "mysql.ipresolvable";
	public static final String mysql_port = "mysql.port";
	public static final String mysql_urltemplate = "mysql.urltemplate"; 
	public static final String mysql_driverclass = "mysql.driverclass"; 

	public static final String mssqlserver_ipresolvable = "mssqlserver.ipresolvable";
	public static final String mssqlserver_port = "mssqlserver.port";
	public static final String mssqlserver_username = "mssqlserver.username";
	public static final String mssqlserver_urltemplate = "mssqlserver.urltemplate"; 
	public static final String mssqlserver_driverclass = "mssqlserver.driverclass"; 

	public static final String oracle_ipresolvable = "oracle.ipresolvable";
	public static final String oracle_port = "oracle.port";
	public static final String oracle_urltemplate = "oracle.urltemplate"; 
	public static final String oracle_driverclass = "oracle.driverclass"; 

	public static final String ibmdb2_ipresolvable = "ibmdb2.ipresolvable";
	public static final String ibmdb2_port = "ibmdb2.port";
	public static final String ibmdb2_urltemplate = "ibmdb2.urltemplate"; 
	public static final String ibmdb2_driverclass = "ibmdb2.driverclass"; 

	public static final String postgres_ipresolvable = "postgres.ipresolvable";
	public static final String postgres_port = "postgres.port";
	public static final String postgres_urltemplate = "postgres.urltemplate"; 
	public static final String postgres_driverclass = "postgres.driverclass"; 

	public static final String ingoingparameter_dbname = "dbname";
	public static final String ingoingparameter_dbfile = "dbfile";
	public static final String ingoingparameter_protocol = "protocol";
	public static final String ingoingparameter_ipresolvable = "ipresolvable";
	public static final String ingoingparameter_port = "port";
	
	public static void main(String args[]) throws Exception {
		setUserValue(ingoingparameter_dbname, "ettnamn");
		setUserValue(ingoingparameter_ipresolvable, "domain.com");
		setUserValue(ingoingparameter_port, "1234");
		System.out.println("mssqlserver_urltemplate "+getString(mssqlserver_urltemplate));
	}

}