package org.omega.connectivity.jdbc.resultset;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.resultset.SQLResult;
import org.omega.connectivity.sshtunnels.TunnelForwardLEncapsulation;

import se.modlab.generics.exceptions.IntolerableException;

public class JDBCConnection implements ConnectionOperations {
	
	protected Connection c;
	private String driverClass;
	private String dbImplementationName;
	private String dburl;
	private String user;
	private String password;
	private String dbname;
	private TunnelForwardLEncapsulation tfle = null;

	public JDBCConnection(Connection c, String driverClass, String dbImplementationName, String dburl, String user, String password, String dbname, TunnelForwardLEncapsulation tfle) {
		this.c = c;
		this.driverClass = driverClass;
		this.dbImplementationName = dbImplementationName;
		this.dburl = dburl;
		this.user = user;
		this.password = password;
		this.dbname = dbname;
		this.tfle = tfle;
	}
	
	public SQLResult select_via_executeQuery(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		//ps.close();
		return new JDBCSQLResult(rs, ps);
	}

	public SQLResult insert_via_executeQuery(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		//ps.close();
		return new JDBCSQLResult(rs, ps);
	}

	public boolean create_execute(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		boolean out = ps.execute();
		ps.close();
		return out;
	}

	public SQLResult insert_GetGeneratedKeys(String sql, String pkfields[]) throws SQLException {
		Statement statement = c.createStatement();
		statement.execute(sql, Statement.RETURN_GENERATED_KEYS);
	    ResultSet rs = statement.getGeneratedKeys();
	    //statement.close();
	    return new JDBCSQLResult(rs, statement);
	}
	
	public int delete_via_executeUpdate(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		int out = ps.executeUpdate();
		ps.close();
		return out;
	}

	public int update_via_executeUpdate(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		int out = ps.executeUpdate();
		ps.close();
		return out;
	}
	
	public void close() throws SQLException {
		c.close();
		if(tfle != null) {
			tfle.closeTunnel();
		}
	}
	
	public Connection getConnection() {
		return c;
	}
	
	public boolean equals(Object o) {
		if(o == null) {
			//System.out.println("GDBAPPConnection.equals null");
			return false;
		}
		if(o == this) {
			//System.out.println("GDBAPPConnection.equals same");
			return true;
		}
		if(!(o instanceof JDBCConnection)) {
			//System.out.println("GDBAPPConnection.equals wrong class "+o.getClass().getName());
			return false;
		}
		JDBCConnection other = (JDBCConnection) o;
		boolean out = c == other.c;
		//System.out.println("GDBAPPConnection.equals not same connection");
		return out;
	}
	
	public String toString() {
		return "Connection "+dbImplementationName+", URL "+dburl+", user "+user+", password "+password;
	}

	public boolean isClosed() throws SQLException {
		return c.isClosed();
	}
	
	public String getUser() {
		return user;
	}
	
	public boolean valueEquals(ConnectionOperations other) throws IntolerableException {
		return equals(other);
	}

	public int uploadToBlob_via_executeUpdate(String sql, InputStream is) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		if(is != null) {
			ps.setBinaryStream(1, is);//), len);
			int out = ps.executeUpdate();
			ps.close();
			return out;
		}
		else {
			ps.setNull(1, java.sql.Types.BLOB);
			int out = ps.executeUpdate();
			ps.close();
			return out;
		}
	}

	public InputStream downloadFromBlob_with_executeQuery(String sql) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			//return new BufferedInputStream(new ProgressMonitorInputStream(null, "Downloading", rs.getBinaryStream(1)));
			return rs.getBinaryStream(1);
		}
		return null;
	}
	
	public String getDBName() {
		return dbname;
	}

/*
 * Snippet from http://www.java2s.com/Code/Java/Database-SQL-JDBC/ReadBLOBsdatafromdatabase.htm
  
    String sql = "SELECT name, description, image FROM pictures ";
    PreparedStatement stmt = conn.prepareStatement(sql);
    ResultSet resultSet = stmt.executeQuery();
    while (resultSet.next()) {
      String name = resultSet.getString(1);
      String description = resultSet.getString(2);
      File image = new File("D:\\java.gif");
      FileOutputStream fos = new FileOutputStream(image);

      byte[] buffer = new byte[1];
      InputStream is = resultSet.getBinaryStream(3);
      while (is.read(buffer) > 0) {
        fos.write(buffer);
      }
      fos.close();
    }
*/

}
