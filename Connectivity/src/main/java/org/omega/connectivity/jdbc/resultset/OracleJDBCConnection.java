package org.omega.connectivity.jdbc.resultset;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.omega.connectivity.resultset.SQLResult;
import org.omega.connectivity.sshtunnels.TunnelForwardLEncapsulation;

public class OracleJDBCConnection extends JDBCConnection {
	
	public OracleJDBCConnection(Connection _c, String _driverClass, String _dbImplementationName, String _dburl, String _user, String _password, String dbname, TunnelForwardLEncapsulation tfle) {
		super(_c, _driverClass, _dbImplementationName, _dburl, _user, _password, dbname, tfle);
	}

	public SQLResult insert_GetGeneratedKeys(String sql, String pkfields[]) throws SQLException {
		PreparedStatement ps = c.prepareStatement(sql, pkfields);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
	    return new JDBCSQLResult(rs, ps);
	}
 
	public boolean create_execute(String sql) throws SQLException {
		Statement statement = c.createStatement();
		boolean out = statement.execute(sql);
		statement.close();
	    return out;
	}

}
