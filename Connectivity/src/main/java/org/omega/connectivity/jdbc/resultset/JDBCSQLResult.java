package org.omega.connectivity.jdbc.resultset;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.omega.connectivity.jdbc.resultset.JDBCSQLResultMetaData;
import org.omega.connectivity.resultset.SQLResult;
import org.omega.connectivity.resultset.SQLResultMetaData;

public class JDBCSQLResult implements SQLResult {
	
	private ResultSet rs = null;
	private Statement statement = null;
	
	public JDBCSQLResult(ResultSet _rs, Statement _statement) {
		rs = _rs;
		statement = _statement;
	}

	public String getString(int idx) throws SQLException {
		return rs.getString(idx);
	}

	public int getInt(int idx) throws SQLException {
		try {
			return rs.getInt(idx);
		}
		catch(Exception e) {
			RowId ri = rs.getRowId(idx);
			//System.out.println("JDBCSQLResult.getRowId("+idx+") -> "+ri);
			String st = rs.getString(idx);
			//System.out.println("JDBCSQLResult.getString("+idx+") -> "+st);
			try {
				return Integer.parseInt(ri.toString());
			}
			catch(Exception ee) {
				System.err.println("JDBCSQLResult.getInt Exception "+ee);
			}
			return -1;
			
		}
	}

	public double getDouble(int idx) throws SQLException {
		return rs. getDouble(idx);
	}

	public Date getDate(int idx) throws SQLException {
		return rs.getDate(idx);
	}

	public Object getObject(int idx) throws SQLException {
		Object obj = rs.getObject(idx);
		try {
			if(obj instanceof Timestamp) {
				Date d = new Date(((Timestamp) obj).getTime());
				return d;
			}
			if(obj instanceof BigDecimal) {
				Double d = ((BigDecimal) obj).doubleValue();
				return d;
			}
			if(obj instanceof BigInteger) {
				Integer i = ((BigInteger) obj).intValue();
				return i;
			}
			if(obj instanceof Short) {
				Short s = (Short) obj;
				return s.intValue();
			}
			if(obj instanceof Long) {
				Long s = (Long) obj;
				return s.intValue();
			}
			return obj;
		}
		catch(NullPointerException cce) {
			throw new SQLException("Unable to retrieve object at row "+rs.getRow()+", column "+idx+". idx = "+idx+", value = "+obj);
		}
	}

	public SQLResultMetaData getMetaData() throws SQLException {
		return new JDBCSQLResultMetaData(rs.getMetaData());
	}

	public boolean next() throws SQLException {
		return rs.next();
	}
	
	public void close() throws SQLException {
		statement.close();
		rs.close();
	}
	

}
