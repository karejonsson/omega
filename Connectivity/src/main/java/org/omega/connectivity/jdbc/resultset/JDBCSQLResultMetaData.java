package org.omega.connectivity.jdbc.resultset;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.omega.connectivity.resultset.SQLResultMetaData;

public class JDBCSQLResultMetaData implements SQLResultMetaData {
	
	private ResultSetMetaData rsmd = null;
	
	public JDBCSQLResultMetaData(ResultSetMetaData _rsmd) {
		rsmd = _rsmd;
	}

	public int getColumnCount() throws SQLException {
		return rsmd.getColumnCount();
	}

	public String getColumnName(int idx) throws SQLException {
		return rsmd.getColumnName(idx);
	}

}
