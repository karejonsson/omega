package org.omega.connectivity.resultset;

import java.sql.Date;
import java.sql.SQLException;

public interface SQLResult {
	
	public String getString(int idx) throws SQLException;
	public int getInt(int idx) throws SQLException;
	public double getDouble(int idx) throws SQLException;
	public Date getDate(int idx) throws SQLException;
	public Object getObject(int idx) throws SQLException;
	public SQLResultMetaData getMetaData() throws SQLException;
	public boolean next() throws SQLException;
	public void close() throws SQLException;

}
