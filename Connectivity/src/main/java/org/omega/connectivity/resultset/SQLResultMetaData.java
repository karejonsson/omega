package org.omega.connectivity.resultset;

import java.sql.SQLException;

public interface SQLResultMetaData {
	
	public int getColumnCount() throws SQLException ;
	public String getColumnName(int idx) throws SQLException;

}
