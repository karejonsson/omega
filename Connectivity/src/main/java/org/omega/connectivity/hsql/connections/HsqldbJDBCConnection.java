package org.omega.connectivity.hsql.connections;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.omega.connectivity.jdbc.resultset.JDBCConnection;
import org.omega.connectivity.sshtunnels.TunnelForwardLEncapsulation;

public class HsqldbJDBCConnection extends JDBCConnection {
	public HsqldbJDBCConnection(Connection _c, String _driverClass, String _dbImplementationName, String _dburl,
			String _user, String _password, String dbname, TunnelForwardLEncapsulation tfle) {
		super(_c, _driverClass, _dbImplementationName, _dburl, _user, _password, dbname, tfle);
	}

	public InputStream downloadFromBlob_via_executeQuery(String sql) throws SQLException {
		PreparedStatement ps = this.c.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Blob b = rs.getBlob(1);
			return b.getBinaryStream();
		}
		return null;
	}
}