package org.omega.connectivity.hsql.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.helpers.OmegaEmbeddedConfig;
import org.omega.connectivity.jdbc.resultset.JDBCConnection;
import org.omega.connectivity.logging.RetainingSQLIntermediateLogger;
import org.omega.connectivity.logging.RetainingSQLMemoryLogger;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.sshtunnels.TunnelForwardLEncapsulation;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserRuntimeError;

public class OmegaEmbeddedHSQL {
	private static JDBCConnection getJDBCConnection(String driverClass, String dbImplementationName, String dburl,
			String user, String password, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		if(tflp != null && !tflp.hasAll()) {
	    	String message = "Insufficient SSH tunnel parameters provided";
	    	UserRuntimeError ure = new UserRuntimeError(message);
	    	throw ure;
		}
		TunnelForwardLEncapsulation tfle = null;
		try {
			if(tflp != null) {
				tfle = new TunnelForwardLEncapsulation(tflp);
			}
		}
		catch(Exception e) {
			String message = "Parameters for SSH tunnel not complete";
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			if(tfle != null && !tfle.openTunnel()) {
				String message = "Cannot open SSH tunnel";
				UserRuntimeError ure = new UserRuntimeError(message);
				throw ure;
			}
		}
		catch(Exception e) {
			String message = "Cannot open SSH tunnel: "+e.getMessage();
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			Thread.sleep(300);
		}
		catch(Exception e) {
		}
		try {
			Class.forName(driverClass);
		} 
		catch (Exception e) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
			String message = "ERROR: failed to load " + dbImplementationName + " JDBC driver! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message, e);
			throw ure;
		}
		Connection c = null;
		try {
			c = DriverManager.getConnection(dburl, user, password);
		} 
		catch (SQLException sqle) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
			String message = "Unable to create connection! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message, sqle);
			throw ure;
		}
		if(c == null) {
			String message = "ERROR: Null connection created! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		return new JDBCConnection(c, driverClass, dbImplementationName, dburl, user, password, dbname, tfle);
	}

	private static JDBCConnection getHsqldbJDBCConnection(String driverClass, String dbImplementationName, String dburl,
			String user, String password, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		TunnelForwardLEncapsulation tfle = null;
		try {
			if(tflp != null) {
				tfle = new TunnelForwardLEncapsulation(tflp);
			}
		}
		catch(Exception e) {
			String message = "Parameters for SSH tunnel not complete";
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			if(tfle != null && !tfle.openTunnel()) {
				String message = "Cannot open SSH tunnel";
				UserRuntimeError ure = new UserRuntimeError(message);
				throw ure;
			}
		}
		catch(Exception e) {
			String message = "Cannot open SSH tunnel: "+e.getMessage();
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			Thread.sleep(300);
		}
		catch(Exception e) {
		}
		try {
			Class.forName(driverClass);
		} catch (Exception e) {
			String message = "Failed to load " + dbImplementationName + " JDBC driver! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}

		Connection c = null;
		try {
			c = DriverManager.getConnection(dburl, user, password);
		} catch (SQLException sqle) {
			String message = "Unable to create connection! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message, sqle);
			throw ure;
		}
		if (c == null) {
			String message = "Null connection created! URL=" + dburl;
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		return new HsqldbJDBCConnection(c, driverClass, dbImplementationName, dburl, user, password, dbname, tfle);
	}

	public static ConnectionOperations getPureHSQLDBFileConnection(String dbfile, String user, String password,
			String dbname) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbfile, dbfile);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbmemory_urltemplate, "");
		//String dburl = "jdbc:hsqldb:file:" + dbfile;
		return getPureHSQLDBConnection(dburl, user, password, dbname, null);
	}

	public static EmbeddedModelHandling connectHSQLDBFile(String gdbappconfig, String encoding, String dbfile,
			String user, String password, String frametitle, String dbname) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbfile, dbfile);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbfile_urltemplate, "");
		//String dburl = "jdbc:hsqldb:file:" + dbfile;
		return connectHSQLDB(gdbappconfig, encoding, dburl, user, password, frametitle, dbname, null);
	}

	public static ConnectionOperations getPureHSQLDBRemoteConnection(String ipresolvable, String port, String protocol,
			String user, String password, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_protocol, protocol);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbremote_urltemplate, "");
		//String dburl = "jdbc:hsqldb:" + protocol + "://" + ipresolvable + ":" + port + "/" + dbname;
		return getPureHSQLDBConnection(dburl, user, password, dbname, tflp);
	}

	public static EmbeddedModelHandling connectHSQLDBRemote(String gdbappconfig, String encoding, String ipresolvable,
			String port, String protocol, String user, String password, String dbname, String frametitle, TunnelForwardLParameters tflp)
					throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_protocol, protocol);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbremote_urltemplate, "");
		//String dburl = "jdbc:hsqldb:" + protocol + "://" + ipresolvable + ":" + port + "/" + dbname;
		return connectHSQLDB(gdbappconfig, encoding, dburl, user, password, frametitle, dbname, tflp);
	}

	public static ConnectionOperations getPureHSQLDBConnection(String dburl, String user, String password, String dbname, TunnelForwardLParameters tflp)
			throws IntolerableException {
		return getJDBCConnection(InstallationProperties.getString(InstallationProperties.hsqldb_driverclass, ""), "HSQLDB", dburl, user, password, dbname, tflp);
	}

	public static EmbeddedModelHandling connectHSQLDB(String gdbappconfig, String encoding, String dburl, String user,
			String password, String frametitle, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		ConnectionOperations c = getHsqldbJDBCConnection(InstallationProperties.getString(InstallationProperties.hsqldb_driverclass, ""), "HSQLDB", dburl, user, password,
				dbname, tflp);
		if (c == null) {
			throw new SystemError("Unable to get connection to HSQLDB");
		}
		Recreator rec = RecreatorManager.getRecreator("HSQLDB");
		if (rec == null) {
			throw new InternalProgrammingError("Unable to get recreator");
		}

		ModelConfiguration hs = null;
		if ((gdbappconfig != null) && (gdbappconfig.trim().length() != 0)) {
			hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
			if (hs == null) {
				throw new UserCompiletimeError("Unable to get DB handling specification from config " + gdbappconfig);
			}
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();

		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}

	public static EmbeddedModelHandling initializeHSQLDBFile(String gdbappconfig, String encoding, String dbfile,
			String user, String password, String frametitle) throws Exception {
		if ((gdbappconfig == null) || (gdbappconfig.length() == 0)) {
			throw new Exception(
					"Cannot initialize HSQLDB file database with Omega config described in file " + gdbappconfig);
		}

		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbfile, dbfile);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbfile_urltemplate, "");
		//String dburl = "jdbc:hsqldb:file:" + dbfile;
		EmbeddedModelHandling details = connectHSQLDB(gdbappconfig, encoding, dburl, user, password, frametitle,
				"hsqldbname", null);
		if (details == null) {
			throw new InternalProgrammingError("Cant initialize DB without Omega details");
		}

		details.initialize();
		return details;
	}

	public static EmbeddedModelHandling initializeHSQLDBRemote(String gdbappconfig, String encoding,
			String ipresolvable, String port, String protocol, String user, String password, String dbname,
			String frametitle, TunnelForwardLParameters tflp) throws Exception {
		if ((gdbappconfig == null) || (gdbappconfig.length() == 0)) {
			throw new Exception(
					"Cannot initialize HSQLDB remote database with Omega config described in file " + gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_protocol, protocol);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbremote_urltemplate, "");
		//String dburl = "jdbc:hsqldb:" + protocol + "://" + ipresolvable + ":" + port + "/" + dbname;

		EmbeddedModelHandling details = connectHSQLDB(gdbappconfig, encoding, dburl, user, password, frametitle,
				dbname, tflp);
		if (details == null) {
			throw new InternalProgrammingError("Cant initialize DB without Omega details");
		}

		details.initialize();
		return details;
	}

	public static EmbeddedModelHandling initializeAndConnectHSQLDBMemory(String gdbappconfig, String encoding,
			String dbname, String user, String password, String frametitle) throws IntolerableException {
		if ((gdbappconfig == null) || (gdbappconfig.length() == 0)) {
			throw new SystemError(
					"Cannot initialize HSQLDB memory database with Omega config described in file " + gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.hsqldbmemory_urltemplate, "");
		//String dburl = "jdbc:hsqldb:mem:" + dbname + ";shutdown=true";
		EmbeddedModelHandling details = connectHSQLDB(gdbappconfig, encoding, dburl, user, password, frametitle,
				dbname, null);
		if (details == null) {
			throw new InternalProgrammingError("Cant initialize DB without Omega details");
		}

		details.initialize();
		return details;
	}
	
	public static void clearMemory(Connection connection) throws Exception {
		try {
			try {
				Statement stmt = connection.createStatement();
				try {
					stmt.execute("TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK");
					connection.commit();
				} finally {
					stmt.close();
				}
			} catch (SQLException e) {
				connection.rollback();
				throw new Exception(e);
			}
		} catch (SQLException e) {
			throw new Exception(e);
		} finally {
			if (connection != null) {
				connection.close();
			}
		}

	}
}