package org.omega.connectivity.sshtunnels;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class TunnelForwardLEncapsulation {
	
	private long reasonableLikleynessTime = 10000;
	private Long timeOfPositiveVerificationOfOpenedness = null;
	
	private int tunnel_lport = -1;
    private String tunnel_rhost = null;
    private int tunnel_rport = -1;
    
    private String login_user = null;
    private String login_domain = null;
    private int ssh_port = 22;
    private String login_password = null;
    
    public String toString() {
    	return "login_user "+login_user+", login_domain "+login_domain+", ssh_port "+ssh_port+
    			", tunnel_lport "+tunnel_lport+", tunnel_rhost "+tunnel_rhost+", tunnel_rport "+tunnel_rport;
    }

	public TunnelForwardLEncapsulation(
			String login_user,
			String login_domain,
			String login_password,
			int tunnel_lport,
		    String tunnel_rhost,
		    int tunnel_rport) {
		this(login_user, login_domain, login_password, tunnel_lport, tunnel_rhost, tunnel_rport, 22);
	}
	
	public TunnelForwardLEncapsulation(TunnelForwardLParameters tfparameters) throws Exception {
		if(!tfparameters.hasAll()) {
			throw new Exception("Parameters missing");
		}
		this.login_user = tfparameters.login_user;
		this.login_domain = tfparameters.login_domain;
		this.login_password = tfparameters.login_password;
		this.tunnel_lport = tfparameters.tunnel_lport;
		this.tunnel_rhost = tfparameters.tunnel_rhost;
		this.tunnel_rport = tfparameters.tunnel_rport;
		this.ssh_port = tfparameters.ssh_port;
	}
	
	public TunnelForwardLEncapsulation(
			String login_user,
			String login_domain,
			String login_password,
			int tunnel_lport,
		    String tunnel_rhost,
		    int tunnel_rport,
		    int ssh_port) {
		this.login_user = login_user;
		this.login_domain = login_domain;
		this.login_password = login_password;
		this.tunnel_lport = tunnel_lport;
		this.tunnel_rhost = tunnel_rhost;
		this.tunnel_rport = tunnel_rport;
		this.ssh_port = ssh_port;
	}
	
	private JSch jsch = null;
	private Session session = null;
	
	private boolean setupNaively() {
		if(!hasAll()) {
			tearDownAmbitiously();
			return false;
		}
		try {
	    	jsch = new JSch();
	    	session = jsch.getSession(login_user, login_domain, ssh_port);
	    	
	        session.setUserInfo(new UserInfo() {
				public String getPassphrase() {
					return null;
				}
				public String getPassword() {
					return login_password;
				}
				public boolean promptPassphrase(String arg0) {
					return true;
				}
				public boolean promptPassword(String arg0) {
					return true;
				}
				public boolean promptYesNo(String arg0) {
					return true;
				}
				public void showMessage(String arg0) {
				}
	        });

	        session.connect();
	    	
	        int assinged_port = session.setPortForwardingL(tunnel_lport, tunnel_rhost, tunnel_rport);
	        
	        if(assinged_port != tunnel_lport) {
	        	tearDownAmbitiously();
	        	return false;
	        }
	    	
	    	return session.isConnected();
		}
		catch(Exception e) {
        	tearDownAmbitiously();
			return false;
		}
	}
	
	private boolean tearDownAmbitiously() {
		boolean out = true;
		try {
			if(session != null) {
				session.disconnect();
			}
			session = null;
		}
		catch(Exception e) {
			session = null;
			out = false;
		}
		jsch = null;
		return out;
	}
	
	private boolean isActuallyOpen() {
		if(session == null) {
			return false;
		}
		return session.isConnected();
	}
	
	public boolean hasAll() {
		if(tunnel_lport == -1) {
			return false;
		}
		if(tunnel_rhost == null) {
			return false;
		}
		if(tunnel_rport == -1) {
			return false;
		}
		if(login_user == null) {
			return false;
		}
		if(login_domain == null) {
			return false;
		}
		if(ssh_port == -1) {
			return false;
		}
		if(login_password == null) {
			return false;
		}
		return true;
	}

	public boolean openTunnel() {
		//System.out.println("TunnelForwardLEncapsulation.openTunnel "+this);
		if(isSupposedToBeOpen()) {
			return false;
		}
		if((jsch != null) && (session != null)) {
			return false; // Already opened
		}
		if(setupNaively()) {
			timeOfPositiveVerificationOfOpenedness = System.currentTimeMillis();
			return true;
		}
		else {
			if(!tearDownAmbitiously()) {
				System.out.println("Unable to stop tunnel");
			}
			return false;
		}
	}

	private boolean isSupposedToBeOpen() {
		return (jsch != null) && (session != null);
	}
	
	public boolean closeTunnel() {
		if(!isSupposedToBeOpen()) {
			return false;
		}
		timeOfPositiveVerificationOfOpenedness = null;
		return tearDownAmbitiously();
	}
	
	public boolean restartTunnel(Long intermediateSleep) {
		try {
			if(!closeTunnel()) {
				return false;
			}
		}
		catch(Exception e) {
			return false;
		}
		if(intermediateSleep != null) {
			try {
				Thread.sleep(intermediateSleep);
			}
			catch(Exception e) {
				tearDownAmbitiously();
				return false;
			}
		}
		try {
			if(!openTunnel()) {
				tearDownAmbitiously();
				return false;
			}
		}
		catch(Exception e) {
			tearDownAmbitiously();
			return false;
		}
		return true;
	}
	
	public Long timeSinceLastPositiveVerification() {
		if(timeOfPositiveVerificationOfOpenedness == null) {
			return null;
		}
		return System.currentTimeMillis() - timeOfPositiveVerificationOfOpenedness;
	}
	
	public long getReasonableLikleynessTime() {
		return reasonableLikleynessTime;
	}

	public void setReasonableLikleynessTime(Long reasonableLikleynessTime) {
		this.reasonableLikleynessTime = reasonableLikleynessTime;
	}

	public boolean isReasonableLikleyStillOpened() {
		Long timeSinceOpened = timeSinceLastPositiveVerification();
		if(timeSinceOpened == null || timeSinceOpened > reasonableLikleynessTime) {
			if(isActuallyOpen()) {
				timeOfPositiveVerificationOfOpenedness = System.currentTimeMillis();
				return true;
			}
			return false;
		}
		return true;
	}
	
	public static void main(String args[]) throws InterruptedException {
		TunnelForwardLEncapsulation tunnel = new TunnelForwardLEncapsulation(
				"utv",
				"localhost",
				"pwd",
				2345,
				"localhost",
			    5432);
		if(!tunnel.openTunnel()) {
			System.out.println("Kunde inte öppna tunneln");
			return;
		}
		System.out.println("Tunneln öppen");
		
		Thread.sleep(10000);
		
		if(!tunnel.closeTunnel()) {
			System.out.println("Kunse inte stänga tunneln");
			return;
		}
		System.out.println("Tunneln stängd");
	}
	
}
