package org.omega.connectivity.sshtunnels;

public class TunnelForwardLParameters {

	public int tunnel_lport = -1;
	public String tunnel_rhost = null;
	public int tunnel_rport = -1;
    
	public String login_user = null;
	public String login_domain = null;
	public int ssh_port = 22;
	public String login_password = null;
	
	public boolean hasAll() {
		if(tunnel_lport < 1) {
			return false;
		}
		if(tunnel_rhost == null) {
			return false;
		}
		if(tunnel_rhost.length() < 5) {
			return false;
		}
		if(tunnel_rport < 1) {
			return false;
		}
		if(login_user == null) {
			return false;
		}
		if(login_user.length() < 1) {
			return false;
		}
		if(login_domain == null) {
			return false;
		}
		if(login_domain.length() < 1) {
			return false;
		}
		if(ssh_port < 1) {
			return false;
		}
		if(login_password == null) {
			return false;
		}
		if(login_password.length() < 2) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return 
				"    login_user \""+login_user+"\"\n"+
				"    login_domain \""+login_domain+"\"\n"+
				"    login_password \""+login_password+"\"\n"+
				"    tunnel_lport \""+tunnel_lport+"\"\n"+
				"    tunnel_rhost \""+tunnel_rhost+"\"\n"+
				"    tunnel_rport \""+tunnel_rport+"\"\n"+
				"    ssh_port \""+ssh_port+"\"\n";
	}

}
