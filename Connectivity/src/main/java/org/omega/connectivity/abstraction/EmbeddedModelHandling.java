package org.omega.connectivity.abstraction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.Type;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.resultset.SQLResult;

import se.modlab.generics.bshro.ifc.HierarchyBranch;
import se.modlab.generics.exceptions.InternalProgrammingError; 
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;

public class EmbeddedModelHandling {
	private ConnectionOperations conn;
	private Recreator rec; 
	private ModelConfiguration hs;
	private SQLMemoryLogger slw;
	private SQLIntermediateLogger si;
	private String frametitle = null;
	private Hashtable<String, Object> objects = new Hashtable<String, Object>();
	
	public EmbeddedModelHandling(ConnectionOperations conn, Recreator rec, ModelConfiguration hs, SQLMemoryLogger slw, SQLIntermediateLogger si, String frametitle) {
		this.conn = conn;
		this.rec = rec;
		this.hs = hs;
		this.slw = slw;
		this.si = si;
		this.frametitle = frametitle;
	}
	
	public ConnectionOperations getConnectionOperations() {
		return conn;
	}
	
	public Connection getConnection() {
		return conn.getConnection();
	}
	
	public String[] allNonBlobMembers(CreateStatement cs) {
		return rec.allNonBlobMembers(cs);
	}
	
	public Object getCacheObject(String label) {
		return objects.get(label);
	}
	
	public void setCacheObject(String label, Object obj) {
		objects.put(label, obj);
	}
	
	public boolean valueEquals(EmbeddedModelHandling other) throws IntolerableException {
		if(!conn.valueEquals(other.conn)) {
			return false;
		}
		if(!hs.valueEquals(other.hs)) {
			return false;
		} 
		return true;
	}
	
	public Recreator getRecreator() {
		return rec;
	}
	
	public ModelConfiguration getModelConfiguration() {
		return hs;
	}
	
	public TableConfiguration getTabelConfiguration(String tablename) {
		return hs.getDBTableHandlingSpecificationOnTableName(tablename);
	}
	
	public SQLIntermediateLogger getSQLIntermediate() {
		return si;
	}
	
	public SQLMemoryLogger getSQLMemoryLog() {
		return slw;
	}
	
	public void close() throws IntolerableException, SQLException {
		conn.close();
		conn.getConnection().close();
	}
	
	public String getFrametitle() {
		return frametitle;
	}
	
	public String toString() {
		return conn.toString();
	}
	
	public Enumeration<String> getTableNames() {
		return hs.getTableNames();
	}
	
	public boolean inHideTableList(String name) {
		return hs.inHideTableList(name);
	}
	
	public String getVersion() {
		return hs.getVersion();
	}
	
	public boolean equals(Object o) {
		if(o == null) {
			//System.out.println("GDBAPPDetails.equals null");
			return false;
		}
		if(!(o instanceof EmbeddedModelHandling)) {
			//System.out.println("GDBAPPDetails.equals wrong class "+o.getClass().getName());
			return false;
		}
		EmbeddedModelHandling d = (EmbeddedModelHandling) o;
		return conn.equals(d.conn);		
	}

	public void initialize() throws IntolerableException {
		int level = 0;
		if(hs == null) {
			throw new InternalProgrammingError("Cant initialize DB without DB handling specification");
			//return;
		}
		Vector<TableConfiguration> onThisLevel = hs.getTablehandlersWithLevel(level);
		if(onThisLevel == null) {
			return;
		}
		while(onThisLevel.size() != 0) {
			for(TableConfiguration table : onThisLevel) {
				//System.out.println("------------- EmbeddedModelhandling "+table.getTableName());
				CreateStatement cs = table.getCreateStatement();
				String createsql[] = rec.create(cs);
				//System.out.println("GDBAPPDetails.initialize sql="+createsql);
				try {
					for(int i = 0 ; i < createsql.length ; i++) {
						si.create_execute(conn, createsql[i], slw);
					}
				}
				catch(Exception e) {
					Object[] opts = { "Continue", "Stop" };
					//System.out.println("Failing SQL:\n"+createsql);
					
					StringBuffer sb = new StringBuffer();
					if(createsql.length == 1) {
						sb.append(createsql[0]+"\n\n");
					}
					else {
						for(int i = 0 ; i < createsql.length ; i++) {
							sb.append(""+i+":\n"+createsql[i]+"\n");
						}
						sb.append("\n");
					}

					UserRuntimeError ure = new UserRuntimeError("SQL execution failed! (Level = "+level+")\n\n"+sb.toString()+"\n\nSQL message: "+e.getMessage());
					throw ure; 
				}
			}
			onThisLevel = hs.getTablehandlersWithLevel(++level);
		}
	}
	
	public void performPrimaryKeyMissingTablesElimination() {
	    Enumeration<String> tablenames = hs.getTableNames();
	    while(tablenames.hasMoreElements()) {
	    	String tablename = tablenames.nextElement();
	    	TableConfiguration ths = hs.getDBTableHandlingSpecificationOnTableName(tablename);
	    	PrimaryKeyDeclaration pkd = ths.getCreateStatement().getPrimaryKeyDeclaration();
	    	if(pkd == null) {
	    		hs.removeDBTableHandlingSpecification(tablename);
	    		logText("Removes table "+tablename+" due to lack of primary key declaration");
	    	}
	    }
	}


	public EmbeddedTableHandling getEnvironment(TableConfiguration ths) { 
		return new EmbeddedTableHandling(hs, ths, conn, rec, slw, si, frametitle);
	}
	
	public TableConfiguration getDBTableHandlingSpecificationOnTableName(String tablename) {
		return hs.getDBTableHandlingSpecificationOnTableName(tablename);
	}
	
	public TableConfiguration getDBTableHandlingSpecificationOnObjectName(String tablename) {
		return hs.getDBTableHandlingSpecificationOnObjectName(tablename);
	}
	
	public String translate(String s) {
		return hs.translate(s);
	}
	
	public void setCache(String flag, Object obj) {
		hs.setCache(flag, obj);
	}
	
	public Object getCache(String flag) {
		return hs.getCache(flag);
	}
	
	public void delete(PrimaryKey pk) throws Exception {
		TableConfiguration ths = hs.getDBTableHandlingSpecificationOnTableName(pk.getTablename());
		String sql = rec.getDelete(ths.getCreateStatement(), pk); 
		//System.out.println("DBRowPool.removeRowObject sql = "+sql);
		int updates = si.delete_via_executeUpdate(conn, sql, slw);
		//System.out.println("DBRowPool.removeRowObject efter");
	}
	
	public int getTableLength(String name, CreateStatement cs) throws SQLException {
    	String sql = rec.tableLength(name, cs);
    	logText("DBPool.getTableLength table "+name+", sql="+sql);
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		rs.next();
		int out = rs.getInt(1);
		return out;
	}
	
	public SQLResult getReferrers(TableConfiguration ths, ForeignKey fk) throws Exception {
		String sql = rec.selectIndexed(ths.getCreateStatement(), fk); 
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
	
	public SQLResult getReferrers(TableConfiguration ths, Key k) throws Exception {
		String sql = rec.selectIndexed(ths.getCreateStatement(), k); 
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
	
	public SQLResult getReferrers(TableConfiguration ths, Hashtable<String, Object> ht) throws Exception {
		String sql = rec.selectSought(ths.getCreateStatement(), ht); 
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
	
	public SQLResult getReferred(TableConfiguration ths, PrimaryKey pk) throws Exception {
		String sql = rec.selectIndexed(ths.getCreateStatement(), pk); 
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
		
	public SQLResult selectAll(TableConfiguration ths) throws Exception {
		String sql = rec.selectAll(ths.getCreateStatement());
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
	
	public void insert(TableConfiguration ths, Hashtable<String, Object> ht) throws Exception {
		String sql = null;
		CreateStatement cs = ths.getCreateStatement();
		Column col = cs.getAutoIncrementColumn();
		sql = rec.getInsert(cs, ht);
		//System.out.println("DBPool.insertPoolObject - 5");
		if(col != null) {
			PrimaryKeyDeclaration pkd = cs.getPrimaryKeyDeclaration();
			//System.out.println("DBPool.insertPoolObject - 5 - 1");
			int autoIncrement = si.execute_Insert(conn, sql, slw, pkd.getColumnNames());
			//System.out.println("DBPool.insertPoolObject - 5 - 2");
			ht.put(col.getName().toUpperCase(), autoIncrement);
		}
		else {
			//System.out.println("DBPool.insertPoolObject - 5 - 3");
			si.insert_via_executeQuery(conn, sql, slw);
		}
	}

	public Collection<TableConfiguration> getTablehandlersLoopable() {
		return hs.getTablehandlersLoopable();
	}

	public Object getUntypedDefault(Type t) {
		return t.getUntypedDefault(rec);
	}

	public String getLastSQL() {
		return si.getLastSQL();
	}

	public void logText(String str) {
		slw.logText(str);
	}
	
	public void log(String str, Exception e) {
		slw.log(str, e);
	}
	
	public void logTrace(Exception e) {
		slw.logTrace(e);
	}
	
	public boolean hasErrors() {
		return hs.hasErrors();
	}
	
	public Vector<String> getErrors(int no) { 
		 return hs.getFirstErrors(no);
	}

	public HierarchyBranch getFileSystemReferenceCatalog() {
		return (HierarchyBranch) hs.getFileSystemReferenceCatalog();
	}
	
}
