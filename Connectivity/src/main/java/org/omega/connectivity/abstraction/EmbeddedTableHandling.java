package org.omega.connectivity.abstraction;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.icons.ImageIconByteArrayHolder;
import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.renderers.TextRenderer;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.Type;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.resultset.SQLResult;

public class EmbeddedTableHandling {
	
	private ModelConfiguration hs = null;
	public TableConfiguration ths = null;
	private ConnectionOperations conn;
	private Recreator rec;
	private SQLMemoryLogger slw;
	private SQLIntermediateLogger si;
	private String frametitle = null;
	
	public EmbeddedTableHandling(ModelConfiguration hs, TableConfiguration ths, ConnectionOperations conn, Recreator rec, SQLMemoryLogger slw, SQLIntermediateLogger si, String frametitle) {
		this.hs = hs;
		this.ths = ths;
		this.conn = conn;
		this.rec = rec;
		this.slw = slw;
		this.si = si;
		this.frametitle = frametitle;
	}

	public EmbeddedTableHandling(EmbeddedTableHandling env, TableConfiguration ths, String frametitle) throws Exception {
		this.hs = env.hs;
		if(ths == null) {
			throw new Exception("Got null for table handling specification");
		}
		this.ths = ths;
		this.conn = env.conn;
		this.rec = env.rec;
		this.slw = env.slw;
		this.si = env.si;
		this.frametitle = frametitle;
	}
	
	public EmbeddedTableHandling(EmbeddedModelHandling details, TableConfiguration ths) throws Exception {
		this.hs = details.getModelConfiguration();
		if(ths == null) {
			throw new Exception("Got null for table handling specification");
		}
		this.ths = ths;
		this.conn = details.getConnectionOperations();
		this.rec = details.getRecreator();
		this.slw = details.getSQLMemoryLog();
		this.si = details.getSQLIntermediate();
		this.frametitle = details.getFrametitle();
	}
	
	public String getFrametitle() {
		return frametitle;
	}
	
	public EmbeddedTableHandling getOtherEnvironmentOnObjectName(String name) throws Exception {
		TableConfiguration referredsType = hs.getDBTableHandlingSpecificationOnObjectName(name);
		if(referredsType == null) {
			throw new Exception("Table "+name+" is refered but not defined");
		}
		return new EmbeddedTableHandling(this, hs.getDBTableHandlingSpecificationOnObjectName(name), frametitle);
	}

	public EmbeddedTableHandling getOtherEnvironmentOnTableName(String name) throws Exception {
		TableConfiguration referredsType = hs.getDBTableHandlingSpecificationOnTableName(name);
		if(referredsType == null) {
			throw new Exception("Table "+name+" is refered but not defined");
		}
		return new EmbeddedTableHandling(this, hs.getDBTableHandlingSpecificationOnTableName(name), frametitle);
	}

	public void logText(String str) {
		slw.logText(str);
	}
	
	public void log(String str, Exception e) {
		slw.log(str, e);
	}
	
	public void logTrace(Exception e) {
		slw.logTrace(e);
	}

	public String getObjectName() {
		return ths.getObjectName();
	}
	
	public String getTableName() {
		return ths.getTableName();
	}
	
	public String translate(String name) {
		return ths.translate(name);
	}
	
	public int getLevel() {
		return ths.getLevel();
	}
	
	public void update(Hashtable<String, Object> ht, PrimaryKey pk) throws Exception {
		String sql = null;
		try {
			sql = rec.getUpdate(ths.getCreateStatement(), ht, pk);  
		}
		catch(Exception e) {
			e.printStackTrace();
			String message = "Failed on update.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
		try {
			int updates = si.update_via_executeUpdate(conn, sql, slw);
		}
		catch(Exception e) {
			String message = "Failed on update.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
	}

	public void uploadToBlob(InputStream is, String columnName, PrimaryKey pk) throws Exception {
		String sql = null;
		try {
			sql = rec.getUploadBlob(ths.getCreateStatement(), columnName, pk);  
		}
		catch(Exception e) {
			String message = "Failed on blob insert.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
		try {
			int updates = si.uploadToBlob_via_executeUpdate(conn, sql, slw, is);
		}
		catch(Exception e) {
			String message = "Failed on blob update.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
	}
	
	public InputStream downloadFromBlob_with_executeQuery(String columnName, PrimaryKey pk) throws Exception {
		String sql = null;
		try {
			sql = rec.getDownloadBlob(ths.getCreateStatement(), columnName, pk);  
		}
		catch(Exception e) {
			String message = "Failed on blob select.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
		try {
			return si.downloadFromBlob_with_executeQuery(conn, sql, slw); //.uploadToBlob_via_executeUpdate(conn, sql, slw, is);
		}
		catch(Exception e) {
			String message = "Failed on blob download.\n\nSql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(message+", object "+toString());
			throw new Exception(message);
		}
	}

	public SQLResult getReferred(TableConfiguration ths, PrimaryKey pk) throws Exception {
		String sql = null;
		try {
			sql = rec.selectIndexed(ths.getCreateStatement(), pk);  
		}
		catch(Exception e) {
			String sqlmessage = "Failed on insert. Sql was "+sql+"\n\n"+
			"Exceptions message was "+e.getMessage();
			logText(sqlmessage);
			throw new Exception(sqlmessage);
		}
		SQLResult rs = si.select_via_executeQuery(conn, sql, slw);
		return rs;
	}
	
	public String getLastSQL() {
		return si.getLastSQL();
	}
	
	public SQLResult select_via_executeQuery(String sql) throws SQLException {
		return si.select_via_executeQuery(conn, sql, slw);
	}
	
	public String getMetaDescription() {
		StringBuffer sb = new StringBuffer();
		String createtable[] = rec.create(ths.getCreateStatement());
		if(createtable.length == 1) {
			sb.append(createtable[0]+"\n\n");
		}
		else {
			for(int i = 0 ; i < createtable.length ; i++) {
				sb.append(""+i+":\n"+createtable[i]+"\n");
			}
			sb.append("\n");
		}
		for(int i = 0 ; i < ths.getNoReferringConstraints() ; i++) {
			ForeignKeyDeclaration con = ths.getReferringConstraint(i);
			sb.append(con.toString()+"\n");
		}
		return sb.toString();
	}

	public String getMetaDescription(Hashtable<String, Object> ht) {
		StringBuffer sb = new StringBuffer(getMetaDescription());
		sb.append("\n");
		Enumeration<String> keys = ht.keys();
		while(keys.hasMoreElements()) {
			String name = keys.nextElement();
			sb.append("Name "+name+" has value "+ht.get(name)+"\n");
		}
		return sb.toString();
	}

	public boolean isUniqueConstraintViolation(Exception e) {
		return rec.isUniqueConstraintViolation(e);
	}
	
	public boolean isConstraintViolation(Exception e) {
		return rec.isConstraintViolation(e);
	}
	
	public String getDBsName() {
		return rec.getDBsName();
	}

	public Object getUntypedDefault(Type t) {
		return t.getUntypedDefault(rec);
	}
	
	public void setCache(String flag, Object obj) {
		hs.setCache(flag, obj);
	}
	
	public Object getCache(String flag) {
		return hs.getCache(flag);
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclaration(String dbtablename, String constraintname) {
		return hs.getDBTableHandlingSpecificationOnTableName(dbtablename).getCreateStatement().getFirstForeignKeyDeclarationFromName(constraintname);
	}
	
	public ForeignKeyDeclaration getForeignKeyDeclaration(String constraintname) {
		return ths.getCreateStatement().getFirstForeignKeyDeclarationFromName(constraintname);
	}
	
	public ForeignKey getForeignKey(int i, Hashtable<String, Object> ht) throws Exception {
		//System.out.println("DBPoolObject getForeignKey("+i+")");
		CreateStatement cs = ths.getCreateStatement();
		//System.out.println("DBPoolObject getForeignKey 1 cs="+cs+" och i="+i);
		ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
		//System.out.println("DBPoolObject getForeignKey 2 fkd="+fkd);
		ForeignKey out = new ForeignKey(fkd);
		//System.out.println("DBPoolObject getForeignKey 3 fk="+out);
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String membername = fkd.getReferringMember(ii);
			//System.out.println("DBPoolObject getForeignKey 4");
			//String tablemembername = fkd.getTableMember(ii);
			//System.out.println("DBPoolObject getForeignKey member = "+membername+", tablemembername = "+tablemembername);
			out.set(membername, ht.get(membername.toUpperCase()));
			//System.out.println("DBPoolObject getForeignKey 5");
		}
		return out;
	}
	
	public Key getKey(int i, Hashtable<String, Object> ht) throws Exception {
		//System.out.println("DBPoolObject getForeignKey("+i+")");
		CreateStatement cs = ths.getCreateStatement();
		//System.out.println("DBPoolObject getForeignKey 1 cs="+cs+" och i="+i);
		KeyDeclaration kd = cs.getKeyDeclaration(i);
		//System.out.println("DBPoolObject getForeignKey 2 fkd="+fkd);
		Key out = new Key(kd);
		//System.out.println("DBPoolObject getForeignKey 3 fk="+out);
		for(int ii = 0 ; ii < kd.getNoMembers() ; ii++) {
			String membername = kd.getMember(ii);
			//System.out.println("DBPoolObject getForeignKey 4");
			//String tablemembername = fkd.getTableMember(ii);
			//System.out.println("DBPoolObject getForeignKey member = "+membername+", tablemembername = "+tablemembername);
			out.set(membername, ht.get(membername.toUpperCase()));
			//System.out.println("DBPoolObject getForeignKey 5");
		}
		return out;
	}
	
	public ForeignKey getDownstreamForeignKey(String name, Hashtable<String, Object> ht) throws Exception {
		CreateStatement cs = ths.getCreateStatement();
		ForeignKeyDeclaration fkd = cs.getFirstForeignKeyDeclarationFromName(name);
		return getDownstreamForeignKey(fkd, ht);
	}
	
	public ForeignKey getDownstreamForeignKey(ForeignKeyDeclaration fkd, Hashtable<String, Object> ht) throws Exception {
		ForeignKey out = new ForeignKey(fkd);
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String membername = fkd.getReferringMember(ii);
			String tablemembername = fkd.getReferredTableMember(ii);
			out.set(membername, ht.get(membername.toUpperCase()));
		}
		return out;
	}
	
	public ForeignKey getUpstreamForeignKey(ForeignKeyDeclaration fkd, Hashtable<String, Object> ht) throws Exception {
		//System.out.println("EmbeddedTableHandling.getUpstreamForeignKey - fkd = "+fkd);
		ForeignKey out = new ForeignKey(fkd);
		for(int ii = 0 ; ii < fkd.getNoReferringMembers() ; ii++) {
			String referringMembername = fkd.getReferringMember(ii);
			String referredMembername = fkd.getReferredTableMember(ii);
			Object value = ht.get(referredMembername.toUpperCase());
			//System.out.println("EmbeddedTableHandling.getUpstreamForeignKey - referringMembername = "+referringMembername+", referredMembername = "+referredMembername+", value = "+value);
			out.set(referringMembername, value);
		}
		return out;
	}
	
	public PrimaryKeyDeclaration getPrimaryKeyDeclaration() {
		return ths.getCreateStatement().getPrimaryKeyDeclaration();
	}
	
	public TextRenderer getInstanceTextRenderer() {
		return ths.getInstanceTextRenderer();
	}
	
	public boolean hasTable(String name) {
		return hs.getDBTableHandlingSpecificationOnTableName(name) != null;
	}
	
	public String getIconfile(String table) {
		return hs.getDBTableHandlingSpecificationOnTableName(table).getIconfile();
	}
	
	public String getIconfile() {
		return ths.getIconfile();
	}
	
	public byte[] getImageIconAsBytes(String table, String key) {
		return hs.getDBTableHandlingSpecificationOnTableName(table).getImageIconAsBytes(key);
	}

	public byte[] getImageIconAsBytes(String key) {
		return ths.getImageIconAsBytes(key);
	}

	public ImageIconByteArrayHolder getImageIconBestEffort(String table, String key) {
		return hs.getDBTableHandlingSpecificationOnTableName(table).getImageIconBestEffort(key);
	}

	public ImageIconByteArrayHolder getImageIconBestEffort(String key) {
		return ths.getImageIconBestEffort(key);
	}

	public void setImageIconAsImageIcon(String table, String key, ImageIconByteArrayHolder icon) {
		hs.getDBTableHandlingSpecificationOnTableName(table).setImageIconAsImageIcon(key, icon);
	}

	public void setImageIconAsImageIcon(String key, ImageIconByteArrayHolder icon) {
		ths.setImageIconAsImageIcon(key, icon);
	}

	public void setProblem(String table, String key, boolean problem) {
		hs.getDBTableHandlingSpecificationOnTableName(table).setProblem(key, problem);
	}
	
	public void setProblem(String key, boolean problem) {
		ths.setProblem(key, problem);
	}
	
	public boolean getProblem(String table, String key) {
		return hs.getDBTableHandlingSpecificationOnTableName(table).getProblem(key);
	}
	
	public boolean getProblem(String key) {
		return hs.getDBTableHandlingSpecificationOnTableName(ths.getTableName()).getProblem(key);
	}
	
	public TextRenderer getReferenceDialogTextRenderer(String tableNamePointingFrom, String constraintsName) {
		return hs.getDBTableHandlingSpecificationOnTableName(tableNamePointingFrom).getReferenceDialogTextRenderer(constraintsName);
	}
	
	public TextRenderer getReferenceDialogTextRenderer(String constraintsName) {
		return ths.getReferenceDialogTextRenderer(constraintsName);
	}
	
	public EmbeddedModelHandling getDetails() {
		return new EmbeddedModelHandling(conn, rec, hs, slw, si, frametitle);
	}

	public Object columnType(int r) {
		Column col = ths.getCreateStatement().getColumn(r);
		return rec.columnType(col);
	}

}
