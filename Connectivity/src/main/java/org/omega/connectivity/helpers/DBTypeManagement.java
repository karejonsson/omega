package org.omega.connectivity.helpers;

import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.UnmanageableType;
import org.omega.configuration.values.LobValue;
import org.omega.configuration.values.UnmanagedValue;
import org.omega.connectivity.abstraction.EmbeddedTableHandling;
import org.omega.connectivity.recreatesql.CommonSQLAssembler;
import org.omega.connectivity.resultset.SQLResult;

public class DBTypeManagement {
	
	public static Object getDefault(Column col) {
		Object out = null;
		Type t = col.getType();
		if(t.hasDefault()) {
			if(t instanceof IntType) {
				if(!(((IntType) t).hasNullDefault())) {
					out = ((IntType) t).getDefault();
				}
			}
			if(t instanceof DoubleType) {
				if(!(((DoubleType) t).hasNullDefault())) {
					out = ((DoubleType) t).getDefault();
				}
			}
			if(t instanceof DecimalType) {
				if(!(((DecimalType) t).hasNullDefault())) {
					out = ((DecimalType) t).getDefault();
				}
			}
			if(t instanceof DatetimeType) {
				if(!(((DatetimeType) t).hasNullDefault())) {
					out = ((DatetimeType) t).getDefault();
				}
			}
			if(t instanceof TextualType) {
				if(!(((TextualType) t).hasNullDefault())) {
					out = ((TextualType) t).getDefault();
				}
			}
		}
		if(t instanceof LobType) {
			out = new LobValue();
			//out_.setBaseAware(false);
		}
		return out;
	}
	
/*	
	public static String getDefault(Column col) {
		String out = null;
		Type t = col.getType();
		if(t.hasDefault()) {
			if(t instanceof IntType) {
				if(!(((IntType) t).hasNullDefault())) {
					out = ""+((IntType) t).getDefault();
				}
			}
			if(t instanceof DoubleType) {
				if(!(((DoubleType) t).hasNullDefault())) {
					out = ""+((DoubleType) t).getDefault();
				}
			}
			if(t instanceof DecimalType) {
				if(!(((DecimalType) t).hasNullDefault())) {
					out = ""+((DecimalType) t).getDefault();
				}
			}
			if(t instanceof TextualType) {
				out = ""+((TextualType) t).getDefault();
			} 
		}
		return out;
	}
 */
	
	public static Object getDBValue(SQLResult rs, int colnr, EmbeddedTableHandling env, String colname) throws Exception {
		Column col = env.ths.getCreateStatement().getColumn(colname);
		if(col == null) {
			throw new Exception("Could not find column from name "+colname+" in table "+env.getObjectName());
		}
		Type t = col.getType();
		if(t instanceof IntType) {
			String s = rs.getString(colnr+1);
			if(s == null) {
				return null;
			}
			return rs.getInt(colnr+1);
		}
		if(t instanceof TextualType) {
			return rs.getString(colnr+1);
		}
		if(t instanceof DoubleType) {
			String s = rs.getString(colnr+1);
			if(s == null) {
				return null;
			}
			return rs.getDouble(colnr+1);
		}
		if(t instanceof DecimalType) {
			String s = rs.getString(colnr+1);
			if(s == null) {
				return null;
			}
			return rs.getDouble(colnr+1);
		}
		if(t instanceof DatetimeType) {
			String s = rs.getString(colnr+1);
			if(s == null) {
				return null;
			}
			return (java.sql.Date) rs.getDate(colnr+1);
		}
		if(t instanceof LobType) {
			return new LobValue();
		}
		if(t instanceof UnmanageableType) {
			UnmanageableType ut = (UnmanageableType) t;
			return new UnmanagedValue(ut);
			//throw new Exception("Unable to handle type "+ut.getTypename());
		}
		throw new Exception("Unable to handle type "+t.getClass().getName());
	}
	
	public static Object getDBValue(Column col, String textualValue) throws Exception {
		Type t = col.getType();
		if(t instanceof IntType) {
			return Integer.parseInt(textualValue);
		}
		if(t instanceof TextualType) {
			return textualValue;
		}
		if(t instanceof DoubleType) {
			return Double.parseDouble(textualValue);
		}
		if(t instanceof DecimalType) {
			return Double.parseDouble(textualValue);
		}
		if(t instanceof DatetimeType) {
			return CommonSQLAssembler.getDate(textualValue);
		}
		if(t instanceof UnmanageableType) {
			UnmanageableType ut = (UnmanageableType) t;
			return new UnmanagedValue(ut);
			//throw new Exception("Unable to handle type "+ut.getTypename());
		}
		throw new Exception("Unable to handle type "+t.getClass().getName());
	}
	
	public static boolean typeCorrect(Column col, Object value) {
		Type t = col.getType();
		if((value == null) && (!t.getNotNull())) {
			return true;
		}
		if((t instanceof IntType) && (value instanceof Integer)) {
			return true;
		}
		if((t instanceof TextualType) && (value instanceof String)) {
			return true;
		}
		if((t instanceof DoubleType) && (value instanceof Double)) {
			return true;
		}
		if((t instanceof DoubleType) && (value instanceof Integer)) {
			return true;
		}
		if((t instanceof DecimalType) && (value instanceof Double)) {
			return true;
		}
		if((t instanceof DatetimeType) && (value instanceof java.sql.Date)) {
			return true;
		}
		return false;
	}

}
