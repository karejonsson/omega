package org.omega.connectivity.helpers;

import java.util.Vector;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.model.TableConfiguration;

import se.modlab.generics.bshro.fs.DiskFilesystemFile;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserRuntimeError;

public class OmegaEmbeddedConfig {

	public static ModelConfiguration getConfig(String gdbappconfig, String encoding, boolean talkative) throws IntolerableException {
		HierarchyLeaf hl = null;
		try {
			hl = DiskFilesystemFile.getHierarchyLeaf(gdbappconfig);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new UserRuntimeError(e.getMessage(), e);
		}
		return getConfig(hl, encoding, talkative);
	}
		
	private static ModelConfiguration getConfig(HierarchyLeaf hl, String encoding, boolean talkative) throws IntolerableException {
		ModelConfiguration hs = ModelOrderCalculator.configTableOrderCalcule_ChosenEncoding(hl, talkative, encoding);
		Vector<TableConfiguration> tablesWithoutLevel = hs.getTablehandlersWithoutLevel();
		if(tablesWithoutLevel.size() != 0) {
			StringBuffer sb = new StringBuffer();
			for(TableConfiguration table : tablesWithoutLevel) {
				sb.append(table.getTableName()+"\n");
			}
			throw new UserCompiletimeError("Constraint level not determinable for:\n"+sb.toString());
		}
		return hs;
	}

}
