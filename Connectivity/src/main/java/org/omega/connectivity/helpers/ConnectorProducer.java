package org.omega.connectivity.helpers;

import org.omega.connectivity.abstraction.EmbeddedModelHandling;

public interface ConnectorProducer {
	public EmbeddedModelHandling getOmegaDetails() throws Exception;
}

