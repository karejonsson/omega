package org.omega.connectivity.helpers;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import se.modlab.generics.bshro.fs.DiskFilesystemHierarchy;
import se.modlab.generics.bshro.ifc.HierarchyBranch;
import se.modlab.generics.bshro.ifc.HierarchyLeaf;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UnclassedError;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.parse.OmegaConfigParser;

public class ModelOrderCalculator {

	public static ModelConfiguration configTableOrderCalcule_NativeEncoding(File f, boolean shutup) throws IntolerableException {
		File p = f.getParentFile();
		DiskFilesystemHierarchy dfsh = new DiskFilesystemHierarchy("root", null, p);
		final HierarchyLeaf hl = (HierarchyLeaf) dfsh.getChild(f.getName());
		//String x = f.getParentFile().getAbsolutePath();
		try {
			return configTableOrderCalcule_NativeEncoding(dfsh, hl.getName(), hl.getInputStream(), shutup);
		}
		catch(Exception ioe) {
			throw new SystemError("Error creating input stream from "+f, ioe);
		}
	}

	public static ModelConfiguration configTableOrderCalcule_ChosenEncoding(HierarchyLeaf hl, boolean shutup, String encoding) throws IntolerableException {
		//String x = f.getParentFile().getAbsolutePath();
		try {
			return configTableOrderCalcule_ChosenEncoding(hl.getParent(), hl.getName(), hl.getInputStream(), shutup, encoding);
		}
		catch(Exception ioe) {
			ioe.printStackTrace();
			throw new SystemError("Error creating input stream from "+hl.getPath(), ioe);
		}
	}

	public static ModelConfiguration configTableOrderCalcule_NativeEncoding(
			HierarchyBranch hb, 
			String filename, 
			InputStream is, 
			boolean shutup) throws IntolerableException {
		return configTableOrderCalcule_ChosenEncoding(
				hb, 
				filename, 
				is, 
				shutup,
				System.getProperty("file.encoding"));
	}
		
	private static ModelConfiguration configTableOrderCalcule_ChosenEncoding(
			HierarchyBranch hb, 
			String filename, 
			InputStream is, 
			boolean shutup,
			String encoding) throws IntolerableException {
		ModelConfiguration hs = new ModelConfiguration();
		StringBuffer em = new StringBuffer();
		try 
		{
			HierarchyObject.setReferenceFile(hb);
			InputStreamReader isr = new InputStreamReader(is, encoding);
			OmegaConfigParser.parseOmegaConfig(hs, isr, "File "+hb.getPath());
			hs.setFileSystemReferenceCatalog(hb);
			hs.performTableOrderCalcule(shutup, em);
		}
		catch(IntolerableException ie) { 
			throw ie;
		}
		catch(NullPointerException npe) {
			IntolerableException ie = new UnclassedError("Table dependency order calcule failed due to inconsistencies.\n"+em, npe);
			throw ie;
		}
		catch(Exception _e) {
			_e.printStackTrace();
			IntolerableException ie = new UnclassedError("Table dependency order calcule failed.\n"+em, _e);
			throw ie;
		}
		catch(Throwable _e) {
			_e.printStackTrace();
			IntolerableException ie = new UnclassedError("Table dependency order calcule failed miserably.\n"+em, _e);
			throw ie;
		}
		return hs;
	}
	
}
