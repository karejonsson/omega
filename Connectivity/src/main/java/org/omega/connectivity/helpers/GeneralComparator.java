package org.omega.connectivity.helpers;

import java.util.Comparator;

public class GeneralComparator implements Comparator<String> {

	public int compare(String s1, String s2) {
		if(s1 == null) {
			return 1;
		}
		if(s2 == null) {
			return -1;
		}
		try {
			long l1 = Long.parseLong(s1);
			long l2 = Long.parseLong(s2);
			return l1 > l2 ? 1 : -1;
		}
		catch(Exception e) {
		}
		try {
			double d1 = Double.parseDouble(s1);
			double d2 = Double.parseDouble(s2);
			return d1 > d2 ? 1 : -1;
		}
		catch(Exception e) {
		}
		return s1.compareTo(s2);
	}

}
