package org.omega.connectivity.recreatesql;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.struct.BlobNumeratedType;
import org.omega.configuration.struct.BlobType;
import org.omega.configuration.struct.ClobNumeratedType;
import org.omega.configuration.struct.ClobType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.LongBlobType;
import org.omega.configuration.struct.MediumBlobType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.Parameter;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.TinyintType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;

import se.modlab.generics.crypto.MD5State;
import se.modlab.generics.crypto.Services;

public class OracleRecreator extends CommonSQLAssembler {
	
	private MSSQLServerRecreator piggyback = new MSSQLServerRecreator();

	public static final String symbol = "Oracle";
	
	public OracleRecreator() {
		super(symbol);
	}

	private String get_AutoIncrementPart(CreateStatement cs, Column col) {
		StringBuffer sb = new StringBuffer();
		sb.append("\n\nCREATE SEQUENCE "+cs.getName()+"_"+col.getName()+"_seq; \n");
		sb.append("CREATE OR REPLACE TRIGGER "+cs.getName()+"_trg \n");
		sb.append("BEFORE INSERT ON "+cs.getName()+" \n");
		sb.append("FOR EACH ROW \n");
		//sb.append("WHEN (new."+col.getName()+" IS NULL) \n");
		sb.append("BEGIN \n");
		sb.append("  SELECT "+cs.getName()+"_"+col.getName()+"_seq.nextval INTO :new."+col.getName()+" FROM dual; \n");
		//sb.append("  SELECT "+cs.getName()+"_seq.NEXTVAL \n");
		//sb.append("  INTO   :new."+col.getName()+" \n");
		//sb.append("  FROM   dual; \n");
		sb.append("END; \n/");
		// CREATE SEQUENCE dept_seq;
		/*
CREATE OR REPLACE TRIGGER dept_bir 
BEFORE INSERT ON departments 
FOR EACH ROW
WHEN (new.id IS NULL)
BEGIN
  SELECT dept_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
		 */
		return sb.toString();
	}
	
	private static String correctForInitiationBadCharacter(String in, CreateStatement cs) {

        if(in.length() <= 30) {
            if(in.startsWith("_")) {
            	in = "A"+in.substring(1);
            }
			return in;
		}

        MD5State internal_md5 = new MD5State();
        internal_md5.update(cs.getName());
        byte md5sum[] = internal_md5.digest();
        
        if(in.length() > 30) {
        	in = in.substring(in.length()-30);
        }

        String md5checksum = Services.toDenseHexString(md5sum);
        String out = md5checksum.substring(0, 10)+in.substring(10);
        System.out.println("MD5 "+in+" -> "+out);
        return out;
	}
	
	private String getSequenceName(CreateStatement cs, Column col) {
		String seqname = cs.getName().toUpperCase()+"_"+col.getName().toUpperCase()+"_SEQ";
		if(seqname.length() <= 30) {
			return seqname;
		}
		seqname = seqname.substring(Math.max(seqname.length()-30, 0));
		return correctForInitiationBadCharacter(seqname, cs);
	}
	
	private String getTriggerName(CreateStatement cs, Column col) {
		String trgname = cs.getName().toUpperCase()+"_TRG";
		if(trgname.length() <= 30) {
			return trgname;
		}
		trgname = trgname.substring(Math.max(trgname.length()-30, 0));
		return correctForInitiationBadCharacter(trgname, cs);
	}
	
	private String getSequencePart(CreateStatement cs, Column col) {
		String seqname = getSequenceName(cs, col);
		return "CREATE SEQUENCE "+seqname+" START WITH 1 INCREMENT BY 1";
	}
	
	private String getTriggerPart(CreateStatement cs, Column col) {
		String seqname = getSequenceName(cs, col);
		String trgname = getTriggerName(cs, col);
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE OR REPLACE TRIGGER "+trgname+" \n");
		sb.append("BEFORE INSERT ON "+getTablesName(cs)+" \n");
		sb.append("FOR EACH ROW \n");
		sb.append("BEGIN \n");
		sb.append("  SELECT "+seqname+".nextval INTO :new."+getColumnsName(col, cs).toUpperCase()+" FROM dual; \n");
		sb.append("END;");
		return sb.toString();
	}
	
	private String _getTriggerPart(CreateStatement cs, Column col) {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE OR REPLACE TRIGGER "+cs.getName().toUpperCase()+"_TRG ");
		sb.append("BEFORE INSERT ON "+cs.getName().toUpperCase()+" ");
		sb.append("FOR EACH ROW ");
		sb.append("BEGIN ");
		sb.append("  SELECT "+cs.getName().toUpperCase()+"_"+col.getName().toUpperCase()+"_SEQ.nextval INTO :new."+col.getName().toUpperCase()+" FROM dual; ");
		sb.append("END;");
		return sb.toString();
	}
	
	public String[] create(CreateStatement cs) {
		StringBuffer sb = new StringBuffer();
		String sequencePart = null;
		String triggerPart = null;
		sb.append("CREATE TABLE "+getTablesName(cs)+" (\n");
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			if(i != 0) {
				sb.append(",\n");
			}
			Column col = cs.getColumn(i);
			String columnsType = columnType(col);
			sb.append(columnsType);
			Type t = col.getType();
			if(cs.getFirstForeignKeyDeclarationFromMember(col.getName()) != null) {
				if(t instanceof NumericType) {
					NumericType nt = (NumericType) t;
					if(!nt.isUnsigned()) { // Not done above
						//if(!unsignedgenerated) {
							//sb.append(" unsigned");
						//}
					}
					if(nt.hasNullDefault()) { // Not done above
						if(!columnsType.contains(" default null")) { //defaultnullgenerated) {
							//defaultnullgenerated = true;
							sb.append(" default null");
						}
					}
				}
				
			}
			if(col.isAutoIncrement()) {
				sequencePart = getSequencePart(cs, col);
				triggerPart = getTriggerPart(cs, col);
			}
			PrimaryKeyDeclaration pk = cs.getPrimaryKeyDeclaration();
			if(pk != null) {
				if(pk.hasMember(col.getName())) {
					sb.append(" PRIMARY KEY");
				}
			}
		}
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			sb.append(",\n");
			ForeignKeyDeclaration fkd = cs.getForeignKeyDeclaration(i);
			StringBuffer fknames = new StringBuffer();
			fknames.append(fkd.getReferringMember(0));
			for(int ii = 1 ; ii < fkd.getNoReferringMembers() ; ii++) {
				fknames.append(", "+fkd.getReferringMember(ii));
			}
			StringBuffer pknames = new StringBuffer();
			pknames.append(fkd.getReferredTableMember(0));
			for(int ii = 1 ; ii < fkd.getNoReferredTableMembers() ; ii++) {
				pknames.append(", "+fkd.getReferredTableMember(ii));
			}
			String conname = getShortEnoughReplacementOfConstraintsName(fkd.getConstraintname(), fknames.toString(), cs);
			sb.append("   constraint "+conname+" foreign key ("+fknames.toString()+") references "+fkd.getReferencedTablename()+" ("+pknames.toString()+")");
		}
		sb.append("\n)");
		for(int i = 0 ; i < cs.getNoParameters() ; i++) {
			Parameter p = cs.getParameter(i);
			sb.append(" "+p.getName()+"="+p.getValue());
		}
		if(triggerPart == null) {
			return new String[] { sb.toString() };
		}
		else {
			return new String[] { sb.toString(), sequencePart, triggerPart };
		}
	}
	
	private static String getShortEnoughReplacementOfConstraintsName(String conname, String fknames, CreateStatement cs) {
		String out = null;
		if(conname != null) {
			out = conname;
		}
		else {
			out = cs.getName()+"_"+fknames.replaceAll(",", "_")+"_con".replaceAll(" ","");
		}
		// seqname.substring(Math.max(seqname.length()-30, 0));
		return correctForInitiationBadCharacter(out.substring(Math.max(out.length()-30, 0)), cs);
	}
	
	private String translateTypename(Type t) {
		String typename = t.getTypename().replaceAll("datetime", "date");
		//String toPublish = typename;
		int idx = typename.lastIndexOf("(");
		if((idx != -1) && (t instanceof IntType)) {
			return typename.substring(0, idx);
		}
		if(t instanceof TextType) { 
			return "varchar(2000)";
		}
		if(t instanceof TinytextType) { 
			return "varchar(20)";
		}
		if(t instanceof TinyintType) { 
			return "number(1)";
		}
		if(t instanceof LobType) {
			if(t instanceof BlobNumeratedType) {
				BlobNumeratedType bnt = (BlobNumeratedType) t;
				return bnt.getTypename()+"("+bnt.getNumeration()+")";
			}
			if(t instanceof BlobType) {
				BlobType bt = (BlobType) t;
				return bt.getTypename();
			}
			if(t instanceof ClobNumeratedType) {
				ClobNumeratedType cnt = (ClobNumeratedType) t;
				return cnt.getTypename()+"("+cnt.getNumeration()+")";
			}
			if(t instanceof ClobType) {
				ClobType ct = (ClobType) t;
				return ct.getTypename();
			}
			if(t instanceof MediumBlobType) {
				return "mediumblob";
			}
			if(t instanceof LongBlobType) {
				return "longblob";
			}
		}
		return typename;
	}
	
	public String columnType(Column col) {
		Type t = col.getType();
		if(t instanceof DecimalType) {
			DecimalType dt = (DecimalType) t;
			if(!dt.hasPrecision()) {
				dt.setPrecision(10);
				dt.setDecimals(3);
			}
		}
		if(t instanceof DoubleType) {
			DoubleType dt = (DoubleType) t;
			if(!dt.hasPrecision()) {
				dt.setPrecision(10);
				dt.setDecimals(3);
			}
		}
		//String typename = t.getTypename().replaceAll("datetime", "date");
		String toPublish = translateTypename(t);
		StringBuffer sb = new StringBuffer();
		sb.append("   "+col.getName()+" "+toPublish.replaceAll("double", "decimal"));
		boolean unsignedgenerated = false;
		boolean defaultgenerated = false;
		if(t instanceof NumericType) {
			NumericType nt = (NumericType) t;
			if(nt.isUnsigned()) {
				unsignedgenerated = true;
				//sb.append(" unsigned");
			}
			if(nt.getZerofill()) {
				//sb.append(" zerofill");
			}
			if(nt.hasNullDefault()) {
				if(!defaultgenerated) {
					defaultgenerated = true;
					sb.append(" default null");
				}
			}
			if(nt.getNotNull()) {
				sb.append(" not null");
			}
			if(nt.hasDefault() && !(nt.hasNullDefault() || nt.getNotNull())) {
				if(!defaultgenerated) {
					defaultgenerated = true;
					if(nt instanceof IntType) {
						sb.append(" default "+((IntType) nt).getDefault()+"");
					}
					if(nt instanceof DoubleType) {
						sb.append(" default "+((DoubleType) nt).getDefault()+"");
					}
					if(nt instanceof DecimalType) {
						sb.append(" default "+((DecimalType) nt).getDefault()+"");
					}
				}
			}
		}
		if(t instanceof TextualType) {
			TextualType tt = (TextualType) t;
			if(tt.getNotNull()) {
				sb.append(" not null");
			}
			if(tt.hasDefault()) {
				if(!defaultgenerated) {
					defaultgenerated = true;
					sb.append(" default '"+tt.getDefault()+"'");
				}
			}
			if(tt.hasNullDefault()) {
				if(!defaultgenerated) {
					defaultgenerated = true;
					sb.append(" default null");
				}
			}
		}
		return sb.toString();
	}
	
	public String deleteColumn(CreateStatement cs, Column col) {
		return piggyback.deleteColumn(cs, col);
	}
	
	public String addColumn(CreateStatement cs, Column col) {
		return piggyback.addColumn(cs, col);
	}

	public String changeColumn(CreateStatement cs, Column col) {
		return piggyback.changeColumn(cs, col);
	}
	
	public String formDate(String default_) throws Exception {
		return piggyback.formDate(default_);
	}
	
	public boolean isIntegrityConstraintViolation(SQLException sqle) {
		return sqle.getMessage().contains("ORA-00001");
	}

	public boolean isUniqueConstraintViolation(Exception e) {
		if(!(e instanceof SQLException)) {
			return false;
		}
		return e.getMessage().contains("ORA-00001");
	}
	
	public String getMatchingOperator() {
		return piggyback.getMatchingOperator();
	}

	public String getWildcard() {
		return piggyback.getWildcard();
	}
	
	private String removeTrailingSemicolon(String sql) {
		if(sql.endsWith(";")) {
			return sql.substring(0, sql.length()-1);
		}
		return sql;
	}

	public String selectAll(CreateStatement cs) {
		return removeTrailingSemicolon(super.selectAll(cs));
	}

	public String[] dropTable(CreateStatement cs) {
		Vector<String> droppers = new Vector<String>();
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			if(col.isAutoIncrement()) {
				String triggerName = getTriggerName(cs, col);
				droppers.addElement("drop trigger "+triggerName+";");
				String sequenceName = getSequenceName(cs, col);
				droppers.addElement("drop sequence "+sequenceName+";");
			}
		}
		String dropper[] = super.dropTable(cs);
		droppers.addElement(dropper[0]);
		String out[] = new String[droppers.size()];
		for(int i = 0 ; i < out.length; i++) {
			out[i] = droppers.elementAt(i);
		}
		return out;
	}

	public String getInsert(CreateStatement cs, Hashtable<String, Object> ht)  throws Exception {
		String insert = removeTrailingSemicolon(super.getInsert(cs, ht));
		return insert;
	}
	
	public String selectIndexed(CreateStatement cs, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, pk));
	}

	public String selectIndexed(CreateStatement cs, ForeignKey fk) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, fk));
	}

	public String selectIndexed(CreateStatement cs, Key k) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, k));
	}

	public String selectSought(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		return removeTrailingSemicolon(super.selectSought(cs, ht));
	}

	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		return removeTrailingSemicolon(super.getUpdate(cs, ht));
	}
	
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.getUpdate(cs, ht, pk));
	}
	
	public String getDelete(CreateStatement cs, int key) throws Exception {
		return removeTrailingSemicolon(super.getDelete(cs, key));
	}

	public String getDelete(CreateStatement cs, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.getDelete(cs, pk));
	}
	
	public String tableLength(String tablename, CreateStatement cs) {
		return removeTrailingSemicolon(super.tableLength(tablename, cs));
	}
	
	public String getNameCut(String name, CreateStatement cs) {
		if(name.length() <= 30) {
			return correctForInitiationBadCharacter(name, cs);
		}
		String out = name.substring(0, 30);
		return correctForInitiationBadCharacter(out, cs);
	}

	public String[] getCurrentDateMacros() {
		return new String[] { "SYSDATE", "SYSDATE()", "CURRENT_DATE" };
	}
	
}
