package org.omega.connectivity.recreatesql;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.Type;

public abstract class CommonSQLAssembler implements Recreator {
	
	private String dbname = null;
	
	protected CommonSQLAssembler(String _dbsName) {
		_dbsName = dbname;
	}
	
	public String getDBsName() {
		return dbname;
	}

	public String selectAll(CreateStatement cs) {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+";";
	}
	
	public final static String formats[] = {
		"yyyy/MM/dd HH:mm:ss",
		"yyyy/MM/dd HH:mm",
		"yyyy/MM/dd HHmm",
		"yyyy/MM/dd HH",
		"yyyy/MM/dd",
		"yyyy-MM-dd HH:mm:ss", 
		"yyyy-MM-dd HH:mm",
		"yyyy-MM-dd HHmm",
		"yyyy-MM-dd HH",
		"yyyy-MM-dd",
		"yyyyMMdd HH:mm:ss",
		"yyyyMMdd HH:mm",
		"yyyyMMdd HHmm",
		"yyyyMMdd HH",
		"yyyyMMdd", 
		"yy/MM/dd HH:mm:ss",
		"yy/MM/dd HH:mm",
		"yy/MM/dd HHmm",
		"yy/MM/dd HH",
		"yy/MM/dd",
		"yy-MM-dd HH:mm:ss", 
		"yy-MM-dd HH:mm",
		"yy-MM-dd HHmm",
		"yy-MM-dd HH",
		"yy-MM-dd",
		"yyMMdd HH:mm:ss",
		"yyMMdd HH:mm",
		"yyMMdd HHmm",
		"yyMMdd HH",
		"yyMMdd", 
	};
	
	public static void main(String args[]) throws Exception {
		String s = "2010-01-01";
		System.out.println(s+" -> "+getDate(s));
	}
	
	public static String createDate(String s, String format) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = sdf.parse(s);
		java.sql.Date sqldate = new java.sql.Date(date.getTime());
		return sqldate.toString();
	}

	public static String getDate(String s) throws Exception {
		if(s == null) {
			return getDate(new Date());
		}
		if(s.trim().length() == 0) {
			return null;
		}
		/*

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
			Date dt = new Date();
			String S = sdf.format(dt); // formats to 09/23/2009 13:53:28.238
			Date dt2 = sdf.parse(S); // parses back
			System.out.println("S "+S);
			java.sql.Date sqldate = new java.sql.Date(dt2.getTime());
			System.out.println("'"+sqldate.toString()+"'");

		 
		 */
		String ss = s.trim();
		for(int i = 0 ; i < formats.length ; i++) {
			try { return createDate(ss, formats[i]); } catch(Exception e) {}
		}
		if(ss.startsWith("(")) {
			ss = ss.substring(1);
		}
		if(ss.startsWith("{")) {
			ss = ss.substring(1);
		}
		if(ss.startsWith("[")) {
			ss = ss.substring(1);
		}
		if(ss.endsWith(")")) {
			ss = ss.substring(0, ss.length()-1);
		}
		if(ss.endsWith("}")) {
			ss = ss.substring(0, ss.length()-1);
		}
		if(ss.endsWith("]")) {
			ss = ss.substring(0, ss.length()-1);
		}
		ss = ss.replaceAll("'", "");
		for(int i = 0 ; i < formats.length ; i++) {
			
			try { return createDate(ss, formats[i]); } catch(Exception e) {}
		}
		Exception e = new Exception("Unable to make a date from '"+s+"'");
		e.printStackTrace();
		throw e;
	}
	
	public static java.sql.Date getSQL_ObjectForDate(String s) throws Exception {
		if(s == null) {
			return getSQL_ObjectForDate(new Date());
		}
		if(s.trim().length() == 0) {
			return null;
		}
		SimpleDateFormat sdf = null;
		for(int i = 0 ; i < formats.length ; i++) {
			try {
				String format = formats[i];
				sdf = new SimpleDateFormat(format);
				Date date = sdf.parse(s);
				return new java.sql.Date(date.getTime());
			}
			catch(Exception e) {
				
			}
		}
		throw new Exception("Unable to make a date from '"+s+"'");
	}
	
	public static Date getDateFromString(String s) {
		SimpleDateFormat sdf = null;
		for(int i = 0 ; i < formats.length ; i++) {
			try {
				String format = formats[i];
				sdf = new SimpleDateFormat(format);
				Date date = sdf.parse(s);
				return date;
			}
			catch(Exception e) {
				
			}
		}
		return null;
	}
	
	public static String getDate(Date date) throws Exception {
		if(date == null) {
			date = new Date();
		}
		//for(int i = 0 ; i < formats.length ; i++) {
			try {
				java.sql.Date sqldate = new java.sql.Date(date.getTime());
				return sqldate.toString();
			}
			catch(Exception e) {
				
			}
		//}
		throw new Exception("Unable to make a date from '"+date+"'");
	}
	
	
	public static java.sql.Date getSQL_ObjectForDate(Date date) throws Exception {
		if(date == null) {
			date = new Date();
		}
		for(int i = 0 ; i < formats.length ; i++) {
			try {
				return new java.sql.Date(date.getTime());
			}
			catch(Exception e) {
			}
		}
		throw new Exception("Unable to make a date from '"+date+"'");
	}
	
	public String[] dropTable(CreateStatement cs) {
		return new String[] { "drop table "+getTablesName(cs)+";" };
	}
	
	
	// insert into texts (text ) values ('Nisse Hult f�rst');
	public String getInsert(CreateStatement cs, Hashtable<String, Object> ht)  throws Exception {
		StringBuffer titles = new StringBuffer();
		StringBuffer values = new StringBuffer();
		Enumeration<String> coltitles = ht.keys();
		int ctr = 0;
		while(coltitles.hasMoreElements()) {
			String title = coltitles.nextElement();
			String titleCut = getNameCut(title, cs);
			if(!cs.hasColumn(title)) {
				throw new Exception("Table "+cs.getName()+" does not have columns title "+title);
			}
			Object obj = ht.get(title);
			Column col = cs.getColumn(title);
			if(col.isAutoIncrement()) { // cs.isPartOfPrimaryKeyDeclaration(col)
				/*
				 * isAutoIncrement is the right choice when primary keys are unique by application logic. SVK 201305
				 */
				continue; // Cannot set auto incremented stuff
			}
			// Analysera numeric / textual och l�gg till
			// Not below that LobTypes shall not be included. 
			Type t = col.getType();
			if(t instanceof NumericType) {
				if(obj != null) {
					if(ctr != 0) {
						titles.append(", ");
						values.append(", ");
					}
					titles.append(titleCut);
					values.append(obj.toString());
					ctr++;
				}
				
			} 
			else if(t instanceof TextualType) {
				if(obj != null) {
					if(ctr != 0) {
						titles.append(", ");
						values.append(", ");
					}
					titles.append(titleCut);
					values.append("'"+obj.toString()+"'");
					ctr++;
				}
			}
			else if(t instanceof DatetimeType) {
				if(obj != null) {
					String after = null;//formDate(obj.toString());
					if(obj instanceof String) {
						//System.out.println("CommonSQLAssembler.getInsert type is String");
						after = formDate(obj.toString());
					}
					if(obj instanceof Date) {
						//System.out.println("CommonSQLAssembler.getInsert type is Date");
						after = formDate(CommonSQLAssembler.getDate((Date) obj));
					}
					//System.out.println("CommonSQLAssembler.insertSQL "+obj.toString()+" -> "+after);
					String out = after;
					if(out != null) {
						if(ctr != 0) {
							titles.append(", ");
							values.append(", ");
						}
						titles.append(titleCut);
						values.append("'"+out+"'");
						ctr++;
					}
				}
			}
			else if(t instanceof LobType) {
				// Not Lob types in insert SQL. That is for separate upload
			}
			else {
				throw new Exception("Internal error, Class "+t.getClass().getName()+" not handled in CommonSQLAssembler");
			}
		}
		return "insert into "+getTablesName(cs)+" ("+titles.toString()+") values ("+values.toString()+");";
	}

	private String allNonBlobMembersCommaSeparated(CreateStatement cs) {
		String[] nonBlobMembers = allNonBlobMembers(cs);
		if(nonBlobMembers == null) {
			return null;
		}
		if(nonBlobMembers.length == 0) {
			return "";
		}
		StringBuffer sb = new StringBuffer(nonBlobMembers[0]);
		for(int i = 1 ; i < nonBlobMembers.length ; i++) {
			sb.append(", "+nonBlobMembers[i]);
		}
		return sb.toString();
	}

	public String[] allNonBlobMembers(CreateStatement cs) {
		String[] out_work = new String[cs.getNoColumns()];
		int ctr = 0;
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			Column col = cs.getColumn(i);
			Type ct = col.getType();
			if(ct instanceof LobType) {
				continue;
			}
			out_work[ctr++] = getColumnsName(cs.getColumn(i), cs);
		}
		String[] out = new String[ctr];
		System.arraycopy(out_work, 0, out, 0, ctr) ;
		return out;
	}
	
	public String selectIndexed(CreateStatement cs, PrimaryKey pk) throws Exception {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+" "+whereClause(cs, pk);
	}

	public String selectIndexed(CreateStatement cs, ForeignKey fk) throws Exception {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+" "+whereClause(cs, fk);
	}

	public String selectIndexed(CreateStatement cs, Key k) throws Exception {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+" "+whereClause(cs, k);
	}

	/*
	public String selectIndexed(CreateStatement cs, String member, int value) throws Exception { 
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+" where "+member+" = "+value+";";
	}

	public String selectIndexed(CreateStatement cs, String member, Date d) throws Exception {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+getTablesName(cs)+" where "+member+" = '"+formDate(getDate(d))+"';";
	}
	*/
	
	public String selectSought(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		return "select "+allNonBlobMembersCommaSeparated(cs)+" from "+
				getTablesName(cs)+" "+
				whereSearchClause(cs, ht);
	}
	
	/*
	public String selectAllIndexed(Hashtable<String, Object> criterion, CreateStatement cs) throws Exception {
		StringBuffer sb = new StringBuffer("select "+allMembers(cs)+" from "+getTablesName(cs)+" where ");
		boolean first = true;
		int ctr = 0; 
		//CreateStatement cs = null;
		//appendAsTypeRequires(CreateStatement cs, sb, ctr, key, obj, " and ");
		for(String key : criterion.keySet()) {
			Object obj = criterion.get(key);
			appendAsTypeRequires(cs, sb, ctr, key, obj, " and ", false);
			ctr++;
		}
		sb.append(";");
		return sb.toString();
	}
	
	public String selectIndexed(String members[], Hashtable<String, Object> criterion, CreateStatement cs) throws Exception {
		String allSql = selectAllIndexed(criterion, cs);
		StringBuffer sb = new StringBuffer();
		sb.append(members[0]);
		for(int i = 1 ; i < members.length ; i++) {
			sb.append(", "+members[i]);
		}
		return "select "+sb.toString()+allSql.substring(8);
	}*/
/*
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht, int key) throws Exception {
		StringBuffer updates = new StringBuffer();
		Enumeration<String> coltitles = ht.keys();
		int ctr = 0;
		while(coltitles.hasMoreElements()) {
			String title = coltitles.nextElement();
			if(!cs.hasColumn(title)) {
				throw new Exception("Table "+getTablesName(cs)+" does not have columns title "+title);
			}
			Object obj = ht.get(title);
			if(obj == null) {
				continue; // The value may not be necessary
			}
			Column col = cs.getColumn(title);
			if(col.isAutoIncrement()) {
				continue; // Cannot set auto incremented stuff
			}
			// Analysera numeric / textual och l�gg till
			Type t = col.getType();
			if(t instanceof NumericType) {
				if((obj != null) && !(obj instanceof NullValue)) {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(title+" = "+obj.toString());
					ctr++;
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(title+" = NULL");
					ctr++;
				}
			} 
			else if(t instanceof TextualType) {
				if((obj != null) && !(obj instanceof NullValue))  {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(title+" = "+"'"+obj.toString()+"'");
					ctr++;
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(title+" = NULL");
					ctr++;
				}
			}
			else if(t instanceof DatetimeType) {
				if((obj != null) && !(obj instanceof NullValue))  {
 					String after = null;//formDate(obj.toString());
					if(obj instanceof String) {
						//System.out.println("CommonSQLAssembler.getUpdate type is String");
						after = formDate(obj.toString());
					}
					if(obj instanceof Date) {
						//System.out.println("CommonSQLAssembler.getUpdate type is Date");
						after = formDate(CommonSQLAssembler.getDate((Date) obj));
					}
					//System.out.println("CommonSQLAssembler.insertSQL "+obj.toString()+" -> "+after);
					String out = after;

					//String out = formDate(obj.toString());
					if(out != null) {
						if(ctr != 0) {
							updates.append(", ");
						}
						updates.append(title+" = "+"'"+out+"'");
						ctr++;
					}
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(title+" = NULL");
					ctr++;
				}
			}
			else {
				throw new Exception("Internal error, Class "+t.getClass().getName()+" not handled in CommonSQLAssembler");
			}
		}
		Column col = cs.getAutoIncrementColumn();
		if(col == null) {
			throw new Exception("Internal error, table "+getTablesName(cs)+" has no identifier.");
		}
		String wherecondition = "where "+getColumnsName(col)+" = "+key;
		return "update "+getTablesName(cs)+" set "+updates.toString()+" "+wherecondition+";";
	}
*/
	
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht, PrimaryKey pk) throws Exception {
		StringBuffer updates = new StringBuffer();
		Enumeration<String> coltitles = ht.keys();
		int ctr = 0;
		while(coltitles.hasMoreElements()) {
			String title = coltitles.nextElement();
			String titleCut = getNameCut(title, cs);
			if(!cs.hasColumn(title)) {
				throw new Exception("Table "+cs.getName()+" does not have columns title "+title);
			}
			Object obj = ht.get(title);
			if(obj == null) {
				continue; // The value may not be necessary
			}
			Column col = cs.getColumn(title);
			if(col.isAutoIncrement()) {
				continue; // Cannot set auto incremented stuff
			}
			// Analysera numeric / textual och l�gg till
			Type t = col.getType();
			if(t instanceof NumericType) {
				if(obj != null) {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(titleCut+" = "+obj.toString());
					ctr++;
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(titleCut+" = NULL");
					ctr++;
				}
			} 
			else if(t instanceof TextualType) {
				if(obj != null)  {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(titleCut+" = "+"'"+obj.toString()+"'");
					ctr++;
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(titleCut+" = NULL");
					ctr++;
				}
			}
			else if(t instanceof DatetimeType) {
				if(obj != null)  {
 					String after = null;//formDate(obj.toString());
					if(obj instanceof String) {
						//System.out.println("CommonSQLAssembler.getUpdate type is String");
						after = formDate(obj.toString());
					}
					if(obj instanceof Date) {
						//System.out.println("CommonSQLAssembler.getUpdate type is Date");
						after = formDate(CommonSQLAssembler.getDate((Date) obj));
					}
					//System.out.println("CommonSQLAssembler.insertSQL "+obj.toString()+" -> "+after);
					String out = after;

					//String out = formDate(obj.toString());
					if(out != null) {
						if(ctr != 0) {
							updates.append(", ");
						}
						updates.append(titleCut+" = "+"'"+out+"'");
						ctr++;
					}
				}
				else {
					if(ctr != 0) {
						updates.append(", ");
					}
					updates.append(titleCut+" = NULL");
					ctr++;
				}
			}
			else if(t instanceof LobType) {
				// These shall be uploaded separately. Keep them out of the SQL.
			}
			else {
				throw new Exception("Internal error, Class "+t.getClass().getName()+" not handled in CommonSQLAssembler");
			}
		}
		return "update "+getTablesName(cs)+" set "+updates.toString()+" "+whereClause(cs, pk)+";";
	}
	
	private boolean canUse(Set<String> keyfields, PrimaryKeyDeclaration pk, Hashtable<String, Object> ht) {
		System.out.println("CommonSQLAssembler.canUse - primary key");
		
		// First determine if we have all members
		for(int i = 0 ; i < pk.getNoMembers() ; i++) {
			String pkmember = pk.getMember(i);
			if(ht.get(pkmember) == null) {
				System.out.println("CommonSQLAssembler.canUse false "+pkmember);
				return false;
			}
		}
		
		// We have all members. Assemble them.
		for(int i = 0 ; i < pk.getNoMembers() ; i++) {
			String pkmember = pk.getMember(i);
			keyfields.add(pkmember);
		}
		return true;
	}
	
	
	private boolean canUse(Set<String> keyfieldsNotCut, KeyDeclaration k, Hashtable<String, Object> ht) {
		// First determine if we have all members
		for(int i = 0 ; i < k.getNoMembers() ; i++) {
			String pkmember = k.getMember(i);
			if(ht.get(pkmember) == null) {
				return false;
			}
		}
		
		// We have all members. Assemble them.
		for(int i = 0 ; i < k.getNoMembers() ; i++) {
			String pkmember = k.getMember(i);
			keyfieldsNotCut.add(pkmember);
		}
		return true;
	}
	
	// This method may not be used. If so check if canUse-methods are used.
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		StringBuffer updates = new StringBuffer();
		Set<String> reserveAsKeyFieldsNotCut = new HashSet<String>();
		if(!canUse(reserveAsKeyFieldsNotCut, cs.getPrimaryKeyDeclaration(), ht)) {
			for(int i = 0 ; i < cs.getNoKeys() ; i++) {
				if(canUse(reserveAsKeyFieldsNotCut, cs.getKeyDeclaration(i), ht)) {
					break;
				}
			}
		}
		if(reserveAsKeyFieldsNotCut.size() == 0) {
			throw new Exception("Cannot find a key tuple (Primary key or unique declaration) that can be assembled");
		}
		Enumeration<String> coltitles = ht.keys();
		int ctr = 0;
		while(coltitles.hasMoreElements()) {
			String title = coltitles.nextElement();
			if(!cs.hasColumn(title)) {
				throw new Exception("Table "+cs.getName()+" does not have columns title "+title);
			}
			Object obj = ht.get(title);
			if(obj == null) {
				continue; // The value may not be necessary
			}
			Column col = cs.getColumn(title);
			if(reserveAsKeyFieldsNotCut.contains(col.getName())) {
				continue; // Must reserve this field for the where clause below
			}
			appendAsTypeRequires(cs, updates, ctr, title, obj, " , ", false);
			ctr++;
		}
		return "update "+getTablesName(cs)+" set "+updates.toString()+" "+whereClause(cs, reserveAsKeyFieldsNotCut, ht)+";";
	}
	
	private void appendAsTypeRequires(CreateStatement cs, StringBuffer updates, int ctr, String title, Object obj, String delimiter, boolean manageWildcards) throws Exception {
		appendAsTypeRequires(cs, cs.getColumn(title), updates, ctr, title, obj, delimiter, manageWildcards);
	}
	
	private String getMatchOperator(String value) {
		if(value.contains("*")) {
			return getMatchingOperator();
		}
		return "=";
	}

	private String getMatchValue(String value) {
		if(value.contains("*")) {
			return value.replaceAll("\\*", getWildcard());
		}
		return value;
	}
	
	private void appendAsTypeRequires(CreateStatement cs, Column col, StringBuffer updates, int ctr, String title, Object obj, String delimiter, boolean manageWildcards) throws Exception {
		// Analysera numeric / textual och l�gg till
		//System.out.println("CommonSQLAssembler.appendAsTypeRequires cs = "+cs+", title = "+title+" csname "+getTablesName(cs));
		//Column col = cs.getColumn(title);
		title = getNameCut(title, cs);
		Type t = col.getType();
		if(t instanceof NumericType) {
			if(obj != null) {
				if(ctr != 0) {
					updates.append(delimiter);
				}
				if(manageWildcards) {
					updates.append(title+" "+getMatchOperator(obj.toString())+" "+getMatchValue(obj.toString()));
				}
				else {
					updates.append(title+" = "+obj.toString());
				}
				ctr++;
			}
		} 
		else if(t instanceof TextualType) {
			if(obj != null)  {
				if(ctr != 0) {
					updates.append(delimiter);
				}
				if(manageWildcards) {
					updates.append(title+" "+getMatchOperator(obj.toString())+" '"+getMatchValue(obj.toString())+"'");
				}
				else {
					updates.append(title+" = '"+obj.toString()+"'");
				}
				ctr++;
			}
		}
		else if(t instanceof DatetimeType) {
			if(obj != null)  {
					String after = null;//formDate(obj.toString());
				if(obj instanceof String) {
					//System.out.println("CommonSQLAssembler.getUpdate type is String");
					after = formDate(obj.toString());
				}
				if(obj instanceof Date) {
					//System.out.println("CommonSQLAssembler.getUpdate type is Date");
					after = formDate(CommonSQLAssembler.getDate((Date) obj));
				}
				//System.out.println("CommonSQLAssembler.insertSQL "+obj.toString()+" -> "+after);
				String out = after;

				//String out = formDate(obj.toString());
				if(out != null) {
					if(ctr != 0) {
						updates.append(delimiter);
					}
					if(manageWildcards) {
						updates.append(title+" "+getMatchOperator(out)+" '"+getMatchValue(out)+"'");
					}
					else {
						updates.append(title+" = '"+out+"'");
					}
					ctr++;
				}
			}
		}
		else {
			throw new Exception("Internal error, Class "+t.getClass().getName()+" not handled in CommonSQLAssembler");
		}
	}
	
	private String whereClause(CreateStatement cs, Set<String> reserveAsKeyFields, Hashtable<String, Object> ht) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("where ");
		int ctr = 0;
		for(String keyField : reserveAsKeyFields) {
			appendAsTypeRequires(cs, sb, ctr, keyField, ht.get(keyField), " and ", false);
			ctr++;
		}
		return sb.toString();
	}
	
	private String whereSearchClause(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("where ");
		int ctr = 0;
		for(String keyField : ht.keySet()) {
			appendAsTypeRequires(cs, sb, ctr, keyField, ht.get(keyField), " and ", true);
			ctr++;
		}
		return sb.toString();
	}
	
	private String whereClause(CreateStatement cs, PrimaryKey pk) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("where ");
		int ctr = 0;
		for(String name : pk.getNames()) {
			appendAsTypeRequires(cs, sb, ctr, name, pk.get(name), " and ", false);
			ctr++;
		}
		return sb.toString();
	}
	
	private String whereClause(CreateStatement cs, ForeignKey fk) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("where ");
		int ctr = 0;
		for(String name : fk.getNames()) {
			appendAsTypeRequires(cs, sb, ctr, name, fk.get(name), " and ", false);
			ctr++;
		}
		return sb.toString();
	}
	
	private String whereClause(CreateStatement cs, Key k) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("where ");
		int ctr = 0;
		for(String name : k.getNames()) {
			appendAsTypeRequires(cs, sb, ctr, name, k.get(name), " and ", false);
			ctr++;
		}
		return sb.toString();
	}
	
	public String getDelete(CreateStatement cs, int key) throws Exception {
		Column col = cs.getAutoIncrementColumn();
		if(col == null) {
			throw new Exception("Internal error, table "+cs.getName()+" has no identifier.");
		}
		return "delete from "+getTablesName(cs)+" where "+getColumnsName(col, cs)+" = "+key+";";
	}

	public String getDelete(CreateStatement cs, PrimaryKey pk) throws Exception {
		return "delete from "+getTablesName(cs)+" "+whereClause(cs, pk);
	}
	
	public String tableLength(String tablename, CreateStatement cs) {
		return "select count(*) from "+getNameCut(tablename, cs)+";";
	}
	
	public boolean isConstraintViolation(Exception e) {
		String msgraw = e.getMessage();
		String msgupper = msgraw == null ? "" : e.getMessage().toUpperCase();
		if(msgupper.contains("CONSTRAINT")) {
			if(msgupper.contains("VIOLAT")) {
				return true;
			}
		}
		return false;
	}

	public abstract boolean isUniqueConstraintViolation(Exception e);
	
	protected String getColumnsName(Column col, CreateStatement cs) {
		return getNameCut(col.getName(), cs);
	}

	protected String getTablesName(CreateStatement cs) {
		return getNameCut(cs.getName(), cs);
	}

	public String getNameCut(String name, CreateStatement cs) {
		return name;
	}
	/*
	abstract public boolean isIntegrityConstraintViolation(SQLException sqle);
	abstract public boolean isUniqueConstraintViolation(SQLException sqle);
	abstract public boolean isReferenceConstraintViolation(SQLException sqle);
	*/
	
	public String getUploadBlob(CreateStatement cs, String columnName, PrimaryKey pk) throws Exception {
		return "update "+cs.getName()+" set "+columnName+"=? "+whereClause(cs, pk)+";";
	}
	
	public String getDownloadBlob(CreateStatement cs, String columnName, PrimaryKey pk) throws Exception {
		return "select "+columnName+" from "+cs.getName()+" "+whereClause(cs, pk)+";";
	}
	
}
