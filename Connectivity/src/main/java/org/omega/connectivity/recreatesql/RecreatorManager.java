package org.omega.connectivity.recreatesql;

import org.omega.configuration.recreatesql.Recreator;

public class RecreatorManager {
	
	public static Recreator getRecreator(String name) {
		if(name.toUpperCase().contains(MYSQLRecreator.symbol.toUpperCase())) {
			return new MYSQLRecreator();
		}
		if(name.toUpperCase().contains(OracleRecreator.symbol.toUpperCase())) {
			return new OracleRecreator();
		}
		if(name.toUpperCase().contains(HSQLDBRecreator.symbol.toUpperCase())) {
			return new HSQLDBRecreator();
		}
		if(name.toUpperCase().contains(MSSQLServerRecreator.symbol.toUpperCase())) {
			return new MSSQLServerRecreator();
		}
		if(name.toUpperCase().contains(POSTGRESSQLRecreator.symbol.toUpperCase())) {
			return new POSTGRESSQLRecreator();
		}
		if(name.toUpperCase().contains(IBMDB2Recreator.symbol.toUpperCase())) {
			return new IBMDB2Recreator();
		}
		return null;
	}
	
	public static String[] getNames() {
		return new String[] {
				MYSQLRecreator.symbol,
				OracleRecreator.symbol,
				HSQLDBRecreator.symbol,
				MSSQLServerRecreator.symbol,
				IBMDB2Recreator.symbol,
				POSTGRESSQLRecreator.symbol
		};
	}

}
