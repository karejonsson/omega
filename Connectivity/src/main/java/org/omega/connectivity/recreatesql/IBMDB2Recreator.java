package org.omega.connectivity.recreatesql;

import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import org.omega.configuration.dbreferences.ForeignKey;
import org.omega.configuration.dbreferences.Key;
import org.omega.configuration.dbreferences.PrimaryKey;
import org.omega.configuration.struct.BlobNumeratedType;
import org.omega.configuration.struct.BlobType;
import org.omega.configuration.struct.ClobNumeratedType;
import org.omega.configuration.struct.ClobType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.LongBlobType;
import org.omega.configuration.struct.MediumBlobType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.Parameter;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;

public class IBMDB2Recreator extends CommonSQLAssembler {
	
	public static final String symbol = "IBMDB2";
	
	public IBMDB2Recreator() {
		super(symbol);
	}

	public String[] create(CreateStatement cs) {
		return new String[] { _create(cs) };
	}
 
	public String _create(CreateStatement cs) {
		for(int i = 0 ; i < cs.getNoKeys() ; i++) {
			KeyDeclaration k = cs.getKeyDeclaration(i);
			for(int j = 0 ; j < k.getNoMembers() ; j++) {
				String keymember = k.getMember(j);
				Column col = cs.getColumn(keymember);
				Type t = col.getType();
				t.setNotNull();
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE "+cs.getName()+" ( ");
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			if(i != 0) {
				sb.append(", ");
			}
			Column col = cs.getColumn(i);
			String columnsType = columnType(col);
			sb.append("   "+col.getName()+columnsType);
			Type t = col.getType();
			if(cs.getFirstForeignKeyDeclarationFromMember(col.getName()) != null) {
				if(t instanceof NumericType) {
					NumericType nt = (NumericType) t;
					if(!nt.isUnsigned()) { // Not done above
						if(!columnsType.contains(" unsigned")) { //unsignedgenerated) {
							if(!(nt instanceof IntType)) {
								sb.append(" unsigned");
							}
						}
					}
					if(nt.hasNullDefault()) { // Not done above
						if(!columnsType.contains(" default null")) { //defaultnullgenerated) {
							//defaultnullgenerated = true;
							sb.append(" default null");
						}
					}
				}
			}
			if(col.isAutoIncrement()) {
				sb.append(" generated always as identity (start with 1, increment by 1) ");
			}
		}
		PrimaryKeyDeclaration pk = cs.getPrimaryKeyDeclaration();
		if(pk != null) {
			sb.append(",   PRIMARY KEY ("+pk.getMember(0));
			for(int i = 1 ; i < pk.getNoMembers() ; i++) {
				sb.append(", "+pk.getMember(i));
			}
			sb.append(")");
		}
		for(int i = 0 ; i < cs.getNoKeys() ; i++) {
			sb.append(", ");
			KeyDeclaration k = cs.getKeyDeclaration(i);
			sb.append("   ");
			if(k.isUnique()) {
				sb.append("unique ");
			}
			sb.append(" ("+k.getMember(0));
			for(int j = 1 ; j < k.getNoMembers() ; j++) {
				sb.append(", "+k.getMember(j));
			}
			sb.append(")");
			if(k.isUsingBtree()) {
				sb.append(" using btree");
			}
		}
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			sb.append(", ");
			ForeignKeyDeclaration c = cs.getForeignKeyDeclaration(i);
			String conname = c.getConstraintname();
			StringBuffer fknames = new StringBuffer();
			fknames.append(c.getReferringMember(0));
			for(int ii = 1 ; ii < c.getNoReferringMembers() ; ii++) {
				fknames.append(", "+c.getReferringMember(ii));
			}
			StringBuffer pknames = new StringBuffer();
			pknames.append(c.getReferredTableMember(0));
			for(int ii = 1 ; ii < c.getNoReferredTableMembers() ; ii++) {
				pknames.append(", "+c.getReferredTableMember(ii));
			}
			if(conname == null) {
				conname = cs.getName()+"_"+fknames.toString().replaceAll(", ", "_")+"_con";
			}
			sb.append("   constraint "+conname+" foreign key ("+fknames.toString()+") references "+c.getReferencedTablename()+" ("+pknames.toString()+")");
			if(c.getOnDelete() != null) sb.append(" ON DELETE "+c.getOnDelete());
			if(c.getOnUpdate() != null) sb.append(" ON UPDATE "+c.getOnUpdate());
		}
		sb.append(" )");
		for(int i = 0 ; i < cs.getNoParameters() ; i++) {
			Parameter p = cs.getParameter(i);
			sb.append(" "+p.getName()+"="+p.getValue());
		}
		return sb.toString();
	}
	
	private String translateTypename(Type t) {
		String typename = t.getTypename();
		typename = t.getTypename().replaceAll("datetime", "date");
		if((t instanceof NumericType) && (typename.indexOf("(") != -1)) {
			typename = typename.substring(0, typename.indexOf("("));
		}
		int idx = typename.lastIndexOf("(");
		if((idx != -1) && (t instanceof NumericType)) {
			return typename.substring(0, idx);
		}
		if(t instanceof TextType) { 
			if(t instanceof TinytextType) { 
				return "varchar(20)";
			}
			return "varchar(2000)";
		}
		if(t instanceof TinytextType) { 
			return "varchar(20)";
		}
		if(t instanceof LobType) {
			if(t instanceof BlobNumeratedType) {
				BlobNumeratedType bnt = (BlobNumeratedType) t;
				return bnt.getTypename()+"("+bnt.getNumeration()+")";
			}
			if(t instanceof BlobType) {
				BlobType bt = (BlobType) t;
				return bt.getTypename();
			}
			if(t instanceof ClobNumeratedType) {
				ClobNumeratedType cnt = (ClobNumeratedType) t;
				return cnt.getTypename()+"("+cnt.getNumeration()+")";
			}
			if(t instanceof ClobType) {
				ClobType ct = (ClobType) t;
				return ct.getTypename();
			}
			if(t instanceof MediumBlobType) {
				return "mediumblob";
			}
			if(t instanceof LongBlobType) {
				return "longblob";
			}
		}
		return typename;
	}

	public String columnType(Column col) {
		StringBuffer sb = new StringBuffer();
		Type t = col.getType();
		sb.append(" "+translateTypename(t));

		boolean unsignedgenerated = false;
		boolean defaultnullgenerated = false;
		boolean notnullgenerated = false;
		if(t instanceof NumericType) {
			NumericType nt = (NumericType) t;
			if(nt.isUnsigned()) {
				unsignedgenerated = true;
				if(!(nt instanceof IntType)) {
					sb.append(" unsigned");
				}
			}
			if(nt.getZerofill()) {
				defaultnullgenerated = true;
				sb.append(" zerofill");
			}
			if(nt.hasNullDefault()) {
				if(!defaultnullgenerated) {
					defaultnullgenerated = true;
					sb.append(" default null");
				}
			}
			if(!notnullgenerated && nt.getNotNull()) {
				sb.append(" not null");
				notnullgenerated = true;
			}
			if(nt.hasDefault() && !(nt.hasNullDefault() || nt.getNotNull())) {
				if(nt instanceof IntType) {
					sb.append(" default '"+((IntType) nt).getDefault()+"'");
				}
				if(nt instanceof DoubleType) {
					sb.append(" default '"+((DoubleType) nt).getDefault()+"'");
				}
				if(nt instanceof DecimalType) {
					sb.append(" default '"+((DecimalType) nt).getDefault()+"'");
				}
			}
		}
		if(t instanceof TextualType) {
			TextualType tt = (TextualType) t;
			if(tt.getNotNull()) {
				if(!defaultnullgenerated) {
					defaultnullgenerated = true;
					sb.append(" default null");
				}
			}
			if(tt.hasDefault()) {
				sb.append(" default '"+tt.getDefault()+"'");
			}
			if(!notnullgenerated && tt.getNotNull()) {
				sb.append(" not null");
				notnullgenerated = true;
			}
		}
		return sb.toString();
	}
	
	public String deleteColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` drop `"+col.getName()+"`";
	}
	
	public String addColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` add `"+col.getName()+"` "+columnType(col);
	}

	public String changeColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` modify `"+col.getName()+"` "+columnType(col);
		//ALTER TABLE t1 MODIFY col1 BIGINT
	}
	
	public String formDate(String default_) throws Exception {
		if(default_ == null) {
			String out = CommonSQLAssembler.getDate(new Date());
			if(out == null) {
				return null;
			}
			return CommonSQLAssembler.getDate(new Date());
		}
		return CommonSQLAssembler.getDate(default_);
	}
	
	public boolean isIntegrityConstraintViolation(SQLException sqle) {
		return sqle.getMessage().startsWith("Duplicate entry");
	}


/*
	public String selectIndexed(String table, String joined, Hashtable<String, Object> criterion) throws Exception {
		StringBuffer sb = new StringBuffer("select * from "+table+" where");
		boolean first = true;
		for(String key : criterion.keySet()) {
			if(!first) {
				sb.append(" and");
			}
			first = false;
			Object obj = criterion.get(key);
			if(obj instanceof Date) {
				sb.append(" "+key+" = '"+formDate(getDate((Date) obj))+"'");
			}
			else {
				sb.append(" "+key+" = "+obj);
			}
		}
		sb.append(";");
		return sb.toString();
	} // select * from avkastning join perfmeas on perfmeas.name = '8901' and avkastning.perfmeasId = perfmeas.Id and avkastning.todate = '2010-02-17 00:00:00';
*/
	public boolean isUniqueConstraintViolation(Exception e) {
		if(!(e instanceof SQLException)) {
			return false;
		}
		SQLException sqle = (SQLException) e;
		return sqle.getErrorCode() == 1169;
	}
	
	public String getMatchingOperator() {
		return "like";
	}

	public String getWildcard() {
		return "%";
	}
	
	public String[] getCurrentDateMacros() {
		return new String[] { "SYSDATE()", "NOW()" };
	}

	private String removeTrailingSemicolon(String sql) {
		if(sql.endsWith(";")) {
			return sql.substring(0, sql.length()-1);
		}
		return sql;
	}

	public String selectAll(CreateStatement cs) {
		return removeTrailingSemicolon(super.selectAll(cs));
	}

	public String getInsert(CreateStatement cs, Hashtable<String, Object> ht)  throws Exception {
		String insert = removeTrailingSemicolon(super.getInsert(cs, ht));
		return insert;
	}
	
	public String selectIndexed(CreateStatement cs, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, pk));
	}

	public String selectIndexed(CreateStatement cs, ForeignKey fk) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, fk));
	}

	public String selectIndexed(CreateStatement cs, Key k) throws Exception {
		return removeTrailingSemicolon(super.selectIndexed(cs, k));
	}

	public String selectSought(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		return removeTrailingSemicolon(super.selectSought(cs, ht));
	}

	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht) throws Exception {
		return removeTrailingSemicolon(super.getUpdate(cs, ht));
	}
	
	public String getUpdate(CreateStatement cs, Hashtable<String, Object> ht, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.getUpdate(cs, ht, pk));
	}
	
	public String getDelete(CreateStatement cs, int key) throws Exception {
		return removeTrailingSemicolon(super.getDelete(cs, key));
	}

	public String getDelete(CreateStatement cs, PrimaryKey pk) throws Exception {
		return removeTrailingSemicolon(super.getDelete(cs, pk));
	}
	
	public String tableLength(String tablename, CreateStatement cs) {
		return removeTrailingSemicolon(super.tableLength(tablename, cs));
	}
	
	public String[] dropTable(CreateStatement cs) {
		String out[] = super.dropTable(cs);
		for(int i = 0 ; i < out.length ; i++) {
			out[i] = removeTrailingSemicolon(out[i]);
		}
		return out;
	}


}
