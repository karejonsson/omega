package org.omega.connectivity.recreatesql;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.omega.configuration.struct.BlobNumeratedType;
import org.omega.configuration.struct.BlobType;
import org.omega.configuration.struct.ClobNumeratedType;
import org.omega.configuration.struct.ClobType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DBMany2ManyRelation;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.LongBlobType;
import org.omega.configuration.struct.MediumBlobType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;
import org.omega.configuration.struct.VarcharType;

public class HSQLDBRecreator extends CommonSQLAssembler { 
	
	public static final String symbol = "HSQLDB";
	
	public HSQLDBRecreator() {
		super(symbol);
	}
	
	public String[] create(CreateStatement cs) {
		return new String[] { _create(cs) };
	}
 
	public String _create(CreateStatement cs) {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE "+cs.getName()+" (\n");
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			//if((i != 0) && (i != cs.getNoColumns() - 1)) {
		    if(i != 0) {
				sb.append(",\n");
			} 
			Column col = cs.getColumn(i);
			String columnsType = columnType(col);
			sb.append("   "+col.getName()+columnsType);
			if(col.isAutoIncrement()) {
				sb.append(" IDENTITY");
			}
		}
		Column autoIncCol = cs.getAutoIncrementColumn();
		PrimaryKeyDeclaration pk = cs.getPrimaryKeyDeclaration();
		if(pk != null) {
			if(!(autoIncCol != null && pk.getNoMembers() == 1)) {
				sb.append(",\n   PRIMARY KEY ");
				// The namehandling below is only implemented for recreation of MySQL statements
				String keyName = pk.getKeyName();
				if(keyName != null) {
					sb.append(keyName+" ");
				}
				sb.append("("+pk.getMember(0));
				for(int i = 1 ; i < pk.getNoMembers() ; i++) {
					sb.append(", "+pk.getMember(i));
				}
				sb.append(")");
			}
		}
		for(int i = 0 ; i < cs.getNoKeys() ; i++) {
			KeyDeclaration k = cs.getKeyDeclaration(i);
			if(!k.isUnique()) {
				continue;
			}
			sb.append(",\n");
			sb.append("   unique ("+k.getMember(0));
			for(int j = 1 ; j < k.getNoMembers() ; j++) {
				sb.append(", "+k.getMember(j));
			}
			sb.append(")");
		}
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			sb.append(",\n");
			ForeignKeyDeclaration c = cs.getForeignKeyDeclaration(i);
			String conname = c.getConstraintname();
			StringBuffer fknames = new StringBuffer();
			fknames.append(c.getReferringMember(0));
			for(int ii = 1 ; ii < c.getNoReferringMembers() ; ii++) {
				fknames.append(", "+c.getReferringMember(ii));
			}
			StringBuffer pknames = new StringBuffer();
			pknames.append(c.getReferredTableMember(0));
			for(int ii = 1 ; ii < c.getNoReferredTableMembers() ; ii++) {
				pknames.append(", "+c.getReferredTableMember(ii));
			}
			if(conname == null) {
				conname = cs.getName()+"_"+fknames.toString().replaceAll(", ", "_")+"_con";
			}
			sb.append("   constraint "+conname+" foreign key ("+fknames.toString()+") references "+c.getReferencedTablename()+" ("+pknames.toString()+")");
		}
		return sb.toString()+"\n);";
	}
	
	private String translateTypename(Type t) {
		String typename = t.getTypename();
		int idx = typename.lastIndexOf("(");
		if((idx != -1) && (t instanceof NumericType)) {
			return typename.substring(0, idx);
		}
		if(t instanceof TextType) { 
			return "varchar(2000)";
		}
		if(t instanceof TinytextType) { 
			return "varchar(20)";
		}
		if(t instanceof LobType) {
			if(t instanceof BlobNumeratedType) {
				return "blob";
			}
			if(t instanceof BlobType) {
				return "blob";
			}
			if(t instanceof ClobNumeratedType) {
				return "clob";
			}
			if(t instanceof ClobType) {
				return "clob";
			}
			if(t instanceof MediumBlobType) {
				return "blob";
			}
			if(t instanceof LongBlobType) {
				return "blob";
			}
		}
		return typename;
	}
	
	public String columnType(Column col) {
		Type t = col.getType();
		String typename = translateTypename(t);
		StringBuffer sb = new StringBuffer();
		sb.append(" "+typename);
		if(t instanceof NumericType) {
			NumericType nt = (NumericType) t;
			if(nt.hasNullDefault()) {
				sb.append(" default null");
			}
			if(nt.getNotNull()) {
				sb.append(" not null");
			}
			if(nt.hasDefault() && !(nt.hasNullDefault() || nt.getNotNull())) {
				if(nt instanceof IntType) {
					sb.append(" default "+((IntType) nt).getDefault());
				}
				if(nt instanceof DoubleType) {
					sb.append(" default "+((DoubleType) nt).getDefault());
				}
				if(nt instanceof DecimalType) {
					sb.append(" default "+((DecimalType) nt).getDefault());
				}
			}
		}
		if(t instanceof TextualType) {
			TextualType tt = (TextualType) t;
			if(tt.hasDefault()) {
				sb.append(" default '"+tt.getDefault()+"'");
			}
			if(tt.getNotNull()) {
				sb.append(" not null");
			}
		}
		return sb.toString();
	}
	
	public String deleteColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` drop column `"+col.getName()+"`";
	}
	
	public String addColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` add `"+col.getName()+"` "+columnType(col);
	}
	
	public String changeColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` alter column `"+col.getName()+"` "+columnType(col);
		//ALTER TABLE PUBLIC.EMPLOYEE ALTER COLUMN SSN CHARACTER(25)
	}
	
	public String formDate(String default_) throws Exception {
		//System.out.println("hsqldb form date "+default_);
		if(default_ == null) {
			String out = CommonSQLAssembler.getDate(new Date());
			if(out == null) {
				return null;
			}
			return CommonSQLAssembler.getDate(new Date())+" 00:00:00";
		}
		String out = CommonSQLAssembler.getDate(default_);
		if(out != null) {
			return out+" 00:00:00";
		}
		return null;
	}
/*	
	public String selectIndexed(String table, Hashtable<String, Object> criterion) throws Exception {
		StringBuffer sb = new StringBuffer("select * from "+table+" join "+joined+" on");
		boolean first = true;
		for(String key : criterion.keySet()) {
			if(!first) {
				sb.append(" and");
			}
			first = false;
			Object obj = criterion.get(key);
			if(obj instanceof Date) {
				sb.append(" "+key+" = '"+formDate(getDate((Date) obj))+"'");
			}
			else {
				sb.append(" "+key+" = "+obj);
			}
		}
		sb.append(";");
		return sb.toString();
	} // select * from avkastning join perfmeas on perfmeas.name = '8901' and avkastning.perfmeasId = perfmeas.Id and avkastning.todate = '2010-02-17 00:00:00';
*/
/*
	private String create(DBMany2ManyRelation m2mr) {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE "+m2mr.getTablename()+" (\n");
		sb.append("  "+m2mr.getRefT1()+" int,\n");
		sb.append("  "+m2mr.getRefT2()+" int,\n");
		sb.append("  UNIQUE("+m2mr.getRefT1()+" , "+m2mr.getRefT2()+" )\n");
		sb.append(");");
		return sb.toString();
	}
	*/
	
	public boolean isIntegrityConstraintViolation(SQLException sqle) {
		return sqle.getMessage().startsWith("integrity constraint violation");
	}
	
	public boolean isUniqueConstraintViolation(Exception e) {
		if(!(e instanceof SQLException)) {
			return false;
		}
		SQLException sqle = (SQLException) e;
		return sqle.getErrorCode() == 104;
	}

/*	
	public static void main(String args[]) {
		DBMany2ManyRelation m2mr = new DBMany2ManyRelation("artist", "album");
		HSQLDBRecreator rec = new HSQLDBRecreator();
		
		DBMany2ManyRelation m2mr_rec = DBMany2ManyRelation.createReciproque(m2mr);
		System.out.println(rec.create(m2mr));
		System.out.println(rec.create(m2mr_rec));
	}
*/
	
	public String getMatchingOperator() {
		return "like";
	}

	public String getWildcard() {
		return "%";
	}
	
	public String[] getCurrentDateMacros() {
		return new String[] { "CURDATE()" };
	}

}

