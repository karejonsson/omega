package org.omega.connectivity.recreatesql;

import java.sql.SQLException;
import java.util.Date;

import org.omega.configuration.struct.BlobNumeratedType;
import org.omega.configuration.struct.BlobType;
import org.omega.configuration.struct.ClobNumeratedType;
import org.omega.configuration.struct.ClobType;
import org.omega.configuration.struct.Column;
import org.omega.configuration.struct.CreateStatement;
import org.omega.configuration.struct.DatetimeType;
import org.omega.configuration.struct.DecimalType;
import org.omega.configuration.struct.DoubleType;
import org.omega.configuration.struct.ForeignKeyDeclaration;
import org.omega.configuration.struct.IntType;
import org.omega.configuration.struct.KeyDeclaration;
import org.omega.configuration.struct.LobType;
import org.omega.configuration.struct.LongBlobType;
import org.omega.configuration.struct.MediumBlobType;
import org.omega.configuration.struct.NumericType;
import org.omega.configuration.struct.Parameter;
import org.omega.configuration.struct.PrimaryKeyDeclaration;
import org.omega.configuration.struct.TextType;
import org.omega.configuration.struct.TextualType;
import org.omega.configuration.struct.TinytextType;
import org.omega.configuration.struct.Type;

public class POSTGRESSQLRecreator extends CommonSQLAssembler {
	
	public static final String symbol = "POSTGRESQL";
	
	public POSTGRESSQLRecreator() {
		super(symbol);
	}

	public String[] create(CreateStatement cs) {
		return new String[] { _create(cs) };
	}
 
	public String _create(CreateStatement cs) {
		StringBuffer sb = new StringBuffer();
		sb.append("CREATE TABLE "+cs.getName()+" (\n");
		for(int i = 0 ; i < cs.getNoColumns() ; i++) {
			if(i != 0) {
				sb.append(",\n");
			}
			Column col = cs.getColumn(i);
			String columnsType = columnType(col);
			sb.append("   "+col.getName()+columnsType);
			//sb.append("   "+col.getName()+" "+col.getType().getTypename());
			/*
			Type t = col.getType();
			String typename = t.getTypename();
			String toPublish = typename;
			int idx = typename.lastIndexOf("(");
			if((idx != -1) && (col.getType() instanceof NumericType)) {
				toPublish = typename.substring(0, idx);
				if(t instanceof IntType) {
					toPublish = "integer";
				}
				if(t instanceof DoubleType) {
					toPublish = "float";
				}
				if(t instanceof DecimalType) {
					toPublish = "float";
				}
			}
			if(t instanceof TextType) { 
				toPublish = "text";
			}
			if(t instanceof DatetimeType) {  
				toPublish = "timestamp";
			}
			if(t instanceof TinytextType) { 
				toPublish = "varchar(20)";
			}
			sb.append("   "+col.getName()+" "+((col.isAutoIncrement()) ? "" : toPublish));
			boolean unsignedgenerated = false;
			boolean defaultnullgenerated = false;
			if(t instanceof NumericType) {
				NumericType nt = (NumericType) t;
				if(nt.isUnsigned()) {
					unsignedgenerated = true;
					//sb.append(" unsigned");
				}
				if(nt.getZerofill()) {
					defaultnullgenerated = true;
					sb.append(" zerofill");
				}
				if(nt.hasNullDefault()) {
					if(!defaultnullgenerated) {
						defaultnullgenerated = true;
						sb.append(((col.isAutoIncrement()) ? "" : " default null"));
					}
				}
				if(nt.getNotNull()) {
					sb.append(((col.isAutoIncrement()) ? "" : " CONSTRAINT no_null not null"));
				}
				if(nt.hasDefault() && !(nt.hasNullDefault() || nt.getNotNull())) {
					if(nt instanceof IntType) {
						sb.append(((col.isAutoIncrement()) ? "" : " default '"+((IntType) nt).getDefault()+"'"));
					}
					if(nt instanceof DoubleType) {
						sb.append(((col.isAutoIncrement()) ? "" : " default '"+((DoubleType) nt).getDefault()+"'"));
					}
					if(nt instanceof DecimalType) {
						sb.append(((col.isAutoIncrement()) ? "" : " default '"+((DecimalType) nt).getDefault()+"'"));
					}
				}
			}
			if(t instanceof TextualType) {
				TextualType tt = (TextualType) t;
				if(tt.getNotNull()) {
					if(!defaultnullgenerated) {
						defaultnullgenerated = true;
						sb.append(" default null");
					}
				}
				if(tt.hasDefault()) {
					sb.append(" default "+tt.getDefault());
				}
			} */
			Type t = col.getType();
			if(cs.getFirstForeignKeyDeclarationFromMember(col.getName()) != null) {
				NumericType nt = (NumericType) t;
				if(!nt.isUnsigned()) { // Not done above
					//if(!unsignedgenerated) {
						//sb.append(" unsigned");
					//}
				}
				if(nt.hasNullDefault()) { // Not done above
					if(!columnsType.contains("default null")) { //defaultnullgenerated) {
						//defaultnullgenerated = true;
						sb.append(" default null");
					}
				}
			}
			if(col.isAutoIncrement()) {
				sb.append(" serial");
			}
		}
		PrimaryKeyDeclaration pk = cs.getPrimaryKeyDeclaration();
		if(pk != null) {
			sb.append(",\n   PRIMARY KEY ("+pk.getMember(0));
			for(int i = 1 ; i < pk.getNoMembers() ; i++) {
				sb.append(", "+pk.getMember(i));
			}
			sb.append(")");
		}
		for(int i = 0 ; i < cs.getNoKeys() ; i++) {
			sb.append(",\n");
			KeyDeclaration k = cs.getKeyDeclaration(i);
			sb.append("   ");
			if(k.isUnique()) {
				sb.append("unique ");
			}
			String keyname = k.getKeyName();
			if(keyname == null) {
				keyname = cs.getName()+"_key_"+i;
			}
			sb.append(" "+" ("+k.getMember(0));
			for(int j = 1 ; j < k.getNoMembers() ; j++) {
				sb.append(", "+k.getMember(j));
			}
			sb.append(")");
		}
		for(int i = 0 ; i < cs.getNoForeignKeyDeclarations() ; i++) {
			sb.append(",\n");
			ForeignKeyDeclaration c = cs.getForeignKeyDeclaration(i);
			String conname = c.getConstraintname();
			StringBuffer fknames = new StringBuffer();
			fknames.append(c.getReferringMember(0));
			for(int ii = 1 ; ii < c.getNoReferringMembers() ; ii++) {
				fknames.append(", "+c.getReferringMember(ii));
			}
			StringBuffer pknames = new StringBuffer();
			pknames.append(c.getReferredTableMember(0));
			for(int ii = 1 ; ii < c.getNoReferredTableMembers() ; ii++) {
				pknames.append(", "+c.getReferredTableMember(ii));
			}
			if(conname == null) {
				conname = cs.getName()+"_"+fknames.toString().replaceAll(", ", "_")+"_con";
			}
			sb.append("   constraint "+conname+" foreign key ("+fknames.toString()+") references "+c.getReferencedTablename()+" ("+pknames.toString()+")");
			if(c.getOnDelete() != null) sb.append(" ON DELETE "+c.getOnDelete());
			if(c.getOnUpdate() != null) sb.append(" ON UPDATE "+c.getOnUpdate());
		}
		sb.append("\n)");
		for(int i = 0 ; i < cs.getNoParameters() ; i++) {
			Parameter p = cs.getParameter(i);
			sb.append(" "+p.getName()+"="+p.getValue());
		}
		return sb.toString()+";";
	}
	
	private String translateTypename(Type t) {
		String typename = t.getTypename();
		String toPublish = typename;
		int idx = typename.lastIndexOf("(");
		if((idx != -1) && (t instanceof NumericType)) {
			toPublish = typename.substring(0, idx);
			if(t instanceof IntType) {
				return "integer";
			}
			if(t instanceof DoubleType) {
				return "float";
			}
			if(t instanceof DecimalType) {
				return "float";
			}
		}
		if(t instanceof TextType) { 
			if(t instanceof TinytextType) { 
				return "varchar(20)";
			}
			return "text";
		}
		if(t instanceof DatetimeType) {  
			return "timestamp";
		}
		if(t instanceof TinytextType) { 
			return "varchar(20)";
		}
		if(t instanceof LobType) {
			if(t instanceof BlobNumeratedType) {
				BlobNumeratedType bnt = (BlobNumeratedType) t;
				return bnt.getTypename()+"("+bnt.getNumeration()+")";
			}
			if(t instanceof BlobType) {
				BlobType bt = (BlobType) t;
				return bt.getTypename();
			}
			if(t instanceof ClobNumeratedType) {
				ClobNumeratedType cnt = (ClobNumeratedType) t;
				return cnt.getTypename()+"("+cnt.getNumeration()+")";
			}
			if(t instanceof ClobType) {
				ClobType ct = (ClobType) t;
				return ct.getTypename();
			}
			if(t instanceof MediumBlobType) {
				return "mediumblob";
			}
			if(t instanceof LongBlobType) {
				return "longblob";
			}
		}
		return typename;
	}

	public String columnType(Column col) {
		Type t = col.getType();
		String typename = translateTypename(t);
		StringBuffer sb = new StringBuffer();
		sb.append(" "+((col.isAutoIncrement()) ? "" : typename));
		boolean unsignedgenerated = false;
		boolean defaultnullgenerated = false;
		if(t instanceof NumericType) {
			NumericType nt = (NumericType) t;
			if(nt.isUnsigned()) {
				unsignedgenerated = true;
				//sb.append(" unsigned");
			}
			if(nt.getZerofill()) {
				defaultnullgenerated = true;
				sb.append(" zerofill");
			}
			if(nt.hasNullDefault()) {
				if(!defaultnullgenerated) {
					defaultnullgenerated = true;
					sb.append(((col.isAutoIncrement()) ? "" : " default null"));
				}
			}
			if(nt.getNotNull()) {
				sb.append(((col.isAutoIncrement()) ? "" : " CONSTRAINT no_null not null"));
			}
			if(nt.hasDefault() && !(nt.hasNullDefault() || nt.getNotNull())) {
				if(nt instanceof IntType) {
					sb.append(((col.isAutoIncrement()) ? "" : " default '"+((IntType) nt).getDefault()+"'"));
				}
				if(nt instanceof DoubleType) {
					sb.append(((col.isAutoIncrement()) ? "" : " default '"+((DoubleType) nt).getDefault()+"'"));
				}
				if(nt instanceof DecimalType) {
					sb.append(((col.isAutoIncrement()) ? "" : " default '"+((DecimalType) nt).getDefault()+"'"));
				}
			}
		}
		if(t instanceof TextualType) {
			TextualType tt = (TextualType) t;
			if(tt.getNotNull()) {
				if(!defaultnullgenerated) {
					defaultnullgenerated = true;
					sb.append(" not null");
				}
			}
			if(tt.hasDefault()) {
				sb.append(" default '"+tt.getDefault()+"'");
			}
		}
		return sb.toString();
	}
	
	public String deleteColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` drop column `"+col.getName()+"`";
	}
	
	public String addColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` add `"+col.getName()+"` "+columnType(col);
	}

	public String changeColumn(CreateStatement cs, Column col) {
		return "alter table `"+cs.getName()+"` alter column `"+col.getName()+"` "+columnType(col);
		//
	}
	
	public String formDate(String default_) throws Exception {
		if(default_ == null) {
			String out = CommonSQLAssembler.getDate(new Date());
			if(out == null) {
				return null;
			}
			return CommonSQLAssembler.getDate(new Date());
		}
		return CommonSQLAssembler.getDate(default_);
	}
	
	public boolean isIntegrityConstraintViolation(SQLException sqle) {
		return sqle.getMessage().startsWith("Duplicate entry");
	}

	public boolean isUniqueConstraintViolation(Exception e) {
		if(!(e instanceof SQLException)) {
			return false;
		}
		SQLException sqle = (SQLException) e;
		return sqle.getErrorCode() == 23505;
	}
	
	public String getMatchingOperator() {
		return "like";
	}

	public String getWildcard() {
		return "%";
	}
	
	public String[] getCurrentDateMacros() {
		return new String[] { "NOW()", "CURRENT_DATE", "CLOCK_TIMESTAMP()" };
	}


}
