package org.omega.connectivity.manager;

import org.omega.connectivity.connections.DBConnectionSpecificationNamecastVisitor;
import org.omega.connectivity.hsql.connections.OmegaEmbeddedHSQL;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

import se.modlab.generics.exceptions.IntolerableException;

public class GenesisVisitor extends DBConnectionSpecificationNamecastVisitor {

    public void visitDBHSQLDBFile() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
        String file = this.getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FILE);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
        Object create_o = this.getAttribute("create");
        boolean create = false;
        if (create_o == null) {
            create = false;
        } else if (create_o instanceof Boolean) {
            Boolean create_b = (Boolean)create_o;
            create = create_b;
        } else {
            create = false;
        }
        this.out = create ? OmegaEmbeddedHSQL.connectHSQLDBFile(configfile, encoding, file, username, password, frametitle, dbname) : OmegaEmbeddedHSQL.connectHSQLDBFile(configfile, encoding, file, username, password, frametitle, dbname);
    }

    public void visitDBHSQLDBMemory() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
        this.out = OmegaEmbeddedHSQL.initializeAndConnectHSQLDBMemory(configfile, encoding, dbname, username, password, frametitle);
    }

    public void visitDBHSQLDBRemote() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
        String protocol = this.getAttributeAsString("protocol");
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
        this.out = OmegaEmbeddedHSQL.connectHSQLDBRemote(configfile, encoding, ipresolvable, port, protocol, username, password, dbname, frametitle, tflp);
    }

	public void visitDBIBMDB2() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
		out = OmegaEmbeddedInternal.connectIBMDB2(configfile, encoding, ipresolvable, port, dbname, username, password, frametitle, tflp);
	}

	public void visitDBMSSQLServer() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
		out = OmegaEmbeddedInternal.connectMSSQLServer(configfile, encoding, ipresolvable, port, dbname, username, password, frametitle, tflp);
	}

	public void visitDBMySQL() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
		out = OmegaEmbeddedInternal.connectMYSQL(configfile, encoding, ipresolvable, port, dbname, username, password, frametitle, tflp);
	}

	public void visitDBOracle() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
		out = OmegaEmbeddedInternal.connectOracle(configfile, encoding, ipresolvable, port, dbname, username, password, frametitle, tflp);
	}

	public void visitDBPOSTGRES() throws IntolerableException {
		String configfile = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.CONFIGFILE);
		String encoding = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.ENCODING);
		String username = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.USERNAME);
		String password = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PASSWORD);
		String frametitle = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.FRAMETITLE);
		String dbname = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.DBNAME);
		String ipresolvable = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.IPRESOLVABLE);
		String port = getAttributeAsString(DBConnectionSpecificationNamecastVisitor.PORT);
		TunnelForwardLParameters tflp = (TunnelForwardLParameters) getAttribute(DBConnectionSpecificationNamecastVisitor.SSHTUNNELPARAMETERS);
		out = OmegaEmbeddedInternal.connectPOSTGRES(configfile, encoding, ipresolvable, port, dbname, username, password, frametitle, tflp);
	}

}
