package org.omega.connectivity.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.omega.configuration.model.ModelConfiguration;
import org.omega.configuration.recreatesql.Recreator;
import org.omega.connectivity.abstraction.EmbeddedModelHandling;
import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.helpers.OmegaEmbeddedConfig;
import org.omega.connectivity.jdbc.resultset.JDBCConnection;
import org.omega.connectivity.jdbc.resultset.OracleJDBCConnection;
import org.omega.connectivity.logging.RetainingSQLIntermediateLogger;
import org.omega.connectivity.logging.RetainingSQLMemoryLogger;
import org.omega.connectivity.logging.SQLIntermediateLogger;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.properties.InstallationProperties;
import org.omega.connectivity.recreatesql.IBMDB2Recreator;
import org.omega.connectivity.recreatesql.MSSQLServerRecreator;
import org.omega.connectivity.recreatesql.MYSQLRecreator;
import org.omega.connectivity.recreatesql.OracleRecreator;
import org.omega.connectivity.recreatesql.POSTGRESSQLRecreator;
import org.omega.connectivity.recreatesql.RecreatorManager;
import org.omega.connectivity.sshtunnels.TunnelForwardLEncapsulation;
import org.omega.connectivity.sshtunnels.TunnelForwardLParameters;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.SystemError;
 
import se.modlab.generics.exceptions.UserRuntimeError;

public class OmegaEmbeddedInternal {
	
	private static TunnelForwardLEncapsulation getTunnelForwardLEncapsulation(TunnelForwardLParameters tflp) throws UserRuntimeError {
		if(tflp == null) {
			return null;
		}
		TunnelForwardLEncapsulation tfle = null;
		try {
			if(tflp != null) {
				tfle = new TunnelForwardLEncapsulation(tflp);
			}
		}
		catch(Exception e) {
			String message = "Parameters for SSH tunnel not complete";
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			if(tfle != null && !tfle.openTunnel()) {
				String message = "Cannot open SSH tunnel";
				UserRuntimeError ure = new UserRuntimeError(message);
				throw ure;
			}
		}
		catch(Exception e) {
			if(tfle != null) {
				try {
					tfle.closeTunnel();
				}
				catch(Exception e2) {
				}
			}
			String message = "Cannot open SSH tunnel: "+e.getMessage();
			UserRuntimeError ure = new UserRuntimeError(message);
			throw ure;
		}
		try {
			Thread.sleep(300);
		}
		catch(Exception e) {
		}
		return tfle;
	}

	private static JDBCConnection getJDBCConnection(String driverClass, String dbImplementationName, String dburl, String user, String password, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		if(tflp != null && !tflp.hasAll()) {
	    	String message = "Insufficient SSH tunnel parameters provided";
	    	UserRuntimeError ure = new UserRuntimeError(message);
	    	throw ure;
		}
		TunnelForwardLEncapsulation tfle = getTunnelForwardLEncapsulation(tflp);
		try {
	        Class.forName(driverClass);
	    } 
	    catch (Exception e) {
	    	String message = "ERROR: failed to load "+dbImplementationName+" JDBC driver! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message, e);
	    	throw ure;
	    }
		Connection c = null;
		try {
			c = DriverManager.getConnection(dburl, user, password);				
		}
		catch(SQLException sqle) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
	    	String message = "Unable to create connection! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message, sqle);
	    	throw ure;
		}
		if(c == null) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
	    	String message = "ERROR: Null connection created! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message);
	    	throw ure;
		}
		return new JDBCConnection(c, driverClass, dbImplementationName, dburl, user, password, dbname, tfle);
	}
	
	private static OracleJDBCConnection getOracleJDBCConnection(String driverClass, String dbImplementationName, String dburl, String user, String password, String dbname, TunnelForwardLParameters tflp) throws IntolerableException {
		if(tflp != null && !tflp.hasAll()) {
	    	String message = "Insufficient SSH tunnel parameters provided";
	    	UserRuntimeError ure = new UserRuntimeError(message);
	    	throw ure;
		}
		TunnelForwardLEncapsulation tfle = getTunnelForwardLEncapsulation(tflp);
	    try {
	    	//System.out.println("GDBAPPEmbedded.getConnection laddar <"+driverClass+">");
	        Class.forName(driverClass);
	    } 
	    catch (Exception e) {
	    	String message = "ERROR: failed to load "+dbImplementationName+" JDBC driver! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message, e);
	    	throw ure;
	    }
		//System.out.println("GDBAPPEmbedded.getConnection laddade <"+driverClass+">");
		Connection c = null;
		try {
			c = DriverManager.getConnection(dburl, user, password);				
		}
		catch(SQLException sqle) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
	    	String message = "Unable to create connection! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message, sqle);
	    	throw ure;
		}
		if(c == null) {
			try {
				if(tfle != null) {
					tfle.closeTunnel();
				}
			}
			catch(Exception e2) {
			}
	    	String message = "ERROR: Null connection created! URL="+dburl;
	    	UserRuntimeError ure = new UserRuntimeError(message);
	    	throw ure;
		}
		return new OracleJDBCConnection(c, driverClass, dbImplementationName, dburl, user, password, dbname, tfle);
	}

	public static ConnectionOperations getPureMYSQLConnection(String ipresolvable, String port, String dbname, String user, String password, TunnelForwardLParameters tflp) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		String dburl = InstallationProperties.getString(InstallationProperties.mysql_urltemplate, "");//"jdbc:mysql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		return getJDBCConnection(InstallationProperties.getString(InstallationProperties.mysql_driverclass, ""), MYSQLRecreator.symbol, dburl, user, password, dbname, tflp);
	}
	
	public static EmbeddedModelHandling connectMYSQL(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws IntolerableException {
		TunnelForwardLEncapsulation tfle = getTunnelForwardLEncapsulation(tflp);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new SystemError("Cannot initialize MySQL database with Omega config described in file "+gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.mysql_urltemplate, "");//"jdbc:mysql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.mysql_driverclass, ""), MYSQLRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(MYSQLRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		if(hs == null) {
			return null;
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}
	
	public static ConnectionOperations getPureOracleConnection(String ipresolvable, String port, String dbname, String user, String password, TunnelForwardLParameters tflp) throws IntolerableException {
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.oracle_urltemplate, "");
		//String dburl = "jdbc:oracle:thin:@"+ipresolvable.trim()+":"+port.trim()+":"+dbname.trim();
		return getOracleJDBCConnection(InstallationProperties.getString(InstallationProperties.oracle_driverclass, ""), OracleRecreator.symbol, dburl, user, password, dbname, tflp);
	}
	
	public static EmbeddedModelHandling connectOracle(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws IntolerableException {
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new SystemError("Cannot initialize Oracle database with Omega config described in file "+gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.oracle_urltemplate, "");
		ConnectionOperations c = getOracleJDBCConnection(InstallationProperties.getString(InstallationProperties.oracle_driverclass, ""), OracleRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(OracleRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		if(hs == null) {
			return null;
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}
	
	public static ConnectionOperations getPureIBMDB2Connection(String ipresolvable, String port, String dbname, String user, String password, TunnelForwardLParameters tflp) throws IntolerableException {
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.ibmdb2_urltemplate, "");
		//String dburl = "jdbc:db2://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		return getJDBCConnection(InstallationProperties.getString(InstallationProperties.ibmdb2_driverclass, ""), IBMDB2Recreator.symbol, dburl, user, password, dbname, tflp);
	}
	
	public static EmbeddedModelHandling connectIBMDB2(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws IntolerableException {
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new SystemError("Cannot initialize IBM-DB2 database with Omega config described in file "+gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.ibmdb2_urltemplate, "");
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.ibmdb2_driverclass, ""), IBMDB2Recreator.symbol, dburl, user, password, dbname,  tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(IBMDB2Recreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		if(hs == null) {
			return null;
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}
	
	public static ConnectionOperations getPurePOSTGRESConnection(String ipresolvable, String port, String dbname, String user, String password, TunnelForwardLParameters tflp) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.postgres_urltemplate, "");
		//String dburl = "jdbc:postgresql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		return getJDBCConnection(InstallationProperties.getString(InstallationProperties.postgres_driverclass, ""), POSTGRESSQLRecreator.symbol, dburl, user, password, dbname, tflp);
	}

	public static EmbeddedModelHandling connectPOSTGRES(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws IntolerableException {
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new SystemError("Cannot initialize Postgres database with Omega config described in file "+gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.postgres_urltemplate, "");
		//String dburl = "jdbc:postgresql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.postgres_driverclass, ""), POSTGRESSQLRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(POSTGRESSQLRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		if(hs == null) {
			return null;
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}

	public static ConnectionOperations getPureMSSQLServerConnection(String ipresolvable, String port, String dbname, String user, String password, TunnelForwardLParameters tflp) throws IntolerableException {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.mssqlserver_urltemplate, "");
		//String dburl = "jdbc:sqlserver://"+ipresolvable.trim()+":"+port.trim()+";databaseName="+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		return getJDBCConnection(InstallationProperties.getString(InstallationProperties.mssqlserver_driverclass, ""), MSSQLServerRecreator.symbol, dburl, user, password, dbname, tflp);
	}
	
	public static EmbeddedModelHandling connectMSSQLServer(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws IntolerableException {
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new SystemError("Cannot initialize MSSQLServer database with Omega config described in file "+gdbappconfig);
		}
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.mssqlserver_urltemplate, "");
		//String dburl = "jdbc:sqlserver://"+ipresolvable.trim()+":"+port.trim()+";databaseName="+dbname.trim();
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.mssqlserver_driverclass, ""), MSSQLServerRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(MSSQLServerRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		if(hs == null) {
			return null;
		}
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		return new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
	}

	public static EmbeddedModelHandling initializeMYSQL(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws Exception {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.mysql_urltemplate, "");
		//String dburl = "jdbc:mysql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new Exception("Cannot initialize MySQL database with Omega config described in file "+gdbappconfig);
		}
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.mysql_driverclass, ""), MYSQLRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(MYSQLRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		EmbeddedModelHandling details = new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
		details.initialize();
		return details;
	}

	public static EmbeddedModelHandling initializeOracle(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws Exception {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.oracle_urltemplate, "");
		//String dburl = "jdbc:oracle:thin:@"+ipresolvable.trim()+":"+port.trim()+":"+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new Exception("Cannot initialize Oracle database with Omega config described in file "+gdbappconfig);
		}
		ConnectionOperations c = getOracleJDBCConnection(InstallationProperties.getString(InstallationProperties.oracle_driverclass, ""), OracleRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(OracleRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		EmbeddedModelHandling details = new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
		details.initialize();
		return details;
	}

	public static EmbeddedModelHandling initializeIBMDB2(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws Exception {
		//jdbc:derby:net://localhost:1527/sample
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.ibmdb2_urltemplate, "");
		//String dburl = "jdbc:db2://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new Exception("Cannot initialize IBM DB2 database with Omega config described in file "+gdbappconfig);
		}
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.ibmdb2_driverclass, ""), IBMDB2Recreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(IBMDB2Recreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		EmbeddedModelHandling details = new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
		details.initialize();
		return details;
	}

	public static EmbeddedModelHandling initializePOSTGRES(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws Exception {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.postgres_urltemplate, "");
		//String dburl = "jdbc:postgresql://"+ipresolvable.trim()+":"+port.trim()+"/"+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new Exception("Cannot initialize Postgres database with Omega config described in file "+gdbappconfig);
		}
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.postgres_driverclass, ""), POSTGRESSQLRecreator.symbol, dburl, user, password, dbname, tflp);
		if(c == null) {
			System.out.println("OmegaEmbedded.initializePOSTGRES no connection");
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(POSTGRESSQLRecreator.symbol); 
		if(rec == null) {
			System.out.println("OmegaEmbedded.initializePOSTGRES no recreator");
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		EmbeddedModelHandling details = new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
		details.initialize();
		return details;
	}

	/*
	public static void main(String args[]) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("Allt ok ladda kod");
		}
		catch(Exception e) {
			System.err.println("Fel ladda kod "+e.getMessage());
			return;
		}
		try {
			Connection conn = DriverManager.getConnection( 
					"jdbc:sqlserver://192.168.100.87:1433;databaseName=FAST2000_boras_20100615_01", 
					"sa", 
					"server"  );
			System.out.println("Allt ok skapa connection");
		}
		catch(Exception e) {
			System.err.println("Fel skapa connection "+e.getMessage());
		}
	}


	 */

	public static EmbeddedModelHandling initializeMSSQLServer(String gdbappconfig, String encoding, String ipresolvable, String port, String dbname, String user, String password, String frametitle, TunnelForwardLParameters tflp) throws Exception {
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_ipresolvable, ipresolvable);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_port, port);
		InstallationProperties.setUserValue(InstallationProperties.ingoingparameter_dbname, dbname);
		String dburl = InstallationProperties.getString(InstallationProperties.mssqlserver_urltemplate, "");
		//String dburl = "jdbc:sqlserver://"+ipresolvable.trim()+":"+port.trim()+";databaseName="+dbname.trim();
		//System.out.println("MySQL connect: URL="+dburl+", password="+password+", user="+user);
		if(gdbappconfig == null || gdbappconfig.length() == 0) {
			throw new Exception("Cannot initialize MSSQLServer database with Omega config described in file "+gdbappconfig);
		}
		ConnectionOperations c = getJDBCConnection(InstallationProperties.getString(InstallationProperties.mssqlserver_driverclass, ""), "MSSQLServer", dburl, user, password, dbname, tflp);
		if(c == null) {
			return null;
		}
		Recreator rec = RecreatorManager.getRecreator(MSSQLServerRecreator.symbol); 
		if(rec == null) {
			return null;
		}
		ModelConfiguration hs = OmegaEmbeddedConfig.getConfig(gdbappconfig, encoding, true);
		SQLMemoryLogger slw = new RetainingSQLMemoryLogger(false);
		SQLIntermediateLogger si = new RetainingSQLIntermediateLogger();
		EmbeddedModelHandling details = new EmbeddedModelHandling(c, rec, hs, slw, si, frametitle);
		details.initialize();
		return details;
	}

}
