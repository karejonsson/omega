package org.omega.connectivity.logging;

import java.io.InputStream;
import java.sql.SQLException;

import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.resultset.SQLResult;

public class DiscardingSQLIntermediateLogger implements SQLIntermediateLogger {

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public SQLIssued getElementAt(int idx) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLastSQL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLResult select_via_executeQuery(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SQLResult insert_via_executeQuery(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean create_execute(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int delete_via_executeUpdate(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update_via_executeUpdate(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int execute_Insert(ConnectionOperations conn, String sql,
			SQLMemoryLogger slw, String[] pkfields) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int uploadToBlob_via_executeUpdate(ConnectionOperations conn,
			String sql, SQLMemoryLogger slw, InputStream is)
			throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public InputStream downloadFromBlob_with_executeQuery(ConnectionOperations conn,
			String sql, SQLMemoryLogger slw) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
