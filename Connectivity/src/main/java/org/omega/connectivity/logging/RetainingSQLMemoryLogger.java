package org.omega.connectivity.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Vector;

public class RetainingSQLMemoryLogger implements SQLMemoryLogger {

	private boolean logSQL = true;
	private final static int noMessagesPreserved = 3000;

	private Vector<String> v = new Vector<String>();

	public RetainingSQLMemoryLogger(boolean _logSQL) {
		logSQL = _logSQL; 
	}
	
	public int getLength() {
		return v.size();
	}

	public String getTextContent() {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < v.size(); i++) {
			sb.append(v.elementAt(i)+"\n");
		}
		return sb.toString();
	}

	public void logText(String text) {
		while(v.size() > noMessagesPreserved) {
			v.remove(0);
		}
		v.addElement(text);
		if(!logSQL) {
			return;
		}
	}
	
	public void logTrace(Throwable e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		logText(sw.toString());
		Throwable t = e.getCause();
		if(t != null) {
			logText("Subjacent: "+t.getMessage());
			sw = new StringWriter();
			t.printStackTrace(new PrintWriter(sw));
			logText(sw.toString());
		}
	}
	
	public void log(String text, Throwable e) {
		logText(text);
		logTrace(e);
		Throwable t = e.getCause(); 
		if(t != null) {
			log("Subjacent: "+t.getMessage(), t);
		}
	}

	public void reset() {
		v.clear();
	}

}
