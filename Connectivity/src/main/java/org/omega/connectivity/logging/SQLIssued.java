package org.omega.connectivity.logging;

import java.util.Date;

public class SQLIssued {
	private String sql = null;
	private long time;
	private String outcome;
	public SQLIssued(String _sql) {
		sql = _sql;
		time = System.currentTimeMillis();
	}
	public void setOutcome(String _outcome) {
		outcome = _outcome;
	}
	public String getSQL() {
		return sql;
	}
	public String getTime() {
		return new Date(time).toString();
	}
	public String getOutcome() {
		return outcome;
	}
}

