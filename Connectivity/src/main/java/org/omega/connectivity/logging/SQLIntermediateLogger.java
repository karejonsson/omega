package org.omega.connectivity.logging;

import java.io.InputStream;
import java.sql.SQLException;

import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.logging.SQLIssued;
import org.omega.connectivity.logging.SQLMemoryLogger;
import org.omega.connectivity.resultset.SQLResult;

public interface SQLIntermediateLogger {
	
	public int getLength();
	
	public SQLIssued getElementAt(int idx);
	
	public String getLastSQL();
	
	public SQLResult select_via_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	public SQLResult insert_via_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	public boolean create_execute(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	
	public int delete_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	
	public int update_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	
	public int execute_Insert(ConnectionOperations conn, String sql, SQLMemoryLogger slw, String pkfields[]) throws Exception;
	
	public int uploadToBlob_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw, InputStream is) throws SQLException;
	
	public InputStream downloadFromBlob_with_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException;
	
}
