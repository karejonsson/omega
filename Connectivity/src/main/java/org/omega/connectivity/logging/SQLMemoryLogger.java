package org.omega.connectivity.logging;

public interface SQLMemoryLogger {

	public int getLength();
	public String getTextContent();
	public void logText(String text);	
	public void logTrace(Throwable e);
	public void log(String text, Throwable e);
	public void reset();
	
}
