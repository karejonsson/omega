package org.omega.connectivity.logging;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Vector;

import org.omega.connectivity.connections.ConnectionOperations;
import org.omega.connectivity.resultset.SQLResult;

public class RetainingSQLIntermediateLogger implements SQLIntermediateLogger {

	public RetainingSQLIntermediateLogger() {
	}

	private Vector<SQLIssued> queue = new Vector<SQLIssued>();
	private static int len = 50;
	
	private void addToLogQueue(SQLIssued sqli) {
		while(queue.size() > len) {
			queue.remove(0);
		}
		queue.add(sqli);
	}
	
	public int getLength() {
		return queue.size();
	}
	
	public SQLIssued getElementAt(int idx) {
		return queue.elementAt(idx);
	}
	
	public String getLastSQL() {
		return queue.elementAt(queue.size()-1).getSQL();
	}
	
	public SQLResult select_via_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			SQLResult rs = conn.select_via_executeQuery(sql);
			if(rs != null) {
				sqli.setOutcome("OK");
			}
			else {
				sqli.setOutcome("Returns null result set");
			}
			return rs;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			sqli.setOutcome("Error: "+sqle.getMessage());
			throw sqle;
		}
	}

	public SQLResult insert_via_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			SQLResult rs = conn.insert_via_executeQuery(sql);
			if(rs != null) {
				sqli.setOutcome("OK");
			}
			else {
				sqli.setOutcome("Returns null result set");
			}
			return rs;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			sqli.setOutcome("Error: "+sqle.getMessage());
			throw sqle;
		}
	}

	public boolean create_execute(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			boolean out = conn.create_execute(sql);
			sqli.setOutcome("OK");
			return out;
		}
		catch(SQLException sqle) {
			sqli.setOutcome("Error: "+sqle.getMessage());
			slw.logText(sqle.getMessage());
			throw sqle;
		}
	}
	
	public int delete_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			int out = conn.delete_via_executeUpdate(sql);
			sqli.setOutcome("OK");
			return out;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			throw sqle;
		}
	}
	
	public int update_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			int out = conn.update_via_executeUpdate(sql);
			sqli.setOutcome("OK");
			return out;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			throw sqle;
		}
	}
	
	public int execute_Insert(ConnectionOperations conn, String sql, SQLMemoryLogger slw, String pkfields[]) throws Exception {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		if(sql.trim().toLowerCase().substring(0,6).compareTo("insert") != 0) {
			String message = "SQL which is not insert in call to executeInsert. SQL="+sql;
			sqli.setOutcome(message);
			slw.logText(message);
			throw new Exception(message);
		}
		try {
			SQLResult rs = conn.insert_GetGeneratedKeys(sql, pkfields);
			//Statement statement = conn.createStatement();
			//statement.execute(sql, Statement.RETURN_GENERATED_KEYS);
		    //ResultSet rs = statement.getGeneratedKeys();
			sqli.setOutcome("OK");
			//(new Throwable()).printStackTrace();

		    if (rs.next()) {
		    	int autoIncrement = rs.getInt(1);
			    rs.close();
			    sqli.setOutcome("Insert OK. Autoincrement = "+autoIncrement);
		        return autoIncrement;
		    } 
		    rs.close();
		}
		catch(SQLException sqle) {
			sqli.setOutcome(sqle.getMessage());
			slw.logText(sqle.getMessage());
			throw sqle;
		}
		String message = "Got no auto increment value in return. SQL="+sql;
		sqli.setOutcome(message);
		slw.logText(message);
		throw new Exception(message);
	}
	
	public int uploadToBlob_via_executeUpdate(ConnectionOperations conn, String sql, SQLMemoryLogger slw, InputStream is) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			int out = conn.uploadToBlob_via_executeUpdate(sql, is);
			sqli.setOutcome("OK");
			return out;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			sqli.setOutcome("Error");
			throw sqle;
		}
	}
	
	public InputStream downloadFromBlob_with_executeQuery(ConnectionOperations conn, String sql, SQLMemoryLogger slw) throws SQLException {
		SQLIssued sqli = new SQLIssued(sql);
		addToLogQueue(sqli);
		slw.logText(sql);
		try {
			InputStream is = conn.downloadFromBlob_with_executeQuery(sql);
			if(is != null) {
				sqli.setOutcome("OK");
			}
			else {
				sqli.setOutcome("Returns null result set");
			}
			return is;
		}
		catch(SQLException sqle) {
			slw.logText(sqle.getMessage());
			sqli.setOutcome("Error: "+sqle.getMessage());
			throw sqle;
		}
	}

}
